# Release Notes

## 1.15.0 - (2019-11-21)

* FEATURE - Incluído recurso que possibilita baixar o relatório de detalhes dos pedidos em xls.

	```
    Path: /relatorios/{id}
    Verbo HTTP: GET
    Método SDK: RelatorioResource.gerar(...);
    Resumo: Este recurso permite que o usuário realize o download do relatório.
    ```
    
* FEATURE - Incluído recurso que realiza a leitura do relatório publicado na fila (blue.rh.relatorios.detalhes.pedidos.queue), verificando se o download pode ou não ser realizado.

* FEATURE - Incluído recurso que possibilita excluir um ou mais relatórios.

	```
    Path: /relatorios/deletar
    Verbo HTTP: POST
    Método SDK: RelatorioResource.deletar(...);
    Resumo: Este recurso permite que o usuário realize a deleção de um ou mais relatórios que foram gerados por ele mesmo.
    ```

* BUGFIX - Realizada a correção de erros sobre duplicidade de detalhes do pedido e totalização do seu respectivo conteúdo.
 
## 1.14.0 - (2019-10-31)

* FEATURE - Incluído recurso que altera dados do usuário (email e data de nascimento).

	```
    Path: /usuarios/{id}
    Verbo HTTP: PUT
    Método SDK: UsuarioResource.atualizarUsuarioResumido(...);
    Resumo: Este recurso permite atualizar o email e a data de nascimento do usuário informado
    ```

* CHANGE - Modificado a forma em que o serviço de pedido obtem a quantidade mínima de dias para upload da planilha, configurações setadas no docker-compose.

* CHANGE - Alterado a validação inicial do upload de pedido e unidade de entrega informando uma mensagem de erro mais coerente (mensagem: O arquivo não deve conter mais de uma aba) quando o arquivo possuir mais de uma aba.

* FEATURE - Incluído path para solicitação de relatório.

    ```
    Path: /relatorios
    Verbo HTTP: POST
    Método SDK: RelatorioResource.solicitar(...);
    Resumo: Este recurso permite que o usuário realize um solicitação de relatório baseado em um intervalo de tempo.
	``` 
	

* FEATURE - Incluído path para listar as solicitações de relatório.

    ```
    Path: /relatorios
    Verbo HTTP: GET
    Método SDK: RelatorioResource.listar(...);
    Resumo: Este recurso permite que o usuário visualize todas as suas solicitações de relatório abertas em um determinado período.
	```

* FEATURE - Incluído recurso que informa alguns dados necessários para a salesforce, informando na requisição o cpf e o identificador do grupo empresa.

	```
    Path: /usuarios?cpf=&idGrupoEmpresa=
    Verbo HTTP: GET
    Método SDK: UsuarioResource.obterUsuario(...);
    Resumo: Este recurso informa alguns dados do usuário passando o identificador do Grupo Empresa e o cpf.
	```
	
## 1.13.1 - (2019-10-24)

* HOTFIX - Realizada a correção da leitura do arquivo no processo de upload da planilha de unidades de entregas.

## 1.13.0 - (2019-10-10)

* CHANGE - Modificado o serviço de pedido centralizado e descentralizado para permitir o cancelamento do pedido quando há problemas de limite.

* CHANGE - Modificado enum de status da carga benefício utilizado no segundo nível do pedido.
<p><b>"GET - /pedidos/{id}/cargas-detalhes"</b></p>

* CHANGE - Alterado upload do pedido e upload do cadastro da unidade de entrega para validar a quantidade máxima de caracteres dos campos necessários.

* CHANGE - Alterado o valor maximo do crédito na validação do upload do pedido de 4999,00 para 4999,99.

* CHANGE - Modificado enum de status da carga benefício utilizado no segundo nível do pedido.
<p><b>"GET - /pedidos/{id}/cargas-detalhes"</b></p>

## 1.12.0 - (2019-09-19)

* FEATURE - Incluído task de execução parametrizavel que possui a finalidade de buscar pedidos com os status de pagamento "Limite de Credito Insuficiente" e "Limite de Credito Vencido" para validar se, de acordo com o limite de credito do grupo, o pedido pode prosseguir o seu fluxo.

* CHANGE - Modificada a query que consulta as pendências de TED para incluir o status: cancelado parcial.

* CHANGE - Incluído nova estrutura de integração VA e VR para a tabela "GruposEmpresasProdutos", alterando os paths:
<p><b>"POST - /grupos-empresas"</b></p>
<p><b>"PUT/PATCH - /grupos-empresas/{id}"</b></p>

* CHANGE - Criado PendenciasPagamentoTedResponse com informações dos dados bancários e número do último pedido com pendência, alterando como response o path:
<p>"GET - /notificacoes/pendencias-ted"</p>

* CHANGE - Incluído o parâmetro "FORMA_PAGAMENTO" no json de response, alterando o path:
<p>"GET - /grupos-empresas/{idGrupoEmpresa}/parametros"</p>

* CHANGE - Incluído o parâmetro "nome-fantasia" no payload "EmpresaPersist", alterando os paths:
<p>"POST - /grupos-empresas"</p> 
<p>"POST - /grupos-empresas/{id}/subgrupos-empresas"</p>
<p>"POST - /subgrupos-empresas/{id}/empresas"</p>
<p>"PUT/PATCH - /subgrupos-empresas/{id}/empresas/{idEmpresa}."</p>

## 1.11.0 - (2019-08-29)

* CHANGE - Alterado o endpoint de cadastro de Grupo Empresa para aceitar o cadastro de múltiplos contatos.

* CHANGE - Alterado path de processamento do pedido (POST /upload) para alterar o status do pagamento caso o grupo empresa pós-pago esteja com o limite de crédito vencido ou insuficiente.

* CHANGE - Alterado relatório (ben_rps_v1.jrxml e ben_rps_v1.jasper) para exibição do CNPJ da Ben no título.

* FEATURE - Incluído configurações de disparos de e-mails sobre limite de crédito de acordo com o status de pagamento de um pedido. 

* FEATURE - Incluído parâmetro de vigência de limite no cadastro e atualização de grupo e subgrupo, incluída validação do parâmetro de acordo com o tipo de contrato.   

## 1.10.1 - (2019-08-13)

* BUGFIX - Gerada nova imagem da aplicação com a validação da funcionalidade de RPS.

## 1.10.0 - (2019-08-08)

* FEATURE - Incluído recurso dos dados do limite de crédito para usuarios de grupo empresa pós-pago e que não fazem parte da lista de exceções (tabela ExcecoesLimiteDisponivel).

	```
    Path: /limite-credito
    Verbo HTTP: GET
    Método SDK: LimiteCreditoResource.obterLimiteCredito(...);
    Resumo: Este recurso informa dados do limite de crédito disponível para o grupo empresa pós-pago e que não fazem parte da lista de exceções (tabela ExcecoesLimiteDisponivel).
	```

* FEATURE - Incluído servico/entidade da tabela ExcecoesLimiteDisponivel (WhiteList para não considerar valor limite do grupo).

* FEATURE - Incluído recurso que realiza a atualização dos dados básicos de um funcionário.

	```
    Path: /funcionarios/{id}
    Verbo HTTP: PUT
    Método SDK: FuncionarioResource.atualizar(...);
    Resumo: Este recurso permite atualizar dados básicos de um funcionário.
	```
	
* FEATURE - Notificação de arquivo pedido em processamento.

    ```
    Path: /notificacoes/pedidos
    Verbo HTTP: GET
    Método SDK: NotificacaoResource.notificacaoArquivoPedidoEmProcessamento(...);
    Resumo: Este recurso permite informar se existe algum arquivo de pedido em processamento (status 0 - Recebido, status 1 - Importado).
	``` 
	

* CHANGE - Modificado o tipo de validação do usuário no seviço de Notificações no método de buscar unidade de entrega em processamento.

## 1.9.1 - (2019-07-19)

* BUGFIX - Utilizado a estratégia de lock pessimita em consulta de empresas para corrigir falha de dead lock em banco de dados. 

## 1.9.0 - (2019-07-18)

* CHANGE - Refatorado métodos de validação de usuário que faziam a mesma coisa e junit (validaUsuario(), autenticarUsuario() e isIdUsuarioSalvo())

* CHANGE - Alterado a execução do fluxo do pedido para a criação de funcionário e sua estrutura, deixando de utilizar a procedure SP_Beneficio_IncluirFuncionario e sua lógica foi construída em nível de API para evitar a duplicação de dados.

* CHANGE - Alterado listagem do detalhe do pedido (terceiro nível), incluindo os campos de endereço e indicador de emissão de cartão.

* CHANGE - Alterado listagem do detalhe do pedido (segundo nível), incluindo a data de crédito do benefício. 

* CHANGE - Adicionado validação dos campos obrigatórios na consulta de pedidos (idGrupoEmpresa e idUsuarioSessao).

* CHANGE - Incluído condição "Lote" do tipo de entrega do cartão no endpoint de upload do pedido (/upload).

* CHANGE - Realizado a correção para salvar o parâmetro tipo de entrega do cartão com o valor PO quando recebido o tipo PORTA_A_PORTA via request. 

* FEATURE - Incluído processo de envio de e-mail para Arquivo Unidade Entrega com o status de processamento com sucesso ou erro.

* CHANGE - Adicionado validação de primeiro nivel para o tipo de pedido Lote. A diferença principal está no layout da planilha, onde em lote temos Unidade de entrega obrigatorio e não temos os campos de endereço.

* FEATURE - Incluído recurso que busca os parametros de um grupo empresa, response configurado de acordo com a necessidade do front-end.

	```
    Path: /grupos-empresas/{id}/parametros
    Verbo HTTP: GET
    Método SDK: GrupoEmpresaResource.listaParametrosGrupoEmpresa(...);
    Resumo: Busca parametros de um grupo empresa.
	``` 
	
* REMOVE - Realizado a remoção do path "/grupos-empresas/parametros/{codigo}", recurso inutilizado.

* REMOVE - Realizado a remoção dos métodos que não estavam sendo utilizados na classe PedidoDetalhadoErrosService.

* CHANGE - Alterado o host da Deloitte do ambiente produtivo.

* FEATURE - Incluído recurso para notificar caso exista para o grupo empresa um arquivo que esteja em status recebido.

	```
    Path: /notificacoes/unidades-entrega/processando
    Verbo HTTP: GET
    Método SDK: NotificacoesResource.notificacaoArquivoUnidadeEntregaEmProcessamento(...);
    Resumo: Notificação de arquivos de unidade entrega em processamento.
	``` 
* FEATURE - Incluído listagem de erro para upload de arquivo de cadastro da(s) unidade(s) de entrega.

    ```
    Path: /unidades-entrega/arquivos/{id}/erros
    Verbo HTTP: GET
    Método SDK: UnidadeEntregaResource.listarArquivoUnidadeEntregaDetalheErro(...);
    Resumo: Este recurso permite listar erros no upload de arquivos de unidades de entrega.
	``` 

* CHANGE - Realizada a alteração da uri de conexão com o MongoDB devido a migração de Cosmos para o Atlas.

* CHANGE - Realizada a alteração da configuração do Klov devido a migração do Mongo Cosmos para o Atlas. 

* CHANGE - Modificado as configurações de AppDynamics de Staging e Produção 

## 1.8.0 - (2019-06-06)

* CHANGE - Incluído o identificador do Grupo Empresa em querys de busca da Unidade de Entrega por código.

* FEATURE - Alterado para que o token de alteração de senha seja gerado pela aplicação e não mais solicitado ao Heimdall.

* FEATURE - Incluído upload do arquivo de cadastro da(s) unidade(s) de entrega.

    ```
    Path: /unidades-entrega
    Verbo HTTP: GET
    Método SDK: UnidadeEntregaResource.uploadFile(...);
    Resumo: Este recurso permite fazer upload de unidades de entrega em formato XLSX e XLS.
	``` 
	
* FEATURE - Incluído listagem das unidades e entrega que estão com o status ativo.

    ```
    Path: /unidades-entrega
    Verbo HTTP: GET
    Método SDK: UnidadeEntregaResource.listarUnidadeEntrega(...);
    Resumo: Este recurso permite listar as unidades de entrega do grupo informado que estão com o status ativo.
	``` 

* CHANGE - Incluído validação para que apenas letras e letras com acentuação sejam permitidas nas seguintes células do pedido: Nome, Logradouro, Endereço, Complemento(Caso preenchido) Bairro e Cidade.
	
* CHANGE - Alterada a validacao no upload do Excel, a respeito do preenchimento de campos obrigatórios em primeira linha pós header. 

* FEATURE - Incluído lista de hierarquia de acessos de usuário RH.

	```
    Path: /usuarios/hierarquia-acessos
    Verbo HTTP: GET
    Método SDK: UsuarioResource.listaHierarquiaAcesso(...);
    Resumo: Este recurso permite listar hierarquia de acessos do usuário.
	``` 
* FEATURE - Incluído recurso para atualizar hierarquia de acessos de usuário RH.
	
	```
    Path: /usuarios/{cpf}/hierarquia-acessos
    Verbo HTTP: PUT
    Método SDK: UsuarioResource.atualizarHierarquiaAcesso(...);
    Resumo: Este recurso permite atualizar hierarquia de acessos do usuário RH.
	``` 

## 1.7.0 - (2019-05-16)

* CHANGE - Incluído configuração de criação automática das configurações do RabbitMQ.

* CHANGE - Incluído TimeZone padrão obtida do application.yml na inicialização do Spring Boot

* FEATURE - Incluído recurso para criar hierarquia de acesso(usuário, permissão e nível acesso) para usuários RH.

	```
	Path: /usuarios/hieraquia-acesso/
	Verbo HTTP: POST
	Método SDK: UsuarioResource.criaHierarquiaAcesso(...);
	Resumo: Cria estrutura hierarquica de acessos para usuário.
	
* FEATURE - Incluída a integração junto a Deloitte para consumo das Notas Fiscais.

* FEATURE - Incluído recurso para listar a nota fiscal.

	```
	Path: /financeiros/{codigoNotaFiscal}/nota-fiscal
	Verbo HTTP: GET
	Método SDK: FinanceiroResource.consultarNotaFiscal(...);
	Resumo: Este recurso permite consultar uma nota fiscal a partir de seu identificador.
	```
	
* CHANGE - Incluído comportamento em path /financeiros/pedidos para retornar em cada item da listagem o respectivo link de acesso a RPS ou Nota Fiscal.

## 1.6.2 - (2019-05-07)

* CHANGE - Alterado a configuração do auto startup do RabbitMQ no application para receber seu respectivo valor via docker-compose.

## 1.6.1 - (2019-04-30)

* CHANGE - Alterado o método de alterar senha para sempre chamar o método PUT pier/api/usuarios/{id}/alterar-senha.

## 1.6.0 - (2019-04-18)

* FEATURE - Incluído recurso para listar a hierarquia organizacional de um grupo, ou seja, retorna a árvore hierarquica de grupo/subgrupo(s)/empresa(s).

	```
	Path: /grupos-empresas/{id}/hierarquia-organizacional
	Verbo HTTP: GET
	Método SDK: GrupoEmpresaResource.listarHierarquiaOrganizacionalGrupo(...);
	Resumo: Consultar a hierarquia organizacional do Grupo Empresa.
	```

* CHANGE - Inclusão do cadastro de pessoa no RH para garantir a inserção transacional entre empresa e pessoa.

* CHANGE - Correção de bug que retorna 400 ao atualizar telefone da empresa sem ramal

* CHANGE - Alterado a arquitetura da solução do pedido para funcionar integrado ao RabbitMQ.

* CHANGE - Incluído o retorno do id do pedido no path "/pedidos/upload"

* CHANGE - Incluído ajuste na validação de tipo de faturamento para cancelar pedidos centralizados.

* CHANGE - Alterado o response do path /pedidos/id/cargas-detalhes  para listar as ações que o usuário poderá efetuar em tela, sendo tratado a ação de cancelar.

* FEATURE - Incluído cancelamento de pedidos-detalhes(descentralizado).

    ```
    Path: /pedidos/{idPedido}/cargas-detalhes/{idCarga}/cancelar
    Verbo HTTP: PUT
    Método SDK: PedidoResource.cancelarPedidoDetalhe(...);
    Resumo: Este recurso permite cancelar um pedido descentralizado.
	```

* CHANGE - Ajuste no processo de listagem dos pedidos em financeiro, para trazer apenas os pedidos que estiverem em Processamento concluído e Cancelado parcial (para atender ao cenário de pedidos descentralizados), e incluír os status da carga beneficio(Pedido cancelado,Carga em Processamento) que não deverão constar na somatória do valor total da rps.

* CHANGE - Removido status enum desnecessários nas classes StatusPermissaoEnum e StatusPagamentoEnum.

* CHANGE - Correção de bug que inativa todas as permissões anteriores de um usuário quando uma nova era atribuída a ele.

* CHANGE - Correção de bug que duplicava permissão de acesso para um mesmo usuário e grupo empresas.

* FEATURE - Incluído recurso para alterar o status do cartão.

    ```
    Path: /cartoes/{id}/status
    Verbo HTTP: PUT
    Método SDK: CartaoResource.alterarStatus(...);
    Resumo: Este recurso permite alterar o status de um cartão.
	```

* CHANGE - Incluído os métodos PUT de cadastro de empresa, grupo-empresa e subgrupo-empresa para uso da SF.

* FEATURE - Incluído recurso para alterar o status do cartão.

    ```
    Path: /cartoes/{id}/status
    Verbo HTTP: PUT
    Método SDK: CartaoResource.alterarStatus(...);
    Resumo: Este recurso permite alterar o status de um cartão.
	```
	
* CHANGE - Inclusão dos métodos PUT de cadastro de empresa, grupo-empresa e subgrupo-empresa para uso da SF.

* CHANGE - Incluído o nome do subgrupo no cadastro de Grupo Empresas.

## 1.5.1 - (2019-03-29)

* BUGFIX - Ajuste para pedido pós pago ser lançado como confirmado

* BUGFIX - Tratamento de Concorrência de schedulers em pedidos.

## 1.5.0  - (2019-03-22)

* CHANGE - Incluído no cancelamento de pedidos, validações de permissão e prazo para cancelamento de 2 dias antes do crédito.

* CHANGE - Alterado o response do path /pedidos para listar as ações que o usuário poderá efetuar em tela, sendo tratado a ação de cancelar.

* CHANGE - Alterado a passagem de parâmetro cep para a procedure SPR_Beneficio_IncluirFuncionario, incluído apóstrofo no início e fim da string para que seja levado em consideração os 
possíveis zeros a esquerda. 

* CHANGE - Alterado a validação do logradouro no processo de verificação do conteúdo do pedido realizado via planilha excel, corrigindo bug que estava incluíndo um zero no final do logradouro ao importar planilha excel.

## 1.4.3 - (2019-03-28)

* BUGFIX - Correção do access do token do PIER.

## 1.4.2  - (2019-03-11)

* BUGFIX - Criado StatusPagamentoEnum para status pendente.

## 1.4.1  - (2019-03-11)

* BUGFIX - Criado statusPermissaoEnum para status cancelado.

## 1.4.0  - (2019-03-01)

* BUGFIX - Adaptado task para evitar travamento do processamento de pedidos por linha detalhe inválida.

* FEATURE - Incluído recurso para buscar pedidos com pendência de pagamento na modalidade TED, de acordo com as permissões de um usuário.
	
	```
	Path: /notificacoes/pendencias-ted
	Verbo HTTP: GET
	Método SDK: NotificacaoResource.notificacaoPendeciaTed(...);
	Resumo: Este recurso permite informar os ids de pedidos com pendências de pagamento na modalidade TED, de acordo com as permissões de um usuário.
	```
	
* CHANGE - Correção da listagem do financeiro que não mostrava pedidos originados via SGR no path "/financeiros/pedidos"

* CHANGE - Removido a obrigatoriedade do cnpj na request do POST /usuarios.

* CHANGE - Alteração no processo de upload do arquivo de pedido, validando caso o tipo de pagamento seja pré-pago alterar o status da carga benefício para "Aguardando Pagamento"

* CHANGE - Incluído status pagamento na listagem dos pedidos

* CHANGE - Ajuste na query de (GET /pedidos)lista de pedidos para o filtro de empresas ser dentro da claúsula where.

## 1.3.0  - (2019-02-15)

* CHANGE - Alteração dos paths de permissão a retirada de /rh , a inclusão de /usuarios no path de perfil-acessos e /grupos-empresas no path de /parametros/{codigo}. 

* CHANGE - Incluído parâmetro para buscar listagem de permissão de acesso de usuário com ordenação no path "rh/permissoes/acessos"

## 1.2.2  - (2019-02-15)

* BUGFIX - Implementação de controle por UUID para sanar problema de concorrência nos schedulers de pedido (planilha xls)

* CHANGE - Correção da totalização do valor do pedido no path "/financeiros/pedidos"

* CHANGE - Correção filtro por ids de empresas no path "/financeiros/pedidos"

* CHANGE - Correção da listagem por permissão de acessos no path "/financeiros/pedidos"

* CHANGE - Inclusão de método privado para tratar caracters como tab e espaço.

* CHANGE - Correção de retorno do CPF no path "/cargas/{id}/funcionarios"

* CHANGE - Correção em lógica de método para retornar dias úteis

## 1.2.1  - (2019-02-12)

* CHANGE - Configurações dos ambientes.

## 1.2.0  - (2019-02-01)

* FEATURE - Incluído recurso para criar permissão entre usuários/grupo empresa
	
	```
	Path: /rh/permissoes
	Verbo HTTP: POST
	Método SDK: PermissoesUsuariosRhResource.salvar(...);
	Resumo: Este recurso tem como objetivo definir permissão de usuários com o grupo empresa do rh.
	```

* FEATURE - Incluído recurso para atualizar grupo empresa
	
	```
	Path: /grupos-empresas/{id}
	Verbo HTTP: PATCH
	Método SDK: GrupoEmpresaResource.atualizarGrupoEmpresa(...);
	Resumo: Este recurso tem como objetivo de atualizar o grupo empresa.
	```
	
* FEATURE - Incluído path de atualização de Subgrupo empresa.
	
	```
	Path: /grupos-empresas/{id}/subgrupos-empresas/{idSubgrupo}
	Verbo HTTP: PATCH
	Método SDK: GrupoEmpresaResource.atualizarSubgrupoEmpresa(...);
	Resumo: Este recurso tem como objetivo alterar dados de Sugbrupo de Empresa.
	```
	
* FEATURE - Este recurso permite atualizar uma empresa.

	```
	Path: /subgrupos-empresas/{id}/empresas/{idEmpresa}
	Verbo HTTP: PATCH
	Método SDK: EmpresaResource.atualizarEmpresa(...)
	Resumo: Este recurso permite atualizar uma empresa

	```
* FEATURE - Incluído recurso lista as operações da permissão de acesso de usuário do rh
	
	```
	Path: /rh/permissoes/{id}/operacoes
	Verbo HTTP: GET
	Método SDK: PermissoesOperacoesRhResource.listarPermissaoOpercaoRh(...);
	Resumo: Este recurso tem como objetivo listar as operações de uma permissão do usuário do rh.
	```
	
* FEATURE - Incluído recurso lista nível permissão acesso de usuário do rh
	
	```
	Path: /rh/permissoes/acessos
	Verbo HTTP: GET
	Método SDK: UsuarioNivelPermissaoRhResource.lista(...);
	Resumo: Este recurso tem como objetivo listar  nivel permissão acesso de usuário do rh.
	```

* FEATURE - Incluído recurso para definir nível permissão acessos de usuário rh
	
	```
	Path: /rh/permissoes/{id}/acessos
	Verbo HTTP: PUT
	Método SDK: UsuarioNivelPermissaoRhResource.salvar(...);
	Resumo: Este recurso tem como objetivo definir nivel permissão acesso de usuário do rh.
	```

* FEATURE - Incluído recurso para definir nível permissão acessos de usuário rh
	
	```
	Path: /rh/permissoes/{id}
	Verbo HTTP: PATCH
	Método SDK: UsuarioNivelPermissaoRhResource.salvar(...);
	Resumo: Este recurso tem como objetivo definir nivel permissão acesso de usuário do rh.
	```
	
* FEATURE - Incluído listagem de parametros do grupo empresa

 ```
   Path: /grupos-empresas/{idGrupoEmpresa}/parametros
   Verbo HTTP: GET
   Método SDK: GrupoEmpresaResource.listarParametrosGrupoEmpresa(...)
   Resumo: Este recurso permite listar os parametros de um grupo empresa.
   
   ```

* FEATURE - Incluído listagem de empresas

   ```
   Path: /empresas
   Verbo HTTP: GET
   Método SDK: GrupoEmpresaResource.listarEmpresa(...);
   Resumo: Este recurso permite listar empresa
   ```
   
* FEATURE - Incluído recurso para listar permissão entre usuários/grupo empresaa
	
	```
	Path: /rh/permissoe
	Verbo HTTP: GET
	Método SDK: PermissoesUsuariosRhResource.listar(...);
	Resumo: Este recurso tem como objetivo listar permissões de usuários com o grupo empresa do rh.
	``
* FEATURE - Incluído recurso para listar operações de acesso do usuário.

	``
	Path: /usuarios/operacoes	
	Verbo HTTP: GET
	Método SDK: OperacaoAcessoResource.listarOperacoesAcesso(...);
	Resumo: Este recurso permite listar operações de acesso.
	```
* FEATURE - Incluído recurso para listar empresas de subgrupos.

	``
	Path: /subgrupos-empresas/{id}/empresas	
	Verbo HTTP: GET
	Método SDK: EmpresaResource.listarEmpresasdoSubGrupoEmpresa(...);	
	Resumo: Este recurso permite listar empresas de subgrupos.
	```
* FEATURE - Incluído recurso para listar subgrupos de um grupo-empresa.

	```
	Path: /grupos-empresas/{id}/subgrupos-empresa
	Verbo HTTP: GET	
	Método SDK: GrupoEmpresaResource.listaSubgrupo(...);;	
	Resumo: Este recurso permite listar subgrupos de grupo-empresa.	
	```
* FEATURE - Incluído recurso para criar subgrupo de emrpesas.

	```	
	Path: /api/grupo-empresa/{id}/subgrupos-empresa
	Verbo HTTP: POST
	Método SDK: GrupoEmpresaResource.criaSubGrupo(...);
	Resumo: Este recurso permite cadastrar um novo subgrupo de empresa.
	```
* FEATURE - Validar senha login usuário

   ```
   Path: /usuarios/{login}/senha   
   Verbo HTTP: GET   
   Método SDK: UsuarioResource.validarSenhaLogin(...);   
   Resumo: Este recurso permite validar a senha do login do usuário
   ```

* CHANGE - Incluída a validação de regras de pré e pós pago a respeito da data de crédito no processo pós upload de excel.

## 1.1.0  - (2019-01-18)

* CHANGE - Adicionado o id da carga benefício no request e no response, e adicionado lista de produto(s) no response (PedidoResource.listarDetalhesCarga...) 

* FEATURE - Incluído recurso para listar funcionários do pedido

	```
	Path: /cargas/{id}/funcionarios
	Verbo HTTP: GET
	Método SDK: PedidoResource.listarDetalhesFuncionariosCarga(...);
	Resumo: Este recurso permite listal detalhes da carga benefício por funcionário.
	```

* CHANGE - Correção de falha em paginação do path "/financeiros/pedidos"

## 1.0.0  - (2018-12-07)

* FEATURE - Incluído Recurso para upload de pedido
	
	```
	Path: /pedidos/upload
	Verbo HTTP: POST
	Método SDK: PedidoResource.upload(...);
	Resumo: Este recurso permite fazer upload de pedido em formato XLSX e XLS.
	```
	
* FEATURE - Incluído Recurso para listar pedidos
	
	```
	Path: /pedidos/
	Verbo HTTP: GET
	Método SDK: PedidoResource.listar(...);
	Resumo: Este recurso permite listar pedidos de um determinado grupo empresa.
	```
	
* FEATURE - Scheduler para validação de pedido
    
    ```
    Método SDK: CargaArquivoTasks.jobValidacaoConteudo();
    Resumo: A task tem como objetivo buscar novos registros e registros que foram interrompidos, para que sejam processados e atualizados. Dependendo do seu status, irão prosseguir para o fluxo de validação de negócio.
    ```
    
    ```
    Método SDK: CargaArquivoTasks.jobValidacaoNegocio();
    Resumo: A task tem como objetivo buscar novos registros e registros que foram interrompidos, para que sejam processados as validações de negócio e atualizando os status de carga.
    ```
	
* FEATURE - Incluído Recurso para listar erros de conteúdo do excel de pedido
	
	```
	Path: /pedidos/{idArquivoPedido}/erros
	Verbo HTTP: GET
	Método SDK: PedidoResource.listarDetalhesErros(...);
	Resumo: Este recurso permite listar erros de um pedido de carga.
	``` 
* FEATURE - Incluído Recurso para cancelamento de pedido

	```
	Path: /pedidos/{id}/cancelar
	Verbo HTTP: PUT
	Método SDK: PedidoResource.cancelarPedido(...);
	Resumo: Este recurso permite cancelar um pedido.
	```

* FEATURE -  Incluído recurso de consulta para listar os detalhes paginado do Pedido(Carga Benefício).

	```
	Path: /pedidos/{id}/cargas-detalhes
	Verbo HTTP: GET
	Método SDK: PedidoResource.listarDetalhesCarga(...);
	Resumo: Este recurso permite listar as empresas que estão presentes em um pedido selecionado.
	```
	
* FEATURE - Incluído Recurso para listagem de pedido financeiro
	
	```
	Path: /financeiros/pedidos
	Verbo HTTP: GET
	Método SDK: FinanceiroResource.listarPedidosFinanceiro(...);
	Resumo: Este recurso permite listar os pedidos financeiros.
	```

* FEATURE - Incluído Recurso para consulta da nota RPS
	
	```
	Path: /financeiros/notas-rps{id}
	Verbo HTTP: GET
	Método SDK: FinanceiroResource.consultarNotaRPS(...);
	Resumo: Este recurso permite consultar uma nota RPS informando o identificador da nota RPS.
	```