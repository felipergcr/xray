
package br.com.conductor.rhblueapi.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class CriacaoXlsUtils {

     private static Sheet criarAba(final Workbook workbook, final String nomeAba) {

          return workbook.createSheet(nomeAba);
     }

     private static void inserirDados(final Sheet spreadSheet, final List<List<String>> relatorioGenerico) {

          int numLinha = 0;
          for (List<String> linha : relatorioGenerico) {
               Row row = spreadSheet.createRow(numLinha);
               Cell cellDados;
               for (int i = 0; i < linha.size(); i++) {
                    cellDados = row.createCell(i);
                    cellDados.setCellValue(linha.get(i));
               }
               numLinha++;
          }
     }

     public static Workbook gerarWorkbook(final String nomeAba, final List<List<String>> relatorioGenerico) {

          Workbook workbook = new HSSFWorkbook();

          Sheet spreadSheet = criarAba(workbook, nomeAba);

          inserirDados(spreadSheet, relatorioGenerico);

          return workbook;
     }

     public static byte[] gerarEmByte(final String nomeAba, final List<List<String>> relatorioGenerico) throws IOException {

          Workbook workbook = gerarWorkbook(nomeAba, relatorioGenerico);

          ByteArrayOutputStream bos = new ByteArrayOutputStream();
          workbook.write(bos);

          bos.close();
          workbook.close();

          return bos.toByteArray();
     }
}
