
package br.com.conductor.rhblueapi.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import br.com.twsoftware.alfred.object.Objeto;

public class DataUtils {
     
     public static final DateTimeFormatter FORMATO_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
     
     public static final DateTimeFormatter FORMATO_YYYY_MM_DD_HH_MM_SS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
     
     public static final DateTimeFormatter FORMATO_DD_MM_YYYY_HH_MM_SS = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
     
     public static final DateTimeFormatter FORMATO_DD_MM_YYYY = DateTimeFormatter.ofPattern("dd/MM/yyyy");
     
     private static String converteLocalDateParaString(LocalDate localDate, DateTimeFormatter format) {
          
          return localDate.format(format);
     }
     
     public static LocalDate converteStringParaLocalDate(String data) {
          
          if(Objeto.isBlank(data)) 
               return null;

          return LocalDate.parse(data);
     }

     public static String localDateParaStringyyyyMMdd(LocalDate localDate) {
          
          return converteLocalDateParaString(localDate, FORMATO_YYYY_MM_DD);
     }
     
     public static String localDateToLocalDateTimeFirstMoment(LocalDate localDate) {
          
          return localDate.atTime(LocalTime.MIN).format(FORMATO_YYYY_MM_DD_HH_MM_SS);
     }
     
     public static String localDateToLocalDateTimeLastMoment(LocalDate localDate) {
          
          return localDate.atTime(LocalTime.MAX).format(FORMATO_YYYY_MM_DD_HH_MM_SS);
     }
     
     public static String localDateTimeParaStringyyyyMMddHHmmss(LocalDateTime localDateTime) {
          
          return localDateTime.format(FORMATO_YYYY_MM_DD_HH_MM_SS);
     }

     public static String localDateTimeParaStringddMMyyyyHHmmss(LocalDateTime localDateTime) {

          return localDateTime.format(FORMATO_DD_MM_YYYY_HH_MM_SS);
     }

     public static String localDateParaStringddMMyyyy(LocalDate localDateTime) {

          return localDateTime.format(FORMATO_DD_MM_YYYY);
     }

     public static String localDateTimeToLocalDateTimeFirstMoment(LocalDateTime localDateTime) {
          
          return localDateTime.toLocalDate().atTime(LocalTime.MIN).format(FORMATO_YYYY_MM_DD_HH_MM_SS);
     }

     public static boolean isDataMaiorQueDataCorrente(LocalDate date) {

          return ChronoUnit.DAYS.between(LocalDate.now(), date) > 0 ? true : false;
     }

}
