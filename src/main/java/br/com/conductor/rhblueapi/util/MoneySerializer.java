
package br.com.conductor.rhblueapi.util;

import java.io.IOException;
import java.math.BigDecimal;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class MoneySerializer extends JsonSerializer<BigDecimal> {

     @Override
     public void serialize(BigDecimal value, JsonGenerator jGen, SerializerProvider serializers) throws IOException, JsonProcessingException {

          jGen.writeString(value.setScale(2, BigDecimal.ROUND_DOWN).toString());
     }

}
