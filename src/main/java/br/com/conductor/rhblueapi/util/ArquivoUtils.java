package br.com.conductor.rhblueapi.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.conductor.rhblueapi.enums.TemplatesExcel;
import br.com.conductor.rhblueapi.exception.ConversaoObjetoException;

public class ArquivoUtils {
     
     public static InputStream converteBlobParaInputStream(byte[] byteArray) {
          return new ByteArrayInputStream(byteArray);
     }   
     
     public static Workbook criaWorkbookDe(InputStream blobInputStream) throws ConversaoObjetoException {         
          try {
               return WorkbookFactory.create(blobInputStream);
          } catch (EncryptedDocumentException | IOException e) {
               throw new ConversaoObjetoException("Ocorreu um erro ao converter o objeto para Workbook");
          }
     }

}