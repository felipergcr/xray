package br.com.conductor.rhblueapi.util;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import br.com.conductor.rhblueapi.domain.UnidadeEntrega;
import br.com.conductor.rhblueapi.enums.Documentos;
import br.com.conductor.rhblueapi.enums.Estados;
import br.com.conductor.rhblueapi.enums.Produtos;
import br.com.conductor.rhblueapi.exception.ConteudoExcelException;
import br.com.twsoftware.alfred.cnpj.CNPJ;
import br.com.twsoftware.alfred.cpf.CPF;
import br.com.twsoftware.alfred.object.Objeto;

public class ValidacaoTemplate {
     
     public static String validacaoCpfCnpj(String texto, Documentos documento) throws ConteudoExcelException {
          texto = converteNumeroToString(texto, documento.getDescricao());
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException(String.format("O %s não pode ser vazio ou nulo.", documento.getDescricao()));
          if(texto.length() > documento.getTamanho())
               throw new ConteudoExcelException(String.format("O %s deve possuir o tamanho %s mas possui o tamanho %s.",
                         documento.getDescricao(), documento.getTamanho(), texto.length()));
          else if(texto.length() < documento.getTamanho())
               texto = StringUtils.leftPad(texto, documento.getTamanho(), "0");
          if(!validacaoLogicaCpfCnpj(texto, documento))
               throw new ConteudoExcelException(String.format("O %s é inválido.", documento.getDescricao()));
          return texto;
     }

     public static String validacaoCodigoUnidade(String texto) throws ConteudoExcelException {

          if (StringUtils.isEmpty(texto))
               return null;

          validacaoBasicaUnidadeEntrega(texto);
          return texto;
     }

     public static UnidadeEntrega validacaoCodigoUnidadeLote(String texto, Optional<List<UnidadeEntrega>> optListaUnidadeEntrega) throws ConteudoExcelException {

          validacaoBasicaUnidadeEntrega(texto);

          if (Boolean.FALSE.equals(optListaUnidadeEntrega.isPresent())) {
               throw new ConteudoExcelException("Não existe unidade(s) de entrega cadastrado para esse grupo empresa.");
          }

          UnidadeEntrega unidadeEntregaCadastrada = optListaUnidadeEntrega.get().stream().filter(u -> u.getCodigoUnidadeEntrega().equals(texto)).findFirst().orElse(null);
          if (Objects.isNull(unidadeEntregaCadastrada))
               throw new ConteudoExcelException("O Código do Local de entrega não está cadastrado no sistema.");
          else
               return unidadeEntregaCadastrada;
     }
     
     public static String validacaoMatricula(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               return null;
          texto = converteNumeroToString(texto, "Matrícula");
          validaQtdCaracteres(texto, 12, "matrícula");
          return texto;
     }
     
     private static boolean validacaoLogicaCpfCnpj(String texto, Documentos documento) {
          if(documento.name().equals(Documentos.CPF.name()))
               return CPF.isValido(texto);
          if(documento.name().equals(Documentos.CNPJ.name())) 
               return CNPJ.isValido(texto);
          return false;
     }
     
     public static String validaNomeCompleto(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("O nome não pode ser vazio ou nulo.");
          
          validaLetrasAcentuacao("O", "nome", texto);
          
          if(texto.split(" ").length == 1)
               throw new ConteudoExcelException("O nome deve ser composto de nome e sobrenome.");
          
          validaQtdCaracteres(texto, 100, "nome");
          
          return texto;
     }
     
     public static String validaDataNascimento(String texto) throws ConteudoExcelException {
          LocalDate dataNascimento = validaData(texto, "data de nascimento");
          if(dataNascimento.isAfter(LocalDate.now()))
               throw new ConteudoExcelException("A data de nascimento não pode ser superior a data atual.");
          return converteLocalDateParaStringddMMyyyy(dataNascimento);
     }
     
     public static String validaDataCredito(String texto) throws ConteudoExcelException {
          LocalDate dataCredito = validaData(texto, "data de crédito");
          if(dataCredito.isBefore(LocalDate.now()))
               throw new ConteudoExcelException("A data de crédito não pode ser inferior a data atual.");
          return converteLocalDateParaStringddMMyyyy(dataCredito);
     }
     
     private static LocalDate validaData(String texto, String celula) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("A data não pode ser vazia ou nula.");
          return ValidacaoTemplate.converteTextoParaLocalDate(texto, celula);
     }
     
     public static String validaLogradouro(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("O logradouro não pode ser vazio ou nulo.");
          
          validaLetrasAcentuacao("O", "logradouro", texto);
          
          validaQtdCaracteres(texto, 100, "logradouro");
                    
          return texto;
     }
     
     public static String validaEndereco(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("O endereço não pode ser vazio ou nulo.");
          
          validaLetrasAcentuacaoNumeros("O", "endereço", texto);
          
          validaQtdCaracteres(texto, 100, "endereço");

          return texto;
     }

     public static String validaNumero(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("O número não pode ser vazio ou nulo.");

          Pattern pattern = Pattern.compile("[A-Za-z]");
          Matcher matcher = pattern.matcher(texto);
          
          if(matcher.find())
               throw new ConteudoExcelException("O número não pode conter letras.");

          pattern = Pattern.compile("[^0-9]");
          matcher = pattern.matcher(texto);
          texto = matcher.replaceAll(".");

          if(!texto.matches("[0-9]{1,}.[0-9]{1}")) 
               throw new ConteudoExcelException("O número não pode conter caracteres especiais ou pontuação.");
          
          validaQtdCaracteres(texto, 10, "número");
          
          return StringUtils.getDigits(texto.split("[\\.]")[0]);
     }
     
     public static Integer validaNumero(Integer numero) throws ConteudoExcelException {
          
          if(numero < 1) {
               throw new ConteudoExcelException("O número não pode ser menor que zero.");
          } else if (numero > 99999) {
               throw new ConteudoExcelException("O número não pode ser maior que 99999."); 
          }
          
          return numero;
     }
     
     public static String validaComplemento(String texto) throws ConteudoExcelException {
          
          if(StringUtils.isNotEmpty(texto)) {
               validaLetrasAcentuacaoNumeros("O", "complemento", texto);
          }
          
          validaQtdCaracteres(texto, 30, "complemento");
          
          return texto;
     }
     
     public static String validaBairro(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("O bairro não pode ser vazio ou nulo.");
          
          validaLetrasAcentuacao("O", "bairro", texto);
          
          validaQtdCaracteres(texto, 100, "bairro");
          
          return texto;
     }
     
     public static String validaCidade(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("A cidade não pode ser vazia ou nula.");
          
          validaLetrasAcentuacao("A", "cidade", texto);
          
          validaQtdCaracteres(texto, 100, "cidade");
          
          return texto;
     }
     
     public static String validaUF(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("A UF não pode ser vazia ou nula.");
          if(Objects.isNull(Estados.findByName(texto)))
               throw new ConteudoExcelException(String.format("O estado %s é inexistente.", texto));
          return texto;
     }
     
     private static LocalDate converteTextoParaLocalDate(String texto, String celula) throws ConteudoExcelException {
          texto = StringUtils.leftPad(texto, 10, "0");
          LocalDate data = null;
          try {
               data = LocalDate.parse(texto, DateTimeFormatter.ofPattern("dd-MMM-yyyy").withLocale(Locale.getDefault()));
          }catch(DateTimeException dte) {
               throw new ConteudoExcelException(String.format("A %s é inválida.", celula));
          }
          return data;
     }

     private static String converteLocalDateParaStringddMMyyyy(LocalDate data) {
          return data.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
     }
     
     public static String validaProdutos(String texto) throws ConteudoExcelException {
          texto = converteNumeroToString(texto, "Produto");
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("O produto não pode ser vazio ou nulo.");
          if(texto.length() < 3)
               texto = StringUtils.leftPad(texto, 3, "0");
          if(!texto.matches("[0-9]{3}"))
               throw new ConteudoExcelException("O produto está fora do padrão. Ex: 999.");
          if(Objects.isNull(Produtos.findByCodigo(texto)))
               throw new ConteudoExcelException(String.format("O Produto %s é inexistente.", texto));
          return texto;
     }
     
     public static String validaValorCredito(String texto) throws ConteudoExcelException {
          Double valor = validaMonetario(texto);
          if(valor > 4999.99) 
               throw new ConteudoExcelException("O valor de crédito não pode ultrapassar R$ 4999,99.");
          if(valor < 0.0) 
               throw new ConteudoExcelException("O valor de crédito não pode ser negativo.");
          return texto;
     }
     
     private static Double validaMonetario(String texto) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("O valor não pode ser vazio ou nulo.");
          if(!texto.matches("^(-)[0-9]{1,4}[.][0-9]{1,2}") && !texto.matches("^[0-9]{1,4}[.][0-9]{1,2}"))
               throw new ConteudoExcelException("O valor está fora do padrão esperado. Ex: 3333,33");
          if(Double.parseDouble(texto) == 0.0)
               throw new ConteudoExcelException("O valor não pode ser 0.");
          return Double.valueOf(texto);
     }
    
     public static String validaCEP(String texto) throws ConteudoExcelException {
          texto = converteNumeroToString(texto, "CEP");
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("O CEP não pode ser vazio ou nulo.");
          if(texto.length() < 8)
               texto = StringUtils.leftPad(texto, 8, "0");
          if(!texto.matches("[0-9]{8}"))
               throw new ConteudoExcelException("O CEP está fora do padrão. Ex: 99999999.");
          if(Long.parseLong(texto) == 0L)
               throw new ConteudoExcelException("O CEP não pode ser de valor 0.");
          return texto;
     } 
     
     private static String converteNumeroToString(String texto, String celula) throws ConteudoExcelException {
          if(StringUtils.isEmpty(texto))
               return StringUtils.EMPTY;
          try {
               return converteExponencial(texto, celula);
          } catch (ConteudoExcelException e) {
               if(texto.toUpperCase().matches(".*[A-Z].*"))
                    throw new ConteudoExcelException(String.format("%s não pode conter letras.", celula));
               return removeCaracteres(texto);
          }
     }

     private static String removeCaracteres(String texto) {
          return texto.replaceAll("[^0-9]", "");
     }
     
     private static String converteExponencial(String texto, String celula) throws ConteudoExcelException {
          Double d = null;
          try {
               d = Double.parseDouble(texto);    
          }catch(NumberFormatException nfe) {
               throw new ConteudoExcelException(String.format("%s inválido. Verifique a sua formatação.", celula));
          }
          return String.valueOf(d.longValue());
     }
     
     private static void validaLetrasAcentuacao(String pronome, String celula, String texto) throws ConteudoExcelException {
          if(!texto.matches("[a-zA-Z\u00C0-\u00FF ]+"))
               throw new ConteudoExcelException(String.format("%s %s não pode conter números e/ou caracteres especiais.", pronome, celula));
     }
     
     private static void validaLetrasAcentuacaoNumeros(String pronome, String celula, String texto) throws ConteudoExcelException {
          if(!texto.matches("[a-zA-Z0-9\u00C0-\u00FF ]+"))
               throw new ConteudoExcelException(String.format("%s %s não pode conter caracteres especiais.", pronome, celula));
     }
     
     private static void validaLetrasENumeros(String pronome, String celula, String texto) throws ConteudoExcelException {
          if(!texto.matches("[a-zA-Z0-9]+"))
               throw new ConteudoExcelException(String.format("%s %s não pode conter caracteres especiais ou letras acentuadas.", pronome, celula));
     }
     
     public static String validacaoTelefone(String texto) throws ConteudoExcelException {
          texto = converteNumeroToString(texto, "Telefone");
          
          if(!texto.matches("[1-9]{2}[1-9]{0,1}[1-9]{1}[0-9]{7}"))
               throw new ConteudoExcelException(String.format("O telefone %s é inválido.", texto));
          
          return String.valueOf(texto);
     }
     
     public static String validacaoTipoDocumento(String texto) throws ConteudoExcelException {

          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException("Tipo de Documento não pode ser vazio ou nulo");
          if(!(Documentos.RG.getDescricao().equals(texto) || Documentos.CPF.getDescricao().equals(texto)))
               throw new ConteudoExcelException("Tipo de Documento só pode ser RG ou CPF");
          
          return texto;
     }


     public static String validacaoCpfRg(String texto, String doc) throws ConteudoExcelException {
          if(Objeto.isBlank(doc))
               throw new ConteudoExcelException("O número não pode ser validado , pois o tipo do documento é inválido");
          
          texto = converteNumeroToString(texto, "Documento oficial de registro");
          
          if(StringUtils.isEmpty(texto))
               throw new ConteudoExcelException(String.format("O %s não pode ser vazio ou nulo.",String.valueOf(doc)));
          if(doc.equals(Documentos.CPF.getDescricao()))
               if(texto.length() > Documentos.CPF.getTamanho()) {
                    throw new ConteudoExcelException(String.format("O %s deve possuir o tamanho %s mas possui o tamanho %s.", Documentos.CPF.getDescricao(), Documentos.CPF.getTamanho().toString(), String.valueOf(texto.length())));
               }else if(texto.length() < Documentos.CPF.getTamanho()){
                    texto = StringUtils.leftPad(texto, Documentos.CPF.getTamanho(), "0");
                    if(!validacaoLogicaCpfCnpj(texto, Documentos.CPF))
                         throw new ConteudoExcelException(String.format("O %s digitado %s é inválido.", Documentos.CPF.getDescricao(), texto));
               }

          if(doc.equals(Documentos.RG.getDescricao()) && (texto.length() < 5 || texto.length() > Documentos.RG.getTamanho()))
               throw new ConteudoExcelException(String.format("O %s digitado %s não deve ser menor que o tamanho %s ou maior que %s.", Documentos.RG.getDescricao(), texto ,String.valueOf(5), Documentos.RG.getTamanho()));
          
         return texto;
     }
     
     private static boolean isNumericoDoExcel(String texto) {

          return texto.matches("[0-9]{1,}.[0-9]{1}") ? true : false;
     }
     
     private static boolean isCodigoUnidadeExistente(String codigoUnidade, List<String> codUnidEntregas ) {
          
          return codUnidEntregas.stream().filter(c -> c.equals(codigoUnidade)).findAny().isPresent();
     }
          
     public static String validacaoCodigoUnidadeEntrega(String texto, List<String> codUnidEntregas) throws ConteudoExcelException {
          
          validacaoBasicaUnidadeEntrega(texto);
          
          if (isCodigoUnidadeExistente(texto, codUnidEntregas))
               throw new ConteudoExcelException("Não pode existir mais de uma linha com mesmo código local de entrega.");
          else
               codUnidEntregas.add(texto);

          return texto;
     }

     private static void validacaoBasicaUnidadeEntrega(String unidadeEntrega) throws ConteudoExcelException {

          if (StringUtils.isEmpty(unidadeEntrega))
               throw new ConteudoExcelException("O Código do Local de entrega não pode ser vazio ou nulo");

          if (isNumericoDoExcel(unidadeEntrega)) {
               unidadeEntrega = StringUtils.getDigits(unidadeEntrega.split("[\\.]")[0]);
          } else {
               validaLetrasENumeros("O", "Código do Local de entrega", unidadeEntrega);
          }

          validaQtdCaracteres(unidadeEntrega, 30, "Código do Local de entrega");
     }

     private static void validaQtdCaracteres(String texto, int qtddCaracteres, String campo) throws ConteudoExcelException {
          
          StringBuilder msgErro = new StringBuilder();
          msgErro.append("O campo ");
          msgErro.append(campo);
          msgErro.append(" excedeu ");
          msgErro.append(qtddCaracteres);
          msgErro.append(" caracteres.");
          if(texto.length() > qtddCaracteres)
               throw new ConteudoExcelException(msgErro.toString());
     }
     
}
