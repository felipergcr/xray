
package br.com.conductor.rhblueapi.util.relatorio;

import br.com.conductor.rhblueapi.enums.relatorios.TipoArquivoRelatorio;

public interface RelatorioCriacaoDefinition<T> {

     TipoArquivoRelatorio getTipoArquivo();

     String getNomeAba();

     String getNomeArquivo();

     String getNomeArquivoCompleto();

}
