
package br.com.conductor.rhblueapi.util.relatorio;

public interface RelatorioHeaderDefinition {

     String getHeader();

     int getPosicaoCelula();

}
