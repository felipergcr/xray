
package br.com.conductor.rhblueapi.util;

public interface RabbitMQConstantes {
     
     String XDEAD_LETTER_ROUTING_KEY = "x-dead-letter-routing-key";

     String XDEAD_LETTER_EXCHANGE = "x-dead-letter-exchange";

     interface Cargas {

          String EXCHANGE_IMPORTACAO = "blue.rh.pedido.importacao.exchange";

          String EXCHANGE_GERENCIA_DETALHES = "blue.rh.pedido.gerencia.detalhes.exchange";

          String EXCHANGE_GERENCIA_FUNCIONARIO = "blue.rh.pedido.gerencia.funcionario.exchange";

          String EXCHANGE_GERA_CARGAS = "blue.rh.pedido.gerencia.cargas.exchange";
          
          String EXCHANGE_FINALIZA_PEDIDO = "blue.rh.pedido.finaliza.exchange";
          
          String EXCHANGE_IMPORTACAO_DLX = "blue.rh.pedido.importacao.exchange.dlx";

          String EXCHANGE_GERENCIA_DETALHES_DLX = "blue.rh.pedido.gerencia.detalhes.exchange.dlx";

          String EXCHANGE_GERENCIA_FUNCIONARIO_DLX = "blue.rh.pedido.gerencia.funcionario.exchange.dlx";

          String EXCHANGE_GERA_CARGAS_DLX = "blue.rh.pedido.gerencia.cargas.exchange.dlx";
          
          String EXCHANGE_FINALIZA_PEDIDO_DLX = "blue.rh.pedido.finaliza.exchange.dlx";

          String ROUTING_KEY_IMPORTACAO = "blue.rh.pedido.importacao.rountingkey";

          String ROUTING_KEY_GERENCIA_DETALHES = "blue.rh.pedido.gerencia.detalhes.rountingkey";

          String ROUTING_KEY_GERENCIA_FUNCIONARIO = "blue.rh.pedido.gerencia.funcionario.routingkey";

          String ROUTING_KEY_GERA_CARGAS = "blue.rh.pedido.gerencia.cargas.rountingkey";
          
          String ROUTING_KEY_FINALIZA_PEDIDO = "blue.rh.pedido.finaliza.routingkey";

          String QUEUE_IMPORTACAO = "blue.rh.pedido.importacao.queue";

          String QUEUE_GERENCIA_DETALHES = "blue.rh.pedido.gerencia.detalhes.queue";

          String QUEUE_GERENCIA_FUNCIONARIO = "blue.rh.pedido.gerencia.funcionario.queue";

          String QUEUE_GERA_CARGAS = "blue.rh.pedido.gerencia.cargas.queue";
          
          String QUEUE_FINALIZA_PEDIDO = "blue.rh.pedido.finaliza.queue";
          
          String QUEUE_IMPORTACAO_DLQ = "blue.rh.pedido.importacao.queue.dlq";

          String QUEUE_GERENCIA_DETALHES_DLQ = "blue.rh.pedido.gerencia.detalhes.queue.dlq";

          String QUEUE_GERENCIA_FUNCIONARIO_DLQ = "blue.rh.pedido.gerencia.funcionario.queue.dlq";

          String QUEUE_GERA_CARGAS_DLQ = "blue.rh.pedido.gerencia.cargas.queue.dlq";
          
          String QUEUE_FINALIZA_PEDIDO_DLQ = "blue.rh.pedido.finaliza.queue.dlq";

     }
     
     interface UnidadesEntrega {
          
          String EXCHANGE_IMPORTACAO = "blue.rh.unidade-entrega.importacao.exchange";

          String EXCHANGE_GERENCIA_DETALHES = "blue.rh.unidade-entrega.gerencia.detalhes.exchange";

          String EXCHANGE_GERA_UNIDADES = "blue.rh.unidade-entrega.gerencia.unidades.exchange";

          String EXCHANGE_IMPORTACAO_DLX = "blue.rh.unidade-entrega.importacao.exchange.dlx";

          String EXCHANGE_GERENCIA_DETALHES_DLX = "blue.rh.unidade-entrega.gerencia.detalhes.exchange.dlx";

          String EXCHANGE_GERA_UNIDADES_DLX = "blue.rh.unidade-entrega.gerencia.unidades.exchange.dlx";

          String ROUTING_KEY_IMPORTACAO = "blue.rh.unidade-entrega.importacao.rountingkey";

          String ROUTING_KEY_GERENCIA_DETALHES = "blue.rh.unidade-entrega.gerencia.detalhes.rountingkey";

          String ROUTING_KEY_GERA_UNIDADES = "blue.rh.unidade-entrega.gerencia.unidades.rountingkey";

          String QUEUE_IMPORTACAO = "blue.rh.unidade-entrega.importacao.queue";

          String QUEUE_GERENCIA_DETALHES = "blue.rh.unidade-entrega.gerencia.detalhes.queue";

          String QUEUE_GERA_UNIDADES = "blue.rh.unidade-entrega.gerencia.unidades.queue";

          String QUEUE_IMPORTACAO_DLQ = "blue.rh.unidade-entrega.importacao.queue.dlq";

          String QUEUE_GERENCIA_DETALHES_DLQ = "blue.rh.unidade-entrega.gerencia.detalhes.queue.dlq";

          String QUEUE_GERA_UNIDADES_DLQ = "blue.rh.unidade-entrega.gerencia.unidades.queue.dlq";
          
     }
     
     interface LimiteCredito {
          
          String EXCHANGE_VALIDACAO_DIARIA = "blue.rh.limite-credito.validacao-diaria.exchange";

          String EXCHANGE_VALIDACAO_DIARIA_DLX = "blue.rh.limite-credito.validacao-diaria.exchange.dlx";

          String ROUTING_KEY_VALIDACAO_DIARIA = "blue.rh.limite-credito.validacao-diaria.routingkey";

          String QUEUE_VALIDACAO_DIARIA = "blue.rh.limite-credito.validacao-diaria.queue";

          String QUEUE_VALIDACAO_DIARIA_DLQ = "blue.rh.limite-credito.validacao-diaria.queue.dlq";

     }
     
     interface StatusPagamento {
          
          String EXCHANGE_STATUS_PAGAMENTO = "blue.rh.status.pagamento.exchange";

          String EXCHANGE_STATUS_PAGAMENTO_DLX = "blue.rh.status.pagamento.exchange.dlx";

          String ROUTING_KEY_STATUS_PAGAMENTO = "blue.rh.status.pagamento.routingkey";

          String QUEUE_STATUS_PAGAMENTO = "blue.rh.status.pagamento.queue";

          String QUEUE_STATUS_PAGAMENTO_DLQ = "blue.rh.status.pagamento.queue.dlq";

     }
     
     interface Relatorios {
          
          String EXCHANGE_RELATORIOS_DETALHES_PEDIDOS = "blue.rh.relatorios.detalhes.pedidos";
          
          String EXCHANGE_RELATORIOS_DETALHES_PEDIDOS_DLX = "blue.rh.relatorios.detalhes.pedidos.exchange.dlx";
          
          String ROUTING_KEY_RELATORIOS_DETALHES_PEDIDOS = "blue.rh.relatorios.detalhes.pedidos.routingkey";
          
          String QUEUE_RELATORIOS_DETALHES_PEDIDOS = "blue.rh.relatorios.detalhes.pedidos.queue";
          
          String QUEUE_RELATORIOS_DETALHES_PEDIDOS_DLQ = "blue.rh.relatorios.detalhes.pedidos.queue.dlq";
          
     }

     interface RegistrarBoleto {

          String EXCHANGE_REGISTRAR_BOLETO = "blue.rh.registrar.boleto.exchange";

          String EXCHANGE_REGISTRAR_BOLETO_DLX = "blue.rh.registrar.boleto.exchange.dlx";

          String ROUTING_KEY_REGISTRAR_BOLETO = "blue.rh.registrar.boleto.routingkey";

          String QUEUE_REGISTRAR_BOLETO = "blue.rh.registrar.boleto.queue";

          String QUEUE_REGISTRAR_BOLETO_DLQ = "blue.rh.registrar.boleto.queue.dlq";

     }

}
