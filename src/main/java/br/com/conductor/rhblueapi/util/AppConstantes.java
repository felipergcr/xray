
package br.com.conductor.rhblueapi.util;

public interface AppConstantes {

     String PATH_UPLOAD = "/upload";

     String PATH_LISTA_PEDIDOS = "/pedidos";

     String PATH_FINANCEIROS = "/financeiros";

     String PATH_GRUPO_EMPRESAS = "/grupos-empresas";

     String PATH_SUBGRUPO_EMPRESAS = "/subgrupos-empresas";

     String PATH_PARAMETROS = "/parametros";

     String PATH_PERMISSOES_RH = "/permissoes";

     String PATH_PERMISSOES_OPERACAO = "/operacoes";

     String PATH_USUARIOS = "/usuarios";

     String PATH_USUARIOS_OPERACAO = "/operacoes";

     String PATH_USUARIOS_ACESSO_RH = "/acessos";

     String PATH_EMPRESAS = "/empresas";

     String ACCEPT = "Accept";

     String PIER_NOTIFICACAO_SMS = "/notificacoes/sms";

     String PATH_PERFIS = "/usuarios/perfis-acesso";

     String ACCESS_TOKEN = "access_token";

     String CLIENT_ID = "client_id";

     String CONTENT_TYPE = "Content-Type";

     String OAUTH_CODE = "code";

     String OAUTH_GRANT_TYPE = "grant_type";

     String FRONT_RH = "FRONT-RH";

     String PIER_NOTIFICACAO_EMAIL = "/notificacoes-email";

     String TOKEN_PARAM = "?token=";

     String PIER_PESSOA_FISICAS = "/pessoas";

     String PIER_USUARIO = "/usuarios";

     String LISTA_PEDIDOS = "/pedidos";

     String PATH_UNIDADES_ENTREGA = "/unidades-entrega";

     String CANCELAR_PEDIDOS = "/pedidos/{id}/cancelar";

     String PROCEDURE_CANCELAR_PEDIDO = "SPR_Beneficio_CancelarCarga";

     String LISTA_DETALHES_PEDIDOS = "/pedidos/{id}/cargas-detalhes";

     String LISTA_ERROS_ARQUIVO_PEDIDO = "/{idArquivoPedido}/erros";
     
     String LISTA_ERROS = "/{id}/erros";

     String PARAMETRO_RH = "RH";

     String PIER_ORIGENS_COMERCIAIS = "/origens-comerciais";

     String PIER_CONTA = "/contas";

     String PIER_PRODUTOS = "/produtos";

     String PIER_ENDERECOS = "/enderecos";

     String PIER_TELEFONES = "/telefones";
     
     String PATH_NOTARPS = "/demonstrativo-faturas/{codigo}";
     
     public static final String PATH_RELATORIOS = "/relatorios";
     
     public static final String PATH_DELETAR = "/deletar";

     interface FileExtension {

          String XLSX = "xlsx";

          String XLS = "xls";

          String PDF = "pdf";
     }

     String APPLICATION = "application";

     String ATTACHMENT = "attachment";

     String FUNCIONARIOS = "/funcionarios";

     String CARGAS = "/cargas";

     String PATH_FERIADOS = "/feriados";

     String PARAMETROS_TIPO_RH = "RH";

     Integer CONTADOR_VALOR_ZERO = 0;

     String PATH_NOTIFICACOES = "/notificacoes";

     String PENDENCIAS_TED = "/pendencias-ted";

     String PATH_CARTOES_ALTERAR_STATUS = "/cartoes/{id}/status";

     String CANCELAR_PEDIDOS_DETALHES = "/pedidos/{idPedido}/cargas-detalhes/{idCarga}/cancelar";

     String API_NAME = "rh-blue-api";

     Long EMISSOR_PADRAO = 1l;

     Long ESTABELECIMENTO_PADRAO = 0l;

     String HIERARQUIA_ORGANIZACIONAL = "/hierarquia-organizacional";

     String HIERARQUIA_ACESSOS = "/hierarquia-acessos";

     String NOTA_FISCAL = "/nota-fiscal";
     
     String PATH_ARQUIVOS = "/arquivos";
     
     String PATH_PROCESSAMENTO = "/processando";
     
     boolean ENDERECO_FLAG_BLOQUEADO_PADRAO = false;
     
     String PATH_FUNCIONARIOS = "/funcionarios";

     String PATH_LIMITE_CREDITO = "/limite-credito";
     
     String LIMITE_CREDITO_VALOR_TOTAL_PEDIDOS_SOLICITADOS = "VALOR_TOTAL_PEDIDOS";
     
     String LIMITE_CREDITO_VALOR_TOTAL_PEDIDOS_PAGO = "VALOR_TOTAL_PAGO";

     long MENOR_INTERVALO_CANCELAMENTO_PEDIDO_1_DIA = 1;

     long MAIOR_INTERVALO_CANCELAMENTO_PEDIDO_45_DIAS = -45;

     String QTD_CARGASPEDIDO = "QTD_CARGASPEDIDO";

     String QTD_CARGASPEDIDOPAGAS = "QTD_CARGASPEDIDOPAGAS";

     public static final int ARQUIVO_PEDIDO_QUANTIDADE_ABAS = 1;
     
     public static final int ARQUIVO_UNIDADE_ENTREGA_QUANTIDADE_ABAS = 1;
}
