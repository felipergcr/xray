package br.com.conductor.rhblueapi.util;

public interface RhHeaderDefinition {

     String getHeader();
     boolean isObrigatorio();
     int getPosicaoCelula();

}
