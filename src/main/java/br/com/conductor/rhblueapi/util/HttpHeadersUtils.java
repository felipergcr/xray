
package br.com.conductor.rhblueapi.util;

import static br.com.conductor.rhblueapi.util.AppConstantes.APPLICATION;
import static br.com.conductor.rhblueapi.util.AppConstantes.ATTACHMENT;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class HttpHeadersUtils {

     public static HttpHeaders headersXls(final String nomeArquivo, final int tamanho) {

          HttpHeaders headers = new HttpHeaders();
          headers.setContentType(new MediaType(APPLICATION, AppConstantes.FileExtension.XLS));
          headers.setContentDispositionFormData(ATTACHMENT, nomeArquivo);
          headers.setContentLength(tamanho);
          return headers;
     }

}
