
package br.com.conductor.rhblueapi.repository.rabbitmq.consumer.relatorios;

import java.io.IOException;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.relatorios.response.RelatorioResponse;
import br.com.conductor.rhblueapi.repository.rabbitmq.consumer.RabbitMQConsumer;
import br.com.conductor.rhblueapi.service.relatorios.gerenciador.GerenciadorRelatorioDetalhesPedidos;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class RelatorioDetalhesPedidosConsumer extends RabbitMQConsumer {

     @Autowired
     private GerenciadorRelatorioDetalhesPedidos gerenciadorRelatorioDetalhesPedidos;

     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.Relatorios.QUEUE_RELATORIOS_DETALHES_PEDIDOS, concurrency = "${app.jms.concurrency.relatorios.detalhes.pedidos}")
     public void consumerRelatorioDetalhesPedidos(Message data) throws IOException {

          try {

               String response = new String(data.getBody());

               RelatorioResponse relatorioResponse = jsonUtil.parseAs(response, RelatorioResponse.class);

               gerenciadorRelatorioDetalhesPedidos.verificaSolicitacaoRelatorio(relatorioResponse);

          } catch (Exception ex) {
               throw ex;
          }
     }
}
