
package br.com.conductor.rhblueapi.repository.relatorios;

import java.util.List;

import br.com.conductor.rhblueapi.domain.relatorios.response.RelatorioResponse;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioFilter;

public interface RelatorioCustomRepository {
     
     Integer countAllWithParameters(RelatorioFilter relatorioFilter);
     
     List<RelatorioResponse> findAllWithParameters(RelatorioFilter relatorioFilter);
     
}
