
package br.com.conductor.rhblueapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaDetalheErro;

public interface ArquivoUnidadeEntregaDetalheErroRepository extends JpaRepository<ArquivoUnidadeEntregaDetalheErro, Long> {

}
