
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import br.com.conductor.rhblueapi.domain.NotaFinanceiroCustom;

public interface FinanceiroCargaControleFinanceiroCustomRepository extends RepositoryCustom {
     
     List<NotaFinanceiroCustom> buscaDadosFinanceiroPor(Long idCargaControleFinanceiro, Long idGrupoEmpresa, List<Integer> idsEmpresas);

}
