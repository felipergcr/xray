package br.com.conductor.rhblueapi.repository;

import br.com.conductor.rhblueapi.domain.HistoricoCancelamentoCartao;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * HistoricoCancelamentoCartaoRepository
 */
public interface HistoricoCancelamentoCartaoRepository extends JpaRepository<HistoricoCancelamentoCartao, Long> {

}
