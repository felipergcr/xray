
package br.com.conductor.rhblueapi.repository;

import org.springframework.data.domain.Pageable;

import br.com.conductor.rhblueapi.domain.response.ArquivoUnidadeEntregaDetalheErroReponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;

public interface ArquivoUnidadeEntregaDetalheErroCustomRepository {

     public PageResponse<ArquivoUnidadeEntregaDetalheErroReponse> findByIdArquivoUnidadeEntregaWithPageable(Long idArquivoUnidadeEntrega, Pageable pageable);

}
