
package br.com.conductor.rhblueapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.SubgrupoEmpresa;

@Repository
public interface SubgrupoRepository extends JpaRepository<SubgrupoEmpresa, Long> {
     
     final String QUERY_SUBGRUPOS_PERMISSAO_USUARIO = " from GRUPOSEMPRESAS GP (NOLOCK), USUARIOSNIVEISPERMISSOESRH UNP (NOLOCK) "
               + " INNER JOIN PERMISSOESUSUARIOSRH PU (NOLOCK) ON UNP.ID_PERMISSAO = PU. ID_PERMISSAO "
               + " WHERE GP.Id_GrupoEmpresaPai = :idGrupoEmpresa "
               + " AND (GP.NOME LIKE CONCAT('%',:nome,'%') OR :nome is null)"
               + " AND (UNP.STATUS = :status OR :status is null) "
               + " AND (PU.id_usuario = :idUsuario OR :idUsuario is null) "
               + " AND (UNP.ID_GRUPOEMPRESA = GP.Id_GrupoEmpresaPai OR UNP.ID_SUBGRUPOEMPRESA = GP.Id_GrupoEmpresa) ";
     
     Page<SubgrupoEmpresa> findByIdGrupoEmpresaPai(Long idGrupoEmpresaPai, Pageable pageable);
     
     @Query(value = "SELECT DISTINCT GP.* " + QUERY_SUBGRUPOS_PERMISSAO_USUARIO, 
               countQuery = "SELECT DISTINCT count(*) " + QUERY_SUBGRUPOS_PERMISSAO_USUARIO, 
               nativeQuery = true)
     Page<SubgrupoEmpresa> findByIdUsuarioAndIdGrupoEmpresaAndNomeAndStatus(
               @Param("idUsuario") final Integer idUsuario, @Param("idGrupoEmpresa") final Long idGrupoEmpresa,
               @Param("nome") final String nomeSubgrupo, @Param("status") final Integer status, Pageable pageable);


     
}
