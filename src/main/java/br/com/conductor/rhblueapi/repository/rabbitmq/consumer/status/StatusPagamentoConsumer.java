
package br.com.conductor.rhblueapi.repository.rabbitmq.consumer.status;

import java.io.IOException;
import java.util.Objects;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.status.DadosAtualizacaoStatusPagamento;
import br.com.conductor.rhblueapi.enums.StatusPagamentoEnum;
import br.com.conductor.rhblueapi.repository.rabbitmq.consumer.RabbitMQConsumer;
import br.com.conductor.rhblueapi.service.PedidoService;
import br.com.conductor.rhblueapi.service.status.StatusService;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class StatusPagamentoConsumer extends RabbitMQConsumer {
     
     @Autowired
     private StatusService statusService;
     
     @Autowired
     private PedidoService pedidoService;
     
     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.StatusPagamento.QUEUE_STATUS_PAGAMENTO, concurrency = "${app.jms.concurrency.status.pagamento}")
     public void consumerStatusPagamento(Message data) throws IOException {

          try {
               
               String response = new String(data.getBody());
               
               DadosAtualizacaoStatusPagamento dadoAtualizacaoStatusPagamento = jsonUtil.parseAs(response, DadosAtualizacaoStatusPagamento.class);
               
               StatusPagamentoEnum statusPagamento = statusService.gerenciadorStatusPagamento(dadoAtualizacaoStatusPagamento);
               
               if(Objects.nonNull(statusPagamento))
                    pedidoService.atualizaStatusPagamento(dadoAtualizacaoStatusPagamento.getNumeroPedido(), statusPagamento);

          } catch (Exception ex) {
               throw ex;
          }

     }

}
