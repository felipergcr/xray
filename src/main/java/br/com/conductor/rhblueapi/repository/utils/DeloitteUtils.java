
package br.com.conductor.rhblueapi.repository.utils;

import static java.util.Collections.singletonList;
import static org.springframework.http.CacheControl.noCache;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.deloitte.IdentificacaoDeloitte;

@Component
public class DeloitteUtils {

     @Value("${app.deloitte.host}")
     private String host;

     @Value("${app.deloitte.path.autentication}")
     private String pathAutentication;
     
     @Value("${app.deloitte.path.notafiscal}")
     private String pathDownloadNotaFiscal;

     @Value("${app.deloitte.user}")
     private String user;

     @Value("${app.deloitte.accessKey}")
     private String accessKey;

     public HttpHeaders incializaHeadersAutenticacao() {

          final HttpHeaders headers = new HttpHeaders();
          headers.setAccept(singletonList(APPLICATION_JSON_UTF8));
          headers.setContentType(APPLICATION_JSON_UTF8);
          headers.setCacheControl(noCache());
          
          return headers;
     }
     
     public HttpHeaders incializaHeadersBuscaNotaFiscal(String accessToken) {

          final HttpHeaders headers = new HttpHeaders();
          headers.setAccept(singletonList(APPLICATION_JSON_UTF8));
          headers.setContentType(APPLICATION_JSON_UTF8);
          headers.setCacheControl(noCache());
          headers.set("authorization", "Bearer "+accessToken);
          
          return headers;
     }

     private StringBuilder inicializaUri() {

          final StringBuilder sb = new StringBuilder();
          sb.append(host);
          return sb;

     }
     
     public IdentificacaoDeloitte identificacaoDeloitte() {
          
          return IdentificacaoDeloitte.builder().userId(user).accessKey(accessKey).build();          
     }
     
     public String uriAutentication() {

          return this.inicializaUri().append(pathAutentication).toString();
     }
     
     public String uriDownloadNotaFiscal() {

          return this.inicializaUri().append(pathDownloadNotaFiscal).toString();
     }

}
