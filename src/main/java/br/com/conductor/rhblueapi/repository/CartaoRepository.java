package br.com.conductor.rhblueapi.repository;

import br.com.conductor.rhblueapi.domain.Cartoes;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * CartaoRepository
 */
public interface CartaoRepository extends JpaRepository<Cartoes, Long> {

}
