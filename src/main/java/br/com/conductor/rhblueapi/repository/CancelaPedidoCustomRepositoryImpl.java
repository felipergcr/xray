
package br.com.conductor.rhblueapi.repository;

import static br.com.conductor.rhblueapi.util.AppConstantes.PROCEDURE_CANCELAR_PEDIDO;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;

@Repository
public class CancelaPedidoCustomRepositoryImpl implements CancelaPedidoCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;

     public void cancelaPedidoProc(Long idPedido) {

          try {

               StoredProcedureQuery stored = entityManager.createStoredProcedureQuery(PROCEDURE_CANCELAR_PEDIDO);

               stored.registerStoredProcedureParameter("Id_ArquivoCarga", Integer.class, ParameterMode.IN);

               stored.setParameter("Id_ArquivoCarga", idPedido.intValue());
               
               stored.registerStoredProcedureParameter("Id_CargaBeneficio", Integer.class, ParameterMode.IN);
               
               stored.setParameter("Id_CargaBeneficio", null);

               stored.execute();

          } catch (InternalError e) {

               ExceptionsMessagesCdtEnum.FALHA_AO_EXECUTAR_PROC_DE_CANCELAMENTO_DE_PEDIDO.raise();
          }
     }

     public void cancelaPedidoDetalhesProc(Long idCarga) {

          try {

               StoredProcedureQuery stored = entityManager.createStoredProcedureQuery(PROCEDURE_CANCELAR_PEDIDO);

               stored.registerStoredProcedureParameter("Id_ArquivoCarga", Integer.class, ParameterMode.IN);

               stored.setParameter("Id_ArquivoCarga", null);

               stored.registerStoredProcedureParameter("Id_CargaBeneficio", Integer.class, ParameterMode.IN);

               stored.setParameter("Id_CargaBeneficio", idCarga.intValue());

               stored.execute();

          } catch (InternalError e) {

               ExceptionsMessagesCdtEnum.FALHA_AO_EXECUTAR_PROC_DE_CANCELAMENTO_DE_PEDIDO.raise();
          }
     }

}
