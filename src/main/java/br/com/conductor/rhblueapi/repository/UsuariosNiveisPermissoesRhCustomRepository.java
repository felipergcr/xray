package br.com.conductor.rhblueapi.repository;

import java.util.List;

import br.com.conductor.rhblueapi.domain.UsuariosNiveisPermissoesRh;

public interface UsuariosNiveisPermissoesRhCustomRepository extends RepositoryCustom {
     
     List<UsuariosNiveisPermissoesRh> findByIdUsuarioAndIdGrupoEmpresaPaiAndOperacao(Long idUsuario, Long idGrupoEmpresaPai, String nomeOperacao);
     
}