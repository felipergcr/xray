
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacionalCustom;
import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacionalGrupoCustom;

@Repository
public class HierarquiaOrganizacionalCustomRepositoryImpl implements HierarquiaOrganizacionalCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;

     @Override
     public List<HierarquiaOrganizacionalCustom> buscaHierarquiaOrganizacional(Long idGrupoEmpresa, Long idSubgrupoEmpresa, Long idEmpresa) {

          StringBuilder sql = new StringBuilder();
          Query query;

          sql.append("SELECT ")
               .append("ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, ")
               .append("GRUPOEMPRESA.Id_GrupoEmpresa AS ID_GRUPOEMPRESA, ")
               .append("SUBGRUPOEMPRESA.Id_GrupoEmpresa AS ID_SUBGRUPOEMPRESA, ")
               .append("EMPRESA.Id_Empresa AS ID_EMPRESA ")
               .append("FROM ")
               .append("GruposEmpresas GRUPOEMPRESA ")
               .append("LEFT JOIN GruposEmpresas SUBGRUPOEMPRESA ON (SUBGRUPOEMPRESA.Id_GrupoEmpresaPai = GRUPOEMPRESA.Id_GrupoEmpresa) ")
               .append("LEFT JOIN empresas EMPRESA ON (EMPRESA.Id_GrupoEmpresa = SUBGRUPOEMPRESA.Id_GrupoEmpresa) ")
               .append("WHERE ")
               .append("((:idGrupoEmpresa IS NULL) OR (GRUPOEMPRESA.Id_GrupoEmpresa = :idGrupoEmpresa)) ")
               .append("AND ((:idSubgrupoEmpresa  IS NULL) OR (SUBGRUPOEMPRESA.Id_GrupoEmpresa = :idSubgrupoEmpresa )) ")
               .append("AND ((:idEmpresa IS NULL) OR (EMPRESA.Id_Empresa = :idEmpresa )) ");

          query = (Query) entityManager.createNativeQuery(sql.toString(), HierarquiaOrganizacionalCustom.class);

          query.setParameter("idGrupoEmpresa", idGrupoEmpresa)
               .setParameter("idSubgrupoEmpresa", idSubgrupoEmpresa)
               .setParameter("idEmpresa", idEmpresa);

          @SuppressWarnings("unchecked")
          List<HierarquiaOrganizacionalCustom> retorno = (List<HierarquiaOrganizacionalCustom>) query.getResultList();

          return retorno;
     }

     @Override
     public List<HierarquiaOrganizacionalGrupoCustom> listarHierarquiaOrganizacionalGrupoCustom(final List<Integer> idsEmpresas) {

          StringBuilder sql = new StringBuilder();
          Query query;

          sql.append("select ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS id, ")
             .append("       g.Id_GrupoEmpresa as Id_GrupoEmpresa, ")
             .append("       g.Nome as Nome_GrupoEmpresa, ")
             .append("       sg.Id_GrupoEmpresa as Id_SubGrupoEmpresa, ")
             .append("       sg.Nome as Nome_SubGrupoEmpresa, ")
             .append("       e.Id_Empresa, ")
             .append("       e.NomeExibicao as Nome_Empresa, ")
             .append("       p.CPF as Cnpj ")
             .append("from Empresas e ")
             .append("inner join Pessoas p on e.Id_Pessoa = p.Id_PessoaFisica ")
             .append("inner join GruposEmpresas sg on sg.Id_GrupoEmpresa = e.Id_GrupoEmpresa and sg.Id_GrupoEmpresaPai is not null ")
             .append("inner join GruposEmpresas g on g.Id_GrupoEmpresa = sg.Id_GrupoEmpresaPai and g.Id_GrupoEmpresaPai is null ")
             .append("where e.Id_Empresa in :idsEmpresas");

          query = (Query) entityManager.createNativeQuery(sql.toString(), HierarquiaOrganizacionalGrupoCustom.class);

          query.setParameter("idsEmpresas", idsEmpresas);

          @SuppressWarnings("unchecked")
          List<HierarquiaOrganizacionalGrupoCustom> retorno = (List<HierarquiaOrganizacionalGrupoCustom>) query.getResultList();

          return retorno;
     }

}
