
package br.com.conductor.rhblueapi.repository.rabbitmq.publish.status;

import java.util.UUID;

import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.status.DadosAtualizacaoStatusPagamento;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.RabbitMQPublish;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class StatusPagamentoPublish extends RabbitMQPublish  {
     
     public void enviarCargasPedidosParaAtualizacaoStatus(DadosAtualizacaoStatusPagamento dadosAtualizacaoStatusPagamento) {
          
          String payload = jsonUtil.toJsonString(dadosAtualizacaoStatusPagamento);
          
          send(RabbitMQConstantes.StatusPagamento.EXCHANGE_STATUS_PAGAMENTO, RabbitMQConstantes.StatusPagamento.ROUTING_KEY_STATUS_PAGAMENTO, payload, UUID.randomUUID().toString());
          
     }

}
