package br.com.conductor.rhblueapi.repository;

import java.util.List;

import br.com.conductor.rhblueapi.domain.ArquivoCargaFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.DadosPedidoFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.PedidoFinanceiroCustom;

public interface FinanceiroPedidosCustomRepository extends RepositoryCustom {
     
     Integer countFinanceiroPedidosPorPermissoes(Long idGrupoEmpresa, List<Integer> idsEmpresas,List<Integer> statusArquivoParaVisualizarRPS);
     
     List<DadosPedidoFinanceiroCustom> buscarDadosPedidoFinanceiro(List<Long> idsArquivoCarga,List<Integer> statusCargaBeneficioParaNaoVisualizarRPS);
     
     List<ArquivoCargaFinanceiroCustom> buscarPedidosPorPermissoesPaginado(Long idGrupoEmpresa, List<Integer> idsEmpresas,List<Integer> statusArquivoParaVisualizarRPS, Integer page, Integer size);
     
     List<PedidoFinanceiroCustom> buscarFinanceiroPorPedidosEPermissoes(Long idGrupoEmpresa, List<Integer> idsEmpresas, List<Long> idsArquivoCarga,List<Integer> statusArquivoParaVisualizarRPS);
     
}