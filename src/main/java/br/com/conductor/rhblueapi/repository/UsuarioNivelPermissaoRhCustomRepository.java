
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRhCustom;
import br.com.conductor.rhblueapi.domain.request.UsuarioNivelPermissaoRhRequest;

public interface UsuarioNivelPermissaoRhCustomRepository extends RepositoryCustom {

     public List<UsuarioPermissaoNivelAcessoRhCustom> buscarUsuarioPorNiveisPermissoes(UsuarioNivelPermissaoRhRequest request, StatusDescricao status, Integer page, Integer size);

     public Integer countUsuariosPorPermissoes(UsuarioNivelPermissaoRhRequest request, StatusDescricao status);

}
