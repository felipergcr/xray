
package br.com.conductor.rhblueapi.repository;

import java.util.List;

public interface NotificacaoPendenciaTedCustomRepository {

     List<Integer> buscaPendenciasTedCentralizado(Long idGrupoEmpresa);

     List<Integer> buscaPendenciasTedDescentralizado(List<Integer> idsEmpresas);

}
