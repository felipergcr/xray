
package br.com.conductor.rhblueapi.repository.rabbitmq.consumer;

import java.io.IOException;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaDetalhe;
import br.com.conductor.rhblueapi.service.usecase.GerenciadorUnidadeEntrega;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class UnidadesEntregaConsumer extends RabbitMQConsumer {

     @Autowired
     private GerenciadorUnidadeEntrega gerenciadorArquivoUnidadeEntrega;

     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.UnidadesEntrega.QUEUE_IMPORTACAO, concurrency = "${app.jms.concurrency.unidade-entrega.importacao}")
     public void consumerImportaUnidadeEntrega(Message data) throws IOException {

          try {

               String idArquivoUnidadeEntrega = new String(data.getBody());

               gerenciadorArquivoUnidadeEntrega.processoImportacaoUnidadeEntrega(Long.parseLong(idArquivoUnidadeEntrega));

          } catch (Exception ex) {
               throw ex;
          }

     }
     
     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.UnidadesEntrega.QUEUE_GERENCIA_DETALHES, concurrency = "${app.jms.concurrency.unidade-entrega.gerencia.detalhes}")
     public void consumerGerenciarDetalhes(Message data) throws IOException {

          try {

               String idPedido = new String(data.getBody());

               gerenciadorArquivoUnidadeEntrega.gerenciaEDistribuiDetalhesArquivo(Long.parseLong(idPedido));

          } catch (Exception ex) {
               throw ex;
          }

     }
     
     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.UnidadesEntrega.QUEUE_GERA_UNIDADES, concurrency = "${app.jms.concurrency.unidade-entrega.gerencia.unidades}")
     public void consumerGeraCargas(Message data) throws IOException {

          try {

               String response = new String(data.getBody());

               ArquivoUnidadeEntregaDetalhe mensagem = jsonUtil.parseAs(response, ArquivoUnidadeEntregaDetalhe.class);

               gerenciadorArquivoUnidadeEntrega.processoPersistenciaUnidades(mensagem);

          } catch (Exception ex) {
               throw ex;
          }

     }

}
