
package br.com.conductor.rhblueapi.repository.rabbitmq.publish;

import static br.com.conductor.rhblueapi.util.AppConstantes.API_NAME;

import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.util.JsonUtil;

@Component
public abstract class RabbitMQPublish {

     private static final String CONTENT_ENCODING = "UTF-8";

     private static final String CONTENT_TYPE = "application/json";

     @Autowired
     private RabbitTemplate rabbitTemplate;
     
     @Autowired
     protected JsonUtil jsonUtil;
     
     protected void send(final String exchange, final String routingKey, final String payload, final String messageId) {

          this.rabbitTemplate.convertAndSend(exchange, routingKey, payload, m -> {
               m.getMessageProperties().setCorrelationId(messageId);
               m.getMessageProperties().setMessageId(messageId);
               m.getMessageProperties().setContentType(CONTENT_TYPE);
               m.getMessageProperties().setContentEncoding(CONTENT_ENCODING);
               m.getMessageProperties().getHeaders().put("ORIGIN", API_NAME);
               m.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
               return m;
          });

     }

}
