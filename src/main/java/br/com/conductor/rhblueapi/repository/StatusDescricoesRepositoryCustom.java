
package br.com.conductor.rhblueapi.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.Status;
import br.com.conductor.rhblueapi.domain.StatusDescricoes;

@Repository
public class StatusDescricoesRepositoryCustom {

     @Autowired
     private StatusRepository statusRepository;

     @Autowired
     private StatusDescricoesRepository statusDescricoesRepository;

     private final HashMap<String, List<StatusDescricoes>> mapStatusDescricoes = new HashMap<String, List<StatusDescricoes>>();

     public List<StatusDescricoes> findStatusDescByNomeStatus(String nomeStatus) throws Exception {

          if (mapStatusDescricoes.get(nomeStatus) == null) {
               atualizaHashMapStatus();
               if (mapStatusDescricoes.get(nomeStatus) == null) {
                    throw new Exception("Status não encontrado");
               }
          }
          return mapStatusDescricoes.get(nomeStatus);
     }
     
     public StatusDescricoes findStatusDescByNomeStatusAndDesc(String nomeStatus, String descricao) throws Exception {
          List<StatusDescricoes> lista = findStatusDescByNomeStatus(nomeStatus);
          StatusDescricoes stsDesc = lista.stream().filter(x -> x.getDescricao().equals(descricao)).collect(Collectors.toList()).get(0);
          return stsDesc;
     }

     private void atualizaHashMapStatus() throws Exception {

          mapStatusDescricoes.clear();

          List<Status> status = statusRepository.findAll();
          List<StatusDescricoes> statusDescricoes = statusDescricoesRepository.findAll();

          if (status.size() == 0 || statusDescricoes.size() == 0) {
               throw new Exception("Status não encontrado.");
          }

          for (Status sts : status) {
               List<StatusDescricoes> stsDesc = new ArrayList<StatusDescricoes>();
               stsDesc = statusDescricoes.stream().filter(x -> x.getCodigoStatus().equals(sts.getCodigoStatus())).collect(Collectors.toList());
               if (!stsDesc.isEmpty()) {
                    mapStatusDescricoes.put(sts.getDescricao(), stsDesc);
               }
          }

     }

     public Long obterIdStatus(String nomeStatus, String descricao) throws Exception {
          StatusDescricoes status = findStatusDescByNomeStatusAndDesc(nomeStatus, descricao);
          return status.getStatus();
     }


}
