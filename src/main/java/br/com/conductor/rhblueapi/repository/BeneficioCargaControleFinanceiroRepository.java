
package br.com.conductor.rhblueapi.repository;

public interface BeneficioCargaControleFinanceiroRepository {

     void atualizaControleFinanceiro(Long idArquivoCarga);
}
