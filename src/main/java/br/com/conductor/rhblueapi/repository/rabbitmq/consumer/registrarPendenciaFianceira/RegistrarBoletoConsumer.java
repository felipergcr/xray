
package br.com.conductor.rhblueapi.repository.rabbitmq.consumer.registrarPendenciaFianceira;

import java.io.IOException;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.repository.rabbitmq.consumer.RabbitMQConsumer;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class RegistrarBoletoConsumer extends RabbitMQConsumer {

     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.RegistrarBoleto.QUEUE_REGISTRAR_BOLETO, concurrency = "${app.jms.concurrency.registrar-boleto.registrar-boleto}")
     public void consumerRegistrarBoleto(Message data) throws IOException {

          try {

               // String response = new String(data.getBody());
               //
               // List<CargaPedidoCustom> cargasPedidos = jsonUtil.parseAsListOf(response, CargaPedidoCustom.class);
               //
               // gerenciadorCarga.gerenciaRegrasLimiteDiario(cargasPedidos);

          } catch (Exception ex) {
               throw ex;
          }

     }

}
