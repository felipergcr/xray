
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.EmpresasCargasDetalhes;

public interface EmpresasCargasDetalhesRepository extends JpaRepository<EmpresasCargasDetalhes, Long> {

     EmpresasCargasDetalhes findByIdFuncionarioAndIdEmpresaCarga(Long idFuncionario, Long idEmpresaCarga);

     List<EmpresasCargasDetalhes> findByIdEmpresaCargaIn(List<Long> idEmpresaCarga);
     
}
