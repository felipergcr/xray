
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;

@Repository
public class ParametrosGruposEmpresasCustomRepositoryImpl implements ParametrosGruposEmpresasCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;
     
     @Override
     public List<ParametroGrupoEmpresa> findByIdGrupoEmpresaAndTipoParametroAndCodigo(Long idGrupoEmpresa, String tipoParametro, String codigo) {
          
          Query query;
          StringBuilder sql = new StringBuilder();
          
          sql.append("select * from parametrosgruposempresas ")
          .append("where id_grupoEmpresa = :idGrupoEmpresa ")
          .append("and Id_Parametro = ( ")
          .append("select id_parametro from parametros ")
          .append("where tipoparametro = :tipoParametro and codigo = :codigo)");
          
          query = entityManager.createNativeQuery(sql.toString(), ParametroGrupoEmpresa.class);
          query.setParameter("idGrupoEmpresa", idGrupoEmpresa);
          query.setParameter("tipoParametro", tipoParametro);
          query.setParameter("codigo", codigo);
          
          @SuppressWarnings("unchecked")
          List<ParametroGrupoEmpresa> response = (List<ParametroGrupoEmpresa>) query.getResultList();
          
          return response;
     }

}
