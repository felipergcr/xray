package br.com.conductor.rhblueapi.repository.utils;

import org.modelmapper.PropertyMap;

import br.com.conductor.rhblueapi.domain.PermissaoOperacaoRh;
import br.com.conductor.rhblueapi.domain.response.PermissaoOperacaoRhResponse;

public class PermissaoOperacaoRhMap {


  public final Response response = new Response();

  public class Response extends PropertyMap<PermissaoOperacaoRh, PermissaoOperacaoRhResponse> {

    @Override
    protected void configure() {
      map().setPermissaoAtiva(source.getStatus() == 0 ? false : true);
    }

  }

}
