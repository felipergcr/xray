
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.StatusDescricao;

@Repository
public class StatusDescricaoCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;

     private final String NATIVE_QUERY = "SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID,s.CodigoStatus,s.Descricao as TIPOSTATUS,sd.Status,sd.Descricao "
               + "FROM StatusDescricoes sd inner join [Status] s on s.CodigoStatus = sd.CodigoStatus "
               + "WHERE s.CodigoStatus > 99 "
               + "ORDER BY s.CodigoStatus, s.Descricao,ID";

     @Cacheable("ALL_STATUS_DESCRICAO")
     @SuppressWarnings("unchecked")
     public Map<String, List<StatusDescricao>> findAll() {
          final Query nativeQuery = entityManager.createNativeQuery(NATIVE_QUERY, StatusDescricao.class);

          final List<StatusDescricao> descricoes = nativeQuery.getResultList();

          final Map<String, List<StatusDescricao>> tipoStatusAgrupado = descricoes.stream().collect(Collectors.groupingBy(s -> s.getTipoStatus().toUpperCase()));

          return tipoStatusAgrupado;
     }

}
