
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;

@Repository
public interface ParametroGrupoEmpresaRepository extends JpaRepository<ParametroGrupoEmpresa, Long> {

     Optional<ParametroGrupoEmpresa> findByIdGrupoEmpresaAndParametro_IdParametro(Long id, Long idParametro);
     
     Page<ParametroGrupoEmpresa> findByIdGrupoEmpresa(Long idGrupoEmpresa, Pageable pageable);
     
     Optional<List<ParametroGrupoEmpresa>> findByIdGrupoEmpresa(Long idGrupoEmpresa);

     @Modifying
     void deleteByIdGrupoEmpresa(Long idGrupo);
}
