
package br.com.conductor.rhblueapi.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.conductor.rhblueapi.domain.UserToken;

public interface UserTokenRepository extends MongoRepository<UserToken, String> {

     UserToken findByToken(String id);

     UserToken findTop1ByUserId(Long id);

}
