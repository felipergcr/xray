package br.com.conductor.rhblueapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.OperacaoAcesso;

public interface OperacaoAcessoRepository extends JpaRepository<OperacaoAcesso, Long> {

     Optional<OperacaoAcesso> findByIdAndIdPlataforma(Long id, Long idPlataforma);


}
