
package br.com.conductor.rhblueapi.repository.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.data.domain.Page;

import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.twsoftware.alfred.object.Objeto;

public class Converters {

     @SuppressWarnings({ "unchecked", "rawtypes" })
     public static PageResponse buildPageResponse(Page p) {

          if (p == null) {

               return null;
          } else {

               PageResponse pageResponse = new PageResponse();
               pageResponse.setContent(p.getContent());
               pageResponse.setHasContent(p.hasContent());
               pageResponse.setNumber(p.getNumber());
               pageResponse.setNumberOfElements(p.getNumberOfElements());
               pageResponse.setSize(p.getSize());
               pageResponse.setTotalElements(p.getTotalElements());
               pageResponse.setTotalPages(p.getTotalPages());
               pageResponse.setFirst(p.isFirst());
               pageResponse.setLast(p.isLast());

               if (p.previousPageable().isPaged()) {
                    pageResponse.setPreviousPage(p.previousPageable().getPageNumber());
               }
               if (p.nextPageable().isPaged()) {
                    pageResponse.setNextPage(p.nextPageable().getPageNumber());
               }

               return pageResponse;
          }
     }

     public static String formatarDataLogEvento(LocalDateTime data) {

          String format = "";

          if (Objeto.notBlank(data)) {

               DateTimeFormatter pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

               format = data.format(pattern);

          }

          return format;
     }

     /**
      * mascararNumeroCartao
      *
      * @param numeroCartao
      * @return String
      */
     public static String mascararNumeroCartao(String numeroCartao) {

          String numeroCartaoMascarado = null;
          if (Objeto.notBlank(numeroCartao) && numeroCartao.length() == 16) {

               String mascara = "********";
               numeroCartaoMascarado = new StringBuilder(numeroCartao.substring(0, 4)).append(mascara).append(numeroCartao.substring(12)).toString();
          }

          return numeroCartaoMascarado;
     }
}
