
package br.com.conductor.rhblueapi.repository.pier;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.repository.utils.PierUtils;


@Repository
public class UsuarioPierRestRepository {

     @Autowired
     private PierUtils pierUtils;

     @Autowired
     private RestTemplate restTemplate;

     public UsuarioResponse consultarUsuario(final Long id) {

          final HttpHeaders headers = pierUtils.initializeHeaders();

          UriComponentsBuilder builder = fromUriString(pierUtils.initializePathBuilder().append("/usuarios/").append(id.toString()).toString());

          HttpEntity<String> httpEntity = new HttpEntity<String>(headers);

          ResponseEntity<UsuarioResponse> response;
          ParameterizedTypeReference<UsuarioResponse> responseType = new ParameterizedTypeReference<UsuarioResponse>() {
          };

          try {

               response = this.restTemplate.exchange(builder.toUriString(), GET, httpEntity, responseType);
               return response.getBody();

          } catch (Exception e) {
               return null;
          }

     }

}
