
package br.com.conductor.rhblueapi.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.conductor.rhblueapi.domain.CargaBeneficio;

public interface CargaBeneficioRepository extends JpaRepository<CargaBeneficio, Long> {

     @Query(value = "SELECT ID_CARGABENEFICIO FROM CARGASBENEFICIOS (NOLOCK) WHERE ID_EMPRESA = :idEmpresa AND ID_GRUPOEMPRESA = :idGrupoEmpresa AND ID_ARQUIVOCARGASTD = :idArquivoCarga ", nativeQuery = true)
     Long findIdCargaBeneficioByIdArquivoCargasAndIdEmpresaAndIdGrupoEmpresa(@Param("idArquivoCarga") Long idArquivoCargas, @Param("idEmpresa") Long idEmpresa, @Param("idGrupoEmpresa") Long idGrupoEmpresa);

     List<CargaBeneficio> findByIdCargaBeneficioIn(List<Long> idsCargaBeneficio);

     Optional<CargaBeneficio> findByIdCargaBeneficio(Long idsCargaBeneficio);

     CargaBeneficio findTopByStatusAndOrigemAndDataProcessamentoLessThan(Integer status, String frontRh, LocalDateTime dataProcessamentoLimite);

     List<CargaBeneficio> findByIdArquivoCargas(Long idArquivoCargas);
     
     @Modifying(clearAutomatically = true)
     @Transactional
     @Query(value = "update cargasbeneficios set status = :status where id_arquivocargastd in ( :numerosPedidos )", nativeQuery = true)
     int updateStatusByIdArquivoCargasIn(@Param("status") Integer status, @Param("numerosPedidos") List<Long> numerosPedidos);
     
}