
package br.com.conductor.rhblueapi.repository.financeiro.statuscarga;

import java.util.List;

import br.com.conductor.rhblueapi.domain.financeiro.CargaBeneficioFinanceiroCustom;

public interface CargaBeneficioFinanceiro {
     
     List<CargaBeneficioFinanceiroCustom> buscaDadosFinaceiroCargasPorNumeroPedido(Long numeroPedido);

}
