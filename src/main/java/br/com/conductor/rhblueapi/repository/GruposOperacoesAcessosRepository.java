package br.com.conductor.rhblueapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.GruposOperacoesAcessos;


public interface GruposOperacoesAcessosRepository extends JpaRepository<GruposOperacoesAcessos, Long> {

}
