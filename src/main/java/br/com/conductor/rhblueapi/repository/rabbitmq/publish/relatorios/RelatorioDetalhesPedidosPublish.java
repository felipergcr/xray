
package br.com.conductor.rhblueapi.repository.rabbitmq.publish.relatorios;

import java.util.UUID;

import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.relatorios.response.RelatorioResponse;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.RabbitMQPublish;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class RelatorioDetalhesPedidosPublish extends RabbitMQPublish {
     
     public void enviarIdentificadorSolicitacaoRelatorio(RelatorioResponse relatorioResponse) {

          String payload = jsonUtil.toJsonString(relatorioResponse);

          send(RabbitMQConstantes.Relatorios.EXCHANGE_RELATORIOS_DETALHES_PEDIDOS, RabbitMQConstantes.Relatorios.ROUTING_KEY_RELATORIOS_DETALHES_PEDIDOS, payload, UUID.randomUUID().toString());

     }

     public void enviarIdentificadorFilaDlq(final RelatorioResponse relatorioResponse) {

          String payload = jsonUtil.toJsonString(relatorioResponse);

          send(RabbitMQConstantes.Relatorios.EXCHANGE_RELATORIOS_DETALHES_PEDIDOS_DLX, RabbitMQConstantes.Relatorios.ROUTING_KEY_RELATORIOS_DETALHES_PEDIDOS, payload, UUID.randomUUID().toString());
     }

}
