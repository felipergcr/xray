
package br.com.conductor.rhblueapi.repository.pier;

import static br.com.conductor.rhblueapi.util.AppConstantes.PIER_NOTIFICACAO_EMAIL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.rhblueapi.domain.NotificacaoEmail;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;

@Service
@RefreshScope
@Lazy
public class NotificacaoEmailPierRest {

     @Value("${app.pier.notificacao.host}${app.pier.basepath}")
     private String server;

     @Autowired
     private HeadersDefaultPier headersPier;

     private RestTemplate restTemplate;

     @Autowired
     public NotificacaoEmailPierRest(RestTemplate restTemplate) {

          this.restTemplate = restTemplate;
     }

     public HttpStatus enviaEmail(NotificacaoEmail email) {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server + PIER_NOTIFICACAO_EMAIL);
          HttpHeaders headers = headersPier.creatHeaders();
          HttpEntity<?> entity = new HttpEntity<>(email, headers);

          try {
               ResponseEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, entity, String.class);
               return response.getStatusCode();
          } catch (HttpClientErrorException e) {
               throw new BadRequestCdt(e.getResponseBodyAsString());
          }
     }

}
