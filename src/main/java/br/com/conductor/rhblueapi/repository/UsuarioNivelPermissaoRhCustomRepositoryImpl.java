package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRhCustom;
import br.com.conductor.rhblueapi.domain.request.UsuarioNivelPermissaoRhRequest;
import br.com.conductor.rhblueapi.enums.OrderByEnum;

@Repository
public class UsuarioNivelPermissaoRhCustomRepositoryImpl implements UsuarioNivelPermissaoRhCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;
     
     @Value("${app.database.sqlserver.blue}")
     private String dataBaseBlue;
     
     @Value("${app.database.sqlserver.accesscontrol}")
     private String dataBaseAccessControl;
     
     public List<UsuarioPermissaoNivelAcessoRhCustom> buscarUsuarioPorNiveisPermissoes(UsuarioNivelPermissaoRhRequest request, StatusDescricao statusNivelPermissao, Integer page, Integer size) {
          
          StringBuilder sql = new StringBuilder();
          Query query;
          
          sql.append("SELECT ")
          .append("UNPR.ID_NIVEISPERMISSOES, ")
          .append("UNPR.ID_PERMISSAO, ")
          .append("UNPR.STATUS, ")
          .append("UNPR.ID_USUARIO_REGISTRO, ")
          .append("PUH.STATUS AS STATUS_PERMISSAO, ")
          .append("GE.ID_GRUPOEMPRESA AS ID_GRUPOEMPRESA, ")
          .append("GE.NOME AS NOME_GRUPOEMPRESA, ")
          .append("SUB.ID_GRUPOEMPRESA AS ID_SUBGRUPOEMPRESA, ")
          .append("SUB.NOME AS NOME_SUBGRUPOEMPRESA, ")
          .append("E.ID_EMPRESA AS ID_EMPRESA, ")
          .append("P.NOME AS NOME_EMPRESA, ")
          .append("U.Nome AS NOME_USUARIO, ")
          .append("U.CPF AS CPF_USUARIO ")
          .append(this.condicoesUsuariosPorPermissoes(request));
                    
          if(request.getOrderBy().equals(OrderByEnum.ASC)) 
               sql.append("ORDER BY U.NOME ASC ");               
          else 
               sql.append("ORDER BY U.NOME DESC ");
          
          sql.append("OFFSET (:page * :size) rows FETCH NEXT :size rows only ");
          
          query = entityManager.createNativeQuery(sql.toString(), UsuarioPermissaoNivelAcessoRhCustom.class);
          
          query.setParameter("IdNivelPermissao", request.getIdNivelPermissao());
          query.setParameter("IdGrupoEmpresa", request.getIdGrupoEmpresa());
          query.setParameter("IdSubgrupoEmpresa", request.getIdSubgrupoEmpresa());
          query.setParameter("IdEmpresa",request.getIdEmpresa());
          query.setParameter("IdUsuarioRegistro", request.getIdUsuarioRegistro());
          query.setParameter("IdPermisssao", request.getIdPermissao());
          query.setParameter("status", Objects.isNull(statusNivelPermissao) ? null : statusNivelPermissao.getStatus());
          query.setParameter("page",page);
          query.setParameter("size",size);
          
          @SuppressWarnings("unchecked")
          List<UsuarioPermissaoNivelAcessoRhCustom> listaPermissoesPaginada = (List<UsuarioPermissaoNivelAcessoRhCustom>) query.getResultList();
          
          return listaPermissoesPaginada;   
     }

     @Override
     public Integer countUsuariosPorPermissoes(UsuarioNivelPermissaoRhRequest request, StatusDescricao statusNivelPermissao) {
          
          StringBuilder sql = new StringBuilder();
          Query query;
          
          sql.append("SELECT ")
          .append("COUNT(U.Nome) AS TOTAL ")
          .append(this.condicoesUsuariosPorPermissoes(request));
                    
          query = entityManager.createNativeQuery(sql.toString());
          
          query.setParameter("IdNivelPermissao", request.getIdNivelPermissao());
          query.setParameter("IdGrupoEmpresa", request.getIdGrupoEmpresa());
          query.setParameter("IdSubgrupoEmpresa", request.getIdSubgrupoEmpresa());
          query.setParameter("IdEmpresa",request.getIdEmpresa());
          query.setParameter("IdUsuarioRegistro", request.getIdUsuarioRegistro());
          query.setParameter("IdPermisssao", request.getIdPermissao());
          query.setParameter("status", Objects.isNull(statusNivelPermissao) ? null : statusNivelPermissao.getStatus());
          
          Integer count = (Integer) query.getSingleResult();

          return count;
     }
     
     private String condicoesUsuariosPorPermissoes(UsuarioNivelPermissaoRhRequest request) {
          
          StringBuilder sql = new StringBuilder();
          
          sql.append("FROM ")
          .append(dataBaseBlue)
          .append(".PERMISSOESUSUARIOSRH PUH ")
          .append("INNER JOIN ").append(dataBaseBlue)
          .append(".USUARIOSNIVEISPERMISSOESRH UNPR ON (PUH.ID_PERMISSAO = UNPR.ID_PERMISSAO) ")
          .append("INNER JOIN ").append(dataBaseAccessControl)
          .append(".Usuarios U ON (PUH.ID_USUARIO = U.ID_USUARIO) ")
          .append("LEFT JOIN ").append(dataBaseBlue)
          .append(".GRUPOSEMPRESAS GE ON (GE.ID_GRUPOEMPRESA = UNPR.ID_GRUPOEMPRESA) ")
          .append("LEFT JOIN ").append(dataBaseBlue)
          .append(".GRUPOSEMPRESAS SUB ON (SUB.ID_GRUPOEMPRESA = UNPR.ID_SUBGRUPOEMPRESA) ")
          .append("LEFT JOIN ").append(dataBaseBlue)
          .append(".EMPRESAS E ON (E.ID_EMPRESA = UNPR.ID_EMPRESA) ")
          .append("LEFT JOIN ").append(dataBaseBlue)
          .append(".PESSOAS P ON (E.ID_PESSOA = P.ID_PESSOAFISICA) ")
          .append("WHERE ")
          .append("((:IdNivelPermissao IS NULL) OR (UNPR.ID_NIVEISPERMISSOES = :IdNivelPermissao)) ")
          .append("AND ((:IdGrupoEmpresa IS NULL) OR (UNPR.ID_GRUPOEMPRESA = :IdGrupoEmpresa)) ")
          .append("AND ((:IdSubgrupoEmpresa IS NULL) OR (UNPR.ID_SUBGRUPOEMPRESA = :IdSubgrupoEmpresa)) ")
          .append("AND ((:IdEmpresa IS NULL) OR (UNPR.ID_EMPRESA = :IdEmpresa)) ")
          .append("AND ((:IdUsuarioRegistro IS NULL) OR (UNPR.ID_USUARIO_REGISTRO = :IdUsuarioRegistro)) ")
          .append("AND ((:IdPermisssao IS NULL) OR (UNPR.ID_PERMISSAO = :IdPermisssao)) ")
          .append("AND ((:status IS NULL) OR (UNPR.STATUS = :status)) ");
          
          return sql.toString();    
     }

}
