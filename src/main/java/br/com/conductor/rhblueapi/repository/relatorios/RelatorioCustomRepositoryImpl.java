
package br.com.conductor.rhblueapi.repository.relatorios;

import static br.com.conductor.rhblueapi.util.DataUtils.localDateToLocalDateTimeFirstMoment;
import static br.com.conductor.rhblueapi.util.DataUtils.localDateToLocalDateTimeLastMoment;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.relatorios.response.RelatorioResponse;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioFilter;

@Repository
public class RelatorioCustomRepositoryImpl implements RelatorioCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;
     
     @Override
     public Integer countAllWithParameters(RelatorioFilter relatorioFilter) {

          StringBuilder sql = new StringBuilder();

          Query query;
          
          sql.append(this.select())
          .append(this.retornoCountRelatorioResponse())
          .append(this.joins())
          .append(this.where());
          
          query = entityManager.createNativeQuery(sql.toString());
          
          query.setParameter("idUsuario", relatorioFilter.getIdUsuario());
          query.setParameter("idGrupoEmpresa", relatorioFilter.getIdGrupoEmpresa());
          query.setParameter("dataInicio", localDateToLocalDateTimeFirstMoment(relatorioFilter.getDataInicio()));
          query.setParameter("dataFim",  localDateToLocalDateTimeLastMoment(relatorioFilter.getDataFim()));
          
          return (Integer) query.getSingleResult();
     }

     @SuppressWarnings("unchecked")
     @Override
     public List<RelatorioResponse> findAllWithParameters(RelatorioFilter relatorioFilter) {
          
          StringBuilder sql = new StringBuilder();

          Query query;
          
          sql.append(this.select())
          .append(this.retornoRelatorioResponse())
          .append(this.joins())
          .append(this.where())
          .append(this.ordenacao())
          .append(this.paginacao());
          
          query = entityManager.createNativeQuery(sql.toString(), RelatorioResponse.class);
          
          query.setParameter("idUsuario", relatorioFilter.getIdUsuario());
          query.setParameter("idGrupoEmpresa", relatorioFilter.getIdGrupoEmpresa());
          query.setParameter("dataInicio", localDateToLocalDateTimeFirstMoment(relatorioFilter.getDataInicio()));
          query.setParameter("dataFim",  localDateToLocalDateTimeLastMoment(relatorioFilter.getDataFim()));
          query.setParameter("numberPage", relatorioFilter.getNumberPage());
          query.setParameter("sizePage", relatorioFilter.getSizePage());
          
          return (List<RelatorioResponse>) query.getResultList();
     }
     
     
     private String select() {

          return "SELECT ";
     }
     
     private String retornoCountRelatorioResponse() {

          return "COUNT(RP.ID_RELATORIO) ";
     }
     
     
     private String retornoRelatorioResponse() {

          return new StringBuilder()
                    .append("RP.ID_RELATORIO AS ID_RELATORIO, ")
                    .append("RP.DATASOLICITACAO AS DATASOLICITACAO, ")
                    .append("RP.DATAINICIO AS DATAINICIO, ")
                    .append("RP.DATAFIM AS DATAFIM, ")
                    .append("TRP.DESCRICAOTIPORELATORIO AS NOMERELATORIO, ")
                    .append("RP.STATUSRELATORIO AS STATUS ")
                    .toString();
     }
     
     private String joins() {
          
          return new StringBuilder()
                    .append("FROM RelatorioPortal RP (NOLOCK) ")
                    .append("INNER JOIN TipoRelatorioPortal TRP (NOLOCK) ")
                    .append("ON (RP.ID_TIPORELATORIOPORTAL = TRP.ID_TIPORELATORIOPORTAL) ")
                    .toString();
     }
     
     private String where() {
          
          return new StringBuilder()
                    .append("WHERE ")
                    .append("RP.ID_USUARIO = :idUsuario ")
                    .append("AND RP.ID_GRUPOEMPRESA = :idGrupoEmpresa ")
                    .append("AND RP.DATASOLICITACAO BETWEEN :dataInicio AND :dataFim ")
                    .toString();
     }
     
     private String ordenacao() {
          
          return "ORDER BY RP.DATASOLICITACAO DESC ";
     }
     
     private String paginacao() {
          
          return "OFFSET (:numberPage * :sizePage) rows FETCH NEXT :sizePage  rows only ";
     }
     
}