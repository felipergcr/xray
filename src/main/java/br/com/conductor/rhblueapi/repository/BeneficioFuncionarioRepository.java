
package br.com.conductor.rhblueapi.repository;

import br.com.conductor.rhblueapi.domain.proc.BeneficioFuncionarioProdutoResponse;

public interface BeneficioFuncionarioRepository {

     BeneficioFuncionarioProdutoResponse atualizaFuncionarioProduto(Long idProduto, Long idFuncionario);

}
