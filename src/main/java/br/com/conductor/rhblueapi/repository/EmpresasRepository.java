package br.com.conductor.rhblueapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.Empresa;

public interface EmpresasRepository extends JpaRepository<Empresa, Long> {
     
     
     
}
