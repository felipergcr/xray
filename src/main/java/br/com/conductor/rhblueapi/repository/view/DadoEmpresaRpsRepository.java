package br.com.conductor.rhblueapi.repository.view;

import br.com.conductor.rhblueapi.domain.view.DadoEmpresaRps;
import org.springframework.data.repository.CrudRepository;

public interface DadoEmpresaRpsRepository extends CrudRepository<DadoEmpresaRps, Long> {
}
