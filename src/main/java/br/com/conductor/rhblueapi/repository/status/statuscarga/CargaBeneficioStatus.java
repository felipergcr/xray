
package br.com.conductor.rhblueapi.repository.status.statuscarga;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import br.com.conductor.rhblueapi.domain.status.CargaBeneficioMinimoCustom;

public interface CargaBeneficioStatus {
     
     List<CargaBeneficioMinimoCustom> buscaCargasPagas(Integer status, LocalDate dataMovimento);
     
     List<CargaBeneficioMinimoCustom> buscaCargasCanceladas(Integer status, LocalDateTime dataHoraAtual, LocalDateTime dataHoraAtualMenos2Horas);
     
     List<CargaBeneficioMinimoCustom> buscaCargasExpiradas(Integer status, LocalDateTime dataHoraAtual, LocalDateTime dataAtualMenosUmDia);

}
