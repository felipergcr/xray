
package br.com.conductor.rhblueapi.repository.financeiro.statuscarga;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.financeiro.CargaBeneficioFinanceiroCustom;
import br.com.conductor.rhblueapi.util.AppConstantes;


@Repository
public class CargaBeneficioFinanceiroImpl implements CargaBeneficioFinanceiro {
     
     @PersistenceContext
     private EntityManager entityManager;

     @SuppressWarnings("unchecked")
     @Override
     public List<CargaBeneficioFinanceiroCustom> buscaDadosFinaceiroCargasPorNumeroPedido(Long numeroPedido) {

          StringBuilder sql = new StringBuilder();

          Query query;
          
          sql.append("WITH VW_CARGASBENEFICIOSFINANCEIRO as ")
          .append("( ")
          .append("SELECT CB.ID_CARGABENEFICIO, CB.Id_CargaControleFinanceiro ")
          .append("FROM CARGASBENEFICIOS CB (NOLOCK) ")
          .append("WHERE CB.ID_ARQUIVOCARGASTD = :numeroPedido ")
          .append(") ")
          .append("select ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, x.* ")
          .append("from ( ")
          .append("SELECT ")
          .append("'")
          .append(AppConstantes.QTD_CARGASPEDIDO)
          .append("'")
          .append("as Tipo, COUNT(CB.ID_CARGABENEFICIO) as Valor ")
          .append("from VW_CARGASBENEFICIOSFINANCEIRO CB ")
          .append("UNION ")
          .append("SELECT ")
          .append("'")
          .append(AppConstantes.QTD_CARGASPEDIDOPAGAS)
          .append("'")
          .append("as Tipo, COUNT(CB.ID_CARGABENEFICIO) as Valor ")
          .append("from VW_CARGASBENEFICIOSFINANCEIRO CB ")
          .append("INNER JOIN CARGACONTROLEFINANCEIRO CCF (NOLOCK) ")
          .append("ON (CB.Id_CargaControleFinanceiro = CCF.Id_CargaControleFinanceiro) ")
          .append("INNER JOIN BOLETOSEMITIDOS BE (NOLOCK) ")
          .append("ON (CCF.Id_Boleto = BE.ID_BOLETO) ")
          .append("INNER JOIN EVENTOSEXTERNOSPAGAMENTOS EEP (NOLOCK) ")
          .append("ON (BE.ID_EVENTOPAGAMENTO = EEP.ID_EVENTOPAGAMENTO) ")
          .append(") x")
          ;
          
          query = entityManager.createNativeQuery(sql.toString(), CargaBeneficioFinanceiroCustom.class);

          query.setParameter("numeroPedido", numeroPedido);

          return (List<CargaBeneficioFinanceiroCustom>) query.getResultList();
     }

}
