
package br.com.conductor.rhblueapi.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.GrupoEmpresa;

@Repository
public interface GrupoEmpresaRepository extends JpaRepository<GrupoEmpresa, Long> {
     
     Page<GrupoEmpresa> findByIdGrupoEmpresaPai(Long idGrupoEmpresaPai, Pageable pageable);
     
     Optional<GrupoEmpresa> findById(Long idGrupoEmpresa);
     
     Optional<GrupoEmpresa> findByIdAndIdGrupoEmpresaPai(Long idGrupoEmpresa, Long idGrupoEmpresaPai);

}
