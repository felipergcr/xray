
package br.com.conductor.rhblueapi.repository.rabbitmq.publish;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.pedido.CargaPedidoCustom;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class LimiteCreditoPublisher extends RabbitMQPublish {
     
     public void enviarCargasPedidosPorGrupoEmpresaParaValidacaoLimiteDiario(List<CargaPedidoCustom> cargasPedidos) {
          
          cargasPedidos.stream().map(CargaPedidoCustom::getNumeroPedido).sorted().toString();
          
          String payload = jsonUtil.toJsonString(cargasPedidos);

          send(RabbitMQConstantes.LimiteCredito.EXCHANGE_VALIDACAO_DIARIA, RabbitMQConstantes.LimiteCredito.ROUTING_KEY_VALIDACAO_DIARIA, payload, 
                    cargasPedidos.stream()
                    .map(CargaPedidoCustom::getNumeroPedido)
                    .sorted()
                    .map(item -> String.valueOf(item))
                    .collect(Collectors.joining()));
     }
     
}
