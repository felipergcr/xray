
package br.com.conductor.rhblueapi.repository.rabbitmq.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.util.JsonUtil;

@Component
public abstract class RabbitMQConsumer {

     @Autowired
     protected JsonUtil jsonUtil;

}
