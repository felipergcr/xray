
package br.com.conductor.rhblueapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.Parametro;

@Repository
public interface ParametroRepository extends JpaRepository<Parametro, Long> {

     Optional<Parametro> findByTipoParametroAndCodigo(String tipoParametro, String codigo);

     Optional<Parametro> findByCodigo(String codigo);

}
