package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.PermissaoOperacaoRh;

public interface PermissaoOperacaoRhRepository extends JpaRepository<PermissaoOperacaoRh, Long> {

     Optional<PermissaoOperacaoRh> findByIdPermissaoAndIdOperacao(Long idPermissao, Long idOperacao);
     
     List<PermissaoOperacaoRh> findAllByIdPermissao(Long idPermissao);
     
     Optional<PermissaoOperacaoRh> findByIdUsuarioRegistro(Long idUsuarioRegistro);
}
