
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.UnidadeEntrega;

public interface UnidadeEntregaRepository extends JpaRepository<UnidadeEntrega, Long> {

     Optional<List<UnidadeEntrega>> findByIdGrupoEmpresaAndStatus(Long idGrupoEmpresa, Integer status);

     Optional<UnidadeEntrega> findTopByIdGrupoEmpresaAndCodigoUnidadeEntregaAndStatus(Long idGrupoEmpresa, String codigoUnidade, Integer status);

     Optional<UnidadeEntrega> findTopByCodigoUnidadeEntregaAndStatusAndIdGrupoEmpresa(String codigoUnidade, Integer status, Long idGrupoEmpresa);

}
