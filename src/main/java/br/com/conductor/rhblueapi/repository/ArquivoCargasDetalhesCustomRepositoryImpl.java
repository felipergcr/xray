package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.ArquivoPedidoDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.util.PageUtils;

@Repository
public class ArquivoCargasDetalhesCustomRepositoryImpl implements ArquivoCargasDetalhesCustomRepository {
     
     @PersistenceContext
     private EntityManager entityManager;

     @Override
     public PageResponse<ArquivoPedidoDetalhesErrosReponse> findByIdArquivoCargasAndStatusDetalheOrderByNumeroLinhaArquivo(Long idArquivoCarga, Integer page, Integer size) {
          
          Pageable pageable = PageRequest.of(page, size);
          Query query;
          StringBuilder sql = new StringBuilder();

          sql.append("SELECT ")
          .append("ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, ")
          .append("acd.NumeroLinhaArquivo AS linha, acde.Motivo AS erro from ARQUIVOSCARGASDETALHES acd ")
          .append("inner join ARQUIVOCARGASTD acs on (acs.ID_ARQUIVOCARGASTD = acd.ID_ARQUIVOCARGASTD) ")
          .append("inner join ARQUIVOSCARGASDETALHESerros acde on (acd.Id_DetalheArquivo = acde.Id_DetalheArquivo) ")
          .append("where acd.id_arquivocargastd = :idArquivoCarga and acd.StatusDetalhe = 2 order by acd.NumeroLinhaArquivo ")
          .append(" offset (:page * :size) rows FETCH NEXT :size rows only ");
          
          query = entityManager.createNativeQuery(sql.toString(), ArquivoPedidoDetalhesErrosReponse.class);
          query.setParameter("idArquivoCarga", idArquivoCarga);
          query.setParameter("page", page);
          query.setParameter("size", size);
          
          Integer count = contadorRegistros(idArquivoCarga);
                  
          @SuppressWarnings("unchecked")
          List<ArquivoPedidoDetalhesErrosReponse> detalhesErros = (List<ArquivoPedidoDetalhesErrosReponse>)  query.getResultList();
          
          PageResponse<ArquivoPedidoDetalhesErrosReponse> pageResponse = new PageResponse<ArquivoPedidoDetalhesErrosReponse>();
          pageResponse.setContent(detalhesErros);
          pageResponse = PageUtils.ajustarPageResponse(pageResponse, Integer.valueOf(pageable.getPageNumber()), Integer.valueOf(pageable.getPageSize()), count);
                
          return pageResponse;
     }
     
     private Integer contadorRegistros(Long idArquivoCarga) {
          
          Query query;
          StringBuilder sql = new StringBuilder();
          
          sql.append("SELECT ")
          .append("count(acd.NumeroLinhaArquivo) from ARQUIVOSCARGASDETALHES acd ")
          .append("inner join ARQUIVOCARGASTD acs on (acs.ID_ARQUIVOCARGASTD = acd.ID_ARQUIVOCARGASTD) ")
          .append("inner join ARQUIVOSCARGASDETALHESerros acde on (acd.Id_DetalheArquivo = acde.Id_DetalheArquivo) ")
          .append("where acd.id_arquivocargastd = :idArquivoCarga and acd.StatusDetalhe = 2 ");
          
          query = entityManager.createNativeQuery(sql.toString());
          query.setParameter("idArquivoCarga", idArquivoCarga);
          
          return (Integer) query.getSingleResult();
     }
     
}
