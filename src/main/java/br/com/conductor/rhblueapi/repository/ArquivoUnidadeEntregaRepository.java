
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntrega;

public interface ArquivoUnidadeEntregaRepository extends JpaRepository<ArquivoUnidadeEntrega, Long> {

     public Optional<List<ArquivoUnidadeEntrega>> findByIdGrupoEmpresaAndStatusIn(Long idGrupoEmpresa, List<Integer> status);
}
