package br.com.conductor.rhblueapi.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhesErros;

@Repository
public interface ArquivosCargasDetalhesErrosRepository extends JpaRepository<ArquivosCargasDetalhesErros, Long> {
     
     Page<ArquivosCargasDetalhesErros> findAllByIdDetalheArquivoIn(List<Long> idArquivos, Pageable pageable);

}
