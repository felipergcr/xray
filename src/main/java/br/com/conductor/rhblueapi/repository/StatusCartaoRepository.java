package br.com.conductor.rhblueapi.repository;

import br.com.conductor.rhblueapi.domain.StatusCartoes;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * StatusCartaoRepository
 */
public interface StatusCartaoRepository extends JpaRepository<StatusCartoes, Long> {

}
