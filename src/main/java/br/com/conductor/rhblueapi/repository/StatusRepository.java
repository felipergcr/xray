
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.config.CacheConfig;
import br.com.conductor.rhblueapi.domain.Status;

public interface StatusRepository extends JpaRepository<Status, Long> {

     @Cacheable(CacheConfig.LISTA_STATUS)
     @Override
     public List<Status> findAll();

     public Optional<Status> findByDescricao(String string);
}
