package br.com.conductor.rhblueapi.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.config.CacheConfig;
import br.com.conductor.rhblueapi.domain.AcessoNiveisUsuario;

@Repository
public interface AcessoNiveisUsuarioRepository extends JpaRepository<AcessoNiveisUsuario, Long>{

     AcessoNiveisUsuario findByIdPlataformaAndDescricaoNivelPefilIgnoreCase(Integer idPlataforma,String string);

     List<AcessoNiveisUsuario> findByNivelPerfilAndIdPlataforma(Integer nivelPerfil, Integer idPlataforma);
    						   
     @Cacheable(CacheConfig.LISTA_ACESSO_NIVEIS_USUARIO)
     @Override
     public List<AcessoNiveisUsuario> findAll();
     
     public List<AcessoNiveisUsuario> findByIdPlataforma(Integer idPlataforma);
}
