
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.NotaFinanceiroCustom;

@Repository
public class FinanceiroCargaControleFinanceiroCustomRepositoryImpl implements FinanceiroCargaControleFinanceiroCustomRepository {
     
     @PersistenceContext
     private EntityManager entityManager;

     @Override
     public List<NotaFinanceiroCustom> buscaDadosFinanceiroPor(Long idCargaControleFinanceiro, Long idGrupoEmpresa, List<Integer> idsEmpresas) {
          
          StringBuilder sql = new StringBuilder();
          Query query;
          
          sql.append("SELECT ")
               .append("ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, ")
               .append("CCF.ID_CARGACONTROLEFINANCEIRO, ")
               .append("CCF.NumeroNF ")
               .append("FROM ")
               .append("ARQUIVOCARGASTD ACS ")
               .append("INNER JOIN CARGASBENEFICIOS CB ON ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD ")
               .append("INNER JOIN CARGACONTROLEFINANCEIRO CCF ON CB.Id_CargaControleFinanceiro = CCF.Id_CargaControleFinanceiro ")
               .append("INNER JOIN EmpresasCargas EC ON (CB.ID_CARGABENEFICIO = EC.ID_CARGABENEFICIO)  ")
               .append("INNER JOIN EMPRESAS E ON (EC.ID_EMPRESA = E.ID_EMPRESA ) ")
               .append("WHERE ")
               .append("ACS.ID_GRUPOEMPRESA = :idGrupoEmpresa ")
               .append("AND CCF.Id_CargaControleFinanceiro = :idCargaControleFinanceiro ")
               .append("AND e.id_empresa in (:idsEmpresas) ");
          
          query = entityManager.createNativeQuery(sql.toString(), NotaFinanceiroCustom.class);
          query.setParameter("idCargaControleFinanceiro", idCargaControleFinanceiro);
          query.setParameter("idGrupoEmpresa", idGrupoEmpresa);
          query.setParameter("idsEmpresas", idsEmpresas);

          @SuppressWarnings("unchecked")
          List<NotaFinanceiroCustom> notaFinanceiro = (List<NotaFinanceiroCustom>) query.getResultList();
          
          return notaFinanceiro;
     }

}
