
package br.com.conductor.rhblueapi.repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.CargaBeneficioCustom;
import br.com.conductor.rhblueapi.domain.CargaBeneficioProdutosCustom;
import br.com.conductor.rhblueapi.domain.FuncionarioCargaBeneficio;
import br.com.conductor.rhblueapi.domain.request.PedidoDetalhesRequest;
import br.com.twsoftware.alfred.object.Objeto;

@Repository
public class CargaBeneficioCustomRepositoryImpl implements CargaBeneficioCustomRepository {

     private static final Integer ERRO_NO_PROCESSAMENTO = 4;

     @PersistenceContext
     private EntityManager entityManager;

     @Override
     public List<CargaBeneficioCustom> listaDetalhadaDePedido(PedidoDetalhesRequest arquivoCargaRequest, Pageable pageable) {

          StringBuilder sql = new StringBuilder();
          sql.append("SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID")
          .append(", ACS.ID_ARQUIVOCARGASTD")
          .append(", EC.Id_CargaBeneficio")
          .append(", CB.ID_EMPRESA")
          .append(", P.CPF")
          .append(", EC.VALOR AS VALOR")
          .append(", ACS.STATUS as STATUSARQUIVOCARGA")
          .append(", CB.STATUS")
          .append(", CB.ID_GRUPOEMPRESA")
          .append(", ACS.FLAGFATURAMENTOCENTRALIZADO")
          .append(", CB.DATAAGENDAMENTO")
          .append(", COUNT(ECD.ID_FUNCIONARIO) AS QTDFUNC")
          .append(" FROM ARQUIVOCARGASTD ACS INNER JOIN CARGASBENEFICIOS CB ON ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD")
          .append(" INNER JOIN EMPRESAS E ON E.ID_EMPRESA = CB.ID_EMPRESA")
          .append(" INNER JOIN PESSOAS P ON P.ID_PESSOAFISICA = E.ID_PESSOA")
          .append(" INNER JOIN EMPRESASCARGAS EC ON EC.ID_CARGABENEFICIO = CB.ID_CARGABENEFICIO")
          .append(" INNER JOIN EMPRESASCARGASDETALHES ECD ON ECD.ID_EMPRESACARGA = EC.ID_EMPRESACARGA")
          .append(" WHERE ");

          sql.append(filtrarSqlParamsDetalhes(arquivoCargaRequest));

          sql.append(" GROUP BY ACS.ID_ARQUIVOCARGASTD , CB.ID_EMPRESA , P.CPF, CB.STATUS, CB.ID_GRUPOEMPRESA, EC.Id_CargaBeneficio,ACS.STATUS,CB.DATAAGENDAMENTO,ACS.FLAGFATURAMENTOCENTRALIZADO, EC.VALOR");

          sql.append(" ORDER BY ACS.ID_ARQUIVOCARGASTD ASC");

          sql.append(" offset (:page * :size) rows FETCH NEXT :size rows only ");

          return constroiPesquisaDetalhadaInteira(sql, arquivoCargaRequest, pageable);
     }

     @Override
     public Integer countRegistrosDetalhado(PedidoDetalhesRequest arquivoCargaRequest, Pageable pageable) {

          StringBuilder str = new StringBuilder();
          str.append("SELECT COUNT (*) from (  SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, ")
          .append("ACS.ID_ARQUIVOCARGASTD, CB.ID_EMPRESA, P.CPF, EC.VALOR, CB.STATUS, CB.ID_GRUPOEMPRESA, CB.DATAAGENDAMENTO,ACS.FLAGFATURAMENTOCENTRALIZADO,ACS.STATUS as STATUSARQUIVOCARGA, COUNT(ECD.ID_FUNCIONARIO) AS QTDFUNC ")
          .append(" FROM ARQUIVOCARGASTD ACS INNER JOIN CARGASBENEFICIOS CB ON ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD")
          .append(" INNER JOIN EMPRESAS E ON E.ID_EMPRESA = CB.ID_EMPRESA")
          .append(" INNER JOIN PESSOAS P ON P.ID_PESSOAFISICA = E.ID_PESSOA")
          .append(" INNER JOIN EMPRESASCARGAS EC ON EC.ID_CARGABENEFICIO = CB.ID_CARGABENEFICIO")
          .append(" INNER JOIN EMPRESASCARGASDETALHES ECD ON ECD.ID_EMPRESACARGA = EC.ID_EMPRESACARGA")
          .append(" WHERE ").append(filtrarSqlParamsDetalhes(arquivoCargaRequest))
          .append(" GROUP BY ACS.ID_ARQUIVOCARGASTD , CB.ID_EMPRESA , P.CPF, EC.VALOR, CB.STATUS,CB.DATAAGENDAMENTO,ACS.STATUS,ACS.FLAGFATURAMENTOCENTRALIZADO,CB.ID_GRUPOEMPRESA ) x");

          return constroiPesquisaDetalhadaCount(str, arquivoCargaRequest, pageable);
     }

     private String filtrarSqlParamsDetalhes(PedidoDetalhesRequest arquivoCargaRequest) {

          StringBuilder sql = new StringBuilder();

          if (Objects.nonNull(arquivoCargaRequest.getIdPedido())) {
               sql.append(" CB.ID_ARQUIVOCARGASTD = :idPedido AND");
          }

          if (Objects.nonNull(arquivoCargaRequest.getIdEmpresasPermitidas())) {
               sql.append(" CB.ID_EMPRESA IN :idEmpresa AND");
          }

          if (Objects.nonNull(arquivoCargaRequest.getIdGrupoEmpresa())) {
               sql.append(" ACS.ID_GRUPOEMPRESA = :idGrupoEmpresa AND");
          }

          if (Objects.nonNull(arquivoCargaRequest.getIdCargaBeneficio())) {
               sql.append(" CB.ID_CARGABENEFICIO = :idCargaBeneficio AND");
          }

          if ("AND".equals(sql.substring(sql.length() - 3, sql.length()))) {
               StringBuilder sqlTemp = new StringBuilder();
               sqlTemp.append(sql.substring(0, sql.length() - 3));
               sql = new StringBuilder(sqlTemp);
          }

          return sql.toString();
     }

     private List<CargaBeneficioCustom> constroiPesquisaDetalhadaInteira(StringBuilder sql, PedidoDetalhesRequest arquivoCargaRequest, Pageable pageable) {

          Query query = entityManager.createNativeQuery(sql.toString(), CargaBeneficioCustom.class);

          query = parametrizarConsultaDetalhada(arquivoCargaRequest, pageable, query, true);

          @SuppressWarnings("unchecked")
          List<CargaBeneficioCustom> retorno = query.getResultList();

          return retorno;

     }

     private Integer constroiPesquisaDetalhadaCount(StringBuilder sql, PedidoDetalhesRequest arquivoCargaRequest, Pageable pageable) {

          Query query = entityManager.createNativeQuery(sql.toString());

          query = parametrizarConsultaDetalhada(arquivoCargaRequest, pageable, query, false);

          Integer resultList = (Integer) query.getSingleResult();

          return resultList;

     }

     private Query parametrizarConsultaDetalhada(PedidoDetalhesRequest arquivoCargaRequest, Pageable pageable, Query query, Boolean paging) {

          if (Objects.nonNull(arquivoCargaRequest.getIdPedido())) {
               query.setParameter("idPedido", arquivoCargaRequest.getIdPedido());
          }

          if (Objects.nonNull(arquivoCargaRequest.getIdEmpresasPermitidas())) {
               query.setParameter("idEmpresa", arquivoCargaRequest.getIdEmpresasPermitidas());
          }

          if (Objects.nonNull(arquivoCargaRequest.getIdGrupoEmpresa())) {
               query.setParameter("idGrupoEmpresa", arquivoCargaRequest.getIdGrupoEmpresa());
          }

          if (Objects.nonNull(arquivoCargaRequest.getIdCargaBeneficio())) {
               query.setParameter("idCargaBeneficio", arquivoCargaRequest.getIdCargaBeneficio());
          }

          if (paging) {
               if (!String.valueOf(pageable.getPageNumber()).isEmpty()) {
                    query.setParameter("page", pageable.getPageNumber());
               }
               if (!String.valueOf(pageable.getPageSize()).isEmpty()) {
                    query.setParameter("size", pageable.getPageSize());
               }
          }

          return query;
     }

     @Override
     public List<FuncionarioCargaBeneficio> listaDetalhesFuncionariosCarga(Long idCargaBeneficio, Integer statusEmpresaCargaDetalheProduto,List<Integer> listaIdFuncionarios, Pageable pageable) {

          if (listaIdFuncionarios.isEmpty()) {
               return new ArrayList<FuncionarioCargaBeneficio>();
          } else {
               int indice = pageable.getPageNumber() * pageable.getPageSize();

               int maximoPagina = indice + pageable.getPageSize();
               int maxUltimaPagina = listaIdFuncionarios.size();
               int max = maximoPagina > maxUltimaPagina ? maxUltimaPagina : maximoPagina;
               Set<Integer> listaIdFuncionariosPaginado = new HashSet<>();
               for (int i = indice; i < max; i++) {
                    listaIdFuncionariosPaginado.add(listaIdFuncionarios.get(i));
               }
               if (Objeto.isBlank(listaIdFuncionariosPaginado)) {
                    return new ArrayList<>();
               }
               
               String sql = " WITH VW_DETALHES_PEDIDOS as ( "+
               " SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, "+
               " func.Id_Funcionario as ID_FUNCIONARIO, "+
               "         pf.Nome as NOME_FUNCIONARIO, "+
               " pf.cpf as CPF, "+
               "         p.Descricao as PRODUTO_DESCRICAO, "+
               " SUM(ecdp.Valor) as VALOR_CARGA, "+
               " ct.Id_Produto as ID_PRODUTO, "+
               " (select case ecdp.FlagNovoCartao "+
               "     when 1 then 'SIM' "+
               "     when 0 then 'NAO' "+
               "     end ) as FlagNovoCartao, "+
               "  cb.id_arquivocargastd "+
               "  FROM empresasCargasDetalhesProdutos ecdp "+
               "  INNER JOIN funcionariosProdutos fp on fp.id_funcionariosProdutos = ecdp.id_funcionariosProdutos "+
               "  INNER JOIN contas ct on ct.id_conta = fp.id_conta "+
               "  INNER JOIN funcionarios func on func.id_funcionario = fp.id_funcionario "+
               "  INNER JOIN pessoasFisicas pf on pf.id_pessoaFisica = func.id_pessoaFisica "+
               "  INNER JOIN empresasCargasDetalhes ecd on ecd.Id_EmpresaCargaDetalhe = ecdp.Id_EmpresaCargaDetalhe "+
               "  INNER JOIN empresasCargas ec on ec.id_empresaCarga = ecd.id_empresaCarga "+
               "  INNER JOIN CargasBeneficios cb on cb.id_cargaBeneficio = ec.id_cargaBeneficio "+
               "  INNER JOIN produtos p on p.Id_Produto = ct.Id_Produto "+
               "  WHERE ecdp.Status not in (:statusErroProcessamento) "+
               "  and ec.id_cargaBeneficio = :idCargaBeneficio "+
               " and func.Id_Funcionario IN :listaIdFuncionariosPaginado "+
               " GROUP BY func.Id_Funcionario, pf.Nome, pf.cpf, p.Descricao, ct.Id_Produto, ecdp.FlagNovoCartao, cb.id_arquivocargastd  ) "+
               " select "+
               " VWDP.ID, VWDP.ID_FUNCIONARIO, VWDP.NOME_FUNCIONARIO, VWDP.CPF, VWDP.PRODUTO_DESCRICAO, VWDP.VALOR_CARGA, VWDP.ID_PRODUTO, VWDP.FlagNovoCartao as EMISSAO_CARTAO, "+
               "           ENDERECO as LOGRADOURO, COMPLEMENTO, MUNICIPIO as Cidade, BAIRRO, UF, CEP "+
               " from "+
               " VW_DETALHES_PEDIDOS VWDP inner join ArquivoCargaFuncionarioStd acf on acf.id_arquivo = VWDP.id_arquivocargastd and acf.Cpf = VWDP.CPF"+
               " UNION "+
               "  select "+
               " VWDP.ID, VWDP.ID_FUNCIONARIO, VWDP.NOME_FUNCIONARIO, VWDP.CPF, VWDP.PRODUTO_DESCRICAO, VWDP.VALOR_CARGA, VWDP.ID_PRODUTO, VWDP.FlagNovoCartao as EMISSAO_CARTAO, "+
               "           concat(Logradouro, ' ' , Endereco,' ', Numero) as LOGRADOURO, Complemento, Cidade, Bairro, Uf, CEP "+
               " from "+
               " VW_DETALHES_PEDIDOS VWDP inner join ArquivosCargasDetalhes acd on acd.id_arquivocargastd = VWDP.id_arquivocargastd and acd.Cpf = VWDP.CPF"+
               " ORDER BY Id_Funcionario ";
               Query query = entityManager.createNativeQuery(sql, FuncionarioCargaBeneficio.class)
                                                  .setParameter("idCargaBeneficio", idCargaBeneficio)
                                                  .setParameter("listaIdFuncionariosPaginado", listaIdFuncionariosPaginado)
                                                  .setParameter("statusErroProcessamento", statusEmpresaCargaDetalheProduto);
               
               @SuppressWarnings("unchecked")
               List<FuncionarioCargaBeneficio> retorno = query.getResultList();

               return retorno;
          }
     }

     @Override
     public List<Integer> listaIdFuncionariosCarga(Long idCargaBeneficio,Integer statusEmpresaCargaDetalheProduto) {
          String sqlFuncionarios = "select func.Id_Funcionario as ID "
                     + "from empresasCargasDetalhesProdutos ecdp "
                     + "inner join funcionariosProdutos fp on fp.id_funcionariosProdutos = ecdp.id_funcionariosProdutos "
                     + "inner join contas ct on ct.id_conta = fp.id_conta "
                     + "inner join funcionarios func on func.id_funcionario = fp.id_funcionario "
                     + "inner join pessoasFisicas pf on pf.id_pessoaFisica = func.id_pessoaFisica "
                     + "inner join empresasCargasDetalhes ecd on ecd.Id_EmpresaCargaDetalhe = ecdp.Id_EmpresaCargaDetalhe "
                     + "inner join empresasCargas ec on ec.id_empresaCarga = ecd.id_empresaCarga "
                     + "where ec.id_cargaBeneficio = :idCargaBeneficio and ecdp.Status not in (:statusErroProcessamento)"
                     + "group by func.Id_Funcionario "
                     + "order by func.Id_Funcionario ";
          

          Query queryCargaBeneficio = (Query) entityManager.createNativeQuery(sqlFuncionarios)
                                                           .setParameter("idCargaBeneficio", idCargaBeneficio)
                                                           .setParameter("statusErroProcessamento", statusEmpresaCargaDetalheProduto);

          @SuppressWarnings("unchecked")
          List<Integer> retorno = queryCargaBeneficio.getResultList();

          return retorno;
     }

     @Override
     public List<CargaBeneficioProdutosCustom> listaCargaBeneficioPorProduto(PedidoDetalhesRequest arquivoCargaRequest) {
          String sql = "SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, "
                     + "EC.ID_CARGABENEFICIO, "
                     + "SUM(ECDP.VALOR) AS VALOR, "
                     + "P.ID_PRODUTO, "
                     + "P.NOME AS NOME_PRODUTO "
                     + "FROM ARQUIVOCARGASTD ACS "
                     + "INNER JOIN CARGASBENEFICIOS CB ON ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD "
                     + "INNER JOIN EMPRESASCARGAS EC ON EC.ID_CARGABENEFICIO = CB.ID_CARGABENEFICIO "
                     + "INNER JOIN EMPRESASCARGASDETALHES ECD ON ECD.ID_EMPRESACARGA = EC.ID_EMPRESACARGA "
                     + "INNER JOIN EMPRESASCARGASDETALHESPRODUTOS ECDP ON ECDP.ID_EMPRESACARGADETALHE = ECD.ID_EMPRESACARGADETALHE "
                     + "INNER JOIN FUNCIONARIOSPRODUTOS FP ON FP.ID_FUNCIONARIOSPRODUTOS = ECDP.ID_FUNCIONARIOSPRODUTOS "
                     + "INNER JOIN CONTAS C ON C.ID_CONTA = FP.ID_CONTA "
                     + "INNER JOIN PRODUTOS P ON P.ID_PRODUTO = C.ID_PRODUTO "
                     + "WHERE CB.ID_ARQUIVOCARGASTD = :idPedido "
                     + "AND CB.ID_EMPRESA IN :idEmpresasPermitidas "
                     + "AND ACS.ID_GRUPOEMPRESA = :idGrupoEmpresa "
                     + "AND ECDP.Status NOT IN (:statusErroProcessamento) "

                     + "GROUP BY EC.ID_CARGABENEFICIO, P.ID_PRODUTO, P.NOME "
                     + "ORDER BY ID_CARGABENEFICIO";
          Query query = entityManager.createNativeQuery(sql, CargaBeneficioProdutosCustom.class)
                    .setParameter("idPedido", arquivoCargaRequest.getIdPedido())
                    .setParameter("idEmpresasPermitidas", arquivoCargaRequest.getIdEmpresasPermitidas())
                    .setParameter("idGrupoEmpresa", arquivoCargaRequest.getIdGrupoEmpresa())
                    .setParameter("statusErroProcessamento", arquivoCargaRequest.getStatusEmpresaCargaDetalheProduto());
          
          @SuppressWarnings("unchecked")
          List<CargaBeneficioProdutosCustom> retorno = query.getResultList();

          return retorno;
     }

}
