
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaDetalhe;

public interface ArquivoUnidadeEntregaDetalheRepository extends JpaRepository<ArquivoUnidadeEntregaDetalhe, Long> {

     @Modifying
     void deleteByIdArquivoUE(Long idArquivoUnidadeUE);

     Optional<List<ArquivoUnidadeEntregaDetalhe>> findByIdArquivoUE(long idArquivoUnidadeEntrega);

     Optional<Long> countByIdArquivoUEAndStatus(Long idArquivoCargas, Integer status);
}
