
package br.com.conductor.rhblueapi.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class BeneficioCargaControleFinanceiroRepositoryImpl implements BeneficioCargaControleFinanceiroRepository {

     private static final String SPR_BENEFICIO_CARGACONTROLEFINANCEIRO = "SPR_Beneficio_CargaControleFinanceiro";

     private static final String STRING_SPACE = " ";

     @PersistenceContext
     private EntityManager em;

     @Transactional
     @Override
     public void atualizaControleFinanceiro(final Long idArquivoCarga) {

          final String proc = StringUtils.join("EXEC ", SPR_BENEFICIO_CARGACONTROLEFINANCEIRO, STRING_SPACE, idArquivoCarga);
          this.em.createNativeQuery(proc).executeUpdate();
     }

}
