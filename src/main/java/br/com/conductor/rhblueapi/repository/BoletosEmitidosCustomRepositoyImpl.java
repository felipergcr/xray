
package br.com.conductor.rhblueapi.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.conductor.rhblueapi.domain.BoletosEmitidos;

public class BoletosEmitidosCustomRepositoyImpl implements BoletosEmitidosCustomRepositoy {

     @PersistenceContext
     private EntityManager em;

     @Override
     public BoletosEmitidos findBoleto(Long idBoleto) {

          Query query;

          StringBuilder sql = new StringBuilder();

          sql.append("SELECT * FROM, ").append("BOLETOSEMITIDOS BE (NOLOCK), ")

                    .append("INNER JOIN TIPOBOLETO TB (NOLOCK), ")

                    .append("ON BE.ID_TIPOBOLETO = TB.ID_TIPOBOLETO, ")

                    .append("INNER JOIN EVENTOSEXTERNOSPAGAMENTOS EP (NOLOCK), ")

                    .append("ON BE.ID_EVENTOPAGAMENTO = EP.ID_EVENTOPAGAMENTO, ")

                    .append("WHERE BE.ID_BOLETO = :idBoleto");

          query = em.createNativeQuery(sql.toString(), BoletosEmitidos.class);

          query.setParameter("idBoleto", idBoleto);

          return (BoletosEmitidos) query.getResultList();

     }

}
