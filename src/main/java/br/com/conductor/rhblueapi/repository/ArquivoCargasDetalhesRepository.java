
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;

@Repository
public interface ArquivoCargasDetalhesRepository extends JpaRepository<ArquivosCargasDetalhes, Long> {

     Page<ArquivosCargasDetalhes> findByIdArquivoCargasAndStatusDetalhe(Long idArquivoCargas, Integer statusDetalhe, Pageable pageable);

     List<ArquivosCargasDetalhes> findByIdArquivoCargasAndStatusDetalhe(Long idArquivoPedido, Integer statusDetalhe);

     List<ArquivosCargasDetalhes> findByIdArquivoCargasAndStatusDetalheOrderByNumeroLinhaArquivo(Long idArquivoPedido, Integer statusDetalhe);

     @Modifying
     void deleteByIdArquivoCargas(Long idArquivoCargas);

     Long countByIdArquivoCargasAndStatusDetalheIn(Long id, List<Integer> status);
     
     Optional<Long> countByIdArquivoCargasAndStatusDetalhe(Long id, Integer status);

     Long countByIdArquivoCargas(Long id);
     
     Optional<List<ArquivosCargasDetalhes>> findByIdArquivoCargas(Long idArquivoCargas);
     
}
