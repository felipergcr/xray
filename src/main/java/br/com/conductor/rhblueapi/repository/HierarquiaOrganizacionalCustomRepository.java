
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacionalCustom;
import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacionalGrupoCustom;

public interface HierarquiaOrganizacionalCustomRepository {

	List<HierarquiaOrganizacionalCustom> buscaHierarquiaOrganizacional(Long idGrupoEmpresa, Long idSubgrupoEmpresa, Long idEmpresa);

     public List<HierarquiaOrganizacionalGrupoCustom> listarHierarquiaOrganizacionalGrupoCustom(final List<Integer> idsEmpresas);

}
