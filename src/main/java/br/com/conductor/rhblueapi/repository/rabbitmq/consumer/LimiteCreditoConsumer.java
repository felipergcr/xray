
package br.com.conductor.rhblueapi.repository.rabbitmq.consumer;

import java.io.IOException;
import java.util.List;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.pedido.CargaPedidoCustom;
import br.com.conductor.rhblueapi.service.usecase.GerenciadorCarga;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class LimiteCreditoConsumer extends RabbitMQConsumer {
     
     @Autowired
     private GerenciadorCarga gerenciadorCarga;
     
     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.LimiteCredito.QUEUE_VALIDACAO_DIARIA, concurrency = "${app.jms.concurrency.limite-credito.validacao-diaria}")
     public void consumerImportacaoConteudo(Message data) throws IOException {

          try {
               
               String response = new String(data.getBody());
               
               List<CargaPedidoCustom> cargasPedidos = jsonUtil.parseAsListOf(response, CargaPedidoCustom.class);

               gerenciadorCarga.gerenciaRegrasLimiteDiario(cargasPedidos);

          } catch (Exception ex) {
               throw ex;
          }

     }

}
