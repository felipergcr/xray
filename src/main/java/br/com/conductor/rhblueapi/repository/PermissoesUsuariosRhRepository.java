
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;

@Repository
public interface PermissoesUsuariosRhRepository extends JpaRepository<PermissoesUsuariosRh, Long> {
     
     Optional<PermissoesUsuariosRh> findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(Long idUsuario, Long idGrupoEmpresa, Integer status);

     @Query(value = "SELECT Empresas.ID_EMPRESA " + 
               "     FROM PERMISSOESUSUARIOSRH  " + 
               "     INNER JOIN USUARIOSNIVEISPERMISSOESRH ON USUARIOSNIVEISPERMISSOESRH.ID_PERMISSAO = PERMISSOESUSUARIOSRH.ID_PERMISSAO " + 
               "     INNER JOIN GRUPOSEMPRESAS GRUPOS ON GRUPOS.Id_GrupoEmpresa = USUARIOSNIVEISPERMISSOESRH.ID_GRUPOEMPRESA  " + 
               "     INNER JOIN GRUPOSEMPRESAS SUBGRUPOS ON GRUPOS.Id_GrupoEmpresa = SUBGRUPOS.Id_GrupoEmpresaPai " + 
               "     INNER JOIN Empresas ON Empresas.Id_GRUPOEMPRESA = SUBGRUPOS.ID_GRUPOEMPRESA " + 
               "     WHERE PERMISSOESUSUARIOSRH.ID_PERMISSAO = ?1 " + 
               "     AND PERMISSOESUSUARIOSRH.STATUS = 1 " +
               " UNION " + 
               " SELECT Empresas.ID_EMPRESA " + 
               "     FROM PERMISSOESUSUARIOSRH  " + 
               "     INNER JOIN USUARIOSNIVEISPERMISSOESRH ON USUARIOSNIVEISPERMISSOESRH.ID_PERMISSAO = PERMISSOESUSUARIOSRH.ID_PERMISSAO " + 
               "     INNER JOIN GRUPOSEMPRESAS SUBGRUPOS ON USUARIOSNIVEISPERMISSOESRH.ID_SUBGRUPOEMPRESA = SUBGRUPOS.Id_GrupoEmpresa " + 
               "     INNER JOIN Empresas ON Empresas.Id_GRUPOEMPRESA = SUBGRUPOS.ID_GRUPOEMPRESA " + 
               "     WHERE PERMISSOESUSUARIOSRH.ID_PERMISSAO = ?1 " + 
               "     AND SUBGRUPOS.Id_GrupoEmpresaPai IS NOT NULL " + 
               "     AND PERMISSOESUSUARIOSRH.STATUS = 1 " + 
               " UNION " + 
               " SELECT Empresas.Id_Empresa " + 
               "     FROM PERMISSOESUSUARIOSRH  " + 
               "     INNER JOIN USUARIOSNIVEISPERMISSOESRH ON USUARIOSNIVEISPERMISSOESRH.ID_PERMISSAO = PERMISSOESUSUARIOSRH.ID_PERMISSAO " + 
               "     INNER JOIN Empresas ON Empresas.Id_EMPRESA = USUARIOSNIVEISPERMISSOESRH.ID_EMPRESA " + 
               "     WHERE PERMISSOESUSUARIOSRH.ID_PERMISSAO = ?1 " + 
               "     AND PERMISSOESUSUARIOSRH.STATUS = 1", nativeQuery = true)
     List<Integer> findEmpresasbyIdPermissao(Long idPermissao);
     
     @Query(value = "SELECT TOP 1 ID_PERMISSAO FROM PERMISSOESUSUARIOSRH WHERE ID_USUARIO = ?1 AND ID_GRUPOEMPRESA = ?2 AND STATUS = ?3 ORDER BY 1 DESC ", nativeQuery= true)
     Long findIdByIdUsuarioAndGrupoEmpresaIdAndStatus(Long idUsuario, Long idGrupoEmpresa, Integer status);
     
}
