package br.com.conductor.rhblueapi.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface PessoaCustomRepository extends RepositoryCustom {

     List<String> findCpfCnpjByIdPermissao(Long idPermissao);

}
