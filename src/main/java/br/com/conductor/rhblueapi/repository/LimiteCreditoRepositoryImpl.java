
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.LimiteCredito.CreditoUtilizadoCustom;
import br.com.conductor.rhblueapi.util.AppConstantes;

@Repository
public class LimiteCreditoRepositoryImpl implements LimiteCreditoRepository {

     @PersistenceContext
     private EntityManager entityManager;

     @Override
     public List<CreditoUtilizadoCustom> listarValoresCreditoUtilizado(Long idGrupoEmpresa, List<Integer> statusPagamento, List<Integer> statusCargaBeneficio) {

          StringBuilder sql = new StringBuilder();
          sql.append("with vw_CargasBeneficios as ");
          sql.append("( ");
          sql.append("   select cb.Id_CargaBeneficio, ccf.Id_Boleto ");
          sql.append("   from CargasBeneficios cb ");
          sql.append("   inner join ARQUIVOCARGASTD ac on ac.ID_ARQUIVOCARGASTD = cb.ID_ARQUIVOCARGASTD ");
          sql.append("   and ac.StatusPagamento NOT IN ( :statusPagamento ) ");
          sql.append("   and ac.ID_GRUPOEMPRESA = :idGrupoEmpresa ");
          sql.append("   inner join CargaControleFinanceiro ccf on ccf.Id_CargaControleFinanceiro = cb.Id_CargaControleFinanceiro ");
          sql.append("   where cb.Status NOT IN ( :statusCargaBeneficio ) ");
          sql.append(") ");
          sql.append("select ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, x.* ");
          sql.append("from ( ");
          sql.append("   select ");
          sql.append("'");
          sql.append(AppConstantes.LIMITE_CREDITO_VALOR_TOTAL_PEDIDOS_SOLICITADOS);
          sql.append("'");
          sql.append(" as Tipo, sum(ec.Valor) as Valor ");
          sql.append("   from vw_CargasBeneficios vwcb ");
          sql.append("   inner join EmpresasCargas ec on ec.Id_CargaBeneficio = vwcb.Id_CargaBeneficio ");
          sql.append("   UNION ");
          sql.append("   select ");
          sql.append("'");
          sql.append(AppConstantes.LIMITE_CREDITO_VALOR_TOTAL_PEDIDOS_PAGO);
          sql.append("'");
          sql.append(" as Tipo, sum(be.ValorBoleto) as Valor ");
          sql.append("   from vw_CargasBeneficios vwcb ");
          sql.append("   inner join BoletosEmitidos be on be.Id_Boleto = vwcb.Id_Boleto ");
          sql.append("   where be.Id_EventoPagamento is not null ");
          sql.append(") x ");

          Query query = entityManager.createNativeQuery(sql.toString(), CreditoUtilizadoCustom.class)
                    .setParameter("statusPagamento", statusPagamento)
                    .setParameter("idGrupoEmpresa", idGrupoEmpresa)
                    .setParameter("statusCargaBeneficio", statusCargaBeneficio);

          @SuppressWarnings("unchecked")
          List<CreditoUtilizadoCustom> retorno = query.getResultList();

          return retorno;
     }
}
