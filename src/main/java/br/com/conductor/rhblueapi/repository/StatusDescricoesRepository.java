
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

import br.com.conductor.rhblueapi.config.CacheConfig;
import br.com.conductor.rhblueapi.domain.StatusDescricoes;

public interface StatusDescricoesRepository extends CrudRepository<StatusDescricoes, Long> {

     @Cacheable(CacheConfig.LISTA_STATUS_DESCRICOES)
     @Override
     public List<StatusDescricoes> findAll();
     
     Optional<StatusDescricoes> findByCodigoStatusAndDescricao(Long codigo, String nome);
     
}
