
package br.com.conductor.rhblueapi.repository;

public interface EmpresasCargasCustomRepository extends RepositoryCustom {

     void updateTotalizaValorByIdEmpresaCarga(Long idEmpresaCarga);

     void updateTotalizacaoQtdNovosCartoesByIdEmpresaCarga(Long idEmpresaCarga);

}
