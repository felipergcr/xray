package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.ArquivoCargaFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.DadosPedidoFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.PedidoFinanceiroCustom;

@Repository
public class FinanceiroPedidosCustomRepositoryImpl implements FinanceiroPedidosCustomRepository {
     
     @PersistenceContext
     private EntityManager entityManager;
     
     @Value("${app.database.sqlserver.blue}")
     private String dataBaseBlue;
     
     @Value("${app.database.sqlserver.accesscontrol}")
     private String dataBaseAccessControl;
     
     @Override
     public Integer countFinanceiroPedidosPorPermissoes(Long idGrupoEmpresa, List<Integer> idsEmpresas, List<Integer>statusArquivoParaVisualizarRPS) {
          
          StringBuilder sql = new StringBuilder();
          Query query;
          
          sql.append("SELECT ")
          .append("COUNT(DISTINCT ACS.ID_ARQUIVOCARGASTD) ")
          .append(this.condicoesFinanceiroPorParametros());
          
          query = entityManager.createNativeQuery(sql.toString());
          
          query.setParameter("idGrupoEmpresa", idGrupoEmpresa);
          query.setParameter("idsEmpresas", idsEmpresas);
          query.setParameter("statusArquivoParaVisualizarRPS", statusArquivoParaVisualizarRPS);
          
          Integer count = (Integer) query.getSingleResult();
          
          return count;
     }
     
     @Override
     public List<ArquivoCargaFinanceiroCustom> buscarPedidosPorPermissoesPaginado(Long idGrupoEmpresa, List<Integer> idsEmpresas, List<Integer> statusArquivoParaVisualizarRPS,Integer page, Integer size) {

          Query query;
          StringBuilder sql = new StringBuilder();
          
          sql.append("SELECT ")
          .append("ACS.ID_ARQUIVOCARGASTD, ")
          .append("ACS.DATAIMPORTACAO ")
          .append(this.condicoesFinanceiroPorParametros())
          .append("group by ACS.ID_ARQUIVOCARGASTD, ACS.DATAIMPORTACAO ")
          .append(this.condicoesPedidoOrdenacao())
          .append("OFFSET (:page * :size) rows FETCH NEXT :size  rows only ");
          
          query = entityManager.createNativeQuery(sql.toString(), ArquivoCargaFinanceiroCustom.class);
          
          query.setParameter("idGrupoEmpresa", idGrupoEmpresa);
          query.setParameter("idsEmpresas", idsEmpresas);
          query.setParameter("statusArquivoParaVisualizarRPS", statusArquivoParaVisualizarRPS);
          query.setParameter("page", page);
          query.setParameter("size", size);
          
          @SuppressWarnings("unchecked")
          List<ArquivoCargaFinanceiroCustom> arquivosCargas = (List<ArquivoCargaFinanceiroCustom>) query.getResultList();
          
          return arquivosCargas;
     }
     
     @Override
     public List<PedidoFinanceiroCustom> buscarFinanceiroPorPedidosEPermissoes(Long idGrupoEmpresa, List<Integer> idsEmpresas, List<Long> idsArquivoCarga, List<Integer>statusArquivoParaVisualizarRPS) {
          
          Query query;
          StringBuilder sql = new StringBuilder();
          
          sql.append("SELECT ")         
          .append("ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, ")
          .append("ACS.ID_ARQUIVOCARGASTD, ")
          .append("CCF.ID_CARGACONTROLEFINANCEIRO, ")
          .append("CCF.DataEmissaoRPS, ")
          .append("EC.VALOR, ")
          .append("CCF.NumeroNF, ")
          .append("IsNull(U.NOME , 'Transferência de arquivo') AS NOME_BENEFICIARIO ")
          .append(this.condicoesFinanceiroPorParametros())
          .append("and acs.ID_ARQUIVOCARGASTD in (:idsArquivoCarga) ")
          .append(this.condicoesPedidoOrdenacao());
          
          query = entityManager.createNativeQuery(sql.toString(), PedidoFinanceiroCustom.class);
          
          query.setParameter("idGrupoEmpresa", idGrupoEmpresa);
          query.setParameter("idsEmpresas", idsEmpresas);
          query.setParameter("idsArquivoCarga", idsArquivoCarga);
          query.setParameter("statusArquivoParaVisualizarRPS",statusArquivoParaVisualizarRPS);
          
          @SuppressWarnings("unchecked")
          List<PedidoFinanceiroCustom> pedidosFinanceiro = (List<PedidoFinanceiroCustom>) query.getResultList();
          
          return pedidosFinanceiro;
     }
     
     private String condicoesFinanceiroPorParametros() {
          
          StringBuilder sql = new StringBuilder();
          
          sql.append("FROM ")
          .append(dataBaseBlue)
          .append(".ARQUIVOCARGASTD ACS ") 
          .append("INNER JOIN ").append(dataBaseBlue)
          .append(".CARGASBENEFICIOS CB ON (ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD) ")
          .append("LEFT JOIN ").append(dataBaseAccessControl)
          .append(".USUARIOS U ON (CB.ID_USUARIO = U.ID_USUARIO) ")
          .append("INNER JOIN ").append(dataBaseBlue)
          .append(".CARGACONTROLEFINANCEIRO CCF ON (CB.ID_CARGACONTROLEFINANCEIRO = CCF.ID_CARGACONTROLEFINANCEIRO) ")
          .append("INNER JOIN ").append(dataBaseBlue)
          .append(".EmpresasCargas EC ON (CB.ID_CARGABENEFICIO = EC.ID_CARGABENEFICIO) ")
          .append("INNER JOIN ").append(dataBaseBlue)
          .append(".EMPRESAS E ON (EC.ID_EMPRESA = E.ID_EMPRESA ) ")
          .append("INNER JOIN ").append(dataBaseBlue)
          .append(".PESSOAS P ON (E.ID_PESSOA= P.ID_PESSOAFISICA )")
          .append("WHERE ACS.ID_GRUPOEMPRESA = :idGrupoEmpresa ")
          .append("and acs.status in (:statusArquivoParaVisualizarRPS) ")
          .append("and e.id_empresa in (:idsEmpresas) ");
          
          return sql.toString();
     }
     
     private String condicoesPedidoOrdenacao() {
          
          StringBuilder sql = new StringBuilder();
          sql.append("order by ACS.ID_ARQUIVOCARGASTD desc ");
          
          return sql.toString();
     }

     @Override
     public List<DadosPedidoFinanceiroCustom> buscarDadosPedidoFinanceiro(List<Long> idsArquivoCarga, List<Integer>statusCargaBeneficioParaNaoVisualizarRPS) {
          
          Query query;
          StringBuilder sql = new StringBuilder();
          
          sql.append("SELECT ")
          .append("CB.ID_ARQUIVOCARGASTD, ")
          .append("SUM(EC.Valor) AS VALOR_TOTAL, ")
          .append("COUNT(distinct(CCF.Id_CargaControleFinanceiro)) AS QTD_NOTAS ")
          .append("FROM ")
          .append(dataBaseBlue)
          .append(".CARGASBENEFICIOS CB ")
          .append("INNER JOIN ").append(dataBaseBlue)
          .append(".EmpresasCargas EC ON (CB.Id_CargaBeneficio = EC.Id_CargaBeneficio) ")
          .append("INNER JOIN ").append(dataBaseBlue)
          .append(".CARGACONTROLEFINANCEIRO CCF ON (CB.Id_CargaControleFinanceiro = CCF.Id_CargaControleFinanceiro) ")
          .append("WHERE CB.ID_ARQUIVOCARGASTD in (:idsArquivoCarga) ")
          .append("AND CB.STATUS NOT IN (:statusCargaBeneficioParaNaoVisualizarRPS)" )
          .append("GROUP BY CB.ID_ARQUIVOCARGASTD ");
          
          query = entityManager.createNativeQuery(sql.toString(), DadosPedidoFinanceiroCustom.class);
          
          query.setParameter("idsArquivoCarga", idsArquivoCarga);
          query.setParameter("statusCargaBeneficioParaNaoVisualizarRPS", statusCargaBeneficioParaNaoVisualizarRPS);
          
          @SuppressWarnings("unchecked")
          List<DadosPedidoFinanceiroCustom> dadosPedidosFinanceiro = (List<DadosPedidoFinanceiroCustom>) query.getResultList();
          
          return dadosPedidosFinanceiro;
     }

}
