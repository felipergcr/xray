package br.com.conductor.rhblueapi.repository.view;

import br.com.conductor.rhblueapi.domain.view.ProdutoRps;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProdutoRpsRepository extends CrudRepository<ProdutoRps, Long> {

    List<ProdutoRps>  findByNumeroRps(Long numeroRps);
}
