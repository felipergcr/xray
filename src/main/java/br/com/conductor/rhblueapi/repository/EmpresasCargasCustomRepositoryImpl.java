
package br.com.conductor.rhblueapi.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
public class EmpresasCargasCustomRepositoryImpl implements EmpresasCargasCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;
     
     @Transactional
     @Override
     public void updateTotalizaValorByIdEmpresaCarga(Long idEmpresaCarga) {
          
          StringBuilder sql = new StringBuilder();
          Query query;
          
          
          sql.append("UPDATE EmpresasCargas ")
               .append("set Valor = (")
               .append("SELECT ")
               .append("sum(ValorTotal) ")
               .append("from EmpresasCargasDetalhes (NOLOCK) ")
               .append("where Id_EmpresaCarga = :idEmpresaCarga ")
               .append(")")
               .append("where Id_EmpresaCarga = :idEmpresaCarga ");
          
          query = entityManager.createNativeQuery(sql.toString());

          query.setParameter("idEmpresaCarga", idEmpresaCarga);

          query.executeUpdate();
          
     }

     @Transactional
     @Override
     public void updateTotalizacaoQtdNovosCartoesByIdEmpresaCarga(Long idEmpresaCarga) {

          StringBuilder sql = new StringBuilder();
          Query query;

          sql.append("UPDATE EmpresasCargas ")
               .append("set QtdeNovosCartoes = (")
               .append("SELECT ")
               .append("sum(CAST(ECDP.FlagNovoCartao AS INT)) ")
               .append("FROM EmpresasCargas EC (NOLOCK) ")
               .append("INNER JOIN EmpresasCargasDetalhes ECD (NOLOCK) ")
               .append("ON (EC.Id_EmpresaCarga = ECD.Id_EmpresaCarga) ")
               .append("INNER JOIN EmpresasCargasDetalhesProdutos ECDP (NOLOCK) ")
               .append("ON (ECD.Id_EmpresaCargaDetalhe = ECDP.Id_EmpresaCargaDetalhe) ")
               .append("where EC.Id_EmpresaCarga = :idEmpresaCarga ")
               .append(") where Id_EmpresaCarga = :idEmpresaCarga ");

          query = entityManager.createNativeQuery(sql.toString());

          query.setParameter("idEmpresaCarga", idEmpresaCarga);

          query.executeUpdate();
     }

}
