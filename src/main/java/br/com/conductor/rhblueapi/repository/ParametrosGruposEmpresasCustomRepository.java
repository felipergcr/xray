package br.com.conductor.rhblueapi.repository;

import java.util.List;

import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;

public interface ParametrosGruposEmpresasCustomRepository {
     
     List<ParametroGrupoEmpresa> findByIdGrupoEmpresaAndTipoParametroAndCodigo(Long idGrupoEmpresa, String tipoParametro, String codigo);
     
}
