
package br.com.conductor.rhblueapi.repository.deloitte;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.ERRO_BUSCAR_NOTA_FISCAL_DELOITTE;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.ERRO_GERAR_ACCESS_TOKEN_DELOITTE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.rhblueapi.domain.deloitte.AutenticacaoDeloitte;
import br.com.conductor.rhblueapi.domain.deloitte.NotaFiscalDeloitte;
import br.com.conductor.rhblueapi.domain.deloitte.NotaFiscalResponse;
import br.com.conductor.rhblueapi.exception.UnprocessableEntityCdt;
import br.com.conductor.rhblueapi.repository.utils.DeloitteUtils;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class DeloitteRest {

     @Autowired
     private DeloitteUtils deloitteUtils;

     @Autowired
     private RestTemplate restTemplate;

     private String geraAccessToken() {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(deloitteUtils.uriAutentication());
          HttpHeaders headers = deloitteUtils.incializaHeadersAutenticacao();
          HttpEntity<?> entity = new HttpEntity<>(deloitteUtils.identificacaoDeloitte(), headers);

          try {
               
               ResponseEntity<AutenticacaoDeloitte> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, entity, AutenticacaoDeloitte.class);
               
               AutenticacaoDeloitte autenticacaoDeloitte = response.getBody();
               
               UnprocessableEntityCdt.checkThrow(Boolean.FALSE.equals(autenticacaoDeloitte.isAuthenticated()), ERRO_GERAR_ACCESS_TOKEN_DELOITTE);
               
               return autenticacaoDeloitte.getAccessToken();
               
          } catch (HttpClientErrorException e) {
               throw new UnprocessableEntityCdt(ERRO_GERAR_ACCESS_TOKEN_DELOITTE.getMessage());
          }

     }
     
     public NotaFiscalResponse buscarNotaFiscal(Long idCargaControleFinanceiro) {
          
          String uri = String.format(deloitteUtils.uriDownloadNotaFiscal(), idCargaControleFinanceiro);
          
          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri);
          String accessToken = this.geraAccessToken();
          HttpHeaders headers = deloitteUtils.incializaHeadersBuscaNotaFiscal(accessToken);
          HttpEntity<?> entity = new HttpEntity<>(headers);
          
          try {
               
               ResponseEntity<NotaFiscalDeloitte> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, NotaFiscalDeloitte.class);
               
               NotaFiscalDeloitte notaFiscalDeloitte = response.getBody();
               
               UnprocessableEntityCdt.checkThrow(!response.getStatusCode().is2xxSuccessful() || Objeto.isBlank(notaFiscalDeloitte.getPdfFile()), ERRO_BUSCAR_NOTA_FISCAL_DELOITTE);
               
               return NotaFiscalResponse.builder().nomeArquivo(notaFiscalDeloitte.getNomeArquivo()).notaFiscal(notaFiscalDeloitte.getPdfFile()).build();
                              
          } catch (HttpClientErrorException e) {
               throw new UnprocessableEntityCdt(ERRO_BUSCAR_NOTA_FISCAL_DELOITTE.getMessage());
          }
          
     }

}
