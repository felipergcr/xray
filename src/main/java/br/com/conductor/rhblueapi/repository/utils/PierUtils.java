
package br.com.conductor.rhblueapi.repository.utils;

import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class PierUtils {

     @Value("${app.pier.host}")
     private String pierHost;

     @Value("${app.pier.basepath}")
     private String pierBasepath;
     
     @Value("${app.pier.accesstoken}")
     private String accessToken;

     public HttpHeaders initializeHeaders() {

          final HttpHeaders headers = new HttpHeaders();
          headers.setAccept(singletonList(APPLICATION_JSON_UTF8));
          headers.setContentType(APPLICATION_JSON_UTF8);
          headers.set("access_token", accessToken);
          return headers;
     }

     public StringBuilder initializePathBuilder() {

          final StringBuilder sb = new StringBuilder();
          sb.append(pierHost);
          sb.append(pierBasepath);
          return sb;
     }

}
