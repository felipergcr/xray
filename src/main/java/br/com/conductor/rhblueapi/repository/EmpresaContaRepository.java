
package br.com.conductor.rhblueapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.EmpresaConta;

@Repository
public interface EmpresaContaRepository extends JpaRepository<EmpresaConta, Long>{

     void deleteByIdEmpresa(Long idEmpresa);
}
