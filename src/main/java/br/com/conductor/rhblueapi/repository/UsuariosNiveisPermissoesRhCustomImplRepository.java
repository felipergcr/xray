package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.UsuariosNiveisPermissoesRh;

@Repository
public class UsuariosNiveisPermissoesRhCustomImplRepository implements UsuariosNiveisPermissoesRhCustomRepository {
     
     @PersistenceContext
     private EntityManager entityManager;

     @Override
     public List<UsuariosNiveisPermissoesRh> findByIdUsuarioAndIdGrupoEmpresaPaiAndOperacao(Long idUsuario, Long idGrupoEmpresaPai, String nomeOperacao) {
          
          Query query;
          StringBuilder sql = new StringBuilder();
          
          sql.append("select unprh.* from USUARIOSNIVEISPERMISSOESRH unprh ")
          .append("where unprh.id_grupoempresa = :idGrupoEmpresa ")
          .append("and unprh.id_permissao = ")
          .append("(select id_permissao from PERMISSOESUSUARIOSRH purh where id_permissao in ")
          .append("(select id_permissao from PERMISSOESOPERACOESRH porh where id_operacao = ")
          .append("(select id_operacao from operacoesacessos where nomeoperacao = :nomeOperacao ) and status = 1) " )
          .append("and purh.id_usuario = :idUsuario) ");
          
          query = entityManager.createNativeQuery(sql.toString(), UsuariosNiveisPermissoesRh.class);
          query.setParameter("idGrupoEmpresa", idGrupoEmpresaPai);
          query.setParameter("nomeOperacao", nomeOperacao);
          query.setParameter("idUsuario", idUsuario);
          
          @SuppressWarnings("unchecked")
          List<UsuariosNiveisPermissoesRh> response = (List<UsuariosNiveisPermissoesRh>) query.getResultList();

          return response;
     }
}
