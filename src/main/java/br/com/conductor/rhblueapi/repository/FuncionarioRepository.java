
package br.com.conductor.rhblueapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.Funcionario;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {
     
     Optional<Funcionario> findByIdPessoaFisicaAndIdEmpresa(Long idPessoaFisica, Long idEmpresa);
}
