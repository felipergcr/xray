
package br.com.conductor.rhblueapi.repository.rabbitmq.publish;

import java.util.UUID;

import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.PedidoUpload;
import br.com.conductor.rhblueapi.domain.pedido.MensagemGerarCarga;
import br.com.conductor.rhblueapi.domain.pedido.MensagemPedidoDetalhado;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class CargasPublish extends RabbitMQPublish {
     
     public void enviarArquivoPedidoImportacao(final PedidoUpload pedidoUpload) {

          String payload = jsonUtil.toJsonString(pedidoUpload);

          send(RabbitMQConstantes.Cargas.EXCHANGE_IMPORTACAO, RabbitMQConstantes.Cargas.ROUTING_KEY_IMPORTACAO, payload, UUID.randomUUID().toString());
          
     }
     
     public void enviarPedidoParaGerarDetalhes(final MensagemPedidoDetalhado mensagemPedidoDetalhado ) {
          
          String payload = jsonUtil.toJsonString(mensagemPedidoDetalhado);
          
          send(RabbitMQConstantes.Cargas.EXCHANGE_GERENCIA_DETALHES, RabbitMQConstantes.Cargas.ROUTING_KEY_GERENCIA_DETALHES, payload, UUID.randomUUID().toString());
     }
     
     public void enviarDetalhesParaGerarFuncionario(final MensagemPedidoDetalhado mensagemPedidoDetalhado ) {
          
          String payload = jsonUtil.toJsonString(mensagemPedidoDetalhado);
          
          send(RabbitMQConstantes.Cargas.EXCHANGE_GERENCIA_FUNCIONARIO, RabbitMQConstantes.Cargas.ROUTING_KEY_GERENCIA_FUNCIONARIO, payload, UUID.randomUUID().toString());
     }
     
     public void enviarDetalheParaGerarCarga(final MensagemGerarCarga mensagemGerarCarga) {
          
          String payload = jsonUtil.toJsonString(mensagemGerarCarga);

          send(RabbitMQConstantes.Cargas.EXCHANGE_GERA_CARGAS, RabbitMQConstantes.Cargas.ROUTING_KEY_GERA_CARGAS, payload, UUID.randomUUID().toString());
     }
     
     public void enviarMensagemFinalizarPedido(final MensagemGerarCarga mensagemGerarCarga, String uuid) {
          
          String payload = jsonUtil.toJsonString(mensagemGerarCarga);
          
          send(RabbitMQConstantes.Cargas.EXCHANGE_FINALIZA_PEDIDO, RabbitMQConstantes.Cargas.ROUTING_KEY_FINALIZA_PEDIDO, payload, uuid);
     }
}
