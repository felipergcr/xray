
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.GrupoEmpresaProduto;

@Repository
public interface GrupoEmpresaProdutoRepository extends JpaRepository<GrupoEmpresaProduto, Long> {
     
     public Optional<List<GrupoEmpresaProduto>> findByIdGrupoEmpresaAndDataDelecaoIsNull(Long idGrupoEmpresa);
}
