
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.ArquivoCargasBinario;

public interface ArquivoCargasBinarioRepository extends JpaRepository<ArquivoCargasBinario, Long> {

     List<ArquivoCargasBinario> findByArquivoCargasStatus(Integer status);

     Optional<ArquivoCargasBinario> findByArquivoCargas(ArquivoCargas arquivoCarga);

}
