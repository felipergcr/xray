
package br.com.conductor.rhblueapi.repository.rabbitmq.publish;

import java.util.UUID;

import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaDetalhe;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class UnidadesEntregaPublish extends RabbitMQPublish {

     public void enviarArquivoUnidadeEntregaImportacao(final Long idArquivoUnidadeEntrega) {

          send(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_IMPORTACAO, RabbitMQConstantes.UnidadesEntrega.ROUTING_KEY_IMPORTACAO, idArquivoUnidadeEntrega.toString(), UUID.randomUUID().toString());
     }

     public void enviarIdUnidadeValidacaoNegocio(final Long idArquivoUnidadeEntrega) {

          send(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_GERENCIA_DETALHES, RabbitMQConstantes.UnidadesEntrega.ROUTING_KEY_GERENCIA_DETALHES, idArquivoUnidadeEntrega.toString(), UUID.randomUUID().toString());
     }

     public void enviarDetalheEGerarUnidades(final ArquivoUnidadeEntregaDetalhe arquivoUnidadeEntregaDetalhe) {

          String payload = jsonUtil.toJsonString(arquivoUnidadeEntregaDetalhe);

          send(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_GERA_UNIDADES, RabbitMQConstantes.UnidadesEntrega.ROUTING_KEY_GERA_UNIDADES, payload, UUID.randomUUID().toString());
     }

}
