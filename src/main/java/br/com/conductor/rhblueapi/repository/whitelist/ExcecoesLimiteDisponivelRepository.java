
package br.com.conductor.rhblueapi.repository.whitelist;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.whitelist.ExcecoesLimiteDisponivel;

@Repository
public interface ExcecoesLimiteDisponivelRepository extends JpaRepository<ExcecoesLimiteDisponivel, Long> {

     public Optional<ExcecoesLimiteDisponivel> findTop1ByIdGrupoEmpresaAndStatus(long idGrupoEmpresa, int status);
}
