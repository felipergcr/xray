
package br.com.conductor.rhblueapi.repository.cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.TipoEndereco;
import br.com.conductor.rhblueapi.repository.TipoEnderecoRepository;

@Repository
public class TipoEnderecoCacheRepository {
     
     @Autowired
     private TipoEnderecoRepository tipoEnderecoRepository;
     
     @Cacheable("ALL_TIPOS_ENDERECOS")
     public List<TipoEndereco> findAll() {
          return tipoEnderecoRepository.findAll();
     }

}
