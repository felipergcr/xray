
package br.com.conductor.rhblueapi.repository.view;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.conductor.rhblueapi.domain.view.FuncionarioProdutoEmpresa;

public interface FuncionarioProdutoEmpresaRepository extends CrudRepository<FuncionarioProdutoEmpresa, Long> {

     @Query(value = "SELECT SUM(valor) FROM vw_FuncionarioProdutoEmpresa WHERE cpf = :cpf and status in (0,1,3,5) and produto_id = :produtoId and empresa_id = :empresaId and data_carga BETWEEN :start AND :end", nativeQuery = true)
     Optional<BigDecimal> findValor(@Param(value="cpf") String cpf, @Param(value="produtoId") Long produtoId, @Param(value="empresaId") Long empresaId, @Param(value="start") LocalDate start, @Param(value="end") LocalDate end);
}
