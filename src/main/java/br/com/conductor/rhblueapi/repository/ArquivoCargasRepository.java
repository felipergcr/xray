
package br.com.conductor.rhblueapi.repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;

@Repository
public interface ArquivoCargasRepository extends JpaRepository<ArquivoCargas, Long> {

     Page<ArquivoCargas> findAllByIdGrupoEmpresa(Long idGrupoEmpresa, Pageable pageable);

     ArquivoCargas findTopByStatusAndOrigemAndDataStatusLessThan(Integer statusEnum, String origem, LocalDateTime dataProcessamento);

     Optional<ArquivoCargas> findTopByStatusAndOrigem(Integer status, String origem);

     Optional<ArquivoCargas> findByStatusAndOrigemAndUuid(Integer status, String origem, String uuid);

     Optional<ArquivoCargas> findByUuid(String uuid);
     
     @Modifying(clearAutomatically = true)
     @Transactional
     @Query(value = "UPDATE ARQUIVOCARGASTD SET UUID = :uuid WHERE ID_ARQUIVOCARGASTD = (SELECT TOP 1 ID_ARQUIVOCARGASTD FROM ARQUIVOCARGASTD WHERE ORIGEM = :origem and STATUS = :status AND UUID IS NULL)", nativeQuery = true)
     int updateUuidForTopStatusAndOrigem(@Param("uuid") String uuid, @Param("origem") String origem, @Param("status") Integer status);

     @Modifying(clearAutomatically = true)
     @Transactional
     @Query(value = "UPDATE ARQUIVOCARGASTD SET UUID = :uuid, DATASTATUS = GETDATE() WHERE ID_ARQUIVOCARGASTD = :id AND STATUS = :status", nativeQuery = true)
     int updateUuid(@Param("uuid") String uuid, @Param("id") Long idArquivoCarga,@Param("status") Integer status);

     @Query(value = "select sum(try_convert(decimal(6,2), valor)) from ArquivosCargasdetalhes where id_arquivocargastd = :idArquivoCarga ", nativeQuery = true)
     BigDecimal selectValorEsperadoSomaTotalizacaoPedido(@Param("idArquivoCarga") Long idArquivoCarga);

     Page<ArquivoCargas> findAllByIdGrupoEmpresaAndIdUsuarioOrderByIdDesc(Long idGrupoEmpresa, Long idUsuario, Pageable pageable);

     Page<ArquivoCargas> findByIdGrupoEmpresaAndStatusOrderByIdDesc(Long idArquivoCarga, Integer status, Pageable pageable);
     
     @Modifying(clearAutomatically = true)
     @Transactional
     @Query(value = "UPDATE ARQUIVOCARGASTD SET UUID = :uuid WHERE ID_ARQUIVOCARGASTD = " +
     "(SELECT TOP 1 ID_ARQUIVOCARGASTD FROM ARQUIVOCARGASTD WHERE UUID IS NOT NULL "
     + "AND ORIGEM = :origem AND STATUS = :status AND DATASTATUS < :data)"
     , nativeQuery = true)
     int updateUuidParaArquivoRetomada(@Param("uuid") String uuid, @Param("status") Integer status, @Param("origem") String origem, @Param("data") String dataProcessamento);

     boolean existsByIdGrupoEmpresaAndStatusIsIn(Long idGrupoEmpresa, List<Integer> statuses);

     Optional<List<ArquivoCargas>> findByStatusPagamento(Integer status);
     
     @Modifying(clearAutomatically = true)
     @Transactional
     @Query(value = "update ARQUIVOCARGASTD set STATUSPAGAMENTO = :status where id_arquivocargastd in ( :numerosPedidos )", nativeQuery = true)
     void updateStatusPagamentoByIdArquivoCargasIn(@Param("status") Integer status, @Param("numerosPedidos") List<Long> numerosPedidos);
     
     @Modifying(clearAutomatically = true)
     @Transactional
     @Query(value = "update ARQUIVOCARGASTD set STATUSPAGAMENTO = :status where id_arquivocargastd = :numeroPedido ", nativeQuery = true)
     void updateStatusPagamentoByIdArquivoCargas(@Param("status") Integer status, @Param("numeroPedido") Long numeroPedido);

}
