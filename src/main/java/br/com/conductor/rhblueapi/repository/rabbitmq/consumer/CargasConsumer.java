
package br.com.conductor.rhblueapi.repository.rabbitmq.consumer;

import java.io.IOException;
import java.util.Objects;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.PedidoUpload;
import br.com.conductor.rhblueapi.domain.pedido.MensagemGerarCarga;
import br.com.conductor.rhblueapi.domain.pedido.MensagemPedidoDetalhado;
import br.com.conductor.rhblueapi.service.usecase.GerenciadorCarga;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class CargasConsumer extends RabbitMQConsumer {

     @Autowired
     private GerenciadorCarga gerenciadorCarga;
     
     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.Cargas.QUEUE_IMPORTACAO, concurrency = "${app.jms.concurrency.pedido.importacao}")
     public void consumerImportacaoConteudo(Message data) throws IOException {

          try {

               String response = new String(data.getBody());

               PedidoUpload pedidoUpload = jsonUtil.parseAs(response, PedidoUpload.class);

               gerenciadorCarga.gerenciaImportacaoArquivo(pedidoUpload);

          } catch (Exception ex) {
               throw ex;
          }

     }

     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.Cargas.QUEUE_GERENCIA_DETALHES, concurrency = "${app.jms.concurrency.pedido.gerencia.detalhes}")
     public void consumerGerenciarDetalhes(Message data) throws IOException {

          try {

               String response = new String(data.getBody());
               
               MensagemPedidoDetalhado mensagemPedidoDetalhado = jsonUtil.parseAs(response, MensagemPedidoDetalhado.class);

               gerenciadorCarga.gerenciaEDistribuiDetalhes(mensagemPedidoDetalhado);

          } catch (Exception ex) {
               throw ex;
          }

     }
     
     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.Cargas.QUEUE_GERENCIA_FUNCIONARIO, concurrency = "${app.jms.concurrency.pedido.gerencia.funcionario}")
     public void consumerGerenciarFuncionario(Message data) throws IOException {

          try {

               String response = new String(data.getBody());
               
               MensagemPedidoDetalhado mensagemPedidoDetalhado = jsonUtil.parseAs(response, MensagemPedidoDetalhado.class);
               
               gerenciadorCarga.gerenciaFuncionarios(mensagemPedidoDetalhado);

          } catch (Exception ex) {
               throw ex;
          }

     }

     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.Cargas.QUEUE_GERA_CARGAS, concurrency = "${app.jms.concurrency.pedido.gerencia.cargas}")
     public void consumerGeraCargas(Message data) throws IOException {

          try {

               String response = new String(data.getBody());

               MensagemGerarCarga mensagem = jsonUtil.parseAs(response, MensagemGerarCarga.class);

               gerenciadorCarga.processoCargasFuncionarios(mensagem);

          } catch (Exception ex) {
               throw ex;
          }
     }
     
     @RabbitListener(autoStartup = "${spring.rabbitmq.autoStartup}", queues = RabbitMQConstantes.Cargas.QUEUE_FINALIZA_PEDIDO, concurrency = "${app.jms.concurrency.pedido.gerencia.cargas}")
     public void consumerFinalizaPedido(Message data) throws IOException {

          try {

               String body = new String(data.getBody());
               
               MensagemGerarCarga mensagem = jsonUtil.parseAs(body, MensagemGerarCarga.class);
               
               String uuid = Objects.isNull(data.getMessageProperties().getMessageId()) ? null : data.getMessageProperties().getMessageId();

               gerenciadorCarga.finalizaPedido(mensagem, uuid);

          } catch (Exception ex) {
               throw ex;
          }
     }
}
