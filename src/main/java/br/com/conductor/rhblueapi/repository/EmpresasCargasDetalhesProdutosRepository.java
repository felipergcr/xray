
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.EmpresasCargasDetalhesProdutos;

public interface EmpresasCargasDetalhesProdutosRepository extends JpaRepository<EmpresasCargasDetalhesProdutos, Long> {

     List<EmpresasCargasDetalhesProdutos> findByIdEmpresaCargaDetalheIn(List<Long> idsEmpresaCargaDetalhes);
}
