package br.com.conductor.rhblueapi.repository.blue;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.rhblueapi.domain.blue.FeriadoResponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.repository.utils.BlueUtils;
import br.com.conductor.rhblueapi.util.AppConstantes;

@Repository
public class FeriadoBlueRestRepository {
     
     @Autowired
     private BlueUtils blueUtils;
     
     @Autowired
     private RestTemplate restTemplate;

     public List<FeriadoResponse> listarFeriados() {

          try {
          
               final List<FeriadoResponse> feriadoResponse = new ArrayList<>();
     
               final HttpHeaders headers = this.blueUtils.initializeHeaders();
               final HttpEntity<String> httpEntity = new HttpEntity<>(headers);
     
               final UriComponentsBuilder builder = fromUriString(this.blueUtils.initializePathBuilder().append(AppConstantes.PATH_FERIADOS).toString());
               final ResponseEntity<PageResponse<FeriadoResponse>> pageableResponse = this.restTemplate.exchange(builder.toUriString(), GET, httpEntity, new ParameterizedTypeReference<PageResponse<FeriadoResponse>>() {
               });
     
               final PageResponse<FeriadoResponse> response = pageableResponse.getBody();
               feriadoResponse.addAll(response.getContent());
     
               return feriadoResponse;
          
          } catch (Exception e) {
               return new ArrayList<>();
          }

     }

}
