
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.conductor.rhblueapi.domain.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Long> {

     Empresa findByPessoaDocumento(String cnpj);
     
     @Query(value = "SELECT e.Id_Empresa, e.NomeFantasia, e.Status, e.Descricao, e.Agencia, "
               + "e.Banco, e.numTelContato, e.dddTelContato, e.EMailContato, e.DtNascimentoContato, "
               + "e.CPFContato, e.NomeContato, e.dataContrato, e.Id_EmpresaPai, e.FlagMatriz, e.ContaCorrente, "
               + "e.DVContaCorrente, e.Id_GrupoEmpresa, e.DataCadastro, e.Id_Pessoa "
               + "FROM EmpresasContas ect "
               + "JOIN Empresas e on ect.Id_Empresa = e.Id_Empresa "
               + "e.DVContaCorrente, e.Id_GrupoEmpresa, e.DataCadastro, e.Id_Pessoa, p.CPF "
               + "FROM EmpresasContas ect (nolock)"
               + "JOIN Empresas e (nolock) ON ect.Id_Empresa = e.Id_Empresa "
               + "JOIN Pessoas p (nolock) ON p.Id_PessoaFisica = e.Id_Pessoa "
               + "WHERE ect.Id_Conta = :id", nativeQuery = true)
     List<Empresa> findEmpresa(@Param("id") Long id);
     
     List<Empresa> findByIdIn(List<Long> idEmpresa);   
     
     Page<Empresa> findByIdGrupoEmpresa(Long idGrupoEmpresa, Pageable pageable);
     
     Optional<Empresa> findByIdPessoa(Long idPessoa);
}
