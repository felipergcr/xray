
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.ArquivoCargasCustom;
import br.com.conductor.rhblueapi.domain.pedido.CargaPedidoCustom;
import br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.util.PageUtils;

@Repository
public class ArquivoCargasCustomRepositoryImpl implements ArquivoCargasCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;
     
     @Override
     public PageResponse<ArquivoCargasCustom> listaDePedido(ArquivoPedidoRequest arquivoPedidoRequest, Pageable pageable) {

          StringBuilder sql = new StringBuilder();
          
          sql.append("SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID , * FROM (")
          .append(" SELECT ACS.ID_ARQUIVOCARGASTD")
          .append(", ACS.NOMEARQUIVO")
          .append(", ACS.DATAPROCESSAMENTO")
          .append(", ACS.DATASTATUS")
          .append(", ACS.DATAIMPORTACAO")
          .append(", ACS.DATAAGENDAMENTO")
          .append(", SUM(EC.VALOR) as VALOR")
          .append(", ACS.STATUS")
          .append(", ACS.ID_GRUPOEMPRESA")
          .append(", ACS.MOTIVOERRO")
          .append(", ACS.ID_USUARIOREGISTRO")
          .append(", ACS.STATUSPAGAMENTO")
          .append(", ACS.FLAGFATURAMENTOCENTRALIZADO")
          .append(", (")
          .append("    select top 1 cb.DataAgendamento")
          .append("    from CargasBeneficios cb")
          .append("    where cb.ID_ARQUIVOCARGASTD = acs.id_arquivocargastd")
          .append("    order by cb.DataAgendamento")
          .append("   ) as menorDataAgendamento")
          .append(" FROM ARQUIVOCARGASTD ACS INNER JOIN CARGASBENEFICIOS CB ON ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD")
          .append(" INNER JOIN EMPRESASCARGAS EC ON EC.ID_CARGABENEFICIO = CB.ID_CARGABENEFICIO")
          .append(" WHERE ")
          .append(" ACS.ID_GRUPOEMPRESA = :idGrupoEmpresa ")
          .append(" AND EC.ID_EMPRESA IN (:idEmpresa) ")
          .append(" AND ((:idArquivoCarga IS NULL) OR (ACS.ID_ARQUIVOCARGASTD = :idArquivoCarga))")
          .append(" GROUP BY ACS.ID_ARQUIVOCARGASTD ,ACS.NOMEARQUIVO , ACS.DATAPROCESSAMENTO, ACS.DATASTATUS, ACS.DATAIMPORTACAO, ACS.DATAAGENDAMENTO , ACS.STATUS, ACS.ID_GRUPOEMPRESA, ACS.MOTIVOERRO, ACS.ID_USUARIOREGISTRO, ACS.STATUSPAGAMENTO, ACS.FLAGFATURAMENTOCENTRALIZADO")
          .append(" UNION ")
          .append(" SELECT ACS.ID_ARQUIVOCARGASTD")
          .append(", ACS.NOMEARQUIVO")
          .append(", ACS.DATAPROCESSAMENTO")
          .append(", ACS.DATASTATUS")
          .append(", ACS.DATAIMPORTACAO")
          .append(", ACS.DATAAGENDAMENTO")
          .append(", 0 as VALOR")
          .append(", ACS.STATUS")
          .append(", ACS.ID_GRUPOEMPRESA")
          .append(", ACS.MOTIVOERRO")
          .append(", ACS.ID_USUARIOREGISTRO")
          .append(", ACS.STATUSPAGAMENTO")
          .append(", ACS.FLAGFATURAMENTOCENTRALIZADO")
          .append(", (")
          .append("    select top 1 cb.DataAgendamento")
          .append("    from CargasBeneficios cb")
          .append("    where cb.ID_ARQUIVOCARGASTD = acs.id_arquivocargastd")
          .append("    order by cb.DataAgendamento")
          .append("   ) as menorDataAgendamento")
          .append(" FROM ARQUIVOCARGASTD ACS LEFT JOIN CARGASBENEFICIOS CB ON ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD ")
          .append(" WHERE  ACS.ID_GRUPOEMPRESA = :idGrupoEmpresa")
          .append(" AND CB.ID_CARGABENEFICIO IS NULL")
          .append(" AND ((:idArquivoCarga IS NULL) OR (ACS.ID_ARQUIVOCARGASTD = :idArquivoCarga))")
          .append(" GROUP BY ACS.ID_ARQUIVOCARGASTD ,ACS.NOMEARQUIVO , ACS.DATAPROCESSAMENTO, ACS.DATASTATUS, ACS.DATAIMPORTACAO, ACS.DATAAGENDAMENTO , ACS.STATUS, ACS.ID_GRUPOEMPRESA, ACS.MOTIVOERRO, ACS.ID_USUARIOREGISTRO, ACS.STATUSPAGAMENTO, ACS.FLAGFATURAMENTOCENTRALIZADO")
          .append(") as TABTEMP")
          .append(" ORDER BY 2 DESC")
          .append(" offset (:page * :size) rows FETCH NEXT :size rows only ");

          Integer count = countRegistros(arquivoPedidoRequest, pageable);

          PageResponse<ArquivoCargasCustom> pageResponse = new PageResponse<ArquivoCargasCustom>();
          pageResponse.setContent(constroiPesquisaInteira(sql, arquivoPedidoRequest, pageable));
          pageResponse = PageUtils.ajustarPageResponse(pageResponse, Integer.valueOf(pageable.getPageNumber()), Integer.valueOf(pageable.getPageSize()), count);

          return pageResponse;
     }

     private Integer countRegistros(ArquivoPedidoRequest arquivoPedidoRequest, Pageable pageable) { 
          
          StringBuilder str = new StringBuilder();
          
          str.append("SELECT COUNT (*) from (")
          .append(" SELECT ACS.ID_ARQUIVOCARGASTD")
          .append(", ACS.NOMEARQUIVO")
          .append(", ACS.DATAPROCESSAMENTO")
          .append(", ACS.DATASTATUS")
          .append(", ACS.DATAIMPORTACAO")
          .append(", ACS.DATAAGENDAMENTO")
          .append(", SUM(EC.VALOR) as VALOR")
          .append(", ACS.STATUS")
          .append(", ACS.ID_GRUPOEMPRESA")
          .append(", ACS.MOTIVOERRO")
          .append(", ACS.ID_USUARIOREGISTRO")
          .append(" FROM ARQUIVOCARGASTD ACS INNER JOIN CARGASBENEFICIOS CB ON ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD")
          .append(" INNER JOIN EMPRESASCARGAS EC ON EC.ID_CARGABENEFICIO = CB.ID_CARGABENEFICIO")
          .append(" WHERE ")
          .append(" ACS.ID_GRUPOEMPRESA = :idGrupoEmpresa ")
          .append(" AND EC.ID_EMPRESA IN (:idEmpresa) ")
          .append(" AND ((:idArquivoCarga IS NULL) OR (ACS.ID_ARQUIVOCARGASTD = :idArquivoCarga))")
          .append(" GROUP BY ACS.ID_ARQUIVOCARGASTD ,ACS.NOMEARQUIVO , ACS.DATAPROCESSAMENTO, ACS.DATASTATUS, ACS.DATAIMPORTACAO, ACS.DATAAGENDAMENTO , ACS.STATUS, ACS.ID_GRUPOEMPRESA, ACS.MOTIVOERRO, ACS.ID_USUARIOREGISTRO")
          .append(" UNION ")
          .append(" SELECT ACS.ID_ARQUIVOCARGASTD")
          .append(", ACS.NOMEARQUIVO")
          .append(", ACS.DATAPROCESSAMENTO")
          .append(", ACS.DATASTATUS")
          .append(", ACS.DATAIMPORTACAO")
          .append(", ACS.DATAAGENDAMENTO")
          .append(", 0 as VALOR")
          .append(", ACS.STATUS")
          .append(", ACS.ID_GRUPOEMPRESA")
          .append(", ACS.MOTIVOERRO")
          .append(", ACS.ID_USUARIOREGISTRO")
          .append(" FROM ARQUIVOCARGASTD ACS LEFT JOIN CARGASBENEFICIOS CB ON ACS.ID_ARQUIVOCARGASTD = CB.ID_ARQUIVOCARGASTD ")
          .append(" WHERE  ACS.ID_GRUPOEMPRESA = :idGrupoEmpresa")
          .append(" AND CB.ID_CARGABENEFICIO IS NULL")
          .append(" AND ((:idArquivoCarga IS NULL) OR (ACS.ID_ARQUIVOCARGASTD = :idArquivoCarga))")
          .append(" GROUP BY ACS.ID_ARQUIVOCARGASTD ,ACS.NOMEARQUIVO , ACS.DATAPROCESSAMENTO, ACS.DATASTATUS, ACS.DATAIMPORTACAO, ACS.DATAAGENDAMENTO , ACS.STATUS, ACS.ID_GRUPOEMPRESA, ACS.MOTIVOERRO, ACS.ID_USUARIOREGISTRO")
          .append(") as TABTEMP");

          return constroiPesquisaCount(str, arquivoPedidoRequest, pageable);
     }

     private List<ArquivoCargasCustom> constroiPesquisaInteira(StringBuilder sql, ArquivoPedidoRequest arquivoPedidoRequest, Pageable pageable) {

          Query query = (Query) entityManager.createNativeQuery(sql.toString(), ArquivoCargasCustom.class);

          query = parametrizarConsulta(arquivoPedidoRequest, pageable, query, true);

          @SuppressWarnings("unchecked")
          List<ArquivoCargasCustom> retorno = (List<ArquivoCargasCustom>) query.getResultList();

          return retorno;

     }

     private Integer constroiPesquisaCount(StringBuilder sql, ArquivoPedidoRequest arquivoPedidoRequest, Pageable pageable) {

          Query query = (Query) entityManager.createNativeQuery(sql.toString());

          query = parametrizarConsulta(arquivoPedidoRequest, pageable, query, false);

          Integer resultList = (Integer) query.getSingleResult();

          return resultList;

     }

     private Query parametrizarConsulta(ArquivoPedidoRequest arquivoPedidoRequest, Pageable pageable, Query query, Boolean paging) {
          
          query.setParameter("idArquivoCarga", arquivoPedidoRequest.getIdArquivoCarga());
          query.setParameter("idEmpresa", arquivoPedidoRequest.getIdEmpresasPermitidas());         
          query.setParameter("idGrupoEmpresa", arquivoPedidoRequest.getIdGrupoEmpresa());

          if (paging) {
               if (!String.valueOf(pageable.getPageNumber()).isEmpty()) {
                    query.setParameter("page", pageable.getPageNumber());
               }
               if (!String.valueOf(pageable.getPageSize()).isEmpty()) {
                    query.setParameter("size", pageable.getPageSize());
               }
          }

          return query;
     }
     
     
     public List<CargaPedidoCustom> buscarCargasPedidosPorStatusPagamento(List<Integer> statusPagamento) {
          
          StringBuilder sql = new StringBuilder();
          
          sql.append("select acs.ID_ARQUIVOCARGASTD as NUMERO_PEDIDO, ")
          .append("acs.StatusPagamento as STATUS_PAGAMENTO_PEDIDO, ")
          .append("acs.ID_GRUPOEMPRESA as ID_GRUPOEMPRESA, ")
          .append("sum(ec.valor) as VALOR_CARGA ")
          .append("from arquivocargastd acs ")
          .append("inner join cargasbeneficios cb on acs.id_Arquivocargastd = cb.id_Arquivocargastd ")
          .append("inner join empresascargas ec on cb.id_cargabeneficio = ec.id_cargabeneficio ")
          .append("where acs.StatusPagamento IN (:statusPagamento) ")
          .append("group by acs.ID_ARQUIVOCARGASTD, acs.ID_GRUPOEMPRESA, acs.StatusPagamento ");
          
          Query query = entityManager.createNativeQuery(sql.toString(), CargaPedidoCustom.class)
                    .setParameter("statusPagamento", statusPagamento);

          @SuppressWarnings("unchecked")
          List<CargaPedidoCustom> retorno = query.getResultList();
          
          return retorno;
     }
     
}
