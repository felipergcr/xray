
package br.com.conductor.rhblueapi.repository.rabbitmq.publish.registrarPendenciaFinanceira;

import java.util.UUID;

import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.request.registroBoleto.RegistroBoletoRequest;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.RabbitMQPublish;
import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Component
public class RegistrarBoletoPublisher extends RabbitMQPublish {

     public void enviarDadosParaRegistrarBoleto(RegistroBoletoRequest registroBoletoRequest) {

          String payload = jsonUtil.toJsonString(registroBoletoRequest);

          send(RabbitMQConstantes.RegistrarBoleto.EXCHANGE_REGISTRAR_BOLETO, RabbitMQConstantes.RegistrarBoleto.ROUTING_KEY_REGISTRAR_BOLETO, payload, UUID.randomUUID().toString());
     }

}
