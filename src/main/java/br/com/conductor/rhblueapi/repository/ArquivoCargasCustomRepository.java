
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import br.com.conductor.rhblueapi.domain.ArquivoCargasCustom;
import br.com.conductor.rhblueapi.domain.pedido.CargaPedidoCustom;
import br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest;
import br.com.conductor.rhblueapi.domain.response.PageResponse;

public interface ArquivoCargasCustomRepository extends Repository<ArquivoCargasCustom, Long> {

     PageResponse<ArquivoCargasCustom> listaDePedido(ArquivoPedidoRequest arquivoPedidoRequest, Pageable pageable);

     public List<CargaPedidoCustom> buscarCargasPedidosPorStatusPagamento(List<Integer> statusPagamento);

}
