package br.com.conductor.rhblueapi.repository;

import br.com.conductor.rhblueapi.domain.LogEventos;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * LogEventosRepository
 */
public interface LogEventosRepository extends JpaRepository<LogEventos, Long> {

}
