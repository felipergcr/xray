package br.com.conductor.rhblueapi.repository;

import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.CANCELADO_PARCIAL;
import static br.com.conductor.rhblueapi.enums.StatusPagamentoFinanceiroEnum.ATIVO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS;
import static br.com.conductor.rhblueapi.enums.TipoStatus.BOLETOS_EMITIDOS;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.FormasPagamento.TED_DOC;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;

@Repository
public class NotificacaoPendenciaTedCustomRepositoryImpl implements NotificacaoPendenciaTedCustomRepository {
     
     @PersistenceContext
     private EntityManager entityManager;
     
     @Autowired
     private StatusCached statusCached;
     
     private List<StatusDescricao> dominioStatusCargas;

     private String statusArquivoCarga() {

          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(ARQUIVO_CARGAS.getNome());

          final StatusDescricao statusProcessamentoConcluido = mapStatusDescricao(dominioStatusCargas, PROCESSAMENTO_CONCLUIDO.getStatus());
          final StatusDescricao statusProcessamentoCancelamentoParcial = mapStatusDescricao(dominioStatusCargas, CANCELADO_PARCIAL.getStatus());

          return Arrays.asList(statusProcessamentoConcluido.getStatus(), statusProcessamentoCancelamentoParcial.getStatus()).stream().map(String::valueOf).collect(Collectors.joining(","));

     }
     
     private Integer statusPagamentoAtivo() {
          dominioStatusCargas = this.statusCached.busca(BOLETOS_EMITIDOS.getNome());
          final StatusDescricao statusAtivo = mapStatusDescricao(dominioStatusCargas, ATIVO.name());
          return statusAtivo.getStatus();
     }
     
     public List<Integer> buscaPendenciasTedCentralizado(Long idGrupoEmpresa){
          
          StringBuilder sql = new StringBuilder();
          Query query;
          
          sql.append(this.condicoesPendenciaTedPorParametros())
          .append(this.condicoesCentralizadoPorParametros(idGrupoEmpresa));
          
          query = entityManager.createNativeQuery(sql.toString());
          query.setParameter("idGrupoEmpresa", idGrupoEmpresa);
          
          @SuppressWarnings("unchecked")
          List<Integer> idsPedidos = (List<Integer>) query.getResultList();
          
          return idsPedidos;
     }
     
     public List<Integer> buscaPendenciasTedDescentralizado(List<Integer> idsEmpresas){
          
          StringBuilder sql = new StringBuilder();
          Query query;
          
          sql.append(this.condicoesPendenciaTedPorParametros())
          .append(this.condicoesDescentralizadoPorParametros(idsEmpresas));
          
          query = entityManager.createNativeQuery(sql.toString());
          query.setParameter("idsEmpresas", idsEmpresas);
          
          @SuppressWarnings("unchecked")
          List<Integer> idsPedidos = (List<Integer>) query.getResultList();
          
          return idsPedidos;
     }
     
     private String condicoesPendenciaTedPorParametros() {

          StringBuilder sql = new StringBuilder();

          sql.append("SELECT ")
          .append("distinct(acd.ID_ARQUIVOCARGASTD) ")
          .append("FROM ")
          .append("empresas E ")
          .append("INNER JOIN EmpresasCargas EC (nolock) ")
          .append("ON (E.Id_Empresa = EC.Id_Empresa) ")
          .append("INNER JOIN CargasBeneficios CB (nolock) ")
          .append("ON (CB.Id_CargaBeneficio = EC.Id_CargaBeneficio) ")
          .append("INNER JOIN ARQUIVOCARGASTD ACD (nolock) ")
          .append("ON (acd.ID_ARQUIVOCARGASTD = cb.ID_ARQUIVOCARGASTD) ")
          .append("INNER JOIN CargaControleFinanceiro ccf (nolock) ")
          .append("ON (CB.Id_CargaControleFinanceiro = ccf.Id_CargaControleFinanceiro)")
          .append("INNER JOIN BoletosEmitidos BE (nolock) ")
          .append("ON (BE.Id_Boleto = ccf.Id_Boleto) ")
          .append("INNER JOIN TipoBoleto TB (nolock) ")
          .append("ON (TB.Id_TipoBoleto =  BE.Id_TipoBoleto) ")
          .append("WHERE ")
          .append("ACD.STATUS IN ( ")
          .append(this.statusArquivoCarga())
          .append(" ) ")
          .append("AND TB.Descricao = '")
          .append(TED_DOC.getChave()).append("' ")
          .append("AND BE.StatusParcela = ")
          .append(this.statusPagamentoAtivo()).append(" ")
          .append("AND BE.Id_EventoPagamento is null ");

          return sql.toString();
     }
     
     private String condicoesCentralizadoPorParametros(Long idGrupoEmpresa) {
          
          StringBuilder sql = new StringBuilder();
          
          sql.append("AND ACD.ID_GRUPOEMPRESA = :idGrupoEmpresa ");
          
          return sql.toString();
     }
     
     private String condicoesDescentralizadoPorParametros(List<Integer> idsEmpresas) {
          
          StringBuilder sql = new StringBuilder();
          
          sql.append("AND E.Id_Empresa in(:idsEmpresas) ");
                    
          return sql.toString();
     }

}
