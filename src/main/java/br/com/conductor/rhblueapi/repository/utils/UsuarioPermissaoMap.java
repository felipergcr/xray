
package br.com.conductor.rhblueapi.repository.utils;

import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.response.UsuarioPermissaoResponse;
import org.modelmapper.PropertyMap;

public class UsuarioPermissaoMap {

     public final Response response = new Response();

     public class Response extends PropertyMap<UsuarioResponse, UsuarioPermissaoResponse> {

          @Override
          protected void configure() {

               map().setIdUsuario(source.getId());
          }

     }

}
