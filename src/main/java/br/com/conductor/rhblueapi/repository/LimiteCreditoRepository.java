
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import br.com.conductor.rhblueapi.domain.LimiteCredito.CreditoUtilizadoCustom;

public interface LimiteCreditoRepository {

     public List<CreditoUtilizadoCustom> listarValoresCreditoUtilizado(final Long idGrupoEmpresa, List<Integer> statusPagamento, List<Integer> statusCargaBeneficio);
}
