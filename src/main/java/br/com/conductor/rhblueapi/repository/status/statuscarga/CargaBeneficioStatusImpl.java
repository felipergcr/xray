package br.com.conductor.rhblueapi.repository.status.statuscarga;

import static br.com.conductor.rhblueapi.util.DataUtils.localDateParaStringyyyyMMdd;
import static br.com.conductor.rhblueapi.util.DataUtils.localDateTimeParaStringyyyyMMddHHmmss;
import static br.com.conductor.rhblueapi.util.DataUtils.localDateTimeToLocalDateTimeFirstMoment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.status.CargaBeneficioMinimoCustom;

@Repository
public class CargaBeneficioStatusImpl implements CargaBeneficioStatus {
     
     @PersistenceContext
     private EntityManager entityManager;

     @SuppressWarnings("unchecked")
     @Override
     public List<CargaBeneficioMinimoCustom> buscaCargasPagas(Integer status, LocalDate dataMovimento) {

          StringBuilder sql = new StringBuilder();

          Query query;
          
          sql.append("WITH VW_CARGASBENEFICIOSSTATUS as ")
          .append("( ")
          .append(this.consultaPadrao())
          .append(this.joinsPago())
          .append(this.condicoesPago())
          .append(") ")
          .append("SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, x.* FROM ")
          .append("( ")
          .append("SELECT * from VW_CARGASBENEFICIOSSTATUS ")
          .append("UNION ")
          .append(this.consultaPadrao())
          .append(this.condicoesUnion())
          .append(") x")
          ;
          
          query = entityManager.createNativeQuery(sql.toString(), CargaBeneficioMinimoCustom.class);

          query.setParameter("dataMovimento", localDateParaStringyyyyMMdd(dataMovimento));
          query.setParameter("statusPagamento", status);

          return (List<CargaBeneficioMinimoCustom>) query.getResultList();
          
     }
     
     @SuppressWarnings("unchecked")
     @Override
     public List<CargaBeneficioMinimoCustom> buscaCargasCanceladas(Integer status, LocalDateTime dataHoraAtual, LocalDateTime dataHoraAtualMenos2Horas) {

          StringBuilder sql = new StringBuilder();     

          Query query;
          
          sql.append("WITH VW_CARGASBENEFICIOSSTATUS as ")
          .append("( ")
          .append(this.consultaPadrao())
          .append(this.condicoesCancelado())
          .append(") ")
          .append("SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, x.* FROM ")
          .append("( ")
          .append("SELECT * from VW_CARGASBENEFICIOSSTATUS ")
          .append("UNION ")
          .append(this.consultaPadrao())
          .append(this.condicoesUnion())
          .append(") x")
          ;
          
          query = entityManager.createNativeQuery(sql.toString(), CargaBeneficioMinimoCustom.class);
          
          query.setParameter("statusCancelado", status);
          query.setParameter("dataHoraAtualMenos2Horas", localDateTimeParaStringyyyyMMddHHmmss(dataHoraAtualMenos2Horas));
          query.setParameter("dataHoraAtual", localDateTimeParaStringyyyyMMddHHmmss(dataHoraAtual));

          return (List<CargaBeneficioMinimoCustom>) query.getResultList();
     }
     
     @SuppressWarnings("unchecked")
     @Override
     public List<CargaBeneficioMinimoCustom> buscaCargasExpiradas(Integer status, LocalDateTime dataHoraAtual, LocalDateTime dataAtualMenosUmDia) {

          StringBuilder sql = new StringBuilder();     

          Query query;
          
          sql.append("WITH VW_CARGASBENEFICIOSSTATUS as ")
          .append("( ")
          .append(this.consultaPadrao())
          .append(this.condicoesExpirado())
          .append(") ")
          .append("SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, x.* FROM ")
          .append("( ")
          .append("SELECT * from VW_CARGASBENEFICIOSSTATUS ")
          .append("UNION ")
          .append(this.consultaPadrao())
          .append(this.condicoesUnion())
          .append(") x")
          ;
          
          query = entityManager.createNativeQuery(sql.toString(), CargaBeneficioMinimoCustom.class);
          
          query.setParameter("statusExpirado", status);
          query.setParameter("dataAtualMenosUmDia", localDateTimeToLocalDateTimeFirstMoment(dataAtualMenosUmDia));
          query.setParameter("dataHoraAtual", localDateTimeParaStringyyyyMMddHHmmss(dataHoraAtual));
          
          return (List<CargaBeneficioMinimoCustom>) query.getResultList();
          
     }
     
     private String consultaPadrao() {
          
          return new StringBuilder()
                    .append("SELECT ")
                    .append("CB.ID_CARGABENEFICIO, CB.ID_ARQUIVOCARGASTD, CB.STATUS, CB.ID_GRUPOEMPRESA ") 
                    .append("FROM CARGASBENEFICIOS CB (NOLOCK) ")
                    .toString();
     }
     
     private String condicoesPago() {
          
          return new StringBuilder()
                    .append("WHERE CB.STATUS = :statusPagamento ")
                    .append("AND EEP.DATAMOVIMENTO = :dataMovimento ")
                    .toString();
     }
     
     private String joinsPago() {
          
          return new StringBuilder()
                    .append("INNER JOIN CARGACONTROLEFINANCEIRO CCF (NOLOCK) ")
                    .append("ON (CB.Id_CargaControleFinanceiro = CCF.Id_CargaControleFinanceiro) ")
                    .append("INNER JOIN BOLETOSEMITIDOS BE (NOLOCK) ")
                    .append("ON (CCF.Id_Boleto = BE.ID_BOLETO) ")
                    .append("INNER JOIN EVENTOSEXTERNOSPAGAMENTOS EEP (NOLOCK) ")
                    .append("ON (BE.ID_EVENTOPAGAMENTO = EEP.ID_EVENTOPAGAMENTO) ")
                    .toString();
     }
     
     private String condicoesCancelado() {
          
          return new StringBuilder()
                    .append("WHERE CB.STATUS =  :statusCancelado ")
                    .append("AND DATACANCELAMENTO BETWEEN :dataHoraAtualMenos2Horas AND :dataHoraAtual")
                    .toString();
     }
     
     private String condicoesExpirado() {
          
          return new StringBuilder()
                    .append("WHERE CB.STATUS =  :statusExpirado ")
                    .append("AND DATACANCELAMENTO BETWEEN :dataAtualMenosUmDia AND :dataHoraAtual")
                    .toString();
     }
     
     private String condicoesUnion() {
          
          return new StringBuilder()
                    .append("where ID_ARQUIVOCARGASTD in (SELECT ID_ARQUIVOCARGASTD from VW_CARGASBENEFICIOSSTATUS) ")
                    .toString();
     }
}
