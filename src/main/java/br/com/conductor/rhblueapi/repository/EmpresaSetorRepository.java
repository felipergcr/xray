
package br.com.conductor.rhblueapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.EmpresaSetor;

@Repository
public interface EmpresaSetorRepository extends JpaRepository<EmpresaSetor, Long> {

     Optional<EmpresaSetor> findByIdEmpresaAndDescricao(Long idEmpresa, String descricao);

}
