
package br.com.conductor.rhblueapi.repository;

import br.com.conductor.rhblueapi.domain.BoletosEmitidos;

public interface BoletosEmitidosCustomRepositoy {

     BoletosEmitidos findBoleto(Long idBoleto);
}
