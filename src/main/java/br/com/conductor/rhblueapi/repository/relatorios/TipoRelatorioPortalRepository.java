
package br.com.conductor.rhblueapi.repository.relatorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.relatorios.TipoRelatorioPortal;

@Repository
public interface TipoRelatorioPortalRepository extends JpaRepository<TipoRelatorioPortal, Long> {
     
     TipoRelatorioPortal findByIdPlataformaAndDescricaoTipoRelatorio(Integer idPlataforma, String descricao);

}
