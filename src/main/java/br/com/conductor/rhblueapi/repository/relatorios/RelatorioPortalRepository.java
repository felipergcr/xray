
package br.com.conductor.rhblueapi.repository.relatorios;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.relatorios.RelatorioPortal;

@Repository
public interface RelatorioPortalRepository extends JpaRepository<RelatorioPortal, Long> {
     
     Page<RelatorioPortal> findByDataInicioAndDataFim(LocalDate dataInicio, LocalDate dataFim, Pageable pageable);
     
     List<RelatorioPortal> findByIdInAndIdUsuarioAndIdGrupoEmpresa(List<Long> idsRelatorio, Long idUsuario, Long idGrupoEmpresa);

     Optional<RelatorioPortal> findByIdAndIdUsuarioAndIdGrupoEmpresa(Long id, Long idUsuario, Long idGrupoEmpresa);

}
