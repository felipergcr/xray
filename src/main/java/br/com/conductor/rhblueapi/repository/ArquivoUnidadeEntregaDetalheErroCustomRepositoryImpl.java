
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.ArquivoPedidoDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.response.ArquivoUnidadeEntregaDetalheErroReponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.util.PageUtils;

@Repository
public class ArquivoUnidadeEntregaDetalheErroCustomRepositoryImpl implements ArquivoUnidadeEntregaDetalheErroCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;

     @Override
     public PageResponse<ArquivoUnidadeEntregaDetalheErroReponse> findByIdArquivoUnidadeEntregaWithPageable(Long idArquivoUnidadeEntrega, Pageable pageable) {

          Query query;
          StringBuilder sql = new StringBuilder();

          sql.append("SELECT ")
          .append("ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, ")
          .append("aued.NumeroLinhaArquivo AS linha, auede.Motivo AS erro ")
          .append("from ArquivosUnidadeEntregaDetalheErro auede ")
          .append("inner join ArquivosUnidadeEntregaDetalhe aued on aued.Id_ArquivoUEDetalhe = auede.Id_ArquivoUEDetalhe ")
          .append("inner join ArquivosUnidadeEntrega aue on (aue.Id_ArquivoUnidadeEntrega = aued.Id_ArquivoUnidadeEntrega) ")
          .append("where aue.Id_ArquivoUnidadeEntrega = :idArquivoUnidadeEntrega order by aued.NumeroLinhaArquivo ")
          .append(" offset (:page * :size) rows FETCH NEXT :size rows only ");

          query = entityManager.createNativeQuery(sql.toString(), ArquivoPedidoDetalhesErrosReponse.class);
          query.setParameter("idArquivoUnidadeEntrega", idArquivoUnidadeEntrega);
          query.setParameter("page", pageable.getPageNumber());
          query.setParameter("size", pageable.getPageSize());

          Integer count = contadorRegistros(idArquivoUnidadeEntrega);

          @SuppressWarnings("unchecked")
          List<ArquivoUnidadeEntregaDetalheErroReponse> detalhesErros = (List<ArquivoUnidadeEntregaDetalheErroReponse>) query.getResultList();

          PageResponse<ArquivoUnidadeEntregaDetalheErroReponse> pageResponse = new PageResponse<ArquivoUnidadeEntregaDetalheErroReponse>();
          pageResponse.setContent(detalhesErros);
          pageResponse = PageUtils.ajustarPageResponse(pageResponse, Integer.valueOf(pageable.getPageNumber()), Integer.valueOf(pageable.getPageSize()), count);

          return pageResponse;
     }

     private Integer contadorRegistros(Long idArquivoUnidadeEntrega) {

          Query query;
          StringBuilder sql = new StringBuilder();

          sql.append("SELECT ")
          .append("count(aued.NumeroLinhaArquivo) ")
          .append("from ArquivosUnidadeEntregaDetalheErro auede ")
          .append("inner join ArquivosUnidadeEntregaDetalhe aued on (aued.Id_ArquivoUEDetalhe = auede.Id_ArquivoUEDetalhe) ")
          .append("inner join ArquivosUnidadeEntrega aue on (aue.Id_ArquivoUnidadeEntrega = aued.Id_ArquivoUnidadeEntrega) ")
          .append("where aued.Id_ArquivoUnidadeEntrega = :idArquivoUnidadeEntrega ");

          query = entityManager.createNativeQuery(sql.toString());
          query.setParameter("idArquivoUnidadeEntrega", idArquivoUnidadeEntrega);

          return (Integer) query.getSingleResult();
     }

}
