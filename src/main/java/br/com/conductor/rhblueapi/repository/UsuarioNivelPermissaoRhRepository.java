
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;

@Repository
public interface UsuarioNivelPermissaoRhRepository extends JpaRepository<UsuarioPermissaoNivelAcessoRh, Long> {

     Optional<List<UsuarioPermissaoNivelAcessoRh>> findByIdPermissao(Long idPermissao);

     Optional<UsuarioPermissaoNivelAcessoRh> findByIdPermissaoAndIdGrupoEmpresaAndStatus(Long idPermissao, Long idGrupoEmpresa, Integer status);

     Optional<UsuarioPermissaoNivelAcessoRh> findByIdPermissaoAndIdSubgrupoEmpresaAndStatus(Long idPermissao, Long idSubgrupoEmpresa, Integer status);

     Optional<UsuarioPermissaoNivelAcessoRh> findByIdPermissaoAndIdEmpresaAndStatus(Long idPermissao, Long idEmpresa, Integer status);

     List<UsuarioPermissaoNivelAcessoRh> findByIdPermissaoAndStatus(Long idPermissao, Integer Status);

     Optional<UsuarioPermissaoNivelAcessoRh> findByIdEmpresa(Long idEmpresa);
}
