
package br.com.conductor.rhblueapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaBinario;

public interface ArquivoUnidadeEntregaBinarioRepository extends JpaRepository<ArquivoUnidadeEntregaBinario, Long> {

     Optional<ArquivoUnidadeEntregaBinario> findByIdArquivoUnidadeEntrega(Long idArquivoUnidadeEntrega);
}
