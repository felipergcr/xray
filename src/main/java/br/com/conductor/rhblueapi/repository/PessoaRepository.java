
package br.com.conductor.rhblueapi.repository;

import br.com.conductor.rhblueapi.domain.PessoaEmpresa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PessoaRepository extends JpaRepository<PessoaEmpresa, Long> {

     Optional<PessoaEmpresa> findByDocumento(String cpf);
}