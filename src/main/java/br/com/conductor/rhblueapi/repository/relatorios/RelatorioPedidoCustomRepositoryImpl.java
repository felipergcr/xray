
package br.com.conductor.rhblueapi.repository.relatorios;

import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS;
import static br.com.conductor.rhblueapi.enums.TipoStatus.EMPRESAS_CARGAS_DETALHES_PRODUTOS;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.DataUtils.localDateToLocalDateTimeFirstMoment;
import static br.com.conductor.rhblueapi.util.DataUtils.localDateToLocalDateTimeLastMoment;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.relatorios.RelatorioPedidoCustom;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;
import br.com.conductor.rhblueapi.enums.StatusEmpresaCargaDetalheProdutoEnum;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;

@Repository
public class RelatorioPedidoCustomRepositoryImpl {

     @PersistenceContext
     private EntityManager entityManager;

     @Autowired
     private StatusCached statusCached;

     private String viewPedidosFiltro() {
          return new StringBuilder("select ID_ARQUIVOCARGASTD ")
                           .append("from ARQUIVOCARGASTD (NOLOCK) ")
                           .append("where ID_GRUPOEMPRESA = :idGrupoEmpresa ")
                           .append("and STATUS not in (:listaStatusNaoUtilizados) ")
                           .append("and DATAPROCESSAMENTO BETWEEN :dataInicio and :dataFim ").toString();
     }

     private String queryFormaPagamento() {
          return new StringBuilder("select Id_GrupoEmpresa, ")
                           .append("       Valor as Prazo_Pagamento ")
                           .append("from ParametrosGruposEmpresas (NOLOCK) ")
                           .append("where Id_Parametro in ( ")
                           .append("                       select Id_Parametro ")
                           .append("                       from Parametros ")
                           .append("                       where TipoParametro = 'RH' and Codigo = 'RH_PrazoPagamento' ")
                           .append("                      ) ")
                           .toString();
     }
     
     private String viewPedidosCompleto() {

          return new StringBuilder("select ac.ID_ARQUIVOCARGASTD, ")
                           .append("       ac.DATAPROCESSAMENTO as Dt_Pedido, ")
                           .append("       ParametrosGruposEmpresas_PrazoPagamento.Prazo_Pagamento, ")
                           .append("       ac.TipoPagamentoPedido as Modalidade_Pagamento, ")
                           .append("       sum(ec.QtdeNovosCartoes) as Qtd_Cartoes_Novos, ")
                           .append("       sum(ec.Valor) as Valor_Pedido, ")
                           .append("       ac.StatusPagamento ")
                           .append("from ARQUIVOCARGASTD ac (NOLOCK) ")
                           .append("inner join Vw_Pedidos_Filtro vwp on vwp.ID_ARQUIVOCARGASTD = ac.ID_ARQUIVOCARGASTD ")
                           .append("inner join cargasbeneficios cb (NOLOCK) on cb.ID_ARQUIVOCARGASTD = ac.ID_ARQUIVOCARGASTD ")
                           .append("inner join EmpresasCargas ec (NOLOCK) on ec.Id_CargaBeneficio = cb.Id_CargaBeneficio ")
                           .append("inner join ( ")
                           .append(this.queryFormaPagamento())
                           .append(") ParametrosGruposEmpresas_PrazoPagamento on ParametrosGruposEmpresas_PrazoPagamento.Id_GrupoEmpresa = ac.ID_GRUPOEMPRESA ")
                           .append("group by ac.ID_ARQUIVOCARGASTD, ac.DATAPROCESSAMENTO, ParametrosGruposEmpresas_PrazoPagamento.Prazo_Pagamento, ac.TipoPagamentoPedido, ac.StatusPagamento ")
                           .toString();
     }

     private String queryQtdFuncSolicitado() {

          return new StringBuilder("select acd.ID_ARQUIVOCARGASTD, ")
                           .append("       e.Id_Empresa, ")
                           .append("       count(acd.Id_DetalheArquivo) as Qtd_Func_Solicitado, ")
                           .append("       convert(smallint, acd.Produto) as Id_Produto ")
                           .append("from ArquivosCargasDetalhes acd (NOLOCK) ")
                           .append("inner join Vw_Pedidos_Filtro vwp on vwp.ID_ARQUIVOCARGASTD = acd.ID_ARQUIVOCARGASTD ")
                           .append("inner join Pessoas p (NOLOCK) on p.CPF = acd.Cnpj ")
                           .append("inner join Empresas e (NOLOCK) on e.Id_Pessoa = p.Id_PessoaFisica ")
                           .append("group by acd.ID_ARQUIVOCARGASTD, e.Id_Empresa, acd.Produto ")
                           .toString();
     }

     private String queryQtdFuncCreditado() {

          return new StringBuilder("select cb.ID_ARQUIVOCARGASTD, ")
                           .append("       ec.Id_Empresa, ")
                           .append("       count(ecdp.Id_EmpresaCargaDetalheProduto) as Qtd_Func_Creditado, ")
                           .append("       c.Id_Produto ")
                           .append("from CargasBeneficios cb (NOLOCK) ")
                           .append("inner join Vw_Pedidos_Filtro vwp on vwp.ID_ARQUIVOCARGASTD = cb.ID_ARQUIVOCARGASTD ")
                           .append("inner join EmpresasCargas ec (NOLOCK) on ec.Id_CargaBeneficio = cb.Id_CargaBeneficio ")
                           .append("inner join EmpresasCargasDetalhes ecd (NOLOCK) on ecd.Id_EmpresaCarga = ec.Id_EmpresaCarga ")
                           .append("inner join EmpresasCargasDetalhesProdutos ecdp (NOLOCK) on ecdp.Id_EmpresaCargaDetalhe = ecd.Id_EmpresaCargaDetalhe ")
                           .append("inner join FuncionariosProdutos fp (NOLOCK) on fp.Id_FuncionariosProdutos = ecdp.Id_FuncionariosProdutos ")
                           .append("inner join Contas (NOLOCK) c on c.Id_Conta = fp.Id_Conta ")
                           .append("where ecdp.Status = :statusProcessadoEmpresasCargasDetalhesProdutos ")
                           .append("group by cb.ID_ARQUIVOCARGASTD, ec.Id_Empresa, c.Id_Produto ")
                           .toString();
     }

     private String viewQtdFuncionarios() {

          return new StringBuilder("select solicitados.ID_ARQUIVOCARGASTD, ")
                           .append("       solicitados.Id_Empresa, ")
                           .append("       solicitados.Id_Produto, ")
                           .append("       solicitados.Qtd_Func_Solicitado, ")
                           .append("       creditados.Qtd_Func_Creditado ")
                           .append("from ( ")
                           .append(this.queryQtdFuncSolicitado())
                           .append(") solicitados ")
                           .append("left join ( ")
                           .append(this.queryQtdFuncCreditado())
                           .append(") creditados on creditados.ID_ARQUIVOCARGASTD = solicitados.ID_ARQUIVOCARGASTD and creditados.Id_Empresa = solicitados.Id_Empresa and creditados.Id_Produto = solicitados.Id_Produto ")
                           .toString();
     }

     private String colunasRelacionadaACargasBeneficios() {

          return new StringBuilder("cb.ID_ARQUIVOCARGASTD as Id_Pedido, ")
                           .append("cb.Id_Empresa, ")
                           .append("p.CPF as Cnpj, ")
                           .append("e.Descricao as Razao_Social, ")
                           .append("grupo.Id_GrupoEmpresa as Id_Grupo, ")
                           .append("grupo.Nome as Nome_Grupo, ")
                           .append("subgrupo.Id_GrupoEmpresa as Id_SubGrupo, ")
                           .append("subgrupo.Nome as Nome_SubGrupo, ")
                           .append("cb.status as Status_CargasBeneficios, ")
                           .append("cb.DataAgendamento as Dt_Credito_Agendada, ")
                           .append("cb.DataProcessamentoCarga as dt_credito_efetiva, ")
                           .append("be.DataVencimento as Dt_Vencimento, ")
                           .append("be.ValorBoleto as Valor_Total_Pagamento, ")
                           .append("eep.DataPagamento as Dt_Pagamento, ")
                           .append("tb.Descricao as Descricao_Tipo_Boleto, ")
                           .toString();
     }

     private String colunasRelacionadaAViewPedidosCompleto() {

          return new StringBuilder("vwpc.Dt_Pedido, ")
                           .append("vwpc.Prazo_Pagamento, ")
                           .append("vwpc.Modalidade_Pagamento, ")
                           .append("vwpc.Qtd_Cartoes_Novos, ")
                           .append("vwpc.Valor_Pedido, ")
                           .append("vwpc.StatusPagamento as Status_Pagamento_ARQUIVOCARGASTD, ")
                           .toString();
     }

     private String colunasRelacionadaAViewQtdFuncionarios() {

          return new StringBuilder("vwqf.Qtd_Func_Solicitado, ")
                           .append("vwqf.Qtd_Func_Creditado, ")
                           .append("vwqf.Id_Produto ")
                           .toString();
     }

     private String tabelasRelacionamentoPrincipal() {

          return new StringBuilder("from CargasBeneficios cb (NOLOCK) ")
                           .append("inner join Empresas e (NOLOCK) on e.Id_Empresa = cb.Id_Empresa ")
                           .append("inner join Pessoas p (NOLOCK) on p.Id_PessoaFisica = e.Id_Pessoa ")
                           .append("inner join GruposEmpresas grupo (NOLOCK) on grupo.Id_GrupoEmpresa = cb.Id_GrupoEmpresa ")
                           .append("inner join GruposEmpresas subgrupo (NOLOCK) on subgrupo.Id_GrupoEmpresaPai = cb.Id_GrupoEmpresa ")
                           .append("                                               and subgrupo.Id_EmpresaPrincipal = e.Id_Empresa ")
                           .append("inner join CargaControleFinanceiro ccf (NOLOCK) on ccf.Id_CargaControleFinanceiro = cb.Id_CargaControleFinanceiro ")
                           .append("inner join BoletosEmitidos be (NOLOCK) on be.Id_Boleto = ccf.Id_Boleto ")
                           .append("inner join TipoBoleto tb (NOLOCK) on tb.Id_TipoBoleto = be.Id_TipoBoleto ")
                           .append("left join EventosExternosPagamentos eep (NOLOCK) on eep.Id_EventoPagamento = be.Id_EventoPagamento ")
                           .append("inner join Vw_Pedidos_Completo vwpc on vwpc.ID_ARQUIVOCARGASTD = cb.ID_ARQUIVOCARGASTD ")
                           .append("inner join Vw_Qtd_Func vwqf on vwqf.ID_ARQUIVOCARGASTD = cb.ID_ARQUIVOCARGASTD and vwqf.Id_Empresa = cb.Id_Empresa ")
                           .toString();
     }

     private String queryPrincipal() {

          return new StringBuilder("with Vw_Pedidos_Filtro as ")
                    .append("( ")
                    .append(this.viewPedidosFiltro())
                    .append("), Vw_Pedidos_Completo as ")
                    .append("( ")
                    .append(this.viewPedidosCompleto())
                    .append("), Vw_Qtd_Func as ")
                    .append("( ")
                    .append(this.viewQtdFuncionarios())
                    .append(") ")
                    .append("select ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS Id, ")
                    .append(this.colunasRelacionadaACargasBeneficios())
                    .append(this.colunasRelacionadaAViewPedidosCompleto())
                    .append(this.colunasRelacionadaAViewQtdFuncionarios())
                    .append(this.tabelasRelacionamentoPrincipal())
                    .toString();
     }

     private List<Integer> listaStatusNaoUtilizado() {

          List<StatusDescricao> dominioStatusArquivoCargaStd = this.statusCached.busca(ARQUIVO_CARGAS.getNome());
          final StatusDescricao statusRecebido = mapStatusDescricao(dominioStatusArquivoCargaStd, StatusArquivoEnum.RECEBIDO.getStatus());
          final StatusDescricao statusImportado = mapStatusDescricao(dominioStatusArquivoCargaStd, StatusArquivoEnum.IMPORTADO.getStatus());
          final StatusDescricao statusInvalidado = mapStatusDescricao(dominioStatusArquivoCargaStd, StatusArquivoEnum.INVALIDADO.getStatus());

          return Arrays.asList(statusRecebido.getStatus(), statusImportado.getStatus(), statusInvalidado.getStatus());
     }

     private Integer statusProcessadoEmpresasCargasDetalhesProdutos() {

          List<StatusDescricao> dominioStatusEmpresasCargasDetalhesProdutos = this.statusCached.busca(EMPRESAS_CARGAS_DETALHES_PRODUTOS.getNome());
          final StatusDescricao statusProcessado = mapStatusDescricao(dominioStatusEmpresasCargasDetalhesProdutos, StatusEmpresaCargaDetalheProdutoEnum.PROCESSADO.getStatus());

          return statusProcessado.getStatus();
     }

     @SuppressWarnings("unchecked")
     public List<RelatorioPedidoCustom> findAllRelatorioPedidoCustom(final Long idGrupoEmpresa, final LocalDate dataInicio, final LocalDate dataFim) {

          String sql = this.queryPrincipal();

          Query query;

          query = entityManager.createNativeQuery(sql, RelatorioPedidoCustom.class);

          query.setParameter("idGrupoEmpresa", idGrupoEmpresa);
          query.setParameter("listaStatusNaoUtilizados", this.listaStatusNaoUtilizado());
          query.setParameter("dataInicio", localDateToLocalDateTimeFirstMoment(dataInicio));
          query.setParameter("dataFim", localDateToLocalDateTimeLastMoment(dataFim));
          query.setParameter("statusProcessadoEmpresasCargasDetalhesProdutos", this.statusProcessadoEmpresasCargasDetalhesProdutos());

          return (List<RelatorioPedidoCustom>) query.getResultList();
     }

}
