
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.RegistroBoletoCustom;

@Repository
public class RegistroBoletoCustomRepositoryImpl {

     @PersistenceContext
     private EntityManager entityManager;

     public List<RegistroBoletoCustom> listarPorPedido(final Long idArquivoCarga) {

          StringBuilder sql = new StringBuilder();
          Query query;

          sql.append(getSelect()).append(getColunaIdentificador()).append(getColunasListarPorPedido()).append(getTabelasListarPorPedido()).append("where ac.ID_ARQUIVOCARGASTD = :idArquivoCarga ");

          query = (Query) entityManager.createNativeQuery(sql.toString(), RegistroBoletoCustom.class);

          query.setParameter("idArquivoCarga", idArquivoCarga);

          return (List<RegistroBoletoCustom>) query.getResultList();

     }

     private String getSelect() {

          return new StringBuilder("select ").toString();
     }

     private String getColunaIdentificador() {

          return new StringBuilder("ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, ").toString();
     }

     private String getColunasListarPorPedido() {

          return new StringBuilder("be.Id_Boleto, ")
                    .append("       e.Bairro as Pagador_Bairro, ")
                    .append("       e.CEP as Pagador_Cep, ")
                    .append("       e.Cidade as Pagador_Cidade, ")
                    .append("       e.NomeLogradouro as Pagador_NomeLogradouro, ")
                    .append("       e.NumeroEndereco as Pagador_NumeroEndereco, ")
                    .append("       e.ComplementoEndereco as Pagador_ComplementoEndereco, ")
                    .append("       p.Nome as Pagador_Nome, ")
                    .append("       p.CPF as Pagador_Cnpj, ")
                    .append("       e.UF as Pagador_Uf, ")
                    .append("       be.NossoNumero as Titulo_NossoNumero, ")
                    .append("       be.ValorBoleto, ")
                    .append("       be.DataEmissao, ")
                    .append("       cb.DataAgendamento ")
                    .toString();
     }

     private String getTabelasListarPorPedido() {

          return new StringBuilder("from ARQUIVOCARGASTD ac (NOLOCK) ")
                    .append("       inner join CargasBeneficios cb (NOLOCK) on cb.ID_ARQUIVOCARGASTD = ac.ID_ARQUIVOCARGASTD ")
                    .append("       inner join CargaControleFinanceiro ccf (NOLOCK) on ccf.Id_CargaControleFinanceiro = cb.Id_CargaControleFinanceiro ")
                    .append("       inner join BoletosEmitidos be (NOLOCK) on be.Id_Boleto = ccf.Id_Boleto ")
                    .append("       inner join Empresas emp (NOLOCK) on emp.Id_Empresa = cb.Id_Empresa ")
                    .append("       inner join Pessoas p (NOLOCK) on p.Id_PessoaFisica = emp.Id_Pessoa ")
                    .append("       inner join Enderecos e (NOLOCK) on e.Id_PessoaFisica = p.Id_PessoaFisica ").toString();
     }

}
