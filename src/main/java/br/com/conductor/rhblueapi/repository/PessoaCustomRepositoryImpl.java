
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository
public class PessoaCustomRepositoryImpl implements PessoaCustomRepository {

     @PersistenceContext
     private EntityManager entityManager;

     @Override
     public List<String> findCpfCnpjByIdPermissao(Long idPermissao) {
          
          Query query;
          StringBuilder sql = new StringBuilder();
          
          sql.append("SELECT Pessoas.CPF ")
          .append("FROM PERMISSOESUSUARIOSRH ")
          .append("INNER JOIN USUARIOSNIVEISPERMISSOESRH ON USUARIOSNIVEISPERMISSOESRH.ID_PERMISSAO = PERMISSOESUSUARIOSRH.ID_PERMISSAO ")
          .append("INNER JOIN GRUPOSEMPRESAS GRUPOS ON GRUPOS.Id_GrupoEmpresa = USUARIOSNIVEISPERMISSOESRH.ID_GRUPOEMPRESA ")
          .append("INNER JOIN GRUPOSEMPRESAS SUBGRUPOS ON GRUPOS.Id_GrupoEmpresa = SUBGRUPOS.Id_GrupoEmpresaPai ")
          .append("INNER JOIN Empresas ON Empresas.Id_GRUPOEMPRESA = SUBGRUPOS.ID_GRUPOEMPRESA ")
          .append("INNER JOIN Pessoas ON Pessoas.Id_PessoaFisica = Empresas.Id_Pessoa ")
          .append("WHERE PERMISSOESUSUARIOSRH.ID_PERMISSAO = :idPermissao ")
          .append("AND PERMISSOESUSUARIOSRH.STATUS = 1 ")
          .append("UNION ")
          .append("SELECT Pessoas.CPF ")
          .append("FROM PERMISSOESUSUARIOSRH ")
          .append("INNER JOIN USUARIOSNIVEISPERMISSOESRH ON USUARIOSNIVEISPERMISSOESRH.ID_PERMISSAO = PERMISSOESUSUARIOSRH.ID_PERMISSAO ")
          .append("INNER JOIN GRUPOSEMPRESAS SUBGRUPOS ON USUARIOSNIVEISPERMISSOESRH.ID_SUBGRUPOEMPRESA = SUBGRUPOS.Id_GrupoEmpresa ")
          .append("INNER JOIN Empresas ON Empresas.Id_GRUPOEMPRESA = SUBGRUPOS.ID_GRUPOEMPRESA ")
          .append("INNER JOIN Pessoas ON Pessoas.Id_PessoaFisica = Empresas.Id_Pessoa ")
          .append("WHERE PERMISSOESUSUARIOSRH.ID_PERMISSAO = :idPermissao ")
          .append("AND SUBGRUPOS.Id_GrupoEmpresaPai IS NOT NULL ")
          .append("AND PERMISSOESUSUARIOSRH.STATUS = 1 ")
          .append("UNION ")
          .append("SELECT Pessoas.CPF ")
          .append("FROM PERMISSOESUSUARIOSRH ")
          .append("INNER JOIN USUARIOSNIVEISPERMISSOESRH ON USUARIOSNIVEISPERMISSOESRH.ID_PERMISSAO = PERMISSOESUSUARIOSRH.ID_PERMISSAO ")
          .append("INNER JOIN Empresas ON Empresas.Id_EMPRESA = USUARIOSNIVEISPERMISSOESRH.ID_EMPRESA ")
          .append("INNER JOIN Pessoas ON Pessoas.Id_PessoaFisica = Empresas.Id_Pessoa ")
          .append("WHERE PERMISSOESUSUARIOSRH.ID_PERMISSAO = :idPermissao ")
          .append("AND PERMISSOESUSUARIOSRH.STATUS = 1");
          
          query = entityManager.createNativeQuery(sql.toString());
          
          query.setParameter("idPermissao", idPermissao);

          @SuppressWarnings("unchecked")
          List<String> listaCnpjs = (List<String>) query.getResultList();

          return listaCnpjs;
     }

}
