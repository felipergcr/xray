
package br.com.conductor.rhblueapi.repository;

import java.time.format.DateTimeFormatter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.proc.BeneficioFuncionarioProdutoResponse;

@Repository
public class BeneficioFuncionarioRepositoryImpl implements BeneficioFuncionarioRepository {
     
     DateTimeFormatter FORMAT_OUTPUT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
     DateTimeFormatter FORMAT_INPUT = DateTimeFormatter.ofPattern("dd/MM/yyyy");

     private static final String COMMA = ",";
     private static final String SPACE = " "; 
     
     private static final String SPR_BENEFICIO_INCLUIR_FUNCIONARIO_PRODUTO = "SPR_Beneficio_IncluirFuncionarioProduto";
     
     @PersistenceContext
     private EntityManager em;

     @Override
     public BeneficioFuncionarioProdutoResponse atualizaFuncionarioProduto(final Long idProduto, final Long idFuncionario) {

          final StringBuilder procBuilder = new StringBuilder("EXEC");
          procBuilder.append(SPACE).append(SPR_BENEFICIO_INCLUIR_FUNCIONARIO_PRODUTO);
          procBuilder.append(SPACE).append(idProduto);
          procBuilder.append(SPACE).append(COMMA).append(idFuncionario);
          procBuilder.append(";");

          return (BeneficioFuncionarioProdutoResponse) this.em.createNativeQuery(procBuilder.toString(), BeneficioFuncionarioProdutoResponse.class).getSingleResult();
     }

}
