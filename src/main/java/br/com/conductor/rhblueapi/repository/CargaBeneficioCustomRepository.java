
package br.com.conductor.rhblueapi.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import br.com.conductor.rhblueapi.domain.CargaBeneficioCustom;
import br.com.conductor.rhblueapi.domain.CargaBeneficioProdutosCustom;
import br.com.conductor.rhblueapi.domain.FuncionarioCargaBeneficio;
import br.com.conductor.rhblueapi.domain.request.PedidoDetalhesRequest;

public interface CargaBeneficioCustomRepository extends Repository<CargaBeneficioCustom, Long> {

     public List<CargaBeneficioCustom> listaDetalhadaDePedido(PedidoDetalhesRequest arquivoCargaRequest, Pageable pageable);

     public Integer countRegistrosDetalhado(PedidoDetalhesRequest arquivoCargaRequest, Pageable pageable);

     public List<FuncionarioCargaBeneficio> listaDetalhesFuncionariosCarga(Long idCargaBeneficio, Integer statusEmpresaCargaDetalheProduto, List<Integer> listaIdFuncionarios, Pageable pageable);

     public List<Integer> listaIdFuncionariosCarga(Long idCargaBeneficio, Integer statusEmpresaCargaDetalheProduto);

     public List<CargaBeneficioProdutosCustom> listaCargaBeneficioPorProduto(PedidoDetalhesRequest arquivoCargaRequest);
}
