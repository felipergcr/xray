
package br.com.conductor.rhblueapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.EmpresasCargas;

@Repository
public interface EmpresasCargasRepository extends JpaRepository<EmpresasCargas, Long> {

     Optional<List<EmpresasCargas>> findByIdCargaBeneficio(Long idCargaBeneficio);

     List<EmpresasCargas> findByIdCargaBeneficioIn(List<Long> collect);

     Optional<EmpresasCargas> findByIdCargaBeneficioAndIdEmpresa(Long idCargaBeneficio, Long idEmpresa);

}
