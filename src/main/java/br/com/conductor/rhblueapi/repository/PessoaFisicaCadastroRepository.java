
package br.com.conductor.rhblueapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.rhblueapi.domain.PessoaFisicaCadastro;

@Repository
public interface PessoaFisicaCadastroRepository extends JpaRepository<PessoaFisicaCadastro, Long> {
     

}
