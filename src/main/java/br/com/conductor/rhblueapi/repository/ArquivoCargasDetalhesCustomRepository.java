package br.com.conductor.rhblueapi.repository;

import br.com.conductor.rhblueapi.domain.ArquivoPedidoDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;

public interface ArquivoCargasDetalhesCustomRepository extends RepositoryCustom {
     
     PageResponse<ArquivoPedidoDetalhesErrosReponse> findByIdArquivoCargasAndStatusDetalheOrderByNumeroLinhaArquivo(Long idArquivoPedido, Integer page, Integer size);

}
