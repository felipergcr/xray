
package br.com.conductor.rhblueapi.functionalInterface;

@FunctionalInterface
public interface RetornaValorAtributo<M, T> {
     
     T retornaAtributoDeObjeto(M modelo);
}
