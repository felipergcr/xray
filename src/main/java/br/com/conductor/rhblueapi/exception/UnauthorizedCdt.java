package br.com.conductor.rhblueapi.exception;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;;

public class UnauthorizedCdt extends ExceptionCdt {
	
	private static final long serialVersionUID = 1L;
	
	public UnauthorizedCdt(final String message) {
		super(UNAUTHORIZED, message);
	}

}
