package br.com.conductor.rhblueapi.exception;


public class BusinessException extends Exception {
     
     /**
      * 
      */
     private static final long serialVersionUID = -3225533793461807488L;

     public BusinessException(String mensagem) {
          super(mensagem);
     }

}
