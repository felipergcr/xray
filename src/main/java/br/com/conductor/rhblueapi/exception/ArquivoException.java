package br.com.conductor.rhblueapi.exception;

public class ArquivoException extends Exception {

     private static final long serialVersionUID = -547025160324301319L;
          
     public ArquivoException(String mensagem) {
          super(mensagem);
     }

}
