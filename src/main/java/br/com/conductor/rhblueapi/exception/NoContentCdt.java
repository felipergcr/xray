package br.com.conductor.rhblueapi.exception;
import static org.springframework.http.HttpStatus.NO_CONTENT;

public class NoContentCdt extends ExceptionCdt {
	
	/**
      * 
      */
     private static final long serialVersionUID = -6913675134084073195L;

     public NoContentCdt (final String message) {
		super(NO_CONTENT, message);
		
	}

}
