package br.com.conductor.rhblueapi.exception;

import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;

public class PreconditionCustom extends ExceptionCdt {

     /**
      * 
      */
     private static final long serialVersionUID = 4878312470946750173L;

     public PreconditionCustom(final String message) {
          super(PRECONDITION_FAILED, message);
     }

}
