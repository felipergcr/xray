package br.com.conductor.rhblueapi.exception;

public class ObjetoVazioException extends Exception {
     
     /**
      * 
      */
     private static final long serialVersionUID = -4980039845209188263L;
     
     public ObjetoVazioException(String mensagem) {
          super(mensagem);
     }

}
