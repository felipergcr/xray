package br.com.conductor.rhblueapi.exception;

import org.springframework.http.HttpStatus;

public class CustomRuntimeException extends ExceptionCdt {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     
     public CustomRuntimeException(HttpStatus status, String message) {

          super(status, message);
     }


}
