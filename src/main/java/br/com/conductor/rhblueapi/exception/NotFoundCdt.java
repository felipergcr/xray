package br.com.conductor.rhblueapi.exception;
import static org.springframework.http.HttpStatus.NOT_FOUND;

public class NotFoundCdt extends ExceptionCdt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5055607301027693550L;

	public NotFoundCdt (final String message) {
		super(NOT_FOUND, message);
		
	}	
	
}
