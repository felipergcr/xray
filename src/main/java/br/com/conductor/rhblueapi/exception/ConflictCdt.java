package br.com.conductor.rhblueapi.exception;
import static org.springframework.http.HttpStatus.CONFLICT;

public class ConflictCdt extends ExceptionCdt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5055607301027693550L;

	public ConflictCdt (final String message) {
		super(CONFLICT, message);
		
	}	
	
}
