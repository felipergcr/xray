package br.com.conductor.rhblueapi.exception;

public class ArquivoVazioException extends Exception {

     private static final long serialVersionUID = -1060223002514821743L;
     
     public ArquivoVazioException(String mensagem) {
          super(mensagem);
     }

}
