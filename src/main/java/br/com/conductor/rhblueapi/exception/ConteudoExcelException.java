package br.com.conductor.rhblueapi.exception;


public class ConteudoExcelException extends Exception {

     private static final long serialVersionUID = 3100655836019664409L;
     
     public ConteudoExcelException(String texto) {
         super(texto);
     }

}
