
package br.com.conductor.rhblueapi.exception;

import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;

public class HttpClientErrorPierException extends ExceptionCdt {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     public HttpClientErrorPierException(final String message) {
          super(PRECONDITION_FAILED, message);
     }

}
