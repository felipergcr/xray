package br.com.conductor.rhblueapi.exception;

public class ConversaoObjetoException extends Exception {

     private static final long serialVersionUID = 8256951689179956103L;

     public ConversaoObjetoException(String mensagem) {
          super(mensagem);
     }

}