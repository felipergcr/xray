package br.com.conductor.rhblueapi.exception;
import static org.springframework.http.HttpStatus.FORBIDDEN;

public class ForbiddenCdt extends ExceptionCdt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5055607301027693550L;

	public ForbiddenCdt (final String message) {
		super(FORBIDDEN, message);
		
	}	
	
}
