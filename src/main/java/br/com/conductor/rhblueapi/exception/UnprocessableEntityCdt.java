
package br.com.conductor.rhblueapi.exception;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

public class UnprocessableEntityCdt extends ExceptionCdt {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     public UnprocessableEntityCdt(final String message) {
          super(UNPROCESSABLE_ENTITY, message);
     }

}
