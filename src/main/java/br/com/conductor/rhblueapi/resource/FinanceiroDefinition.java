
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_400;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.PageArquivoCargasResponseCustom;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = {"Financeiros"})
public interface FinanceiroDefinition {

    @ApiOperation(value = "Lista pedidos financeiro", notes = "Lista os pedidos financeiro")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista de pedidos financeiro", response = PageArquivoCargasResponseCustom.class),
            @ApiResponse(code = 204, message = "Pesquisa não encontrada", response = ErroInfo.class),
            @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
            @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class)
    })
    ResponseEntity<PageArquivoCargasResponseCustom> listarPedidosFinanceiro(@ApiParam("page") Integer page,@ApiParam("size") Integer size,
                                                                            @Valid @ModelAttribute ArquivoPedidoRequest request);


    @ApiOperation(value = "Consultar nota RPS", notes = "Recurso utilizado para consultar Nota RPS, informando como parâmetro o ID da nota RPS",
            produces = "application/pdf", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({@ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
            @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
            @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class)})
    ResponseEntity<?> consultarNotaRPS(@ApiParam(name = "codigo", value = "Código da nota RPS", required = true) Long codigoRPS);

     @ApiOperation(value = "Consultar Nota Fiscal", notes = "Recurso utilizado para consultar nota fiscal, informando como parâmetro o ID da nota fiscal",
               produces = "application/pdf", consumes = MediaType.APPLICATION_JSON_VALUE)
     @ApiResponses({ 
          @ApiResponse(code = 412, message = "Grupo empresa não encontrado", response = ErroInfo.class), 
          @ApiResponse(code = 412, message = "Usuário não encontrado", response = ErroInfo.class), 
          @ApiResponse(code = 404, message = "Permissão não encontrada", response = ErroInfo.class), 
          @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação", response = ErroInfo.class),
          @ApiResponse(code = 403, message = "Usuário não possui acesso para visualizar a nota fiscal", response = ErroInfo.class),
          @ApiResponse(code = 412, message = "O código pesquisado não é de uma Nota Fiscal, é de uma RPS", response = ErroInfo.class),
          @ApiResponse(code = 422, message = "Ocorreu um erro ao tentar gerar o access token para acesso a Nota Fiscal", response = ErroInfo.class),
          @ApiResponse(code = 422, message = "Ocorreu um erro ao tentar gerar a Nota Fiscal", response = ErroInfo.class)
          })
     ResponseEntity<?> consultarNotaFiscal(@ApiParam(name = "codigoNotaFiscal", value = "Código da nota fiscal", required = true) Long codigoNotaFiscal, @Valid @ModelAttribute IdentificacaoRequest usuarioGrupoResquest);

}
