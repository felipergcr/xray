
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_400;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_412;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import javax.management.BadAttributeValueExpException;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.response.GrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaOrganizacionalGrupoResponse;
import br.com.conductor.rhblueapi.domain.update.GrupoEmpresaUpdate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Grupo Empresa" })
public interface GrupoEmpresaDefinition {

	@ApiOperation(value = "Cadastro de Grupos empresa", notes = "Cria grupo empresa", response = GrupoEmpresaResponse.class)
	@ApiResponses({ 
	     @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
	     @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
	     @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
	     @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<GrupoEmpresaResponse> cadastrarGrupo(@Valid GrupoEmpresaPersist request) throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;
     
    @ApiOperation(value = "Atualizar Grupo empresa", notes = "Recurso para atualizar os grupos de empresa", response = GrupoEmpresaResponse.class)
    @ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
         @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
         @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
    ResponseEntity<GrupoEmpresaResponse> atualizarGrupoEmpresa(final Long idGrupoEmpresa, @RequestBody(required = true) @Valid final GrupoEmpresaUpdate request);
    
    @ApiOperation(value = "Atualizar Grupo empresa", notes = "Recurso para atualizar os grupos de empresa", response = GrupoEmpresaResponse.class)
    @ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
         @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
         @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
    ResponseEntity<GrupoEmpresaResponse> atualizarGrupoEmpresaPut(final Long idGrupoEmpresa, @RequestBody(required = true) @Valid final GrupoEmpresaUpdate request);

    @ApiOperation(value = "Consultar a hierarquia organizacional do Grupo Empresa", notes = "Consultar a hierarquia organizacional do Grupo Empresa")
    @ApiResponses({ 
         @ApiResponse(code = 400, message = "Usuário não existe.", response = ErroInfo.class),
         @ApiResponse(code = 400, message = "Grupo empresa não encontrado.", response = ErroInfo.class),
         @ApiResponse(code = 404, message = "Usuário Registro não possui vínculo para realizar essa operação", response = ErroInfo.class),
         @ApiResponse(code = 204, message = "Empresas não encontrado para o usuário e grupo empresa informado", response = ErroInfo.class),
         @ApiResponse(code = 204, message = "Não existe hierarquia organizacional para o usuário e grupo empresa informado", response = ErroInfo.class),
         @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
    HierarquiaOrganizacionalGrupoResponse listarHierarquiaOrganizacionalGrupo(@PathVariable Long id, @RequestParam(required = true) Long idUsuarioSessao);
         
     @ApiOperation(value = "Busca parametros de um grupo empresa", notes = "Consultar parametros")
     @ApiResponses({ 
          @ApiResponse(code = 404, message = "Usuário não encontrado", response = ErroInfo.class),
          @ApiResponse(code = 404, message = "Grupo empresa não localizado", response = ErroInfo.class),
          @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação", response = ErroInfo.class),
          @ApiResponse(code = 404, message = "Nível de acesso não encontrado", response = ErroInfo.class),
          @ApiResponse(code = 404, message = "Parametros do grupo empresa não encontrados.", response = ErroInfo.class) })
     ResponseEntity<?> listaParametrosGrupoEmpresa(Long idGrupoEmpresa, Long idUsuario);
     
}
