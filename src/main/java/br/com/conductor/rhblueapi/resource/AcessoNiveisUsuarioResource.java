
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_PERFIS;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;
import java.util.List;

import javax.management.BadAttributeValueExpException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.response.AcessoNiveisUsuarioResponse;
import br.com.conductor.rhblueapi.enums.Plataforma;
import br.com.conductor.rhblueapi.service.AcessoNiveisUsuarioService;

@RestController
public class AcessoNiveisUsuarioResource implements AcessoNiveisUsuarioDefinition {

     @Autowired
     private AcessoNiveisUsuarioService acessoNiveisUsuarioService;

     @Override
     @ResponseStatus(value = OK)
     @GetMapping(value = PATH_PERFIS, produces = APPLICATION_JSON_UTF8_VALUE)
     public ResponseEntity<?> listarNiveisAcessoUsuario(@RequestParam(name = "plataforma", required = false) Plataforma plataforma) throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException {

          List<AcessoNiveisUsuarioResponse> response = acessoNiveisUsuarioService.listarAcessosNiveisUsuario(plataforma);

          return ResponseEntity.ok(response);
     }

}
