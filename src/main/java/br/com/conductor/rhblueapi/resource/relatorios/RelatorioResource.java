
package br.com.conductor.rhblueapi.resource.relatorios;

import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_DELETAR;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_RELATORIOS;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.conductor.rhblueapi.domain.relatorios.RelatorioPortal;
import br.com.conductor.rhblueapi.domain.relatorios.response.RelatorioResponse;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioDeletarRequest;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioFilter;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioRequest;
import br.com.conductor.rhblueapi.service.acessos.GerenciadorAcessoService;
import br.com.conductor.rhblueapi.service.relatorios.RelatorioService;
import br.com.conductor.rhblueapi.service.relatorios.criacao.RelatorioPedidoCriacao;
import br.com.conductor.rhblueapi.service.relatorios.validacao.RelatoriosValidacaoService;
import br.com.conductor.rhblueapi.util.HttpHeadersUtils;

@RestController
@RequestMapping(value = PATH_RELATORIOS, produces = APPLICATION_JSON_UTF8_VALUE)
public class RelatorioResource implements RelatorioDefinition {
     
     @Autowired
     private GerenciadorAcessoService gerenciadorAcessoService;
     
     @Autowired
     private RelatoriosValidacaoService relatoriosValidacaoService;
     
     @Autowired
     private RelatorioService relatorioService;
     
     @Autowired
     private RelatorioPedidoCriacao relatorioPedidoCriacao;
     
     @PostMapping
     @ResponseStatus(HttpStatus.CREATED)
     public RelatorioResponse solicitar(@Valid @RequestBody RelatorioRequest relatorioRequest) {
          
          gerenciadorAcessoService.validaIdentificaoEObtemIdPermissaoUsuarioLogado(relatorioRequest.getIdentificacao());
          
          relatoriosValidacaoService.validaPeriodo(relatorioRequest.getPeriodoRequest());
          
          return relatorioService.solicitar(relatorioRequest);
     }

     @GetMapping
     @Override
     public Page<RelatorioResponse> listar(@Valid RelatorioFilter relatorioFilter) {
          
          RelatorioRequest relatorioRequest = relatoriosValidacaoService.createAdapter(relatorioFilter.getDataInicio(), relatorioFilter.getDataFim(), relatorioFilter.getIdUsuario(), relatorioFilter.getIdGrupoEmpresa());
          
          gerenciadorAcessoService.validaIdentificaoEObtemIdPermissaoUsuarioLogado(relatorioRequest.getIdentificacao());
          
          relatoriosValidacaoService.validaPeriodo(relatorioRequest.getPeriodoRequest());
          
          return relatorioService.buscar(relatorioFilter);
     }

     @PostMapping(value = PATH_DELETAR)
     @Override
     public ResponseEntity<?> deletar(@Valid @RequestBody RelatorioDeletarRequest relatorioDeletarRequest) {

          gerenciadorAcessoService.validaIdentificacaoUsuario(relatorioDeletarRequest.getIdentificacao());

          List<RelatorioPortal> relatorios = relatorioService.autenticarRelatoriosProcessadosPorIdsEUsuarioEGrupoEmpresa(relatorioDeletarRequest.getIdsRelatorio(), relatorioDeletarRequest.getIdentificacao().getIdUsuario(), relatorioDeletarRequest.getIdentificacao().getIdGrupoEmpresa());

          relatorioService.deletarRelatorios(relatorios);

          return new ResponseEntity<>(HttpStatus.CREATED);
     }

     @GetMapping(value = "/{id}")
     @Override
     public ResponseEntity<?> gerar(@PathVariable(name = "id") Long id, @Valid @ModelAttribute IdentificacaoRequest identificacaoRequest) throws IOException {

          gerenciadorAcessoService.validaIdentificaoEObtemIdPermissaoUsuarioLogado(identificacaoRequest);

          RelatorioPortal relatorio = relatorioService.autenticarRelatorioProcessadoPorIdEUsuarioEGrupoEmpresa(id, identificacaoRequest.getIdUsuario(), identificacaoRequest.getIdGrupoEmpresa());

          byte[] relatorioByte = relatorioPedidoCriacao.gerarRelatorioPorGrupoEPeriodo(relatorio.getIdGrupoEmpresa(), relatorio.getDataInicio(), relatorio.getDataFim());

          HttpHeaders headers = HttpHeadersUtils.headersXls(relatorioPedidoCriacao.getNomeArquivoCompleto(), relatorioByte.length);

          return new ResponseEntity<>(relatorioByte, headers, HttpStatus.OK);

     }

}
