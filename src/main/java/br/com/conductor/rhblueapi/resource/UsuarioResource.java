package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.util.AppConstantes.HIERARQUIA_ACESSOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_USUARIOS;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import javax.management.BadAttributeValueExpException;
import javax.validation.Valid;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.domain.persist.HierarquiaAcessoUpdatePersist;
import br.com.conductor.rhblueapi.domain.persist.UsuarioPersist;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResumidoResponse;
import br.com.conductor.rhblueapi.domain.request.UsuarioAcessoRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioHierarquiaAcessoRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioRequestBlue;
import br.com.conductor.rhblueapi.domain.request.UsuarioSenhaTokenUpdateRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioSenhaUpdateRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioValidarSenhaLoginRequest;
import br.com.conductor.rhblueapi.domain.response.AlterarSenhaResponse;
import br.com.conductor.rhblueapi.domain.response.UsuarioHierarquiaAcessoResponse;
import br.com.conductor.rhblueapi.domain.update.UsuarioResumidoUpdate;
import br.com.conductor.rhblueapi.service.HierarquiaAcessoService;
import br.com.conductor.rhblueapi.service.UsuarioPierService;
import br.com.conductor.rhblueapi.service.UsuarioService;
import br.com.conductor.rhblueapi.service.acessos.GerenciadorAcessoService;
import br.com.conductor.rhblueapi.util.GenericConvert;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value = PATH_USUARIOS )
@Validated
public class UsuarioResource implements UsuarioDefinition {

     @Autowired
     private UsuarioPierService usuarioPierService;
     
     @Autowired
     private HierarquiaAcessoService hierarquiaAcessoService;
     
     @Autowired
     private GerenciadorAcessoService gerenciadorAcessoService;
     
     @Autowired
     private UsuarioService usuarioService;

     @Override
     @ResponseStatus(value = CREATED)
     @PostMapping
     public ResponseEntity<UsuarioResponse> criaUsuario(@RequestBody(required = true) @Valid UsuarioRequestBlue usuario) 
               throws JsonParseException, JsonMappingException, IOException {

          UsuarioResponse response = this.usuarioPierService.criarUsuario(usuario, true);
          return new ResponseEntity<UsuarioResponse>(response, HttpStatus.CREATED);
     }

     @Override
     @ResponseStatus(value = OK)
     @PutMapping(value = "/senhas/{token}")
     public ResponseEntity<HttpStatus> alterarSenhaToken(@PathVariable(required = true) String token, @RequestBody(required = true) UsuarioSenhaTokenUpdateRequest senha)
               throws JsonParseException, JsonMappingException, IOException {

          HttpStatus response = usuarioPierService.alteraSenha(token, senha);
          return new ResponseEntity<HttpStatus>(response);
     }

     @Override
     @ResponseStatus(value = OK)
     @PatchMapping(value = "/{id}/acessos")
     public ResponseEntity<UsuarioResponse> alterarAcesso(@PathVariable(required = true) Long id, @Valid @RequestBody(required = true) UsuarioAcessoRequest usuario) {

          return ResponseEntity.ok(this.usuarioPierService.alterarUsuarioAcesso(id, usuario));
     }

     @Override
     @ResponseStatus(value = CREATED)
     @PutMapping(value = "/senhas")
     public ResponseEntity<AlterarSenhaResponse> alterarSenhas(@RequestParam(name = "operacao", required = true) final String operacao, @RequestBody(required = true)  @Valid final  UsuarioSenhaUpdateRequest senha)
               throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException {

          final AlterarSenhaResponse response = usuarioPierService.alterarSenha(operacao, senha);
          return new ResponseEntity<AlterarSenhaResponse>(response, HttpStatus.CREATED);
     }


     @Override
     @ResponseStatus(value = OK)
     @PostMapping(value = "/{login}/senha")
     public ResponseEntity<?> validarSenhaLogin(@ApiIgnore @RequestHeader("access_token") String accessToken, @PathVariable(required = true) String login,  
               @RequestBody(required = true) @Valid UsuarioValidarSenhaLoginRequest usuario) 
                         throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException {
          UsuarioResponse response = this.usuarioPierService.validarSenhaLogin(login, usuario);
          return new ResponseEntity<UsuarioResponse>(response, HttpStatus.OK);          
     }

     @Override
     @ResponseStatus(value = CREATED)
     @PostMapping(value =  HIERARQUIA_ACESSOS)
     @Validated
     public void criaHierarquiaAcesso( @Valid @RequestBody UsuarioPersist usuarioPersist) {
          hierarquiaAcessoService.criaHierarquiaAcesso(usuarioPersist);
     }
     
     @Override
     @RequestMapping(value = HIERARQUIA_ACESSOS , method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
     public ResponseEntity<UsuarioHierarquiaAcessoResponse> listaHierarquiaAcesso(@ModelAttribute @Valid UsuarioHierarquiaAcessoRequest request) {
     return new ResponseEntity<>(hierarquiaAcessoService.retornaHierarquiaAcesso(request), HttpStatus.OK);
     }
     
     @Override
     @ResponseStatus(value = OK)
     @PutMapping(value = "/{cpf}" + HIERARQUIA_ACESSOS)
     public void atualizarHierarquiaAcesso(@PathVariable(name = "cpf" , required=true)  @CPF  String cpf,  @RequestBody(required = true)  @Valid HierarquiaAcessoUpdatePersist update) throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException {

          hierarquiaAcessoService.atualizarHierarquiaAcesso(update, cpf);
     }

     @Override
     @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
     public ResponseEntity<UsuarioResumidoResponse> obterUsuario(String cpf, Long idGrupoEmpresa) {

          Usuario usuario = gerenciadorAcessoService.validaPorCpfEGrupoEmpresa(cpf, idGrupoEmpresa);

          return new ResponseEntity<>(GenericConvert.convertModelMapper(usuario, UsuarioResumidoResponse.class), HttpStatus.OK);
     }

     @Override
     @PutMapping(value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
     public ResponseEntity<UsuarioResumidoResponse> atualizarUsuarioResumido(@PathVariable(name = "id") Long idUsuario, @RequestBody @Valid UsuarioResumidoUpdate usuarioResumidoUpdate) {

          Usuario usuario = usuarioService.autenticarUsuarioPorId(idUsuario);

          usuario = usuarioService.atualizarUsuarioResumido(usuario, usuarioResumidoUpdate);

          return new ResponseEntity<>(GenericConvert.convertModelMapper(usuario, UsuarioResumidoResponse.class), HttpStatus.OK);

     }
}
