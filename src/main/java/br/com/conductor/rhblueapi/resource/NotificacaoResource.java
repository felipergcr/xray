package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.util.AppConstantes.LISTA_PEDIDOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_NOTIFICACOES;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_PROCESSAMENTO;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_UNIDADES_ENTREGA;
import static br.com.conductor.rhblueapi.util.AppConstantes.PENDENCIAS_TED;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.NotificacoesArquivoPedidoResponse;
import br.com.conductor.rhblueapi.domain.response.NotificacoesArquivoUnidadeEntregaResponse;
import br.com.conductor.rhblueapi.domain.response.PendenciasPagamentoTedResponse;
import br.com.conductor.rhblueapi.service.NotificacaoService;
import br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService;

@RestController
@RequestMapping(value = PATH_NOTIFICACOES, produces = APPLICATION_JSON_UTF8_VALUE)
public class NotificacaoResource implements NotificacaoDefinition {

     @Autowired
     private NotificacaoService notificacaoService;

     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;

     @Override
     @GetMapping(value = PENDENCIAS_TED)
     public ResponseEntity<PendenciasPagamentoTedResponse> notificacaoPendeciaTed(IdentificacaoRequest identificacaoRequest) {

          return new ResponseEntity<>(notificacaoService.buscaPendenciasPagamentoTed(identificacaoRequest), HttpStatus.OK);
     }

     @Override
     @GetMapping(value=PATH_UNIDADES_ENTREGA+PATH_PROCESSAMENTO)
     public ResponseEntity<NotificacoesArquivoUnidadeEntregaResponse> notificacaoArquivoUnidadeEntregaEmProcessamento(@ModelAttribute IdentificacaoRequest identificacaoRequest){

          usuarioNivelPermissaoRhService.usuarioPossuiNivelHierarquiaNoGrupo(identificacaoRequest.getIdUsuario(), identificacaoRequest.getIdGrupoEmpresa());

          return new ResponseEntity<>(notificacaoService.buscaArquivoUnidadeEntregaEmProcessamento(identificacaoRequest), HttpStatus.OK);
     }

     @Override
     @GetMapping(value=LISTA_PEDIDOS)
     public ResponseEntity<NotificacoesArquivoPedidoResponse> notificacaoArquivoPedidoEmProcessamento(@Valid @ModelAttribute IdentificacaoRequest identificacaoRequest){

          return new ResponseEntity<>(notificacaoService.buscaArquivoProcessoEmProcessamento(identificacaoRequest), HttpStatus.OK);
     }


}
