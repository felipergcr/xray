package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_GRUPO_EMPRESAS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_SUBGRUPO_EMPRESAS;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import javax.management.BadAttributeValueExpException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.persist.SubgrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.request.SubgrupoEmpresaRequest;
import br.com.conductor.rhblueapi.domain.response.PageSubgrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.SubgrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.update.SubgrupoEmpresaUpdate;
import br.com.conductor.rhblueapi.service.GrupoEmpresaService;
import br.com.conductor.rhblueapi.service.grupoempresa.CadastroGrupoEmpresaValidacaoService;

@RestController
public class SubgrupoEmpresaResource implements SubgrupoEmpresaDefinition {
     
     @Autowired
     private CadastroGrupoEmpresaValidacaoService cadastroGrupoEmpresaValidacaoService;

     @Autowired
     private GrupoEmpresaService grupoEmpresaServico;
     
     @Override
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value =  PATH_GRUPO_EMPRESAS + "/{id}" + PATH_SUBGRUPO_EMPRESAS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public PageSubgrupoEmpresaResponse listaSubgrupo(@PathVariable("id") Long idGrupoEmpresa, @RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "50") Integer size, @ModelAttribute SubgrupoEmpresaRequest subgrupoEmpresaRequest) {

          return grupoEmpresaServico.listaSubgrupo(idGrupoEmpresa, subgrupoEmpresaRequest,  page,  size);
     }

     @RequestMapping(value = PATH_GRUPO_EMPRESAS + "/{id}" + PATH_SUBGRUPO_EMPRESAS + "/{idSubgrupo}", produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PATCH)
     public ResponseEntity<SubgrupoEmpresaResponse> atualizarSubgrupoEmpresa(
               @PathVariable("id") final Long idGrupo,
               @PathVariable("idSubgrupo") final Long idSubgrupo, @RequestBody final SubgrupoEmpresaUpdate request) {
          
          SubgrupoEmpresaResponse response = grupoEmpresaServico.atualizarSubgrupoEmpresa(idGrupo, idSubgrupo, request);
          return new ResponseEntity<SubgrupoEmpresaResponse>(response, HttpStatus.OK);
     }
     
     @RequestMapping(value = PATH_GRUPO_EMPRESAS + "/{id}" + PATH_SUBGRUPO_EMPRESAS + "/{idSubgrupo}", produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PUT)
     public ResponseEntity<SubgrupoEmpresaResponse> atualizarSubgrupoEmpresaPut(
               @PathVariable("id") final Long idGrupo,
               @PathVariable("idSubgrupo") final Long idSubgrupo, @RequestBody final SubgrupoEmpresaUpdate request) {
          
          SubgrupoEmpresaResponse response = grupoEmpresaServico.atualizarSubgrupoEmpresa(idGrupo, idSubgrupo, request);
          return new ResponseEntity<SubgrupoEmpresaResponse>(response, HttpStatus.OK);
     }
     
     @Override
     @ResponseStatus(HttpStatus.CREATED)
     @RequestMapping(value = PATH_GRUPO_EMPRESAS + "/{id}" + PATH_SUBGRUPO_EMPRESAS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
     public ResponseEntity<SubgrupoEmpresaResponse> cadastrarSubgrupo(@PathVariable("id") Long idGrupoPai, @Valid @RequestBody SubgrupoEmpresaPersist persist) throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException {

          cadastroGrupoEmpresaValidacaoService.validaRequest(persist);
          
          return new ResponseEntity<SubgrupoEmpresaResponse>(this.grupoEmpresaServico.cadastraSubGrupo(persist, idGrupoPai), HttpStatus.CREATED);
     }
}