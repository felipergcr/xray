package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_200;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_400;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_401;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_403;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_409;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_412;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import javax.management.BadAttributeValueExpException;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.persist.HierarquiaAcessoUpdatePersist;
import br.com.conductor.rhblueapi.domain.persist.UsuarioPersist;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResumidoResponse;
import br.com.conductor.rhblueapi.domain.request.UsuarioAcessoRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioHierarquiaAcessoRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioRequestBlue;
import br.com.conductor.rhblueapi.domain.request.UsuarioSenhaTokenUpdateRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioSenhaUpdateRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioValidarSenhaLoginRequest;
import br.com.conductor.rhblueapi.domain.response.UsuarioHierarquiaAcessoResponse;
import br.com.conductor.rhblueapi.domain.update.UsuarioResumidoUpdate;
import br.com.conductor.rhblueapi.util.AppConstantes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.annotations.ApiIgnore;

@Api(value=AppConstantes.PATH_USUARIOS, produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Usuário" })
public interface UsuarioDefinition {

	@ApiOperation(value = "Criação usuário", notes = "Cria um usuário no Pier", response = UsuarioResponse.class)
	@ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
			@ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
			@ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
			@ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) 
	})
	ResponseEntity<?> criaUsuario(@Valid UsuarioRequestBlue request)
			throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;
	
     @ApiOperation(value = "Cria estrutura hierarquica de acessos para usuário", 
               notes = "Cria um usuário no Pier caso não exista, permissão e nível de acesso")
     @ApiResponses({ 
          @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class), 
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), 
          @ApiResponse(code = 403, message = MENSAGEM_GLOBAL_403, response = ErroInfo.class),
          @ApiResponse(code = 409, message = MENSAGEM_GLOBAL_409, response = ErroInfo.class), 
          @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class), 
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class)
          })
     void criaHierarquiaAcesso(@RequestBody @Valid  UsuarioPersist request);
	
	@ApiOperation(value = "Alterar senha do usuário conforme o TOKEN", notes = "Alterar senha de um usuario no Pier")
	@ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
			@ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
			@ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
			@ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) 
	})
	ResponseEntity<?> alterarSenhaToken(@PathVariable String token, @RequestBody(required=true) @Valid UsuarioSenhaTokenUpdateRequest senha)
			throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;
	
     @ApiOperation(value = "Alterar dados de acesso do usuário", notes = "Alterar um usuario no Pier")
     @ApiResponses({ 
          @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class)
     }) 
     ResponseEntity<?> alterarAcesso(@PathVariable Long id, @Valid UsuarioAcessoRequest usuario);

     @ApiOperation(value = "Alterar senha do usuário", notes = "Alterar senha do usuário")
     @ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
          @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class)  
     })
     ResponseEntity<?> alterarSenhas(@RequestParam String operacao, @RequestBody(required = true) @Valid final  UsuarioSenhaUpdateRequest senha)
               throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;
     
     @ApiOperation(value = "Validar senha login", notes = "Validar senha login do usuario", response = UsuarioResponse.class)
     @ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 401, message = MENSAGEM_GLOBAL_401, response = ErroInfo.class),  
          @ApiResponse(code = 403, message = MENSAGEM_GLOBAL_403, response = ErroInfo.class),  
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) 
     })
     ResponseEntity<?> validarSenhaLogin(@ApiIgnore @ApiParam(value = "Access Token", required = true) String accessToken, 
               @PathVariable(required = true) String login, @RequestBody(required = true) @Valid UsuarioValidarSenhaLoginRequest request)
               throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException; 
     
     @ApiOperation(value = "Atualizar hierarquia acesso de usuário", notes = "Atualiza nível de acesso da hierarquia de acesso do usuário RH")
     @ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 403, message = MENSAGEM_GLOBAL_403, response = ErroInfo.class),  
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
          @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),  
     })
     void atualizarHierarquiaAcesso(@PathVariable(name = "cpf" , required=true) @CPF String cpf, @RequestBody(required = true) @Valid HierarquiaAcessoUpdatePersist update)throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;
          
     @ApiOperation(value = "Lista hierarquia de acessos do usuário", notes = "Lista dados do usuário, permissão e nível de acesso")
     @ApiResponses({ 
          @ApiResponse(code = 200, message = MENSAGEM_GLOBAL_200, response = UsuarioHierarquiaAcessoResponse.class),
          @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
          @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) 
     })
     ResponseEntity<UsuarioHierarquiaAcessoResponse> listaHierarquiaAcesso(@RequestBody(required=true)  @Valid UsuarioHierarquiaAcessoRequest request); 
     
     @ApiOperation(value = "Obter dados do usuário", notes = "Lista dados do usuário, permissão e nível de acesso")
     @ApiResponses({ 
          @ApiResponse(code = 404, message = "Usuário não existe.", response = ErroInfo.class),
          @ApiResponse(code = 404, message = "Grupo empresa não encontrado.", response = ErroInfo.class),
          @ApiResponse(code = 403, message = "O CPF informado não possui vinculo com o Grupo empresa.", response = ErroInfo.class),
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) 
     })
     ResponseEntity<UsuarioResumidoResponse> obterUsuario(@RequestParam @Pattern(regexp = "\\d+", message = "CPF inválido") String cpf, @RequestParam Long idGrupoEmpresa); 
     
     @ApiOperation(value = "Atualizar alguns dados do usuário", notes = "Atualiza o email, data de nascimento e a data da ultima atualização do usuário informado")
     @ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 404, message = "Usuário não encontrado", response = ErroInfo.class)
     })
     ResponseEntity<UsuarioResumidoResponse> atualizarUsuarioResumido(@PathVariable(name = "id") Long idUsuario, @RequestBody @Valid UsuarioResumidoUpdate usuarioResumidoUpdate);
}
