
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_PERMISSOES_OPERACAO;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_PERMISSOES_RH;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.conductor.rhblueapi.domain.persist.PermissoesOperacoesRhPersist;
import br.com.conductor.rhblueapi.domain.request.PermissoesOperacoesRhResquest;
import br.com.conductor.rhblueapi.service.PermissoesOperacoesRhService;

@RestController
public class PermissoesOperacoesRhResource implements PermissoesOperacoesRhDefinition {

  @Autowired
  private PermissoesOperacoesRhService permissoesOperacoesService;

  @Override
  @ResponseStatus(HttpStatus.OK)
  @PutMapping(value = PATH_PERMISSOES_RH + "/{id}" + PATH_PERMISSOES_OPERACAO, produces = APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<?> vincularPermissaoOpercaoRh(@Valid @RequestBody(required = true) PermissoesOperacoesRhPersist persist, @PathVariable Long id) {
    return new ResponseEntity<>(permissoesOperacoesService.vincularPermissaoOperacao(persist, id), HttpStatus.OK);
  }

  @Override
  @ResponseStatus(HttpStatus.OK)
  @GetMapping(value = PATH_PERMISSOES_RH + "/{id}" + PATH_PERMISSOES_OPERACAO,produces = APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<?> listarPermissaoOpercaoRh(@PathVariable Long id,@ModelAttribute @Valid PermissoesOperacoesRhResquest request,@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "50") Integer size) {
    return new ResponseEntity<>(permissoesOperacoesService.listarPermissoesOperacoesRh(id, request, page, size),HttpStatus.OK);
  }

}
