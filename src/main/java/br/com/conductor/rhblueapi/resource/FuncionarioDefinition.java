
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_200;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import org.springframework.http.ResponseEntity;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.request.funcionario.FuncionarioAtualizacaoRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Funcionários" })
public interface FuncionarioDefinition {

     @ApiOperation(value = "Atualizar dados de um funcionário", notes = "Este recurso permite atualizar dados básicos de um funcionário.")
     @ApiResponses({
          @ApiResponse(code = 400, message = "O request possui dados inválidos.", response = ErroInfo.class), 
          @ApiResponse(code = 404, message = "Usuário não encontrado.", response = ErroInfo.class), 
          @ApiResponse(code = 404, message = "Grupo empresa não localizado.", response = ErroInfo.class), 
          @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação.", response = ErroInfo.class),
          @ApiResponse(code = 403, message = "Usuário não possui acesso para atualizar os dados do funcionário.", response = ErroInfo.class),
          @ApiResponse(code = 400, message = "Endereço obrigatório.", response = ErroInfo.class),
          @ApiResponse(code = 404, message = "Pessoa não encontrada.", response = ErroInfo.class),
          @ApiResponse(code = 404, message = "Endereço não encontrado.", response = ErroInfo.class),
          @ApiResponse(code = 200, message = MENSAGEM_GLOBAL_200) })
     ResponseEntity<?> atualizar(@ApiParam(value = "Identificador do funcionário" , required = true) final Long id,
               @ApiParam(value = "Payload de request do funcionário" , required = true) final FuncionarioAtualizacaoRequest funcionarioAtualizacaoRequest);

}
