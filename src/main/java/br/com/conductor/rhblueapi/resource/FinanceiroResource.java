
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.util.AppConstantes.APPLICATION;
import static br.com.conductor.rhblueapi.util.AppConstantes.ATTACHMENT;
import static br.com.conductor.rhblueapi.util.AppConstantes.NOTA_FISCAL;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_FINANCEIROS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_LISTA_PEDIDOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_NOTARPS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_RELATORIOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.FileExtension.PDF;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.conductor.rhblueapi.domain.deloitte.NotaFiscalResponse;
import br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.PageArquivoCargasResponseCustom;
import br.com.conductor.rhblueapi.service.FinanceiroService;
import br.com.conductor.rhblueapi.util.AppConstantes;
import br.com.twsoftware.alfred.object.Objeto;

@RestController
public class FinanceiroResource implements FinanceiroDefinition {

    @Autowired
    private FinanceiroService financeiroService;

    @Override
    @RequestMapping(value = PATH_FINANCEIROS+PATH_LISTA_PEDIDOS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    public ResponseEntity<PageArquivoCargasResponseCustom> listarPedidosFinanceiro(@RequestParam(name = "page", defaultValue = "0") Integer page, 
              @RequestParam(name = "size", defaultValue = "10") Integer size, @ModelAttribute ArquivoPedidoRequest request) {

         
        PageArquivoCargasResponseCustom pageArquivoCargasResponse = this.financeiroService.listarPedidosFinanceiro(page, size, request);
        
        pageArquivoCargasResponse.getContent().stream().forEach( item -> {
             item.getCargaControleFinanceiros().stream().forEach( cargaControleFinanceiro -> {
                  
                  if(Objeto.isBlank(cargaControleFinanceiro.getNumeroNF())) {
                       cargaControleFinanceiro.add(linkTo(methodOn(FinanceiroResource.class).consultarNotaRPS(cargaControleFinanceiro.getIdCargaControleFinanceiro())).withRel("rps"));
                  }
                  else {
                       IdentificacaoRequest usuarioGrupoResquest = IdentificacaoRequest.builder().idGrupoEmpresa(request.getIdGrupoEmpresa()).idUsuario(request.getIdUsuarioSessao()).build();
                       cargaControleFinanceiro.add(linkTo(methodOn(FinanceiroResource.class).consultarNotaFiscal(cargaControleFinanceiro.getIdCargaControleFinanceiro(), usuarioGrupoResquest)).withRel("nota-fiscal"));
                  }
                       
             });
        });

        return new ResponseEntity<>(pageArquivoCargasResponse, HttpStatus.OK);
    }

    @Override
    @RequestMapping(value = PATH_RELATORIOS+PATH_NOTARPS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    public ResponseEntity<?> consultarNotaRPS(@PathVariable("codigo") final Long codigoRPS) {
        return financeiroService.consultarNotaRPS(codigoRPS);
    }

     @Override
     @RequestMapping(value = PATH_FINANCEIROS+"/{codigoNotaFiscal}"+NOTA_FISCAL, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public ResponseEntity<?> consultarNotaFiscal(@PathVariable("codigoNotaFiscal") final Long codigoNotaFiscal, @Valid @ModelAttribute IdentificacaoRequest request) {
          
          NotaFiscalResponse notaFiscalResponse = financeiroService.consultarNotaFiscal(codigoNotaFiscal, request);
          
          HttpHeaders headers = new HttpHeaders();
          headers.setContentType(new MediaType(APPLICATION, PDF));
          headers.setContentDispositionFormData(ATTACHMENT, notaFiscalResponse.getNomeArquivo());
          headers.setContentLength(notaFiscalResponse.getNotaFiscal().length);
          
          return new ResponseEntity<>(notaFiscalResponse.getNotaFiscal(), headers, HttpStatus.OK); 
     }

}
