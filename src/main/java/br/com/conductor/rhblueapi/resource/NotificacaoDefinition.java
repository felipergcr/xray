
package br.com.conductor.rhblueapi.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;

import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.NotificacoesArquivoPedidoResponse;
import br.com.conductor.rhblueapi.domain.response.NotificacoesArquivoUnidadeEntregaResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags= {"Notificações"})
public interface NotificacaoDefinition {
     
     @ApiOperation(value = "Notificação de pendência de pagamento na modalidade TED", notes = "Retorna ids dos pedidos com pendências de pagamento na modalidade TED" )
     @ApiResponses({
          @ApiResponse(code = 412, message = "Grupo empresa não encontrado"),
          @ApiResponse(code = 412, message = "Usuário não encontrado."),
          @ApiResponse(code = 404, message = "Permissão não encontrada"),
          @ApiResponse(code = 412, message = "Parametro empresa não localizado"),
          @ApiResponse(code = 412, message = "Forma de pagamento é diferente de TED"),
          @ApiResponse(code = 404, message = "Nível de acesso não encontrado."),
          @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação"),
          @ApiResponse(code = 204, message = "Registros não encontrados."),
          
     })
     ResponseEntity<?> notificacaoPendeciaTed(@ModelAttribute IdentificacaoRequest identificacaoRequest);

     @ApiOperation(value = "Notificação de arquivos de unidade entrega em processamento", notes = "Retorna apenas status code", response=NotificacoesArquivoUnidadeEntregaResponse.class )
     @ApiResponses({
          @ApiResponse(code = 404, message = "Grupo empresa não encontrado"),
          @ApiResponse(code = 404, message = "Usuário não encontrado."),
          @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação"),
     })
     ResponseEntity<?> notificacaoArquivoUnidadeEntregaEmProcessamento(IdentificacaoRequest identificacaoRequest);

     @ApiOperation(value = "Notificação de arquivos de pedido em processamento", notes = "Retorna apenas status code", response= NotificacoesArquivoPedidoResponse.class )
     @ApiResponses({
               @ApiResponse(code = 412, message = "Usuário não encontrado."),
               @ApiResponse(code = 404, message = "Grupo empresa não encontrado"),
               @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação"),
     })
     ResponseEntity<?> notificacaoArquivoPedidoEmProcessamento(IdentificacaoRequest identificacaoRequest);

}
