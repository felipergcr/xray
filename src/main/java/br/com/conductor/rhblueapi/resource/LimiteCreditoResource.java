
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.enums.limiteCredito.OrigemBuscaLimiteCreditoEnum.ENDPOINT_BUSCA_LIMITE_DIARIO;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_LIMITE_CREDITO;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conductor.rhblueapi.domain.LimiteCredito.LimiteCredito;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.LimiteCreditoResponse;
import br.com.conductor.rhblueapi.service.LimiteCreditoService;
import br.com.conductor.rhblueapi.service.acessos.GerenciadorAcessoService;

@RestController
@RequestMapping(value = PATH_LIMITE_CREDITO, produces = APPLICATION_JSON_UTF8_VALUE)
public class LimiteCreditoResource implements LimiteCreditoDefinition {

     @Autowired
     private LimiteCreditoService limiteCreditoService;

     @Autowired
     private GerenciadorAcessoService gerenciadorAcessoService;

     @Override
     @GetMapping
     public ResponseEntity<?> obterLimiteCredito(IdentificacaoRequest identificacaoRequest) {

          gerenciadorAcessoService.validaIdentificaoEObtemIdPermissaoUsuarioLogado(identificacaoRequest);

          LimiteCredito limiteCredito = limiteCreditoService.obterLimiteCredito(identificacaoRequest.getIdGrupoEmpresa(), ENDPOINT_BUSCA_LIMITE_DIARIO);

          LimiteCreditoResponse response = new LimiteCreditoResponse();
          BeanUtils.copyProperties(limiteCredito, response);

          return new ResponseEntity<>(response, HttpStatus.OK);
     }
}
