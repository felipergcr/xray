
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.util.AppConstantes.CANCELAR_PEDIDOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.CANCELAR_PEDIDOS_DETALHES;
import static br.com.conductor.rhblueapi.util.AppConstantes.CARGAS;
import static br.com.conductor.rhblueapi.util.AppConstantes.FUNCIONARIOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.LISTA_DETALHES_PEDIDOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.LISTA_ERROS_ARQUIVO_PEDIDO;
import static br.com.conductor.rhblueapi.util.AppConstantes.LISTA_PEDIDOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_UPLOAD;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.conductor.rhblueapi.domain.PedidoResponse;
import br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest;
import br.com.conductor.rhblueapi.domain.request.PedidoDetalheFuncionarioRequest;
import br.com.conductor.rhblueapi.domain.request.PedidoDetalhesRequest;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.service.PedidoDetalhadoService;
import br.com.conductor.rhblueapi.service.PedidoService;
import io.swagger.annotations.ApiParam;

@RestController
public class PedidoResource implements PedidoDefinition {

     @Autowired
     private PedidoService pedidoService;

     @Autowired
     private PedidoDetalhadoService pedidoDetalheService;

     @Override
     @ResponseStatus(HttpStatus.CREATED)
     @RequestMapping(value = PATH_UPLOAD, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
     public ResponseEntity<PedidoResponse> uploadFile(@ModelAttribute IdentificacaoRequest usuarioGrupoResquest, @ApiParam(name = "file", value = "Selecione o arquivo XLSX ou XLS para Upload", required = true) @RequestPart(required = true) MultipartFile file) {

          return new ResponseEntity<PedidoResponse>(this.pedidoService.uploadFile(usuarioGrupoResquest, file), CREATED);
     }

     @Override
     @RequestMapping(value = LISTA_PEDIDOS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public ResponseEntity<?> listarPedidos(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size, @Valid @ModelAttribute ArquivoPedidoRequest request) {

          return new ResponseEntity<>(this.pedidoService.listarPedidos(page, size, request), HttpStatus.OK);
     }

     @Override
     @RequestMapping(value = LISTA_DETALHES_PEDIDOS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public ResponseEntity<?> listarDetalhesCarga(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size, @PathVariable(value = "id") Long idPedido, @ModelAttribute PedidoDetalhesRequest request) {

          return new ResponseEntity<>(this.pedidoDetalheService.listarDetalhesCarga(idPedido, request, page, size), HttpStatus.OK);
     }

     @Override
     @GetMapping(value = LISTA_PEDIDOS + LISTA_ERROS_ARQUIVO_PEDIDO, produces = APPLICATION_JSON_UTF8_VALUE)
     public ResponseEntity<?> listarDetalhesErros(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size, @PathVariable(required = true, name = "idArquivoPedido") Long idArquivoPedido) {

          return new ResponseEntity<>(this.pedidoService.listarDetalhesErros(page, size, idArquivoPedido), HttpStatus.OK);
     }

     @Override
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = CANCELAR_PEDIDOS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PUT)
     public ResponseEntity<?> cancelarPedido(@PathVariable Long id, @RequestParam(required = true) Long idUsuario) {

          return this.pedidoService.cancelarPedido(id, idUsuario);
     }

     @Override
     @RequestMapping(value = CARGAS + "/{id}" + FUNCIONARIOS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public ResponseEntity<?> listarDetalhesFuncionariosCarga(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size, @PathVariable(required = true, name = "id") Long idCargaBeneficio, @ModelAttribute PedidoDetalheFuncionarioRequest request) {

          return new ResponseEntity<>(this.pedidoDetalheService.listaDetalhesFuncionariosCarga(request, idCargaBeneficio, page, size), HttpStatus.OK);
     }

     @Override
     @RequestMapping(value = CANCELAR_PEDIDOS_DETALHES, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PUT)
     public ResponseEntity<?> cancelarPedidoDetalhe(@PathVariable(name = "idPedido") Long idPedido, @PathVariable("idCarga") Long idCarga, @RequestParam Long idUsuario) {

          this.pedidoDetalheService.cancelarPedidoDetalhes(idPedido, idCarga, idUsuario);

          return new ResponseEntity<>(HttpStatus.OK);
     }



}
