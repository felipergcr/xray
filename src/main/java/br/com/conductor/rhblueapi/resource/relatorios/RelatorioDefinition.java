
package br.com.conductor.rhblueapi.resource.relatorios;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.conductor.rhblueapi.domain.relatorios.response.RelatorioResponse;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioDeletarRequest;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioFilter;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = {"Relatórios"})
public interface RelatorioDefinition {
     
     @ApiOperation(value = "Solicitar relatório", notes = "Solicitar relatório")
     @ApiResponses(value = {
             @ApiResponse(code = 201, message = "Solicitação recebida com sucesso."),
             @ApiResponse(code = 404, message = "Usuário não encontrado."),
             @ApiResponse(code = 404, message = "Grupo empresa não localizado."),
             @ApiResponse(code = 400, message = "A data início não pode ser maior que a data atual."),
             @ApiResponse(code = 400, message = "A data fim não pode ser maior que a data atual."),
             @ApiResponse(code = 400, message = "A data fim do período não pode ser menor que a data início."),
             @ApiResponse(code = 400, message = "O intervalo do período solicitado não pode ser maior do que %s dias.")
     })
     public RelatorioResponse solicitar(@Valid @RequestBody RelatorioRequest relatorioRequest);
     
     @ApiOperation(value = "Listar relatórios", notes = "Listar relatórios")
     @ApiResponses(value = {
             @ApiResponse(code = 200, message = "Solicitação recebida com sucesso."),
             @ApiResponse(code = 404, message = "Usuário não encontrado."),
             @ApiResponse(code = 404, message = "Grupo empresa não localizado."),
             @ApiResponse(code = 400, message = "A data início não pode ser maior que a data atual."),
             @ApiResponse(code = 400, message = "A data fim não pode ser maior que a data atual."),
             @ApiResponse(code = 400, message = "A data fim do período não pode ser menor que a data início."),
             @ApiResponse(code = 400, message = "O intervalo do período solicitado não pode ser maior do que %s dias.")
     })
     public Page<RelatorioResponse> listar(@Valid RelatorioFilter relatorioFilter);

     @ApiOperation(value = "Deletar relatório(s)", notes = "Deletar relatório(s)")
     @ApiResponses(value = {
             @ApiResponse(code = 201, message = "Deleção realizada com sucesso."),
             @ApiResponse(code = 404, message = "Usuário não encontrado."),
             @ApiResponse(code = 404, message = "Grupo empresa não localizado."),
             @ApiResponse(code = 403, message = "O usuário e o grupo informados não possui acesso ao(s) relatório(s) informado(s)"),
     })
     public ResponseEntity<?> deletar(@Valid @RequestBody RelatorioDeletarRequest relatorioDeletarRequest);

     @ApiOperation(value = "Gerar relatório", notes = "Gerar relatório")
     @ApiResponses(value = {
             @ApiResponse(code = 200, message = "Solicitação recebida com sucesso."),
             @ApiResponse(code = 404, message = "Usuário não encontrado."),
             @ApiResponse(code = 404, message = "Grupo empresa não localizado."),
             @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação"),
             @ApiResponse(code = 404, message = "Não foi possível encontrar o relatório com os parâmetros informados"),
             @ApiResponse(code = 400, message = "O relatório precisa estar com o status processado para realizar a operação"),
             @ApiResponse(code = 404, message = "Não existe dados para o relatório solicitado")
     })
     public ResponseEntity<?> gerar(@PathVariable(name = "id") Long id, @Valid @ModelAttribute IdentificacaoRequest identificacaoRequest) throws IOException ;
}
