
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.REQUEST_COM_DADOS_INVALIDO;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_FUNCIONARIOS;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.conductor.rhblueapi.domain.Funcionario;
import br.com.conductor.rhblueapi.domain.request.funcionario.FuncionarioAtualizacaoRequest;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.service.acessos.FuncionarioAcessoService;
import br.com.conductor.rhblueapi.service.funcionario.FuncionarioService;

@RestController
@RequestMapping(value = PATH_FUNCIONARIOS, produces = APPLICATION_JSON_UTF8_VALUE)
public class FuncionarioResource implements FuncionarioDefinition {
     
     @Autowired
     private FuncionarioService funcionarioService;
     
     @Autowired
     private FuncionarioAcessoService funcionarioAcessoService;
     
     @PutMapping("/{id}")
     @ResponseStatus(HttpStatus.OK)
     @Override
     public ResponseEntity<?> atualizar(@PathVariable("id") final Long id, @RequestBody(required = true) @Valid final FuncionarioAtualizacaoRequest funcionarioAtualizacaoRequest) {
          
          List<String> erros = funcionarioAcessoService.validaFuncionarioAtualizacaoRequest(funcionarioAtualizacaoRequest);
          
          BadRequestCdt.checkThrow(CollectionUtils.isNotEmpty(erros), REQUEST_COM_DADOS_INVALIDO, erros.toString());
          
          Funcionario funcionario = funcionarioAcessoService.validaSePermiteAtualizacaoFuncionario(id, funcionarioAtualizacaoRequest);
          
          return ResponseEntity.ok(funcionarioService.atualizaDadosFuncionario(funcionario, funcionarioAtualizacaoRequest));
     }

}
