
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_403;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_412;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.PedidoResponse;
import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest;
import br.com.conductor.rhblueapi.domain.request.PedidoDetalheFuncionarioRequest;
import br.com.conductor.rhblueapi.domain.request.PedidoDetalhesRequest;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.PageArquivoCargasResponse;
import br.com.conductor.rhblueapi.domain.response.PageArquivoPedidoDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.response.PagePedidoDetalheResponse;
import br.com.conductor.rhblueapi.domain.response.PagePedidoFuncionariosDetalhesResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Pedidos" })
public interface PedidoDefinition {

     @ApiOperation(value = "Upload  de Arquivo", produces = "application/json", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
     @ApiResponses(value = { @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<PedidoResponse> uploadFile(@ModelAttribute IdentificacaoRequest usuarioGrupoResquest, @ApiParam(name = "file", value = "Selecione o arquivo XLSX ou XLS para Upload", required = true) @RequestPart("file") MultipartFile file);

     @ApiOperation(value = "Lista pedidos", notes = "Lista os pedidos")
     @ApiResponses(value = { @ApiResponse(code = 200, message = "Lista de pedidos", response = PageArquivoCargasResponse.class), @ApiResponse(code = 204, message = "Pesquisa não encontrada", response = ErroInfo.class), @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> listarPedidos(@ApiParam("page") Integer page, @ApiParam("size") Integer size, @Valid @ModelAttribute ArquivoPedidoRequest request);

     @ApiOperation(value = "Lista detalhes do pedido ", notes = "Este recurso permite listar as empresas que estão presentes em um pedido selecionado")
     @ApiResponses(value = { @ApiResponse(code = 200, message = "Lista de pedidos", response = PagePedidoDetalheResponse.class), @ApiResponse(code = 204, message = "Usuário não possuí acesso aos detalhes deste pedido", response = ErroInfo.class), @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> listarDetalhesCarga(@ApiParam("page") Integer page, @ApiParam("size") Integer size, @Valid @PathVariable("id") Long idPedido, @Valid @ModelAttribute PedidoDetalhesRequest request);

     @ApiOperation(value = "Lista detalhes erros", notes = "Lista os detalhes dos erros de um pedido")
     @ApiResponses(value = { @ApiResponse(code = 200, message = "Lista erros detalhados", response = PageArquivoPedidoDetalhesErrosReponse.class), @ApiResponse(code = 204, message = "Não existem detalhes para o pedido informado", response = ErroInfo.class), @ApiResponse(code = 204, message = "Não existem erros para o pedido informado", response = ErroInfo.class), @ApiResponse(code = 412, message = "O status do pedido é diferente de inválido", response = ErroInfo.class), @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class) })
     ResponseEntity<?> listarDetalhesErros(@ApiParam("page") Integer page, @ApiParam("size") Integer size, Long idArquivoPedido);

     @ApiOperation(value = "Cancelar pedido", notes = "Recurso para realizar cancelamento de pedido.", response = ArquivoCargas.class)
     @ApiResponses(value = { @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class), @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> cancelarPedido(@PathVariable Long id, @RequestParam(required = true) Long idUsuario);

     @ApiOperation(value = "Lista detalhes da carga benefício por funcionário", notes = "Este recurso permite listar os funcionários que receberam a carga benefício")
     @ApiResponses(value = { @ApiResponse(code = 200, message = "Lista de funcionários por carga", response = PagePedidoFuncionariosDetalhesResponse.class), @ApiResponse(code = 204, message = "Usuário não possuí acesso aos detalhes dessa carga", response = ErroInfo.class), @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> listarDetalhesFuncionariosCarga(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size, @PathVariable(required = true, name = "id") Long idCargaBeneficio, @ModelAttribute PedidoDetalheFuncionarioRequest request);

     @ApiOperation(value = "Cancelar pedido detalhes", notes = "Recurso para realizar cancelamento de pedido detalhe.", response = ArquivoCargas.class)
     @ApiResponses(value = { @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class), @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404), @ApiResponse(code = 403, message = MENSAGEM_GLOBAL_403, response = ErroInfo.class), @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> cancelarPedidoDetalhe(@PathVariable Long idPedido, @PathVariable Long idCarga, @ApiParam(required = true) Long idUsuario);

}
