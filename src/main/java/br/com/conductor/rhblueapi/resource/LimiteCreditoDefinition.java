
package br.com.conductor.rhblueapi.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;

import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Limite de Crédito" })
public interface LimiteCreditoDefinition {

     @ApiOperation(value = "Dados do limite de crédito", notes = "Informa os dados do limite de crédito para os grupos pós pago que não estão na lista de exceção para consideração dessa regra")
     @ApiResponses({ @ApiResponse(code = 404, message = "Usuário não existe."), 
          @ApiResponse(code = 404, message = "Grupo empresa não encontrado."), 
          @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação")
     })
     ResponseEntity<?> obterLimiteCredito(@ModelAttribute IdentificacaoRequest identificacaoRequest);
}
