package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_204;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.request.OperacaoAcessoRequest;
import br.com.conductor.rhblueapi.domain.response.PageOperacaoAcessoResponse;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.util.AppConstantes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = AppConstantes.PATH_USUARIOS_OPERACAO,produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Usuário" })
public interface OperacaoAcessoDefinition {
   
     @ApiOperation(value = "Listar Operações do Sistema",notes="Listar Operações do Sistema" ,response = PageOperacaoAcessoResponse.class  )
     @ApiResponses({
          @ApiResponse(code = 204, message = MENSAGEM_GLOBAL_204, response = NoContentCdt.class), 
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> listarOperacoesAcesso(@RequestParam(name="page",defaultValue = "0") Integer page,
               @RequestParam(name="size",defaultValue = "50") Integer size,
               @ModelAttribute OperacaoAcessoRequest request) throws JsonParseException, JsonMappingException, IOException;


}
