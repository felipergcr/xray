package br.com.conductor.rhblueapi.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.conductor.rhblueapi.domain.persist.UsuarioNivelPermissaoRhPersist;
import br.com.conductor.rhblueapi.domain.request.UsuarioNivelPermissaoRhRequest;
import br.com.conductor.rhblueapi.domain.response.PageUsuarioNivelPermissaoRHResponse;
import br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService;
import br.com.conductor.rhblueapi.util.AppConstantes;

@RestController
public class UsuarioNivelPermissaoRhResource implements UsuarioNivelPermissaoRhDefinition {

  @Autowired
  private UsuarioNivelPermissaoRhService service;

  @Override
  @ResponseStatus(HttpStatus.OK)
  @PutMapping(value = AppConstantes.PATH_PERMISSOES_RH + "/{id}" + AppConstantes.PATH_USUARIOS_ACESSO_RH,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<String> salvar(@ModelAttribute @Valid UsuarioNivelPermissaoRhPersist persist,@PathVariable(value = "id", required = true) Long idPermissao) {

    return new ResponseEntity<String>(service.salvarNivelPermissaoUsuario(persist, idPermissao), HttpStatus.OK);

  }

  @Override
  @ResponseStatus(HttpStatus.OK)
  @GetMapping(value = AppConstantes.PATH_PERMISSOES_RH + AppConstantes.PATH_USUARIOS_ACESSO_RH,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<PageUsuarioNivelPermissaoRHResponse> listar(@ModelAttribute UsuarioNivelPermissaoRhRequest request, @RequestParam(defaultValue = "0") Integer page,@RequestParam(defaultValue = "50") Integer size) {

    return new ResponseEntity<PageUsuarioNivelPermissaoRHResponse>(service.listarNivelPermissaoUsuario(request, page, size), HttpStatus.OK);
  }

}
