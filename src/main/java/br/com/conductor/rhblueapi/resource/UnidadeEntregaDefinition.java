
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.request.UnidadeEmpresaRequest;
import br.com.conductor.rhblueapi.domain.request.ArquivoUnidadeEntregaDetalheErroRequest;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.PageArquivoUnidadeEntregaDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.domain.response.ArquivoUnidadeEntregaDetalheErroReponse;
import br.com.conductor.rhblueapi.domain.response.UnidadeEntregaResponse;
import br.com.conductor.rhblueapi.exception.BusinessException;
import br.com.conductor.rhblueapi.exception.ObjetoVazioException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Unidades de Entregas" })
public interface UnidadeEntregaDefinition {

     @ApiOperation(value = "Upload  de Arquivo", produces = "application/json", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
     @ApiResponses(value = { 
               @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), 
               @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<UnidadeEntregaResponse> uploadFile(@ModelAttribute IdentificacaoRequest usuarioGrupoResquest, @ApiParam(name = "file", value = "Selecione o arquivo XLSX ou XLS para Upload", required = true) @RequestPart("file") MultipartFile file) throws ObjetoVazioException, BusinessException;

     @ApiOperation(value = "Lista unidades de entrega", notes = "Lista os pedidos")
     @ApiResponses(value = { 
               @ApiResponse(code = 404, message = "Usuário não encontrado", response = ErroInfo.class), 
               @ApiResponse(code = 404, message = "Grupo empresa não localizado.", response = ErroInfo.class),
               @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação", response = ErroInfo.class) })
     ResponseEntity<?> listarUnidadeEntrega(@ApiParam("page") Integer page, @ApiParam("size") Integer size, @Valid @ModelAttribute UnidadeEmpresaRequest request);
     
     @ApiOperation(value = "Lista os erros no upload de arquivo de unidades de entrega", notes = "Lista erros em arquivo de unidade de entrega ")
     @ApiResponses(value = { 
               @ApiResponse(code = 404, message = "Usuário não encontrado", response = ErroInfo.class),
               @ApiResponse(code = 404, message = "Grupo Empresa não localizado", response = ErroInfo.class), 
               @ApiResponse(code = 404, message = "Arquivo de unidades de entrega não localizado", response = ErroInfo.class), 
               @ApiResponse(code = 403, message = "Usuário da sessão não possui vínculo para realizar essa operação", response = ErroInfo.class),
               @ApiResponse(code = 412, message = "Não foi encontrado nenhum arquivo com o identificador informado", response = ErroInfo.class),
               @ApiResponse(code = 412, message = "O status do cadastro da unidade de entrega deve ser igual Inválidado ou Processamento Concluído", response = ErroInfo.class),
               @ApiResponse(code = 412, message = "A paginação não pode ser negativa", response = ErroInfo.class)})
     ResponseEntity<PageArquivoUnidadeEntregaDetalhesErrosReponse> listarArquivoUnidadeEntregaDetalheErro(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size, @PathVariable(name = "id" , required = true) Long idArquivoUnidadeEntrega, @Valid @ModelAttribute ArquivoUnidadeEntregaDetalheErroRequest request);
}
