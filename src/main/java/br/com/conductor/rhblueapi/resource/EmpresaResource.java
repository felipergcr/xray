package br.com.conductor.rhblueapi.resource;


import br.com.conductor.rhblueapi.domain.persist.EmpresaPersist;
import br.com.conductor.rhblueapi.domain.request.EmpresaRequest;
import br.com.conductor.rhblueapi.domain.response.EmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.PageEmpresaCustomResponse;
import br.com.conductor.rhblueapi.domain.update.EmpresaUpdate;
import br.com.conductor.rhblueapi.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_EMPRESAS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_SUBGRUPO_EMPRESAS;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class EmpresaResource implements EmpresaDefinition {

     @Autowired
     private EmpresaService empresaService;

     @Override
     @ResponseStatus(HttpStatus.CREATED)
     @RequestMapping(value = PATH_SUBGRUPO_EMPRESAS + "/{id}" + PATH_EMPRESAS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
     public ResponseEntity<EmpresaResponse> cadastrarEmpresa(
               @PathVariable(required = true , name="id")  Long idSubGrupo,
               @RequestBody(required = true) @Valid EmpresaPersist persist) throws IOException {

          return new ResponseEntity<>(this.empresaService.cadastraEmpresa(idSubGrupo, persist), HttpStatus.CREATED);
     }

     @Override
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = PATH_EMPRESAS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public ResponseEntity<?> listarEmpresa(EmpresaRequest empresaRequest) {
          return new ResponseEntity<>(this.empresaService.listar(empresaRequest), OK);
     }

     @Override
     @RequestMapping(value = PATH_SUBGRUPO_EMPRESAS + "/{id}" + PATH_EMPRESAS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public ResponseEntity<PageEmpresaCustomResponse> listarEmpresasdoSubGrupoEmpresa(@PathVariable (required = true , name="id") Long idSubgrupoEmpresa,
               @RequestParam(name="page",defaultValue = "0") Integer page,@RequestParam(name="size",defaultValue = "50") Integer size ) {

          return ResponseEntity.ok(empresaService.listarEmpresasdoSubGrupoEmpresa(idSubgrupoEmpresa, page, size));
     }

     @Override
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = PATH_SUBGRUPO_EMPRESAS + "/{id}" + PATH_EMPRESAS + "/{idEmpresa}", produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PATCH)
     public ResponseEntity<?> atualizarEmpresa(
               @PathVariable (required = true , name="id") Long idSubgrupoEmpresa,
               @PathVariable(required = true , name="idEmpresa") Long idEmpresa,
               @Valid @RequestBody EmpresaUpdate empresaUpdate) {

          EmpresaResponse response = empresaService.atualizar(empresaUpdate, idSubgrupoEmpresa, idEmpresa);
          return new ResponseEntity<>(response, OK);
     }
     
          @Override
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = PATH_SUBGRUPO_EMPRESAS + "/{id}" + PATH_EMPRESAS + "/{idEmpresa}", produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PUT)
     public ResponseEntity<?> atualizarEmpresaPut(
               @PathVariable (required = true , name="id") Long idSubgrupoEmpresa,
               @PathVariable(required = true , name="idEmpresa") Long idEmpresa,
               @Valid @RequestBody EmpresaUpdate empresaUpdate) {

          EmpresaResponse response = empresaService.atualizar(empresaUpdate, idSubgrupoEmpresa, idEmpresa);
          return new ResponseEntity<>(response, OK);
     }

}
