package br.com.conductor.rhblueapi.resource;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.persist.EmpresaPersist;
import br.com.conductor.rhblueapi.domain.request.EmpresaRequest;
import br.com.conductor.rhblueapi.domain.response.EmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.PageEmpresaCustomResponse;
import br.com.conductor.rhblueapi.domain.update.EmpresaUpdate;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.management.BadAttributeValueExpException;
import javax.validation.Valid;
import java.io.IOException;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Empresa" })
public interface EmpresaDefinition {

	@ApiOperation(value = "Cadastro de Empresas", notes = "Cria empresa", response = EmpresaResponse.class)
	@ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
			@ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
			@ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
			@ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
	ResponseEntity<EmpresaResponse> cadastrarEmpresa(
			@ApiParam(value = "Identificador do Grupo Empresa", required = true)
			@PathVariable Long id,
			@Valid @RequestBody EmpresaPersist persit)
			throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;

	@ApiOperation(value = "Listar empresa", notes = "Listar empresa",response = EmpresaResponse.class, responseContainer="List")
	@ApiResponses({
			@ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
			@ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
			@ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
			@ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
	ResponseEntity<?> listarEmpresa(@Valid EmpresaRequest empresaRequest);


	@ApiOperation(value = "Listar empresas por identificador do subgrupo empresa", response = PageEmpresaCustomResponse.class,notes = "Listar empresas")
	@ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
			@ApiResponse(code = 204, message = "Pesquisa não encotrada ", response = ErroInfo.class ),
			@ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
			@ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
			@ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
	ResponseEntity<?> listarEmpresasdoSubGrupoEmpresa(@Valid Long idGrupoEmpresa, @ApiParam("page") Integer page,@ApiParam("size") Integer size) throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;

	/**
	 * atualizarEmpresa
	 * método responsável por realizar atualização cadastral de empresas
	 *
	 * @param idGrupoEmpresa
	 * @param idEmpresa
	 * @param empresaUpdate
	 * @return
	 * @throws IOException
	 */
	@ApiOperation(value = "Atualizar empresa", notes = "Atualizar empresa", response = EmpresaResponse.class)
	@ApiResponses({
			@ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
			@ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
			@ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
			@ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
	ResponseEntity<?> atualizarEmpresa(@Valid Long idGrupoEmpresa, @PathVariable Long idEmpresa, @Valid @RequestBody EmpresaUpdate empresaUpdate);
	
	/**
      * atualizarEmpresa
      * método responsável por realizar atualização cadastral de empresas
      *
      * @param idGrupoEmpresa
      * @param idEmpresa
      * @param empresaUpdate
      * @return
      * @throws IOException
      */
     @ApiOperation(value = "Atualizar empresa", notes = "Atualizar empresa", response = EmpresaResponse.class)
     @ApiResponses({
               @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
               @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
               @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
               @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> atualizarEmpresaPut(@Valid Long idGrupoEmpresa, @PathVariable Long idEmpresa, @Valid @RequestBody EmpresaUpdate empresaUpdate);
}
