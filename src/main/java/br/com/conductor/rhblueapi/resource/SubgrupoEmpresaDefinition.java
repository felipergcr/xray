package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_204;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_400;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_412;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import javax.management.BadAttributeValueExpException;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.persist.SubgrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.request.SubgrupoEmpresaRequest;
import br.com.conductor.rhblueapi.domain.response.PageSubgrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.SubgrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.update.SubgrupoEmpresaUpdate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Subgrupo Empresa" })
public interface SubgrupoEmpresaDefinition {

	
	
	@ApiOperation(value = "Lista subgrupos", notes = "Lista subgrupos")
	@ApiResponses({
	     @ApiResponse(code = 204, message = MENSAGEM_GLOBAL_204, response = ErroInfo.class),
	     @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
	     @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
	     @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     PageSubgrupoEmpresaResponse listaSubgrupo(Long idGrupoEmpresa, Integer page, Integer size, SubgrupoEmpresaRequest subgrupoEmpresaRequest);

	
	@ApiOperation(value = "Atualizar Subgrupos Empresa", notes = "Atualizar Subgrupo Empresa" )
	@ApiResponses({
          @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
          @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
	ResponseEntity<?> atualizarSubgrupoEmpresa(Long idGrupo, Long idSubgrupo, @RequestBody SubgrupoEmpresaUpdate request);
	
	@ApiOperation(value = "Atualizar Subgrupos Empresa", notes = "Atualizar Subgrupo Empresa" )
     @ApiResponses({
          @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
          @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> atualizarSubgrupoEmpresaPut(Long idGrupo, Long idSubgrupo, @RequestBody SubgrupoEmpresaUpdate request);
	
	@ApiOperation(tags = { "Subgrupo Empresa" },value = "Cadastro de Subgrupos", notes = "Cadastro de Subgrupos", response = SubgrupoEmpresaResponse.class)
     @ApiResponses({ 
          @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
          @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
          @ApiResponse(code = 412, message = MENSAGEM_GLOBAL_412, response = ErroInfo.class),
          @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<SubgrupoEmpresaResponse> cadastrarSubgrupo(@PathVariable("id") Long idGrupoPai, @Valid @RequestBody SubgrupoEmpresaPersist request)     throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;


}
