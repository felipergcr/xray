
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_400;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;

import java.io.IOException;

import javax.management.BadAttributeValueExpException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.response.AcessoNiveisUsuarioResponse;
import br.com.conductor.rhblueapi.enums.Plataforma;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = { "Usuário" })
public interface AcessoNiveisUsuarioDefinition {

     @ApiOperation(tags = { "Usuário" }, value = "Níveis de Acesso", notes = "Lista os níveis de acesso")
     @ApiResponses({ @ApiResponse(code = 204, message = "Pesquisa não encotrada ", response = AcessoNiveisUsuarioResponse.class), @ApiResponse(code = 200, message = "Lista de níveis encontrada", response = AcessoNiveisUsuarioResponse.class), @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class), @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> listarNiveisAcessoUsuario(@RequestParam(name = "plataforma") Plataforma plataforma) throws JsonParseException, JsonMappingException, IOException, BadAttributeValueExpException;

}
