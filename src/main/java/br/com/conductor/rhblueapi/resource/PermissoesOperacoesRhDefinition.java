package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_200;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_204;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_400;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.persist.PermissoesOperacoesRhPersist;
import br.com.conductor.rhblueapi.domain.request.PermissoesOperacoesRhResquest;
import br.com.conductor.rhblueapi.domain.response.PagePermissaoOperacaoRhResponse;
import br.com.conductor.rhblueapi.domain.response.PermissaoOperacaoRhResponse;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = {"Permissão"})
public interface PermissoesOperacoesRhDefinition {

  @ApiOperation(value = "Vincula permissão a operação", notes = "Vincula permissão a operação",
      response = PermissaoOperacaoRhResponse.class)
  @ApiResponses({@ApiResponse(code = 200, message = MENSAGEM_GLOBAL_200, response = PermissaoOperacaoRhResponse.class),
      @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
      @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class)})
  ResponseEntity<?> vincularPermissaoOpercaoRh(final PermissoesOperacoesRhPersist persist, @PathVariable final Long id);

  @ApiOperation(value = "Listar permissão e operação de usuário", notes = "Listar permissão e operação de usuário",
      response = PermissaoOperacaoRhResponse.class)
  @ApiResponses({
      @ApiResponse(code = 200, message = MENSAGEM_GLOBAL_200, response = PagePermissaoOperacaoRhResponse.class),
      @ApiResponse(code = 204, message = MENSAGEM_GLOBAL_204, response = ErroInfo.class),
      @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class)})
  ResponseEntity<?> listarPermissaoOpercaoRh(final Long id, final PermissoesOperacoesRhResquest request, Integer page, Integer size);

}
