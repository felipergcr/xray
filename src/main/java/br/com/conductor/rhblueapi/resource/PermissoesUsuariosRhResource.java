
package br.com.conductor.rhblueapi.resource;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.persist.PermissoesRhPersist;
import br.com.conductor.rhblueapi.domain.request.PermissoesRhRequest;
import br.com.conductor.rhblueapi.domain.update.PermissoesRhUpdate;
import br.com.conductor.rhblueapi.domain.response.PagePermissoesRhResponse;
import br.com.conductor.rhblueapi.domain.response.PermissoesRhResponse;
import br.com.conductor.rhblueapi.service.PermissoesUsuariosRhService;
import br.com.conductor.rhblueapi.util.AppConstantes;

@RestController
public class PermissoesUsuariosRhResource implements PermissoesUsuariosRhDefinition {

     @Autowired
     private PermissoesUsuariosRhService permissoesService;

     @Override
     @ResponseStatus(CREATED)
     @PostMapping(value = AppConstantes.PATH_PERMISSOES_RH, produces = APPLICATION_JSON_UTF8_VALUE)
     public ResponseEntity<PermissoesRhResponse> salvar(@RequestBody(required = true) @Valid PermissoesRhPersist persist) throws JsonParseException, JsonMappingException, IOException {

          PermissoesRhResponse response = permissoesService.salvar(persist, true);
          return new ResponseEntity<PermissoesRhResponse>(response, CREATED);
     }

     @Override
     @ResponseStatus(OK)
     @PatchMapping(value = AppConstantes.PATH_PERMISSOES_RH + "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
     public ResponseEntity<PermissoesRhResponse> update(@PathVariable(value = "id") Long id, @RequestBody(required = true) @Valid PermissoesRhUpdate update) throws JsonParseException, JsonMappingException, IOException {

          PermissoesRhResponse response = permissoesService.alterar(id, update);
          return new ResponseEntity<PermissoesRhResponse>(response, OK);
     }

     @Override
     @ResponseStatus(value = OK)
     @GetMapping(value = AppConstantes.PATH_PERMISSOES_RH)
     public ResponseEntity<?> listar(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "50") Integer size, @Valid @ModelAttribute  PermissoesRhRequest request) throws JsonParseException, JsonMappingException, IOException {

          PagePermissoesRhResponse response = permissoesService.listar(page, size, request);
          return new ResponseEntity<PagePermissoesRhResponse>(response, HttpStatus.OK);
     }
}
