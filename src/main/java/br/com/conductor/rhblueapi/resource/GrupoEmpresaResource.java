package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.util.AppConstantes.HIERARQUIA_ORGANIZACIONAL;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_GRUPO_EMPRESAS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_PARAMETROS;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.response.GrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaOrganizacionalGrupoResponse;
import br.com.conductor.rhblueapi.domain.update.GrupoEmpresaUpdate;
import br.com.conductor.rhblueapi.service.GrupoEmpresaService;
import br.com.conductor.rhblueapi.service.HierarquiaOrganizacionalService;
import br.com.conductor.rhblueapi.service.ParametrosService;
import br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService;
import br.com.conductor.rhblueapi.service.grupoempresa.CadastroGrupoEmpresaValidacaoService;

@RestController
public class GrupoEmpresaResource implements GrupoEmpresaDefinition {
     
     @Autowired
     private CadastroGrupoEmpresaValidacaoService cadastroGrupoEmpresaValidacaoService;

     @Autowired
     private GrupoEmpresaService grupoEmpresaServico;

     @Autowired
     private ParametrosService parametrosService;

     @Autowired
     private HierarquiaOrganizacionalService hierarquiaOrganizacionalService;
     
     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;
     
     @Override
     @ResponseStatus(HttpStatus.CREATED)
     @RequestMapping(value = PATH_GRUPO_EMPRESAS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
     public ResponseEntity<GrupoEmpresaResponse> cadastrarGrupo(@Valid @RequestBody GrupoEmpresaPersist persist) throws JsonParseException, JsonMappingException, IOException {
          
          cadastroGrupoEmpresaValidacaoService.validaRequest(persist);

          return new ResponseEntity<GrupoEmpresaResponse>(this.grupoEmpresaServico.cadastrar(persist), HttpStatus.CREATED);
     }

     @Override
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value = PATH_GRUPO_EMPRESAS + "/{id}", produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PATCH)
     public ResponseEntity<GrupoEmpresaResponse> atualizarGrupoEmpresa(@PathVariable("id") final Long idGrupoEmpresa, @RequestBody(required = true) @Valid final GrupoEmpresaUpdate update) {
          
          cadastroGrupoEmpresaValidacaoService.validaRequest(update);

          GrupoEmpresaResponse response = this.grupoEmpresaServico.atualizarGrupoEmpresa(idGrupoEmpresa, update);

          return new ResponseEntity<GrupoEmpresaResponse>(response, OK);
     }
     
     @Override
     @ResponseStatus(HttpStatus.OK)
     @RequestMapping(value =  PATH_GRUPO_EMPRESAS + "/{id}", produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.PUT)
     public ResponseEntity<GrupoEmpresaResponse> atualizarGrupoEmpresaPut(@PathVariable("id") final Long idGrupoEmpresa, @RequestBody(required = true) @Valid final GrupoEmpresaUpdate update) {
          
          cadastroGrupoEmpresaValidacaoService.validaRequest(update);

          GrupoEmpresaResponse response = this.grupoEmpresaServico.atualizarGrupoEmpresa(idGrupoEmpresa, update);

          return new ResponseEntity<GrupoEmpresaResponse>(response, OK);
     }

     @Override
     @RequestMapping(value = PATH_GRUPO_EMPRESAS + "/{id}" + HIERARQUIA_ORGANIZACIONAL, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public HierarquiaOrganizacionalGrupoResponse listarHierarquiaOrganizacionalGrupo(@PathVariable("id") Long idGrupoEmpresa, @RequestParam(required = true) Long idUsuarioSessao) {

          return this.hierarquiaOrganizacionalService.listarHierarquiaOrganizacionalGrupoResponse(idGrupoEmpresa, idUsuarioSessao);
     }

     @Override
     @RequestMapping(value = PATH_GRUPO_EMPRESAS +"/{id}"+ PATH_PARAMETROS, produces = APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
     public ResponseEntity<?> listaParametrosGrupoEmpresa(@PathVariable("id") Long id, @RequestParam(value="id-usuario", required = true) Long idUsuario) {
          
          usuarioNivelPermissaoRhService.usuarioPossuiNivelHierarquiaNoGrupo(idUsuario, id);

          return new ResponseEntity<String>(parametrosService.listaParametrosPorIdGrupoEmpresa(id), OK);
     }

}