package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_200;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_201;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_204;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_400;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_409;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import javax.websocket.server.PathParam;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.persist.UsuarioNivelPermissaoRhPersist;
import br.com.conductor.rhblueapi.domain.request.UsuarioNivelPermissaoRhRequest;
import br.com.conductor.rhblueapi.domain.response.PageUsuarioNivelPermissaoRHResponse;
import br.com.conductor.rhblueapi.domain.response.UsuarioNivelPermissaoRhResponse;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = {"Permissão"})
public interface UsuarioNivelPermissaoRhDefinition {


  @ApiOperation(value = "Define nível de acesso da permissão de usuário",produces = "application/json")
  @ApiResponses({@ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
      @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
      @ApiResponse(code = 403, message = MENSAGEM_GLOBAL_409, response = ErroInfo.class),
      @ApiResponse(code = 201, message = MENSAGEM_GLOBAL_201, response = UsuarioNivelPermissaoRhResponse.class)})
  public ResponseEntity<?> salvar(@ModelAttribute UsuarioNivelPermissaoRhPersist persist, @PathParam(value = "id") Long idNPermissao);

  @ApiOperation(value = "Listar nível de permissão acesso de usuário",produces = "application/json")
  @ApiResponses({
      @ApiResponse(code = 200, message = MENSAGEM_GLOBAL_200, response = PageUsuarioNivelPermissaoRHResponse.class),
      @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
      @ApiResponse(code = 204, message = MENSAGEM_GLOBAL_204, response = NoContentCdt.class)})
  public ResponseEntity<?> listar(@ModelAttribute UsuarioNivelPermissaoRhRequest request, @RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "50") Integer size);
}
