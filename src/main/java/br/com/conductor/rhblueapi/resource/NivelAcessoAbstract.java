
package br.com.conductor.rhblueapi.resource;

import br.com.conductor.rhblueapi.domain.persist.HierarquiaAcessoUpdatePersist;

public abstract class NivelAcessoAbstract {
     
     public abstract void salvarNivelAcesso(HierarquiaAcessoUpdatePersist persist, Long idPermisso);

}
