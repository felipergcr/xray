
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_204;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_400;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_404;
import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.MENSAGEM_GLOBAL_500;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.domain.persist.PermissoesRhPersist;
import br.com.conductor.rhblueapi.domain.request.PermissoesRhRequest;
import br.com.conductor.rhblueapi.domain.update.PermissoesRhUpdate;
import br.com.conductor.rhblueapi.domain.response.PagePermissoesRhResponse;
import br.com.conductor.rhblueapi.domain.response.PermissoesRhResponse;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.util.AppConstantes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = AppConstantes.PATH_PERMISSOES_RH, produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Permissão" })
public interface PermissoesUsuariosRhDefinition {

     @ApiOperation(value = "Efetiva cadastro da permissão do usuário/grupo empresa", notes = "Efetiva cadastro da permissão do usuário/grupo empresa", response = PermissoesRhResponse.class)
     @ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class), 
                     @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), 
                     @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<PermissoesRhResponse> salvar(@RequestBody(required = true) PermissoesRhPersist persist) throws JsonParseException, JsonMappingException, IOException;

     @ApiOperation(value = "Efetiva alteração da permissão do usuário/grupo empresa", notes = "Efetiva alteração da permissão do usuário/grupo empresa", response = PermissoesRhResponse.class)
     @ApiResponses({ @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class), 
                     @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class), 
                     @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<PermissoesRhResponse> update(@PathVariable(value = "id") Long id, @RequestBody(required = true) PermissoesRhUpdate persist) throws JsonParseException, JsonMappingException, IOException;

     @ApiOperation(value = "Lista as permissões do usuário/grupo empresa", response = PagePermissoesRhResponse.class)
     @ApiResponses({ @ApiResponse(code = 204, message = MENSAGEM_GLOBAL_204, response = NoContentCdt.class), 
                     @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<?> listar(@ApiParam(value = "Page") Integer page, @ApiParam(value = "Size") Integer size, @ModelAttribute PermissoesRhRequest request) throws JsonParseException, JsonMappingException, IOException;

}
