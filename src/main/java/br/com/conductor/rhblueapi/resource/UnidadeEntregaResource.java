
package br.com.conductor.rhblueapi.resource;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.PAGINACAO_NEGATIVA;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.LISTA_ERROS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_ARQUIVOS;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_UNIDADES_ENTREGA;
import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_UPLOAD;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.reflect.TypeToken;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.UnidadeEntrega;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.request.ArquivoUnidadeEntregaDetalheErroRequest;
import br.com.conductor.rhblueapi.domain.request.UnidadeEmpresaRequest;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.PageArquivoUnidadeEntregaDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.response.PageUnidadeEntregaResponse;
import br.com.conductor.rhblueapi.domain.response.UnidadeEntregaResponse;
import br.com.conductor.rhblueapi.enums.StatusUnidadesEntrega;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.exception.BusinessException;
import br.com.conductor.rhblueapi.exception.ObjetoVazioException;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.service.ArquivoUnidadeEntregaDetalheErroService;
import br.com.conductor.rhblueapi.service.ArquivoUnidadeEntregaService;
import br.com.conductor.rhblueapi.service.UnidadeEntregaService;
import br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.GenericConvert;
import br.com.conductor.rhblueapi.util.PageUtils;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = PATH_UNIDADES_ENTREGA, produces = APPLICATION_JSON_UTF8_VALUE)
public class UnidadeEntregaResource implements UnidadeEntregaDefinition {

     @Autowired
     private UnidadeEntregaService unidadeEntregaService;
     
     @Autowired
     private ArquivoUnidadeEntregaService arquivoUnidadeEntregaService;
     
     @Autowired
     private ArquivoUnidadeEntregaDetalheErroService arquivoUnidadeEntregaDetalheErroService;
     
     @Autowired
     private StatusCached statusCached;
     
     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;
     
     private final String ORDER_BY_COD_UNID_ENTREGA = "codigoUnidadeEntrega";

     @Override
     @ResponseStatus(HttpStatus.CREATED)
     @PostMapping(value = PATH_UPLOAD)
     public ResponseEntity<UnidadeEntregaResponse> uploadFile(@ModelAttribute IdentificacaoRequest usuarioGrupoResquest, @ApiParam(name = "file", value = "Selecione o arquivo XLSX ou XLS para Upload", required = true) @RequestPart(required = true) MultipartFile file) throws ObjetoVazioException, BusinessException {

          return new ResponseEntity<UnidadeEntregaResponse>(this.arquivoUnidadeEntregaService.uploadFile(usuarioGrupoResquest, file), CREATED);
     }

     @Override
     @GetMapping
     public ResponseEntity<PageUnidadeEntregaResponse> listarUnidadeEntrega(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size, @ModelAttribute UnidadeEmpresaRequest request) {

          this.unidadeEntregaService.validarUnidadeEmpresaRequest(request);

          List<StatusDescricao> dominioStatusUnidadeEntrega = this.statusCached.busca(TipoStatus.UNIDADES_ENTREGA.getNome());
          final StatusDescricao statusAtivo = mapStatusDescricao(dominioStatusUnidadeEntrega, StatusUnidadesEntrega.ATIVO.getDescricao());
          
          Direction direction = Sort.Direction.ASC;
          Pageable pageable = PageRequest.of(page, size, direction, ORDER_BY_COD_UNID_ENTREGA);
          UnidadeEntrega unidadeEntrega = UnidadeEntrega.builder().codigoUnidadeEntrega(request.getCodUnidadeEntrega()).idGrupoEmpresa(request.getIdGrupoEmpresa()).status(statusAtivo.getStatus()).build();
          Page<UnidadeEntrega> retorno = this.unidadeEntregaService.listarUnidadeEntregaPaginado(pageable, unidadeEntrega);

          PageImpl<UnidadeEntrega> pageImplResponse = new PageImpl<>(retorno.getContent(), pageable, retorno.getTotalElements());

          PageUnidadeEntregaResponse pageResponse = new PageUnidadeEntregaResponse(GenericConvert.convertModelMapperToPageResponse(pageImplResponse, new TypeToken<List<UnidadeEntrega>>() {
          }.getType()));

          if (retorno.getContent().isEmpty()) {
               return new ResponseEntity<>(pageResponse, HttpStatus.NO_CONTENT);
          } else {
               return new ResponseEntity<>(pageResponse, HttpStatus.OK);
          }
     }
     
     @Override
     @GetMapping(value = PATH_ARQUIVOS + LISTA_ERROS)
     @ResponseStatus(HttpStatus.OK)
     public ResponseEntity<PageArquivoUnidadeEntregaDetalhesErrosReponse> listarArquivoUnidadeEntregaDetalheErro(@RequestParam(name = "page", defaultValue = "0") Integer page, @RequestParam(name = "size", defaultValue = "10") Integer size, @PathVariable(name= "id", required = true) Long idArquivoUnidadeEntrega, @Valid ArquivoUnidadeEntregaDetalheErroRequest request) {

          usuarioNivelPermissaoRhService.autenticarNivelAcessoRhGrupo(request.getIdUsuarioLogado(), request.getIdGrupoEmpresa());

          arquivoUnidadeEntregaService.validarStatusIgualAConcluidoOuInvalidado(idArquivoUnidadeEntrega);

          PreconditionCustom.checkThrow(PageUtils.isPageWithNegativeNumber(page, size), PAGINACAO_NEGATIVA);
          Pageable pageable = PageRequest.of(page, size);
          return new ResponseEntity<>(arquivoUnidadeEntregaDetalheErroService.buscarErrosUploadArquivoUnidadeEntrega(idArquivoUnidadeEntrega, pageable), HttpStatus.OK);
     }

}