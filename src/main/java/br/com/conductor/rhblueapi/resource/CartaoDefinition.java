package br.com.conductor.rhblueapi.resource;

import br.com.conductor.rhblueapi.domain.CartaoStatusRequest;
import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;

import static br.com.conductor.rhblueapi.exception.GlobalExceptionHandler.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@Api(produces = APPLICATION_JSON_UTF8_VALUE, tags = { "Cartões" })
public interface CartaoDefinition {

     @ApiOperation(value = "Alterar Status do Cartão", notes = "Recurso Utilizado para alterar o do status do cartão, como por exemplo: CANCELADO.")
     @ApiResponses({
             @ApiResponse(code = 400, message = MENSAGEM_GLOBAL_400, response = ErroInfo.class),
             @ApiResponse(code = 404, message = MENSAGEM_GLOBAL_404, response = ErroInfo.class),
             @ApiResponse(code = 500, message = MENSAGEM_GLOBAL_500, response = ErroInfo.class) })
     ResponseEntity<Void> alterarStatusCartao(
             @ApiParam(value = "Identificador do cartão", name = "id", example = "10" , required = true) Long id,
             @ApiParam(value = "Objeto Request CartaoStatusRequest" , required = true) CartaoStatusRequest cartaoStatus);

}
