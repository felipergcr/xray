
package br.com.conductor.rhblueapi.resource;

import br.com.conductor.rhblueapi.domain.CartaoStatusRequest;
import br.com.conductor.rhblueapi.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static br.com.conductor.rhblueapi.util.AppConstantes.PATH_CARTOES_ALTERAR_STATUS;

@RestController
public class CartaoResource implements CartaoDefinition {

     @Autowired
     private CartaoService cartaoService;

     /***
      * alterarStatus
      *
      * @param id
      * @param cartaoStatus
      * @return ResponseEntity<Void>
      */
     @Override
     @ResponseStatus(HttpStatus.OK)
     @PutMapping(value = PATH_CARTOES_ALTERAR_STATUS)
     public ResponseEntity<Void> alterarStatusCartao(
             @PathVariable("id") Long id,
             @Valid @RequestBody CartaoStatusRequest cartaoStatus) {

          cartaoService.alterarStatusCartao(id, cartaoStatus);
          return new ResponseEntity<>(HttpStatus.OK);
     }

}
