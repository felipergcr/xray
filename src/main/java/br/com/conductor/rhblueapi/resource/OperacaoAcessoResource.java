package br.com.conductor.rhblueapi.resource;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.request.OperacaoAcessoRequest;
import br.com.conductor.rhblueapi.domain.response.OperacaoAcessoResponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.service.OperacaoAcessoService;
import br.com.conductor.rhblueapi.util.AppConstantes;

@RestController
@RequestMapping(value = AppConstantes.PATH_USUARIOS)
public class OperacaoAcessoResource implements OperacaoAcessoDefinition {
     
     @Autowired
     private OperacaoAcessoService operacaoAcessoService;
     
     @Override
     @ResponseStatus(value = HttpStatus.OK)
     @GetMapping(value = AppConstantes.PATH_USUARIOS_OPERACAO)
     public ResponseEntity<?> listarOperacoesAcesso(@RequestParam(name="page",defaultValue = "0") Integer page,
                                                             @RequestParam(name="size",defaultValue = "50") Integer size,
                                                             @ModelAttribute @Valid OperacaoAcessoRequest request)
               throws JsonParseException, JsonMappingException, IOException {

          request.setPage(page);
          request.setSize(size);
          PageResponse<OperacaoAcessoResponse> response = operacaoAcessoService.listar(request);

          return new ResponseEntity<PageResponse<OperacaoAcessoResponse>>(response, HttpStatus.OK);
     }
}
