
package br.com.conductor.rhblueapi.service.utils;

import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.conductor.rhblueapi.domain.StatusDescricao;

public final class StatusUtils {

     public static StatusDescricao mapStatusDescricao(final List<StatusDescricao> dominioStatus, final String descricao) {

          return ofNullable(dominioStatus).orElse(emptyList()).stream().filter(s -> StringUtils.equalsIgnoreCase(descricao, s.getDescricao())).findAny().orElse(null);
     }

     public static StatusDescricao mapStatusDescricaoPorStatus(final List<StatusDescricao> dominioStatus, final Integer status) {

          return ofNullable(dominioStatus).orElse(emptyList()).stream().filter(s -> s.getStatus().equals(status)).findAny().orElse(null);
     }
}
