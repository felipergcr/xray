package br.com.conductor.rhblueapi.service;

import br.com.conductor.rhblueapi.domain.LogEventos;
import br.com.conductor.rhblueapi.repository.LogEventosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogEventosService {

    @Autowired
    private LogEventosRepository logEventosRepository;


    public LogEventos criarLogEventos(LogEventos logEventos) {
        return logEventosRepository.save(logEventos);
    }
}
