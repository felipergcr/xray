package br.com.conductor.rhblueapi.service;

import br.com.conductor.rhblueapi.domain.StatusCartoes;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.repository.StatusCartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.CARTAO_STATUS_CANCELAMENTO_INVALIDO;

@Service
public class StatusCartaoService {

     @Autowired
     private StatusCartaoRepository statusCartaoRepository;

     public StatusCartoes buscarStatusCartaoPorId (final Long id) {
         return statusCartaoRepository.findById(id).orElseThrow(() -> new BadRequestCdt(CARTAO_STATUS_CANCELAMENTO_INVALIDO.getMessage()));
     }

     public List<StatusCartoes> buscarTodos () {
         return statusCartaoRepository.findAll();
     }

}
