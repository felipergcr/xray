
package br.com.conductor.rhblueapi.service.funcionario;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.DADOS_ENDERECO_OBRIGATORIOS;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.FUNCIONARIO_NAO_ENCONTRADO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoEntregaCartao.PORTA_A_PORTA;
import static br.com.twsoftware.alfred.object.Objeto.isBlank;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import br.com.conductor.rhblueapi.domain.Endereco;
import br.com.conductor.rhblueapi.domain.Funcionario;
import br.com.conductor.rhblueapi.domain.PessoaEmpresa;
import br.com.conductor.rhblueapi.domain.empresa.EmpresaMinimo;
import br.com.conductor.rhblueapi.domain.funcionario.FuncionarioMinimoResponse;
import br.com.conductor.rhblueapi.domain.funcionario.FuncionarioProcessamentoStep;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.request.funcionario.EnderecoFuncionarioRequest;
import br.com.conductor.rhblueapi.domain.request.funcionario.FuncionarioAtualizacaoRequest;
import br.com.conductor.rhblueapi.domain.request.pessoafisica.EnderecoRequest;
import br.com.conductor.rhblueapi.domain.request.pessoafisica.PessoaFisicaRequest;
import br.com.conductor.rhblueapi.domain.response.funcionario.EnderecoFuncionarioResponse;
import br.com.conductor.rhblueapi.domain.response.funcionario.FuncionarioAtualizacaoResponse;
import br.com.conductor.rhblueapi.enums.Estados;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoEntregaCartao;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.FuncionarioRepository;
import br.com.conductor.rhblueapi.service.EnderecoService;
import br.com.conductor.rhblueapi.service.GrupoEmpresaService;
import br.com.conductor.rhblueapi.service.PessoaFisicaService;
import br.com.conductor.rhblueapi.util.Transacional;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class FuncionarioService {

     @Autowired
     private FuncionarioRepository funcionarioRepository;

     @Autowired
     private PessoaFisicaService pessoaFisicaService;

     @Autowired
     private EnderecoService enderecoService;
     
     @Autowired
     private GrupoEmpresaService grupoEmpresaService;
     
     @Autowired
     private FuncionarioService funcionarioServiceAop;

     @Transactional
     public FuncionarioMinimoResponse processoGerarPessoaFisica(FuncionarioProcessamentoStep funcionarioProcessamentoStep, ArquivosCargasDetalhes detalhe, EmpresaMinimo empresaMinimoResponse) {
          
          PessoaFisicaRequest pessoaFisicaRequest = pessoaFisicaService.converte(detalhe);
          
          PessoaEmpresa pessoaEmpresa = pessoaFisicaService.persisteEObtemPessoaFisica(funcionarioProcessamentoStep.getOptPessoaFisica(), pessoaFisicaRequest);
          
          EnderecoRequest enderecoRequest = enderecoService.converte(detalhe);
          
          Long idEndereco = enderecoService.persisteEobtemIdEndereco(funcionarioProcessamentoStep.getOptEndereco(), enderecoRequest, pessoaEmpresa.getIdPessoa());
          
          Long idFuncionario = this.persisteEobtemIdFuncionario(funcionarioProcessamentoStep.getOptFuncionario(), empresaMinimoResponse, pessoaEmpresa, idEndereco, detalhe.getMatricula(), detalhe.getCodUnidade());
          
          return FuncionarioMinimoResponse.builder()
                    .idPessoa(pessoaEmpresa.getIdPessoa())
                    .idFuncionario(idFuncionario)
                    .idEmpresa(empresaMinimoResponse.getIdEmpresa())
                    .build();
     }
     
     public FuncionarioProcessamentoStep popularSteps(ArquivosCargasDetalhes detalhe, EmpresaMinimo empresaMinimoResponse) {
          
          Optional<PessoaEmpresa> optPessoaEmpresa = pessoaFisicaService.buscaPessoaPorDocumento(detalhe.getCpf());
          
          Optional<Funcionario> optFuncionario = Optional.empty();
          
          Optional<Endereco> optEndereco = Optional.empty();
          
          if(optPessoaEmpresa.isPresent())
               optFuncionario = funcionarioRepository.findByIdPessoaFisicaAndIdEmpresa(optPessoaEmpresa.get().getIdPessoa(), empresaMinimoResponse.getIdEmpresa());

          if(optFuncionario.isPresent())
               optEndereco = enderecoService.buscaEnderecoPorId(optFuncionario.get().getIdEnderecoEntrega());
          
          return FuncionarioProcessamentoStep.builder()
                    .optPessoaFisica(optPessoaEmpresa)
                    .optFuncionario(optFuncionario)
                    .optEndereco(optEndereco)
                    .build();
     }

     private Long persisteEobtemIdFuncionario(Optional<Funcionario> optFuncionario, EmpresaMinimo empresaMinimoResponse, PessoaEmpresa pessoaEmpresa, Long idEndereco, String matricula, String codUnidadeEntrega) {

          return optFuncionario.isPresent() ? this.atualizarAtributos(optFuncionario.get(), matricula, codUnidadeEntrega).getId() : this.cadastrar(empresaMinimoResponse.getIdEmpresa(), empresaMinimoResponse.getIdEmpresaSetor(), pessoaEmpresa.getIdPessoa(), idEndereco, matricula, codUnidadeEntrega).getId();
     }

     private Funcionario cadastrar(Long idEmpresa, Long idEmpresaSetor, Long idPessoaFisica, Long idEndereco, String matricula, String codUnidadeEntrega) {

          Funcionario funcionario = Funcionario.builder().idEmpresa(idEmpresa).idEmpresaSetor(idEmpresaSetor).idPessoaFisica(idPessoaFisica).idEnderecoEntrega(idEndereco).dataCadastro(LocalDateTime.now()).dataStatus(LocalDateTime.now()).matricula(matricula).codigoUnidadeEntrega(codUnidadeEntrega).build();

          return funcionarioRepository.save(funcionario);
     }
     
     private Funcionario atualizarAtributos(Funcionario funcionario, String matricula, String codUnidadeEntrega) {
          
          Funcionario funcionarioAtualizado = Funcionario.builder().build();
          
          BeanUtils.copyProperties(funcionario, funcionarioAtualizado);
          
          funcionarioAtualizado = Objeto.notBlank(codUnidadeEntrega) ? this.editarAtributoCodigoUnidadeEntrega(funcionarioAtualizado, codUnidadeEntrega) : funcionarioAtualizado;
          
          funcionarioAtualizado = this.editarAtributoMatricula(funcionarioAtualizado, matricula);
          
          return funcionarioAtualizado.equals(funcionario) ? funcionario : funcionarioRepository.save(funcionarioAtualizado);
     }
     
     
     private Funcionario editarAtributoMatricula(Funcionario funcionario, String matricula) {
          
          if(StringUtils.equals(funcionario.getMatricula(), matricula))
               return funcionario;
          
          funcionario.setMatricula(matricula);
          
          return funcionario;
     }

     
     private Funcionario editarAtributoCodigoUnidadeEntrega(Funcionario funcionario, String codUnidadeEntrega) {
          
          if(StringUtils.equals(funcionario.getCodigoUnidadeEntrega(), codUnidadeEntrega))
               return funcionario;
          
          funcionario.setCodigoUnidadeEntrega(codUnidadeEntrega);
          
          return funcionario;
     }
     
     private boolean isAtualizaEndereco(final IdentificacaoRequest identificacao) {
          
          TipoEntregaCartao tipoEntrega = grupoEmpresaService.tipoEntregaCartao(identificacao.getIdGrupoEmpresa());
          
          return tipoEntrega.equals(PORTA_A_PORTA);
          
     }
     
     public FuncionarioAtualizacaoResponse atualizaDadosFuncionario(Funcionario funcionario, final FuncionarioAtualizacaoRequest funcionarioAtualizacaoRequest) {
          
          boolean isAtualizaEndereco = this.isAtualizaEndereco(funcionarioAtualizacaoRequest.getIdentificacao());
          
          BadRequestCdt.checkThrow(isAtualizaEndereco && isBlank(funcionarioAtualizacaoRequest.getEndereco()), DADOS_ENDERECO_OBRIGATORIOS);
          
          return funcionarioServiceAop.atualizaDadosFuncionario(funcionario, funcionarioAtualizacaoRequest, isAtualizaEndereco);
     }

     @Transacional
     public FuncionarioAtualizacaoResponse atualizaDadosFuncionario(Funcionario funcionario, final FuncionarioAtualizacaoRequest funcionarioAtualizacaoRequest, boolean atualizaEndereco) {

          funcionario = this.atualizarAtributos(funcionario, funcionarioAtualizacaoRequest.getMatricula(), null);
          
          pessoaFisicaService.atualizar(funcionario.getIdPessoaFisica(), funcionarioAtualizacaoRequest);
          
          if(atualizaEndereco) {
               
               EnderecoRequest enderecoRequest = enderecoService.converte(funcionarioAtualizacaoRequest.getEndereco());
               
               enderecoRequest = enderecoService.atualizar(funcionario.getIdEnderecoEntrega(), enderecoRequest);
               
               funcionarioAtualizacaoRequest.setEndereco(this.converte(enderecoRequest));
          }
          
          return this.converte(funcionarioAtualizacaoRequest, atualizaEndereco);
     }
     
     private FuncionarioAtualizacaoResponse converte(FuncionarioAtualizacaoRequest funcionarioAtualizacaoRequest, boolean atualizaEndereco) {
          
          if(!atualizaEndereco)
               funcionarioAtualizacaoRequest.setEndereco(null);

          return this.converte(funcionarioAtualizacaoRequest);
     }

     private FuncionarioAtualizacaoResponse converte(FuncionarioAtualizacaoRequest funcionarioAtualizacaoRequest) {
          
          FuncionarioAtualizacaoResponse funcionarioAtualizacaoResponse = FuncionarioAtualizacaoResponse.builder().build();
          
          BeanUtils.copyProperties(funcionarioAtualizacaoRequest, funcionarioAtualizacaoResponse);
          
          if(Objects.nonNull(funcionarioAtualizacaoRequest.getEndereco())) {
               EnderecoFuncionarioResponse enderecoFuncionarioResponse = EnderecoFuncionarioResponse.builder().build();
               
               BeanUtils.copyProperties(funcionarioAtualizacaoRequest.getEndereco(), enderecoFuncionarioResponse);
               
               funcionarioAtualizacaoResponse.setEndereco(enderecoFuncionarioResponse);
          }
          
          return funcionarioAtualizacaoResponse;
     }
     
     private EnderecoFuncionarioRequest converte(EnderecoRequest enderecoRequest) {
          
          EnderecoFuncionarioRequest enderecoFuncionarioRequest = EnderecoFuncionarioRequest.builder().build();
          
          BeanUtils.copyProperties(enderecoRequest, enderecoFuncionarioRequest);
          
          enderecoFuncionarioRequest.setUf(Estados.findByName(enderecoRequest.getUf()));
          
          return enderecoFuncionarioRequest;
     }
     
     public Funcionario buscaFuncionarioPorId(Long idFuncionario) {
          
          Optional<Funcionario> optFuncionario = funcionarioRepository.findById(idFuncionario);
          
          NotFoundCdt.checkThrow(!optFuncionario.isPresent(), FUNCIONARIO_NAO_ENCONTRADO);
          
          return optFuncionario.get();
     }
     
}
