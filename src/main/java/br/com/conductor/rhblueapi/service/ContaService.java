package br.com.conductor.rhblueapi.service;

import br.com.conductor.rhblueapi.domain.EmpresaConta;
import br.com.conductor.rhblueapi.domain.Parametro;
import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.ContaPersist;
import br.com.conductor.rhblueapi.domain.pier.ContaRequestPier;
import br.com.conductor.rhblueapi.domain.pier.ContaResponsePier;
import br.com.conductor.rhblueapi.domain.pier.ProdutoResponsePier;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.repository.EmpresaContaRepository;
import br.com.conductor.rhblueapi.repository.ParametroGrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.ParametroRepository;
import br.com.conductor.rhblueapi.rest.ContaPierRest;
import br.com.conductor.rhblueapi.rest.OrigensComercialRest;
import br.com.conductor.rhblueapi.util.ConstantesParametros;
import br.com.twsoftware.alfred.object.Objeto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ContaService {

     @Autowired
     private ContaPierRest contaPierRest;

     @Autowired
     private OrigensComercialRest origensComercialRest;

     @Autowired
     private EmpresaContaRepository empresaContaRepository;

     @Autowired
     private ParametroRepository parametrosRepository;

     @Autowired
     private ParametroGrupoEmpresaRepository parametrosGrupoEmpresaRepository;

     private static final String CANAL_ENTRADA = "BLUE-API";

     private static final Integer VALOR_PONTUACAO_DEFAULT = 0;

     public void criarContasEmpresa(final ContaPersist contaPersist) {

          BadRequestCdt.checkThrow(Objeto.isBlank(contaPersist), ExceptionsMessagesCdtEnum.PARAMETROS_CONTA_NAO_INFORMADOS);
          BadRequestCdt.checkThrow(Objeto.isBlank(contaPersist.getIdPessoa()), ExceptionsMessagesCdtEnum.ID_PESSOA_NAO_INFORMADO);
          BadRequestCdt.checkThrow(Objeto.isBlank(contaPersist.getIdEmpresa()), ExceptionsMessagesCdtEnum.ID_EMPRESA_NAO_INFORMADO);
          BadRequestCdt.checkThrow(Objeto.isBlank(contaPersist.getIdProduto()), ExceptionsMessagesCdtEnum.ID_PRODUTO_NAO_INFORMADO);
          BadRequestCdt.checkThrow(!origensComercialRest.hasOrigemComercial(contaPersist.getIdOrigemComercial()), ExceptionsMessagesCdtEnum.ORIGEM_COMERCIAL_NAO_ENCONTRADA);

          Long idContaPrincipal = criarConta(contaPersist.getIdPessoa(), contaPersist.getIdProduto(), contaPersist);

          salvarVinculoEmpresaConta(contaPersist.getIdEmpresa(), idContaPrincipal);

          final List<Long> tipoProduto = consultaTipoProduto(contaPersist.getIdGrupoEmpresaPai(), contaPersist.getProdutosResponse());

          for (Long idProduto : tipoProduto) {

               Long idConta = criarConta(contaPersist.getIdPessoa(), idProduto, contaPersist);

               salvarVinculoEmpresaConta(contaPersist.getIdEmpresa(), idConta);
          }
     }

     private EmpresaConta salvarVinculoEmpresaConta(Long idEmpresa, Long idConta) {

          EmpresaConta empresaConta = new EmpresaConta();
          empresaConta.setIdEmpresa(idEmpresa);
          empresaConta.setIdConta(idConta);
          return empresaContaRepository.save(empresaConta);
     }

     private List<Long> consultaTipoProduto(Long idGrupoEmpresa, List<ProdutoResponsePier> produtosResponse) {

          List<Long> listaIdProdutos = new ArrayList<>();

          Optional<Parametro> paramProdutoVA = parametrosRepository.findByCodigo(ConstantesParametros.RH_USA_PRODUTO_VA);

          Optional<Parametro> paramProdutoVR = parametrosRepository.findByCodigo(ConstantesParametros.RH_USA_PRODUTO_VR);

          BadRequestCdt.checkThrow(!paramProdutoVA.isPresent(), ExceptionsMessagesCdtEnum.PARAMETROS_GRUPO_EMPRESA_NAO_CADASTRADO);

          BadRequestCdt.checkThrow(!paramProdutoVR.isPresent(), ExceptionsMessagesCdtEnum.PARAMETROS_GRUPO_EMPRESA_NAO_CADASTRADO);

          Long idParametroProdutoVA = paramProdutoVA.get().getIdParametro();

          Long idParametroProdutoVR = paramProdutoVR.get().getIdParametro();

          final String parametroVA = "Produto PAT - Alimentação";
          final String parametroVR = "Produto PAT - Refeição";

          Optional<ParametroGrupoEmpresa> parametrosGrupoEmpresaTipoProdutoVA = parametrosGrupoEmpresaRepository.findByIdGrupoEmpresaAndParametro_IdParametro(idGrupoEmpresa, idParametroProdutoVA);
          if (parametrosGrupoEmpresaTipoProdutoVA.isPresent() && Objects.equals(parametrosGrupoEmpresaTipoProdutoVA.get().getValor(), String.valueOf(1))) {
               produtosResponse.forEach(produto -> {
                    if (Objects.equals(parametroVA, produto.getNome())) {
                         listaIdProdutos.add(produto.getId());
                    }
               });
          }

          Optional<ParametroGrupoEmpresa> parametrosGrupoEmpresaTipoProdutoVR = parametrosGrupoEmpresaRepository.findByIdGrupoEmpresaAndParametro_IdParametro(idGrupoEmpresa, idParametroProdutoVR);
          if (parametrosGrupoEmpresaTipoProdutoVR.isPresent() && Objects.equals(parametrosGrupoEmpresaTipoProdutoVR.get().getValor(), String.valueOf(1))) {
               produtosResponse.forEach(produto -> {
                    if (Objects.equals(parametroVR, produto.getNome())) {
                         listaIdProdutos.add(produto.getId());
                    }
               });
          }

          return listaIdProdutos;
     }

     private Long criarConta(Long idPessoa, Long idProduto, ContaPersist contaPersist) {

          ContaRequestPier contaRequest = ContaRequestPier.builder()
                    .canalEntrada(CANAL_ENTRADA)
                    .diaVencimento(1)
                    .flagFaturaPorEmail(0)
                    .idOrigemComercial(contaPersist.getIdOrigemComercial())
                    .idPessoa(idPessoa)
                    .idProduto(idProduto)
                    .limiteConsignado(BigDecimal.valueOf(0.00))
                    .limiteGlobal(BigDecimal.valueOf(0.00))
                    .limiteMaximo(BigDecimal.valueOf(0.00))
                    .limiteParcelas(BigDecimal.valueOf(0.00))
                    .valorRenda(BigDecimal.valueOf(0.00))
                    .valorPontuacao(VALOR_PONTUACAO_DEFAULT)
                    .idEnderecoCorrespondencia(contaPersist.getIdEnderecoCorrespondencia())
                    .build();

          ResponseEntity<ContaResponsePier> contaResponse = contaPierRest.criaConta(contaRequest);

          return contaResponse.getBody().getId();
     }

     public void deleteContaEmpresa(Long idEmpresa) {
          empresaContaRepository.deleteByIdEmpresa(idEmpresa);
     }
}
