
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.ARQUIVO_UNIDADE_ENTREGA_STATUS_DIFERENTE_INVALIDADO_PROCESSAMENTO_CONCLUIDO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaEnum.IMPORTADO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaEnum.INVALIDADO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaEnum.PROCESSAMENTO_CONCLUIDO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaEnum.RECEBIDO;
import static br.com.conductor.rhblueapi.enums.TemplatesExcel.ARQUIVO_UNIDADES_ENTREGA;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_UNIDADE_ENTREGA;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.ARQUIVO_UNIDADE_ENTREGA_QUANTIDADE_ABAS;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntrega;
import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaBinario;
import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.UnidadeEntregaResponse;
import br.com.conductor.rhblueapi.enums.HeaderUnidadeEntregaEnum;
import br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaEnum;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.BusinessException;
import br.com.conductor.rhblueapi.exception.ConflictCdt;
import br.com.conductor.rhblueapi.exception.ConversaoObjetoException;
import br.com.conductor.rhblueapi.exception.ObjetoVazioException;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.repository.ArquivoUnidadeEntregaBinarioRepository;
import br.com.conductor.rhblueapi.repository.ArquivoUnidadeEntregaRepository;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.UnidadesEntregaPublish;
import br.com.conductor.rhblueapi.service.email.EmailFinalizaUnidadeEntregaService;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.AppConstantes;
import br.com.conductor.rhblueapi.util.ArquivoUtils;
import br.com.conductor.rhblueapi.util.EntityGenericUtil;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class ArquivoUnidadeEntregaService {

     @Autowired
     private ArquivoUnidadeEntregaBinarioRepository arquivoUnidadeEntregaBinarioRepository;

     @Autowired
     private ArquivoUnidadeEntregaRepository arquivoUnidadeEntregaRepository;

     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;

     @Autowired
     private UnidadeEntregaDetalheService unidadeEntregaDetalheService;

     @Autowired
     private UploadArquivoService uploadArquivoService;

     @Autowired
     private UnidadesEntregaPublish unidadesEntregaPublish;

     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;

     @Autowired
     private StatusCached statusCached;
     
     @Autowired
     private EmailFinalizaUnidadeEntregaService emailFinalizaUnidadeEntregaService;

     private static final int LINHA_CABECALHO = 5;

     public UnidadeEntregaResponse uploadFile(IdentificacaoRequest request, MultipartFile file) {

          BadRequestCdt.checkThrow(!file.getOriginalFilename().endsWith(AppConstantes.FileExtension.XLSX) && !file.getOriginalFilename().endsWith(AppConstantes.FileExtension.XLS), ExceptionsMessagesCdtEnum.PLANILHA_EXTENSAO);

          usuarioNivelPermissaoRhService.autenticarNivelAcessoRhGrupo(request.getIdUsuario(), request.getIdGrupoEmpresa());

          ConflictCdt.checkThrow(isVerificarArquivoUnidadeEntregaEmProcessamento(request.getIdGrupoEmpresa()), ExceptionsMessagesCdtEnum.UPLOAD_ARQUIVO_UNIDADE_ENTREGA_PROCESSANDO);

          ArquivoUnidadeEntregaBinario arquivoUnidadeEntregaBinario;

          try {

               Workbook workbook = uploadArquivoService.lerPlanilha(file);

               validarCabecalho(workbook);

               validaPrimeiraLinhaConteudo(workbook);

               arquivoUnidadeEntregaBinario = salvarPlanilha(request, file);

               workbook.close();

          } catch (Exception e) {
               throw new BadRequestCdt(e.getMessage());
          }

          Long idArquivoUnidadeEntrega = arquivoUnidadeEntregaBinario.getIdArquivoUnidadeEntrega();

          unidadesEntregaPublish.enviarArquivoUnidadeEntregaImportacao(idArquivoUnidadeEntrega);

          return UnidadeEntregaResponse.builder().idArquivoUnidadeEntrega(idArquivoUnidadeEntrega).build();
     }

     private void validarCabecalho(Workbook workbook) {

          Sheet sheet = workbook.getSheetAt(0);

          validaQtdAbas(workbook.getNumberOfSheets());
          
          BadRequestCdt.checkThrow(Objects.isNull(sheet.getRow(LINHA_CABECALHO)), ExceptionsMessagesCdtEnum.PLANILHA_INVALIDA);

          Iterator<Cell> cellIterator = sheet.getRow(LINHA_CABECALHO).cellIterator();
          while (cellIterator.hasNext()) {
               Cell celula = cellIterator.next();
               String cabecalhoCampo = EntityGenericUtil.removeAccents(celula.getStringCellValue().trim());
               BadRequestCdt.checkThrow(!HeaderUnidadeEntregaEnum.existeHeaderUnidadeEntregaEnum(cabecalhoCampo, celula.getColumnIndex()), ExceptionsMessagesCdtEnum.PLANILHA_INVALIDA);
          }
     }

     private void validaQtdAbas(final Integer qtdAbas) {

          BadRequestCdt.checkThrow(qtdAbas != ARQUIVO_UNIDADE_ENTREGA_QUANTIDADE_ABAS, ExceptionsMessagesCdtEnum.ARQUIVO_SOMENTE_UMA_ABA);
     }

     private void validaPrimeiraLinhaConteudo(Workbook workbook) {

          Sheet sheet = workbook.getSheetAt(0);

          Row linha = sheet.getRow(ARQUIVO_UNIDADES_ENTREGA.getLinhaInicioConteudo());

          List<String> listaErros = new ArrayList<>();

          Arrays.asList(HeaderUnidadeEntregaEnum.values()).stream().forEach(item -> {
               if (item.isObrigatorio() && (Objects.isNull(linha.getCell(item.getPosicaoCelula())) || Objeto.isBlank(linha.getCell(item.getPosicaoCelula()).toString()))) {
                    listaErros.add(String.format("%s é obrigatório/a", item.getHeader()));
               }
          });

          if (CollectionUtils.isNotEmpty(listaErros)) {
               throw new BadRequestCdt(listaErros.toString());
          }
     }

     private ArquivoUnidadeEntregaBinario salvarPlanilha(IdentificacaoRequest request, MultipartFile file) throws IOException {

          List<StatusDescricao> dominioStatusUnidadeEntrega = this.statusCached.busca(TipoStatus.ARQUIVO_UNIDADE_ENTREGA.getNome());
          final StatusDescricao statusRecebido = mapStatusDescricao(dominioStatusUnidadeEntrega, RECEBIDO.getStatus());

          ArquivoUnidadeEntrega arquivoUnidadeEntrega = new ArquivoUnidadeEntrega();
          arquivoUnidadeEntrega.setIdGrupoEmpresa(request.getIdGrupoEmpresa());
          arquivoUnidadeEntrega.setIdUsuarioRegistro(request.getIdUsuario());
          arquivoUnidadeEntrega.setStatus(statusRecebido.getStatus());
          arquivoUnidadeEntrega.setDataUpload(LocalDateTime.now());
          arquivoUnidadeEntrega.setDataAtualizacao(LocalDateTime.now());
          arquivoUnidadeEntrega.setDataProcessamento(LocalDateTime.now());
          arquivoUnidadeEntrega.setDataStatus(LocalDateTime.now());
          arquivoUnidadeEntrega.setNomeArquivo(file.getOriginalFilename());

          arquivoUnidadeEntregaRepository.save(arquivoUnidadeEntrega);

          ArquivoUnidadeEntregaBinario arquivoUnidadeEntregaBinario = new ArquivoUnidadeEntregaBinario();
          arquivoUnidadeEntregaBinario.setIdArquivoUnidadeEntrega(arquivoUnidadeEntrega.getId());
          arquivoUnidadeEntregaBinario.setArquivo(file.getBytes());

          return arquivoUnidadeEntregaBinarioRepository.save(arquivoUnidadeEntregaBinario);
     }

     public Optional<ArquivoUnidadeEntrega> buscaPorId(Long idArquivoUnidadeEntrega) {

          return arquivoUnidadeEntregaRepository.findById(idArquivoUnidadeEntrega);
     }

     public ArquivoUnidadeEntrega salvar(ArquivoUnidadeEntrega arquivoUnidadeEntrega) {

          arquivoUnidadeEntrega.setDataStatus(LocalDateTime.now());

          return arquivoUnidadeEntregaRepository.save(arquivoUnidadeEntrega);
     }

     public boolean inicializaImportacao(final ArquivoUnidadeEntrega arquivoUnidadeEntrega) throws ObjetoVazioException, BusinessException, ConversaoObjetoException {

          this.buscarNivelPermissaoPor(arquivoUnidadeEntrega);

          final byte[] blob = this.buscaBlobPor(arquivoUnidadeEntrega);
          final InputStream blobInputStream = ArquivoUtils.converteBlobParaInputStream(blob);
          final Workbook workbook = ArquivoUtils.criaWorkbookDe(blobInputStream);

          return this.unidadeEntregaDetalheService.isConteudoValido(workbook, ARQUIVO_UNIDADES_ENTREGA, arquivoUnidadeEntrega);
     }

     private byte[] buscaBlobPor(ArquivoUnidadeEntrega arquivoUnidadeEntrega) throws ObjetoVazioException {

          return this.buscaArquivoUnidadeEntregaBinarioPor(arquivoUnidadeEntrega).getArquivo();
     }

     private ArquivoUnidadeEntregaBinario buscaArquivoUnidadeEntregaBinarioPor(ArquivoUnidadeEntrega arquivoUnidadeEntrega) throws ObjetoVazioException {

          Optional<ArquivoUnidadeEntregaBinario> opt = arquivoUnidadeEntregaBinarioRepository.findByIdArquivoUnidadeEntrega(arquivoUnidadeEntrega.getId());
          if (!opt.isPresent()) {
               throw new ObjetoVazioException("Não foi localizado registro blob para este Arquivo de unidade entrega.");
          }
          return opt.get();
     }

     private PermissoesUsuariosRh buscarNivelPermissaoPor(ArquivoUnidadeEntrega arquivoUnidadeEntrega) throws ObjetoVazioException, BusinessException {

          Optional<PermissoesUsuariosRh> optPermissao = permissoesUsuariosRhService.obterPermissaoAtivaPor(arquivoUnidadeEntrega.getIdUsuarioRegistro(), arquivoUnidadeEntrega.getIdGrupoEmpresa());

          if (!optPermissao.isPresent())
               throw new ObjetoVazioException("Usuário sem permissões para cadastrar unidades de entrega.");
          return optPermissao.get();
     }

     @Async
     public void atualizaDadosPosProcessamento(ArquivoUnidadeEntrega arquivoUnidadeEntrega, boolean isValido, String message) {

          List<StatusDescricao> dominioStatusUnidadeEntrega = this.statusCached.busca(TipoStatus.ARQUIVO_UNIDADE_ENTREGA.getNome());
          final StatusDescricao statusImportado = mapStatusDescricao(dominioStatusUnidadeEntrega, IMPORTADO.getStatus());
          final StatusDescricao statusInvalidado = mapStatusDescricao(dominioStatusUnidadeEntrega, INVALIDADO.getStatus());

          if (isValido) {
               arquivoUnidadeEntrega.setStatus(statusImportado.getStatus());
          } else {

               emailFinalizaUnidadeEntregaService.enviar(arquivoUnidadeEntrega, INVALIDADO);
               
               arquivoUnidadeEntrega.setMsgErro(message);
               arquivoUnidadeEntrega.setStatus(statusInvalidado.getStatus());
          }
          this.salvar(arquivoUnidadeEntrega);

     }

     public boolean isStatusPedidoDiferenteDe(Long idUnidadeEntrega, StatusArquivoUnidadeEntregaEnum statusUnidadeEntrega) {

          final StatusDescricao status = mapStatusDescricao(this.statusCached.busca(ARQUIVO_UNIDADE_ENTREGA.getNome()), statusUnidadeEntrega.getStatus());

          Optional<ArquivoUnidadeEntrega> arquivoUnidadeEntrega = this.buscaPorId(idUnidadeEntrega);
          PreconditionCustom.checkThrow(!arquivoUnidadeEntrega.isPresent(), ExceptionsMessagesCdtEnum.ARQUIVO_UNIDADE_ENTREGA_NAO_ENCONTRADO);

          return arquivoUnidadeEntrega.get().getStatus() != status.getStatus();
     }

     private void alterarStatus(ArquivoUnidadeEntrega arquivoUnidadeEntrega, StatusArquivoUnidadeEntregaEnum statusUnidadeEntrega) {

          List<StatusDescricao> dominioStatusUnidadeEntrega = this.statusCached.busca(TipoStatus.ARQUIVO_UNIDADE_ENTREGA.getNome());
          final StatusDescricao statusDescricao = mapStatusDescricao(dominioStatusUnidadeEntrega, statusUnidadeEntrega.getStatus());

          arquivoUnidadeEntrega.setDataStatus(LocalDateTime.now());
          arquivoUnidadeEntrega.setStatus(statusDescricao.getStatus());
          
          arquivoUnidadeEntregaRepository.save(arquivoUnidadeEntrega);
     }
     
     public void alterarStatusProcessamentoConcluido(ArquivoUnidadeEntrega arquivoUnidadeEntrega) {

          this.alterarStatus(arquivoUnidadeEntrega, PROCESSAMENTO_CONCLUIDO);
          emailFinalizaUnidadeEntregaService.enviar(arquivoUnidadeEntrega, PROCESSAMENTO_CONCLUIDO);
     }

     public Optional<List<ArquivoUnidadeEntrega>> buscarPorGrupoEmpresaEStatus(Long idGrupoEmpresa, List<Integer> status) {

          return arquivoUnidadeEntregaRepository.findByIdGrupoEmpresaAndStatusIn(idGrupoEmpresa, status);
     }

     public boolean isVerificarArquivoUnidadeEntregaEmProcessamento(Long idGrupoEmpresa) {
          
          List<Integer> listaDeStatus = new ArrayList<>();
          final StatusDescricao statusRecebido = mapStatusDescricao(statusCached.busca(TipoStatus.ARQUIVO_UNIDADE_ENTREGA.getNome()), StatusArquivoUnidadeEntregaEnum.RECEBIDO.getStatus());
          final StatusDescricao statusImportado = mapStatusDescricao(statusCached.busca(TipoStatus.ARQUIVO_UNIDADE_ENTREGA.getNome()), StatusArquivoUnidadeEntregaEnum.IMPORTADO.getStatus());
          
          listaDeStatus.add(statusRecebido.getStatus());
          listaDeStatus.add(statusImportado.getStatus());

          return buscarPorGrupoEmpresaEStatus(idGrupoEmpresa, listaDeStatus).isPresent();

     }

     private boolean isStatusArquivoIgualA(final Optional<ArquivoUnidadeEntrega> arquivoUnidadeEntrega, StatusArquivoUnidadeEntregaEnum statusArquivoUnidadeEntrega) {

          final StatusDescricao status = mapStatusDescricao(this.statusCached.busca(ARQUIVO_UNIDADE_ENTREGA.getNome()), statusArquivoUnidadeEntrega.getStatus());

          return arquivoUnidadeEntrega.get().getStatus() == status.getStatus();
     }

     private boolean isStatusProcessamentoConcluido(final Optional<ArquivoUnidadeEntrega> arquivoUnidadeEntrega) {

          return isStatusArquivoIgualA(arquivoUnidadeEntrega, PROCESSAMENTO_CONCLUIDO);
     }

     private boolean isStatusInvalidado(final Optional<ArquivoUnidadeEntrega> arquivoUnidadeEntrega) {

          return isStatusArquivoIgualA(arquivoUnidadeEntrega, INVALIDADO);
     }

     public void validarStatusIgualAConcluidoOuInvalidado(final Long idArquivoUnidadeEntrega) {

          Optional<ArquivoUnidadeEntrega> arquivoUnidadeEntrega = this.buscaPorId(idArquivoUnidadeEntrega);
          PreconditionCustom.checkThrow(!arquivoUnidadeEntrega.isPresent(), ExceptionsMessagesCdtEnum.ARQUIVO_UNIDADE_ENTREGA_NAO_ENCONTRADO);

          boolean isArquivoProcessamentoConcluido = this.isStatusProcessamentoConcluido(arquivoUnidadeEntrega);
          boolean isArquivoInvalidado = this.isStatusInvalidado(arquivoUnidadeEntrega);
          PreconditionCustom.checkThrow((!isArquivoProcessamentoConcluido && !isArquivoInvalidado), ARQUIVO_UNIDADE_ENTREGA_STATUS_DIFERENTE_INVALIDADO_PROCESSAMENTO_CONCLUIDO);
     }
}
