
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.DATA_NASCIMENTO_MAIOR_QUE_ATUAL;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.repository.UsuarioRepository;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.update.UsuarioResumidoUpdate;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.util.DataUtils;

@Service
public class UsuarioService {

     @Autowired
     private UsuarioRepository usuarioRepository;

     @Value("${app.plataforma.rh.cod}")
     private Integer idPlataformaRh;

     public Usuario obterUsuarioPorCpf(final String cpf) {

          return usuarioRepository.findByCpfAndIdPlataforma(cpf, idPlataformaRh).orElse(null);
     }

     public Usuario autenticarUsuarioPorCpf(final String cpf) {

          Usuario usuario = obterUsuarioPorCpf(cpf);
          NotFoundCdt.checkThrow(Objects.isNull(usuario), ExceptionsMessagesCdtEnum.USUARIO_NAO_LOCALIZADO);

          return usuario;
     }

     public Usuario autenticarUsuarioPorId(final Long idUsuario) {

          Optional<Usuario> usuario = usuarioRepository.findByIdAndIdPlataforma(idUsuario, idPlataformaRh);
          NotFoundCdt.checkThrow(Boolean.FALSE.equals(usuario.isPresent()), ExceptionsMessagesCdtEnum.USUARIO_NAO_LOCALIZADO);

          return usuario.get();
     }

     public Usuario atualizarUsuarioResumido(final Usuario usuario, final UsuarioResumidoUpdate usuarioResumidoUpdate) {

          BadRequestCdt.checkThrow(DataUtils.isDataMaiorQueDataCorrente(usuarioResumidoUpdate.getDataNascimento()), DATA_NASCIMENTO_MAIOR_QUE_ATUAL);

          usuario.setEmail(usuarioResumidoUpdate.getEmail());
          usuario.setDataNascimento(usuarioResumidoUpdate.getDataNascimento());
          usuario.setDataUltimaAtualizacao(LocalDateTime.now());

          return usuarioRepository.save(usuario);
     }
}
