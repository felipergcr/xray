
package br.com.conductor.rhblueapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhesErros;
import br.com.conductor.rhblueapi.domain.LinhaValidadaExcel;
import br.com.conductor.rhblueapi.repository.ArquivosCargasDetalhesErrosRepository;

@Service
public class PedidoDetalhadoErrosService {

     @Autowired
     private ArquivosCargasDetalhesErrosRepository arquivosCargasDetalhesErrosRepository;

     @Async
     public void salvarErrosLinhaDetalhada(Long idArquivosDetalhePedidos, LinhaValidadaExcel linhaValidada) {

          List<ArquivosCargasDetalhesErros> loteErros = new ArrayList<>();
          linhaValidada.getErros().stream().forEach(erro -> {
               ArquivosCargasDetalhesErros detalheErro = new ArquivosCargasDetalhesErros();
               detalheErro.setIdDetalheArquivo(idArquivosDetalhePedidos);
               detalheErro.setMotivo(erro);
               loteErros.add(detalheErro);
          });
          arquivosCargasDetalhesErrosRepository.saveAll(loteErros);
     }

     public void salvar(ArquivosCargasDetalhesErros erro) {

          arquivosCargasDetalhesErrosRepository.save(erro);
     }

}
