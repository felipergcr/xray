
package br.com.conductor.rhblueapi.service.status;

import static br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum.getByDescricao;
import static br.com.conductor.rhblueapi.enums.TipoStatus.CARGAS_BENEFICIOS;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricaoPorStatus;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.status.DadosAtualizacaoStatusPagamento;
import br.com.conductor.rhblueapi.domain.status.StatusPrioritarioDetalhado;
import br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum;
import br.com.conductor.rhblueapi.enums.StatusPagamentoEnum;
import br.com.conductor.rhblueapi.service.LimiteCreditoService;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.factory.StatusCargaBeneficioFactory;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaBeneficioStrategy;
import br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;

@Service
public class StatusService {
     
     @Autowired
     private StatusCached statusCached;
     
     @Autowired
     private LimiteCreditoService limiteCreditoService;

     private List<StatusCargaBeneficioEnum> converteStatusEntidadeCargasBeneficioParaEnum(List<Integer> statusCargasBeneficios) {
          
          final List<StatusDescricao> dominioCargasBeneficios = this.statusCached.busca(CARGAS_BENEFICIOS.getNome());
          
          List<StatusCargaBeneficioEnum> statusEnum = new ArrayList<StatusCargaBeneficioEnum>();
          
          for(Integer status : statusCargasBeneficios) {
               StatusDescricao statusDescricao  = mapStatusDescricaoPorStatus(dominioCargasBeneficios, status);
               StatusCargaBeneficioEnum statusCargaBeneficioEnum = getByDescricao(statusDescricao.getDescricao());
               statusEnum.add(statusCargaBeneficioEnum);
          }
          
          return statusEnum;
     }
     
     private StatusPrioritarioDetalhado buscaStatusPrioritarioPersonalizado(List<Integer> statusCargasBeneficios) {
          
          List<StatusCargaBeneficioEnum> statusEnum = this.converteStatusEntidadeCargasBeneficioParaEnum(statusCargasBeneficios);
          
          StatusCargaBeneficioEnum statusPrioritario = statusEnum.stream().distinct().sorted(Comparator.comparing(StatusCargaBeneficioEnum::getPrioridade)).findFirst().get();
          
          Predicate<StatusCargaBeneficioEnum> todosStatusIguais = statusCargas -> statusCargas.name().equals(statusPrioritario.name());
          
          boolean isTodosStatusIguais = statusEnum.stream().allMatch(todosStatusIguais);
          
          return StatusPrioritarioDetalhado.builder().status(statusPrioritario).isTodosStatusIguais(isTodosStatusIguais).build();
     }
     
     private StatusPagamentoEnum retornaStatusPagamento(Long idGrupoEmpresa, StatusPagamentoBaseEnum statusPagamento) {
          
          StatusPagamentoEnum status = null;
          
          switch(statusPagamento) {
               case LIMITE_CREDITO:
                    status = limiteCreditoService.isDataVigenciaDoLimiteExpirado(idGrupoEmpresa) ? StatusPagamentoEnum.LIMITE_CREDITO_VENCIDO : StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE; 
                    break;
               case PAGO:
                    status = StatusPagamentoEnum.PAGO;
                    break;
               case PAGO_PARCIALMENTE:
                    status = StatusPagamentoEnum.PAGO_PARCIALMENTE;
                    break;
               case AGUARDANDO_PAGAMENTO: 
                    status = StatusPagamentoEnum.AGUARDANDO_PAGAMENTO;
                    break;
               case CANCELADO:
                    status = StatusPagamentoEnum.CANCELADO;
                    break;
               case CANCELADO_PARCIAL:
                    status = StatusPagamentoEnum.CANCELADO_PARCIAL;
                    break;
               case EXPIRADO:
                    status = StatusPagamentoEnum.EXPIRADO;
                    break;
          }
          
          return status;
     }
     
     public StatusPagamentoEnum gerenciadorStatusPagamento(DadosAtualizacaoStatusPagamento dadoAtualizacaoStatusPagamento) {
          
          StatusPrioritarioDetalhado statusPrioritarioDetalhado = this.buscaStatusPrioritarioPersonalizado(dadoAtualizacaoStatusPagamento.getStatusCargas());
          StatusCargaBeneficioStrategy strategy = StatusCargaBeneficioFactory.newStrategy(statusPrioritarioDetalhado.getStatus());
          
          if(Objects.nonNull(strategy)) {
               
               StatusPagamentoBaseEnum statusPagamentoBase = strategy.retornaStatusPagamento(statusPrioritarioDetalhado.isTodosStatusIguais(), dadoAtualizacaoStatusPagamento.getQtdCargasPedido(), dadoAtualizacaoStatusPagamento.getQtdCargasPedidoPagas());
               
               return this.retornaStatusPagamento(dadoAtualizacaoStatusPagamento.getIdGrupoEmpresa(), statusPagamentoBase);
          }
          
          return null;
     }
     

}
