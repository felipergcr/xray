
package br.com.conductor.rhblueapi.service.strategy.nivelpermissao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacional;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;

public abstract class GerenciarNivelPermissaoStrategy {
     
     @Autowired
     protected StatusCached statusCached;

     public abstract List<UsuarioPermissaoNivelAcessoRh> retornarNiveisAcessoInativado(HierarquiaOrganizacional hierarquiaOrganizacional, List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso);

     public abstract boolean isPossuiAcessoNivelSuperior(HierarquiaOrganizacional hierarquiaOrganizacional, List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso);
     
     public abstract boolean isPossuiNivelAcessoPretendido(Long idPretendido, List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso);
     
     public abstract UsuarioPermissaoNivelAcessoRh criarNivelAcesso(Long idPretendido, Long idPermissao, Long idUsuarioRegistro);

}
