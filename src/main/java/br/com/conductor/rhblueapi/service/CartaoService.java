package br.com.conductor.rhblueapi.service;

import br.com.conductor.rhblueapi.domain.*;
import br.com.conductor.rhblueapi.domain.blue.FuncionarioRequest;
import br.com.conductor.rhblueapi.domain.blue.FuncionarioResponse;
import br.com.conductor.rhblueapi.domain.pier.AgendamentoStatusCartao;
import br.com.conductor.rhblueapi.domain.response.CartaoResponse;
import br.com.conductor.rhblueapi.domain.response.EmpresaResponse;
import br.com.conductor.rhblueapi.enums.TabelasSistema;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.CartaoRepository;
import br.com.conductor.rhblueapi.repository.utils.Converters;
import br.com.conductor.rhblueapi.rest.CartaoPierRestRepository;
import br.com.conductor.rhblueapi.rest.EmpresaBlueRestRepository;
import br.com.conductor.rhblueapi.rest.blue.FuncionarioBlueRestRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.*;
import static br.com.conductor.rhblueapi.enums.Flag.NAO;
import static br.com.conductor.rhblueapi.enums.TipoPortador.ADICIONAL;
import static br.com.conductor.rhblueapi.enums.TipoPortador.TITULAR;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.FLAG_EMBOSSA_NOME_PORTADOR;
import static br.com.conductor.rhblueapi.exception.ExceptionCdt.checkThrow;
import static br.com.conductor.rhblueapi.repository.utils.Converters.formatarDataLogEvento;
import static br.com.conductor.rhblueapi.util.AppConstantes.PARAMETRO_RH;

/**
 * CartaoService
 */
@Service
public class CartaoService {

     private static final String PROCESSO_BLOQUEIO = "LAB:CancelamentoCartao";

     private static final String USUARIO_RESPONSAVEL = "RH-BLUE-API";

     private static final String MOTIVO_AGENDAMENTO_STATUS = "API_PORTADOR";

     private static final String CANCELAR_CARTAO_OBSERVACAO = "API_RH";

     private static final String PARAMETRO_HABILITADO = "1";

     private final List<StatusCartaoEnum> listaStatusPermiteCancelamento = Arrays.asList(StatusCartaoEnum.NORMAL,
             StatusCartaoEnum.BLOQUEADO, StatusCartaoEnum.BLOQUEADO_SENHA_INCORRETA, StatusCartaoEnum.BLOQUEADO_TEMPORARIO);

     @Autowired
     private CartaoRepository cartaoRepository;

     @Autowired
     private CartaoService cartaoServiceAOP;

     @Autowired
     private StatusCartaoService statusCartaoService;

     @Autowired
     private LogEventosService logEventosService;

     @Autowired
     private HistoricoCancelamentoCartaoService historicoCancelamentoCartaoService;

     @Autowired
     private CartaoPierRestRepository cartaoPierRestRepository;

     @Autowired
     private FuncionarioBlueRestRepository funcionarioBlueRestRepository;

     @Autowired
     private EmpresaBlueRestRepository empresaBlueRestRepository;

     @Autowired
     private ParametrosService parametrosService;

     /**
      * cancelarCartao
      *
      * @param idCartao
      * @param idStatusCartao
      * @param observacao
      * @return CartaoResponse
      */
     @Transactional
     public CartaoResponse cancelarCartao(final Long idCartao, final Long idStatusCartao, final String observacao) {

          //Verifica se cartão existe
          Cartoes cartao = cartaoRepository.findById(idCartao)
                  .orElseThrow(() -> new NotFoundCdt(CARTAO_NAO_ENCONTRADO.getMessage()));

          //Verifica se cartão já está cancelado (Diferente de status 1-Normal ou 2-Bloqueado 37-Bloqueado Prevenção 29-Bloqueado Senha Incorreta)
          checkThrow(listaStatusPermiteCancelamento
                  .stream()
                  .noneMatch(status -> status.equalsNome(statusCartaoService.buscarStatusCartaoPorId(
                          cartao.getIdStatusCartao()).getNome())), CARTAO_CANCELAMENTO_JA_REALIZADO);

          //Verifica se novo status do cartão existe no banco de dados
          StatusCartoes statusCartaoNovo = statusCartaoService.buscarStatusCartaoPorId(idStatusCartao);

          //Verifica se novo status do cartão é diferente de DESBLOQUEADO (1-Normal)
          checkThrow(StatusCartaoEnum.NORMAL.equalsNome(statusCartaoNovo.getNome()), CARTAO_STATUS_CANCELAMENTO_INVALIDO);

          //Verifica se novo status do cartão é igual ao status atual
          checkThrow(statusCartaoNovo.getId().equals(cartao.getIdStatusCartao()), CARTAO_STATUS_IGUAL);

          //Verifica se novo status do cartão permite cancelamento
          checkThrow(statusCartaoNovo.getFlagCancelamento().equals(NAO.getFlag()), CARTAO_STATUS_NAO_PERMITE_CANCELAMENTO);

          Long statusAnterior = cartao.getIdStatusCartao();
          LocalDateTime dataStatusAnterior = cartao.getDataStatusCartao();

          //Atualiza status do cartão
          cartao.setIdStatusCartao(statusCartaoNovo.getId());
          cartao.setDataStatusCartao(LocalDateTime.now());
          cartaoRepository.save(cartao);

          //Cria log de evento status
          LogEventos logEventos = new LogEventos(TabelasSistema.CARTOES.getId(), "Status", cartao.getNumeroCartao(), statusAnterior.toString(), statusCartaoNovo.getId().toString(), USUARIO_RESPONSAVEL, PROCESSO_BLOQUEIO);
          logEventosService.criarLogEventos(logEventos);

          //Cria log de evento status data
          LogEventos logEventosData = new LogEventos(TabelasSistema.CARTOES.getId(), "StatusData", cartao.getNumeroCartao(), formatarDataLogEvento(dataStatusAnterior), formatarDataLogEvento(LocalDateTime.now()), USUARIO_RESPONSAVEL, PROCESSO_BLOQUEIO);
          logEventosService.criarLogEventos(logEventosData);

          //Cria historico de cancelamento
          historicoCancelamentoCartaoService.criarHistoricoCancelamentoCartao(cartao, statusCartaoNovo, observacao);

          return convertCartaoResponse(cartao);
     }

     public void alterarStatusCartao(final Long id, final CartaoStatusRequest cartaoStatusRequest) {

          final StatusCartaoEnum statusEnum = cartaoStatusRequest.getStatusCartao();

          final List<StatusCartoes> statusCartao = statusCartaoService.buscarTodos();

          final StatusCartoes statusDestino = statusCartao.stream()
                  .filter(p -> StringUtils.equals(p.getNome(), statusEnum.getNome()))
                  .findFirst()
                  .orElseThrow(() -> new BadRequestCdt(STATUS_CARTAO_ERRO_INVALIDO.getMessage()));

          switch (statusEnum) {
              case BLOQUEADO_TEMPORARIO:
                  cartaoPierRestRepository.bloquear(id, statusDestino.getId(), cartaoStatusRequest.getMotivo());
                  break;

              case NORMAL:
                  cartaoPierRestRepository.desbloquear(id);
                  break;

              default:
                  final Cartoes actualCartao = buscarCartaoPorId(id);

                  // Cancelar Cartão
                  if (statusDestino.getFlagCancelamento().equals(NumberUtils.INTEGER_ONE)) {
                      cartaoServiceAOP.cancelarCartao(id, statusDestino.getId(), CANCELAR_CARTAO_OBSERVACAO);
                      gerarNovaVia(id, actualCartao.getIdPessoa());
                  }
                  break;
          }

          adicionaAgendamento(id, cartaoStatusRequest, statusDestino);
     }

     /**
      * convertCartaoResponse
      *
      * @param cartao
      * @return CartaoResponse
      */
     private CartaoResponse convertCartaoResponse(final Cartoes cartao) {

          CartaoResponse cartaoResponse = new CartaoResponse();

          cartaoResponse.setId(cartao.getId());
          cartaoResponse.setIdStatusCartao(cartao.getIdStatusCartao());
          cartaoResponse.setIdEstagioCartao(cartao.getIdEstagioCartao());
          cartaoResponse.setIdConta(cartao.getPortador().getId().getIdConta());
          cartaoResponse.setIdPessoa(cartao.getIdPessoa());

          cartaoResponse.setTipoPortador(TITULAR.getCodigo().equals(cartao.getPortador().getId().getPortador()) ? TITULAR.getTipo() : ADICIONAL.getTipo());
          cartaoResponse.setNumeroCartao(Converters.mascararNumeroCartao(cartao.getNumeroCartao()));
          cartaoResponse.setNomeImpresso(cartao.getPortador().getNomeImpresso());
          cartaoResponse.setDataGeracao(cartao.getDataGeracao());
          cartaoResponse.setDataStatusCartao(cartao.getDataStatusCartao());
          cartaoResponse.setDataEstagioCartao(cartao.getDataEstagioCartao());
          cartaoResponse.setDataValidade(cartao.getDataValidade());
          cartaoResponse.setDataImpressao(cartao.getDataImpressao());
          cartaoResponse.setArquivoImpressao(cartao.getArquivoImpressao());
          cartaoResponse.setFlagImpressaoOrigemComercial(cartao.getFlagImpressaoOrigemComercial());
          cartaoResponse.setFlagVirtual(cartao.getFlagProvisorio());
          cartaoResponse.setCodigoDesbloqueio(cartao.getCodigoDesbloqueio());
          cartaoResponse.setSequencialCartao(cartao.getSequencialCartao());

          cartaoResponse.setFlagTitular(TITULAR.getCodigo().equals(cartao.getPortador().getId().getPortador()) ? TITULAR.getCodigo() : ADICIONAL.getCodigo());
          cartaoResponse.setIdStatus(cartao.getIdStatusCartao());
          cartaoResponse.setDataStatus(cartao.getDataStatusCartao());
          cartaoResponse.setIdEstagio(cartao.getIdEstagioCartao());
          cartaoResponse.setDataEstagio(cartao.getDataEstagioCartao());
          cartaoResponse.setNumeroBin(cartao.getBin());
          cartaoResponse.setNumeroCartaoHash(cartao.getCartaoHash());
          cartaoResponse.setNumeroCartaoCriptografado(cartao.getCriptografiaHSM());
          cartaoResponse.setDataEmissao(cartao.getDataGeracao());
          cartaoResponse.setCartaoVirtual(cartao.getFlagProvisorio());
          cartaoResponse.setImpressaoAvulsa(cartao.getFlagImpressaoOrigemComercial());
          cartaoResponse.setNomeArquivoImpressao(cartao.getArquivoImpressao());

          return cartaoResponse;
     }

     private Cartoes buscarCartaoPorId(final Long id) {
          return cartaoRepository.findById(id).orElseThrow(() -> new NotFoundCdt(CARTAO_NAO_ENCONTRADO.getMessage()));
     }

     private void gerarNovaVia(final Long id, final Long idPessoa) {

          final FuncionarioRequest funcionarioRequest = new FuncionarioRequest();
          funcionarioRequest.setIdPessoa(idPessoa);

          final FuncionarioResponse funcionarioResponse = funcionarioBlueRestRepository
                  .consultarFuncionario(funcionarioRequest)
                  .stream()
                  .findFirst()
                  .orElseThrow(() -> new NotFoundCdt(FUNCIONARIO_NAO_ENCONTRADO.getMessage()));

          final EmpresaResponse empresaResponse = empresaBlueRestRepository
                  .listarEmpresas(Collections.singletonList(funcionarioResponse.getIdEmpresa()))
                  .stream()
                  .findFirst()
                  .orElseThrow(() -> new NotFoundCdt(EMPRESA_NAO_ENCONTRADA.getMessage()));

          final ParametroGrupoEmpresa parametrosGruposEmpresas = parametrosService
                  .buscaParametrosGrupoEmpresaPor(empresaResponse.getIdGrupoEmpresa(), PARAMETRO_RH, FLAG_EMBOSSA_NOME_PORTADOR);

          if (Objects.nonNull(parametrosGruposEmpresas) && parametrosGruposEmpresas.getValor().equals(PARAMETRO_HABILITADO)) {
               br.com.conductor.rhblueapi.domain.pier.CartaoResponse cartao = cartaoPierRestRepository.gerarNovaVia(id);
               checkThrow(Objects.isNull(cartao), ERRO_GERACAO_CARTAO);
          }
     }

     private void adicionaAgendamento(final Long id, final CartaoStatusRequest cartaoStatusRequest, final StatusCartoes statusDestino) {

          if (!Objects.isNull(cartaoStatusRequest.getDataAgendamentoFim())) {
               this.cartaoPierRestRepository.agendaTrocaStatus(id, AgendamentoStatusCartao.builder()
                       .dataFim(cartaoStatusRequest.getDataAgendamentoFim())
                       .dataInicio(cartaoStatusRequest.getDataAgendamentoInicio())
                       .idUsuarioLogado(cartaoStatusRequest.getIdUsuario())
                       .status(statusDestino.getId())
                       .motivo(MOTIVO_AGENDAMENTO_STATUS)
                       .build());
          }
     }

}
