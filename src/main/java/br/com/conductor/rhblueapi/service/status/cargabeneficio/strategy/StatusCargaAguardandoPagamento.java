
package br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy;

import static br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum.AGUARDANDO_PAGAMENTO;

import br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum;

public class StatusCargaAguardandoPagamento implements StatusCargaBeneficioStrategy {

     @Override
     public StatusPagamentoBaseEnum retornaStatusPagamento(boolean todosStatusIguais, int qtdCargasPedido, int qtdCargasPedidoPagas) {

          return AGUARDANDO_PAGAMENTO;
     }

}