
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.TipoStatus.PERMISSOES_USUARIOS_RH;
import static br.com.conductor.rhblueapi.enums.TipoStatus.USUARIOS_NIVEIS_PERMISSOES_RH;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.CONTADOR_VALOR_ZERO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.repository.UsuarioRepository;
import br.com.conductor.rhblueapi.domain.Empresa;
import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacional;
import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.SubgrupoEmpresa;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRhCustom;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.UsuarioNivelPermissaoRhPersist;
import br.com.conductor.rhblueapi.domain.request.UsuarioNivelPermissaoRhRequest;
import br.com.conductor.rhblueapi.domain.response.PageUsuarioNivelPermissaoRHResponse;
import br.com.conductor.rhblueapi.domain.response.UsuarioNivelPermissaoRhResponse;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.enums.StatusNivelPermissaoAcessoEnum;
import br.com.conductor.rhblueapi.enums.StatusPermissaoEnum;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.EmpresaRepository;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.PermissoesUsuariosRhRepository;
import br.com.conductor.rhblueapi.repository.SubgrupoRepository;
import br.com.conductor.rhblueapi.repository.UsuarioNivelPermissaoRhCustomRepository;
import br.com.conductor.rhblueapi.repository.UsuarioNivelPermissaoRhRepository;
import br.com.conductor.rhblueapi.service.strategy.nivelpermissao.GerenciarNivelPermissaoStrategy;
import br.com.conductor.rhblueapi.service.strategy.nivelpermissao.NivelPermissaoFactory;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.GenericConvert;

@Service
public class UsuarioNivelPermissaoRhService {

     @Autowired
     private UsuarioRepository usuarioRepository;

     @Autowired
     private PermissoesUsuariosRhRepository permissoesUsuariosRhRepository;

     @Autowired
     private UsuarioNivelPermissaoRhRepository usuarioNivelPermissaoRhRepository;

     @Autowired
     private EmpresaRepository empresaRepository;

     @Autowired
     private SubgrupoRepository subgrupoRepository;

     @Autowired
     private GrupoEmpresaRepository grupoEmpresaRepository;

     @Autowired
     private UsuarioNivelPermissaoRhCustomRepository usuarioNivelPermissaoRhCustomRepository;

     @Autowired
     private StatusCached statusCached;

     private List<StatusDescricao> dominioStatusCargas;

     private static final Integer STATUS_ATIVO_NIVEL_PERMISSAO = 1;

     private static final Integer STATUS_ATIVO = 1;

     private static final Integer STATUS_INATIVO = 0;

     private static final Integer STATUS_BLOQUEADO = 2;

     @Autowired
     private HierarquiaOrganizacionalService hierarquiaOrganizacionalService;

     @Autowired
     private NivelPermissaoFactory nivelPermissaoFactory;
     
     @Autowired
     private UsuarioPierService usuarioPierService;
     
     @Autowired
     private GrupoEmpresaService grupoEmpresaService;
     
     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;

     public String salvarNivelPermissaoUsuario(UsuarioNivelPermissaoRhPersist persist, Long idPermissao) {

          Optional<Usuario> usuarioRegistro = usuarioRepository.findById(persist.getIdUsuarioRegistro());

          NotFoundCdt.checkThrow(!usuarioRegistro.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO);

          Optional<PermissoesUsuariosRh> permissaoUsuario = permissoesUsuariosRhRepository.findById(idPermissao);

          NotFoundCdt.checkThrow(!permissaoUsuario.isPresent(), ExceptionsMessagesCdtEnum.PERMISSAO_NAO_ENCONTRADA);

          NotFoundCdt.checkThrow(!Objects.equals(permissaoUsuario.get().getStatus(), STATUS_ATIVO_NIVEL_PERMISSAO), ExceptionsMessagesCdtEnum.PERMISSAO_NAO_ENCONTRADA);

          Optional<PermissoesUsuariosRh> permissaoUsuarioRegistroValido = permissoesUsuariosRhRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(usuarioRegistro.get().getId(), permissaoUsuario.get().getGrupoEmpresa().getId(), STATUS_ATIVO);

          ForbiddenCdt.checkThrow(!permissaoUsuarioRegistroValido.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          ForbiddenCdt.checkThrow(!Objects.equals(permissaoUsuarioRegistroValido.get().getStatus(), STATUS_ATIVO_NIVEL_PERMISSAO), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          mapNivelAcesso(persist, idPermissao, permissaoUsuario.get(), permissaoUsuarioRegistroValido.get().getId());

          return "Permissão adicionada com sucesso";

     }

     public PageUsuarioNivelPermissaoRHResponse listarNivelPermissaoUsuario(UsuarioNivelPermissaoRhRequest request, Integer page, Integer size) {

          Pageable pageable = PageRequest.of(page, size);

          StatusDescricao statusNivelPermissao = null;

          if (Objects.nonNull(request.getStatus())) {
               dominioStatusCargas = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());

               statusNivelPermissao = mapStatusDescricao(dominioStatusCargas, request.getStatus().name());
          }

          Integer contador = usuarioNivelPermissaoRhCustomRepository.countUsuariosPorPermissoes(request, statusNivelPermissao);

          NoContentCdt.checkThrow((contador == CONTADOR_VALOR_ZERO), ExceptionsMessagesCdtEnum.PESQUISA_NAO_ENCONTRADA);

          List<UsuarioPermissaoNivelAcessoRhCustom> listaNiveisPermissoesPaginada = usuarioNivelPermissaoRhCustomRepository.buscarUsuarioPorNiveisPermissoes(request, statusNivelPermissao, page, size);

          List<UsuarioNivelPermissaoRhResponse> listaDadosConvertidos = convertDados(listaNiveisPermissoesPaginada);

          PageImpl<UsuarioNivelPermissaoRhResponse> pageDados = new PageImpl<>(listaDadosConvertidos, pageable, contador);

          PageUsuarioNivelPermissaoRHResponse pageReponse = new PageUsuarioNivelPermissaoRHResponse(GenericConvert.convertModelMapperToPageResponse(pageDados, new TypeToken<List<UsuarioNivelPermissaoRhResponse>>() {
          }.getType()));

          return pageReponse;

     }

     private List<UsuarioNivelPermissaoRhResponse> convertDados(List<UsuarioPermissaoNivelAcessoRhCustom> listaPermissoesPaginada) {

          List<UsuarioNivelPermissaoRhResponse> listaDadosConvertidos = new ArrayList<>();

          UsuarioNivelPermissaoRhResponse usuarioNivelPermissao;

          for (UsuarioPermissaoNivelAcessoRhCustom usr : listaPermissoesPaginada) {
               usuarioNivelPermissao = new UsuarioNivelPermissaoRhResponse();
               usuarioNivelPermissao.setIdNivelPermissao(usr.getIdNivelPermissao());
               usuarioNivelPermissao.setIdPermissao(usr.getIdPermissao());
               usuarioNivelPermissao.setStatus(convertStatusNivel(usr));
               mapResponseUsuarioNivel(usr, usuarioNivelPermissao);
               usuarioNivelPermissao.setIdUsuarioRegistro(usr.getIdUsuarioRegistro());
               converterStatusPermissao(usr, usuarioNivelPermissao);
               usuarioNivelPermissao.setNomeUsuarioNivel(usr.getNomeUsuario());
               usuarioNivelPermissao.setCpfUsuarioNivel(usr.getCpfUsuario().trim());

               listaDadosConvertidos.add(usuarioNivelPermissao);

          }

          return listaDadosConvertidos;
     }

     private void mapResponseUsuarioNivel(UsuarioPermissaoNivelAcessoRhCustom usuarioPermissaoNivelAcessoRh, UsuarioNivelPermissaoRhResponse usuario) {

          if (Objects.nonNull(usuarioPermissaoNivelAcessoRh.getIdGrupoEmpresa())) {
               usuario.setNivelAcesso(NivelPermissaoEnum.GRUPO);
               usuario.setNomeNivel(usuarioPermissaoNivelAcessoRh.getNomeGrupoEmpresa());
          }

          if (Objects.nonNull(usuarioPermissaoNivelAcessoRh.getIdSubgrupoEmpresa())) {
               usuario.setNivelAcesso(NivelPermissaoEnum.SUBGRUPO);
               usuario.setNomeNivel(usuarioPermissaoNivelAcessoRh.getNomeSubGrupoEmpresa());
          }

          if (Objects.nonNull(usuarioPermissaoNivelAcessoRh.getIdEmpresa())) {
               usuario.setNivelAcesso(NivelPermissaoEnum.EMPRESA);
               usuario.setNomeNivel(usuarioPermissaoNivelAcessoRh.getNomeEmpresa());
          }
     }

     private StatusNivelPermissaoAcessoEnum convertStatusNivel(UsuarioPermissaoNivelAcessoRhCustom usuarioPermissaoNivelAcessoRh) {

          if (usuarioPermissaoNivelAcessoRh.getStatus() == STATUS_INATIVO) {
               return StatusNivelPermissaoAcessoEnum.INATIVO;
          } else {
               return StatusNivelPermissaoAcessoEnum.ATIVO;
          }
     }

     private void converterStatusPermissao(UsuarioPermissaoNivelAcessoRhCustom usuarioPermissaoNivelAcessoRh, UsuarioNivelPermissaoRhResponse usuario) {

          if (usuarioPermissaoNivelAcessoRh.getStatusPermissao() == STATUS_INATIVO) {
               usuario.setStatusPermissao(StatusPermissaoEnum.INATIVO);
          }

          if (usuarioPermissaoNivelAcessoRh.getStatusPermissao() == STATUS_ATIVO) {
               usuario.setStatusPermissao(StatusPermissaoEnum.ATIVO);
          }

          if (usuarioPermissaoNivelAcessoRh.getStatusPermissao() == STATUS_BLOQUEADO) {
               usuario.setStatusPermissao(StatusPermissaoEnum.BLOQUEADO);
          }
     }

     private void mapNivelAcesso(UsuarioNivelPermissaoRhPersist persist, Long idPermissao, PermissoesUsuariosRh permissaoUsuario, Long idPermissaoUsuarioRegistro) {

          switch (persist.getNivelAcesso()) {
               case GRUPO:

                    mapNivelAcessoGrupo(persist, idPermissao, permissaoUsuario, idPermissaoUsuarioRegistro);

                    break;

               case SUBGRUPO:

                    mapNivelAcessoSubgrupo(persist, idPermissao, permissaoUsuario, idPermissaoUsuarioRegistro);

                    break;

               case EMPRESA:

                    mapNivelAcessoEmpresa(persist, idPermissao, permissaoUsuario, idPermissaoUsuarioRegistro);

                    break;

               default:
                    break;
          }

     }

     private void mapNivelAcessoEmpresa(UsuarioNivelPermissaoRhPersist persist, Long idPermissao, PermissoesUsuariosRh permissaoUsuario, Long idPermissaoUsuarioRegistro) {

          List<Long> idEmpresasValidos = validaEmpresa(persist, permissaoUsuario, idPermissaoUsuarioRegistro);
          
          idEmpresasValidos.stream().forEach(idEmpresa -> {

               HierarquiaOrganizacional hierarquiaOrganizacional = hierarquiaOrganizacionalService.buscaHierarquiaOrganizacional(null, null, idEmpresa);

               this.gerenciaNiveisAcesso(persist.getNivelAcesso(), idEmpresa, idPermissao, persist.getIdUsuarioRegistro(), hierarquiaOrganizacional);
          });
     }

     private void mapNivelAcessoSubgrupo(UsuarioNivelPermissaoRhPersist persist, Long idPermissao, PermissoesUsuariosRh permissaoUsuario, Long idPermissaoUsuarioRegistro) {

          List<Long> idSubgruposValidos = validaSubgrupos(persist, permissaoUsuario, idPermissaoUsuarioRegistro);
          
          idSubgruposValidos.stream().forEach(idSubgrupo -> {

               HierarquiaOrganizacional hierarquiaOrganizacional = hierarquiaOrganizacionalService.buscaHierarquiaOrganizacional(null, idSubgrupo, null);

               this.gerenciaNiveisAcesso(persist.getNivelAcesso(), idSubgrupo, idPermissao, persist.getIdUsuarioRegistro(), hierarquiaOrganizacional);
          });

     }
     
     private void gerenciaNiveisAcesso(NivelPermissaoEnum nivelAcesso, Long idPretendido, Long idPermissao, Long idUsuarioRegistro, HierarquiaOrganizacional hierarquiaOrganizacional) {

          List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso = new ArrayList<>();

          GerenciarNivelPermissaoStrategy gerenciadorNivelPermissao = nivelPermissaoFactory.criar(nivelAcesso);

          List<UsuarioPermissaoNivelAcessoRh> listaNiveisPermissaoSalvar;

          try {

               listaNiveisAcesso = buscaNiveisAcessoAtivosPor(idPermissao);

               listaNiveisPermissaoSalvar = new ArrayList<>();

               if (!gerenciadorNivelPermissao.isPossuiNivelAcessoPretendido(idPretendido, listaNiveisAcesso))
                    listaNiveisPermissaoSalvar.add(gerenciadorNivelPermissao.criarNivelAcesso(idPretendido, idPermissao, idUsuarioRegistro));

               listaNiveisPermissaoSalvar.addAll(gerenciadorNivelPermissao.retornarNiveisAcessoInativado(hierarquiaOrganizacional, listaNiveisAcesso));

               if (CollectionUtils.isEmpty(listaNiveisPermissaoSalvar)) {
                    return;
               }
       
               
               this.salvarNiveisPermissaoLote(listaNiveisPermissaoSalvar);

          } catch (NotFoundCdt eNF) {
               this.salvarNivelAcesso(gerenciadorNivelPermissao.criarNivelAcesso(idPretendido, idPermissao, idUsuarioRegistro));
          }

     }

     private void mapNivelAcessoGrupo(UsuarioNivelPermissaoRhPersist persist, Long idPermissao, PermissoesUsuariosRh permissaoUsuario, Long idPermissaoUsuarioRegistro) {

          List<Long> idGruposValidos = validaGrupo(persist, permissaoUsuario, idPermissaoUsuarioRegistro);

          Long idGrupoPretendido = idGruposValidos.get(0);
          
               
          HierarquiaOrganizacional hierarquiaOrganizacional = hierarquiaOrganizacionalService.buscaHierarquiaOrganizacional(idGrupoPretendido, null, null);

          this.gerenciaNiveisAcesso(persist.getNivelAcesso(), idGrupoPretendido, idPermissao, persist.getIdUsuarioRegistro(), hierarquiaOrganizacional);

     }

     private List<Long> validaGrupo(UsuarioNivelPermissaoRhPersist persist, PermissoesUsuariosRh permissaoUsuario, Long idPermissaoUsuarioRegistro) {

          BadRequestCdt.checkThrow(persist.getIdNivelAcesso().size() > 1, ExceptionsMessagesCdtEnum.LISTA_DE_NIVEL_POR_GRUPO_ACEITA_APENAS_UM_PARAMETRO);

          List<Long> listaGrupoValid = new ArrayList<>();

          Optional<GrupoEmpresa> dadosGrupo = grupoEmpresaRepository.findById(persist.getIdNivelAcesso().get(0));

          NotFoundCdt.checkThrow(!dadosGrupo.isPresent(), ExceptionsMessagesCdtEnum.NIVEL_ACESSO_NAO_EXISTE);

          ForbiddenCdt.checkThrow(!Objects.equals(dadosGrupo.get().getId(), permissaoUsuario.getGrupoEmpresa().getId()), ExceptionsMessagesCdtEnum.NIVEL_PERMISSAO_ACESSO_NAO_AUTORIZADO);

          ForbiddenCdt.checkThrow(!usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdGrupoEmpresaAndStatus(idPermissaoUsuarioRegistro, dadosGrupo.get().getId(), STATUS_ATIVO_NIVEL_PERMISSAO).isPresent(), ExceptionsMessagesCdtEnum.NIVEL_PERMISSAO_ACESSO_NAO_AUTORIZADO);

          listaGrupoValid.add(persist.getIdNivelAcesso().get(0));

          return listaGrupoValid;
     }

     private List<Long> validaSubgrupos(UsuarioNivelPermissaoRhPersist persist, PermissoesUsuariosRh permissaoUsuario, Long idPermissaoUsuarioRegistro) {

          List<Long> listaSubgrupoValido = new ArrayList<>();

          for (long idSubgrupo : persist.getIdNivelAcesso()) {

               Optional<SubgrupoEmpresa> dadosSubgrupo = subgrupoRepository.findById(idSubgrupo);

               NotFoundCdt.checkThrow(!dadosSubgrupo.isPresent(), ExceptionsMessagesCdtEnum.NIVEL_ACESSO_NAO_EXISTE);

               ForbiddenCdt.checkThrow(!Objects.equals(dadosSubgrupo.get().getIdGrupoEmpresaPai(), permissaoUsuario.getGrupoEmpresa().getId()), ExceptionsMessagesCdtEnum.NIVEL_PERMISSAO_ACESSO_NAO_AUTORIZADO);

               ForbiddenCdt.checkThrow(!usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdGrupoEmpresaAndStatus(idPermissaoUsuarioRegistro, dadosSubgrupo.get().getIdGrupoEmpresaPai(), STATUS_ATIVO_NIVEL_PERMISSAO).isPresent() && !usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdSubgrupoEmpresaAndStatus(idPermissaoUsuarioRegistro, dadosSubgrupo.get().getId(), STATUS_ATIVO_NIVEL_PERMISSAO).isPresent(), ExceptionsMessagesCdtEnum.NIVEL_PERMISSAO_ACESSO_NAO_AUTORIZADO);

               listaSubgrupoValido.add(idSubgrupo);

          }

          return listaSubgrupoValido;
     }

     private List<Long> validaEmpresa(UsuarioNivelPermissaoRhPersist persist, PermissoesUsuariosRh permissaoUsuario, Long idPermissaoUsuarioRegistro) {

          List<Long> listaEmpresaValido = new ArrayList<>();

          for (long idEmpresa : persist.getIdNivelAcesso()) {

               Optional<Empresa> dadosEmpresa = empresaRepository.findById(idEmpresa);

               NotFoundCdt.checkThrow(!dadosEmpresa.isPresent(), ExceptionsMessagesCdtEnum.NIVEL_ACESSO_NAO_EXISTE);

               Optional<SubgrupoEmpresa> dadosSubgrupo = subgrupoRepository.findById(dadosEmpresa.get().getIdGrupoEmpresa());

               ForbiddenCdt.checkThrow(!Objects.equals(dadosSubgrupo.get().getIdGrupoEmpresaPai(), permissaoUsuario.getGrupoEmpresa().getId()), ExceptionsMessagesCdtEnum.NIVEL_PERMISSAO_ACESSO_NAO_AUTORIZADO);

               ForbiddenCdt.checkThrow(!usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdGrupoEmpresaAndStatus(idPermissaoUsuarioRegistro, dadosSubgrupo.get().getIdGrupoEmpresaPai(), STATUS_ATIVO_NIVEL_PERMISSAO).isPresent() && !usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdEmpresaAndStatus(idPermissaoUsuarioRegistro, idEmpresa, STATUS_ATIVO_NIVEL_PERMISSAO).isPresent() && !usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdSubgrupoEmpresaAndStatus(idPermissaoUsuarioRegistro, dadosSubgrupo.get().getId(), STATUS_ATIVO_NIVEL_PERMISSAO).isPresent(), ExceptionsMessagesCdtEnum.NIVEL_PERMISSAO_ACESSO_NAO_AUTORIZADO);

               listaEmpresaValido.add(idEmpresa);
          }

          return listaEmpresaValido;
     }

     /**
      * método busca uma permissão por grupo e retornar uma exceção se não localizar permissâo .
      * 
      * @param idGrupo
      * @param idPermissao
      * @return
      */
     public UsuarioPermissaoNivelAcessoRh autenticarPermissaoNivelGrupoAtiva(Long idGrupo, Long idPermissao) {

          Optional<UsuarioPermissaoNivelAcessoRh> nivelPermissaoGrupo = usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdGrupoEmpresaAndStatus(idPermissao, idGrupo, STATUS_ATIVO);

          ForbiddenCdt.checkThrow(!nivelPermissaoGrupo.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          return nivelPermissaoGrupo.get();
     }

     public Optional<UsuarioPermissaoNivelAcessoRh> obterUsuarioNivelPermissaoGrupoAtivo(final Long idPermissao, final Long idGrupoEmpresa) {

          dominioStatusCargas = this.statusCached.busca(USUARIOS_NIVEIS_PERMISSOES_RH.getNome());
          final StatusDescricao statusNivelPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, StatusNivelPermissaoAcessoEnum.ATIVO.name());
          return usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdGrupoEmpresaAndStatus(idPermissao, idGrupoEmpresa, statusNivelPermissaoAtiva.getStatus());
     }

     public void deletePermissaoNivelAcessoByIdEmpresa(Long idEmpresa) {
          Optional<UsuarioPermissaoNivelAcessoRh> permissaoNivelAcessoRh = usuarioNivelPermissaoRhRepository.findByIdEmpresa(idEmpresa);

          permissaoNivelAcessoRh.ifPresent(nivelAcesso -> usuarioNivelPermissaoRhRepository.delete(nivelAcesso));
     }

     public static boolean isAcessoNivelGrupo(HierarquiaOrganizacional hierarquiaOrganizacional, List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso) {

          return listaNiveisAcesso.stream().anyMatch(nivel -> Objects.nonNull(nivel.getIdGrupoEmpresa()) && nivel.getIdGrupoEmpresa().equals(hierarquiaOrganizacional.getIdGrupoEmpresa()));
     }

     public static boolean isAcessoNivelSubgrupo(HierarquiaOrganizacional hierarquiaOrganizacional, List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso) {

          return listaNiveisAcesso.stream().anyMatch(nivel -> Objects.nonNull(nivel.getIdSubgrupoEmpresa()) && hierarquiaOrganizacional.getListaIdsSubgrupoEmpresa().contains(nivel.getIdSubgrupoEmpresa()));
     }

     public void salvarNivelAcessoViaSalesforce(Long idGrupoEmpresa, Long idSubgrupoEmpresa, Long idEmpresa, Long idPermissao, Long idUsuarioRegistro, NivelPermissaoEnum nivel) {

          try {

               List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso = this.buscaNiveisAcessoAtivosPor(idPermissao);

               HierarquiaOrganizacional hierarquiaOrganizacional = hierarquiaOrganizacionalService.buscaHierarquiaOrganizacional(idGrupoEmpresa, idSubgrupoEmpresa, idEmpresa);

               GerenciarNivelPermissaoStrategy gerenciadorNivelPermissao = nivelPermissaoFactory.criar(nivel);

               boolean isNivelAcessoSuperior = gerenciadorNivelPermissao.isPossuiAcessoNivelSuperior(hierarquiaOrganizacional, listaNiveisAcesso);

               if (Boolean.FALSE.equals(isNivelAcessoSuperior))
                    this.criarNivelAcesso(idGrupoEmpresa, idSubgrupoEmpresa, idEmpresa, idPermissao, idUsuarioRegistro);

          } catch (NotFoundCdt nfe) {
               this.criarNivelAcesso(idGrupoEmpresa, idSubgrupoEmpresa, idEmpresa, idPermissao, idUsuarioRegistro);
          }
     }

     private List<UsuarioPermissaoNivelAcessoRh> buscaNiveisAcessoAtivosPor(Long idPermissao) {

          final StatusDescricao statusNivelAcessoAtivo = mapStatusDescricao(this.statusCached.busca(USUARIOS_NIVEIS_PERMISSOES_RH.getNome()), StatusNivelPermissaoAcessoEnum.ATIVO.getDescricao());

          List<UsuarioPermissaoNivelAcessoRh> listaNivelPermissao = usuarioNivelPermissaoRhRepository.findByIdPermissaoAndStatus(idPermissao, statusNivelAcessoAtivo.getStatus());

          NotFoundCdt.checkThrow(CollectionUtils.isEmpty(listaNivelPermissao), ExceptionsMessagesCdtEnum.NIVEL_ACESSO_NAO_ENCONTRADO);

          return listaNivelPermissao;
     }
     
     public List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcessosPor(Long idPermissao) {
          
          final StatusDescricao statusNivelAcessoAtivo = mapStatusDescricao(this.statusCached.busca(USUARIOS_NIVEIS_PERMISSOES_RH.getNome()), StatusNivelPermissaoAcessoEnum.ATIVO.getDescricao());
          
          List<UsuarioPermissaoNivelAcessoRh> listaNivelPermissao = usuarioNivelPermissaoRhRepository.findByIdPermissaoAndStatus(idPermissao, statusNivelAcessoAtivo.getStatus());
          
          return listaNivelPermissao;
     }
     
   public List<UsuarioPermissaoNivelAcessoRh> listarNiveisAcessoAtivosPor(Long idPermissao) {
          
          final StatusDescricao statusNivelAcessoAtivo = mapStatusDescricao(this.statusCached.busca(USUARIOS_NIVEIS_PERMISSOES_RH.getNome()), StatusNivelPermissaoAcessoEnum.ATIVO.getDescricao());
          
          List<UsuarioPermissaoNivelAcessoRh> listaNivelPermissao = usuarioNivelPermissaoRhRepository.findByIdPermissaoAndStatus(idPermissao, statusNivelAcessoAtivo.getStatus());
          
          return listaNivelPermissao;
     }
     

     private UsuarioPermissaoNivelAcessoRh montaNivelAcesso(Long idGrupoEmpresa, Long idSubgrupoEmpresa, Long idEmpresa, Long idPermissao, Long idUsuarioRegistro) {

          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(USUARIOS_NIVEIS_PERMISSOES_RH.getNome());
          final StatusDescricao statusNivelAcessoAtivo = mapStatusDescricao(dominioStatusCargas, StatusNivelPermissaoAcessoEnum.ATIVO.getDescricao());

          UsuarioPermissaoNivelAcessoRh nivelAcessoPermissao = new UsuarioPermissaoNivelAcessoRh();

          if (Objects.nonNull(idGrupoEmpresa))
               nivelAcessoPermissao.setIdGrupoEmpresa(idGrupoEmpresa);
          if (Objects.nonNull(idSubgrupoEmpresa))
               nivelAcessoPermissao.setIdSubgrupoEmpresa(idSubgrupoEmpresa);
          if (Objects.nonNull(idEmpresa))
               nivelAcessoPermissao.setIdEmpresa(idEmpresa);
          nivelAcessoPermissao.setIdPermissao(idPermissao);
          nivelAcessoPermissao.setIdUsuarioRegistro(idUsuarioRegistro);
          nivelAcessoPermissao.setDataRegistro(LocalDateTime.now());
          nivelAcessoPermissao.setStatus(statusNivelAcessoAtivo.getStatus());

          return nivelAcessoPermissao;
     }

     private UsuarioPermissaoNivelAcessoRh salvarNivelAcesso(UsuarioPermissaoNivelAcessoRh nivelAcesso) {

          usuarioNivelPermissaoRhRepository.save(nivelAcesso);

          return nivelAcesso;
     }

     private UsuarioPermissaoNivelAcessoRh criarNivelAcesso(Long idGrupoEmpresa, Long idSubgrupoEmpresa, Long idEmpresa, Long idPermissao, Long idUsuarioRegistro) {

          UsuarioPermissaoNivelAcessoRh nivelAcesso = this.montaNivelAcesso(idGrupoEmpresa, idSubgrupoEmpresa, idEmpresa, idPermissao, idUsuarioRegistro);

          return this.salvarNivelAcesso(nivelAcesso);
     }

     private void salvarNiveisPermissaoLote(List<UsuarioPermissaoNivelAcessoRh> listaNiveisPermissao) {

          usuarioNivelPermissaoRhRepository.saveAll(listaNiveisPermissao);
     }

     public UsuarioPermissaoNivelAcessoRh autenticarNivelAcessoRhGrupo(Long idUsuario, Long idGrupoEmpresa) {
          
          List<UsuarioPermissaoNivelAcessoRh> niveis = this.usuarioPossuiNivelHierarquiaNoGrupo(idUsuario, idGrupoEmpresa);

          List<UsuarioPermissaoNivelAcessoRh> niveisGrupo = niveis.stream().filter(n -> Objects.nonNull(n.getIdGrupoEmpresa())).collect(Collectors.toList());

          ForbiddenCdt.checkThrow(niveisGrupo.isEmpty(), ExceptionsMessagesCdtEnum.NAO_POSSUI_NIVEL_ACESSO_GRUPO);

          return niveisGrupo.get(0);
     }
     
     public List<UsuarioPermissaoNivelAcessoRh> usuarioPossuiNivelHierarquiaNoGrupo(Long idUsuario, Long idGrupoEmpresa) {
          
          usuarioPierService.autenticarUsuario(idUsuario);

          grupoEmpresaService.autenticarGrupoEmpresa(idGrupoEmpresa);

          PermissoesUsuariosRh permissao = permissoesUsuariosRhService.autenticarPermissaoGrupoAtiva(idUsuario, idGrupoEmpresa).get();

          return buscaNiveisAcessoAtivosPor(permissao.getId());
          
     }
}