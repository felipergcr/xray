
package br.com.conductor.rhblueapi.service.relatorios;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.relatorios.RelatorioPedidoCustom;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.relatorios.RelatorioPedidoCustomRepositoryImpl;

@Service
public class RelatorioPedidoService {

     @Autowired
     private RelatorioPedidoCustomRepositoryImpl relatorioPedidoCustomRepositoryImpl;

     public List<RelatorioPedidoCustom> autenticarRelatorioPedidoCustom(final Long idGrupoEmpresa, final LocalDate dataInicio, final LocalDate dataFim) {

          List<RelatorioPedidoCustom> listaRelatorioPedidos = relatorioPedidoCustomRepositoryImpl.findAllRelatorioPedidoCustom(idGrupoEmpresa, dataInicio, dataFim);

          NotFoundCdt.checkThrow(listaRelatorioPedidos.isEmpty(), ExceptionsMessagesCdtEnum.RELATORIO_PEDIDO_EMPTY);

          return listaRelatorioPedidos;
     }

     public List<RelatorioPedidoCustom> obterDadosRelatorioPedidoCustom(final Long idGrupoEmpresa, final LocalDate dataInicio, final LocalDate dataFim) {

          return relatorioPedidoCustomRepositoryImpl.findAllRelatorioPedidoCustom(idGrupoEmpresa, dataInicio, dataFim);
     }

}
