
package br.com.conductor.rhblueapi.service.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntrega;
import br.com.conductor.rhblueapi.domain.email.GenericoEmail;
import br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaEnum;

@Service
public class EmailFinalizaUnidadeEntregaService extends EmailUtilService<ArquivoUnidadeEntrega, StatusArquivoUnidadeEntregaEnum> {
     
     @Value("${app.pier.notificacao.parametros.header.img}")
     private String header_img;

     @Value("${app.pier.notificacao.parametros.footer.img}")
     private String footer_img;
     
     @Value("${app.pier.notificacao.unidade-entrega.sucesso.id}")
     private Long idTemplateUnidadeEntregaNotificacaoSucesso;
     
     @Value("${app.pier.notificacao.unidade-entrega.erros.id}")
     private Long idTemplateUnidadeEntregaNotificacaoErros;
     
     @Value("${app.pier.notificacao.unidade-entrega.url.sucesso}")
     private String unidadeEntregaSucessoLink;

     @Value("${app.pier.notificacao.unidade-entrega.url.erros}")
     private String unidadeEntregaErrosLink;
     

     @Override
     public void enviar(ArquivoUnidadeEntrega arquivo, StatusArquivoUnidadeEntregaEnum status) {
          
          GenericoEmail<StatusArquivoUnidadeEntregaEnum> genericoEmail = new GenericoEmail<StatusArquivoUnidadeEntregaEnum>();
          
          genericoEmail.setIdUsuario(arquivo.getIdUsuarioRegistro());
          genericoEmail.setIdObjeto(arquivo.getId());
          genericoEmail.setDataHora(arquivo.getDataStatus());
          genericoEmail.setStatus(status);
          
          super.enviar(genericoEmail);
     }

     @Override
     Long getContentTemplate(StatusArquivoUnidadeEntregaEnum status) {
          
          Long idTemplateUnidadeEntregaNotificao = null;
          
          switch (status) {
               case INVALIDADO:
                    idTemplateUnidadeEntregaNotificao = idTemplateUnidadeEntregaNotificacaoErros;
                    break;
               default:
                    idTemplateUnidadeEntregaNotificao = idTemplateUnidadeEntregaNotificacaoSucesso;
                    break;
          }
          return idTemplateUnidadeEntregaNotificao;

     }

     @Override
     String getContentUrl(Long idPath, StatusArquivoUnidadeEntregaEnum status) {

          String url = null;

          switch (status) {
               case PROCESSAMENTO_CONCLUIDO:
                    url = unidadeEntregaSucessoLink;
                    break;
               default:
                    url = unidadeEntregaErrosLink;
                    break;
          }
          return String.format(url, idPath);
     }
     

     @Override
     Map<String, Object> popularParametros(GenericoEmail<StatusArquivoUnidadeEntregaEnum> genericoEmail, String nomeDestinatario, String url) {
          
          Map<String, Object> parametros = new HashMap<>();
          parametros.put("header_img_unidadeEntrega", header_img);
          parametros.put("user_name", nomeDestinatario);
          parametros.put("date", genericoEmail.getDate());
          parametros.put("time", genericoEmail.getHour());
          parametros.put("new_password_link", url);
          parametros.put("footer_img_unidadeEntrega", footer_img);
          
          return parametros;
     }

}
