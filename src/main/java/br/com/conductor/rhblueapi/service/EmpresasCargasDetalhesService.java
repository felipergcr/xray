
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.StatusEmpresaCargaDetalheProdutoEnum.CARGA_EM_PROCESSAMENTO;
import static br.com.conductor.rhblueapi.enums.StatusEmpresaCargaDetalheProdutoEnum.CONFIRMADO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.EMPRESAS_CARGAS_DETALHES;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import br.com.conductor.rhblueapi.domain.EmpresasCargasDetalhes;
import br.com.conductor.rhblueapi.domain.EmpresasCargasDetalhes.EmpresasCargasDetalhesBuilder;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.proc.CadastroFuncionarioDetalhe;
import br.com.conductor.rhblueapi.repository.EmpresasCargasDetalhesRepository;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;

@Service
public class EmpresasCargasDetalhesService {

     @Autowired
     private EmpresasCargasDetalhesRepository empresasCargasDetalhesRepository;

     @Autowired
     private StatusCached statusCached;
     
     @Value("${app.cargas.paginacao.tamanho}")
     private Integer quantidadeItensPaginacao;
     
     public List<EmpresasCargasDetalhes> buscaEAlteraStatusEmLotePorIdsEmpresasCargas(List<Long> idsEmpresasCargas) {
          
          List<EmpresasCargasDetalhes> empresasCargasDetalhes = this.buscaEmpresasCargaPor(idsEmpresasCargas);

          final List<StatusDescricao> dominioEmpresaCargasDetalhes = this.statusCached.busca(EMPRESAS_CARGAS_DETALHES.getNome());
          final StatusDescricao statusEmpresasCargasDetalhesConfirmado = mapStatusDescricao(dominioEmpresaCargasDetalhes, CONFIRMADO.getStatus());

          return empresasCargasDetalhes.stream().map(detalhes -> {
               detalhes.setStatus(statusEmpresasCargasDetalhesConfirmado.getStatus());
               return detalhes;
          }).collect(toList());
     }
     
     public void salvarEmLote(List<EmpresasCargasDetalhes> empresasCargasDetalhes) {
          
          this.empresasCargasDetalhesRepository.saveAll(empresasCargasDetalhes);
     }

     private List<EmpresasCargasDetalhes> buscaEmpresasCargaPor(List<Long> idsEmpresasCargas) {
          
          List<EmpresasCargasDetalhes> listaEmpresasCargas = new ArrayList<EmpresasCargasDetalhes>();
          
          IntStream.range(0, (idsEmpresasCargas.size() + quantidadeItensPaginacao - 1) / quantidadeItensPaginacao)
          .mapToObj(i -> idsEmpresasCargas.subList(i * quantidadeItensPaginacao, Math.min(quantidadeItensPaginacao * (i + 1), idsEmpresasCargas.size())))
          .forEach(lista -> {
               listaEmpresasCargas.addAll(empresasCargasDetalhesRepository.findByIdEmpresaCargaIn(lista));
          });
          
          return listaEmpresasCargas;

     }
     
     public Long geraEmpresasCargasDetalhes(ArquivosCargasDetalhes detalhe, CadastroFuncionarioDetalhe cadastroFuncionarioDetalhe, Long idEmpresaCarga) {
          
          EmpresasCargasDetalhes empresaCargaDetalhe = this.empresasCargasDetalhesRepository
                    .findByIdFuncionarioAndIdEmpresaCarga(cadastroFuncionarioDetalhe.getIdFuncionario(), idEmpresaCarga);
                    
          final StatusDescricao statusCargasEmProcessamento = mapStatusDescricao(this.statusCached.busca(EMPRESAS_CARGAS_DETALHES.getNome()), CARGA_EM_PROCESSAMENTO.getStatus());
          
          final EmpresasCargasDetalhesBuilder cargaDetalhesBuilder = EmpresasCargasDetalhes.builder()
                    .status(statusCargasEmProcessamento.getStatus())
                    .idFuncionario(cadastroFuncionarioDetalhe.getIdFuncionario())
                    .idEmpresaCarga(idEmpresaCarga);

          BigDecimal valorTotal = new BigDecimal(detalhe.getValorBeneficio());
          
          if(Objects.nonNull(empresaCargaDetalhe)) {
               cargaDetalhesBuilder.id(empresaCargaDetalhe.getId());
               valorTotal = valorTotal.add(empresaCargaDetalhe.getValorTotal());
          }
          
          cargaDetalhesBuilder.valorTotal(valorTotal);
          
          empresaCargaDetalhe = this.empresasCargasDetalhesRepository.save(cargaDetalhesBuilder.build());
          
          return empresaCargaDetalhe.getId();
     }

}
