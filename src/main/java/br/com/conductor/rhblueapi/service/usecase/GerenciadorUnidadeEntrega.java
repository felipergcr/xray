
package br.com.conductor.rhblueapi.service.usecase;

import static br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaDetalhe.IMPORTADO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaDetalhe.VALIDADO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaEnum.PROCESSAMENTO_CONCLUIDO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaEnum.RECEBIDO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_UNIDADE_ENTREGA;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntrega;
import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaDetalhe;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.exception.BusinessException;
import br.com.conductor.rhblueapi.exception.ConversaoObjetoException;
import br.com.conductor.rhblueapi.exception.ObjetoVazioException;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.UnidadesEntregaPublish;
import br.com.conductor.rhblueapi.service.ArquivoUnidadeEntregaService;
import br.com.conductor.rhblueapi.service.UnidadeEntregaDetalheService;
import br.com.conductor.rhblueapi.service.UnidadeEntregaService;

@Component
public class GerenciadorUnidadeEntrega {

     @Autowired
     private StatusCached statusCached;
     
     @Autowired
     private UnidadesEntregaPublish unidadesEntregaPublish;
     
     @Autowired
     private ArquivoUnidadeEntregaService arquivoUnidadeEntregaService;
     
     @Autowired
     private UnidadeEntregaDetalheService unidadeEntregaDetalheService;
     
     @Autowired
     private UnidadeEntregaService unidadeEntregaService;

     public void processoImportacaoUnidadeEntrega(Long idArquivoUnidadeEntrega) {

          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(ARQUIVO_UNIDADE_ENTREGA.getNome());
          final StatusDescricao statusRecebido = mapStatusDescricao(dominioStatusCargas, RECEBIDO.getStatus());

          Optional<ArquivoUnidadeEntrega> arq = arquivoUnidadeEntregaService.buscaPorId(idArquivoUnidadeEntrega);
          
          arq.ifPresent(arquivo -> { 
               unidadeEntregaDetalheService.deletarArquivoCargasDetalhadoPor(arquivo.getId());
               arquivo.setStatus(statusRecebido.getStatus());
               arquivoUnidadeEntregaService.salvar(arquivo);
               
               try {

                    boolean isArquivoValido = arquivoUnidadeEntregaService.inicializaImportacao(arquivo);
                    arquivoUnidadeEntregaService.atualizaDadosPosProcessamento(arquivo, isArquivoValido, null);

                    if(isArquivoValido)
                         unidadesEntregaPublish.enviarIdUnidadeValidacaoNegocio(arquivo.getId());

               } catch (ObjetoVazioException | ConversaoObjetoException | BusinessException e) {
                    arquivoUnidadeEntregaService.atualizaDadosPosProcessamento(arquivo, false, e.getMessage());
               }
          });

     }

     public void gerenciaEDistribuiDetalhesArquivo(long idArquivoUnidadeEntrega) {
          
          Optional<List<ArquivoUnidadeEntregaDetalhe>> listaDetalhes = unidadeEntregaDetalheService.buscaListaDetalhesPorIdArquivo(idArquivoUnidadeEntrega);
          
          listaDetalhes.ifPresent(detalhes -> {
               
               detalhes.stream().forEach(detalhe -> {
                    unidadesEntregaPublish.enviarDetalheEGerarUnidades(detalhe);
               });

          });
          
     }
     
     private boolean isUltimoRegistroProcessado(Long idArquivoUnidadeEntrega) {

          return (unidadeEntregaDetalheService.buscaQuantidadeRegistrosPor(idArquivoUnidadeEntrega, VALIDADO) <= 0 
                    && arquivoUnidadeEntregaService.isStatusPedidoDiferenteDe(idArquivoUnidadeEntrega, PROCESSAMENTO_CONCLUIDO));
     }
     
     private void finalizaProcessosGerarUnidades(final ArquivoUnidadeEntrega arquivoUnidadeEntrega) {
          
          unidadeEntregaService.processoPersistenciaUnidades(arquivoUnidadeEntrega.getIdGrupoEmpresa());
          
          arquivoUnidadeEntregaService.alterarStatusProcessamentoConcluido(arquivoUnidadeEntrega);
     } 

     public void processoPersistenciaUnidades(ArquivoUnidadeEntregaDetalhe arquivoUnidadeEntregaDetalhe) {
          
          Optional<ArquivoUnidadeEntrega> optArquivoUnidadeEntrega = arquivoUnidadeEntregaService.buscaPorId(arquivoUnidadeEntregaDetalhe.getIdArquivoUE());
          
          ArquivoUnidadeEntrega arquivoUnidadeEntrega = optArquivoUnidadeEntrega.get();

          unidadeEntregaService.processoIncluirRegistro(arquivoUnidadeEntregaDetalhe, arquivoUnidadeEntrega.getIdGrupoEmpresa());
          
          unidadeEntregaDetalheService.alterarStatus(arquivoUnidadeEntregaDetalhe, IMPORTADO);
          
          if(this.isUltimoRegistroProcessado(arquivoUnidadeEntrega.getId())) {
               this.finalizaProcessosGerarUnidades(arquivoUnidadeEntrega);
          }
          
     }

}
