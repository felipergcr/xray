
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum.AGUARDANDO_PAGAMENTO;
import static br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum.CARGA_PROCESSANDO;
import static br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum.EXPIRADO;
import static br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum.PEDIDO_CANCELADO;
import static br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO;
import static br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum.PENDENCIA_LIMITE;
import static br.com.conductor.rhblueapi.enums.TipoStatus.CARGAS_BENEFICIOS;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.FRONT_RH;
import static java.util.stream.Collectors.toList;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.CargaBeneficio;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.status.CargaBeneficioMinimoCustom;
import br.com.conductor.rhblueapi.enums.StatusPagamentoEnum;
import br.com.conductor.rhblueapi.enums.TipoPagamentoPedidoEnum;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.functionalInterface.RetornaValorAtributo;
import br.com.conductor.rhblueapi.repository.CargaBeneficioRepository;
import br.com.conductor.rhblueapi.repository.status.statuscarga.CargaBeneficioStatusImpl;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;

@Service
public class CargaBeneficioService {

     @Autowired
     private CargaBeneficioRepository cargaBeneficioRepository;
     
     @Autowired 
     private CargaBeneficioStatusImpl cargaBeneficioStatusImpl; 
     
     @Autowired
     private StatusCached statusCached;

     public LocalDateTime buscaMenorDataAgendamentoDeCargaBenefecio(Long idArquivo) {

          Optional<LocalDateTime> cargaBeneficioFiltrado = getMenorDataAgendamento(idArquivo);

          NotFoundCdt.checkThrow(!cargaBeneficioFiltrado.isPresent(), ExceptionsMessagesCdtEnum.CARGA_NAO_ENCONTRADA);

          return cargaBeneficioFiltrado.get();
     }

     private Optional<LocalDateTime> getMenorDataAgendamento(Long idArquivo) {

          return cargaBeneficioRepository.findByIdArquivoCargas(idArquivo).stream().min(Comparator.comparing(CargaBeneficio::getDataAgendamento)).map(CargaBeneficio::getDataAgendamento);

     }

     public LocalDateTime getDataCargaBeneficio(Long idCarga) {

          Optional<CargaBeneficio> cargaBeneficioFiltrado = cargaBeneficioRepository.findByIdCargaBeneficio(idCarga);

          NotFoundCdt.checkThrow(!cargaBeneficioFiltrado.isPresent(), ExceptionsMessagesCdtEnum.CARGA_NAO_ENCONTRADA);

          return cargaBeneficioFiltrado.get().getDataAgendamento();

     }
     
     public List<CargaBeneficio> buscaEAlteraStatusEmLotePor(Long idArquivoCargas, final TipoPagamentoPedidoEnum tipoPagamentoPedido, final StatusPagamentoEnum statusPagamentoEnum) {

          final List<CargaBeneficio> cargasBeneficios = cargaBeneficioRepository.findByIdArquivoCargas(idArquivoCargas);

          final StatusDescricao statusDescricao = statusPagamentoCargaBeneficioPor(tipoPagamentoPedido, statusPagamentoEnum);

          return cargasBeneficios.stream().map(carga -> {
               carga.setStatus(statusDescricao.getStatus());
               return carga;
          }).collect(toList());
     }
     
     public void salvarEmLote(final List<CargaBeneficio> cargasBeneficios) {
          cargaBeneficioRepository.saveAll(cargasBeneficios);
     }

     public List<CargaBeneficio> atualizaStatusCargasBeneficioPor(Long idArquivoCargas, final TipoPagamentoPedidoEnum tipoPagamentoPedido, final StatusPagamentoEnum statusPagamentoEnum) {

          final List<CargaBeneficio> cargasBeneficios = cargaBeneficioRepository.findByIdArquivoCargas(idArquivoCargas);

          final StatusDescricao statusDescricao = statusPagamentoCargaBeneficioPor(tipoPagamentoPedido, statusPagamentoEnum);

          return this.cargaBeneficioRepository.saveAll(cargasBeneficios.stream().map(carga -> {
               carga.setStatus(statusDescricao.getStatus());
               return carga;
          }).collect(toList()));
     }

     private StatusDescricao statusPagamentoCargaBeneficioPor(final TipoPagamentoPedidoEnum tipoPagamentoPedido, final StatusPagamentoEnum statusPagamentoEnum) {

          if (tipoPagamentoPedido.equals(TipoPagamentoPedidoEnum.PRE)) {
               return statusCargaBeneficioParaPrePago();
          } else {
               return statusCargaBeneficioParaPosPago(statusPagamentoEnum);
          }
     }

     private StatusDescricao statusCargaBeneficioParaPrePago() {

          final List<StatusDescricao> dominioCargasBeneficios = this.statusCached.busca(CARGAS_BENEFICIOS.getNome());
          return mapStatusDescricao(dominioCargasBeneficios, AGUARDANDO_PAGAMENTO.getStatus());
     }
     
     private StatusDescricao statusCargaBeneficioParaPosPago(final StatusPagamentoEnum statusPagamentoEnum) {
          final List<StatusDescricao> dominioCargasBeneficios = this.statusCached.busca(CARGAS_BENEFICIOS.getNome());
          if(Objects.isNull(statusPagamentoEnum)) {
               return mapStatusDescricao(dominioCargasBeneficios, PEDIDO_CONFIRMADO.getStatus());
          } else {
               return statusPagamentoEnum.equals(StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE) || statusPagamentoEnum.equals(StatusPagamentoEnum.LIMITE_CREDITO_VENCIDO) ? mapStatusDescricao(dominioCargasBeneficios, PENDENCIA_LIMITE.getStatus()) : mapStatusDescricao(dominioCargasBeneficios, PEDIDO_CONFIRMADO.getStatus());
          }
     }
     
     public CargaBeneficio getCargaBeneficio(Long idCarga) {

          Optional<CargaBeneficio> cargaBeneficio = cargaBeneficioRepository.findByIdCargaBeneficio(idCarga);

          NotFoundCdt.checkThrow(!cargaBeneficio.isPresent(), ExceptionsMessagesCdtEnum.CARGA_NAO_ENCONTRADA);

          return cargaBeneficio.get();

     }
     
     private Long geraCargaBeneficio(ArquivoCargas arquivoCargas, LocalDate dataAgendamentoCarga, Long idEmpresa) {
          
          final StatusDescricao statusCargasEmProcessamento = mapStatusDescricao(this.statusCached.busca(CARGAS_BENEFICIOS.getNome()), CARGA_PROCESSANDO.getStatus());
          
          CargaBeneficio cargaBeneficio = this.cargaBeneficioRepository.save(CargaBeneficio.builder()
                    .status(statusCargasEmProcessamento.getStatus())
                    .nomeArquivo(arquivoCargas.getNome())
                    .idUsuario(arquivoCargas.getIdUsuario())
                    .idEmpresa(idEmpresa)
                    .idGrupoEmpresa(arquivoCargas.getIdGrupoEmpresa())
                    .origem(FRONT_RH)
                    .dataImportacao(arquivoCargas.getDataImportacao())
                    .dataAgendamento(dataAgendamentoCarga.atStartOfDay())
                    .dataProcessamento(LocalDateTime.now())
                    .idArquivoCargas(arquivoCargas.getId())
                    .build());
          
          return cargaBeneficio.getIdCargaBeneficio();
          
     } 
     
     public Optional<Long> buscaIdCargaBeneficioPor(Long idArquivoCargas, Long idEmpresa, Long idGrupoEmpresa) {
          
          Long idCargaBeneficio = cargaBeneficioRepository.findIdCargaBeneficioByIdArquivoCargasAndIdEmpresaAndIdGrupoEmpresa(idArquivoCargas, idEmpresa, idGrupoEmpresa);
          
          return Optional.ofNullable(idCargaBeneficio);
     }
     
     public Long persisteEObtemIdCargaBeneficio(ArquivoCargas arquivoCargas, LocalDate dataAgendamentoCarga, Long idEmpresa) {

          Optional<Long> idCargaBeneficio = this.buscaIdCargaBeneficioPor(arquivoCargas.getId(), idEmpresa, arquivoCargas.getIdGrupoEmpresa());

          return idCargaBeneficio.isPresent() ? idCargaBeneficio.get() : this.geraCargaBeneficio(arquivoCargas, dataAgendamentoCarga, idEmpresa);

     }
     
     public void atualizaStatusCargasBeneficioEmLote(List<Long> numerosPedidos, final StatusPagamentoEnum statusPagamentoEnum) {

          final StatusDescricao statusDescricao = this.statusCargaBeneficioParaPosPago(statusPagamentoEnum);

          cargaBeneficioRepository.updateStatusByIdArquivoCargasIn(statusDescricao.getStatus(), numerosPedidos);
     }
     
     public Map<Long, List<CargaBeneficioMinimoCustom>> gerenciaRegistrosComStatusCargaPago() {
          
          final List<StatusDescricao> dominioCargasBeneficios = this.statusCached.busca(CARGAS_BENEFICIOS.getNome());
          final StatusDescricao statusCargasPedidoConfirmado = mapStatusDescricao(dominioCargasBeneficios, PEDIDO_CONFIRMADO.getStatus());
          
          LocalDate data = LocalDate.now().minusDays(1);
          
          List<CargaBeneficioMinimoCustom> cargasBeneficiosCustom = cargaBeneficioStatusImpl.buscaCargasPagas(statusCargasPedidoConfirmado.getStatus(), data);
          
          if(CollectionUtils.isNotEmpty(cargasBeneficiosCustom)) {

               RetornaValorAtributo<CargaBeneficioMinimoCustom,Long> retornaValorAtributo = cargaBeneficio -> cargaBeneficio.getIdPedido();
               
               return  cargasBeneficiosCustom.stream().collect(Collectors.groupingBy(cargaBeneficio -> retornaValorAtributo.retornaAtributoDeObjeto(cargaBeneficio), toList()));
          }
          
          return new HashMap<>();
     }
     
     public Map<Long, List<CargaBeneficioMinimoCustom>> gerenciaRegistrosComStatusCargaCancelado() {
          
          final List<StatusDescricao> dominioCargasBeneficios = this.statusCached.busca(CARGAS_BENEFICIOS.getNome());
          final StatusDescricao statusCargasCancelado = mapStatusDescricao(dominioCargasBeneficios, PEDIDO_CANCELADO.getStatus());
          
          LocalDateTime dataHoraAtual = LocalDateTime.now();
          LocalDateTime dataHoraMenosDuasHoras = LocalDateTime.now().minusHours(2);
          
          List<CargaBeneficioMinimoCustom> cargasBeneficiosCustom = cargaBeneficioStatusImpl.buscaCargasCanceladas(statusCargasCancelado.getStatus(), dataHoraAtual, dataHoraMenosDuasHoras);
          
          if(CollectionUtils.isNotEmpty(cargasBeneficiosCustom)) {

               RetornaValorAtributo<CargaBeneficioMinimoCustom,Long> retornaValorAtributo = cargaBeneficio -> cargaBeneficio.getIdPedido();
               
               return  cargasBeneficiosCustom.stream().collect(Collectors.groupingBy(cargaBeneficio -> retornaValorAtributo.retornaAtributoDeObjeto(cargaBeneficio), toList()));
          }
          
          return new HashMap<>();
     }
     
     public Map<Long, List<CargaBeneficioMinimoCustom>> gerenciaRegistrosComStatusCargaExpirado() {
          
          final List<StatusDescricao> dominioCargasBeneficios = this.statusCached.busca(CARGAS_BENEFICIOS.getNome());
          final StatusDescricao statusCargasExpirado = mapStatusDescricao(dominioCargasBeneficios, EXPIRADO.getStatus());
          
          LocalDateTime dataHoraAtual = LocalDateTime.now();
          LocalDateTime dataHoraMenosUmDia = LocalDateTime.now().minusDays(1);
          
          List<CargaBeneficioMinimoCustom> cargasBeneficiosCustom = cargaBeneficioStatusImpl.buscaCargasExpiradas(statusCargasExpirado.getStatus(), dataHoraAtual, dataHoraMenosUmDia);
          
          if(CollectionUtils.isNotEmpty(cargasBeneficiosCustom)) {

               RetornaValorAtributo<CargaBeneficioMinimoCustom,Long> retornaValorAtributo = cargaBeneficio -> cargaBeneficio.getIdPedido();
               
               return  cargasBeneficiosCustom.stream().collect(Collectors.groupingBy(cargaBeneficio -> retornaValorAtributo.retornaAtributoDeObjeto(cargaBeneficio), toList()));
          }
          
          return new HashMap<>();
     }
     
}
