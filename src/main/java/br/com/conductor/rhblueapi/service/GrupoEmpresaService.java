
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.NivelPermissaoEnum.GRUPO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.TIPO_ENTREGA_CARTAO;
import static br.com.conductor.rhblueapi.exception.ExceptionCdt.checkThrow;
import static br.com.conductor.rhblueapi.util.AppConstantes.PARAMETRO_RH;
import static br.com.conductor.rhblueapi.util.DataUtils.converteStringParaLocalDate;
import static br.com.conductor.rhblueapi.util.DataUtils.localDateParaStringyyyyMMdd;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.cfg.beanvalidation.IntegrationException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.Empresa;
import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;
import br.com.conductor.rhblueapi.domain.SubgrupoEmpresa;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.EmpresaContatoPersist;
import br.com.conductor.rhblueapi.domain.persist.EmpresaPersist;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.persist.PermissoesOperacoesRhPersist;
import br.com.conductor.rhblueapi.domain.persist.PermissoesRhPersist;
import br.com.conductor.rhblueapi.domain.persist.SubgrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.request.OperacaoAcessoRequest;
import br.com.conductor.rhblueapi.domain.request.PessoaPierRest;
import br.com.conductor.rhblueapi.domain.request.SubgrupoEmpresaRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioPierRest;
import br.com.conductor.rhblueapi.domain.request.UsuarioRequestBlue;
import br.com.conductor.rhblueapi.domain.response.CadastroGrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.EmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.GrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.OperacaoAcessoResponse;
import br.com.conductor.rhblueapi.domain.response.PageOperacaoAcessoResponse;
import br.com.conductor.rhblueapi.domain.response.PageSubgrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.PessoaResponse;
import br.com.conductor.rhblueapi.domain.response.SubgrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.UsuarioPermissaoResponse;
import br.com.conductor.rhblueapi.domain.update.GrupoEmpresaUpdate;
import br.com.conductor.rhblueapi.domain.update.SubgrupoEmpresaUpdate;
import br.com.conductor.rhblueapi.enums.ModeloDePedido;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.enums.Plataforma;
import br.com.conductor.rhblueapi.enums.SegmentoEmpresaEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ModeloDeCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoEntregaCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPagamento;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPedido;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoProduto;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TransferenciaProdutos;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.repository.EmpresaRepository;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.ParametroGrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.SubgrupoRepository;
import br.com.conductor.rhblueapi.repository.utils.UsuarioPermissaoMap;
import br.com.conductor.rhblueapi.util.ConstantesParametros;
import br.com.conductor.rhblueapi.util.GenericConvert;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class GrupoEmpresaService {

     @Value("${app.plataforma.rh.cod}")
     private Integer rh;

     @Autowired
     private GrupoEmpresaRepository grupoEmpresaRepository;

     @Autowired
     private SubgrupoRepository subGrupoRepository;

     @Autowired
     private EmpresaService empresaService;

     @Autowired
     private ParametroGrupoEmpresaRepository parametroGrupoEmpresaRepository;

     @Autowired
     private UsuarioPierService usuarioService;

     @Autowired
     private UsuarioPierRest usuarioRest;

     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;

     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;

     @Autowired
     private PermissoesOperacoesRhService permissoesOperacoesRhService;

     @Autowired
     private OperacaoAcessoService operacaoAcessoService;

     @Autowired
     private PessoaPierRest pessoaPierRest;
     
     @Autowired
     private ParametrosService parametrosService;

     @Autowired
     private EmpresaRepository empresaRepository;

     @Autowired
     private CadastroGrupoEmpresaService cadastroGrupoEmpresaService;
     
     @Autowired
     private GrupoEmpresaProdutoService grupoEmpresaProdutoService;

     private static final Long USUARIO_REGISTRO_PRIMEIRO_ACESSO = 0l;

     public GrupoEmpresaResponse cadastrar(GrupoEmpresaPersist grupoEmpresaPersist) throws IOException {
          
          CadastroGrupoEmpresaResponse cadastroGrupoEmpresaResponse = salvarEntidades(grupoEmpresaPersist);

          try{
               cadastroGrupoEmpresaResponse.setEmpresa(salvarEmpresa(cadastroGrupoEmpresaResponse.getSubGrupo().getId(), grupoEmpresaPersist.getEmpresaMatriz()));
               atualizarGrupo(cadastroGrupoEmpresaResponse.getGrupoEmpresa(), cadastroGrupoEmpresaResponse.getEmpresa());
               atualizarSubGrupo(cadastroGrupoEmpresaResponse.getSubGrupo(), cadastroGrupoEmpresaResponse.getEmpresa());

               List<UsuarioResponse> usuarios = new ArrayList<>();

               for (EmpresaContatoPersist contato : grupoEmpresaPersist.getEmpresaMatriz().getContatos()){
                    usuarios.add(cadastrarUsuario(contato, grupoEmpresaPersist.getEmpresaMatriz().getCnpj(), cadastroGrupoEmpresaResponse.getGrupoEmpresa()));
               }

               List<UsuarioPermissaoResponse> usuarioPermissaoResponseList = GenericConvert.convertWithMapping(usuarios, new TypeToken<List<UsuarioPermissaoResponse>>() {}.getType(), new UsuarioPermissaoMap().response);

               return GrupoEmpresaResponse.builder()
                         .idPessoa(cadastroGrupoEmpresaResponse.getEmpresa().getIdPessoa())
                         .id(cadastroGrupoEmpresaResponse.getGrupoEmpresa().getId())
                         .idSubGrupo(cadastroGrupoEmpresaResponse.getSubGrupo().getId())
                         .idEmpresa(cadastroGrupoEmpresaResponse.getEmpresa().getIdEmpresa())
                         .usuarios(usuarioPermissaoResponseList)
                         .build();

          } catch (Exception innerException) {

               deleteEmpresaPrincipal(cadastroGrupoEmpresaResponse.getEmpresa());
               deleteSubgrupo(cadastroGrupoEmpresaResponse.getSubGrupo());
               deleteGrupoEmpresa(cadastroGrupoEmpresaResponse.getGrupoEmpresa());

               throw innerException;
          }
     }

     public CadastroGrupoEmpresaResponse salvarEntidades(GrupoEmpresaPersist persist) {
          
          persist.getParametros().setPrazoPagamento(cadastroGrupoEmpresaService.obtemPrazoDePagamento(persist.getParametros()));

          BadRequestCdt.checkThrow(Objects.equals(persist.getParametros().getTipoFaturamento(), TipoFaturamentoEnum.CENTRALIZADO) && Objects.equals(persist.getParametros().getTipoPedido(),
                    TipoPedido.DESCENTRALIZADO), ExceptionsMessagesCdtEnum.FORMA_DE_FATURAMENTO_INVALIDA);

          List<PessoaResponse> pessoas = this.pessoaPierRest.consultaPessoaFisica(persist.getEmpresaMatriz().getCnpj());
          BadRequestCdt.checkThrow(!pessoas.isEmpty(), ExceptionsMessagesCdtEnum.EMPRESA_JA_CADASTRADA);

          persist.getEmpresaMatriz().setNivelPermissaoContato(NivelPermissaoEnum.GRUPO);

          return cadastroGrupoEmpresaService.salvar(persist);
     }

     private EmpresaResponse salvarEmpresa(Long idSubGrupo, EmpresaPersist persist) throws IOException {
          return this.empresaService.cadastraEmpresa(idSubGrupo, persist);
     }

     private void deleteEmpresaPrincipal(EmpresaResponse empresaPrincipal) {
          if(Objeto.notBlank(empresaPrincipal) && Objeto.notBlank(empresaPrincipal.getIdEmpresa())) {
               empresaService.deleteEmpresa(empresaPrincipal.getIdEmpresa());
          }
     }

     @Transactional
     public void deleteGrupoEmpresa(GrupoEmpresa grupoEmpresa) {
          if(Objeto.notBlank(grupoEmpresa)) {
               parametrosService.deletarParametroPorIdGrupo(grupoEmpresa.getId());
               grupoEmpresaRepository.delete(grupoEmpresa);
          }
     }

     @Transactional
     public void deleteSubgrupo(SubgrupoEmpresa subGrupo) {
          if(Objeto.notBlank(subGrupo)) {
               parametrosService.deletarParametroPorIdGrupo(subGrupo.getId());
               subGrupoRepository.delete(subGrupo);
          }
     }

     private List<UsuarioResponse> verificaCadastroUsuario(EmpresaContatoPersist contato) {
          return usuarioRest.consultaUsuario(contato.getCpf(), rh);
     }

     private void atualizarGrupo(GrupoEmpresa grupoEmpresa, EmpresaResponse empresaPrincipal) {

          if(Objeto.notBlank(grupoEmpresa) && Objeto.notBlank(empresaPrincipal)) {
               grupoEmpresa.setIdEmpresaPrincipal(empresaPrincipal.getIdEmpresa());
               this.grupoEmpresaRepository.save(grupoEmpresa);
          }
     }

     private void atualizarSubGrupo(SubgrupoEmpresa subGrupo, EmpresaResponse empresaPrincipal) {

          if(Objeto.notBlank(subGrupo) && Objeto.notBlank(empresaPrincipal)) {
               subGrupo.setIdEmpresaPrincipal(empresaPrincipal.getIdEmpresa());
               this.subGrupoRepository.save(subGrupo);
          }
     }

     private UsuarioResponse cadastrarUsuario(EmpresaContatoPersist contato, String cnpj, GrupoEmpresa grupoEmpresa) throws IOException {

          List<UsuarioResponse> usuarioList = verificaCadastroUsuario(contato);

          UsuarioResponse usuarioResponse = new UsuarioResponse();

          if (usuarioList.isEmpty()) {

               usuarioResponse = criaUsuario(contato, cnpj);
                              
               PermissoesRhPersist permissaoPersist = PermissoesRhPersist.builder()
                         .idGrupoEmpresa(grupoEmpresa.getId())
                         .idUsuario(usuarioResponse.getId())
                         .idUsuarioRegistro(USUARIO_REGISTRO_PRIMEIRO_ACESSO)
                         .build();
               
               Long idPermissao = permissoesUsuariosRhService.salvarPermissaoViaSalesforce(permissaoPersist);
               
               usuarioNivelPermissaoRhService.salvarNivelAcessoViaSalesforce(grupoEmpresa.getId(), null, null, idPermissao, USUARIO_REGISTRO_PRIMEIRO_ACESSO, GRUPO);

               criaPermissoesAcesso(listaOperacoes(), idPermissao);
               
               usuarioResponse.setIdPermissao(idPermissao);
               return usuarioResponse;

          } else {
               
               PermissoesRhPersist permissaoPersist = PermissoesRhPersist.builder()
                         .idGrupoEmpresa(grupoEmpresa.getId())
                         .idUsuario(usuarioList.get(0).getId())
                         .idUsuarioRegistro(USUARIO_REGISTRO_PRIMEIRO_ACESSO)
                         .build();

               Long idPermissao = permissoesUsuariosRhService.salvarPermissaoViaSalesforce(permissaoPersist);
               
               usuarioNivelPermissaoRhService.salvarNivelAcessoViaSalesforce(grupoEmpresa.getId(), null, null, idPermissao, USUARIO_REGISTRO_PRIMEIRO_ACESSO, GRUPO);

               criaPermissoesAcesso(listaOperacoes(), idPermissao);

               usuarioList.get(0).setIdPermissao(idPermissao);
               return usuarioList.get(0);
          }
     }

     private void criaPermissoesAcesso(List<Long> idOperacoesAcessos, Long idPermissao) {

          permissoesOperacoesRhService.vincularPermissaoOperacaoChamadaUsuarioPrincipal(PermissoesOperacoesRhPersist.builder()
                    .IdOperacao(idOperacoesAcessos)
                    .idUsuarioRegistro(USUARIO_REGISTRO_PRIMEIRO_ACESSO)
                    .build(), idPermissao);
     }

     private List<Long> listaOperacoes() {

          OperacaoAcessoRequest request = new OperacaoAcessoRequest();
          request.setPage(0);
          request.setSize(50);
          request.setIdPlataforma(16l);

          PageOperacaoAcessoResponse pageOperacoes = operacaoAcessoService.listar(request);

          List<OperacaoAcessoResponse> listOperacoes = pageOperacoes.getContent();

          List<Long> idOperacoesAcessos = listOperacoes.stream().map(OperacaoAcessoResponse::getId).collect(Collectors.toList());

          return idOperacoesAcessos;
     }

     private UsuarioResponse criaUsuario(EmpresaContatoPersist contato, String cnpj) throws JsonParseException, JsonMappingException, IOException {

          return usuarioService.criaUsuario(UsuarioRequestBlue.builder().nome(contato.getNome())
               .cpf(contato.getCpf()).cnpj(cnpj).email(contato.getEmail()).plataforma(Plataforma.RH).build());
     }

     private void verificaTipoProdutoPersistVA(List<TipoProduto> tipoProduto, ParametroGrupoEmpresa parametro) {

          tipoProduto.forEach(action -> {
               if (StringUtils.isNotEmpty(action.name()) && StringUtils.equals(action.name(), "VA")) {
                    parametro.setValor(String.valueOf(1));
               }
          });
     }

     private void verificaTipoProdutoPersistVR(List<TipoProduto> tipoProduto, ParametroGrupoEmpresa parametro) {

          tipoProduto.forEach(action -> {
               if (StringUtils.isNotEmpty(action.name()) && StringUtils.equals(action.name(), "VR")) {
                    parametro.setValor(String.valueOf(1));
               }
          });
     }

     public SubgrupoEmpresaResponse cadastraSubGrupo(SubgrupoEmpresaPersist persist, Long idGrupoEmpresa) throws IOException {

          Optional<GrupoEmpresa> grupoEmpresa = grupoEmpresaRepository.findById(idGrupoEmpresa);

          BadRequestCdt.checkThrow(Objects.isNull(grupoEmpresa) || !grupoEmpresa.isPresent(), ExceptionsMessagesCdtEnum.SUBGRUPO_EMPRESA_NAO_CADASTRADO);

          persist.getEmpresaMatriz().setFlagMatriz(1);

          SubgrupoEmpresa subGrupo = new SubgrupoEmpresa();
          subGrupo.setNomeSubGrupo(persist.getNomeSubGrupo());
          subGrupo.setDataCadastro(LocalDateTime.now());
          subGrupo.setIdGrupoEmpresaPai(grupoEmpresa.get().getId());

          subGrupo = salvarSubgrupo(subGrupo);
          
          cadastroGrupoEmpresaService.cadastrarParametrosGrupo(subGrupo.getId(), persist.getCondicoes(), persist.getParametros());

          persist.getEmpresaMatriz().setNivelPermissaoContato(NivelPermissaoEnum.SUBGRUPO);

          CadastroGrupoEmpresaResponse cadastro = CadastroGrupoEmpresaResponse.builder()
                    .subGrupo(subGrupo)
                    .build();

          try {
               cadastro.setEmpresa(salvarEmpresa(subGrupo.getId(), persist.getEmpresaMatriz()));
          } catch (Exception e) {
               deleteSubgrupo(subGrupo);
               throw e;
          }

          BadRequestCdt.checkThrow(Objects.isNull(cadastro.getEmpresa()), ExceptionsMessagesCdtEnum.ERRO_AO_CADASTRAR_EMPRESA);

          subGrupo.setIdEmpresaPrincipal(cadastro.getEmpresa().getIdEmpresa());
          salvarSubgrupo(subGrupo);

          List<UsuarioPermissaoResponse> usuarioPermissaoResponseList = GenericConvert.convertWithMapping(cadastro.getEmpresa().getUsuarios(), new TypeToken<List<UsuarioPermissaoResponse>>() {}.getType(), new UsuarioPermissaoMap().response);

          return SubgrupoEmpresaResponse.builder()
                          .idEmpresaPrincipal(subGrupo.getIdEmpresaPrincipal())
                          .idGrupoEmpresaPai(subGrupo.getIdGrupoEmpresaPai())
                          .usuarios(usuarioPermissaoResponseList)
                          .idPessoa(cadastro.getEmpresa().getIdPessoa())
                          .nomeSubGrupo(subGrupo.getNomeSubGrupo())
                          .codigoExterno(subGrupo.getCodigoExterno())
                          .dataCadastro(subGrupo.getDataCadastro())
                          .id(subGrupo.getId()).build();
     }


     private SubgrupoEmpresa salvarSubgrupo(SubgrupoEmpresa persist) {

          try {
               return this.subGrupoRepository.save(persist);

          } catch (Exception e) {
               throw new IntegrationException("Falha ao criar subgrupo" + e.getMessage());
          }
     }
     
     public SubgrupoEmpresaResponse atualizarSubgrupoEmpresa(final Long idGrupo, final Long idSubgrupo, final SubgrupoEmpresaUpdate request) {
          
          grupoEmpresaRepository.findById(idGrupo).orElseThrow(() -> new NotFoundCdt(ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_ENCONTRADO.getMessage()));
          
          final Optional<SubgrupoEmpresa> subgrupoEmpresaAtual = subGrupoRepository.findById(idSubgrupo);
          NotFoundCdt.checkThrow(!subgrupoEmpresaAtual.isPresent(), ExceptionsMessagesCdtEnum.SUBRGRUPO_NAO_EXISTE);
          
          final SubgrupoEmpresa subgrupoAtual = subgrupoEmpresaAtual.get();
          
          PreconditionCustom.checkThrow(!Objects.equals(idGrupo, subgrupoAtual.getIdGrupoEmpresaPai()), ExceptionsMessagesCdtEnum.ERRO_SUBGRUPO_NAO_E_GRUPO);
          
          PreconditionCustom.checkThrow(Objects.isNull(request), ExceptionsMessagesCdtEnum.DADOS_ATUALIZACAO_NAO_INFORMADOS);
          
          if (Objects.nonNull(request.getIdEmpresaPrincipal())) {
               
               final Optional<Empresa> empresaPrincipalAtualizar = empresaRepository.findById(request.getIdEmpresaPrincipal());
               NotFoundCdt.checkThrow(!empresaPrincipalAtualizar.isPresent(), ExceptionsMessagesCdtEnum.EMPRESA_NAO_ENCONTRADA);
               
               PreconditionCustom.checkThrow(!Objects.equals(subgrupoAtual.getId(), empresaPrincipalAtualizar.get().getIdGrupoEmpresa()), ExceptionsMessagesCdtEnum.ERRO_EMPRESA_NAO_E_DE_SUBGRUPO);
          }
          
          final ModelMapper modelMapper = new ModelMapper();
          modelMapper.getConfiguration().setSkipNullEnabled(true).setMatchingStrategy(MatchingStrategies.STRICT);
          modelMapper.map(request, subgrupoAtual);
          
          SubgrupoEmpresa subGrupoSalvo = subGrupoRepository.save(subgrupoAtual);
          
          return SubgrupoEmpresaResponse.builder()
                    .nomeSubGrupo(subGrupoSalvo.getNomeSubGrupo())
                    .idEmpresaPrincipal(subGrupoSalvo.getIdEmpresaPrincipal())
                    .idGrupoEmpresaPai(subGrupoSalvo.getIdGrupoEmpresaPai())
                    .dataCadastro(subGrupoSalvo.getDataCadastro())
                    .id(subGrupoSalvo.getId()).build();
     }


     public PageSubgrupoEmpresaResponse listaSubgrupo(Long idGrupoEmpresa, SubgrupoEmpresaRequest request, Integer page, Integer size) {

          grupoEmpresaRepository.findById(idGrupoEmpresa).orElseThrow(() -> new NotFoundCdt(ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_LOCALIZADO.getMessage()));

          Pageable pageable = PageRequest.of(page, size);

          Page<SubgrupoEmpresa> subGrupoResponse;

          if (Objects.nonNull(request) && Objects.nonNull(request.getIdUsuario())) {

               subGrupoResponse = subGrupoRepository.findByIdUsuarioAndIdGrupoEmpresaAndNomeAndStatus(request.getIdUsuario(), idGrupoEmpresa, request.getNome(), 1, pageable);

          } else {

               subGrupoResponse = subGrupoRepository.findByIdGrupoEmpresaPai(idGrupoEmpresa, pageable);

          }

          NoContentCdt.checkThrow(Objects.isNull(subGrupoResponse) || !subGrupoResponse.hasContent(), ExceptionsMessagesCdtEnum.SUBGRUPOS_NAO_LOCALIZADOS);

          PageSubgrupoEmpresaResponse pageSubgrupoEmpresaResponse = new PageSubgrupoEmpresaResponse(GenericConvert.convertModelMapperToPageResponse(subGrupoResponse, new TypeToken<List<SubgrupoEmpresaResponse>>() {
          }.getType()));

          return pageSubgrupoEmpresaResponse;
     }

     @Transactional
     public GrupoEmpresaResponse atualizarGrupoEmpresa(final Long idGrupoEmpresa,  final GrupoEmpresaUpdate update) {

          final Optional<GrupoEmpresa> grupo = this.grupoEmpresaRepository.findById(idGrupoEmpresa);
          checkThrow(!grupo.isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_LOCALIZADO);
        
          checkThrow(Objeto.notBlank(grupo.get().getIdGrupoEmpresaPai()), ExceptionsMessagesCdtEnum.SUBGRUPO_NAO_PODE_SER_ATUALIZADO);

          if(Objeto.notBlank(update.getIdEmpresaPrincipal())) {
               
               Optional<Empresa> empresa = this.empresaRepository.findById(update.getIdEmpresaPrincipal());
               NotFoundCdt.checkThrow(!empresa.isPresent(), ExceptionsMessagesCdtEnum.EMPRESA_NAO_ENCONTRADA);

               Optional<GrupoEmpresa> grupoEmpresa = this.grupoEmpresaRepository.findById(empresa.get().getIdGrupoEmpresa());

               checkThrow(!grupoEmpresa.isPresent(), ExceptionsMessagesCdtEnum.SUBRGRUPO_NAO_EXISTE);

               checkThrow(!Objects.equals(grupoEmpresa.get().getIdGrupoEmpresaPai() ,idGrupoEmpresa), ExceptionsMessagesCdtEnum.EMPRESA_NAO_FAZ_PARTE_GRUPO);
           }

          if (Objects.nonNull(update.getIdEmpresaPrincipal())) grupo.get().setIdEmpresaPrincipal(update.getIdEmpresaPrincipal());
          if (Objects.nonNull(update.getNomeGrupo())) grupo.get().setNomeGrupo(update.getNomeGrupo());

          final GrupoEmpresa grupoEmpresa = this.grupoEmpresaRepository.save(grupo.get());

          final GrupoEmpresaResponse grupoEmpresaParam = this.atualizaParametrosGrupo(idGrupoEmpresa, update);

          GrupoEmpresaResponse convertModelGrupoEmpresa = GenericConvert.convertModelMapper(grupoEmpresaParam, GrupoEmpresaResponse.class);
          convertModelGrupoEmpresa.setNomeGrupo(grupoEmpresa.getNomeGrupo());
          convertModelGrupoEmpresa.setIdEmpresaPrincipal(grupoEmpresa.getIdEmpresaPrincipal());

          return convertModelGrupoEmpresa;
       }

     private GrupoEmpresaResponse atualizaParametrosGrupo(final Long idGrupoEmpresa, final GrupoEmpresaUpdate update) {

          final GrupoEmpresaResponse grupo = new GrupoEmpresaResponse();

          List<TipoProduto> produtos = new ArrayList<>();

          if (Objeto.notBlank(update.getTipoPedido())) {

               String tipo = parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_TIPO_PEDIDO, TipoPedido.valueOf(update.getTipoPedido().toString()).getTipo().toString()).getValor();

               grupo.setTipoPedido(TipoPedido.equalsTipo(tipo));
          }
          
          if(Objeto.notBlank(update.getModeloCartao())) grupo.setModeloCartao(ModeloDeCartao.valueOf(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_MODELO_CARTAO, String.valueOf(update.getModeloCartao())).getValor()));

          if (Objeto.notBlank(update.getTipoContrato()))
               grupo.setTipoContrato(TipoContratoEnum.valueOf(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_TIPO_CONTRATO, String.valueOf(update.getTipoContrato())).getValor()));

          if (Objeto.notBlank(update.getPrazoPagamento()))
               grupo.setPrazoPagamento(Integer.parseInt(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_PRAZO_PAGAMENTO, update.getPrazoPagamento().toString()).getValor()));

          if (Objeto.notBlank(update.getSegmentoEmpresa()))
               grupo.setSegmentoEmpresa(SegmentoEmpresaEnum.valueOf(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_SEGMENTO_EMPRESA, update.getSegmentoEmpresa().toString()).getValor()));

          if (Objeto.notBlank(update.getTransferencia()))
               grupo.setTransferencia(TransferenciaProdutos.valueOf(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_TRANSF_PRODUTOS, update.getTransferencia().name()).getValor()));

          if (Objeto.notBlank(update.getEmailNotaFiscal()))
               grupo.setEmailNotaFiscal(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_EMAIL_NF, update.getEmailNotaFiscal().toString()).getValor());

          if (Objeto.notBlank(update.getTipoFaturamento())) {

               String tipo = parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_TIPO_FATURAMENTO, TipoFaturamentoEnum.valueOf(update.getTipoFaturamento().toString()).getValor().toString()).getValor();

               grupo.setTipoFaturamento(TipoFaturamentoEnum.equalsTipo(tipo));
          }

          if (Objeto.notBlank(update.getTipoEntrega()))
               grupo.setTipoEntrega(TipoEntregaCartao.findItemByValue(parametrosService.cadastraParametro(idGrupoEmpresa, ParametrosEnum.TIPO_ENTREGA_CARTAO.getValor(), update.getTipoEntrega().getValor()).getValor()));

          if (Objeto.notBlank(update.getLimiteCreditoDisponivel()))
               grupo.setLimiteCreditoDisponivel(new BigDecimal(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_LIMITE_CREDITO, update.getLimiteCreditoDisponivel().toString()).getValor()));

          if (Objeto.notBlank(update.getModeloPedido()))
               grupo.setModeloPedido(ModeloDePedido.valueOf(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_MODELO_PEDIO, update.getModeloPedido().toString()).getValor()));

          if (Objeto.notBlank(update.getTipoPagamento()))
               grupo.setTipoPagamento(TipoPagamento.buscaPorValor(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_TIPO_PAGAMENTO, update.getTipoPagamento().getValor()).getValor()));

          if (Objeto.notBlank(update.getTipoProduto())) {

               String tipoProdutoVA = parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_TIPO_PRODUTO_VA, update.getTipoProduto().toString()).getValor();

               String[] tiposProdutos = this.regexTipoProduto(tipoProdutoVA).split(",");

               for (String tipoProduto : tiposProdutos) {

                    if (TipoProduto.VA.name().equals(tipoProduto.trim())) {
                         produtos.add(TipoProduto.equalsTipo(tipoProduto));
                         break;
                    }
               }
          }

          if (Objeto.notBlank(update.getTipoProduto())) {

               String tipoProdutoVR = parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.RH_TIPO_PRODUTO_VR, update.getTipoProduto().toString()).getValor();

               String[] tiposProdutos = this.regexTipoProduto(tipoProdutoVR).split(",");

               for (String tipoProduto : tiposProdutos) {

                    if (TipoProduto.VR.name().equals(tipoProduto.trim())) {
                         produtos.add(TipoProduto.equalsTipo(tipoProduto.trim()));
                         break;
                    }
               }
          }

          grupo.setTipoProduto(produtos);

          Optional<ParametroGrupoEmpresa> usaVAParamLocalizado = this.parametroGrupoEmpresaRepository.findByIdGrupoEmpresaAndParametro_IdParametro(idGrupoEmpresa, parametrosService.consultaParametroRh(ParametrosEnum.USA_PRODUTO_VA.getValor()));
          PreconditionCustom.checkThrow(!usaVAParamLocalizado.isPresent(), ExceptionsMessagesCdtEnum.PARAMETRO_GRUPO_NAO_ENCONTRADO);

          final ParametroGrupoEmpresa usaVAPersist = usaVAParamLocalizado.get();

          if (Objeto.notBlank(update.getTipoProduto())) {
               verificaTipoProdutoPersistVA(update.getTipoProduto(), usaVAPersist);
          }

          if (StringUtils.isNotEmpty(usaVAPersist.getValor())) {
               this.parametroGrupoEmpresaRepository.save(usaVAPersist);
          }

          Optional<ParametroGrupoEmpresa> usaVRParamLocalizado = this.parametroGrupoEmpresaRepository.findByIdGrupoEmpresaAndParametro_IdParametro(idGrupoEmpresa, parametrosService.consultaParametroRh(ParametrosEnum.USA_PRODUTO_VR.getValor()));

          PreconditionCustom.checkThrow(!usaVRParamLocalizado.isPresent(), ExceptionsMessagesCdtEnum.PARAMETRO_GRUPO_NAO_ENCONTRADO);
          final ParametroGrupoEmpresa usaVRPersist = usaVRParamLocalizado.get();

          if (Objeto.notBlank(update.getTipoProduto())) {
               this.verificaTipoProdutoPersistVR(update.getTipoProduto(), usaVRPersist);
          }

          if (StringUtils.isNotEmpty(usaVRPersist.getValor())) {
               this.parametroGrupoEmpresaRepository.save(usaVRPersist);
          }
          
          if(update.getTipoContrato().equals(TipoContratoEnum.POS))
               grupo.setDataVigenciaLimite(converteStringParaLocalDate(parametrosService.cadastraParametro(idGrupoEmpresa, ConstantesParametros.VIGENCIA_LIMITE, localDateParaStringyyyyMMdd(update.getDataVigenciaLimite())).getValor()));
          
          this.cadastrarParametroTipoProduto(idGrupoEmpresa, update.getTipoProduto());
          
          return grupo;
     }

     private void cadastrarParametroTipoProduto(final Long idGrupoEmpresa, final List<TipoProduto> produtos) {
          
          grupoEmpresaProdutoService.atualizarGrupoEmpresaProdutos(idGrupoEmpresa, produtos);
     }
     
     private String regexTipoProduto(String object) {
          return object.replace("[","").replace("]","");
     }


	public Optional<GrupoEmpresa> autenticarGrupoEmpresa(Long idGrupoEmpresa) {

          Optional<GrupoEmpresa> grupoEmpresa = grupoEmpresaRepository.findByIdAndIdGrupoEmpresaPai(idGrupoEmpresa, null);
          NotFoundCdt.checkThrow(!grupoEmpresa.isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_LOCALIZADO);
          return grupoEmpresa;
     }
	
	public Optional<Long> isIdGrupoEmpresaSalvo(Long idGrupoEmpresa) {
	     
	     Optional<GrupoEmpresa> optGrupoEmpresa = grupoEmpresaRepository.findById(idGrupoEmpresa);
	     
          return Optional.ofNullable(optGrupoEmpresa.isPresent() ? optGrupoEmpresa.get().getId() : null);
	     
	}

	public Optional<GrupoEmpresa> buscaGrupoEmpresaPor(Long idGrupoEmpresa) {

          Optional<GrupoEmpresa> grupoEmpresa = grupoEmpresaRepository.findById(idGrupoEmpresa);
          return grupoEmpresa;
     }
	
	public TipoEntregaCartao tipoEntregaCartao(Long idGrupoEmpresa) {

	     String parametro = parametrosService.buscaParametrosGrupoEmpresaPor(idGrupoEmpresa, PARAMETRO_RH, TIPO_ENTREGA_CARTAO).getValor();

	     TipoEntregaCartao tipoEntrega = TipoEntregaCartao.findItemByValue(parametro);
	     
	     return tipoEntrega;
	}
	

}

