
package br.com.conductor.rhblueapi.service.usecase;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.repository.StatusDescricaoCustomRepository;

@Component
public class StatusCached {

     @Autowired
     private StatusDescricaoCustomRepository statusDescricaoCustomRepository;

     public List<StatusDescricao> busca(final String tipoStatus) {

          final Map<String, List<StatusDescricao>> descricao = this.statusDescricaoCustomRepository.findAll();
          final List<StatusDescricao> statuses = descricao.get(StringUtils.upperCase(tipoStatus));
          return statuses;
     }

}
