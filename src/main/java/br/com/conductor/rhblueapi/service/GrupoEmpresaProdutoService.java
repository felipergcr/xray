
package br.com.conductor.rhblueapi.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.GrupoEmpresaProduto;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoProduto;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaProdutoRepository;

@Service
public class GrupoEmpresaProdutoService {

     @Autowired
     private GrupoEmpresaProdutoRepository grupoEmpresaProdutoRepository;

     public List<GrupoEmpresaProduto> salvarGrupoEmpresaProdutos(final Long idGrupoEmpresa, final List<TipoProduto> produtos) {
         
          List<GrupoEmpresaProduto> grupoEmpresaProdutos = new ArrayList<GrupoEmpresaProduto>();
          for (TipoProduto prod : produtos) {
               grupoEmpresaProdutos.add(
                         GrupoEmpresaProduto.builder()
                         .idGrupoEmpresa(idGrupoEmpresa)
                         .idProduto(prod.getIdentificador())
                         .dataCadastro(LocalDateTime.now())
                         .build()
                         );
          }
          return grupoEmpresaProdutoRepository.saveAll(grupoEmpresaProdutos);
     }
     
     public void atualizarGrupoEmpresaProdutos(final Long idGrupoEmpresa, final List<TipoProduto> produtos) {

          Optional<List<GrupoEmpresaProduto>> grupoEmpresaProdutos = grupoEmpresaProdutoRepository.findByIdGrupoEmpresaAndDataDelecaoIsNull(idGrupoEmpresa);

          List<GrupoEmpresaProduto> grupoEmpresaProdutosAux = new ArrayList<GrupoEmpresaProduto>();

          grupoEmpresaProdutosAux.addAll(deletarGrupoEmpresaProdutos(grupoEmpresaProdutos.isPresent() ? grupoEmpresaProdutos.get() : new ArrayList<>(), produtos));
          grupoEmpresaProdutosAux.addAll(inserirGrupoEmpresaProdutos(produtos, grupoEmpresaProdutos.isPresent() ? grupoEmpresaProdutos.get() : new ArrayList<>(), idGrupoEmpresa));

          if (Boolean.FALSE.equals(grupoEmpresaProdutosAux.isEmpty()))
               grupoEmpresaProdutoRepository.saveAll(grupoEmpresaProdutosAux);
     }

     private List<GrupoEmpresaProduto> deletarGrupoEmpresaProdutos(final List<GrupoEmpresaProduto> grupoEmpresaProdutos, final List<TipoProduto> produtos) {

          List<GrupoEmpresaProduto> grupoEmpresaProdutosAux = new ArrayList<GrupoEmpresaProduto>();

          for (GrupoEmpresaProduto gep : grupoEmpresaProdutos) {
               Optional<TipoProduto> prod = obterProduto(produtos, gep.getIdProduto());
               if (Boolean.FALSE.equals(prod.isPresent())) {
                    gep.setDataDelecao(LocalDateTime.now());
                    grupoEmpresaProdutosAux.add(gep);
               }
          }

          return grupoEmpresaProdutosAux;
     }

     private Optional<TipoProduto> obterProduto(final List<TipoProduto> produtos, final Integer idProduto) {

          return Optional.ofNullable(produtos.stream().filter(p -> p.getIdentificador().equals(idProduto)).findFirst().orElse(null));
     }

     private List<GrupoEmpresaProduto> inserirGrupoEmpresaProdutos(final List<TipoProduto> produtos, final List<GrupoEmpresaProduto> grupoEmpresaProdutos, final Long idGrupoEmpresa) {

          List<GrupoEmpresaProduto> grupoEmpresaProdutosAux = new ArrayList<GrupoEmpresaProduto>();

          for (TipoProduto produto : produtos) {
               Optional<GrupoEmpresaProduto> gep = obterGrupoEmpresaProduto(grupoEmpresaProdutos, produto.getIdentificador());
               if (Boolean.FALSE.equals(gep.isPresent())) {
                    grupoEmpresaProdutosAux.add(
                              GrupoEmpresaProduto.builder()
                              .idGrupoEmpresa(idGrupoEmpresa)
                              .idProduto(produto.getIdentificador())
                              .dataCadastro(LocalDateTime.now())
                              .build()
                              );
               }
          }

          return grupoEmpresaProdutosAux;
     }

     private Optional<GrupoEmpresaProduto> obterGrupoEmpresaProduto(final List<GrupoEmpresaProduto> grupoEmpresaProdutos, final Integer idProduto) {

          return Optional.ofNullable(grupoEmpresaProdutos.stream().filter(gep -> gep.getIdProduto().equals(idProduto)).findFirst().orElse(null));
     }

}
