
package br.com.conductor.rhblueapi.service.relatorios;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.EXCLUSAO_RELATORIOS_STATUS_PROCESSANDO;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.USUARIO_E_GRUPO_SEM_ACESSO_AO_RELATORIO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.RELATORIOS;
import static br.com.conductor.rhblueapi.enums.relatorios.status.StatusSolicitacaoRelatorio.PROCESSANDO;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricaoPorStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.relatorios.RelatorioPortal;
import br.com.conductor.rhblueapi.domain.relatorios.TipoRelatorioPortal;
import br.com.conductor.rhblueapi.domain.relatorios.response.RelatorioResponse;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioFilter;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioRequest;
import br.com.conductor.rhblueapi.enums.relatorios.TipoRelatorio;
import br.com.conductor.rhblueapi.enums.relatorios.status.StatusSolicitacaoRelatorio;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.relatorios.RelatorioDetalhesPedidosPublish;
import br.com.conductor.rhblueapi.repository.relatorios.RelatorioCustomRepositoryImpl;
import br.com.conductor.rhblueapi.repository.relatorios.RelatorioPortalRepository;
import br.com.conductor.rhblueapi.repository.relatorios.TipoRelatorioPortalRepository;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.Transacional;

@Service
public class RelatorioService {
     
     @Autowired
     private RelatorioPortalRepository relatorioPortalRepository;
     
     @Autowired
     private TipoRelatorioPortalRepository tipoRelatorioPortalRepository;
     
     @Autowired
     private RelatorioCustomRepositoryImpl relatorioCustomRepositoryImpl;
     
     @Autowired
     private StatusCached statusCached;
     
     @Autowired
     private RelatorioDetalhesPedidosPublish relatorioDetalhesPedidosPublish;
     
     @Value("${app.plataforma.rh.cod}")
     private Integer idPlataforma;
     
     public RelatorioResponse solicitar(RelatorioRequest relatorioRequest) {

          TipoRelatorio tipoRelatorio = TipoRelatorio.DETALHES_PEDIDOS;
          TipoRelatorioPortal tipoRelatorioPortal = buscarTipoRelatorio(tipoRelatorio);
          RelatorioPortal relatorioPortal = inserirSolicitacao(relatorioRequest ,tipoRelatorioPortal);
          RelatorioResponse response = createResponse(relatorioPortal);
          publicarMensagem(response);     
          return response;
     }

     private TipoRelatorioPortal buscarTipoRelatorio(TipoRelatorio tipoRelatorio) {
          
          return tipoRelatorioPortalRepository.findByIdPlataformaAndDescricaoTipoRelatorio(idPlataforma, tipoRelatorio.getDescricao());
     }
     
     private RelatorioPortal inserirSolicitacao(RelatorioRequest relatorioRequest, TipoRelatorioPortal tipoRelatorioPortal) {
          
          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(RELATORIOS.getNome());
          final StatusDescricao statusProcessando = mapStatusDescricao(dominioStatusCargas, PROCESSANDO.getDescricao());

          return relatorioPortalRepository.save(RelatorioPortal.builder()
                    .tipoRelatorioPortal(tipoRelatorioPortal)
                    .dataSolicitacao(LocalDateTime.now())
                    .dataInicio(relatorioRequest.getPeriodoRequest().getDataInicio())
                    .dataFim(relatorioRequest.getPeriodoRequest().getDataFim())
                    .idUsuario(relatorioRequest.getIdentificacao().getIdUsuario())
                    .idGrupoEmpresa(relatorioRequest.getIdentificacao().getIdGrupoEmpresa())
                    .status(statusProcessando.getStatus())
                    .build());
          
     }
     
     private RelatorioResponse createResponse(RelatorioPortal relatorioPortal) {

          return RelatorioResponse.builder()
                    .id(relatorioPortal.getId())
                    .dataSolicitacao(relatorioPortal.getDataSolicitacao())
                    .dataInicio(relatorioPortal.getDataInicio())
                    .dataFim(relatorioPortal.getDataFim())
                    .nomeRelatorio(relatorioPortal.getTipoRelatorioPortal().getDescricaoTipoRelatorio())
                    .status(getStatusSolicitacao(relatorioPortal.getStatus()))
                    .build();

     }
     
     private void publicarMensagem(RelatorioResponse response) {
          
          relatorioDetalhesPedidosPublish.enviarIdentificadorSolicitacaoRelatorio(response);
     }
     
     public Page<RelatorioResponse> buscar(RelatorioFilter relatorioFilter) {
          
          Pageable pageable = PageRequest.of(relatorioFilter.getNumberPage(), relatorioFilter.getSizePage());
          
          Integer totalElements = relatorioCustomRepositoryImpl.countAllWithParameters(relatorioFilter);
          
          List<RelatorioResponse> content = relatorioCustomRepositoryImpl.findAllWithParameters(relatorioFilter);
          
          content.stream().forEach(f -> f.setStatus(getStatusSolicitacao(f.getStatusRelatorio())));
          
          return new PageImpl<>(content, pageable, totalElements);
     }
     
     private String getStatusSolicitacao(Integer status) {
          
          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(RELATORIOS.getNome());
          final StatusDescricao statusDescricao = mapStatusDescricaoPorStatus(dominioStatusCargas, status);
          
          return StatusSolicitacaoRelatorio.findByDescricao(statusDescricao.getDescricao()).name();
     }

     @Transacional
     public void deletarRelatorios(final List<RelatorioPortal> relatorios) {

          relatorioPortalRepository.deleteAll(relatorios);
     }

     public List<RelatorioPortal> obterRelatoriosPorIdsEIdUsuarioEIdGrupoEmpresa(final List<Long> ids, Long idUsuario, Long idGrupoEmpresa) {

          return relatorioPortalRepository.findByIdInAndIdUsuarioAndIdGrupoEmpresa(ids, idUsuario, idGrupoEmpresa);
     }

     private boolean possuiRelatoriosComStatus(final List<RelatorioPortal> relatorios, final StatusSolicitacaoRelatorio status) {

          List<StatusDescricao> dominioStatusRelatorio = this.statusCached.busca(RELATORIOS.getNome());
          final StatusDescricao statusDescricao = mapStatusDescricao(dominioStatusRelatorio, status.getDescricao());

          return relatorios.stream().anyMatch(r -> r.getStatus().equals(statusDescricao.getStatus()));
     }

     public List<RelatorioPortal> autenticarRelatoriosProcessadosPorIdsEUsuarioEGrupoEmpresa(List<Long> ids, Long idUsuario, Long idGrupoEmpresa) {

          List<RelatorioPortal> relatorios = obterRelatoriosPorIdsEIdUsuarioEIdGrupoEmpresa(ids, idUsuario, idGrupoEmpresa);

          ForbiddenCdt.checkThrow(ids.size() != relatorios.size(), USUARIO_E_GRUPO_SEM_ACESSO_AO_RELATORIO);

          BadRequestCdt.checkThrow(possuiRelatoriosComStatus(relatorios, StatusSolicitacaoRelatorio.PROCESSANDO), EXCLUSAO_RELATORIOS_STATUS_PROCESSANDO);

          return relatorios;
     }

     public Optional<RelatorioPortal> obterRelatorioPorIdEIdUsuarioEIdGrupoEmpresa(final Long id, final Long idUsuario, final Long idGrupoEmpresa) {

          return relatorioPortalRepository.findByIdAndIdUsuarioAndIdGrupoEmpresa(id, idUsuario, idGrupoEmpresa);
     }

     public RelatorioPortal autenticarRelatorioProcessadoPorIdEUsuarioEGrupoEmpresa(final Long id, final Long idUsuario, final Long idGrupoEmpresa) {

          Optional<RelatorioPortal> optRelatorio = obterRelatorioPorIdEIdUsuarioEIdGrupoEmpresa(id, idUsuario, idGrupoEmpresa);

          NotFoundCdt.checkThrow(Boolean.FALSE.equals(optRelatorio.isPresent()), ExceptionsMessagesCdtEnum.RELATORIO_NOT_FOUND);

          List<StatusDescricao> dominioStatusRelatorio = this.statusCached.busca(RELATORIOS.getNome());
          final StatusDescricao statusProcessado = mapStatusDescricao(dominioStatusRelatorio, StatusSolicitacaoRelatorio.PROCESSADO.getDescricao());

          BadRequestCdt.checkThrow(optRelatorio.get().getStatus() != statusProcessado.getStatus(), ExceptionsMessagesCdtEnum.RELATORIO_DIFERENTE_PROCESSADO);

          return optRelatorio.get();

     }

     public Optional<RelatorioPortal> obterRelatorioPorId(final Long id) {

          return relatorioPortalRepository.findById(id);
     }

     public RelatorioPortal salvar(final RelatorioPortal relatorioPortal) {

          return relatorioPortalRepository.save(relatorioPortal);
     }

}
