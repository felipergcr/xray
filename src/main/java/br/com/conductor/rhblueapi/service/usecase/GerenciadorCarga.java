package br.com.conductor.rhblueapi.service.usecase;

import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.RECEBIDO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.VALIDADO_PARCIAL;
import static br.com.conductor.rhblueapi.enums.StatusPagamentoEnum.AGUARDANDO_PAGAMENTO;
import static br.com.conductor.rhblueapi.enums.StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE;
import static br.com.conductor.rhblueapi.enums.StatusPagamentoEnum.LIMITE_CREDITO_VENCIDO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS_STATUSPAGAMENTO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.FORMA_PAGAMENTO;
import static br.com.conductor.rhblueapi.enums.limiteCredito.OrigemBuscaLimiteCreditoEnum.GERENCIADOR_LIMITE_DIARIO;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.PARAMETRO_RH;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhesErros;
import br.com.conductor.rhblueapi.domain.CargaBeneficio;
import br.com.conductor.rhblueapi.domain.EmpresasCargas;
import br.com.conductor.rhblueapi.domain.EmpresasCargasDetalhes;
import br.com.conductor.rhblueapi.domain.EmpresasCargasDetalhesProdutos;
import br.com.conductor.rhblueapi.domain.PedidoUpload;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.LimiteCredito.LimiteCredito;
import br.com.conductor.rhblueapi.domain.pedido.CargaPedidoCustom;
import br.com.conductor.rhblueapi.domain.pedido.MensagemGerarCarga;
import br.com.conductor.rhblueapi.domain.pedido.MensagemItemPedido;
import br.com.conductor.rhblueapi.domain.pedido.MensagemPedidoDetalhado;
import br.com.conductor.rhblueapi.domain.proc.CadastroFuncionarioDetalhe;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;
import br.com.conductor.rhblueapi.enums.StatusDetalheEnum;
import br.com.conductor.rhblueapi.enums.StatusPagamentoEnum;
import br.com.conductor.rhblueapi.enums.TipoPagamentoPedidoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.FormasPagamento;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPagamento;
import br.com.conductor.rhblueapi.exception.BusinessException;
import br.com.conductor.rhblueapi.exception.ConversaoObjetoException;
import br.com.conductor.rhblueapi.exception.ObjetoVazioException;
import br.com.conductor.rhblueapi.repository.BeneficioCargaControleFinanceiroRepository;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.CargasPublish;
import br.com.conductor.rhblueapi.service.CargaBeneficioService;
import br.com.conductor.rhblueapi.service.EmpresaService;
import br.com.conductor.rhblueapi.service.EmpresasCargasDetalhesProdutosService;
import br.com.conductor.rhblueapi.service.EmpresasCargasDetalhesService;
import br.com.conductor.rhblueapi.service.EmpresasCargasService;
import br.com.conductor.rhblueapi.service.LimiteCreditoService;
import br.com.conductor.rhblueapi.service.ParametrosService;
import br.com.conductor.rhblueapi.service.PedidoDetalhadoErrosService;
import br.com.conductor.rhblueapi.service.PedidoDetalhadoService;
import br.com.conductor.rhblueapi.service.PedidoService;
import br.com.conductor.rhblueapi.service.email.EmailFinalizaPedidoService;
import br.com.conductor.rhblueapi.service.email.EmailPendenciaLimiteService;
import br.com.conductor.rhblueapi.service.email.EmailPendenciaTedService;
import br.com.conductor.rhblueapi.service.funcionario.GerenciadorFuncionario;
import br.com.conductor.rhblueapi.service.strategy.formaPagamento.FormaPagamentoFactory;
import br.com.conductor.rhblueapi.service.strategy.formaPagamento.FormaPagamentoStrategy;
import br.com.conductor.rhblueapi.util.Transacional;

@Component
public class GerenciadorCarga {

     @Autowired
     private StatusCached statusCached;

     @Autowired
     private PedidoService pedidoService;
     
     @Autowired
     private CargaBeneficioService cargaBeneficioService;

     @Autowired
     private EmpresasCargasService empresasCargasService;
     
     @Autowired
     private EmpresasCargasDetalhesService empresasCargasDetalhesService;

     @Autowired
     private EmpresasCargasDetalhesProdutosService empresasCargasDetalhesProdutosService;
     
     @Autowired
     private BeneficioCargaControleFinanceiroRepository beneficioCargaControleFinanceiroRepository;
     
     @Autowired
     private AtualizacaoFuncionario atualizacaoFuncionario;

     @Autowired
     private PedidoDetalhadoService pedidoDetalhadoService;

     @Autowired
     private PedidoDetalhadoErrosService pedidoDetalhadoErrosService;

     @Autowired
     private CargasPublish cargasPublish;

     @Autowired
     private EmpresaService empresaService;

     @Autowired
     private ValidacaoNegocio validacaoNegocio;
     
     @Autowired
     private GerenciadorFuncionario gerenciadorFuncionario;
     
     @Autowired
     private GerenciadorCarga gerenciadorCargaAop;
     
     @Autowired
     private EmailFinalizaPedidoService emailFinalizaPedidoService;
     
     @Autowired
     private EmailPendenciaLimiteService emailPendenciaLimiteService;
     
     @Autowired
     private LimiteCreditoService limiteCreditoService;

     @Autowired
     private EmailPendenciaTedService emailPendenciaTedService;

     @Autowired
     private ParametrosService parametrosService;
     
     @Autowired
     private FormaPagamentoFactory formaPagamentoFactory;
     
     private static final String MENSAGEM_FALHA_DE_NEGOCIO = "Falha de negócio: valor excedente";

     public void gerenciaImportacaoArquivo(final PedidoUpload pedidoUpload) {

          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(ARQUIVO_CARGAS.getNome());
          final StatusDescricao statusRecebido = mapStatusDescricao(dominioStatusCargas, RECEBIDO.getStatus());

          Optional<ArquivoCargas> arquivoCargas = pedidoService.buscaPorId(pedidoUpload.getIdArquivoCargas());

          arquivoCargas.ifPresent(arquivo -> {
               pedidoDetalhadoService.deletarArquivoCargasDetalhadoPor(arquivo.getId());
               arquivo.setStatus(statusRecebido.getStatus());
               pedidoService.salvar(arquivo);
               
               try {

                    boolean isArquivoValido = pedidoService.inicializaImportacao(arquivo, pedidoUpload.getTipoEntregaCartao());
                    arquivo = pedidoService.atualizaDadosPosProcessamento(arquivo, isArquivoValido, null);

                    if(isArquivoValido) {
                         cargasPublish.enviarPedidoParaGerarDetalhes(MensagemPedidoDetalhado.builder()
                                   .arquivo(arquivo)
                                   .detalhes(pedidoDetalhadoService.buscaListaDetalhesPorIdArquivo(arquivo.getId()).get())
                                   .build());
                    }

                         

               } catch (ObjetoVazioException | ConversaoObjetoException | BusinessException e) {
                    pedidoService.atualizaDadosPosProcessamento(arquivo, false, e.getMessage());
               }
          });

     }

     
     public void gerenciaEDistribuiDetalhes(final MensagemPedidoDetalhado mensagemPedidoDetalhado) {
          
          Map<String, LocalDate> mapDetalhes = mensagemPedidoDetalhado.getDetalhes().stream().collect(
                    Collectors.toMap(ArquivosCargasDetalhes::getCnpj, ArquivosCargasDetalhes::getDataAgendamentoCarga,
                         (oldValue, newValue) -> oldValue
                        )
                    );

          mapDetalhes.entrySet().stream().forEach(detalhe -> {

               Long idEmpresa = empresaService.buscaIdEmpresaPorCnpj(detalhe.getKey()).get();
               
               gerenciadorCargaAop.geraCargaBeneficioEEmpresaCarga(mensagemPedidoDetalhado.getArquivo(), idEmpresa, detalhe.getValue());

          });
          
          cargasPublish.enviarDetalhesParaGerarFuncionario(mensagemPedidoDetalhado);
          
     }
     
     @Transactional
     public void geraCargaBeneficioEEmpresaCarga(final ArquivoCargas arquivo, final Long idEmpresa, final LocalDate dataAgendamentoCarga) {
          
          Long idCargaBeneficio = cargaBeneficioService.persisteEObtemIdCargaBeneficio(arquivo, dataAgendamentoCarga, idEmpresa);

          empresasCargasService.persisteEObtemIdEmpresaCarga(idCargaBeneficio, idEmpresa);
     }

     @Transacional
     public void executaProcessoRegistroValido(ArquivoCargas arquivoCargas, MensagemGerarCarga mensagemGerarCarga) {

          final CadastroFuncionarioDetalhe cadastroFuncionarioDetalhe = this.atualizacaoFuncionario.atualizaFuncionarioProduto(mensagemGerarCarga);

          final Long idCargaBeneficio = cargaBeneficioService.buscaIdCargaBeneficioPor(arquivoCargas.getId(), cadastroFuncionarioDetalhe.getIdEmpresa(), arquivoCargas.getIdGrupoEmpresa()).get();

          final Long idEmpresasCargas = empresasCargasService.buscaIdEmpresaCargaPor(idCargaBeneficio, cadastroFuncionarioDetalhe.getIdEmpresa()).get();

          final Long idEmpresasCargasDetalhes = empresasCargasDetalhesService.geraEmpresasCargasDetalhes(mensagemGerarCarga.getMensagemItemPedido().getDetalhe(), cadastroFuncionarioDetalhe, idEmpresasCargas);

          empresasCargasDetalhesProdutosService.geraEmpresasCargasDetalhesProdutos(idEmpresasCargasDetalhes, mensagemGerarCarga.getMensagemItemPedido().getDetalhe(), cadastroFuncionarioDetalhe);

          pedidoDetalhadoService.atualizarStatus(mensagemGerarCarga.getMensagemItemPedido().getDetalhe(), StatusDetalheEnum.IMPORTADO);

     }

     @Transactional
     public void executaProcessoRegistroInvalido(ArquivosCargasDetalhes detalhe) {

          detalhe.setMotivoErro(MENSAGEM_FALHA_DE_NEGOCIO);

          pedidoDetalhadoService.atualizarStatus(detalhe, StatusDetalheEnum.INVALIDADO);

          ArquivosCargasDetalhesErros erro = new ArquivosCargasDetalhesErros(null, detalhe.getId(), detalhe.getMotivoErro());
          pedidoDetalhadoErrosService.salvar(erro);

     }

     public void finalizaProcessosGerarCargas(Long idArquivoCargas, final TipoPagamentoPedidoEnum tipoPagamentoPedido) {

          List<CargaBeneficio> cargasBeneficios = cargaBeneficioService.buscaEAlteraStatusEmLotePor(idArquivoCargas, tipoPagamentoPedido, null);
          List<Long> idsCargasBeneficio = cargasBeneficios.stream().map(CargaBeneficio::getIdCargaBeneficio).collect(Collectors.toList());
          
          List<EmpresasCargas> empresasCargas = empresasCargasService.buscaEmpresasCargaPorIdsCargaBeneficio(idsCargasBeneficio);
          List<Long> idsEmpresasCargas = empresasCargas.stream().map(EmpresasCargas::getId).collect(Collectors.toList());
          
          List<EmpresasCargasDetalhes> empresasCargasDetalhes = empresasCargasDetalhesService.buscaEAlteraStatusEmLotePorIdsEmpresasCargas(idsEmpresasCargas);
          List<Long> idsEmpresasCargasDetalhes = empresasCargasDetalhes.stream().map(EmpresasCargasDetalhes::getId).collect(Collectors.toList());
          
          List<EmpresasCargasDetalhesProdutos> empresasCargasDetalhesProdutos = empresasCargasDetalhesProdutosService.buscaEAlteraStatusEmLotePorIdEmpresasCargasDetalhes(idsEmpresasCargasDetalhes);

          gerenciadorCargaAop.finalizaProcessosGerarCargasPersistencia(idArquivoCargas, cargasBeneficios, idsEmpresasCargas, empresasCargasDetalhes, empresasCargasDetalhesProdutos);

     }
     
     @Transactional
     public void finalizaProcessosGerarCargasPersistencia(final Long idArquivoCargas, final List<CargaBeneficio> cargasBeneficios, final List<Long> idsEmpresasCargas, 
               final List<EmpresasCargasDetalhes> empresasCargasDetalhes, List<EmpresasCargasDetalhesProdutos> empresasCargasDetalhesProdutos) {
          
          cargaBeneficioService.salvarEmLote(cargasBeneficios);
          
          empresasCargasService.salvarEmLote(idsEmpresasCargas);
          
          empresasCargasDetalhesService.salvarEmLote(empresasCargasDetalhes);
          
          empresasCargasDetalhesProdutosService.salvarEmLote(empresasCargasDetalhesProdutos);
          
          this.beneficioCargaControleFinanceiroRepository.atualizaControleFinanceiro(idArquivoCargas);
     }

     private ArquivoCargas atualizaStatusArquivoCargaECargaBeneficio(final ArquivoCargas arquivoCargas, final TipoPagamentoPedidoEnum tipoPagamentoPedido) {

          final StatusPagamentoEnum statusPagamentoEnum = obterStatusPagamento(arquivoCargas, tipoPagamentoPedido);
          
          cargaBeneficioService.atualizaStatusCargasBeneficioPor(arquivoCargas.getId(), tipoPagamentoPedido, statusPagamentoEnum);
          
          return pedidoService.atualizaStatusArquivoCarga(arquivoCargas, PROCESSAMENTO_CONCLUIDO, statusPagamentoEnum);
     }

     public void enviarNotificacaoProcessamento(final ArquivoCargas arquivoCargas) {

          final Long totalItemsInvalidos = pedidoDetalhadoService.buscaQuantidadeRegistrosPor(arquivoCargas.getId(), StatusDetalheEnum.INVALIDADO);

          if (totalItemsInvalidos.longValue() > 0) {
               emailFinalizaPedidoService.enviar(arquivoCargas, VALIDADO_PARCIAL);
          } else {
               String formaPagamento = parametrosService.buscaParametrosGrupoEmpresaPor(arquivoCargas.getIdGrupoEmpresa(), PARAMETRO_RH, FORMA_PAGAMENTO).getValor();

               if (equalsIgnoreCase(TipoPagamento.TED.getValor(), formaPagamento)) {
                    emailPendenciaTedService.enviar(arquivoCargas, PROCESSAMENTO_CONCLUIDO);
               } else {
                    emailFinalizaPedidoService.enviar(arquivoCargas, PROCESSAMENTO_CONCLUIDO);
               }
          }

     }

     public void processoCargasFuncionarios(MensagemGerarCarga mensagemGerarCarga) {
          
          ArquivoCargas arquivoCargas = mensagemGerarCarga.getMensagemItemPedido().getArquivo();

          if(this.validacaoNegocio.isValid(mensagemGerarCarga.getMensagemItemPedido().getDetalhe()))
               gerenciadorCargaAop.executaProcessoRegistroValido(arquivoCargas, mensagemGerarCarga);
          else
               gerenciadorCargaAop.executaProcessoRegistroInvalido(mensagemGerarCarga.getMensagemItemPedido().getDetalhe());
          
          arquivoCargas = pedidoService.buscaPorId(arquivoCargas.getId()).get();
          
          mensagemGerarCarga.getMensagemItemPedido().setArquivo(arquivoCargas);
          
          if(Objects.isNull(arquivoCargas.getUuid()))
               cargasPublish.enviarMensagemFinalizarPedido(mensagemGerarCarga, null);
          
     }
     
     public void finalizaPedido(MensagemGerarCarga mensagemGerarCarga, String uuid) {
          
          ArquivoCargas arquivoCargas = mensagemGerarCarga.getMensagemItemPedido().getArquivo();
          
          if(Objects.isNull(uuid)) {
               arquivoCargas = pedidoService.inserirUUID(arquivoCargas);
               mensagemGerarCarga.getMensagemItemPedido().setArquivo(arquivoCargas);
          }
          
          if(!pedidoService.isStatusPedidoDiferenteDe(arquivoCargas.getId(), StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO))
               return;
          
          if(pedidoDetalhadoService.buscaQuantidadeRegistrosPor(arquivoCargas.getId(), StatusDetalheEnum.VALIDADO) == 0) {
               
               TipoPagamentoPedidoEnum tipoPagamentoPedido = TipoPagamentoPedidoEnum.getByNome(arquivoCargas.getTipoPagamentoPedido());
               
               this.finalizaProcessosGerarCargas(arquivoCargas.getId(), tipoPagamentoPedido);

               ArquivoCargas arquivoCargasAux = atualizaStatusArquivoCargaECargaBeneficio(arquivoCargas, tipoPagamentoPedido);

               this.gerenciaDisparoEmail(arquivoCargasAux);

               FormasPagamento formaPagamento = parametrosService.obterFormaPagamento(arquivoCargasAux.getIdGrupoEmpresa());
               FormaPagamentoStrategy formaPagamentoStrategy = formaPagamentoFactory.criar(formaPagamento);
               formaPagamentoStrategy.registrarPendenciaFinanceira(arquivoCargasAux);

          } else {
               cargasPublish.enviarMensagemFinalizarPedido(mensagemGerarCarga, arquivoCargas.getUuid());
          }
          
     }
     
     public void gerenciaFuncionarios(MensagemPedidoDetalhado mensagemPedidoDetalhado) {
          
          ArquivoCargas arquivo = mensagemPedidoDetalhado.getArquivo();
          
          mensagemPedidoDetalhado.getDetalhes().stream().forEach(detalhe -> {
               
               gerenciadorFuncionario.gerenciaFuncionario(MensagemItemPedido.builder()
                         .arquivo(arquivo)
                         .detalhe(detalhe)
                         .build());
          });
          
     }
     
     private void gerenciaDisparoEmail(final ArquivoCargas arquivoCargas) {
          
          if(arquivoCargas.getTipoPagamentoPedido().equals(TipoContratoEnum.PRE.name()))
               this.gerenciaDisparoEmailPre(arquivoCargas);
          else
               this.gerenciaDisparoEmailPos(arquivoCargas);
     }
     
     private void gerenciaDisparoEmailPos(final ArquivoCargas arquivoCargas) {

          if (isPedidoComPendenciaLimite(arquivoCargas))
               emailPendenciaLimiteService.enviar(arquivoCargas, LIMITE_CREDITO_VENCIDO);
          else
               this.enviarNotificacaoProcessamento(arquivoCargas);
     }
     
     private void gerenciaDisparoEmailPre(final ArquivoCargas arquivoCargas) {
          
          this.enviarNotificacaoProcessamento(arquivoCargas);
     }


     private boolean isPedidoComPendenciaLimite(ArquivoCargas arquivoCargas) {

          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome());
          final StatusDescricao statusPagamentoLimiteCreditoVencido = mapStatusDescricao(dominioStatusCargas, LIMITE_CREDITO_VENCIDO.getStatus());
          final StatusDescricao statusPagamentoLimiteCreditoInsulficiente = mapStatusDescricao(dominioStatusCargas, LIMITE_CREDITO_INSUFICIENTE.getStatus());
          
          if(arquivoCargas.getStatusPagamento().equals(statusPagamentoLimiteCreditoVencido.getStatus()) || 
                    arquivoCargas.getStatusPagamento().equals(statusPagamentoLimiteCreditoInsulficiente.getStatus()))
               return true;
          
          return false;
     }

     private StatusPagamentoEnum obterStatusPagamentoPosPago(final ArquivoCargas arquivoCargas) {
          
          if (limiteCreditoService.isDataVigenciaDoLimiteExpirado(arquivoCargas.getIdGrupoEmpresa())) {
               return StatusPagamentoEnum.LIMITE_CREDITO_VENCIDO;
          } else if (limiteCreditoService.isLimiteCreditoInsuficiente(arquivoCargas.getIdGrupoEmpresa())) {
               return StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE;
          } else {
               return StatusPagamentoEnum.AGUARDANDO_PAGAMENTO;
          }
     }

     private StatusPagamentoEnum obterStatusPagamentoPrePago() {

          return StatusPagamentoEnum.AGUARDANDO_PAGAMENTO;
     }

     private StatusPagamentoEnum obterStatusPagamento(final ArquivoCargas arquivoCargas, final TipoPagamentoPedidoEnum tipoPagamentoPedido) {

          if (tipoPagamentoPedido.equals(TipoPagamentoPedidoEnum.PRE)) {
               return obterStatusPagamentoPrePago();
          } else {
               return obterStatusPagamentoPosPago(arquivoCargas);
          }
     }
     
     public void gerenciaRegrasLimiteDiario(List<CargaPedidoCustom> cargasPedidos) {
          
          Long idGrupoEmpresa = cargasPedidos.get(0).getIdGrupoEmpresa();
          
          LimiteCredito limiteCredito = limiteCreditoService.obterLimiteCredito(idGrupoEmpresa, GERENCIADOR_LIMITE_DIARIO);
          
          if(limiteCredito.isValidaLimite()) {
               this.validaDataVigenciaELimiteDiario(cargasPedidos, limiteCredito);
          }
     }
     
     private void validaDataVigenciaELimiteDiario(List<CargaPedidoCustom> cargasPedidos, LimiteCredito limiteCredito) {
          
          Long idGrupoEmpresa = cargasPedidos.get(0).getIdGrupoEmpresa();
          
          if(this.isDataAtualDentroDaDataVigencia(idGrupoEmpresa)) {
               this.validacaoLimiteDiario(cargasPedidos, limiteCredito);
          } else {
               List<Long> numerosPedidos = cargasPedidos.stream().map(CargaPedidoCustom::getNumeroPedido).collect(Collectors.toList());
               gerenciadorCargaAop.atualizaStatusProcessoTask(numerosPedidos, LIMITE_CREDITO_VENCIDO);
          }
     }
     
     private void validacaoLimiteDiario(List<CargaPedidoCustom> cargasPedidos, LimiteCredito limiteCredito) {
          
          BigDecimal limiteDisponivel = limiteCredito.getValorDisponivel();
          
          List<Long> pedidosDentroDoLimite = new ArrayList<Long>();
          List<Long> pedidosForaDoLimite = new ArrayList<Long>();
          
          List<CargaPedidoCustom> cargasPedidosOrdenadoPorNumeroPedido = cargasPedidos.stream().sorted(Comparator.comparing(CargaPedidoCustom::getNumeroPedido)).collect(Collectors.toList());
          
          for(int i = 0; i < cargasPedidosOrdenadoPorNumeroPedido.size(); i++ ) {
               
               CargaPedidoCustom cargaPedido = cargasPedidosOrdenadoPorNumeroPedido.get(i);
               
               BigDecimal valorPedido = cargaPedido.getValorCarga().setScale(2, RoundingMode.HALF_UP);

               if(this.isValorSeEnquadraNoLimite(limiteDisponivel, valorPedido)) {
                    
                    limiteDisponivel = limiteDisponivel.subtract(valorPedido);
                    
                    pedidosDentroDoLimite.add(cargaPedido.getNumeroPedido());
                    
               } else {
                    break;
               }
          }
          
          pedidosForaDoLimite = cargasPedidosOrdenadoPorNumeroPedido.stream()
                    .filter(cargaPedido -> !pedidosDentroDoLimite.contains(cargaPedido.getNumeroPedido()))
                    .map(CargaPedidoCustom::getNumeroPedido)
                    .collect(Collectors.toList());
          
          if(!CollectionUtils.isEmpty(pedidosDentroDoLimite))
               gerenciadorCargaAop.atualizaStatusProcessoTask(pedidosDentroDoLimite, AGUARDANDO_PAGAMENTO);
          
          if(!CollectionUtils.isEmpty(pedidosForaDoLimite))
               gerenciadorCargaAop.atualizaStatusProcessoTask(pedidosForaDoLimite, LIMITE_CREDITO_INSUFICIENTE);
     }
     
     private boolean isDataAtualDentroDaDataVigencia(Long idGrupoEmpresa) {
          
          LocalDate vigenciaLimite = parametrosService.obterDataVigenciaDoLimite(idGrupoEmpresa);
          
          LocalDate hoje = LocalDate.now();
          
          if(vigenciaLimite.isEqual(hoje) || vigenciaLimite.isAfter(hoje))
               return true;
          return false;
     }
     
     private boolean isValorSeEnquadraNoLimite(BigDecimal valorDisponivelLimite, BigDecimal valorCarga) {
          
          if(valorDisponivelLimite.subtract(valorCarga).compareTo(BigDecimal.ZERO) >= 0)
               return true;
          return false;
     }
     
     @Transactional
     public void atualizaStatusProcessoTask(List<Long> numerosPedidos, StatusPagamentoEnum status) {
          
          pedidoService.atualizaStatusPagamentoEmLote(numerosPedidos, status);
          cargaBeneficioService.atualizaStatusCargasBeneficioEmLote(numerosPedidos, status);
     }
     
}
