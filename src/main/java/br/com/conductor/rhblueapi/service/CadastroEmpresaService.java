package br.com.conductor.rhblueapi.service;

import br.com.conductor.rhblueapi.domain.Empresa;
import br.com.conductor.rhblueapi.domain.PessoaEmpresa;
import br.com.conductor.rhblueapi.domain.persist.EmpresaPersist;
import br.com.conductor.rhblueapi.domain.persist.EnderecoAprovadoPersist;
import br.com.conductor.rhblueapi.domain.persist.PessoaPersist;
import br.com.conductor.rhblueapi.domain.persist.TelefonePessoaAprovadaPersist;
import br.com.conductor.rhblueapi.domain.response.EmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.EnderecoResponse;
import br.com.conductor.rhblueapi.domain.response.PessoaResponse;
import br.com.conductor.rhblueapi.enums.StatusEmpresaEnum;
import br.com.conductor.rhblueapi.enums.TipoEnderecoEnum;
import br.com.conductor.rhblueapi.repository.EmpresaRepository;
import br.com.conductor.rhblueapi.util.GenericConvert;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class CadastroEmpresaService {

     @Autowired
     private PessoaService pessoaService;

     @Autowired
     private EmpresaRepository empresaRepository;

     private static final String CANAL_ENTRADA = "RH-API";

     private static final Integer ID_TIPO_TEL_COMERCIAL = 2;

     @Transactional
     public EmpresaResponse salvar(Long idSubGrupo, EmpresaPersist empresaPersist) {

          PessoaPersist pessoaPersist = getPessoaFisica(empresaPersist);

          PessoaResponse pessoaResponse = pessoaService.salvar(pessoaPersist);

          Empresa empresa = GenericConvert.convertModelMapper(empresaPersist, Empresa.class);
          empresa.setIdGrupoEmpresa(idSubGrupo);
          setValuesDefaultEmpresa(empresa, pessoaResponse.getIdPessoa(), empresaPersist);

          Empresa empresaSalva = empresaRepository.save(empresa);

          EmpresaResponse empresaResponse = mapResponseEmpresa(pessoaResponse, empresaSalva);

          return empresaResponse;
     }

     private PessoaPersist getPessoaFisica(EmpresaPersist empresaPersist) {

          return PessoaPersist.builder()
                    .nome(empresaPersist.getRazaoSocial())
                    .canalEntrada(CANAL_ENTRADA)
                    .documento(empresaPersist.getCnpj())
                    .diaVencimento(1)
                    .enderecos(montaEndereco(empresaPersist))
                    .telefones(montaTelefone(empresaPersist))
                    .build();
     }

     private List<TelefonePessoaAprovadaPersist> montaTelefone(EmpresaPersist empresaPersist) {

          return Arrays.asList(TelefonePessoaAprovadaPersist.builder()
                    .idTipoTelefone(ID_TIPO_TEL_COMERCIAL)
                    .ddd(addZeroDDD(empresaPersist.getTelefone().getDdd()))
                    .telefone(empresaPersist.getTelefone().getTelefone()).build());
     }

     private String addZeroDDD(String numero) {

          return StringUtils.join(0, numero);
     }

     private List<EnderecoAprovadoPersist> montaEndereco(EmpresaPersist empresaPersist) {

          return Arrays.asList(EnderecoAprovadoPersist.builder()
                    .idTipoEndereco(TipoEnderecoEnum.CORRESPONDENCIA.getId())
                    .bairro(empresaPersist.getEndereco().getBairro())
                    .cep(empresaPersist.getEndereco().getCep())
                    .cidade(empresaPersist.getEndereco().getCidade())
                    .logradouro(empresaPersist.getEndereco().getLogradouro())
                    .numero(empresaPersist.getEndereco().getNumero())
                    .uf(empresaPersist.getEndereco().getUf())
                    .complemento(StringUtils.isEmpty(empresaPersist.getEndereco().getComplemento())
                              ? null : empresaPersist.getEndereco().getComplemento())
                    .build());
     }

     private void setValuesDefaultEmpresa(Empresa empresa, Long idPessoaFisica, EmpresaPersist empresaPersist) {

          PessoaEmpresa pessoa = PessoaEmpresa.builder().idPessoa(idPessoaFisica).build();
          empresa.setPessoa(pessoa);
          empresa.setDataCadastro(LocalDateTime.now());
          empresa.setStatus(StatusEmpresaEnum.PENDENTE.getId());
          if (!Objects.equals(empresaPersist.getFlagMatriz(), 1)) {
               empresa.setFlagMatriz(0);
          }
     }

     private EmpresaResponse mapResponseEmpresa(PessoaResponse pessoaResponse, Empresa empresaResponseDB) {

          return EmpresaResponse.builder()
                    .nomeEmpresa(empresaResponseDB.getNomeExibicao())
                    .idEmpresa(empresaResponseDB.getId())
                    .idPessoa(pessoaResponse.getIdPessoa())
                    .idGrupoEmpresa(empresaResponseDB.getIdGrupoEmpresa())
                    .idEnderecoCorrespondencia(getIdEnderecoCorrespondencia(pessoaResponse.getEnderecos()))
                    .build();
     }

     private Long getIdEnderecoCorrespondencia(List<EnderecoResponse> enderecosAprovadosResponse) {

          for (EnderecoResponse enderecoAprovadoResponse : enderecosAprovadosResponse) {

               if (enderecoAprovadoResponse.isCorrespondencia()) {

                    return enderecoAprovadoResponse.getId();
               }
          }

          return null;
     }
}
