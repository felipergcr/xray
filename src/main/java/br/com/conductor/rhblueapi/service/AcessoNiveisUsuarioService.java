
package br.com.conductor.rhblueapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.AcessoNiveisUsuario;
import br.com.conductor.rhblueapi.domain.response.AcessoNiveisUsuarioResponse;
import br.com.conductor.rhblueapi.enums.Plataforma;
import br.com.conductor.rhblueapi.repository.AcessoNiveisUsuarioRepository;
import br.com.conductor.rhblueapi.util.GenericConvert;

@Service
public class AcessoNiveisUsuarioService {

     @Value("${app.plataforma.rh.cod}")
     private int RH_ID_PLATAFORMA;

     @Autowired
     AcessoNiveisUsuarioRepository acessoNiveisUsuarioRepository;

     public List<AcessoNiveisUsuarioResponse> listarAcessosNiveisUsuario(Plataforma plataforma) {

          List<AcessoNiveisUsuario> acessos = new ArrayList<AcessoNiveisUsuario>();

          if (plataforma == null) {
               acessos = acessoNiveisUsuarioRepository.findAll();
          } else {
               Integer idPlataforma = resolveIdPlataforma(plataforma);
               acessos = acessoNiveisUsuarioRepository.findByIdPlataforma(idPlataforma);
          }

          List<AcessoNiveisUsuarioResponse> retorno = new ArrayList<AcessoNiveisUsuarioResponse>();

          for (AcessoNiveisUsuario ac : acessos)
               retorno.add(GenericConvert.convertModelMapper(ac, AcessoNiveisUsuarioResponse.class));

          return retorno;
     }

     private int resolveIdPlataforma(Plataforma plataforma) {

          if (Plataforma.RH.equals(plataforma)) {
               return RH_ID_PLATAFORMA;
          }
          return 0;
     }
}
