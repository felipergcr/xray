
package br.com.conductor.rhblueapi.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import br.com.conductor.rhblueapi.domain.Endereco;
import br.com.conductor.rhblueapi.domain.PessoaEmpresa;
import br.com.conductor.rhblueapi.domain.PessoaFisicaCadastro;
import br.com.conductor.rhblueapi.domain.TelefonePessoaFisica;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.EnderecoAprovadoPersist;
import br.com.conductor.rhblueapi.domain.persist.PessoaPersist;
import br.com.conductor.rhblueapi.domain.persist.TelefonePessoaAprovadaPersist;
import br.com.conductor.rhblueapi.domain.response.EnderecoResponse;
import br.com.conductor.rhblueapi.domain.response.PessoaResponse;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.repository.EnderecoRepository;
import br.com.conductor.rhblueapi.repository.PessoaRepository;
import br.com.conductor.rhblueapi.util.AppConstantes;
import br.com.conductor.rhblueapi.util.GenericConvert;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class PessoaService {

     @Autowired
     private PessoaRepository pessoaRepository;
     
     @Autowired
     private EnderecoRepository enderecoRepository;

     public PessoaResponse salvar(final PessoaPersist pessoaPersist) {

          BadRequestCdt.checkThrow(Objeto.isBlank(pessoaPersist.getDocumento()), ExceptionsMessagesCdtEnum.CPF_NAO_INFORMADO);
          BadRequestCdt.checkThrow(!Objeto.isBlank(pessoaPersist.getDataNascimento()) &&  pessoaPersist.getDataNascimento().isAfter(LocalDate.now()), ExceptionsMessagesCdtEnum.DATA_NASCIMENTO_INVALIDA);
          BadRequestCdt.checkThrow(Objeto.isBlank(pessoaPersist.getDiaVencimento()), ExceptionsMessagesCdtEnum.CONTA_DIA_VENCIMENTO_INVALIDO);

          PessoaEmpresa pessoa = GenericConvert.convertModelMapper(pessoaPersist, PessoaEmpresa.class);
          PessoaFisicaCadastro pessoaFisica = GenericConvert.convertModelMapper(pessoaPersist, PessoaFisicaCadastro.class);
          pessoaFisica.setPessoaEmpresa(pessoa);
          pessoa.setPessoaFisica(pessoaFisica);

          adicionarTelefones(pessoaFisica, pessoaPersist.getTelefones());
          
          PessoaEmpresa pessoaSalva = pessoaRepository.save(pessoa);
          
          List<Endereco> enderecos = adicionarEnderecos(pessoaSalva.getPessoaFisica(), pessoaPersist.getEnderecos());
          
          List<EnderecoResponse> enderecosResponse = new ArrayList<EnderecoResponse>();
          
          enderecos.stream().forEach(endereco -> {
               enderecosResponse.add(GenericConvert.convertModelMapper(endereco, EnderecoResponse.class));
          });
          
          PessoaResponse pessoaResponse = GenericConvert.convertModelMapper(pessoaSalva, PessoaResponse.class);

          BadRequestCdt.checkThrow(Objeto.isBlank(pessoaSalva.getPessoaFisica()), ExceptionsMessagesCdtEnum.ERRO_AO_SALVAR_PESSOA_FISICA);

          GenericConvert.convertModelMapper(pessoaSalva.getPessoaFisica(), pessoaResponse);
          
          pessoaResponse.setEnderecos(enderecosResponse);

          return pessoaResponse;
     }

     private void adicionarTelefones(PessoaFisicaCadastro pessoa, List<TelefonePessoaAprovadaPersist> telefonesPersist) {

          List<TelefonePessoaFisica> telefones = Lists.newArrayList();

          if (Objeto.notBlank(telefonesPersist)) {

               for (TelefonePessoaAprovadaPersist telefonePessoaAprovadaPersist : telefonesPersist) {
                    TelefonePessoaFisica telefone = GenericConvert.convertModelMapper(telefonePessoaAprovadaPersist, TelefonePessoaFisica.class);
                    telefone.setPessoaFisica(pessoa);

                    telefones.add(telefone);
               }

               pessoa.setTelefones(telefones);
          }
     }

     private List<Endereco> adicionarEnderecos(PessoaFisicaCadastro pessoa, List<EnderecoAprovadoPersist> enderecosAprovadosPersist) {

          BadRequestCdt.checkThrow(!existeEnderecoCorrespondencia(enderecosAprovadosPersist), ExceptionsMessagesCdtEnum.ENDERECO_CORRESPONDENCIA_INEXISTENTE);

          List<Endereco> enderecos = Lists.newArrayList();

          if (Objeto.notBlank(enderecosAprovadosPersist)) {

               BadRequestCdt.checkThrow((enderecosAprovadosPersist.size() > 3), ExceptionsMessagesCdtEnum.ENDERECO_NAO_DEVE_CONTER_MAIS_DE_TRES);

               List<Integer> tiposEndereco = Lists.newArrayList();

               Integer qtdeCorrespondencia = 0;
               for (EnderecoAprovadoPersist enderecoAprovadoPersist : enderecosAprovadosPersist) {

                    Endereco endereco = GenericConvert.convertModelMapper(enderecoAprovadoPersist, Endereco.class);
                    endereco.setIdPessoaFisica(pessoa.getIdPessoa());
                    endereco.setIdEstabelecimento(AppConstantes.ESTABELECIMENTO_PADRAO);

                    if (enderecoAprovadoPersist.isCorrespondencia()) {
                         qtdeCorrespondencia++;
                    }

                    BadRequestCdt.checkThrow(tiposEndereco.contains(enderecoAprovadoPersist.getIdTipoEndereco()), ExceptionsMessagesCdtEnum.ENDERECO_DEVE_SER_DE_TIPOS_DIFERENTES);

                    tiposEndereco.add(enderecoAprovadoPersist.getIdTipoEndereco());

                    enderecos.add(endereco);
               }

               BadRequestCdt.checkThrow((qtdeCorrespondencia > 1), ExceptionsMessagesCdtEnum.ENDERECO_DEVE_CONTER_APENAS_UM_PARA_CORRESPONDENCIA);

          }
          
          return enderecoRepository.saveAll(enderecos);
          
     }

     private Boolean existeEnderecoCorrespondencia(List<EnderecoAprovadoPersist> enderecos) {

          for(EnderecoAprovadoPersist endereco : enderecos) {

               if(endereco.isCorrespondencia()) {
                    return true;
               }
          }

          return false;
     }

     public void deletePessoa(Long idPessoa) {
          pessoaRepository.deleteById(idPessoa);
     }
     
     public Optional<Long> buscaPessoaJuridica(String cnpj) {

          Optional<PessoaEmpresa> optPessoaEmpresa = pessoaRepository.findByDocumento(cnpj);

          return Optional.ofNullable(optPessoaEmpresa.isPresent() ? optPessoaEmpresa.get().getIdPessoa() : null);
     }
     
}
