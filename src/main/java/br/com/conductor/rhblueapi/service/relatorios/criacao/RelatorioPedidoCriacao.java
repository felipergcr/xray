
package br.com.conductor.rhblueapi.service.relatorios.criacao;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.relatorios.RelatorioPedidoCustom;
import br.com.conductor.rhblueapi.enums.TipoPagamentoPedidoEnum;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.FormasPagamento;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoProduto;
import br.com.conductor.rhblueapi.enums.relatorios.RelatorioHeaderPedido;
import br.com.conductor.rhblueapi.enums.relatorios.TipoArquivoRelatorio;
import br.com.conductor.rhblueapi.service.relatorios.RelatorioPedidoService;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.service.utils.StatusUtils;
import br.com.conductor.rhblueapi.util.CriacaoXlsUtils;
import br.com.conductor.rhblueapi.util.DataUtils;
import br.com.conductor.rhblueapi.util.relatorio.RelatorioCriacaoDefinition;

@Component
public class RelatorioPedidoCriacao implements RelatorioCriacaoDefinition<RelatorioPedidoCustom>{

     private static final String NOME_ABA = "Relatorio_Detalhado";

     private static final String NOME_ARQUIVO = "Relatorio_Detalhado_Pedido";

     @Autowired
     private StatusCached statuscached;

     @Autowired
     private RelatorioPedidoService relatorioPedidoService;

     @Override
     public TipoArquivoRelatorio getTipoArquivo() {

          return TipoArquivoRelatorio.XLS;
     }

     @Override
     public String getNomeAba() {

          return NOME_ABA;
     }

     @Override
     public String getNomeArquivo() {

          return NOME_ARQUIVO;
     }

     @Override
     public String getNomeArquivoCompleto() {

          return new StringBuilder(getNomeArquivo()).append(".").append(getTipoArquivo().getTipo()).toString();
     }

     public byte[] gerarRelatorioPorGrupoEPeriodo(final Long idGrupoEmpresa, final LocalDate dataInicio, final LocalDate dataFim) throws IOException {

          List<RelatorioPedidoCustom> listaRelatorioPedidoCustom = relatorioPedidoService.autenticarRelatorioPedidoCustom(idGrupoEmpresa, dataInicio, dataFim);

          List<List<String>> relatorioGenerico = criarRelatorioGenerico(listaRelatorioPedidoCustom);

          return CriacaoXlsUtils.gerarEmByte(NOME_ABA, relatorioGenerico);

     }

     private List<String> listaCabecalho() {

          List<String> cabecalhos = new ArrayList<>();
          Arrays.asList(RelatorioHeaderPedido.values()).forEach(cabecalho -> cabecalhos.add(cabecalho.getHeader()));
          return cabecalhos;
     }

     private String formatarValorTotalPagamento(BigDecimal valorTotalPagamento) {

          return valorTotalPagamento.setScale(2, BigDecimal.ROUND_DOWN).toString().replace(".", ",");
     }

     private List<List<String>> listaDados(final List<RelatorioPedidoCustom> listaRelatorioPedidoCustom) {

          List<List<String>> dados = new ArrayList<>();
          for (RelatorioPedidoCustom relatorioPedidoCustom : listaRelatorioPedidoCustom) {
               String idPedido = relatorioPedidoCustom.getIdPedido().toString();
               String cnpj = relatorioPedidoCustom.getCnpj();
               String razaoSocial = relatorioPedidoCustom.getRazaoSocial();
               String nomeGrupo = relatorioPedidoCustom.getNomeGrupo();
               String nomeSubgrupo = relatorioPedidoCustom.getNomeSubGrupo();
               TipoProduto tipoProduto = TipoProduto.obterPorIdentificador(relatorioPedidoCustom.getIdProduto().intValue());
               String descricaoProduto = Objects.isNull(tipoProduto) ? "" : tipoProduto.getDescricaoBen();
               String dtPedido = DataUtils.localDateTimeParaStringddMMyyyyHHmmss(relatorioPedidoCustom.getDtPedido());
               List<StatusDescricao> dominioStatusCargas = statuscached.busca(TipoStatus.CARGAS_BENEFICIOS.getNome());
               StatusDescricao statusCargas = StatusUtils.mapStatusDescricaoPorStatus(dominioStatusCargas, relatorioPedidoCustom.getStatusCargasBeneficios());
               String statusPedido = statusCargas.getDescricao();
               String qtdFuncSolitado = relatorioPedidoCustom.getQtdFuncSolicitado().toString();
               String qtdFuncCreditado = Objects.isNull(relatorioPedidoCustom.getQtdFuncCreditado()) ? "" : relatorioPedidoCustom.getQtdFuncCreditado().toString();
               String qtdCartoesNovos = relatorioPedidoCustom.getQtdCartoesNovos().toString();
               String valorPedido = relatorioPedidoCustom.getValorPedido().toString();
               String dtCreditoAgendada = DataUtils.localDateParaStringddMMyyyy(relatorioPedidoCustom.getDtCreditoAgendada());
               String dtCreditoEfetivada = Objects.isNull(relatorioPedidoCustom.getDtCreditoEfetiva()) ? "" : DataUtils.localDateParaStringddMMyyyy(relatorioPedidoCustom.getDtCreditoEfetiva());
               FormasPagamento formaPag = FormasPagamento.obterPorChave(relatorioPedidoCustom.getDescricaoTipoBoleto());
               String formaPagamento = Objects.isNull(formaPag) ? "" : formaPag.getDescricao();
               TipoPagamentoPedidoEnum tipoPagamentoPedidoEnum = TipoPagamentoPedidoEnum.getByNome(relatorioPedidoCustom.getModalidadePagamento());
               String modalidadePagamento = Objects.isNull(tipoPagamentoPedidoEnum) ? "" : tipoPagamentoPedidoEnum.getDescricao();
               String prazoPagamento = relatorioPedidoCustom.getPrazoPagamento();
               String dtVencimento = DataUtils.localDateParaStringddMMyyyy(relatorioPedidoCustom.getDtVencimento());
               String valorTotalPagamento = formatarValorTotalPagamento(relatorioPedidoCustom.getValorTotalPagamento());
               List<StatusDescricao> dominioArquivoCargaStatusPagamento = statuscached.busca(TipoStatus.ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome());
               StatusDescricao statusDescricaoPagamento = StatusUtils.mapStatusDescricaoPorStatus(dominioArquivoCargaStatusPagamento, relatorioPedidoCustom.getStatusPagamentoArquivoCargaStd());
               String statusPagamento = statusDescricaoPagamento.getDescricao();
               String dtPagamento = Objects.isNull(relatorioPedidoCustom.getDtPagamento()) ? "" : DataUtils.localDateParaStringddMMyyyy(relatorioPedidoCustom.getDtPagamento());

               dados.add(Arrays.asList(idPedido, 
                         cnpj, 
                         razaoSocial, 
                         nomeGrupo, 
                         nomeSubgrupo, 
                         descricaoProduto, 
                         dtPedido, 
                         statusPedido, 
                         qtdFuncSolitado, 
                         qtdFuncCreditado, 
                         qtdCartoesNovos, 
                         valorPedido, 
                         dtCreditoAgendada, 
                         dtCreditoEfetivada, 
                         formaPagamento, 
                         modalidadePagamento, 
                         prazoPagamento, 
                         dtVencimento, 
                         valorTotalPagamento, 
                         statusPagamento, 
                         dtPagamento));

          }

          return dados;
     }

     private List<List<String>> criarRelatorioGenerico(final List<RelatorioPedidoCustom> listaRelatorioPedidoCustom) {

          List<List<String>> relatorioGenerico = new ArrayList<>();

          relatorioGenerico.add(listaCabecalho());

          relatorioGenerico.addAll(listaDados(listaRelatorioPedidoCustom));

          return relatorioGenerico;
     }
}
