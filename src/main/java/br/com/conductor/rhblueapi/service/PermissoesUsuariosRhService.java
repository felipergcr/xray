package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.PERMISSAO_NAO_ENCONTRADA;
import static br.com.conductor.rhblueapi.enums.StatusPermissaoEnum.ATIVO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.PERMISSOES_USUARIOS_RH;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioResumido;
import br.com.conductor.rhblueapi.controleAcesso.repository.UsuarioRepository;
import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.StatusDescricoes;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.PermissoesRhPersist;
import br.com.conductor.rhblueapi.domain.request.PermissoesRhRequest;
import br.com.conductor.rhblueapi.domain.response.PagePermissoesRhResponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.domain.response.PermissoesRhResponse;
import br.com.conductor.rhblueapi.domain.response.PermissoesUsuariosRhResponse;
import br.com.conductor.rhblueapi.domain.update.PermissoesRhUpdate;
import br.com.conductor.rhblueapi.enums.StatusPermissaoEnum;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.ConflictCdt;
import br.com.conductor.rhblueapi.exception.ExceptionCdt;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.PermissoesUsuariosRhRepository;
import br.com.conductor.rhblueapi.repository.StatusDescricoesRepositoryCustom;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.EntityGenericUtil;
import br.com.conductor.rhblueapi.util.GenericConvert;
import br.com.conductor.rhblueapi.util.PageUtils;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class PermissoesUsuariosRhService {

     private static final Integer STATUS_PERMISSAO_ATIVA = 1;

     @Autowired
     private PermissoesUsuariosRhRepository permissoesRepository;

     @Autowired
     private UsuarioRepository usuarioRepository;

     @Autowired
     private GrupoEmpresaRepository grupoEmpresaRepository;

     @Autowired
     private StatusDescricoesRepositoryCustom statusDescricoesRepositoryCustom;

     @Autowired
     private StatusCached statusCached;

     @Autowired
     private GrupoEmpresaService grupoEmpresaService;

     @Autowired
     private UsuarioPierService usuarioPierService;

     private HashMap<Integer, StatusPermissaoEnum> mapStatus = new HashMap<Integer, StatusPermissaoEnum>();

     private final String PERMISSOESUSUARIOSRH = "PERMISSOESUSUARIOSRH";

     public PermissoesRhResponse salvar(final PermissoesRhPersist persist, Boolean recurso) {

          BadRequestCdt.checkThrow(Objeto.isBlank(persist.getIdUsuario()), ExceptionsMessagesCdtEnum.USUARIO_OBRIGATORIO);

          NotFoundCdt.checkThrow(!usuarioRepository.findById(persist.getIdUsuario()).isPresent(), ExceptionsMessagesCdtEnum.USUARIO_NAO_LOCALIZADO);

          BadRequestCdt.checkThrow(Objeto.isBlank(persist.getIdGrupoEmpresa()), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_OBRIGATORIO);

          NotFoundCdt.checkThrow(!grupoEmpresaRepository.findById(persist.getIdGrupoEmpresa()).isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_LOCALIZADO);

          BadRequestCdt.checkThrow(Objeto.isBlank(persist.getIdUsuarioRegistro()), ExceptionsMessagesCdtEnum.USUARIO_REGISTRO_OBRIGATORIO);

          NotFoundCdt.checkThrow(!usuarioRepository.findById(persist.getIdUsuarioRegistro()).isPresent(), ExceptionsMessagesCdtEnum.USUARIO_REGISTRO_NAO_ENCONTRADO);

          if (recurso) {
               BadRequestCdt.checkThrow(!permissoesRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(persist.getIdUsuarioRegistro(), persist.getIdGrupoEmpresa(), 1).isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);
          }

          ConflictCdt.checkThrow(permissoesRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(persist.getIdUsuario(), persist.getIdGrupoEmpresa(), 1).isPresent(), ExceptionsMessagesCdtEnum.PERMISSAO_USUARIO_GRUPO_EMPRESA_EXISTENTE);

          PermissoesUsuariosRh permissaoInsert = GenericConvert.convertModelMapper(persist, PermissoesUsuariosRh.class);
          GrupoEmpresa grupoEmpresa = new GrupoEmpresa();
          grupoEmpresa.setId(persist.getIdGrupoEmpresa());
          permissaoInsert.setGrupoEmpresa(grupoEmpresa);
          permissaoInsert.setStatus(1);
          permissaoInsert.setDataRegistro(LocalDateTime.now());

          PermissoesUsuariosRh permissao = permissoesRepository.save(permissaoInsert);

          if (Objeto.notBlank(permissao) && Objeto.notBlank(permissao.getStatus())) {
               obterStatusPermissoes();
               permissao.setStatusDescricao(mapStatus.get(permissao.getStatus()));
          }

          return GenericConvert.convertModelMapper(permissao, PermissoesRhResponse.class);
     }

     public PermissoesRhResponse alterar(Long id, PermissoesRhUpdate update) {

          Optional<PermissoesUsuariosRh> permissoesOpt = permissoesRepository.findById(id);
          NotFoundCdt.checkThrow(!permissoesOpt.isPresent(), ExceptionsMessagesCdtEnum.PERMISSAO_NAO_ENCONTRADA);

          PermissoesUsuariosRh permissoesAnterior = permissoesOpt.get();

          if (!Objects.isNull(update.getIdUsuario()) && !update.getIdUsuario().equals(permissoesAnterior.getIdUsuario())) {
               NoContentCdt.checkThrow(!usuarioRepository.findById(update.getIdUsuario()).isPresent(), ExceptionsMessagesCdtEnum.USUARIO_NAO_LOCALIZADO);
          }

          if (!Objects.isNull(update.getIdGrupoEmpresa()) && !update.getIdGrupoEmpresa().equals(permissoesAnterior.getGrupoEmpresa().getId())) {
               BadRequestCdt.checkThrow(!grupoEmpresaRepository.findById(update.getIdGrupoEmpresa()).isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_LOCALIZADO);
          }

          NotFoundCdt.checkThrow(!usuarioRepository.findById(update.getIdUsuarioRegistro()).isPresent(), ExceptionsMessagesCdtEnum.USUARIO_REGISTRO_NAO_ENCONTRADO);

          BadRequestCdt.checkThrow(!permissoesRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(update.getIdUsuarioRegistro(), update.getIdGrupoEmpresa(), 1).isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          GrupoEmpresa grupoEmpresa = new GrupoEmpresa();
          grupoEmpresa.setId(update.getIdGrupoEmpresa());

          PermissoesRhResponse retorno = new PermissoesRhResponse();
          try {
               StatusDescricoes status = statusDescricoesRepositoryCustom.findStatusDescByNomeStatusAndDesc(PERMISSOESUSUARIOSRH, update.getStatusDescricao().toString());
               BadRequestCdt.checkThrow(Objects.isNull(status) || Objects.isNull(status.getStatus()), ExceptionsMessagesCdtEnum.STATUS_INVALIDO);

               PermissoesUsuariosRh updateConvertido = PermissoesUsuariosRh.builder().idUsuario(update.getIdUsuario()).grupoEmpresa(grupoEmpresa).idUsuarioRegistro(update.getIdUsuarioRegistro()).status(status.getStatus().intValue()).dataRegistro(LocalDateTime.now()).build();

               permissoesAnterior = EntityGenericUtil.mergeObjects(updateConvertido, permissoesAnterior, PermissoesUsuariosRh.class);

               PermissoesUsuariosRh permissao = permissoesRepository.save(permissoesAnterior);

               retorno = GenericConvert.convertModelMapper(permissao, PermissoesRhResponse.class);
               obterStatusPermissoes();
               retorno.setStatusDescricao(mapStatus.get(permissao.getStatus()));

          } catch (Exception e) {
               ExceptionCdt.checkThrow(true, ExceptionsMessagesCdtEnum.GLOBAL_ERRO_SERVIDOR);
          }

          return retorno;
     }

     public PagePermissoesRhResponse listar(Integer page, Integer size, PermissoesRhRequest request) {

          PermissoesUsuariosRh permissao = GenericConvert.convertModelMapper(request, PermissoesUsuariosRh.class);

          if (Objeto.notBlank(request.getStatusDescricao())) {
               Long idStatus = 0L;
               try {
                    idStatus = statusDescricoesRepositoryCustom.findStatusDescByNomeStatusAndDesc(PERMISSOESUSUARIOSRH, request.getStatusDescricao().name()).getStatus();
               } catch (Exception e) {
                    e.printStackTrace();
                    ExceptionCdt.checkThrow(true, ExceptionsMessagesCdtEnum.GLOBAL_ERRO_SERVIDOR);
               }
               permissao.setStatus(Integer.valueOf(idStatus.toString()));
          }

          GrupoEmpresa grupoEmpresa = new GrupoEmpresa();
          grupoEmpresa.setId(request.getIdGrupoEmpresa());
          permissao.setGrupoEmpresa(grupoEmpresa);

          Example<PermissoesUsuariosRh> example = Example.of(permissao);

          Pageable pageable = PageRequest.of(page, size);

          Page<PermissoesUsuariosRh> response = permissoesRepository.findAll(example, pageable);

          NoContentCdt.checkThrow(Objects.isNull(response) || !response.hasContent(), ExceptionsMessagesCdtEnum.RECURSO_NAO_ENCONTRADO);

          List<Long> idsUsuarios = response.getContent().stream().map(PermissoesUsuariosRh::getIdUsuario).collect(Collectors.toList());

          List<Usuario> usuarios = usuarioRepository.findAllByIdIn(idsUsuarios);
          HashMap<Long, UsuarioResumido> mapUsuarios = new HashMap<Long, UsuarioResumido>();
          for (Usuario usr : usuarios) {
               mapUsuarios.put(usr.getId(), GenericConvert.convertModelMapper(usr, UsuarioResumido.class));
          }

          obterStatusPermissoes();
          for (PermissoesUsuariosRh perm : response) {
               perm.setUsuario(mapUsuarios.get(perm.getIdUsuario()));
               perm.setStatusDescricao(mapStatus.get(perm.getStatus()));
          }

          List<PermissoesUsuariosRhResponse> ret = response.getContent().stream().map(p -> PermissoesUsuariosRhResponse.builder().id(p.getId()).idUsuario(p.getIdUsuario()).grupoEmpresa(p.getGrupoEmpresa()).statusDescricao(p.getStatusDescricao()).idUsuarioRegistro(p.getIdUsuarioRegistro()).usuario(p.getUsuario()).build()).collect(Collectors.toList());

          PageResponse<PermissoesUsuariosRhResponse> pageResponse = new PageResponse<>();
          pageResponse.setContent(ret);
          Integer countElements = Long.valueOf(response.getTotalElements()).intValue();
          pageResponse = PageUtils.ajustarPageResponse(pageResponse, page, size, countElements);

          return new PagePermissoesRhResponse(pageResponse);
     }

     private void obterStatusPermissoes() {

          if (!mapStatus.isEmpty()) {
               return;
          }

          try {
               List<StatusDescricoes> status = statusDescricoesRepositoryCustom.findStatusDescByNomeStatus(PERMISSOESUSUARIOSRH);
               for (StatusDescricoes statusDescricoes : status) {
                    mapStatus.put(statusDescricoes.getStatus().intValue(), StatusPermissaoEnum.valueOf(statusDescricoes.getDescricao()));
               }

          } catch (Exception e) {
               e.printStackTrace();
               ExceptionCdt.checkThrow(true, ExceptionsMessagesCdtEnum.GLOBAL_ERRO_SERVIDOR);
          }
     }
     
     public Long salvarPermissaoViaSalesforce(PermissoesRhPersist permissaoPersist) {

          try {
               return this.buscaIdPermissaoAtivaPor(permissaoPersist.getIdUsuario(), permissaoPersist.getIdGrupoEmpresa());
          }catch(NotFoundCdt e) {
               return this.salvarPermissao(permissaoPersist).getId();
          }
     }
     
     private PermissoesRhResponse salvarPermissao(PermissoesRhPersist permissaoPersist) {
          
          PermissoesUsuariosRh permissao = this.montarPermissao(permissaoPersist);
          permissao = this.salvarPermissao(permissao);
          return this.montarPermissaoResponse(permissao);
     }
     
     public Long buscaIdPermissaoAtivaPor(Long idUsuario, Long idGrupoEmpresa) {
          
          List<StatusDescricao> dominioStatus = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatus, ATIVO.name());
                    
          Long IdPermissao = permissoesRepository.findIdByIdUsuarioAndGrupoEmpresaIdAndStatus(idUsuario, idGrupoEmpresa, statusPermissaoAtiva.getStatus());
          
          NotFoundCdt.checkThrow(Objects.isNull(IdPermissao), PERMISSAO_NAO_ENCONTRADA);
          
          return IdPermissao;
     }
     
     public Long getIdPermissaoAtiva(Long idUsuario, Long idGrupoEmpresa) {
          
          List<StatusDescricao> dominioStatus = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatus, ATIVO.name());
                    
          Long IdPermissao = permissoesRepository.findIdByIdUsuarioAndGrupoEmpresaIdAndStatus(idUsuario, idGrupoEmpresa, statusPermissaoAtiva.getStatus());
          
          return IdPermissao;
     }
        
     private PermissoesUsuariosRh montarPermissao(PermissoesRhPersist permissaoRh) {
          
          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, ATIVO.name());
          
          PermissoesUsuariosRh permissao = GenericConvert.convertModelMapper(permissaoRh, PermissoesUsuariosRh.class);
          permissao.setGrupoEmpresa(new GrupoEmpresa());
          permissao.getGrupoEmpresa().setId(permissaoRh.getIdGrupoEmpresa());
          permissao.setStatus(statusPermissaoAtiva.getStatus());
          permissao.setDataRegistro(LocalDateTime.now());
          
          return permissao;
          
     }
     
     private PermissoesUsuariosRh salvarPermissao(final PermissoesUsuariosRh permissao) {
         
          return permissoesRepository.save(permissao);
     }
     
     private PermissoesRhResponse montarPermissaoResponse(PermissoesUsuariosRh permissao) {
          
          List<StatusDescricao> dominioStatus = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          
          Optional<StatusDescricao> statusPermissao = dominioStatus.stream().filter(status -> status.getStatus().equals(permissao.getStatus())).findFirst();

          permissao.setStatusDescricao(StatusPermissaoEnum.findByDescricao(statusPermissao.get().getDescricao()));

          return GenericConvert.convertModelMapper(permissao, PermissoesRhResponse.class);
          
     }

     public Optional<PermissoesUsuariosRh> obterPermissaoAtivaPor(Long idUsuarioRegistro, Long idGrupo) {

          List<StatusDescricao> dominioStatus = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatus, StatusPermissaoEnum.ATIVO.name());

           return permissoesRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(idUsuarioRegistro, idGrupo, statusPermissaoAtiva.getStatus());

     }


     public PermissoesUsuariosRh autenticarPermissaoAtiva(Long idUsuarioRegistro, Long idGrupo) {

          Optional<PermissoesUsuariosRh> permissaoUsuarioRegistroValido = permissoesRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(idUsuarioRegistro, idGrupo, STATUS_PERMISSAO_ATIVA);

          ForbiddenCdt.checkThrow(!permissaoUsuarioRegistroValido.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          return permissaoUsuarioRegistroValido.get();
     }

     public List<Integer> buscaIdsEmpresasPorPermissaoUsuario(Long idPermissao) {

          return permissoesRepository.findEmpresasbyIdPermissao(idPermissao);

     }

     public Boolean isPossuiAutorizacaoEmpresa(Long idPermissao, Long idEmpresa) {

          List<Integer> listIdsEmpresasPermissao = permissoesRepository.findEmpresasbyIdPermissao(idPermissao);
          
          if(!CollectionUtils.isEmpty(listIdsEmpresasPermissao)) {
               return listIdsEmpresasPermissao.contains(idEmpresa.intValue());
          }
          
          return false;

     }

     public Optional<PermissoesUsuariosRh> autenticarPermissaoGrupoAtiva(Long idUsuarioRegistro, Long idGrupo) {

          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome()), ATIVO.name());

          Optional<PermissoesUsuariosRh> permissaoUsuarioRegistroValido = permissoesRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(idUsuarioRegistro, idGrupo, statusPermissaoAtiva.getStatus());

          ForbiddenCdt.checkThrow(!permissaoUsuarioRegistroValido.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          return permissaoUsuarioRegistroValido;
     }

     public List<Integer> listarEmpresasPermitidas(final Long idUsuario, final Long idGrupoEmpresa) {

          usuarioPierService.autenticarUsuario(idUsuario);

          grupoEmpresaService.autenticarGrupoEmpresa(idGrupoEmpresa);

          Optional<PermissoesUsuariosRh> permissao = this.autenticarPermissaoGrupoAtiva(idUsuario, idGrupoEmpresa);

          return permissoesRepository.findEmpresasbyIdPermissao(permissao.get().getId());

     }

}
