
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.StatusEmpresaCargaDetalheProdutoEnum.CARGA_EM_PROCESSAMENTO;
import static br.com.conductor.rhblueapi.enums.StatusEmpresaCargaDetalheProdutoEnum.CONFIRMADO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.EMPRESAS_CARGAS_DETALHES_PRODUTOS;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import br.com.conductor.rhblueapi.domain.EmpresasCargasDetalhesProdutos;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.proc.CadastroFuncionarioDetalhe;
import br.com.conductor.rhblueapi.repository.EmpresasCargasDetalhesProdutosRepository;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;

@Service
public class EmpresasCargasDetalhesProdutosService {

     @Autowired
     private EmpresasCargasDetalhesProdutosRepository empresasCargasDetalhesProdutosRepository;

     @Autowired
     private StatusCached statusCached;
     
     @Value("${app.cargas.paginacao.tamanho}")
     private Integer quantidadeItensPaginacao;
     
     public List<EmpresasCargasDetalhesProdutos> buscaEAlteraStatusEmLotePorIdEmpresasCargasDetalhes(List<Long> idsEmpresasCargasDetalhes) {
          
          List<EmpresasCargasDetalhesProdutos> empresasCargasDetalhesProdutos = this.buscaEmpresasCargaPor(idsEmpresasCargasDetalhes);
          
          final List<StatusDescricao> dominioEmpresaCargasDetalhesProdutos = this.statusCached.busca(EMPRESAS_CARGAS_DETALHES_PRODUTOS.getNome());
          final StatusDescricao statusEmpresasCargasProdutosConfirmado = mapStatusDescricao(dominioEmpresaCargasDetalhesProdutos, CONFIRMADO.getStatus());

          return empresasCargasDetalhesProdutos.stream().map(detalhes -> {
               detalhes.setStatus(statusEmpresasCargasProdutosConfirmado.getStatus());
               return detalhes;
          }).collect(toList());
     } 
     
     public void salvarEmLote(List<EmpresasCargasDetalhesProdutos> empresasCargasDetalhesProdutos) {

          empresasCargasDetalhesProdutosRepository.saveAll(empresasCargasDetalhesProdutos);
     }

     private List<EmpresasCargasDetalhesProdutos> buscaEmpresasCargaPor(List<Long> idsEmpresasCargasDetalhes) {
          
          List<EmpresasCargasDetalhesProdutos> listaEmpresasCargasDetalhesProdutos = new ArrayList<EmpresasCargasDetalhesProdutos>();
          
          IntStream.range(0, (idsEmpresasCargasDetalhes.size() + quantidadeItensPaginacao - 1) / quantidadeItensPaginacao)
          .mapToObj(i -> idsEmpresasCargasDetalhes.subList(i * quantidadeItensPaginacao, Math.min(quantidadeItensPaginacao * (i + 1), idsEmpresasCargasDetalhes.size())))
          .forEach(lista -> {
               listaEmpresasCargasDetalhesProdutos.addAll(empresasCargasDetalhesProdutosRepository.findByIdEmpresaCargaDetalheIn(lista));
          });
          
          return listaEmpresasCargasDetalhesProdutos;
     }

     public void geraEmpresasCargasDetalhesProdutos(Long idEmpresaCargaDetalhe , ArquivosCargasDetalhes detalhe, CadastroFuncionarioDetalhe cadastroFuncionarioDetalhe ) {
          
          final StatusDescricao statusCargasEmProcessamento = mapStatusDescricao(this.statusCached.busca(EMPRESAS_CARGAS_DETALHES_PRODUTOS.getNome()), CARGA_EM_PROCESSAMENTO.getStatus());
          
          this.empresasCargasDetalhesProdutosRepository.save(EmpresasCargasDetalhesProdutos.builder()
                    .idEmpresaCargaDetalhe(idEmpresaCargaDetalhe)
                    .idFuncionariosProdutos(cadastroFuncionarioDetalhe.getIdFuncionarioProduto())
                    .valor(new BigDecimal(detalhe.getValorBeneficio()))
                    .flagNovoCartao(cadastroFuncionarioDetalhe.getFlagCartao())
                    .status(statusCargasEmProcessamento.getStatus())
                    .statusData(LocalDateTime.now())
                    .build());
     }

}
