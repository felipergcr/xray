
package br.com.conductor.rhblueapi.service.acessos;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.USUARIO_SEM_PERMISSAO_ATUALIZAR_FUNCIONARIO;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.Funcionario;
import br.com.conductor.rhblueapi.domain.request.funcionario.FuncionarioAtualizacaoRequest;
import br.com.conductor.rhblueapi.exception.ConteudoExcelException;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.service.funcionario.FuncionarioService;
import br.com.conductor.rhblueapi.util.ValidacaoTemplate;

@Service
public class FuncionarioAcessoService {
     
     @Autowired
     private GerenciadorAcessoService gerenciadorAcessoService;
     
     @Autowired
     private FuncionarioService funcionarioService;
     
     public List<String> validaFuncionarioAtualizacaoRequest(FuncionarioAtualizacaoRequest funcionarioAtualizacaoRequest) {
          
          List<String> erros = new ArrayList<String>();
          
          try {
               ValidacaoTemplate.validaNomeCompleto(funcionarioAtualizacaoRequest.getNome());
          } catch (ConteudoExcelException e) {
               erros.add(e.getMessage());
          }
          
          try {
               ValidacaoTemplate.validacaoMatricula(funcionarioAtualizacaoRequest.getMatricula());
          } catch (ConteudoExcelException e) {
               erros.add(e.getMessage());
          }
          
          try {
               ValidacaoTemplate.validaDataNascimento(funcionarioAtualizacaoRequest.getDataNascimento().format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")));
          } catch (ConteudoExcelException e) {
               erros.add(e.getMessage());
          }
          
          if(Objects.nonNull(funcionarioAtualizacaoRequest.getEndereco())) {
               
               try {
                    ValidacaoTemplate.validaLogradouro(funcionarioAtualizacaoRequest.getEndereco().getLogradouro());
               } catch (ConteudoExcelException e) {
                    erros.add(e.getMessage());
               }
               
               try {
                    ValidacaoTemplate.validaNumero(funcionarioAtualizacaoRequest.getEndereco().getNumero());
               } catch (ConteudoExcelException e) {
                    erros.add(e.getMessage());
               }
               
               try {
                    ValidacaoTemplate.validaComplemento(funcionarioAtualizacaoRequest.getEndereco().getComplemento());
               } catch (ConteudoExcelException e) {
                    erros.add(e.getMessage());
               }
               
               try {
                    ValidacaoTemplate.validaBairro(funcionarioAtualizacaoRequest.getEndereco().getBairro());
               } catch (ConteudoExcelException e) {
                    erros.add(e.getMessage());
               }
               
               try {
                    ValidacaoTemplate.validaCidade(funcionarioAtualizacaoRequest.getEndereco().getCidade());
               } catch (ConteudoExcelException e) {
                    erros.add(e.getMessage());
               }
               
          }
          
          return erros;
     }
     
     public Funcionario validaSePermiteAtualizacaoFuncionario(Long id, FuncionarioAtualizacaoRequest funcionarioAtualizacaoRequest) {
          
          List<Integer> idsEmpresas = gerenciadorAcessoService.validaIdentificacaoEObtemIdsEmpresas(funcionarioAtualizacaoRequest.getIdentificacao());
          
          Funcionario funcionario = funcionarioService.buscaFuncionarioPorId(id);
          
          boolean isNaoPossuiAcesso = idsEmpresas.stream()
                    .filter(idEmpresa -> idEmpresa.equals(funcionario.getIdEmpresa().intValue()))
                    .findFirst().map(p -> {
                         return false;
                     }).orElseGet(() -> {
                          return true;
                     });
          
          ForbiddenCdt.checkThrow(isNaoPossuiAcesso, USUARIO_SEM_PERMISSAO_ATUALIZAR_FUNCIONARIO);
          
          return funcionario;
     }

}
