
package br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoSubGrupoResponse;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;

@Service
public class HierarquiaAcessoSubgrupoStrategy extends HierarquiaAcessoStrategy {

     private static String NIVEL_PERMISSAO_ACESSO = "nivelPermissao";

     private static final String ACESSOS = "acessos";

     @Override
     public List<UsuarioPermissaoNivelAcessoRh> retornaAcessoPermitido(Map<String, Object> acessoUserLogado, Map<String, Object> acessoUserPretendido) {

          List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupoUserPretendido = getAcessos(acessoUserPretendido);

          List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupoUserLogado = getAcessos(acessoUserLogado);

          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.GRUPO)) {
               return getAcessos(acessoUserPretendido);
          }

          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.EMPRESA)) {

               return filtraPorAcessoEmpresa(acessoSubgrupoUserPretendido, acessoSubgrupoUserLogado);
          }

          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.HIBRIDO)) {

               return filtraPorAcessoHibrido(acessoSubgrupoUserPretendido, acessoSubgrupoUserLogado);
          }

          return filtraPorEmpresa(acessoSubgrupoUserPretendido, acessoSubgrupoUserLogado);

     }

     public List<HierarquiaAcessoSubGrupoResponse> mapHierarquiaSubGrupo(List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupo) {

          List<HierarquiaAcessoSubGrupoResponse> listNivelSubGrupos = new ArrayList<>();

          if (acessoSubgrupo.isEmpty()) {
               return Collections.emptyList();
          }

          acessoSubgrupo.forEach(action -> {
               listNivelSubGrupos.add(HierarquiaAcessoSubGrupoResponse.builder().idSubgrupo(action.getIdSubgrupoEmpresa()).nomeDescricao(action.getSubgrupo().getNomeSubGrupo()).build());
          });

          return listNivelSubGrupos;

     }

     private List<UsuarioPermissaoNivelAcessoRh> getAcessos(Map<String, Object> acesso) {

          return (List<UsuarioPermissaoNivelAcessoRh>) acesso.get(ACESSOS);
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtraPorEmpresa(List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupoUserLogado) {

          List<Long> idSugrupoUserLogado = acessoSubgrupoUserLogado.stream().map(UsuarioPermissaoNivelAcessoRh::getIdSubgrupoEmpresa).collect(Collectors.toList());

          return acessoSubgrupoUserPretendido.stream().filter(actual -> idSugrupoUserLogado.contains(actual.getIdSubgrupoEmpresa())).collect(Collectors.toList());
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtraPorAcessoHibrido(List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupoUserLogado) {

          List<Long> idSubgrupoDaEmpresaUserLogado = acessoSubgrupoUserLogado.stream().map(nivel -> nivel.getEmpresa().getIdGrupoEmpresa()).collect(Collectors.toList());

          List<UsuarioPermissaoNivelAcessoRh> acessoPorEmpresaUsuarioLogado = acessoSubgrupoUserPretendido.stream().filter(subgrupo -> idSubgrupoDaEmpresaUserLogado.contains(subgrupo.getIdSubgrupoEmpresa())).collect(Collectors.toList());

          List<Long> idSugrupoUserLogado = acessoSubgrupoUserLogado.stream().map(UsuarioPermissaoNivelAcessoRh::getIdSubgrupoEmpresa).collect(Collectors.toList());

          List<UsuarioPermissaoNivelAcessoRh> acessoPorSubgrupoUsuarioLogado = acessoSubgrupoUserPretendido.stream().filter(actual -> idSugrupoUserLogado.contains(actual.getIdSubgrupoEmpresa())).collect(Collectors.toList());

          List<UsuarioPermissaoNivelAcessoRh> acessosPermitidos = mergeListAcessos(acessoPorEmpresaUsuarioLogado, acessoPorSubgrupoUsuarioLogado);

          return acessosPermitidos;
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtraPorAcessoEmpresa(List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupoUserLogado) {

          List<Long> idSugrupoEmpresaUserLogado = acessoSubgrupoUserLogado.stream().map(nivel -> nivel.getEmpresa().getIdGrupoEmpresa()).collect(Collectors.toList());

          return acessoSubgrupoUserPretendido.stream().filter(subgrupo -> idSugrupoEmpresaUserLogado.contains(subgrupo.getIdSubgrupoEmpresa())).collect(Collectors.toList());
     }

     private List<UsuarioPermissaoNivelAcessoRh> mergeListAcessos(List<UsuarioPermissaoNivelAcessoRh> listAcessoPrimaria, List<UsuarioPermissaoNivelAcessoRh> listAcessoSecundaria) {

          List<UsuarioPermissaoNivelAcessoRh> acessosPermitidos = new ArrayList<>();

          listAcessoPrimaria.forEach(nivel -> acessosPermitidos.add(nivel));

          listAcessoSecundaria.forEach(nivel -> acessosPermitidos.add(nivel));

          return acessosPermitidos.stream().distinct().collect(Collectors.toList());
     }

}
