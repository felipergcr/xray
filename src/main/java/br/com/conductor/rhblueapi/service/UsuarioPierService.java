package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.controleAcesso.domain.TipoAlteracaoUsuario.ALTERACAO;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.GLOBAL_RECURSO_NAO_ENCONTRADO;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.USUARIO_NAO_LOCALIZADO;
import static br.com.conductor.rhblueapi.enums.OperacaoSenhaEnum.ESQUECI_SENHA;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.join;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.controleAcesso.domain.PerfisAcessosUsuarios;
import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.domain.VinculosUsuariosPerfisAcessosUsuarios;
import br.com.conductor.rhblueapi.controleAcesso.repository.PerfisAcessosUsuariosRepository;
import br.com.conductor.rhblueapi.controleAcesso.repository.UsuarioRepository;
import br.com.conductor.rhblueapi.controleAcesso.repository.VinculosUsuariosPerfisAcessosUsuariosRepository;
import br.com.conductor.rhblueapi.controleAcesso.tasks.LogUsuarioTask;
import br.com.conductor.rhblueapi.domain.NotificacaoEmail;
import br.com.conductor.rhblueapi.domain.UserToken;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.request.AlterarSenhaRequestPier;
import br.com.conductor.rhblueapi.domain.request.UsuarioAcessoRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioPierRest;
import br.com.conductor.rhblueapi.domain.request.UsuarioRequestBlue;
import br.com.conductor.rhblueapi.domain.request.UsuarioRequestPier;
import br.com.conductor.rhblueapi.domain.request.UsuarioSenhaTokenUpdateRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioSenhaUpdateRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioValidarSenhaLoginRequest;
import br.com.conductor.rhblueapi.domain.response.AlterarSenhaResponse;
import br.com.conductor.rhblueapi.enums.MessagesGenericRh;
import br.com.conductor.rhblueapi.enums.Plataforma;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.ExceptionCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.mongo.UserTokenRepository;
import br.com.conductor.rhblueapi.repository.pier.NotificacaoEmailPierRest;
import br.com.twsoftware.alfred.cpf.CPF;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UsuarioPierService {

     @Autowired
     private UsuarioPierRest usuarioPierRest;

     @Autowired
     private UserTokenRepository repositoryToken;

     @Autowired
     private UsuarioRepository usuarioRepository;

     @Autowired
     private LogUsuarioTask logUsuarioTask;

     @Autowired
     private NotificacaoEmailPierRest sendMail;
     
     @Value("${app.pier.notificacao.template.rh.id}")
     private Long idTemplateNotificacao;

     @Value("${app.pier.notificacao.template.rh.parametros.header.img}")
     private String header_img;

     @Value("${app.pier.notificacao.template.rh.parametros.company.name}")
     private String company_name;

     @Value("${app.pier.notificacao.template.rh.parametros.registry.link.criar}")
     private String registry_link;

     @Value("${app.pier.notificacao.template.rh.recuperarSenha.parametros.registry.link}")
     private String registry_link_alterar_senha;

     @Value("${app.pier.notificacao.template.rh.parametros.footer.img}")
     private String footer_img;

     @Value("${app.plataforma.rh.cod}")
     private int rh;

     @Value("${app.pier.notificacao.template.rh.recuperarSenha.parametros.header.img}")
     private String rhRecuperarSenhaHeaderImg;

     @Value("${app.pier.notificacao.template.rh.recuperarSenha.id}")
     private Long idTemplateRecuperarSenhaRh;

     @Value("${app.pier.notificacao.template.rh.recuperarSenha.parametros.footer.img}")
     private String rhRecuperarSenhaFooterImg;

     private final String TOKEN_PARAM = "?token=";

     @Autowired
     private VinculosUsuariosPerfisAcessosUsuariosRepository VinculosUsuariosPerfisAcessosUsuariosRepository;

     @Autowired
     private PerfisAcessosUsuariosRepository perfisAcessosUsuariosRepository;

     public UsuarioResponse criaUsuario(UsuarioRequestBlue usuarioBlue) throws JsonParseException, JsonMappingException, IOException {

		return criarUsuario(usuarioBlue, true);
	}

          /**
           * Gera o token com UUID. Realiza a criação de um usuário através de chamada ao Pier. Se enviar e-mail, então o token é enviado para o usuário definir a senha.
           * @param usuarioBlue - contém todos os dados para a criação do usuário.
           * @param enviarEmail - se true, realiza o envio de e-mail para o que está definido em usuarioBlue.email
           * @param gerarSenha - true: gera uma senha aleatoria. false: ustiliza o que está definido em usuarioBlue.senha.
           * @return usuário criado.
           */
     public UsuarioResponse criarUsuario(UsuarioRequestBlue usuarioBlue, boolean gerarSenha) {

          // verificaPlataforma
          BadRequestCdt.checkThrow(Objects.isNull(usuarioBlue.getPlataforma()), ExceptionsMessagesCdtEnum.PLATAFORMA_INVALIDA);
          BadRequestCdt.checkThrow(!usuarioBlue.getPlataforma().name().equalsIgnoreCase(Plataforma.RH.name()), ExceptionsMessagesCdtEnum.PLATAFORMA_INVALIDA);
          
          // preparar usuário para enviar ao Pier
          UsuarioRequestPier usuarioPier = UsuarioRequestPier.builder()
                    .login(usuarioBlue.getCpf())
                    .plataforma(mapPlataforma().get(usuarioBlue.getPlataforma().name()))
                    .email(usuarioBlue.getEmail())
                    .nome(usuarioBlue.getNome())
                    .cpf(usuarioBlue.getCpf())
                    .senha(gerarSenha ? String.valueOf(geraSenha()) : usuarioBlue.getSenha())
                    .build();

          // solicita ao Pier a criação do usupario
          UsuarioResponse response = usuarioPierRest.criaUsuario(usuarioPier);
          
          // Prepara um token para ser gravado no mongo e enviado por e-mail
          String accessToken = UUID.randomUUID().toString();
          
          // gravar no MongoDB o novo usuário e respectivo token
          try {
               salvarTokenUsuario(response.getId(), response.getLogin(), accessToken, usuarioPier.getSenha());
          } catch (Exception e) {
               log.error("[rh-api][UsuarioPierRest][criarUsuario] Erro ao gravar dados do usuario criado no MongoDB.", e);
               // a operação é cancelada poi se senviar o e-mail, a api não terá como validar o token posteriormente.
               throw new BadRequestCdt(MessagesGenericRh.ERRO_ENVIO_EMAIL.getMessage());
          }
          
          // tenta enviar e-mail
          try {
               NotificacaoEmail notificacaoEmail = montarEmailUsuario(response.getId(), usuarioBlue, idTemplateNotificacao, accessToken);
     
               if (sendMail.enviaEmail(notificacaoEmail) != HttpStatus.OK) {
                    throw new BadRequestCdt(MessagesGenericRh.ERRO_ENVIO_EMAIL.getMessage());
               }
          } catch (Exception e) {
               log.error("[rh-api][UsuarioPierRest][criarUsuario] Erro ao enviar e-mail.", e);
               throw new BadRequestCdt(MessagesGenericRh.ERRO_ENVIO_EMAIL.getMessage());
          }

          return response;
     }


     public HttpStatus alteraSenha(String token, UsuarioSenhaTokenUpdateRequest novaSenha) throws JsonParseException, JsonMappingException, IOException {

          UserToken userToken = this.repositoryToken.findByToken(token);

          if (ObjectUtils.allNotNull(userToken) && userToken.getStatusToken()) {
               HttpStatus response = usuarioPierRest.alterarSenha(AlterarSenhaRequestPier.builder().senhaAtual(String.valueOf(userToken.getSenhaPadrao())).id(userToken.getUserId()).login(userToken.getLogin()).senhaNova(novaSenha.getNovaSenha()).build());

               if (Objects.equals(response, HttpStatus.OK)) {
                    userToken.setStatusToken(false);
                    repositoryToken.save(userToken);
                    return response;
               }
          }
          throw new BadRequestCdt(MessagesGenericRh.TOKEN_INVALIDO.getMessage());
     }

     public AlterarSenhaResponse alterarSenha(final String operacao, final UsuarioSenhaUpdateRequest senha) throws JsonParseException, JsonMappingException, IOException {

          AlterarSenhaResponse response = AlterarSenhaResponse.builder().build();

          if (ESQUECI_SENHA.getDescricao().equals(operacao)) {
               response = recuperarSenha(senha);
          } else {
               throw new BadRequestCdt(MessagesGenericRh.PARAM_OPERACAO_INVALIDO.getMessage());
          }

          return response;
     }

     private AlterarSenhaResponse recuperarSenha(UsuarioSenhaUpdateRequest senha) throws JsonParseException, JsonMappingException, IOException {

          validarDadosLogin(senha);

          AlterarSenhaResponse response = null;

          NotificacaoEmail notificacaoEmail = new NotificacaoEmail();


		UsuarioResponse userResponse = usuarioPierRest.recuperarUsuario(senha.getCpf(), mapPlataforma().get(senha.getPlataforma().name()));
		String accessToken = UUID.randomUUID().toString();;

		salvarTokenUsuario(userResponse.getId(), userResponse.getLogin(),accessToken, "");

		notificacaoEmail = montarEmailRecuperarSenhaRH(userResponse.getNome(), userResponse.getEmail(), accessToken);

		response = AlterarSenhaResponse.builder().email(mascararEmail(userResponse.getEmail())).build();
          

          if (response == null) {
               throw new NotFoundCdt(MessagesGenericRh.USUARIO_INEXISTENTE.getMessage());
          }

          if (sendMail.enviaEmail(notificacaoEmail) != HttpStatus.OK) {
               throw new BadRequestCdt(MessagesGenericRh.ERRO_ENVIO_EMAIL.getMessage());
          }

          return response;
     }

     private void salvarTokenUsuario(Long userId, String login, String accessToken, String senhaPadrao) {

          repositoryToken.save(UserToken.builder().userId(userId).login(login).senhaPadrao(senhaPadrao).token(accessToken).statusToken(true).build());
     }

     private void validarDadosLogin(UsuarioSenhaUpdateRequest senha) {

          if (Objects.isNull(senha.getPlataforma())) {
               throw new BadRequestCdt(MessagesGenericRh.PLATAFORM_INVALIDO.getMessage());
          }

          if (StringUtils.equals(senha.getPlataforma().name(), Plataforma.RH.name())) {
               if (StringUtils.isBlank(senha.getCpf())) {
                    throw new BadRequestCdt(MessagesGenericRh.CPF_INEXISTENTE.getMessage());
               }
               if (!CPF.isValido(senha.getCpf())) {
                    throw new BadRequestCdt(MessagesGenericRh.INVALID_CPF.getMessage());
               }
          }

     }

     public UsuarioResponse alterarUsuarioAcesso(final Long id, final UsuarioAcessoRequest usuarioAcesso) {

          final Optional<Usuario> optUsuario = this.usuarioRepository.findById(id);
          ExceptionCdt.checkThrow(!optUsuario.isPresent(), GLOBAL_RECURSO_NAO_ENCONTRADO);

          final Usuario actualUsuario = optUsuario.get();
          mapUsuario(usuarioAcesso, actualUsuario);

          final Usuario usuarioAtualizado = this.usuarioRepository.saveAndFlush(actualUsuario);

          this.logUsuarioTask.logUsuario(actualUsuario, ALTERACAO);

          UsuarioResponse response = new UsuarioResponse();
          BeanUtils.copyProperties(usuarioAtualizado, response);
          return response;
     }

     private void mapUsuario(final UsuarioAcessoRequest usuarioAcesso, final Usuario actualUsuario) {

          if (isNotEmpty(usuarioAcesso.getLogin())) {
               actualUsuario.setLogin(usuarioAcesso.getLogin());
          }

          if (isNotEmpty(usuarioAcesso.getEmail())) {
               actualUsuario.setEmail(usuarioAcesso.getEmail());
          }

          if (usuarioAcesso.getStatus() != null) {
               actualUsuario.setStatus(usuarioAcesso.getStatus());
          }

          if (usuarioAcesso.getTentativasIncorretas() != null) {
               actualUsuario.setTentativasIncorretas(usuarioAcesso.getTentativasIncorretas());
          }

          if (usuarioAcesso.getBloquearAcesso() != null) {
               actualUsuario.setBloquearAcesso(usuarioAcesso.getBloquearAcesso());
          }
          actualUsuario.setDataUltimaAtualizacao(LocalDateTime.now());
     }

     private NotificacaoEmail montarEmailRecuperarSenhaRH(final String nomeUsuario, final String email, String token) {

          Map<String, Object> parametros = new HashMap<>();
          parametros.put("header_img", rhRecuperarSenhaHeaderImg);
          parametros.put("user_name", nomeUsuario);
          parametros.put("registry_link", registry_link_alterar_senha + TOKEN_PARAM + token);
          parametros.put("footer_img", rhRecuperarSenhaFooterImg);

          return NotificacaoEmail.builder().destinatarios(Arrays.asList(email)).idTemplateNotificacao(idTemplateRecuperarSenhaRh).parametrosConteudo(parametros).build();
     }

     private NotificacaoEmail montarEmailUsuario(Long idUsuario, UsuarioRequestBlue usuario, Long idTemplateNotificacao,  String token) {

          Map<String, Object> parametros = new HashMap<>();
          parametros.put("header_img", header_img);
          parametros.put("user_name", usuario.getNome());
          // definindo de onde iremos pegar nome da empresa;
          parametros.put("company_name", company_name);
          parametros.put("registry_link", join(registry_link, TOKEN_PARAM, token));
          parametros.put("footer_img", footer_img);

          return NotificacaoEmail.builder().destinatarios(Arrays.asList(usuario.getEmail())).idTemplateNotificacao(idTemplateNotificacao).parametrosConteudo(parametros).build();
     }


     @SuppressWarnings("unused")
     private void salvarTokenUsuario(Long id, String accessToken, String senhaPadrao) {

          repositoryToken.save(UserToken.builder().userId(id).senhaPadrao(senhaPadrao).token(accessToken).statusToken(true).build());
     }

     public Map<String, Integer> mapPlataforma() {

          Map<String, Integer> mapPlataforma = new HashMap<>();
          mapPlataforma.put(Plataforma.RH.name(), rh);
          return mapPlataforma;
     }

     private int geraSenha() {

          Random geraSenha = new Random();
          int senha = geraSenha.nextInt(50);
          return senha;
     }

     @SuppressWarnings("null")
     public UsuarioResponse validarSenhaLogin(String login, @Valid UsuarioValidarSenhaLoginRequest usuario) throws JsonParseException, JsonMappingException, IOException {

          String idPlataforma = mapPlataforma().get(usuario.getPlataforma().name()).toString();

          usuarioPierRest.validarSenhaLogin(login, usuario, idPlataforma);

          Usuario user = usuarioRepository.findByEmailAndIdPlataforma(login, Long.valueOf(idPlataforma));
          BadRequestCdt.checkThrow(Objects.isNull(user), ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO);

          UsuarioResponse response = new UsuarioResponse();
          BeanUtils.copyProperties(user, response);

          List<VinculosUsuariosPerfisAcessosUsuarios> idsVinculos = this.VinculosUsuariosPerfisAcessosUsuariosRepository.findByIdUsuario(user.getId());

          if (Objects.nonNull(idsVinculos)) {

               List<Long> idsPerfis = new ArrayList<>();

               for (int i = 0; i < idsVinculos.size(); i++) {
                    idsPerfis.add(idsVinculos.get(i).getIdPerfilAcessoUsuario());
               }

               List<PerfisAcessosUsuarios> listaPerfisUsuarioAcesso = this.perfisAcessosUsuariosRepository.findAllById(idsPerfis);

               response.setIdsPerfis(listaPerfisUsuarioAcesso);
          }

          return response;
     }

     private String mascararEmail(final String email) {

          if (StringUtils.isBlank(email)) {
               return email;
          }

          final StringBuffer emailMascarado = new StringBuffer();
          int index = email.indexOf("@");
          String subEmail = email.substring(0, index);

          if (index > 2) {
               emailMascarado.append(StringUtils.leftPad(subEmail.substring(index - 2), subEmail.length(), "x"));
          } else {
               emailMascarado.append(StringUtils.leftPad("", subEmail.length(), "x"));
          }

          emailMascarado.append(email.substring(index, email.length()));

          return emailMascarado.toString();
     }

     public UsuarioResponse recuperarUsuario(final String cpf, final Integer idPlataforma) {

          BadRequestCdt.checkThrow(Objects.isNull(idPlataforma), ExceptionsMessagesCdtEnum.PLATAFORMA_INVALIDA);

          final Optional<Usuario> usuario = usuarioRepository.findByCpfAndIdPlataforma(cpf, idPlataforma.longValue());

          if (!usuario.isPresent()) {
               return null;
          }

          final UsuarioResponse response = new UsuarioResponse();
          BeanUtils.copyProperties(usuario.get(), response);

          return response;
     }

     public Optional<Usuario> buscarPorId(final Long idUsuario) {

          return usuarioRepository.findByIdAndIdPlataforma(idUsuario, rh);
     }

     public Optional<Usuario> autenticarUsuario(Long idUsuario) {

          Optional<Usuario> optUsuario = buscarPorId(idUsuario);

          NotFoundCdt.checkThrow(!optUsuario.isPresent(), USUARIO_NAO_LOCALIZADO);

          return optUsuario;
     }

     public Optional<Long> isIdUsuarioSalvo(Long idUsuario) {

          Optional<Usuario> optUsuario = buscarPorId(idUsuario);

          return Optional.ofNullable(optUsuario.isPresent() ? optUsuario.get().getId() : null);
     }

}
