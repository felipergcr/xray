package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.PARAMETROS_GRUPO_EMPRESA_NAO_ENCONTRADO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.FORMA_PAGAMENTO;
import static br.com.conductor.rhblueapi.util.AppConstantes.PARAMETRO_RH;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;

import br.com.conductor.rhblueapi.domain.Parametro;
import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.enums.TipoPagamentoPedidoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.FormasPagamento;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.repository.ParametroGrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.ParametroRepository;
import br.com.conductor.rhblueapi.repository.ParametrosGruposEmpresasCustomRepositoryImpl;
import br.com.conductor.rhblueapi.util.AppConstantes;
import br.com.conductor.rhblueapi.util.DataUtils;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class ParametrosService {
     
     @Autowired
     private ParametroGrupoEmpresaRepository parametroGrupoEmpresaRepository;
     
     @Autowired
     private ParametroRepository parametroRepository;

     @Autowired
     private ParametrosGruposEmpresasCustomRepositoryImpl parametrosGruposEmpresasCustomRepository;
     
     public List<ParametroGrupoEmpresa> buscaResultadoParametrosGrupoEmpresaPor(Long idGrupoEmpresaPai, String tipoParametro, String codigo) {
          List<ParametroGrupoEmpresa> parametrosGruposEmpresas = parametrosGruposEmpresasCustomRepository.findByIdGrupoEmpresaAndTipoParametroAndCodigo(idGrupoEmpresaPai, tipoParametro, codigo);
          NotFoundCdt.checkThrow(CollectionUtils.isEmpty(parametrosGruposEmpresas), ExceptionsMessagesCdtEnum.GLOBAL_RECURSO_NAO_ENCONTRADO);
          return parametrosGruposEmpresas;
     }
     
     public ParametroGrupoEmpresa buscaParametrosGrupoEmpresaPor(Long idGrupoEmpresaPai, String tipoParametro, ParametrosEnum parametrosCodigo) {
          ParametroGrupoEmpresa parametrosGruposEmpresas = this.buscaResultadoParametrosGrupoEmpresaPor(idGrupoEmpresaPai, tipoParametro, parametrosCodigo.getValor()).stream().findFirst().get();
          return parametrosGruposEmpresas;
     }
     
     public ParametroGrupoEmpresa cadastraParametro(Long idGrupo, String tipoParametro, String valor) {

          Optional<ParametroGrupoEmpresa> parametroLocalizado = parametroGrupoEmpresaRepository.findByIdGrupoEmpresaAndParametro_IdParametro(idGrupo, consultaParametroRh(tipoParametro));
          PreconditionCustom.checkThrow(!parametroLocalizado.isPresent(), ExceptionsMessagesCdtEnum.PARAMETRO_GRUPO_NAO_ENCONTRADO);
          ParametroGrupoEmpresa parametroPersit = parametroLocalizado.get();
          parametroPersit.setValor(valor);
          return parametroGrupoEmpresaRepository.save(parametroPersit);
     }
     
     public Long consultaParametroRh(String codigoParametro) {

          Optional<Parametro> parametro = parametroRepository.findByTipoParametroAndCodigo(AppConstantes.PARAMETROS_TIPO_RH,codigoParametro);
          BadRequestCdt.checkThrow(!parametro.isPresent(), ExceptionsMessagesCdtEnum.PARAMETROS_GRUPO_EMPRESA_NAO_CADASTRADO);
          return parametro.get().getIdParametro();
     }
     
     @Transactional
     public void deletarParametroPorIdGrupo(Long idGrupo) {
          parametroGrupoEmpresaRepository.deleteByIdGrupoEmpresa(idGrupo);
     }
     
     private List<ParametroGrupoEmpresa> buscarParametrosPorIdGrupoEmpresa(Long idGrupoEmpresa) {
          
          Optional<List<ParametroGrupoEmpresa>> optParametrosGrupoEmpresa = parametroGrupoEmpresaRepository.findByIdGrupoEmpresa(idGrupoEmpresa);
          
          NotFoundCdt.checkThrow(Boolean.FALSE.equals(optParametrosGrupoEmpresa.isPresent()), PARAMETROS_GRUPO_EMPRESA_NAO_ENCONTRADO);
          
          return optParametrosGrupoEmpresa.get();
     }
     
     private Map<String, String> montaMapParametros(List<ParametroGrupoEmpresa> parametros) {
          
          return Arrays.asList(ParametrosEnum.values()).stream()
                    .filter(parametro -> Boolean.TRUE.equals(parametro.isExibirParaFront()))
                    .collect(Collectors.toMap(ParametrosEnum::name, parametro -> buscaParametroEmListaPersistida(parametro, parametros)));
     }
     
     private String buscaParametroEmListaPersistida(ParametrosEnum parametroEnum, List<ParametroGrupoEmpresa> parametros) {
          
          String retorno = parametros.stream()
                    .filter(parametro -> StringUtils.equals(parametro.getParametro().getCodigo(), parametroEnum.getValor()))
                    .map(ParametroGrupoEmpresa::getValor)
                    .findFirst()
                    .orElse(StringUtils.EMPTY);

          return retorno;
     }
     
     public String listaParametrosPorIdGrupoEmpresa(Long idGrupoEmpresa) {
          
          List<ParametroGrupoEmpresa> parametros = this.buscarParametrosPorIdGrupoEmpresa(idGrupoEmpresa);

          return new Gson().toJson(this.montaMapParametros(parametros));
          
     }

     public TipoPagamentoPedidoEnum obterTipoPagamento(Long idGrupoEmpresa) {

          ParametroGrupoEmpresa parametroTipoPagamento = new ParametroGrupoEmpresa();
          try {
               parametroTipoPagamento = buscaParametrosGrupoEmpresaPor(idGrupoEmpresa, AppConstantes.PARAMETRO_RH, ParametrosEnum.TIPO_CONTRATO);
          } catch (NotFoundCdt n) {
               ExceptionsMessagesCdtEnum.PARAMETRO_TIPO_PAGAMENTO_NAO_ENCONTRADO.raise();
          }

          return parametroTipoPagamento.getValor().equals(TipoPagamentoPedidoEnum.POS.getNome()) ? TipoPagamentoPedidoEnum.POS : TipoPagamentoPedidoEnum.PRE;
     }

     public boolean isTipoPagamentoPrePago(final long idGrupoEmpresa) {

          return this.obterTipoPagamento(idGrupoEmpresa).equals(TipoPagamentoPedidoEnum.PRE) ? true : false;
     }

     public LocalDate obterDataVigenciaDoLimite(final long idGrupoEmpresa) {

          ParametroGrupoEmpresa parametroVigencia = new ParametroGrupoEmpresa();
          LocalDate dataVigencia = LocalDate.now();
          try {
               parametroVigencia = this.buscaParametrosGrupoEmpresaPor(idGrupoEmpresa, AppConstantes.PARAMETRO_RH, ParametrosEnum.VIGENCIA_LIMITE);
               dataVigencia = Objeto.isBlank(parametroVigencia.getValor()) ? dataVigencia : DataUtils.converteStringParaLocalDate(parametroVigencia.getValor());
          } catch (NotFoundCdt e) {
               ExceptionsMessagesCdtEnum.PARAMETRO_DATA_VIGENCIA_NAO_ENCONTRADO.raise();
          }
          return dataVigencia;
     }

     public FormasPagamento obterFormaPagamento(final long idGrupoEmpresa) {

          ParametroGrupoEmpresa parametroFormaPagamento = buscaParametrosGrupoEmpresaPor(idGrupoEmpresa, PARAMETRO_RH, FORMA_PAGAMENTO);

          return FormasPagamento.obterPorValorParametro(parametroFormaPagamento.getValor());
     }

}
