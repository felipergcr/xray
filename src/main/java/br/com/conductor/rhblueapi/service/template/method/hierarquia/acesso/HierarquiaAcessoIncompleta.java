package br.com.conductor.rhblueapi.service.template.method.hierarquia.acesso;

import java.io.IOException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.PermissoesRhPersist;
import br.com.conductor.rhblueapi.domain.persist.UsuarioNivelPermissaoRhPersist;
import br.com.conductor.rhblueapi.domain.persist.UsuarioPersist;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.response.PermissoesRhResponse;
import br.com.conductor.rhblueapi.exception.ExceptionCdt;
import br.com.conductor.rhblueapi.service.PermissoesUsuariosRhService;
import br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService;
import br.com.conductor.rhblueapi.service.UsuarioPierService;

@Component
public class HierarquiaAcessoIncompleta extends HierarquiaAcessoAbstract {
     
     @Value("${app.plataforma.rh.cod}")
     private Integer rh;
     
     @Autowired
     private UsuarioPierService usuarioPierService;
     
     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;
     
     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;
     
     @Override
     public void criarHierarquiaMethod(UsuarioPersist usuarioPersist) {
          
               UsuarioResponse usuarioResponse = usuarioPierService.recuperarUsuario(usuarioPersist.getCpf(), rh);
               
               ExceptionCdt.checkThrow(Objects.isNull(usuarioResponse), ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO);
               
               PermissoesRhPersist permissoesRhPersist = PermissoesRhPersist.builder()
                         .idGrupoEmpresa(usuarioPersist.getIdGrupoEmpresa())
                         .idUsuario(usuarioResponse.getId())
                         .idUsuarioRegistro(usuarioPersist.getIdUsuarioLogado())
                         .build();
               
               PermissoesRhResponse permissaoResponse = permissoesUsuariosRhService.salvar(permissoesRhPersist, true);
          
               UsuarioNivelPermissaoRhPersist  usuarioNivelPermissaoRhPersist = UsuarioNivelPermissaoRhPersist.builder()
                         .idNivelAcesso(usuarioPersist.getIdsNivelAcesso())
                         .idUsuarioRegistro(usuarioPersist.getIdUsuarioLogado())
                         .nivelAcesso(usuarioPersist.getNivelAcesso())
                         .build();
               
               usuarioNivelPermissaoRhService.salvarNivelPermissaoUsuario(usuarioNivelPermissaoRhPersist, permissaoResponse.getId());
               
     }
     
}
