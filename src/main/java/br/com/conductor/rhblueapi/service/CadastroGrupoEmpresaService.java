package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.util.DataUtils.localDateParaStringyyyyMMdd;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;
import br.com.conductor.rhblueapi.domain.ParametrosPersist;
import br.com.conductor.rhblueapi.domain.SubgrupoEmpresa;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.CondicoesComerciaisPersist;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.response.CadastroGrupoEmpresaResponse;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPedido;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoProduto;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.ParametroGrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.SubgrupoRepository;
import br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.factory.TipoContratoFactory;
import br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.strategy.TipoContratoStrategy;
import br.com.conductor.rhblueapi.util.ConstantesParametros;
import br.com.conductor.rhblueapi.util.GenericConvert;

@Service
public class CadastroGrupoEmpresaService {

     @Autowired
     private GrupoEmpresaRepository grupoEmpresaRepository;

     @Autowired
     private SubgrupoRepository subGrupoRepository;

     @Autowired
     private ParametrosService parametrosService;

     @Autowired
     private ParametroGrupoEmpresaRepository parametroGrupoEmpresaRepository;
     
     @Autowired
     private GrupoEmpresaProdutoService grupoEmpresaProdutoService;
     
     @Autowired
     private TipoContratoFactory tipoContratoFactory;

     @Transactional
     public CadastroGrupoEmpresaResponse salvar(GrupoEmpresaPersist persist) {

          GrupoEmpresa grupoEmpresa = cadastrarGrupo(persist);

          cadastrarParametrosGrupo(grupoEmpresa.getId(), persist.getCondicoes(), persist.getParametros());
          this.cadastrarParametroTipoProduto(grupoEmpresa.getId(), persist.getParametros().getTipoProduto());
          
          defineEmpresaMatriz(persist);

          SubgrupoEmpresa subGrupo = salvaSubgrupo(persist, grupoEmpresa);
          cadastrarParametrosGrupo(subGrupo.getId(), persist.getCondicoes(), persist.getParametros());

          persist.getEmpresaMatriz().setNivelPermissaoContato(NivelPermissaoEnum.GRUPO);

          return CadastroGrupoEmpresaResponse.builder()
                    .grupoEmpresa(grupoEmpresa)
                    .subGrupo(subGrupo)
                    .build();
     }

     private GrupoEmpresa cadastrarGrupo(GrupoEmpresaPersist persist) {

          GrupoEmpresa grupo = GenericConvert.convertModelMapper(persist, GrupoEmpresa.class);
          grupo.setDataCadastro(LocalDateTime.now());
          grupo = this.grupoEmpresaRepository.save(grupo);
          return grupo;
     }

     private void defineEmpresaMatriz(GrupoEmpresaPersist persist) {

          persist.getEmpresaMatriz().setFlagMatriz(1);
     }

     private SubgrupoEmpresa salvaSubgrupo(GrupoEmpresaPersist persist, GrupoEmpresa grupo) {

          SubgrupoEmpresa subGrupoPersist = new SubgrupoEmpresa();
          subGrupoPersist.setIdGrupoEmpresaPai(grupo.getId());
          subGrupoPersist.setNomeSubGrupo(persist.getNomeGrupo());
          subGrupoPersist.setDataCadastro(LocalDateTime.now());

          SubgrupoEmpresa subGrupoResponsePersist = this.subGrupoRepository.save(subGrupoPersist);
          return subGrupoResponsePersist;
     }

     public void cadastrarParametrosGrupo(Long idGrupo, CondicoesComerciaisPersist condicaoComercial, ParametrosPersist parametros) {
          
          parametros.setPrazoPagamento(this.obtemPrazoDePagamento(parametros)); 
          
          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TIPO_PEDIDO, TipoPedido.valueOf(parametros.getTipoPedido().toString()).getTipo().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TIPO_CONTRATO, String.valueOf(parametros.getTipoContrato()));

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TAXA_ADMIN, condicaoComercial.getTaxaAdministracao().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TAXA_DISP_CREDITO, condicaoComercial.getTaxaDisponibilizacaoCredito().toPlainString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TAXA_EMISSAO_CARTAO, condicaoComercial.getTaxaEmissaoCartao().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TAXA_REEMISSAO_CARTAO, condicaoComercial.getTaxaReemissaoCartao().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_PRAZO_PAGAMENTO, parametros.getPrazoPagamento().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_SEGMENTO_EMPRESA, parametros.getSegmentoEmpresa().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TRANSF_PRODUTOS, parametros.getTransferencia().name());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_VALOR_MINIMO_VA, condicaoComercial.getValorMinimoVA().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_VALOR_MINIMO_VR, condicaoComercial.getValorMinimoVR().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TAXA_ENTREGA, condicaoComercial.getTaxaEntrega().toString());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_EMAIL_NF, parametros.getEmailNotaFiscal());

          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TIPO_FATURAMENTO, TipoFaturamentoEnum.valueOf(parametros.getTipoFaturamento().toString()).getValor().toString());

          parametrosService.cadastraParametro(idGrupo, ParametrosEnum.TIPO_ENTREGA_CARTAO.getValor(), parametros.getTipoEntrega().getValor());
          
          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_TIPO_PAGAMENTO, parametros.getTipoPagamento().getValor());
          
          parametrosService.cadastraParametro(idGrupo, ConstantesParametros.RH_LIMITE_CREDITO, parametros.getLimiteCreditoDisponivel().toString());
          
          if(parametros.getTipoContrato().equals(TipoContratoEnum.POS)) 
               parametrosService.cadastraParametro(idGrupo, ConstantesParametros.VIGENCIA_LIMITE, localDateParaStringyyyyMMdd(parametros.getDataVigenciaLimite()));

          Optional<ParametroGrupoEmpresa> usaVAParamLocalizado = parametroGrupoEmpresaRepository.findByIdGrupoEmpresaAndParametro_IdParametro(idGrupo, parametrosService.consultaParametroRh(ParametrosEnum.USA_PRODUTO_VA.getValor()));

          PreconditionCustom.checkThrow(!usaVAParamLocalizado.isPresent(), ExceptionsMessagesCdtEnum.PARAMETRO_GRUPO_NAO_ENCONTRADO);
          
          ParametroGrupoEmpresa usaVAPersist = usaVAParamLocalizado.get();
          
          verificaTipoProdutoPersistVA(parametros.getTipoProduto(), usaVAPersist);
          
          if (StringUtils.isNotEmpty(usaVAPersist.getValor())) {
               parametroGrupoEmpresaRepository.save(usaVAPersist);
          }

          Optional<ParametroGrupoEmpresa> usaVRParamLocalizado = parametroGrupoEmpresaRepository.findByIdGrupoEmpresaAndParametro_IdParametro(idGrupo, parametrosService.consultaParametroRh(ParametrosEnum.USA_PRODUTO_VR.getValor()));

          PreconditionCustom.checkThrow(!usaVRParamLocalizado.isPresent(), ExceptionsMessagesCdtEnum.PARAMETRO_GRUPO_NAO_ENCONTRADO);
          
          ParametroGrupoEmpresa usaVRPersist = usaVRParamLocalizado.get();
         
          verificaTipoProdutoPersistVR(parametros.getTipoProduto(), usaVRPersist);
          
          if (StringUtils.isNotEmpty(usaVRPersist.getValor())) {
               parametroGrupoEmpresaRepository.save(usaVRPersist);
          }
          
     }
     private void verificaTipoProdutoPersistVA(List<TipoProduto> tipoProduto, ParametroGrupoEmpresa parametro) {

          tipoProduto.forEach(action -> {
               if (StringUtils.isNotEmpty(action.name()) && StringUtils.equals(action.name(), "VA")) {
                    parametro.setValor(String.valueOf(1));
               }
          });
     }

     private void verificaTipoProdutoPersistVR(List<TipoProduto> tipoProduto, ParametroGrupoEmpresa parametro) {

          tipoProduto.forEach(action -> {
               if (StringUtils.isNotEmpty(action.name()) && StringUtils.equals(action.name(), "VR")) {
                    parametro.setValor(String.valueOf(1));
               }
          });
     }
     
     private void cadastrarParametroTipoProduto(final Long idGrupoEmpresa, final List<TipoProduto> produtos) {

          grupoEmpresaProdutoService.salvarGrupoEmpresaProdutos(idGrupoEmpresa, produtos);
     }
     
     public Integer obtemPrazoDePagamento(ParametrosPersist parametros) {
          
          TipoContratoStrategy strategy = tipoContratoFactory.newStrategy(parametros.getTipoContrato());
          Integer diasPrazoPagamento = strategy.aplicaRegraPrazoPagamento(parametros.getPrazoPagamento());
          
          return diasPrazoPagamento;
     }

}
