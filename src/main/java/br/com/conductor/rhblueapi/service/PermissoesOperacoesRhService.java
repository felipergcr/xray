
package br.com.conductor.rhblueapi.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.transaction.Transactional;

import org.hibernate.cfg.beanvalidation.IntegrationException;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.repository.UsuarioRepository;
import br.com.conductor.rhblueapi.domain.OperacaoAcesso;
import br.com.conductor.rhblueapi.domain.PermissaoOperacaoRh;
import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;
import br.com.conductor.rhblueapi.domain.Status;
import br.com.conductor.rhblueapi.domain.StatusDescricoes;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.PermissoesOperacoesRhPersist;
import br.com.conductor.rhblueapi.domain.request.PermissoesOperacoesRhResquest;
import br.com.conductor.rhblueapi.domain.response.PagePermissaoOperacaoRhResponse;
import br.com.conductor.rhblueapi.domain.response.PermissaoOperacaoRhResponse;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.OperacaoAcessoRepository;
import br.com.conductor.rhblueapi.repository.PermissaoOperacaoRhRepository;
import br.com.conductor.rhblueapi.repository.PermissoesUsuariosRhRepository;
import br.com.conductor.rhblueapi.repository.StatusDescricoesRepository;
import br.com.conductor.rhblueapi.repository.StatusRepository;
import br.com.conductor.rhblueapi.repository.utils.PermissaoOperacaoRhMap;
import br.com.conductor.rhblueapi.util.GenericConvert;

@Service
public class PermissoesOperacoesRhService {

     @Autowired
     private PermissaoOperacaoRhRepository permissaoOperacaoRhRepository;

     @Autowired
     private UsuarioRepository usuarioRepository;

     @Autowired
     private StatusDescricoesRepository statusDescricaoRepository;

     @Autowired
     private PermissoesUsuariosRhRepository permissoesUsuariosRhRepository;

     @Autowired
     private StatusRepository statusRepository;

     @Autowired
     private OperacaoAcessoRepository operacaoAcessoRepository;

     private Long plataforma = 16L;

     public List<PermissaoOperacaoRhResponse> vincularPermissaoOperacao(PermissoesOperacoesRhPersist persist, Long id) {

          Optional<PermissoesUsuariosRh> permissaoUsuario = permissoesUsuariosRhRepository.findById(id);

          NotFoundCdt.checkThrow(!permissaoUsuario.isPresent(), ExceptionsMessagesCdtEnum.PERMISSAO_NAO_ENCONTRADA);

          Optional<Usuario> usuarioRegistro = usuarioRepository.findById(persist.getIdUsuarioRegistro());

          NotFoundCdt.checkThrow(!usuarioRegistro.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_REGISTRO_NAO_ENCONTRADO);

          for (Long idOperacao : persist.getIdOperacao()) {

               Optional<OperacaoAcesso> operacaoValida = operacaoAcessoRepository.findByIdAndIdPlataforma(idOperacao, plataforma);

               NotFoundCdt.checkThrow(!operacaoValida.isPresent(), ExceptionsMessagesCdtEnum.OPERACAO_INVALIDA);

          }

          Optional<PermissoesUsuariosRh> permissaoUsuarioRegistro = permissoesUsuariosRhRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(usuarioRegistro.get().getId(), permissaoUsuario.get().getGrupoEmpresa().getId(), 1);

          BadRequestCdt.checkThrow(!permissaoUsuarioRegistro.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          return geraPermissaoList(persist, id);

     }

     public List<PermissaoOperacaoRhResponse> vincularPermissaoOperacaoChamadaUsuarioPrincipal(PermissoesOperacoesRhPersist persist, Long id) {

          for (Long idOperacao : persist.getIdOperacao()) {

               Optional<OperacaoAcesso> operacaoValida = operacaoAcessoRepository.findByIdAndIdPlataforma(idOperacao, plataforma);

               NotFoundCdt.checkThrow(!operacaoValida.isPresent(), ExceptionsMessagesCdtEnum.OPERACAO_INVALIDA);

          }

          return geraPermissaoList(persist, id);

     }

     public PagePermissaoOperacaoRhResponse listarPermissoesOperacoesRh(Long id, PermissoesOperacoesRhResquest request, Integer page, Integer size) {

          Optional<PermissoesUsuariosRh> permissaoUsuario = permissoesUsuariosRhRepository.findById(id);

          NotFoundCdt.checkThrow(!permissaoUsuario.isPresent(), ExceptionsMessagesCdtEnum.PERMISSAO_NAO_ENCONTRADA);

          Pageable pageable = PageRequest.of(page, size);

          PermissaoOperacaoRh query = mapperPermissaoOperacaoList(request);

          query.setIdPermissao(id);

          Example<PermissaoOperacaoRh> example = Example.of(query);

          Page<PermissaoOperacaoRh> response = permissaoOperacaoRhRepository.findAll(example, pageable);

          NoContentCdt.checkThrow(!response.hasContent(), ExceptionsMessagesCdtEnum.PESQUISA_NAO_ENCONTRADA);

          List<PermissaoOperacaoRhResponse> convertList = new ArrayList<>();

          convertResponseStatus(convertList, response);

          PageImpl<PermissaoOperacaoRhResponse> pageConvert = new PageImpl<>(convertList, pageable, response.getTotalElements());

          PagePermissaoOperacaoRhResponse pageReponse = new PagePermissaoOperacaoRhResponse(GenericConvert.convertModelMapperToPageResponse(pageConvert, new TypeToken<List<PermissaoOperacaoRhResponse>>() {
          }.getType()));

          return pageReponse;
     }

     private void convertResponseStatus(List<PermissaoOperacaoRhResponse> convertList, Page<PermissaoOperacaoRh> response) {

          for (PermissaoOperacaoRh permissaoOperacaoRh : response) {

               PermissaoOperacaoRhResponse responseObj = new PermissaoOperacaoRhResponse();

               responseObj = GenericConvert.convertModelMapper(permissaoOperacaoRh, PermissaoOperacaoRhResponse.class);

               if (permissaoOperacaoRh.getStatus() == 0) {

                    responseObj.setPermissaoAtiva(false);

               } else {
                    responseObj.setPermissaoAtiva(true);
               }

               convertList.add(responseObj);
          }

     }

     private PermissaoOperacaoRh mapperPermissaoOperacaoList(PermissoesOperacoesRhResquest request) {

          PermissaoOperacaoRh permissaoOperacaoUpdate = new PermissaoOperacaoRh();

          if (Objects.nonNull(request.getStatus())) {

               Optional<Status> statusRetorno = statusRepository.findByDescricao("PERMISSOESOPERACOESRH");

               Optional<StatusDescricoes> statusDescRetorno = statusDescricaoRepository.findByCodigoStatusAndDescricao(statusRetorno.get().getCodigoStatus(), request.getStatus().toString());

               if (statusDescRetorno.isPresent()) {
                    permissaoOperacaoUpdate.setStatus(statusDescRetorno.get().getStatus().longValue());
               }

          } else {
               permissaoOperacaoUpdate.setStatus(null);
          }

          request.setStatus(null);
          PermissaoOperacaoRh permissaoOperacaoUpdateCovert = GenericConvert.convertModelMapper(request, PermissaoOperacaoRh.class);
          permissaoOperacaoUpdateCovert.setStatus(permissaoOperacaoUpdate.getStatus());

          return permissaoOperacaoUpdateCovert;
     }

     @Transactional
     private List<PermissaoOperacaoRh> salvarPermissaoOperacao(List<PermissaoOperacaoRh> permissao) {

          try {
               return permissaoOperacaoRhRepository.saveAll(permissao);
          } catch (Exception e) {
               throw new IntegrationException(" falha ao salvar permissão e operação " + e.getMessage());
          }
     }

     private List<PermissaoOperacaoRhResponse> geraPermissaoList(PermissoesOperacoesRhPersist persist, Long idPermissao) {

          PermissaoOperacaoRh permissaoOperacao = new PermissaoOperacaoRh();

          List<PermissaoOperacaoRh> permissaoOperacaoList = new ArrayList<>();

          List<PermissaoOperacaoRh> operacaoExistenteList = permissaoOperacaoRhRepository.findAllByIdPermissao(idPermissao);

          List<Long> idUsado = new ArrayList<>();

          for (PermissaoOperacaoRh permissaoOperacaoRh : operacaoExistenteList) {
               if (!persist.getIdOperacao().contains(permissaoOperacaoRh.getIdOperacao())) {

                    permissaoOperacaoRh.setDataRegistro(LocalDateTime.now());
                    permissaoOperacaoRh.setIdUsuarioRegistro(persist.getIdUsuarioRegistro());
                    permissaoOperacaoRh.setStatus(0l);

                    permissaoOperacaoList.add(permissaoOperacaoRh);
                    idUsado.add(permissaoOperacaoRh.getIdOperacao());

               }

               if (persist.getIdOperacao().contains(permissaoOperacaoRh.getIdOperacao()) && permissaoOperacaoRh.getStatus() == 1) {

                    permissaoOperacaoRh.setIdUsuarioRegistro(persist.getIdUsuarioRegistro());
                    permissaoOperacaoRh.setDataRegistro(LocalDateTime.now());

                    permissaoOperacaoList.add(permissaoOperacaoRh);
                    idUsado.add(permissaoOperacaoRh.getIdOperacao());

               }

               if (persist.getIdOperacao().contains(permissaoOperacaoRh.getIdOperacao()) && permissaoOperacaoRh.getStatus() == 0) {

                    permissaoOperacaoRh.setIdPermissaoOperacao(permissaoOperacaoRh.getIdPermissaoOperacao());
                    permissaoOperacaoRh.setIdPermissao(permissaoOperacaoRh.getIdPermissao());
                    permissaoOperacaoRh.setIdUsuarioRegistro(persist.getIdUsuarioRegistro());
                    permissaoOperacaoRh.setDataRegistro(LocalDateTime.now());
                    permissaoOperacaoRh.setStatus(1l);

                    permissaoOperacaoList.add(permissaoOperacaoRh);
                    idUsado.add(permissaoOperacaoRh.getIdOperacao());

               }

          }

          for (Long idOperacao : persist.getIdOperacao()) {

               if (!idUsado.contains(idOperacao)) {

                    permissaoOperacao = new PermissaoOperacaoRh();
                    permissaoOperacao.setIdOperacao(idOperacao);
                    permissaoOperacao.setIdUsuarioRegistro(persist.getIdUsuarioRegistro());
                    permissaoOperacao.setDataRegistro(LocalDateTime.now());
                    permissaoOperacao.setIdPermissao(idPermissao);
                    permissaoOperacao.setStatus(1L);

                    permissaoOperacaoList.add(permissaoOperacao);

               }

          }

          List<PermissaoOperacaoRhResponse> listaOrdenada = GenericConvert.convertWithMapping(salvarPermissaoOperacao(permissaoOperacaoList), new TypeToken<List<PermissaoOperacaoRhResponse>>() {
          }.getType(), new PermissaoOperacaoRhMap().response);

          listaOrdenada.sort(Comparator.comparing(PermissaoOperacaoRhResponse::getIdPermissaoOperacao).reversed());

          return listaOrdenada;

     }

}
