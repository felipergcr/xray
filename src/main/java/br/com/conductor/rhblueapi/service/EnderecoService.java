
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.ENDERECO_NAO_ENCONTRADO;
import static br.com.conductor.rhblueapi.enums.TipoEnderecoEnum.FUNCIONARIO;
import static org.apache.commons.lang.StringUtils.upperCase;

import java.time.LocalDateTime;
import java.util.Optional;

import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import br.com.conductor.rhblueapi.domain.Endereco;
import br.com.conductor.rhblueapi.domain.TipoEndereco;
import br.com.conductor.rhblueapi.domain.request.funcionario.EnderecoFuncionarioRequest;
import br.com.conductor.rhblueapi.domain.request.pessoafisica.EnderecoRequest;
import br.com.conductor.rhblueapi.enums.TipoEnderecoEnum;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.EnderecoRepository;
import br.com.conductor.rhblueapi.repository.cache.TipoEnderecoCacheRepository;

@Service
public class EnderecoService {
     
     @Autowired
     private EnderecoRepository enderecoRepository;
     
     @Autowired
     private TipoEnderecoCacheRepository tipoEnderecoCacheRepository;
     
     public Optional<Endereco> buscaEnderecoPorId(Long id) {
          
          return enderecoRepository.findById(id);
     }
     
     public Long persisteEobtemIdEndereco(Optional<Endereco> optEndereco, EnderecoRequest enderecoRequest, Long idPessoaFisica) {
          
          return optEndereco.isPresent() ? this.atualizarEnderecoFuncionario(enderecoRequest, optEndereco.get()).getId() : this.cadastraEnderecoFuncionario(enderecoRequest, idPessoaFisica).getId();  
     }
     
     private Endereco cadastraEnderecoFuncionario(EnderecoRequest enderecoRequest, Long idPessoaFisica) {
          
          Endereco endereco = this.cadastrar(enderecoRequest, idPessoaFisica, FUNCIONARIO);
          
          return endereco;
     }
     
     private Endereco cadastrar(EnderecoRequest enderecoRequest, Long idPessoaFisica, TipoEnderecoEnum tipoEndereco) {
          
          Endereco endereco = this.montaEndereco(enderecoRequest, idPessoaFisica, tipoEndereco);
          
          return enderecoRepository.save(endereco);
     }
     
     private Endereco atualizarEnderecoFuncionario(EnderecoRequest enderecoRequest, Endereco enderecoPersistido) {

          return this.atualizar(enderecoRequest, enderecoPersistido, this.buscaTipoEnderecoPorTipo(FUNCIONARIO));
     }
     
     private Endereco atualizar(EnderecoRequest enderecoRequest, Endereco enderecoPersistido, TipoEndereco tipoEndereco) {
          
          Endereco endereco = Endereco.builder().build();
          
          BeanUtils.copyProperties(enderecoRequest, endereco);
          
          if(this.isEqualsBusinessRules(endereco, enderecoPersistido))
               return enderecoPersistido;
          
          BeanUtils.copyProperties(enderecoRequest, enderecoPersistido, "id");
          
          enderecoPersistido.setDataAtualizacao(LocalDateTime.now());

          return enderecoRepository.save(enderecoPersistido);
     }
     
     private Endereco montaEndereco(EnderecoRequest enderecoRequest, Long idPessoaFisica, TipoEnderecoEnum tipoEndereco) {
          
          Endereco endereco = Endereco.builder().build();
          
          BeanUtils.copyProperties(enderecoRequest, endereco);
          
          endereco.setIdPessoaFisica(idPessoaFisica);
          endereco.setIdTipoEndereco(this.buscaTipoEnderecoPorTipo(tipoEndereco).getId().intValue());
          endereco.setDataInclusao(LocalDateTime.now());
          endereco.setDataAtualizacao(LocalDateTime.now());

          return endereco;
     }
     
     private TipoEndereco buscaTipoEnderecoPorTipo(TipoEnderecoEnum tipoEndereco) {
          
          return tipoEnderecoCacheRepository.findAll()
                    .stream()
                    .filter(te -> upperCase(te.getNome()).equals(upperCase(tipoEndereco.name())))
                    .findFirst()
                    .get();
     }
     
     public EnderecoRequest converte(ArquivosCargasDetalhes detalhe) {

          return EnderecoRequest.builder()
                    .logradouro(detalhe.getEndereco())
                    .numero(Integer.parseInt(detalhe.getNumero()))
                    .complemento(detalhe.getComplemento())
                    .bairro(detalhe.getBairro())
                    .cidade(detalhe.getCidade())
                    .cep(detalhe.getCep())
                    .uf(detalhe.getUf())
                    .build();
     }
     
     public EnderecoRequest converte(EnderecoFuncionarioRequest enderecoFuncionario) {
          
          return EnderecoRequest.builder()
                    .logradouro(enderecoFuncionario.getLogradouro())
                    .numero(enderecoFuncionario.getNumero())
                    .complemento(enderecoFuncionario.getComplemento())
                    .bairro(enderecoFuncionario.getBairro())
                    .cidade(enderecoFuncionario.getCidade())
                    .cep(enderecoFuncionario.getCep())
                    .uf(enderecoFuncionario.getUf().name())
                    .build();
     }

     private boolean isEqualsBusinessRules(Endereco source, Endereco target) {

          if (StringUtils.equals(source.getLogradouro(), target.getLogradouro()) && source.getNumero().equals(target.getNumero())
                    && StringUtils.equals(source.getComplemento(), target.getComplemento()) && StringUtils.equals(source.getBairro(), target.getBairro())
                    && StringUtils.equals(source.getCep(), target.getCep()) && StringUtils.equals(source.getCidade(), target.getCidade())
                    && StringUtils.equals(source.getUf(), target.getUf())
                    )
               return true;

          return false;
     }
     
     public EnderecoRequest atualizar(Long idEndereco, EnderecoRequest enderecoRequest) {

          Optional<Endereco> optEndereco = enderecoRepository.findById(idEndereco);
          
          NotFoundCdt.checkThrow(!optEndereco.isPresent(), ENDERECO_NAO_ENCONTRADO);
          
          Endereco endereco = this.atualizarEnderecoFuncionario(enderecoRequest, optEndereco.get());
          
          BeanUtils.copyProperties(endereco, enderecoRequest);
          
          return enderecoRequest;
     }

}
