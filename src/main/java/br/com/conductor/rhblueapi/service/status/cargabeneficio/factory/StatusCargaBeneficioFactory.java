
package br.com.conductor.rhblueapi.service.status.cargabeneficio.factory;

import br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaAguardandoPagamento;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaCargaEfetuada;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaExpirado;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaPedidoCancelado;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaPedidoConfirmado;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaPedidoProcessado;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaPendenciaLimite;
import br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy.StatusCargaBeneficioStrategy;

public class StatusCargaBeneficioFactory {

     public static StatusCargaBeneficioStrategy newStrategy(StatusCargaBeneficioEnum statusCargaBeneficioEnum) {

          StatusCargaBeneficioStrategy strategy = null;

          switch (statusCargaBeneficioEnum) {

               case PENDENCIA_LIMITE:
                    strategy = new StatusCargaPendenciaLimite();
                    break;
               case PEDIDO_PROCESSADO:
                    strategy = new StatusCargaPedidoProcessado();
                    break;
               case PEDIDO_CONFIRMADO:
                    strategy = new StatusCargaPedidoConfirmado();
                    break;
               case AGUARDANDO_PAGAMENTO:
                    strategy = new StatusCargaAguardandoPagamento();
                    break;
               case CARGA_EFETUADA:
                    strategy = new StatusCargaCargaEfetuada();
                    break;
               case PEDIDO_CANCELADO:
                    strategy = new StatusCargaPedidoCancelado();
                    break;
               case EXPIRADO:
                    strategy = new StatusCargaExpirado();
                    break;
               default:
                    break;
          }

          return strategy;
     }

}
