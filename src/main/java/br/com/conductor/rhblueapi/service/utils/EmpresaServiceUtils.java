
package br.com.conductor.rhblueapi.service.utils;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.Empresa;
import br.com.conductor.rhblueapi.domain.persist.PermissoesOperacoesRhPersist;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.request.OperacaoAcessoRequest;
import br.com.conductor.rhblueapi.domain.request.UsuarioPierRest;
import br.com.conductor.rhblueapi.domain.request.UsuarioRequestBlue;
import br.com.conductor.rhblueapi.domain.response.OperacaoAcessoResponse;
import br.com.conductor.rhblueapi.domain.response.PageOperacaoAcessoResponse;
import br.com.conductor.rhblueapi.domain.response.PermissaoOperacaoRhResponse;
import br.com.conductor.rhblueapi.domain.update.EmpresaUpdate;
import br.com.conductor.rhblueapi.enums.Plataforma;
import br.com.conductor.rhblueapi.service.OperacaoAcessoService;
import br.com.conductor.rhblueapi.service.PermissoesOperacoesRhService;
import br.com.conductor.rhblueapi.service.UsuarioPierService;



/**
 * EmpresaServiceUtils
 */
@Component
public class EmpresaServiceUtils {

     /**
      * usuarioRest
      */
     @Autowired
     private UsuarioPierRest usuarioRest;

     /**
      * usuarioService
      */
     @Autowired
     private UsuarioPierService usuarioService;

     /**
      * permissoesOperacoesRhService
      */
     @Autowired
     private PermissoesOperacoesRhService permissoesOperacoesRhService;

     @Autowired
     private OperacaoAcessoService operacaoAcessoService;
     
     private static final Long USUARIO_REGISTRO_PRIMEIRO_ACESSO = 0l;

     /**
      * validarUpdate Método responsável por verificar quais campos serão atualizados e substituir os valores
      *
      * @param empresa
      * @param empresaUpdate
      * @return
      */
     public static Empresa validarUpdate(Empresa empresa, EmpresaUpdate empresaUpdate) {

          if (Objects.nonNull(empresaUpdate.getDescricao()))
               empresa.setDescricao(empresaUpdate.getDescricao());
          if (Objects.nonNull(empresaUpdate.getNomeExibicao()))
               empresa.setNomeExibicao(empresaUpdate.getNomeExibicao());
          if (Objects.nonNull(empresaUpdate.getStatus()))
               empresa.setStatus(empresaUpdate.getStatus());
          if (Objects.nonNull(empresaUpdate.getFlagMatriz()))
               empresa.setFlagMatriz(empresaUpdate.getFlagMatriz());
          if (Objects.nonNull(empresaUpdate.getDataContrato()))
               empresa.setDataContrato(empresaUpdate.getDataContrato());
          if (Objects.nonNull(empresaUpdate.getNomeContato()))
               empresa.setNomeContato(empresaUpdate.getNomeContato());
          if (Objects.nonNull(empresaUpdate.getCpfContato()))
               empresa.setCpfContato(empresaUpdate.getCpfContato());
          if (Objects.nonNull(empresaUpdate.getDataNascimentoContato()))
               empresa.setDataNascimentoContato(empresaUpdate.getDataNascimentoContato());
          if (Objects.nonNull(empresaUpdate.getEmailContato()))
               empresa.setEmailContato(empresaUpdate.getEmailContato());
          if (Objects.nonNull(empresaUpdate.getDddTelContato()))
               empresa.setDddTelContato("0" + empresaUpdate.getDddTelContato());
          if (Objects.nonNull(empresaUpdate.getNumTelContato()))
               empresa.setNumTelContato(empresaUpdate.getNumTelContato());
          if (Objects.nonNull(empresaUpdate.getBanco()))
               empresa.setBanco(empresaUpdate.getBanco());
          if (Objects.nonNull(empresaUpdate.getAgencia()))
               empresa.setAgencia(empresaUpdate.getAgencia());
          if (Objects.nonNull(empresaUpdate.getContaCorrente()))
               empresa.setContaCorrente(Long.valueOf(empresaUpdate.getContaCorrente()));
          if (Objects.nonNull(empresaUpdate.getDvContaCorrente()))
               empresa.setDvContaCorrente(empresaUpdate.getDvContaCorrente());
          if (Objects.nonNull(empresaUpdate.getRazaoSocial()))
               empresa.getPessoa().setNome(empresaUpdate.getRazaoSocial());
          if (Objects.nonNull(empresaUpdate.getNomeFantasia()))
               empresa.setNomeFantasia(empresaUpdate.getNomeFantasia());

          return empresa;

     }

     /**
      * verificaCadastroUsuario
      * 
      * @param cpf
      * @param plataforma
      * @return
      */
     public List<UsuarioResponse> verificaCadastroUsuario(String cpf, Integer plataforma) {

          return usuarioRest.consultaUsuario(cpf, plataforma);
     }

     /**
      * criaUsuario
      * 
      * @param nome
      * @param cpf
      * @param cnpj
      * @param email
      * @param plataforma
      * @return
      * @throws IOException
      */
     public UsuarioResponse criaUsuario(String nome, String cpf, String cnpj, String email, Plataforma plataforma) throws IOException {

          return usuarioService.criaUsuario(UsuarioRequestBlue.builder().nome(nome).cpf(cpf).cnpj(cnpj).email(email).plataforma(plataforma).build());

     }

     public List<Long> listaOperacoes() {

          OperacaoAcessoRequest request = new OperacaoAcessoRequest();
          request.setPage(0);
          request.setSize(50);
          request.setIdPlataforma(16l);

          PageOperacaoAcessoResponse pageOperacoes = operacaoAcessoService.listar(request);

          List<OperacaoAcessoResponse> listOperacoes = pageOperacoes.getContent();

          List<Long> idOperacoesAcessos = listOperacoes.stream().map(OperacaoAcessoResponse::getId).collect(Collectors.toList());

          return idOperacoesAcessos;
     }

     public List<PermissaoOperacaoRhResponse>  criaPermissoesAcessoUsuarioPrincipal(List<Long> idOperacoesAcessos, Long idPermissao) {

          return permissoesOperacoesRhService.vincularPermissaoOperacaoChamadaUsuarioPrincipal(PermissoesOperacoesRhPersist.builder().IdOperacao(idOperacoesAcessos).idUsuarioRegistro(USUARIO_REGISTRO_PRIMEIRO_ACESSO).build(), idPermissao);
          
     }


}
