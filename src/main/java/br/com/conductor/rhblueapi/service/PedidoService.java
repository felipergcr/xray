
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.IMPORTADO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.INVALIDADO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.RECEBIDO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.VALIDADO_PARCIAL;
import static br.com.conductor.rhblueapi.enums.StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE;
import static br.com.conductor.rhblueapi.enums.StatusPagamentoEnum.LIMITE_CREDITO_VENCIDO;
import static br.com.conductor.rhblueapi.enums.StatusPermissaoEnum.ATIVO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS_STATUSPAGAMENTO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.PERMISSOES_USUARIOS_RH;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.FLAG_FATURAMENTO_CENTRALIZADO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.FLAG_PEDIDO_CENTRALIZADO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.TIPO_CONTRATO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.TIPO_ENTREGA_CARTAO;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.ARQUIVO_PEDIDO_QUANTIDADE_ABAS;
import static br.com.conductor.rhblueapi.util.AppConstantes.FRONT_RH;
import static br.com.conductor.rhblueapi.util.AppConstantes.MAIOR_INTERVALO_CANCELAMENTO_PEDIDO_45_DIAS;
import static br.com.conductor.rhblueapi.util.AppConstantes.MENOR_INTERVALO_CANCELAMENTO_PEDIDO_1_DIA;
import static br.com.conductor.rhblueapi.util.AppConstantes.PARAMETRO_RH;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioResumido;
import br.com.conductor.rhblueapi.controleAcesso.repository.UsuarioRepository;
import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.ArquivoCargasBinario;
import br.com.conductor.rhblueapi.domain.ArquivoCargasCustom;
import br.com.conductor.rhblueapi.domain.ArquivoPedidoDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.PedidoResponse;
import br.com.conductor.rhblueapi.domain.PedidoUpload;
import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.StatusDescricoes;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.pedido.CargaPedidoCustom;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.AcoesPedidoResponse;
import br.com.conductor.rhblueapi.domain.response.ArquivoPedidoResponse;
import br.com.conductor.rhblueapi.domain.response.PageArquivoCargasResponse;
import br.com.conductor.rhblueapi.domain.response.PageArquivoPedidoDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.enums.HeaderPedidoLoteEnum;
import br.com.conductor.rhblueapi.enums.HeaderPedidoPortaPortaEnum;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;
import br.com.conductor.rhblueapi.enums.StatusPagamentoEnum;
import br.com.conductor.rhblueapi.enums.TemplatesExcel;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoEntregaCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.BusinessException;
import br.com.conductor.rhblueapi.exception.ConversaoObjetoException;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.exception.ObjetoVazioException;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.repository.ArquivoCargasBinarioRepository;
import br.com.conductor.rhblueapi.repository.ArquivoCargasCustomRepositoryImpl;
import br.com.conductor.rhblueapi.repository.ArquivoCargasDetalhesCustomRepositoryImpl;
import br.com.conductor.rhblueapi.repository.ArquivoCargasRepository;
import br.com.conductor.rhblueapi.repository.CancelaPedidoCustomRepositoryImpl;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.PermissoesUsuariosRhRepository;
import br.com.conductor.rhblueapi.repository.PessoaCustomRepositoryImpl;
import br.com.conductor.rhblueapi.repository.StatusDescricoesRepositoryCustom;
import br.com.conductor.rhblueapi.repository.pier.UsuarioPierRestRepository;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.CargasPublish;
import br.com.conductor.rhblueapi.service.email.EmailFinalizaPedidoService;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.AppConstantes;
import br.com.conductor.rhblueapi.util.ArquivoUtils;
import br.com.conductor.rhblueapi.util.EntityGenericUtil;
import br.com.conductor.rhblueapi.util.GenericConvert;
import br.com.conductor.rhblueapi.util.PageUtils;
import br.com.conductor.rhblueapi.util.RhHeaderDefinition;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class PedidoService implements Serializable {

     private static final long serialVersionUID = 1L;

     @Autowired
     private ArquivoCargasBinarioRepository arquivoCargasBinarioRepository;

     @Autowired
     private PedidoDetalhadoService pedidoDetalhadoService;

     @Autowired
     private UsuarioPierRestRepository usuarioPierRestRepository;

     @Autowired
     private PessoaCustomRepositoryImpl pessoaCustomRepositoryImpl;

     @Autowired
     private PermissoesUsuariosRhRepository permissoesUsuariosRhRepository;

     @Autowired
     private StatusDescricoesRepositoryCustom statusDescricaoRepositoryCustom;

     @Autowired
     private UsuarioRepository usuarioRepository;

     @Autowired
     private ArquivoCargasRepository arquivoCargasRepository;

     @Autowired
     private ArquivoCargasCustomRepositoryImpl arquivoCargasRepositoryCustomImpl;

     @Autowired
     private GrupoEmpresaRepository grupoEmpresaRepository;

     @Autowired
     private UploadArquivoService uploadArquivoService;

     @Autowired
     private StatusCached statusCached;

     @Autowired
     private ParametrosService parametrosService;

     @Autowired
     private ArquivoCargasDetalhesCustomRepositoryImpl arquivoCargasDetalhesCustomRepositoryImpl;

     @Autowired
     private CancelaPedidoCustomRepositoryImpl cancelaPedidoCustomRepositoyImpl;

     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;

     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;

     @Autowired
     private UsuarioPierService usuarioPierService;

     @Autowired
     private CargaBeneficioService cargaBeneficioService;

     @Autowired
     private EmailFinalizaPedidoService emailFinalizaPedidoService;

     private static final int DATA_PROCESSAMENTO_LIMITE = 60 * 3;

     private List<StatusDescricao> dominioStatusCargas;

     private HashMap<Integer, StatusDescricoes> mapStatus = new HashMap<Integer, StatusDescricoes>();

     private HashMap<Integer, StatusDescricoes> mapStatusPagamento = new HashMap<Integer, StatusDescricoes>();

     private final String ARQUIVOCARGAS = "ARQUIVOCARGAS";

     @PersistenceContext
     private EntityManager entityManager;

     @Autowired
     private CargasPublish cargasPublish;

     public PedidoResponse uploadFile(IdentificacaoRequest usuarioGrupoResquest, MultipartFile file) {

          BadRequestCdt.checkThrow(!file.getOriginalFilename().endsWith(AppConstantes.FileExtension.XLSX) && !file.getOriginalFilename().endsWith(AppConstantes.FileExtension.XLS), ExceptionsMessagesCdtEnum.PLANILHA_EXTENSAO);

          UsuarioResponse usuario = usuarioPierRestRepository.consultarUsuario(usuarioGrupoResquest.getIdUsuario());
          BadRequestCdt.checkThrow(Objects.isNull(usuario), ExceptionsMessagesCdtEnum.USUARIO_INEXISTENTE);

          Optional<GrupoEmpresa> GrupoEmpresa = grupoEmpresaRepository.findById(usuarioGrupoResquest.getIdGrupoEmpresa());
          BadRequestCdt.checkThrow(!GrupoEmpresa.isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_ENCONTRADO);

          ArquivoCargasBinario arquivoCargasBinario;

          String parametroTipoEntrega = buscarParametroTipoEntrega(usuarioGrupoResquest.getIdGrupoEmpresa());

          try {
               Workbook workbook = uploadArquivoService.lerPlanilha(file);

               final List<RhHeaderDefinition> cabecalho = isTipoEntregaLote(parametroTipoEntrega)? Arrays.asList(HeaderPedidoLoteEnum.values()): Arrays.asList(HeaderPedidoPortaPortaEnum.values());

               validarCabecalho(workbook, cabecalho, parametroTipoEntrega);

               arquivoCargasBinario = salvarPlanilha(usuarioGrupoResquest, file);

               workbook.close();

          } catch (Exception e) {
               throw new BadRequestCdt(e.getMessage());
          }

          PedidoUpload pedidoUpload = PedidoUpload.builder().idArquivoCargas(arquivoCargasBinario.getArquivoCargas().getId()).tipoEntregaCartao(parametroTipoEntrega).build();

          cargasPublish.enviarArquivoPedidoImportacao(pedidoUpload);

          return PedidoResponse.builder().numeroPedido(pedidoUpload.getIdArquivoCargas()).build();
     }

     public boolean isTipoEntregaLote(String tipoEntrega){

          Optional<String> tipo =  Optional.ofNullable(tipoEntrega);

          if(tipo.isPresent()){
               return tipo.get().equals(TipoEntregaCartao.RH.getValor());
          }
          return false;
     }

     public String buscarParametroTipoEntrega(Long idGrupoEmpresa) {

          String parametro = null;
          try {
               parametro = parametrosService.buscaParametrosGrupoEmpresaPor(idGrupoEmpresa, PARAMETRO_RH, TIPO_ENTREGA_CARTAO).getValor();
          } catch (NotFoundCdt e) {
               ExceptionsMessagesCdtEnum.PARAMETRO_GRUPO_NAO_ENCONTRADO.raise();
          }
          return parametro;
     }

     private void validarCabecalho(Workbook workbook, List<RhHeaderDefinition> cabecalho, String parametroTipoEntrega) {

          Sheet sheet = workbook.getSheetAt(0);
          Row linhaCabecalho = sheet.getRow(5);

          validaQtdAbas(workbook.getNumberOfSheets());
          
          // Se tiver mais que uma ABA e Cabecalho Vazio
          BadRequestCdt.checkThrow(Objects.isNull(linhaCabecalho) || !this.isTamanhoHeaderEsperado(linhaCabecalho, cabecalho.size()), ExceptionsMessagesCdtEnum.PLANILHA_INVALIDA);

          // Se o cabeçalho contem os campos padrões
          Iterator<Cell> cellIterator = linhaCabecalho.cellIterator();

          while (cellIterator.hasNext()) {
               Cell cell = cellIterator.next();
               String header = EntityGenericUtil.removeAccents(cell.getStringCellValue().trim().toUpperCase());

               BadRequestCdt.checkThrow(cabecalho.stream().noneMatch(str -> str.getHeader().equals(header)), ExceptionsMessagesCdtEnum.PLANILHA_INVALIDA);
          }

          TemplatesExcel template = isTipoEntregaLote(parametroTipoEntrega)? TemplatesExcel.ARQUIVO_CARGA_LOTE: TemplatesExcel.ARQUIVO_CARGA_PORTA_A_PORTA;
          Row linha = sheet.getRow(template.getLinhaInicioConteudo());

          List<String> listaErros = new ArrayList<>();

          cabecalho.stream().forEach(item -> {
               if(item.isObrigatorio()) {
                    if(Objects.isNull(linha.getCell(item.getPosicaoCelula())) || Objeto.isBlank(linha.getCell(item.getPosicaoCelula()).toString())) {
                         listaErros.add(String.format("%s obrigátorio/a", item.getHeader()));
                    }
               }
          });

          if(CollectionUtils.isNotEmpty(listaErros)) {
               throw new BadRequestCdt(listaErros.toString());
          }
     }

     private void validaQtdAbas(final Integer qtdAbas) {

          BadRequestCdt.checkThrow(qtdAbas != ARQUIVO_PEDIDO_QUANTIDADE_ABAS, ExceptionsMessagesCdtEnum.ARQUIVO_SOMENTE_UMA_ABA);
     }

     private boolean isTamanhoHeaderEsperado(Row linhaCabecalho, int expectedSize){
          if(linhaCabecalho.getPhysicalNumberOfCells() != expectedSize){
               return false;
          }
          return true;
     }

     private ArquivoCargasBinario salvarPlanilha(IdentificacaoRequest usuarioGrupoResquest, MultipartFile file) throws IOException {

          dominioStatusCargas = this.statusCached.busca(ARQUIVO_CARGAS.getNome());
          final StatusDescricao statusRecebido = mapStatusDescricao(dominioStatusCargas, RECEBIDO.getStatus());

          final ArquivoCargas arquivoCarga = new ArquivoCargas();
          arquivoCarga.setStatus(statusRecebido.getStatus());
          arquivoCarga.setDataStatus(LocalDateTime.now());
          arquivoCarga.setNome(file.getOriginalFilename());
          arquivoCarga.setIdUsuario(usuarioGrupoResquest.getIdUsuario());
          arquivoCarga.setIdGrupoEmpresa(usuarioGrupoResquest.getIdGrupoEmpresa());
          arquivoCarga.setOrigem(AppConstantes.FRONT_RH.toString());
          arquivoCarga.setDataImportacao(LocalDateTime.now());

          ArquivoCargasBinario arquivoCargasBinario = new ArquivoCargasBinario();
          arquivoCargasBinario.setArquivo(file.getBytes());
          arquivoCargasBinario.setArquivoCargas(arquivoCarga);

          return arquivoCargasBinarioRepository.save(arquivoCargasBinario);
     }

     public List<ArquivoCargasBinario> listarPlanilha(Integer status) throws IOException {

          final List<ArquivoCargasBinario> listaArquivoCargas = arquivoCargasBinarioRepository.findByArquivoCargasStatus(status);

          BadRequestCdt.checkThrow(listaArquivoCargas.isEmpty(), ExceptionsMessagesCdtEnum.PEDIDO_NAO_ENCONTRADO);

          return listaArquivoCargas;
     }

     public ArquivoCargas buscaArquivoParaProcessar() throws ObjetoVazioException {

          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(ARQUIVO_CARGAS.getNome());
          final StatusDescricao statusValidadoParcial = mapStatusDescricao(dominioStatusCargas, VALIDADO_PARCIAL.getStatus());
          final StatusDescricao statusRecebido = mapStatusDescricao(dominioStatusCargas, RECEBIDO.getStatus());

          ArquivoCargas arquivoCarga = null;
          try {
               arquivoCarga = this.buscaPrimeiroArquivoPorStatus(statusValidadoParcial);
          } catch (ObjetoVazioException e) {
               arquivoCarga = this.buscaPrimeiroArquivoPorStatus(statusRecebido);
          }

          if (arquivoCarga.getStatus().intValue() == statusValidadoParcial.getStatus().intValue()) {
               this.pedidoDetalhadoService.deletarArquivoCargasDetalhadoPor(arquivoCarga.getId());
          } else {
               arquivoCarga.setStatus(statusValidadoParcial.getStatus());
          }

          this.atualizarArquivoCarga(arquivoCarga);

          return arquivoCarga;
     }

     private ArquivoCargas buscaPrimeiroArquivoPorStatus(final StatusDescricao status) throws ObjetoVazioException {

          final LocalDateTime dataProcessamentoLimite = LocalDateTime.now().minusSeconds(DATA_PROCESSAMENTO_LIMITE);
          ArquivoCargas arquivoCarga = null;

          if (StringUtils.equalsIgnoreCase(VALIDADO_PARCIAL.getStatus(), status.getDescricao())) {
               arquivoCarga = this.arquivoCargasRepository.findTopByStatusAndOrigemAndDataStatusLessThan(status.getStatus(), FRONT_RH, dataProcessamentoLimite);
          } else {
               arquivoCarga = this.arquivoCargasRepository.findTopByStatusAndOrigem(status.getStatus(), FRONT_RH).orElse(null);
          }

          if (Objects.isNull(arquivoCarga)) {
               throw new ObjetoVazioException(String.format("Não foi encontrado registros com o status %s.", status));
          }
          return arquivoCarga;
     }

     private ArquivoCargas atualizarArquivoCarga(ArquivoCargas arquivoCarga) {

          arquivoCarga.setDataStatus(LocalDateTime.now());
          
          return arquivoCargasRepository.save(arquivoCarga);
     }

     public ArquivoCargas atualizaDadosPosProcessamento(ArquivoCargas arquivoCarga, boolean isValido, String mensagem) {

          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(ARQUIVO_CARGAS.getNome());
          final StatusDescricao statusImportado = mapStatusDescricao(dominioStatusCargas, IMPORTADO.getStatus());
          final StatusDescricao statusInvalidado = mapStatusDescricao(dominioStatusCargas, INVALIDADO.getStatus());

          if (isValido) {
               arquivoCarga.setValor(arquivoCargasRepository.selectValorEsperadoSomaTotalizacaoPedido(arquivoCarga.getId()));
               arquivoCarga.setStatus(statusImportado.getStatus());
          } else {
               emailFinalizaPedidoService.enviar(arquivoCarga, INVALIDADO);
               arquivoCarga.setMotivoErro(mensagem);
               arquivoCarga.setStatus(statusInvalidado.getStatus());
          }
          return this.atualizarArquivoCarga(arquivoCarga);
     }

     public ArquivoCargasBinario buscaArquivoPedidoBinarioPor(ArquivoCargas arquivoCarga) throws ObjetoVazioException {

          Optional<ArquivoCargasBinario> opt = arquivoCargasBinarioRepository.findByArquivoCargas(arquivoCarga);
          if (!opt.isPresent()) {
               throw new ObjetoVazioException("Não foi localizado registro blob para este pedido.");
          }
          return opt.get();
     }

     private byte[] buscaBlobPor(ArquivoCargas arquivoCarga) throws ObjetoVazioException {

          return this.buscaArquivoPedidoBinarioPor(arquivoCarga).getArquivo();
     }

     public boolean inicializaImportacao(final ArquivoCargas arquivoCarga, final String tipoEntregaCartao) throws ObjetoVazioException, ConversaoObjetoException, BusinessException {

          final PermissoesUsuariosRh permissoes = this.buscarNivelPermissaoPor(arquivoCarga);
          buscaParametrosPedido(arquivoCarga);

          TemplatesExcel template = tipoEntregaCartao.equals(TipoEntregaCartao.PORTA_A_PORTA.getValor()) ? TemplatesExcel.ARQUIVO_CARGA_PORTA_A_PORTA : TemplatesExcel.ARQUIVO_CARGA_LOTE;

          final byte[] blob = this.buscaBlobPor(arquivoCarga);
          final InputStream blobInputStream = ArquivoUtils.converteBlobParaInputStream(blob);
          final Workbook workbook = ArquivoUtils.criaWorkbookDe(blobInputStream);

          final List<String> listaCpfCnpj = this.buscaListaCpfCnpjPor(permissoes.getId());
          return this.pedidoDetalhadoService.isConteudoValido(workbook, template, arquivoCarga, listaCpfCnpj);
     }

     private PermissoesUsuariosRh buscarNivelPermissaoPor(ArquivoCargas arquivoCarga) throws ObjetoVazioException, BusinessException {

          dominioStatusCargas = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, ATIVO.name());

          Optional<PermissoesUsuariosRh> optPermissao = permissoesUsuariosRhRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(arquivoCarga.getIdUsuario(), arquivoCarga.getIdGrupoEmpresa(), statusPermissaoAtiva.getStatus());

          if (!optPermissao.isPresent())
               throw new ObjetoVazioException("Usuário sem permissões para gerar pedidos.");
          return optPermissao.get();
     }

     private List<String> buscaListaCpfCnpjPor(Long idPermissao) throws ObjetoVazioException {

          List<String> listaCpfCnpj = pessoaCustomRepositoryImpl.findCpfCnpjByIdPermissao(idPermissao);
          if (CollectionUtils.isEmpty(listaCpfCnpj))
               throw new ObjetoVazioException("Não existem empresas vinculadas ao grupo.");
          return listaCpfCnpj;
     }

     public PageArquivoCargasResponse listarPedidos(Integer page, Integer size, ArquivoPedidoRequest request) {

          Pageable pageable = PageRequest.of(page, size);

          Optional<GrupoEmpresa> grupoEmpresaExiste = grupoEmpresaRepository.findById(request.getIdGrupoEmpresa());
          NotFoundCdt.checkThrow(!grupoEmpresaExiste.isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_LOCALIZADO);

          UsuarioResponse usuarioExiste = usuarioPierRestRepository.consultarUsuario(request.getIdUsuarioSessao());
          NotFoundCdt.checkThrow(Objects.isNull(usuarioExiste), ExceptionsMessagesCdtEnum.USUARIO_NAO_LOCALIZADO);

          dominioStatusCargas = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, ATIVO.name());

          Optional<PermissoesUsuariosRh> optPermissao = permissoesUsuariosRhRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(request.getIdUsuarioSessao(), request.getIdGrupoEmpresa(), statusPermissaoAtiva.getStatus());
          ForbiddenCdt.checkThrow(!optPermissao.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          if (Objects.isNull(request.getIdEmpresasPermitidas()) || request.getIdEmpresasPermitidas().isEmpty()) {

               List<Integer> idEmpresas = permissoesUsuariosRhRepository.findEmpresasbyIdPermissao(optPermissao.get().getId());
               ForbiddenCdt.checkThrow(idEmpresas.size() == 0, ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

               request.setIdEmpresasPermitidas(idEmpresas);
          }

          PageResponse<ArquivoPedidoResponse> mapUsuario = null;

          PageResponse<ArquivoCargasCustom> pagePedidosStd = this.arquivoCargasRepositoryCustomImpl.listaDePedido(request, pageable);
          NoContentCdt.checkThrow(pagePedidosStd.getContent().isEmpty(), ExceptionsMessagesCdtEnum.NAO_HA_PEDIDOS);

          Optional<UsuarioPermissaoNivelAcessoRh> permissaoNivelGrupo = usuarioNivelPermissaoRhService.obterUsuarioNivelPermissaoGrupoAtivo(optPermissao.get().getId(), request.getIdGrupoEmpresa());

          mapUsuario = mapUsuarioSession(page, size, pagePedidosStd, permissaoNivelGrupo.isPresent());

          PageArquivoCargasResponse response = new PageArquivoCargasResponse(mapUsuario);

          return response;
     }

     private PageResponse<ArquivoPedidoResponse> mapUsuarioSession(Integer page, Integer size, PageResponse<ArquivoCargasCustom> listaPedidosStd, Boolean isPermissaoNivelGrupo) {

          List<Long> idsUsuarios = listaPedidosStd.getContent().stream().map(ArquivoCargasCustom::getIdUsuario).collect(Collectors.toList());

          List<Usuario> usuarios = usuarioRepository.findAllByIdIn(idsUsuarios);

          HashMap<Long, UsuarioResumido> mapUsuarios = new HashMap<Long, UsuarioResumido>();

          for (Usuario usr : usuarios) {
               mapUsuarios.put(usr.getId(), GenericConvert.convertModelMapper(usr, UsuarioResumido.class));
          }

          List<ArquivoPedidoResponse> listaPedidos = GenericConvert.convertModelMapper(listaPedidosStd.getContent(), new TypeToken<List<ArquivoPedidoResponse>>() {
          }.getType());

          obterStatusPermissoes();

          obterStatusPagamento();


          for (ArquivoPedidoResponse pedido : listaPedidos) {
               pedido.setUsuario(mapUsuarios.get(pedido.getIdUsuario()));

               String descricao = this.mapStatus.get(pedido.getStatus().intValue()).getDescricao();
               pedido.setStatusEnum(StatusArquivoEnum.getByDescricao(descricao.toUpperCase()).name());

               if (Objeto.notBlank(pedido.getStatusPagamento())) {
                    String descricaoStatusPagamento = this.mapStatusPagamento.get(pedido.getStatusPagamento().intValue()).getDescricao();
                    pedido.setStatusPagamentoEnum(StatusPagamentoEnum.getByDescricao(descricaoStatusPagamento).name());
               }

               pedido.setAcoes(configuraAcoesPedido(pedido, isPermissaoNivelGrupo));

          }

          PageResponse<ArquivoPedidoResponse> pageResponse = new PageResponse<ArquivoPedidoResponse>();
          pageResponse.setContent(listaPedidos);
          Integer countElements = Long.valueOf(listaPedidosStd.getTotalElements()).intValue();
          return PageUtils.ajustarPageResponse(pageResponse, page, size, countElements);
     }

     public PageArquivoPedidoDetalhesErrosReponse listarDetalhesErros(Integer page, Integer size, Long idArquivoPedido) {

          dominioStatusCargas = this.statusCached.busca(ARQUIVO_CARGAS.getNome());
          final StatusDescricao statusInvalidado = mapStatusDescricao(dominioStatusCargas, INVALIDADO.getStatus());
          final StatusDescricao statusProcessamentoConcluido = mapStatusDescricao(dominioStatusCargas, PROCESSAMENTO_CONCLUIDO.getStatus());

          Optional<ArquivoCargas> optArquivoCarga = arquivoCargasRepository.findById(idArquivoPedido);
          NotFoundCdt.checkThrow(!optArquivoCarga.isPresent(), ExceptionsMessagesCdtEnum.GLOBAL_RECURSO_NAO_ENCONTRADO);
          PreconditionCustom.checkThrow((!optArquivoCarga.get().getStatus().equals(statusInvalidado.getStatus()) && !optArquivoCarga.get().getStatus().equals(statusProcessamentoConcluido.getStatus())), ExceptionsMessagesCdtEnum.PEDIDO_STATUS_DIFERENTE_INVALIDO_PROCESSAMENTO_CONCLUIDO);

          PageResponse<ArquivoPedidoDetalhesErrosReponse> listaPedidoDetalhesErros = arquivoCargasDetalhesCustomRepositoryImpl.findByIdArquivoCargasAndStatusDetalheOrderByNumeroLinhaArquivo(idArquivoPedido, page, size);
          NoContentCdt.checkThrow(CollectionUtils.isEmpty(listaPedidoDetalhesErros.getContent()), ExceptionsMessagesCdtEnum.NAO_HA_DETALHES_PEDIDO);

          return new PageArquivoPedidoDetalhesErrosReponse(listaPedidoDetalhesErros);
     }

     public void buscaParametrosPedido(ArquivoCargas arquivoCargas) throws BusinessException {

          String valorTipoContrato = null;
          String valorPedidoCentralizado = null;
          String valorFaturamentoCentralizado = null;
          StringBuilder erros = new StringBuilder();

          String mensagemPadrao = "Parametrização não encontrada sobre %s. ";

          try {
               valorTipoContrato = parametrosService.buscaParametrosGrupoEmpresaPor(arquivoCargas.getIdGrupoEmpresa(), AppConstantes.PARAMETRO_RH, TIPO_CONTRATO).getValor();
          } catch (NotFoundCdt e) {
               erros.append(String.format(mensagemPadrao, TIPO_CONTRATO.getDescricao()));
          }

          try {
               valorPedidoCentralizado = parametrosService.buscaParametrosGrupoEmpresaPor(arquivoCargas.getIdGrupoEmpresa(), AppConstantes.PARAMETRO_RH, FLAG_PEDIDO_CENTRALIZADO).getValor();
          } catch (NotFoundCdt e) {
               erros.append(String.format(mensagemPadrao, FLAG_PEDIDO_CENTRALIZADO.getDescricao()));
          }

          try {
               valorFaturamentoCentralizado = parametrosService.buscaParametrosGrupoEmpresaPor(arquivoCargas.getIdGrupoEmpresa(), AppConstantes.PARAMETRO_RH, FLAG_FATURAMENTO_CENTRALIZADO).getValor();
          } catch (NotFoundCdt e) {
               erros.append(String.format(mensagemPadrao, FLAG_FATURAMENTO_CENTRALIZADO.getDescricao()));
          }

          if (StringUtils.isEmpty(erros.toString())) {
               arquivoCargas.setTipoPagamentoPedido(valorTipoContrato);
               arquivoCargas.setFlagPedidoCentralizado(StringUtils.equals(valorPedidoCentralizado, "0") ? Boolean.FALSE : Boolean.TRUE);
               arquivoCargas.setFlagFaturamentoCentralizado(StringUtils.equals(valorFaturamentoCentralizado, "0") ? Boolean.FALSE : Boolean.TRUE);
               atualizarArquivoCarga(arquivoCargas);
          } else {
               throw new BusinessException(erros.toString());
          }

     }

     private void obterStatusPermissoes() {

          if (!mapStatus.isEmpty()) {
               return;
          }

          try {
               List<StatusDescricoes> status = statusDescricaoRepositoryCustom.findStatusDescByNomeStatus(ARQUIVOCARGAS);
               for (StatusDescricoes statusDescricoes : status) {
                    mapStatus.put(statusDescricoes.getStatus().intValue(), statusDescricoes);
               }

          } catch (Exception e) {
               e.printStackTrace();
               ExceptionsMessagesCdtEnum.GLOBAL_ERRO_SERVIDOR.raise();
          }
     }

     private void obterStatusPagamento() {

          if (!mapStatusPagamento.isEmpty()) {
               return;
          }

          try {
               List<StatusDescricoes> statusPagamento = statusDescricaoRepositoryCustom.findStatusDescByNomeStatus(ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome());
               for (StatusDescricoes statusDescricoes : statusPagamento) {
                    mapStatusPagamento.put(statusDescricoes.getStatus().intValue(), statusDescricoes);
               }

          } catch (Exception e) {
               e.printStackTrace();
               ExceptionsMessagesCdtEnum.GLOBAL_ERRO_SERVIDOR.raise();
          }
     }

     /**
      * cancelarPedido Método responsável por realizar cancelamento de pedido
      *
      * @param id
      * @return
      */
     @Transactional
     public ResponseEntity<?> cancelarPedido(Long id, Long idUsuario) {

          Optional<Usuario> usuarioRegistro = usuarioPierService.autenticarUsuario(idUsuario);

          ArquivoCargas arquivo = buscaPedidoPorId(id);

          PermissoesUsuariosRh permissaoUsuario = permissoesUsuariosRhService.autenticarPermissaoAtiva(usuarioRegistro.get().getId(), arquivo.getIdGrupoEmpresa());

          usuarioNivelPermissaoRhService.autenticarPermissaoNivelGrupoAtiva(arquivo.getIdGrupoEmpresa(), permissaoUsuario.getId());

          PreconditionCustom.checkThrow(!isIdStatusPedidoIgualA(arquivo.getStatus(), PROCESSAMENTO_CONCLUIDO), ExceptionsMessagesCdtEnum.STATUS_ARQUIVO_NAO_PERMITE_CANCELAMENTO);

          PreconditionCustom.checkThrow(!arquivo.getFlagFaturamentoCentralizado(), ExceptionsMessagesCdtEnum.NAO_CANCELADO_FATURAMENTRO_DESCENTRALIZADO);

          boolean isPeriodoForaRegraCancelamento = !this.isPeriodoDentroRegraCancelamento(arquivo.getId(), arquivo.getIdGrupoEmpresa(), arquivo.getStatusPagamento());
          BadRequestCdt.checkThrow(isPeriodoForaRegraCancelamento, ExceptionsMessagesCdtEnum.FORA_DO_PERIODO_DE_CANCELAMENTO);

          cancelaPedidoCustomRepositoyImpl.cancelaPedidoProc(id);

          return new ResponseEntity<>(HttpStatus.OK);

     }

     private boolean isIdStatusPedidoIgualA(Integer statusPedido, StatusArquivoEnum statusArquivoEnum) {

          return mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS.getNome()), statusArquivoEnum.getStatus()).getStatus().equals(statusPedido);

     }

     private boolean isIdStatusPagamentoIgualA(Integer statusPagamento, StatusPagamentoEnum statusPagamentoEnum) {

          return mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome()), statusPagamentoEnum.getStatus()).getStatus().equals(statusPagamento);

     }

     private boolean isPeriodoDentroRegraCancelamento(Long idPedido, Long idGrupoEmpresa, Integer idStatusPagamento) {

          LocalDateTime dataAgendamento = cargaBeneficioService.buscaMenorDataAgendamentoDeCargaBenefecio(idPedido);
          Long periodo = ChronoUnit.DAYS.between(LocalDate.now().atStartOfDay(), dataAgendamento.with(LocalTime.MAX));

          if (!parametrosService.isTipoPagamentoPrePago(idGrupoEmpresa)) {
               if (isIdStatusPagamentoIgualA(idStatusPagamento, LIMITE_CREDITO_INSUFICIENTE) || isIdStatusPagamentoIgualA(idStatusPagamento, LIMITE_CREDITO_VENCIDO)) {
                    return periodo > MAIOR_INTERVALO_CANCELAMENTO_PEDIDO_45_DIAS;
               }
          }

          return periodo >= MENOR_INTERVALO_CANCELAMENTO_PEDIDO_1_DIA;

     }


     private ArquivoCargas buscaPedidoPorId(Long id) {

          Optional<ArquivoCargas> arquivo = arquivoCargasRepository.findById(id);

          NotFoundCdt.checkThrow(!arquivo.isPresent(), ExceptionsMessagesCdtEnum.ARQUIVO_NAO_ENCONTRADO);

          return arquivo.get();
     }

     private AcoesPedidoResponse configuraAcoesPedido(ArquivoPedidoResponse pedido, Boolean isPermissaoNivelGrupo) {

          boolean isArquivoProcessamentoConcluido = StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.name().equals(pedido.getStatusEnum());
          boolean isTipoFaturamentoCentralizado = TipoFaturamentoEnum.CENTRALIZADO.getValor().equals(pedido.getFlagFaturamentoCentralizado());

          boolean podeCancelarPedido = false;

          if (isPermissaoNivelGrupo && isArquivoProcessamentoConcluido && isTipoFaturamentoCentralizado) {
               podeCancelarPedido = this.isPeriodoDentroRegraCancelamento(pedido.getId(), pedido.getIdGrupoEmpresa(), pedido.getStatusPagamento());
          }

          return AcoesPedidoResponse.builder().cancelar(podeCancelarPedido).build();
     }

     public Optional<ArquivoCargas> buscaPorId(Long idArquivoCarga) {

          return arquivoCargasRepository.findById(idArquivoCarga);
     }

     public ArquivoCargas salvar(ArquivoCargas arquivoCarga) {
          
          arquivoCarga.setDataStatus(LocalDateTime.now());
          
          return arquivoCargasRepository.save(arquivoCarga);
     }
     
     public ArquivoCargas atualizaStatusArquivoCarga(ArquivoCargas arquivoCarga, final StatusArquivoEnum statusArquivoEnum, final StatusPagamentoEnum statusPagamentoEnum) {

          final Optional<ArquivoCargas> optArquivoCarga = this.arquivoCargasRepository.findById(arquivoCarga.getId());

          final ArquivoCargas ArquivoCargasAux = optArquivoCarga.get();

          if (Objects.nonNull(statusArquivoEnum)) {
               final List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(ARQUIVO_CARGAS.getNome());
               final StatusDescricao statusDescricao = mapStatusDescricao(dominioStatusCargas, statusArquivoEnum.getStatus());

               ArquivoCargasAux.setStatus(statusDescricao.getStatus());

          }

          if (Objects.nonNull(statusPagamentoEnum)) {
               final List<StatusDescricao> dominioStatusPagamento = this.statusCached.busca(ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome());
               final StatusDescricao statusDescricaoPagamento = mapStatusDescricao(dominioStatusPagamento, statusPagamentoEnum.getStatus());

               ArquivoCargasAux.setStatusPagamento(statusDescricaoPagamento.getStatus());
          }

          ArquivoCargasAux.setDataProcessamento(LocalDateTime.now());
          return this.arquivoCargasRepository.save(ArquivoCargasAux);
     }

     public boolean isStatusPedidoDiferenteDe(Long idArquivoCargas, StatusArquivoEnum statusArquivo) {

          final StatusDescricao status = mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS.getNome()), statusArquivo.getStatus());

          return this.buscaPorId(idArquivoCargas).get().getStatus() != status.getStatus();
     }

     public Boolean existeArquivoPedidoPorGrupoComStatus(Long idGrupoEmpresa, List<Integer> statuses) {

          return arquivoCargasRepository.existsByIdGrupoEmpresaAndStatusIsIn(idGrupoEmpresa, statuses);
     }

     private List<CargaPedidoCustom> buscarCargasPedidosComProblemasDeLimiteDeCredito() {

          final StatusDescricao statusPagamentoLimiteCreditoInsuficiente = mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome()), LIMITE_CREDITO_INSUFICIENTE.getStatus());
          final StatusDescricao statusPagamentoLimiteCreditoVencido = mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome()), LIMITE_CREDITO_VENCIDO.getStatus());

          return arquivoCargasRepositoryCustomImpl.buscarCargasPedidosPorStatusPagamento(Arrays.asList(statusPagamentoLimiteCreditoInsuficiente.getStatus(), statusPagamentoLimiteCreditoVencido.getStatus()));
     }

     public Map<Long, List<CargaPedidoCustom>> buscarMapPedidosComProblemasDeLimiteDeCredito() {

          List<CargaPedidoCustom> cargasPedidos = this.buscarCargasPedidosComProblemasDeLimiteDeCredito();

          if(CollectionUtils.isNotEmpty(cargasPedidos))
               return cargasPedidos.stream().collect(Collectors.groupingBy(CargaPedidoCustom::getIdGrupoEmpresa, toList()));
          else
               return Collections.emptyMap();
     }

     public void atualizaStatusPagamentoEmLote(List<Long> numerosPedidos, StatusPagamentoEnum status) {

          List<StatusDescricao> dominioStatusPagamentoArquivoCargas = this.statusCached.busca(TipoStatus.ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome());
          final StatusDescricao statusPagamento = mapStatusDescricao(dominioStatusPagamentoArquivoCargas, status.getStatus());
          
          arquivoCargasRepository.updateStatusPagamentoByIdArquivoCargasIn(statusPagamento.getStatus(), numerosPedidos);
     }
     
     public void atualizaStatusPagamento(Long numeroPedido, StatusPagamentoEnum status) {
          
          List<StatusDescricao> dominioStatusPagamentoArquivoCargas = this.statusCached.busca(TipoStatus.ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome());
          final StatusDescricao statusPagamento = mapStatusDescricao(dominioStatusPagamentoArquivoCargas, status.getStatus());
          
          arquivoCargasRepository.updateStatusPagamentoByIdArquivoCargas(statusPagamento.getStatus(), numeroPedido);
     }
     
     public ArquivoCargas inserirUUID(ArquivoCargas arquivoCarga) {
          
          arquivoCarga.setUuid(UUID.randomUUID().toString().substring(0, 11));
               
          return arquivoCargasRepository.save(arquivoCarga);
     }
     
}
