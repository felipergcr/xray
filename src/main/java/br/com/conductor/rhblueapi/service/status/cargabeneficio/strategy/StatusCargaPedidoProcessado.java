
package br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy;

import static br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum.AGUARDANDO_PAGAMENTO;
import static br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum.PAGO;
import static br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum.PAGO_PARCIALMENTE;

import br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum;

public class StatusCargaPedidoProcessado implements StatusCargaBeneficioStrategy {

     @Override
     public StatusPagamentoBaseEnum retornaStatusPagamento(boolean todosStatusIguais, int qtdCargasPedido, int qtdCargasPedidoPagas) {
          
          boolean isAllPagos = qtdCargasPedido == qtdCargasPedidoPagas;
          
          if (todosStatusIguais && isAllPagos)
               return PAGO;
          
          if (qtdCargasPedidoPagas > 0)
               return PAGO_PARCIALMENTE;

          return AGUARDANDO_PAGAMENTO;
     }

}
