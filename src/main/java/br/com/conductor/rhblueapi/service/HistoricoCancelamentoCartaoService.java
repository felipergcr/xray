package br.com.conductor.rhblueapi.service;

import br.com.conductor.rhblueapi.domain.Cartoes;
import br.com.conductor.rhblueapi.domain.HistoricoCancelamentoCartao;
import br.com.conductor.rhblueapi.domain.HistoricoCancelamentoCartaoPK;
import br.com.conductor.rhblueapi.domain.StatusCartoes;
import br.com.conductor.rhblueapi.repository.HistoricoCancelamentoCartaoRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static br.com.conductor.rhblueapi.util.EntityGenericUtil.trimy;

@Service
public class HistoricoCancelamentoCartaoService {

    private static final String USUARIO_RESPONSAVEL = "RH-BLUE-API";

    @Autowired
    private HistoricoCancelamentoCartaoRepository historicoCancelamentoCartaoRepository;

    public HistoricoCancelamentoCartao criarHistoricoCancelamentoCartao(final Cartoes cartao, final StatusCartoes statusCartoes, final String observacao) {

        final String DESCRICAO = StringUtils.join(trimy(statusCartoes.getNome()), " - ", observacao);

        HistoricoCancelamentoCartao historicoCancelamentoCartao = new HistoricoCancelamentoCartao();
        HistoricoCancelamentoCartaoPK historicoCancelamentoCartaoPK = new HistoricoCancelamentoCartaoPK();

        historicoCancelamentoCartaoPK.setIdConta(cartao.getPortador().getId().getIdConta());
        historicoCancelamentoCartaoPK.setDataCancelamento(LocalDateTime.now());

        historicoCancelamentoCartao.setId(historicoCancelamentoCartaoPK);
        historicoCancelamentoCartao.setIdCartao(cartao.getId());
        historicoCancelamentoCartao.setCartao(cartao.getNumeroCartao());
        historicoCancelamentoCartao.setDescricao(DESCRICAO);
        historicoCancelamentoCartao.setResponsavel(USUARIO_RESPONSAVEL);

        return historicoCancelamentoCartaoRepository.save(historicoCancelamentoCartao);
    }

}
