
package br.com.conductor.rhblueapi.service.relatorios.validacao;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.request.relatorios.PeriodoRequest;
import br.com.conductor.rhblueapi.domain.request.relatorios.RelatorioRequest;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;

@Service
public class RelatoriosValidacaoService {
     
     @Value("${app.regras.relatorios.periodo.dias.detalhes-pedidos}")
     private Integer qtdDiasMaximoPeriodo;
     
     
     public RelatorioRequest createAdapter(LocalDate dataInicio, LocalDate dataFim, Long idUsuario, Long idGrupoEmpresa) {
          
          PeriodoRequest periodoRequest = PeriodoRequest.builder()
                    .dataInicio(dataInicio)
                    .dataFim(dataFim)
                    .build();
          
          IdentificacaoRequest identificacao = IdentificacaoRequest.builder()
                    .idUsuario(idUsuario)
                    .idGrupoEmpresa(idGrupoEmpresa)
                    .build();
          
          return RelatorioRequest.builder()
                    .periodoRequest(periodoRequest)
                    .identificacao(identificacao)
                    .build();
          
     }
     
     public void validaPeriodo(PeriodoRequest periodoRequest) {
          
          LocalDate dataInicio = periodoRequest.getDataInicio();
          LocalDate dataFim = periodoRequest.getDataFim();
          
          BadRequestCdt.checkThrow(dataInicio.isAfter(LocalDate.now()), ExceptionsMessagesCdtEnum.PERIODO_DATAINICIO_MAIOR_DATAATUAL);
          
          BadRequestCdt.checkThrow(dataFim.isAfter(LocalDate.now()), ExceptionsMessagesCdtEnum.PERIODO_DATAFIM_MAIOR_DATAATUAL);
          
          BadRequestCdt.checkThrow(dataFim.isBefore(dataInicio), ExceptionsMessagesCdtEnum.PERIODO_DATAFIM_MENOR_DATAINICIO);
          
          BadRequestCdt.checkThrow(ChronoUnit.DAYS.between(dataInicio, dataFim) > qtdDiasMaximoPeriodo , ExceptionsMessagesCdtEnum.PERIODO_INTERVALO_MAIOR_QUE_O_PERMITIDO,
                    String.format(ExceptionsMessagesCdtEnum.PERIODO_INTERVALO_MAIOR_QUE_O_PERMITIDO.getMessage(), qtdDiasMaximoPeriodo));
          
     }
     
}
