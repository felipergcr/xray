package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.CARGA_FORA_DO_PERIODO_DE_CANCELAMENTO;
import static br.com.conductor.rhblueapi.enums.StatusDetalheEnum.INVALIDADO;
import static br.com.conductor.rhblueapi.enums.StatusDetalheEnum.VALIDADO;
import static br.com.conductor.rhblueapi.enums.StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE;
import static br.com.conductor.rhblueapi.enums.StatusPagamentoEnum.LIMITE_CREDITO_VENCIDO;
import static br.com.conductor.rhblueapi.enums.StatusPermissaoEnum.ATIVO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS_DETALHES;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS_STATUSPAGAMENTO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.CARGAS_BENEFICIOS;
import static br.com.conductor.rhblueapi.enums.TipoStatus.PERMISSOES_USUARIOS_RH;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.MAIOR_INTERVALO_CANCELAMENTO_PEDIDO_45_DIAS;
import static br.com.conductor.rhblueapi.util.AppConstantes.MENOR_INTERVALO_CANCELAMENTO_PEDIDO_1_DIA;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.reflect.TypeToken;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import br.com.conductor.rhblueapi.domain.CargaBeneficio;
import br.com.conductor.rhblueapi.domain.CargaBeneficioCustom;
import br.com.conductor.rhblueapi.domain.CargaBeneficioProdutosCustom;
import br.com.conductor.rhblueapi.domain.Empresa;
import br.com.conductor.rhblueapi.domain.EmpresasCargas;
import br.com.conductor.rhblueapi.domain.FuncionarioCargaBeneficio;
import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.LinhaValidadaExcel;
import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;
import br.com.conductor.rhblueapi.domain.ProdutoCargaCustom;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.StatusDescricoes;
import br.com.conductor.rhblueapi.domain.UnidadeEntrega;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.request.PedidoDetalheFuncionarioRequest;
import br.com.conductor.rhblueapi.domain.request.PedidoDetalhesRequest;
import br.com.conductor.rhblueapi.domain.response.AcoesCargaDetalheResponse;
import br.com.conductor.rhblueapi.domain.response.PagePedidoDetalheResponse;
import br.com.conductor.rhblueapi.domain.response.PagePedidoFuncionariosDetalhesResponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.domain.response.PedidoDetalheFuncionarioProdutoResponse;
import br.com.conductor.rhblueapi.domain.response.PedidoDetalheFuncionarioResponse;
import br.com.conductor.rhblueapi.domain.response.PedidoDetalhesResponse;
import br.com.conductor.rhblueapi.enums.Documentos;
import br.com.conductor.rhblueapi.enums.HeaderPedidoLoteEnum;
import br.com.conductor.rhblueapi.enums.HeaderPedidoPortaPortaEnum;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;
import br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum;
import br.com.conductor.rhblueapi.enums.StatusDetalheEnum;
import br.com.conductor.rhblueapi.enums.StatusEmpresaCargaDetalheProdutoEnum;
import br.com.conductor.rhblueapi.enums.StatusPagamentoEnum;
import br.com.conductor.rhblueapi.enums.TemplatesExcel;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.exception.BusinessException;
import br.com.conductor.rhblueapi.exception.ConteudoExcelException;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.repository.ArquivoCargasDetalhesRepository;
import br.com.conductor.rhblueapi.repository.ArquivoCargasRepository;
import br.com.conductor.rhblueapi.repository.CancelaPedidoCustomRepositoryImpl;
import br.com.conductor.rhblueapi.repository.CargaBeneficioCustomRepository;
import br.com.conductor.rhblueapi.repository.CargaBeneficioRepository;
import br.com.conductor.rhblueapi.repository.EmpresasCargasRepository;
import br.com.conductor.rhblueapi.repository.EmpresasRepository;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.PermissoesUsuariosRhRepository;
import br.com.conductor.rhblueapi.repository.StatusDescricoesRepositoryCustom;
import br.com.conductor.rhblueapi.repository.pier.UsuarioPierRestRepository;
import br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.factory.TipoContratoFactory;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.GenericConvert;
import br.com.conductor.rhblueapi.util.PageUtils;
import br.com.conductor.rhblueapi.util.RhHeaderDefinition;
import br.com.conductor.rhblueapi.util.ValidacaoTemplate;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class PedidoDetalhadoService implements Serializable {

     private static final long serialVersionUID = 1L;

     private static final DateTimeFormatter DATETIME_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy").withLocale(Locale.getDefault());

     @Autowired
     private ArquivoCargasDetalhesRepository arquivosCargasDetalhesRepository;

     @Autowired
     private PedidoDetalhadoErrosService pedidoDetalhadoErrosService;

     @Autowired
     public CargaBeneficioRepository cargaBeneficioRepository;

     @Autowired
     public CargaBeneficioCustomRepository cargaBeneficioCustomRepository;

     @Autowired
     private PermissoesUsuariosRhRepository permissoesUsuariosRhRepository;

     @Autowired
     private GrupoEmpresaRepository grupoEmpresaRepository;

     @Autowired
     private UsuarioPierRestRepository usuarioPierRestRepository;

     @Autowired
     private StatusDescricoesRepositoryCustom statusDescricaoRepositoryCustom;

     private HashMap<Integer, StatusDescricoes> mapStatus = new HashMap<Integer, StatusDescricoes>();

     @Autowired
     private ArquivoCargasRepository arquivoCargasRepository;

     @Autowired
     private EmpresasCargasRepository empresasCargasRepository;

     @Autowired
     private EmpresasRepository empresasRepository;

     @Autowired
     private StatusCached statusCached;

     @Autowired
     private DataService dataService;

     private List<StatusDescricao> dominioStatusCargas;

     @Autowired
     private UsuarioPierService usuarioPierService;

     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;

     @Autowired
     private CargaBeneficioService cargaBeneficioService;

     @Autowired
     private CancelaPedidoCustomRepositoryImpl cancelaPedidoCustomRepositoryImpl;
     
     @Autowired
     private UnidadeEntregaService unidadeEntregaService;

     @Autowired
     private ParametrosService parametrosService;
     
     @Autowired
     private TipoContratoFactory tipoContratoFactory;

     public PagePedidoDetalheResponse listarDetalhesCarga(Long idPedido, PedidoDetalhesRequest request, Integer page, Integer size) {

          Pageable pageable = PageRequest.of(page, size);

          List<CargaBeneficio> pedidoExiste = cargaBeneficioRepository.findByIdArquivoCargas(idPedido);

          NoContentCdt.checkThrow(Objects.isNull(pedidoExiste) || pedidoExiste.isEmpty(), ExceptionsMessagesCdtEnum.PEDIDO_INEXISTENTE);

          request.setIdPedido(idPedido);

          Optional<GrupoEmpresa> grupoEmpresaExiste = grupoEmpresaRepository.findById(request.getIdGrupoEmpresa());
          NoContentCdt.checkThrow(!grupoEmpresaExiste.isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_INEXISTENTE);

          UsuarioResponse usuarioExiste = usuarioPierRestRepository.consultarUsuario(request.getIdUsuarioSessao());
          NoContentCdt.checkThrow(Objects.isNull(usuarioExiste), ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO);

          dominioStatusCargas = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, ATIVO.name());

          Optional<PermissoesUsuariosRh> optPermissao = permissoesUsuariosRhRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(request.getIdUsuarioSessao(), request.getIdGrupoEmpresa(), statusPermissaoAtiva.getStatus());
          ForbiddenCdt.checkThrow(!optPermissao.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          if (Objects.isNull(request.getIdEmpresasPermitidas()) || request.getIdEmpresasPermitidas().isEmpty()) {
               List<Integer> idEmpresas = permissoesUsuariosRhRepository.findEmpresasbyIdPermissao(optPermissao.get().getId());
               ForbiddenCdt.checkThrow(idEmpresas.size() == 0, ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

               request.setIdEmpresasPermitidas(idEmpresas);
          }
          
          request.setStatusEmpresaCargaDetalheProduto(statusPedidoSemErroDeProcessamento());

		  List<CargaBeneficioCustom> listaCargasCustom = cargaBeneficioCustomRepository.listaDetalhadaDePedido(request, pageable);
          ForbiddenCdt.checkThrow(CollectionUtils.isEmpty(listaCargasCustom), ExceptionsMessagesCdtEnum.USUARIO_SEM_PERMISSAO_PEDIDO);

          Integer sizeListaCargasCustom = cargaBeneficioCustomRepository.countRegistrosDetalhado(request, pageable);

          List<CargaBeneficioProdutosCustom> prod = cargaBeneficioCustomRepository.listaCargaBeneficioPorProduto(request);
          ForbiddenCdt.checkThrow(Objeto.isBlank(prod), ExceptionsMessagesCdtEnum.USUARIO_SEM_PERMISSAO_PEDIDO);
          HashMap<Long, List<ProdutoCargaCustom>> listaCargasProdutos = mapeiaProdutosCargaBeneficio(prod);

          obterStatusCargaBeneficio();

          List<PedidoDetalhesResponse> listaCargas = GenericConvert.convertModelMapper(listaCargasCustom, new TypeToken<List<PedidoDetalhesResponse>>() {
          }.getType());

          for (PedidoDetalhesResponse pedido : listaCargas) {
               String descricao = mapStatus.get(pedido.getStatusCargaBeneficio().intValue()).getDescricao();
               pedido.setStatusEnum(StatusCargaBeneficioEnum.getByDescricao(descricao).status);
               pedido.setAcoes(this.acoesPossiveis(pedido));
               pedido.setProdutos(listaCargasProdutos.get(pedido.getIdCargaBeneficio()));
          }

          PageResponse<PedidoDetalhesResponse> pageResponse = new PageResponse<PedidoDetalhesResponse>();
          pageResponse.setContent(listaCargas);
          Integer countElements = sizeListaCargasCustom.intValue();
          pageResponse = PageUtils.ajustarPageResponse(pageResponse, page, size, countElements);

          PagePedidoDetalheResponse response = new PagePedidoDetalheResponse(pageResponse);

          return response;
     }

     private AcoesCargaDetalheResponse acoesPossiveis(final PedidoDetalhesResponse pedido) {

          boolean isPedidoDescentralizado = TipoFaturamentoEnum.DESCENTRALIZADO.getValor().equals(pedido.getFlagFaturamentoCentralizado());

          boolean isStatusArquivoPermitido = isIdStatusPedidoIgualA(pedido.getStatusArquivoCarga(), StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO) ||
                    isIdStatusPedidoIgualA(pedido.getStatusArquivoCarga(), StatusArquivoEnum.CANCELADO_PARCIAL);

          boolean isStatusCargaBeneficioPermitido = (isDescricaoStatusCargaBeneficioIgualA(pedido.getStatusEnum(), StatusCargaBeneficioEnum.CARGA_EFETUADA) ||
                    isDescricaoStatusCargaBeneficioIgualA(pedido.getStatusEnum(), StatusCargaBeneficioEnum.PENDENCIA_LIMITE) ||
                    isDescricaoStatusCargaBeneficioIgualA(pedido.getStatusEnum(), StatusCargaBeneficioEnum.AGUARDANDO_PAGAMENTO) ||
                    isDescricaoStatusCargaBeneficioIgualA(pedido.getStatusEnum(), StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO));

          boolean isPeriodoDentroRegraCancelamento = Objeto.notBlank(pedido.getDataAgendamento()) &&
                    this.isPeriodoDentroRegraCancelamento(pedido.getIdGrupoEmpresa(), pedido.getIdPedido(), pedido.getDataAgendamento().atStartOfDay());

          boolean flagPodeCancelarPedido = false;

          if (isPedidoDescentralizado &&
                    isStatusArquivoPermitido &&
                    isStatusCargaBeneficioPermitido &&
                    isPeriodoDentroRegraCancelamento) {
               flagPodeCancelarPedido = true;
          }

          return AcoesCargaDetalheResponse.builder().cancelar(flagPodeCancelarPedido).build();
     }

     private HashMap<Long, List<ProdutoCargaCustom>> mapeiaProdutosCargaBeneficio(List<CargaBeneficioProdutosCustom> cargasProdutos) {

          HashMap<Long, List<ProdutoCargaCustom>> carga = new HashMap<>();

          ProdutoCargaCustom produtoCarga = new ProdutoCargaCustom();

          List<ProdutoCargaCustom> produtosCarga = new ArrayList<>();

          Long idCarga = cargasProdutos.get(0).getIdCargaBeneficio();
          for (int i = 0; i < cargasProdutos.size(); i++) {
               if (idCarga.compareTo(cargasProdutos.get(i).getIdCargaBeneficio()) == 0) {
                    produtoCarga = ProdutoCargaCustom.builder().idProduto(cargasProdutos.get(i).getIdProduto()).nomeProduto(cargasProdutos.get(i).getNomeProduto()).valorTotal(cargasProdutos.get(i).getValorTotal()).build();
                    produtosCarga.add(produtoCarga);
               } else {
                    carga.put(cargasProdutos.get(i - 1).getIdCargaBeneficio(), produtosCarga);

                    produtosCarga = new ArrayList<>();
                    produtoCarga = ProdutoCargaCustom.builder().idProduto(cargasProdutos.get(i).getIdProduto()).nomeProduto(cargasProdutos.get(i).getNomeProduto()).valorTotal(cargasProdutos.get(i).getValorTotal()).build();
                    produtosCarga.add(produtoCarga);
               }
          }
          carga.put(cargasProdutos.get(cargasProdutos.size() - 1).getIdCargaBeneficio(), produtosCarga);

          return carga;
     }

     private void obterStatusCargaBeneficio() {

          if (!mapStatus.isEmpty()) {
               return;
          }

          try {
               List<StatusDescricoes> status = statusDescricaoRepositoryCustom.findStatusDescByNomeStatus(TipoStatus.CARGAS_BENEFICIOS.getNome());
               for (StatusDescricoes statusDescricoes : status) {
                    mapStatus.put(statusDescricoes.getStatus().intValue(), statusDescricoes);
               }
               ForbiddenCdt.checkThrow(mapStatus.isEmpty(), ExceptionsMessagesCdtEnum.GLOBAL_ERRO);
          } catch (Exception e) {
               e.printStackTrace();
               ExceptionsMessagesCdtEnum.GLOBAL_ERRO_SERVIDOR.raise();
          }
     }

     private List<ArquivosCargasDetalhes> buscaDetalhesPorStatus(Long idArquivoPedido, StatusDetalheEnum status) {

          List<ArquivosCargasDetalhes> detalhes = arquivosCargasDetalhesRepository.findByIdArquivoCargasAndStatusDetalheOrderByNumeroLinhaArquivo(idArquivoPedido, status.getIndex());
          NoContentCdt.checkThrow(CollectionUtils.isEmpty(detalhes), ExceptionsMessagesCdtEnum.NAO_HA_DETALHES_PEDIDO);

          return detalhes;
     }

     public List<ArquivosCargasDetalhes> buscaDetalhesInvalidados(Long idArquivoPedido) {

          return this.buscaDetalhesPorStatus(idArquivoPedido, StatusDetalheEnum.INVALIDADO);
     }

     private void salvarRespostaValidacao(Long idArquivoCargas, LinhaValidadaExcel linhaValidada) {

          ArquivosCargasDetalhes arquivosDetalhePedidos = alterarStatusPosValidacao(linhaValidada);
          arquivosDetalhePedidos = salvarLinhaDetalhada(idArquivoCargas, arquivosDetalhePedidos);
          if (!linhaValidada.isValido()) {
               pedidoDetalhadoErrosService.salvarErrosLinhaDetalhada(arquivosDetalhePedidos.getId(), linhaValidada);
          }
     }

     private ArquivosCargasDetalhes alterarStatusPosValidacao(final LinhaValidadaExcel linhaValidada) {

          List<StatusDescricao> statusDetalhe = this.statusCached.busca(ARQUIVO_CARGAS_DETALHES.getNome());
          StatusDescricao status = null;

          ArquivosCargasDetalhes arquivoPedidoDetalhe = linhaValidada.getDetalhe();
          if (linhaValidada.isValido())
               status = mapStatusDescricao(statusDetalhe, VALIDADO.name());
          else
               status = mapStatusDescricao(statusDetalhe, INVALIDADO.name());

          arquivoPedidoDetalhe.setStatusDetalhe(status.getStatus());

          return arquivoPedidoDetalhe;
     }

     private ArquivosCargasDetalhes salvarLinhaDetalhada(Long idArquivoPedido, ArquivosCargasDetalhes arquivosCargasDetalhes) {

          arquivosCargasDetalhes.setIdArquivoCargas(idArquivoPedido);
          return arquivosCargasDetalhesRepository.save(arquivosCargasDetalhes);
     }

     public boolean isConteudoValido(final Workbook workbook, final TemplatesExcel template, final ArquivoCargas arquivoCarga, final List<String> listaCpfCnpj) {

          boolean isPedidoLote = template.equals(TemplatesExcel.ARQUIVO_CARGA_LOTE) ? true : false;

          final Sheet sheet = workbook.getSheetAt(template.getIndiceAba());

          Integer linhaCabecalho = 0;

          String tokenInicioCabecalho = "CNPJ";
          for (Row row : sheet) {

               if (row.getCell(0) == null) {
                    linhaCabecalho++;
                    continue;
               }

               if (tokenInicioCabecalho.equals(row.getCell(0).getStringCellValue())) {
                    break;
               }
               linhaCabecalho++;
          }

          Optional<List<UnidadeEntrega>> optListaUnidadeEntrega = null;
          List<RhHeaderDefinition> listHeader = null;
          if (isPedidoLote) {
               optListaUnidadeEntrega = unidadeEntregaService.buscarUnidadeEntregaAtivaPorGrupoEmpresa(arquivoCarga.getIdGrupoEmpresa());
               listHeader = Arrays.asList(HeaderPedidoLoteEnum.values());
          } else {
               listHeader = Arrays.asList(HeaderPedidoPortaPortaEnum.values());
          }

          LinhaValidadaExcel linhaValidada = null;
          int controleLinhas = 0;
          boolean isConteudoValido = true;
          Map<String, String> mapCnpjDataCredito = new HashMap<>();

          for (final Row row : sheet) {
               if (controleLinhas > linhaCabecalho) {
                    if (isCelulasObrigatoriasVazio(row, listHeader))
                         break;

                    Map<Integer, String> mapeamento = mapeiaLinhaPedido(row, template.getQuantidadeCelulas());

                    if (isPedidoLote)
                         linhaValidada = isLinhaValidaLote(mapeamento, row.getRowNum()+1, listaCpfCnpj, template, mapCnpjDataCredito, arquivoCarga.getTipoPagamentoPedido(), optListaUnidadeEntrega);
                    else
                         linhaValidada = isLinhaValidaPortaAPorta(mapeamento, row.getRowNum()+1, listaCpfCnpj, template, mapCnpjDataCredito, arquivoCarga.getTipoPagamentoPedido());

                    salvarRespostaValidacao(arquivoCarga.getId(), linhaValidada);

                    isConteudoValido = isConteudoValido ? linhaValidada.isValido() : isConteudoValido;
               }
               controleLinhas++;
          }

          return isConteudoValido;

     }

     private Map<Integer, String> mapeiaLinhaPedido(Row row, Integer quantidadeCelulas) {

          int celula = 1;
          final Map<Integer, String> mapeamento = new HashMap<>();
          for (final Cell cell : row) {
               if (celula > quantidadeCelulas)
                    break;

               mapeamento.put(celula, cell.toString());
               celula++;
          }
          return mapeamento;
     }

     private boolean isCelulasObrigatoriasVazio(final Row row, final List<RhHeaderDefinition> listHeader) {

          boolean linhaVazia = false;
          for (RhHeaderDefinition h : listHeader) {
               if (h.isObrigatorio()) {
                    if (Objects.isNull(row.getCell(h.getPosicaoCelula())) || StringUtils.isEmpty(row.getCell(h.getPosicaoCelula()).toString()))
                         linhaVazia = true;
                    else {
                         linhaVazia = false;
                         break;
                    }
               }
          }
          return linhaVazia;
     }

     private String limpaString(String str) {

          if (!Objects.isNull(str)) {
               str = str.replace("\u00A0", "").replace("\u2007", "").replace("\u202F", "").replace("\u0009", "").trim();
          }

          return str;
     }

     private LinhaValidadaExcel isLinhaValidaPortaAPorta(final Map<Integer, String> conteudo, final Integer numeroLinhaExcel, final List<String> listaCpfCnpj, final TemplatesExcel template, final Map<String, String> mapCnpjDataCredito, final String tipoPagamento) {

          final LinhaValidadaExcel linhaValidada = LinhaValidadaExcel.builder().isValido(true).numeroLinhaExcel(numeroLinhaExcel).erros(new ArrayList<>()).build();
          ArquivosCargasDetalhes detalhe = new ArquivosCargasDetalhes();
          detalhe.setNumeroLinhaArquivo(numeroLinhaExcel);
          linhaValidada.setDetalhe(detalhe);

          if (conteudo.size() != template.getQuantidadeCelulas()) {
               linhaValidada.getErros().add("Quantidade de células preenchidas diferente do esperado.");
               linhaValidada.setValido(false);
               return linhaValidada;
          }

          try {
               detalhe.setCnpj(ValidacaoTemplate.validacaoCpfCnpj(limpaString(conteudo.get(1)), Documentos.CNPJ));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setCodUnidade(ValidacaoTemplate.validacaoCodigoUnidade(limpaString(conteudo.get(2))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setMatricula(ValidacaoTemplate.validacaoMatricula(limpaString(conteudo.get(3))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setNome(ValidacaoTemplate.validaNomeCompleto(limpaString(conteudo.get(4))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setCpf(ValidacaoTemplate.validacaoCpfCnpj(limpaString(conteudo.get(5)), Documentos.CPF));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setDataNascimento(ValidacaoTemplate.validaDataNascimento(limpaString(conteudo.get(6))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setProduto(ValidacaoTemplate.validaProdutos(limpaString(conteudo.get(7))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setValorBeneficio(ValidacaoTemplate.validaValorCredito(limpaString(conteudo.get(8))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               final String dataCredito = ValidacaoTemplate.validaDataCredito(limpaString(conteudo.get(9)));
               detalhe.setDataCredito(dataCredito);
               detalhe.setDataAgendamentoCarga(LocalDate.parse(dataCredito, DATETIME_FORMAT));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setLogradouro(ValidacaoTemplate.validaLogradouro(limpaString(conteudo.get(10))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setEndereco(ValidacaoTemplate.validaEndereco(limpaString(conteudo.get(11))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setNumero(ValidacaoTemplate.validaNumero(limpaString(conteudo.get(12))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setComplemento(ValidacaoTemplate.validaComplemento(limpaString(conteudo.get(13))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setBairro(ValidacaoTemplate.validaBairro(limpaString(conteudo.get(14))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setCidade(ValidacaoTemplate.validaCidade(limpaString(conteudo.get(15))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setUf(ValidacaoTemplate.validaUF(limpaString(conteudo.get(16))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setCep(ValidacaoTemplate.validaCEP(limpaString(conteudo.get(17))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          if (Objects.nonNull(detalhe.getCnpj()) && Objects.nonNull(detalhe.getDataCredito()))
               this.validacoesNegocioCnpj(linhaValidada, detalhe.getCnpj(), detalhe.getDataCredito(), mapCnpjDataCredito);

          if (Objects.nonNull(detalhe.getCnpj()))
               this.validacoesNegocioCnpj(linhaValidada, detalhe.getCnpj(), listaCpfCnpj);

          if (Objects.nonNull(detalhe.getDataCredito()))
               this.validacoesNegocioDataCredito(linhaValidada, detalhe.getDataCredito(), tipoPagamento);

          return linhaValidada;
     }

     private LinhaValidadaExcel isLinhaValidaLote(final Map<Integer, String> conteudo, final Integer numeroLinhaExcel, final List<String> listaCpfCnpj, final TemplatesExcel template, final Map<String, String> mapCnpjDataCredito, final String tipoPagamento, Optional<List<UnidadeEntrega>> optListaUnidadeEntrega) {

          final LinhaValidadaExcel linhaValidada = LinhaValidadaExcel.builder().isValido(true).numeroLinhaExcel(numeroLinhaExcel).erros(new ArrayList<>()).build();
          ArquivosCargasDetalhes detalhe = new ArquivosCargasDetalhes();
          detalhe.setNumeroLinhaArquivo(numeroLinhaExcel);
          linhaValidada.setDetalhe(detalhe);

          if (conteudo.size() != template.getQuantidadeCelulas()) {
               linhaValidada.getErros().add("Quantidade de células preenchidas diferente do esperado.");
               linhaValidada.setValido(false);
               return linhaValidada;
          }

          try {
               detalhe.setCnpj(ValidacaoTemplate.validacaoCpfCnpj(limpaString(conteudo.get(1)), Documentos.CNPJ));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               UnidadeEntrega unidadeEntrega = ValidacaoTemplate.validacaoCodigoUnidadeLote(limpaString(conteudo.get(2)), optListaUnidadeEntrega);
               detalhe.setCodUnidade(unidadeEntrega.getCodigoUnidadeEntrega());
               detalhe.setLogradouro(unidadeEntrega.getLogradouro());
               detalhe.setEndereco(unidadeEntrega.getEndereco());
               detalhe.setNumero(unidadeEntrega.getEnderecoNumero());
               detalhe.setComplemento(unidadeEntrega.getEnderecoComplemento());
               detalhe.setBairro(unidadeEntrega.getBairro());
               detalhe.setCidade(unidadeEntrega.getCidade());
               detalhe.setUf(unidadeEntrega.getUf());
               detalhe.setCep(unidadeEntrega.getCep());
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setMatricula(ValidacaoTemplate.validacaoMatricula(limpaString(conteudo.get(3))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setNome(ValidacaoTemplate.validaNomeCompleto(limpaString(conteudo.get(4))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setCpf(ValidacaoTemplate.validacaoCpfCnpj(limpaString(conteudo.get(5)), Documentos.CPF));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setDataNascimento(ValidacaoTemplate.validaDataNascimento(limpaString(conteudo.get(6))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setProduto(ValidacaoTemplate.validaProdutos(limpaString(conteudo.get(7))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               detalhe.setValorBeneficio(ValidacaoTemplate.validaValorCredito(limpaString(conteudo.get(8))));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          try {
               final String dataCredito = ValidacaoTemplate.validaDataCredito(limpaString(conteudo.get(9)));
               detalhe.setDataCredito(dataCredito);
               detalhe.setDataAgendamentoCarga(LocalDate.parse(dataCredito, DATETIME_FORMAT));
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }

          if (Objects.nonNull(detalhe.getCnpj()) && Objects.nonNull(detalhe.getDataCredito()))
               this.validacoesNegocioCnpj(linhaValidada, detalhe.getCnpj(), detalhe.getDataCredito(), mapCnpjDataCredito);

          if (Objects.nonNull(detalhe.getCnpj()))
               this.validacoesNegocioCnpj(linhaValidada, detalhe.getCnpj(), listaCpfCnpj);

          if (Objects.nonNull(detalhe.getDataCredito()))
               this.validacoesNegocioDataCredito(linhaValidada, detalhe.getDataCredito(), tipoPagamento);

          return linhaValidada;
     }

     private void validacoesNegocioDataCredito(LinhaValidadaExcel linhaValidada, String dataCredito, String tipoPagamento) {

          try {
               validaDataCreditoPorTipoContrato(dataCredito, tipoPagamento);
          } catch (BusinessException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }
     }
     
     private void validaDataCreditoPorTipoContrato(String dataCredito, String tipoPagamento) throws BusinessException {

          final List<LocalDate> datas = dataService.diasUteis(LocalDate.now(), LocalDate.parse(dataCredito, DATETIME_FORMAT));
          
          TipoContratoEnum tipoContrato = TipoContratoEnum.getByName(tipoPagamento);
          
          Integer qtdDiasUteis = tipoContratoFactory.newStrategy(tipoContrato).obtemQuantidadeDiasMinimoUploadPedido();

          if (datas.size() < qtdDiasUteis)
               throw new BusinessException(String.format("A data de crédito do benefício não pode ser inferior a %s dias a contar da data do pedido", qtdDiasUteis));
     }

     private void buscaCnpjEmLista(String cnpj, List<String> listaCpfCnpj) throws ConteudoExcelException {

          if (!listaCpfCnpj.contains(cnpj))
               throw new ConteudoExcelException(String.format("Usuário sem permissão para gerar carga para o CNPJ %s", cnpj));
     }

     @Transactional
     public void deletarArquivoCargasDetalhadoPor(Long idArquivoCarga) {

          this.arquivosCargasDetalhesRepository.deleteByIdArquivoCargas(idArquivoCarga);
     }

     private void validaDataCreditoPorCnpj(final Map<String, String> mapCnpjDataCredito, String cnpj, String dataCredito) throws BusinessException {

          String dataCreditoArmazenada = mapCnpjDataCredito.get(cnpj);
          if (Objects.isNull(dataCreditoArmazenada))
               mapCnpjDataCredito.put(cnpj, dataCredito);
          else
               comparaDatasCredito(dataCreditoArmazenada, dataCredito, cnpj);
     }

     private void comparaDatasCredito(String dataCreditoArmazenada, String dataCredito, String cnpj) throws BusinessException {

          if (!StringUtils.equals(dataCreditoArmazenada, dataCredito))
               throw new BusinessException(String.format("Existem datas de crédito diferentes para o CNPJ %s", cnpj));
     }

     private void validacoesNegocioCnpj(LinhaValidadaExcel linhaValidada, String cnpj, List<String> listaCpfCnpj) {

          try {
               buscaCnpjEmLista(cnpj, listaCpfCnpj);
          } catch (ConteudoExcelException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }
     }

     private void validacoesNegocioCnpj(LinhaValidadaExcel linhaValidada, String cnpj, String dataCredito, Map<String, String> mapCnpjDataCredito) {

          try {
               validaDataCreditoPorCnpj(mapCnpjDataCredito, cnpj, dataCredito);
          } catch (BusinessException e) {
               linhaValidada.getErros().add(e.getMessage());
               linhaValidada.setValido(false);
          }
     }

     public PagePedidoFuncionariosDetalhesResponse listaDetalhesFuncionariosCarga(PedidoDetalheFuncionarioRequest request, Long idCargaBeneficio, Integer page, Integer size) {

          UsuarioResponse usuarioExiste = usuarioPierRestRepository.consultarUsuario(request.getIdUsuarioSessao());
          NoContentCdt.checkThrow(Objects.isNull(usuarioExiste), ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO);

          Optional<List<EmpresasCargas>> empresasCarga = empresasCargasRepository.findByIdCargaBeneficio(idCargaBeneficio);
          NoContentCdt.checkThrow(!empresasCarga.isPresent(), ExceptionsMessagesCdtEnum.CARGA_BENEFICIO_INEXISTENTE);

          Long idEmpresa = empresasCarga.get().get(0).getIdEmpresa();
          Optional<Empresa> empresa = empresasRepository.findById(idEmpresa);
          NoContentCdt.checkThrow(!empresa.isPresent(), ExceptionsMessagesCdtEnum.EMPRESA_INEXISTENTE);

          Long idSubGrupoEmpresa = empresa.get().getIdGrupoEmpresa();
          NoContentCdt.checkThrow(Objeto.isBlank(idSubGrupoEmpresa), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_INEXISTENTE);

          Optional<GrupoEmpresa> grupoEmpresa = grupoEmpresaRepository.findById(idSubGrupoEmpresa);
          NoContentCdt.checkThrow(!grupoEmpresa.isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_INEXISTENTE);

          dominioStatusCargas = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, ATIVO.name());

          Optional<PermissoesUsuariosRh> optPermissao = permissoesUsuariosRhRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(request.getIdUsuarioSessao(), grupoEmpresa.get().getIdGrupoEmpresaPai(), statusPermissaoAtiva.getStatus());
          ForbiddenCdt.checkThrow(!optPermissao.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          List<Integer> idEmpresasPermissao = permissoesUsuariosRhRepository.findEmpresasbyIdPermissao(optPermissao.get().getId());
          ForbiddenCdt.checkThrow(idEmpresasPermissao.size() == 0, ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);

          ForbiddenCdt.checkThrow(!idEmpresasPermissao.contains(idEmpresa.intValue()), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);
          
          List<Integer> listaIdFuncionarios = cargaBeneficioCustomRepository.listaIdFuncionariosCarga(idCargaBeneficio, statusPedidoSemErroDeProcessamento());

		  NoContentCdt.checkThrow(Objeto.isBlank(listaIdFuncionarios), ExceptionsMessagesCdtEnum.NAO_HA_FUNCIONARIOS_CARGA_BENEFICIO);

          Pageable pageable = PageRequest.of(page, size);
          List<FuncionarioCargaBeneficio> listaFuncionariosPaginado = cargaBeneficioCustomRepository.listaDetalhesFuncionariosCarga(idCargaBeneficio,statusPedidoSemErroDeProcessamento(),listaIdFuncionarios, pageable);
          PageResponse<PedidoDetalheFuncionarioResponse> pageResponse = new PageResponse<PedidoDetalheFuncionarioResponse>();
          NoContentCdt.checkThrow(CollectionUtils.isEmpty(listaFuncionariosPaginado), ExceptionsMessagesCdtEnum.NAO_HA_DETALHES_CNPJ);

          pageResponse.setContent(mapeiaFuncionarioCargaBeneficio(listaFuncionariosPaginado));

          pageResponse = PageUtils.ajustarPageResponse(pageResponse, page, size, listaIdFuncionarios.size());

          return new PagePedidoFuncionariosDetalhesResponse(pageResponse);
     }

     private List<PedidoDetalheFuncionarioResponse> mapeiaFuncionarioCargaBeneficio(List<FuncionarioCargaBeneficio> funcionariosProdutos) {

          PedidoDetalheFuncionarioResponse funcionario = PedidoDetalheFuncionarioResponse.builder()
                    .idFuncionario(funcionariosProdutos.get(0).getIdFuncionario())
                    .nome(funcionariosProdutos.get(0).getNome())
                    .cpf(funcionariosProdutos.get(0).getCpf())
                    .logradouro(funcionariosProdutos.get(0).getLogradouro())
                    .complemento(funcionariosProdutos.get(0).getLogradouro())
                    .bairro(funcionariosProdutos.get(0).getBairro())
                    .cidade(funcionariosProdutos.get(0).getCidade())
                    .uf(funcionariosProdutos.get(0).getUf())
                    .cep(funcionariosProdutos.get(0).getCep()).build();

          PedidoDetalheFuncionarioProdutoResponse produto = PedidoDetalheFuncionarioProdutoResponse.builder()
                    .idProduto(funcionariosProdutos.get(0).getIdProduto())
                    .nomeProduto(funcionariosProdutos.get(0).getNomeProduto())
                    .valorCarga(funcionariosProdutos.get(0).getValorCarga())
                    .indicadorEmissaoCartao(funcionariosProdutos.get(0).getIndicadorEmissaoCartao()).build();

          List<PedidoDetalheFuncionarioProdutoResponse> produtos = new ArrayList<>();
          produtos.add(produto);

          List<PedidoDetalheFuncionarioResponse> funcionarios = new ArrayList<>();
          for (int i = 1; i <= funcionariosProdutos.size() - 1; i++) {
               if (funcionario.getIdFuncionario().compareTo(funcionariosProdutos.get(i).getIdFuncionario()) == 0) {

            	   PedidoDetalheFuncionarioProdutoResponse outroProduto = PedidoDetalheFuncionarioProdutoResponse.builder()
                              .idProduto(funcionariosProdutos.get(i).getIdProduto())
                              .nomeProduto(funcionariosProdutos.get(i).getNomeProduto())
                              .valorCarga(funcionariosProdutos.get(i).getValorCarga())
                              .indicadorEmissaoCartao(funcionariosProdutos.get(i).getIndicadorEmissaoCartao()).build();
                    produtos.add(outroProduto);
               } else {
                    funcionario.setProdutos(produtos);
                    funcionarios.add(funcionario);

                    funcionario = PedidoDetalheFuncionarioResponse.builder()
                              .idFuncionario(funcionariosProdutos.get(i).getIdFuncionario())
                              .nome(funcionariosProdutos.get(i).getNome())
                              .cpf(funcionariosProdutos.get(i).getCpf())
                              .logradouro(funcionariosProdutos.get(i).getLogradouro())
                              .complemento(funcionariosProdutos.get(i).getLogradouro())
                              .bairro(funcionariosProdutos.get(i).getBairro())
                              .cidade(funcionariosProdutos.get(i).getCidade())
                              .uf(funcionariosProdutos.get(i).getUf())
                              .cep(funcionariosProdutos.get(i).getCep()).build();

                    produto = PedidoDetalheFuncionarioProdutoResponse.builder()
                              .idProduto(funcionariosProdutos.get(i).getIdProduto())
                              .nomeProduto(funcionariosProdutos.get(i).getNomeProduto())
                              .valorCarga(funcionariosProdutos.get(i).getValorCarga())
                              .indicadorEmissaoCartao(funcionariosProdutos.get(i).getIndicadorEmissaoCartao()).build();

                    produtos = new ArrayList<>();
                    produtos.add(produto);
               }
          }
          funcionario.setProdutos(produtos);
          funcionarios.add(funcionario);

          return funcionarios;
     }

     private Integer statusPedidoSemErroDeProcessamento() {
          
          dominioStatusCargas = this.statusCached.busca(TipoStatus.EMPRESAS_CARGAS_DETALHES_PRODUTOS.getNome());
          StatusDescricao statusErroNoProcessamento = mapStatusDescricao(dominioStatusCargas, StatusEmpresaCargaDetalheProdutoEnum.ERRO_NO_PROCESSAMENTO.getStatus());
          
          return statusErroNoProcessamento.getCodigoStatus();
     }
     
     public void cancelarPedidoDetalhes(Long idPedido, Long idCarga, Long idUsuario) {

          usuarioPierService.autenticarUsuario(idUsuario);

          Optional<ArquivoCargas> arquivo = buscaPedido(idPedido);

          CargaBeneficio cargaBeneficio = cargaBeneficioService.getCargaBeneficio(idCarga);

          validaStatusPedidoDetalhes(arquivo.get().getStatus(), cargaBeneficio.getStatus());

          PreconditionCustom.checkThrow(arquivo.get().getFlagFaturamentoCentralizado(), ExceptionsMessagesCdtEnum.TIPO_FATURAMENTO_NAO_PERMITE_CANCELAMENTO);

          validaPermissaENivelUsuario(idUsuario, arquivo.get().getIdGrupoEmpresa(), cargaBeneficio.getIdEmpresa());

          boolean isPeriodoForaRegraCancelamento = !isPeriodoDentroRegraCancelamento(cargaBeneficio.getIdGrupoEmpresa(), cargaBeneficio.getIdArquivoCargas() ,cargaBeneficio.getDataAgendamento());
          PreconditionCustom.checkThrow(isPeriodoForaRegraCancelamento, CARGA_FORA_DO_PERIODO_DE_CANCELAMENTO);

          cancelaPedidoCustomRepositoryImpl.cancelaPedidoDetalhesProc(idCarga);

     }

     private void validaPermissaENivelUsuario(Long idUsuario, Long idGrupo, Long idEmpresa) {

          PermissoesUsuariosRh permissaoUsuario = permissoesUsuariosRhService.autenticarPermissaoAtiva(idUsuario, idGrupo);

          ForbiddenCdt.checkThrow(!permissoesUsuariosRhService.isPossuiAutorizacaoEmpresa(permissaoUsuario.getId(), idEmpresa), ExceptionsMessagesCdtEnum.NAO_POSSUI_PERMISSAO_PARA_CANCELAR_CARGA);

     }

     private void validaStatusPedidoDetalhes(Integer statusArquivoStd, Integer statusCarga) {

          PreconditionCustom.checkThrow(!isIdStatusPedidoIgualA(statusArquivoStd, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO)
                    && !isIdStatusPedidoIgualA(statusArquivoStd, StatusArquivoEnum.CANCELADO_PARCIAL),
                    ExceptionsMessagesCdtEnum.STATUS_ARQUIVO_NAO_PERMITE_CANCELAMENTO);

          PreconditionCustom.checkThrow(!isStatusCargaBeneficioIgualA(statusCarga, StatusCargaBeneficioEnum.AGUARDANDO_PAGAMENTO)
                    && !isStatusCargaBeneficioIgualA(statusCarga, StatusCargaBeneficioEnum.CARGA_EFETUADA)
                    && !isStatusCargaBeneficioIgualA(statusCarga, StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO)
                    && !isStatusCargaBeneficioIgualA(statusCarga, StatusCargaBeneficioEnum.PENDENCIA_LIMITE),
                    ExceptionsMessagesCdtEnum.STATUS_CARGA_NAO_PERMITE_CANCELAMENTO);

     }

     private Optional<ArquivoCargas> buscaPedido(Long id) {

          Optional<ArquivoCargas> arquivo = arquivoCargasRepository.findById(id);

          NotFoundCdt.checkThrow(!arquivo.isPresent(), ExceptionsMessagesCdtEnum.ARQUIVO_NAO_ENCONTRADO);

          return arquivo;
     }

     private boolean isIdStatusPedidoIgualA(Integer statusPedido, StatusArquivoEnum statusArquivoEnum) {

          return mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS.getNome()), statusArquivoEnum.getStatus()).getStatus().equals(statusPedido);

     }

     private boolean isIdStatusPagamentoIgualA(Integer statusPagamento, StatusPagamentoEnum statusPagamentoEnum) {

          return mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome()), statusPagamentoEnum.getStatus()).getStatus().equals(statusPagamento);

     }

     private boolean isStatusCargaBeneficioIgualA(Integer statusCargaBeneficio, StatusCargaBeneficioEnum statusCargaBeneficioEnum) {

          return mapStatusDescricao(this.statusCached.busca(CARGAS_BENEFICIOS.getNome()), statusCargaBeneficioEnum.getStatus()).getStatus().equals(statusCargaBeneficio);

     }

     private boolean isDescricaoStatusCargaBeneficioIgualA(String statusCargaBeneficio, StatusCargaBeneficioEnum statusCargaBeneficioEnum) {

          return mapStatusDescricao(this.statusCached.busca(CARGAS_BENEFICIOS.getNome()), statusCargaBeneficioEnum.getStatus()).getDescricao().equals(statusCargaBeneficio);

     }

     private boolean isPeriodoDentroRegraCancelamento(Long idGrupoEmpresa, Long idCarga, LocalDateTime dataAgendamento) {

          Optional<ArquivoCargas> arquivo = this.buscaPedido(idCarga);
          Integer idStatusPagamento = arquivo.get().getStatusPagamento();

          Long periodo = ChronoUnit.DAYS.between(LocalDate.now().atStartOfDay(), dataAgendamento.with(LocalTime.MAX));

          if (!parametrosService.isTipoPagamentoPrePago(idGrupoEmpresa)) {
               if (isIdStatusPagamentoIgualA(idStatusPagamento, LIMITE_CREDITO_INSUFICIENTE) || isIdStatusPagamentoIgualA(idStatusPagamento, LIMITE_CREDITO_VENCIDO)) {
                    return periodo > MAIOR_INTERVALO_CANCELAMENTO_PEDIDO_45_DIAS;
               }
          }

          return periodo >= MENOR_INTERVALO_CANCELAMENTO_PEDIDO_1_DIA;
     }

     public Optional<List<ArquivosCargasDetalhes>> buscaListaDetalhesPorIdArquivo(Long idArquivoCargas) {

          return arquivosCargasDetalhesRepository.findByIdArquivoCargas(idArquivoCargas);
     }

     public ArquivosCargasDetalhes atualizarStatus(ArquivosCargasDetalhes arquivosCargasDetalhes, StatusDetalheEnum status) {
          
          final StatusDescricao statusDetalhe = mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS_DETALHES.getNome()), status.name());
          
          arquivosCargasDetalhes.setStatusDetalhe(statusDetalhe.getStatus());
          
          return arquivosCargasDetalhesRepository.save(arquivosCargasDetalhes);
     }
     
     public Long buscaQuantidadeRegistrosPor(Long idArquivoCargas, StatusDetalheEnum statusDetalhe) {
          
          final StatusDescricao status = mapStatusDescricao(this.statusCached.busca(ARQUIVO_CARGAS_DETALHES.getNome()), statusDetalhe.name());
          
          return arquivosCargasDetalhesRepository.countByIdArquivoCargasAndStatusDetalhe(idArquivoCargas, status.getStatus()).get();
     }
}

