
package br.com.conductor.rhblueapi.service.relatorios.gerenciador;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.relatorios.RelatorioPedidoCustom;
import br.com.conductor.rhblueapi.domain.relatorios.RelatorioPortal;
import br.com.conductor.rhblueapi.domain.relatorios.response.RelatorioResponse;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.enums.relatorios.status.StatusSolicitacaoRelatorio;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.relatorios.RelatorioDetalhesPedidosPublish;
import br.com.conductor.rhblueapi.service.relatorios.RelatorioPedidoService;
import br.com.conductor.rhblueapi.service.relatorios.RelatorioService;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.service.utils.StatusUtils;
import br.com.twsoftware.alfred.object.Objeto;

@Component
public class GerenciadorRelatorioDetalhesPedidos {

     @Autowired
     private RelatorioService relatorioService;

     @Autowired
     private RelatorioPedidoService relatorioPedidoService;

     @Autowired
     private RelatorioDetalhesPedidosPublish relatorioDetalhesPedidosPublish;

     @Autowired
     private StatusCached statusCached;

     public void verificaSolicitacaoRelatorio(final RelatorioResponse relatorioResponse) {

          Optional<RelatorioPortal> optRelatorioPortal = relatorioService.obterRelatorioPorId(relatorioResponse.getId());

          if (Boolean.FALSE.equals(existeRelatorioPortal(optRelatorioPortal))) {
               enviarFilaDlq(relatorioResponse);
          } else {
               RelatorioPortal relatorioPortal = optRelatorioPortal.get();
               List<RelatorioPedidoCustom> dados = relatorioPedidoService.obterDadosRelatorioPedidoCustom(relatorioPortal.getIdGrupoEmpresa(), relatorioPortal.getDataInicio(), relatorioPortal.getDataFim());

               List<StatusDescricao> dominioRelatorioPortal = statusCached.busca(TipoStatus.RELATORIOS.getNome());
               if (Objeto.isBlank(dados)) {
                    StatusDescricao statusNaoHaDados = StatusUtils.mapStatusDescricao(dominioRelatorioPortal, StatusSolicitacaoRelatorio.NAO_HA_PEDIDOS.getDescricao());
                    relatorioPortal.setStatus(statusNaoHaDados.getStatus());
               } else {
                    StatusDescricao statusProcessado = StatusUtils.mapStatusDescricao(dominioRelatorioPortal, StatusSolicitacaoRelatorio.PROCESSADO.getDescricao());
                    relatorioPortal.setStatus(statusProcessado.getStatus());
               }
               relatorioService.salvar(relatorioPortal);
          }

     }

     private boolean existeRelatorioPortal(final Optional<RelatorioPortal> optRelatorioPortal) {

          return optRelatorioPortal.isPresent();
     }

     private void enviarFilaDlq(final RelatorioResponse relatorioResponse) {

          relatorioDetalhesPedidosPublish.enviarIdentificadorFilaDlq(relatorioResponse);
     }

}
