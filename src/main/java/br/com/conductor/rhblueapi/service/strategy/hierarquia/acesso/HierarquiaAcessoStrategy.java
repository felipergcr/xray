package br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso;

import java.util.List;
import java.util.Map;

import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;

public abstract class HierarquiaAcessoStrategy {
     
     public abstract List<UsuarioPermissaoNivelAcessoRh> retornaAcessoPermitido(Map<String, Object> acessoUserLogado, Map<String, Object> acessoUserPretendido);


}
