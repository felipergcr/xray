package br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.domain.request.EmpresaRequest;
import br.com.conductor.rhblueapi.domain.response.EmpresaCustomResponse;
import br.com.conductor.rhblueapi.domain.response.EmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoSubGrupoResponse;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.service.EmpresaService;
import br.com.conductor.rhblueapi.service.GrupoEmpresaService;

@Service
public class HierarquiaAcessoHibridoStrategy  extends HierarquiaAcessoStrategy {

     private static final String ACESSOS = "acessos";

     private static String NIVEL_PERMISSAO_ACESSO = "nivelPermissao";
     
     @Autowired
     private EmpresaService empresaService;
     
     @Autowired
     private GrupoEmpresaService grupoEmpresaService;
     
     @Override
     public List<UsuarioPermissaoNivelAcessoRh> retornaAcessoPermitido(Map<String, Object> acessoUserLogado, Map<String, Object> acessoUserPretendido) {

          List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido = getAcessos(acessoUserPretendido);

          List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado = getAcessos(acessoUserLogado);
          
          List<UsuarioPermissaoNivelAcessoRh> acessoPermitido = new ArrayList<>();
        
          switch (NivelPermissaoEnum.valueOf(acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).toString())) {
               case GRUPO:
                    acessoPermitido = executaCenarioGrupo(nivelHibridoUserPretendido, nivelAcessoUserLogado);
                    break;
               case SUBGRUPO:
                    acessoPermitido = executaCenarioSubgrupo(nivelHibridoUserPretendido, nivelAcessoUserLogado);
                    break;
               case EMPRESA:
                    acessoPermitido = executaCenarioEmpresa(nivelHibridoUserPretendido, nivelAcessoUserLogado);
                    break;
               case HIBRIDO:
                    acessoPermitido = executaCenarioHibrido(nivelHibridoUserPretendido, nivelAcessoUserLogado);
                    break;
          }
          
          return acessoPermitido;
     }
     
     public List<HierarquiaAcessoSubGrupoResponse> mapHierarquiaSugropoHibrido(List<UsuarioPermissaoNivelAcessoRh> acesso) {

          List<HierarquiaAcessoSubGrupoResponse> listNivelSubGrupos = new ArrayList<>();
          
          if(acesso.isEmpty()){
               return null;
          }
          
          acesso.stream().filter(nivel -> Objects.nonNull(nivel.getIdSubgrupoEmpresa())).forEach(action ->{
               
               Optional<GrupoEmpresa> subgrupoEmpresa = grupoEmpresaService.buscaGrupoEmpresaPor(action.getIdSubgrupoEmpresa());
               
               if (subgrupoEmpresa.isPresent()) {
                    
                    HierarquiaAcessoSubGrupoResponse subrupo = HierarquiaAcessoSubGrupoResponse
                              .builder()
                              .idSubgrupo(subgrupoEmpresa.get().getId())
                              .nomeDescricao(subgrupoEmpresa.get().getNomeGrupo())
                              .build();

                    List<UsuarioPermissaoNivelAcessoRh> empresas = acesso.stream().filter(nivel -> Objects.nonNull(nivel.getEmpresa()) && nivel.getEmpresa().getIdGrupoEmpresa().equals(subrupo.getIdSubgrupo())).collect(Collectors.toList());
                    
                    subrupo.setEmpresas(buildHierarquiaEmpresa(empresas));
                    
                    listNivelSubGrupos.add(subrupo);
               }
               
          });
          
          acesso.stream().filter(nivel -> Objects.nonNull(nivel.getEmpresa())).forEach(action -> {
               
                    Optional<GrupoEmpresa> subgrupoPorEmpresa = grupoEmpresaService.buscaGrupoEmpresaPor(action.getEmpresa().getIdGrupoEmpresa());

                    List<Long> idSubgrupo = listNivelSubGrupos.stream().map(HierarquiaAcessoSubGrupoResponse::getIdSubgrupo).collect(Collectors.toList());
                    
                    if (subgrupoPorEmpresa.isPresent() && !idSubgrupo.contains(action.getEmpresa().getIdGrupoEmpresa())) {

                         HierarquiaAcessoSubGrupoResponse subgrupo = HierarquiaAcessoSubGrupoResponse.builder()
                                   .idSubgrupo(subgrupoPorEmpresa.get().getId())
                                   .nomeDescricao(subgrupoPorEmpresa.get().getNomeGrupo())
                                   .build();

                         List<UsuarioPermissaoNivelAcessoRh> empresas = acesso.stream().filter(nivel -> Objects.nonNull(nivel.getEmpresa()) && nivel.getEmpresa().getIdGrupoEmpresa().equals(subgrupoPorEmpresa.get().getId())).collect(Collectors.toList());

                         subgrupo.setEmpresas(buildHierarquiaEmpresa(empresas));

                         listNivelSubGrupos.add(subgrupo);
                    }
          });
          
          return listNivelSubGrupos;
                
     }

     private List<UsuarioPermissaoNivelAcessoRh> executaCenarioGrupo(List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado) {

          Long idGrupoUserLogado = nivelAcessoUserLogado.stream().mapToLong(UsuarioPermissaoNivelAcessoRh::getIdGrupoEmpresa).sum();
          
          return  filtraAcessoPorGrupo(nivelHibridoUserPretendido, idGrupoUserLogado);
     }

     private List<UsuarioPermissaoNivelAcessoRh> executaCenarioSubgrupo(List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado) {

          List<Long> idSugrupoUserLogado = nivelAcessoUserLogado.stream().filter(nivel -> Objects.nonNull(nivel.getIdSubgrupoEmpresa())).map(UsuarioPermissaoNivelAcessoRh::getIdSubgrupoEmpresa).collect(Collectors.toList());
         
          List<UsuarioPermissaoNivelAcessoRh> acessoEmpresasPorSubgrupo = filtraAcessoEmpresaPorSubgrupo(nivelHibridoUserPretendido, nivelAcessoUserLogado, idSugrupoUserLogado);
          
          List<UsuarioPermissaoNivelAcessoRh> acessoSubgruposporSubgrupo = filtraAcessoSubgrupoPorSugrupo(nivelHibridoUserPretendido, idSugrupoUserLogado);
          
          return  mergeListAcessos(acessoSubgruposporSubgrupo,acessoEmpresasPorSubgrupo);
     }

     private List<UsuarioPermissaoNivelAcessoRh> executaCenarioEmpresa(List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado) {

          List<UsuarioPermissaoNivelAcessoRh> acessoEmpresasporEmpresas  = filtraAcessoPorEmpresa(nivelHibridoUserPretendido, nivelAcessoUserLogado);
          
          List<UsuarioPermissaoNivelAcessoRh> acessoSubgruposPorEmpresas  = filtraAcessoSubgrupoPorSugrupo( nivelHibridoUserPretendido, nivelAcessoUserLogado.stream().filter(nivel -> Objects.nonNull(nivel.getEmpresa())).map(nivel -> nivel.getEmpresa().getIdGrupoEmpresa()).collect(Collectors.toList()));
          
          return  mergeListAcessos(acessoSubgruposPorEmpresas,acessoEmpresasporEmpresas);
     }

     private List<UsuarioPermissaoNivelAcessoRh> executaCenarioHibrido(List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado) {

          List<UsuarioPermissaoNivelAcessoRh> acessoPermitido;
         
          List<Long> idSugrupoEmpresaUserLogado = nivelAcessoUserLogado.stream().filter(nivel -> Objects.nonNull(nivel.getIdSubgrupoEmpresa())).map(UsuarioPermissaoNivelAcessoRh::getIdSubgrupoEmpresa).collect(Collectors.toList());
          
          List<UsuarioPermissaoNivelAcessoRh> acessoHibridoEmpresasPorSubgrupo = filtraAcessoEmpresaPorSubgrupo(nivelHibridoUserPretendido, nivelAcessoUserLogado, idSugrupoEmpresaUserLogado);
          
          List<UsuarioPermissaoNivelAcessoRh> acessoHibridoSubgrupos = filtraAcessoSubgrupoPorSugrupo(nivelHibridoUserPretendido, idSugrupoEmpresaUserLogado);
          
          acessoPermitido = mergeListAcessos(acessoHibridoSubgrupos,acessoHibridoEmpresasPorSubgrupo);
          
          List<UsuarioPermissaoNivelAcessoRh> acessoHibridoEmpresas  = filtraAcessoPorEmpresa(nivelHibridoUserPretendido, nivelAcessoUserLogado);
          
          acessoPermitido = mergeListAcessos(acessoPermitido,acessoHibridoEmpresas);
         
          return acessoPermitido;
     }
     
     private List<UsuarioPermissaoNivelAcessoRh> filtraAcessoEmpresaPorSubgrupo(List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado, List<Long> idSubgrupoUserLogado) {

          List<EmpresaCustomResponse> empresaSubgrupoUserLogado = buscaEmpresaPorSubgrupo(idSubgrupoUserLogado);
          
          List<Long> idEmpresasSubgrupoHibridoUserLogado = empresaSubgrupoUserLogado.stream().map(EmpresaCustomResponse::getId).collect(Collectors.toList());

          return nivelHibridoUserPretendido.stream().filter(nivel -> idEmpresasSubgrupoHibridoUserLogado.contains(nivel.getIdEmpresa())).collect(Collectors.toList());
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtraAcessoPorGrupo(List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido, Long idGrupoUserLogado) {

          Optional<Long> idSubgrupoUserPretendido = nivelHibridoUserPretendido.stream().filter(nivel -> Objects.nonNull(nivel.getEmpresa())).findFirst().map(empresa -> empresa.getEmpresa().getIdGrupoEmpresa());

          Long idGrupoUserPretendido = grupoEmpresaService.buscaGrupoEmpresaPor(idSubgrupoUserPretendido.get()).filter(grupo -> Objects.nonNull(grupo.getIdGrupoEmpresaPai())).map(GrupoEmpresa::getIdGrupoEmpresaPai).orElse(0l);

          return (idGrupoUserPretendido.equals(idGrupoUserLogado)) ? nivelHibridoUserPretendido : Collections.emptyList();
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtraAcessoSubgrupoPorSugrupo(List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido, List<Long> idSugrupoHibridoUserLogado) {

          
          return nivelHibridoUserPretendido.stream().filter(actual -> idSugrupoHibridoUserLogado.contains(actual.getIdSubgrupoEmpresa())).collect(Collectors.toList());
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtraAcessoPorEmpresa(List<UsuarioPermissaoNivelAcessoRh> nivelHibridoUserPretendido, List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado) {

          List<Long> idEmpresaHibridoUserLogado = nivelAcessoUserLogado.stream().map(UsuarioPermissaoNivelAcessoRh::getIdEmpresa).collect(Collectors.toList());
          
          return nivelHibridoUserPretendido.stream().filter(nivel -> idEmpresaHibridoUserLogado.contains(nivel.getIdEmpresa())).collect(Collectors.toList());
  
     }

     private List<EmpresaCustomResponse> buscaEmpresaPorSubgrupo(List<Long> idSugrupoUserLogado) {

          List<EmpresaCustomResponse> empresaSubgrupoUserLogado = new ArrayList<>();
          
          idSugrupoUserLogado.stream().filter(ids -> Objects.nonNull(ids)).forEach(id ->{
               
               empresaSubgrupoUserLogado.addAll(empresaService.listarEmpresasdoSubGrupoEmpresa(id, 0, 10).content);
          });
          
          return empresaSubgrupoUserLogado;
     }

     private List<UsuarioPermissaoNivelAcessoRh> getAcessos(Map<String, Object> acesso) {

          return (List<UsuarioPermissaoNivelAcessoRh>) acesso.get(ACESSOS);
     }
     
     private List<UsuarioPermissaoNivelAcessoRh> mergeListAcessos(List<UsuarioPermissaoNivelAcessoRh> listAcessoPrimaria, List<UsuarioPermissaoNivelAcessoRh> listAcessoSecundaria) {

          List<UsuarioPermissaoNivelAcessoRh> acessosPermitidos = new ArrayList<>();

          listAcessoPrimaria.forEach(nivel -> acessosPermitidos.add(nivel));

          listAcessoSecundaria.forEach(nivel -> acessosPermitidos.add(nivel));

          return acessosPermitidos.stream().distinct().collect(Collectors.toList());
     }
     
     private List<HierarquiaAcessoEmpresaResponse> buildHierarquiaEmpresa(List<UsuarioPermissaoNivelAcessoRh> empresas) {
          
          if(empresas.isEmpty()) {
               return Collections.emptyList();
          }

          EmpresaRequest empresasRequest = new EmpresaRequest();

          empresasRequest.setIdsEmpresas(empresas.stream().map(empresa -> empresa.getIdEmpresa()).collect(Collectors.toList()));

          List<HierarquiaAcessoEmpresaResponse> listEmpresas = new ArrayList<>();

          List<EmpresaResponse> empresasFind = empresaService.listar(empresasRequest);
          
          empresasFind.stream().forEach(action -> {

               listEmpresas.add(HierarquiaAcessoEmpresaResponse.builder()
                         .cnpj(action.getCnpj())
                         .idEmpresa(action.getIdEmpresa())
                         .nomeEmpresa(action.getNomeEmpresa())
                         .build());
          });

          return listEmpresas;

     }
}
