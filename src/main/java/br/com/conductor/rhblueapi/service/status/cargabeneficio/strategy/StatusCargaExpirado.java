
package br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy;

import static br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum.EXPIRADO;

import br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum;

public class StatusCargaExpirado implements StatusCargaBeneficioStrategy {

     @Override
     public StatusPagamentoBaseEnum retornaStatusPagamento(boolean todosStatusIguais, int qtdCargasPedido, int qtdCargasPedidoPagas) {

          return EXPIRADO;
     }

}