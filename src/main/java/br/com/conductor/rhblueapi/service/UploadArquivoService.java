package br.com.conductor.rhblueapi.service;

import java.io.FileInputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.util.AppConstantes;

@Service
public class UploadArquivoService implements Serializable {
     
     /**
      * 
      */
     private static final long serialVersionUID = -8988808176383463757L;

     public Workbook lerPlanilha(MultipartFile file) {

          try {

               String temp = System.getProperty("java.io.tmpdir");

               // Salva o arquivo temporariamente
               byte[] bytes = file.getBytes();
               Path path = Paths.get(temp + file.getOriginalFilename());
               Files.write(path, bytes);

               FileInputStream arquivo = new FileInputStream(path.toString());

               // retorna XLS ou XLSX
               Workbook workbook = getWorkbook(arquivo, path.toString());

               return workbook;

          } catch (Exception e) {
               throw new BadRequestCdt(e.getMessage());
          }
     }

     private static Workbook getWorkbook(FileInputStream inputStream, String extensaoArquivo) {

          Workbook workbook = null;

          try {

               if (extensaoArquivo.endsWith(AppConstantes.FileExtension.XLSX)) {
                    workbook = new XSSFWorkbook(inputStream);
               } else if (extensaoArquivo.endsWith(AppConstantes.FileExtension.XLS)) {
                    workbook = new HSSFWorkbook(inputStream);
               } else {
                    throw new IllegalArgumentException(ExceptionsMessagesCdtEnum.PLANILHA_EXTENSAO.toString());
               }

          } catch (Exception e) {
               e.printStackTrace();
          }

          return workbook;
     }


}
