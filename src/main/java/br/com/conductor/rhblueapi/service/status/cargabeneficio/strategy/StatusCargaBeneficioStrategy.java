
package br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy;

import br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum;

public interface StatusCargaBeneficioStrategy {

     StatusPagamentoBaseEnum retornaStatusPagamento(boolean todosStatusIguais, int qtdCargasPedido, int qtdCargasPedidoPagas);

}
