
package br.com.conductor.rhblueapi.service.usecase;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import br.com.conductor.rhblueapi.domain.Empresa;
import br.com.conductor.rhblueapi.repository.EmpresaRepository;
import br.com.conductor.rhblueapi.repository.view.FuncionarioProdutoEmpresaRepository;

@Component
public class ValidacaoNegocio {

     private static final BigDecimal LIMITE_CARGA = new BigDecimal(5000);

     @Autowired
     private FuncionarioProdutoEmpresaRepository funcionarioProdutoEmpresaRepository;

     @Autowired
     private EmpresaRepository empresaRepository;

     public boolean isValid(final ArquivosCargasDetalhes detalhe) {

          final Empresa empresa = this.empresaRepository.findByPessoaDocumento(detalhe.getCnpj());

          final LocalDate start = detalhe.getDataAgendamentoCarga().withDayOfMonth(1);
          final LocalDate end = detalhe.getDataAgendamentoCarga().withDayOfMonth(detalhe.getDataAgendamentoCarga().lengthOfMonth());

          final BigDecimal valorCarga = this.funcionarioProdutoEmpresaRepository.findValor(detalhe.getCpf(), Long.parseLong(detalhe.getProduto()), empresa.getId(), start, end).orElse(new BigDecimal(0));

          return valorCarga.add(new BigDecimal(detalhe.getValorBeneficio())).compareTo(LIMITE_CARGA) < 0;
     }

}
