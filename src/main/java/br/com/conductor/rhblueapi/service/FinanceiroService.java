
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_ENCONTRADO_PRE_CONDITION;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.NOTA_FISCAL_PRE_CONDITION;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO_PRE_CONDITION;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.USUARIO_SEM_ACESSO_NOTA_FISCAL_DELOITTE;
import static br.com.conductor.rhblueapi.enums.StatusPermissaoEnum.ATIVO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.PERMISSOES_USUARIOS_RH;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;

import java.io.Serializable;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioResumido;
import br.com.conductor.rhblueapi.controleAcesso.repository.UsuarioRepository;
import br.com.conductor.rhblueapi.domain.ArquivoCargaFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.DadosPedidoFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.NotaFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.PedidoFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.deloitte.NotaFiscalResponse;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.ArquivoPedidoResponseCustom;
import br.com.conductor.rhblueapi.domain.response.CargaControleFinanceiroResponse;
import br.com.conductor.rhblueapi.domain.response.PageArquivoCargasResponseCustom;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.domain.view.DadoEmpresaRps;
import br.com.conductor.rhblueapi.domain.view.ProdutoRps;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;
import br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.report.rps.DemonstrativoRpsService;
import br.com.conductor.rhblueapi.repository.FinanceiroCargaControleFinanceiroCustomRepository;
import br.com.conductor.rhblueapi.repository.FinanceiroPedidosCustomRepository;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.PermissoesUsuariosRhRepository;
import br.com.conductor.rhblueapi.repository.deloitte.DeloitteRest;
import br.com.conductor.rhblueapi.repository.view.DadoEmpresaRpsRepository;
import br.com.conductor.rhblueapi.repository.view.ProdutoRpsRepository;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.PageUtils;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class FinanceiroService implements Serializable {

     private static final long serialVersionUID = 990584691059150355L;

     @Autowired
     private DadoEmpresaRpsRepository dadoEmpresaRpsRepository;

     @Autowired
     private ProdutoRpsRepository produtoRpsRepository;

     @Autowired
     private DemonstrativoRpsService demonstrativoRpsService;

     @Autowired
     private UsuarioRepository usuarioRepository;

     @Autowired
     private GrupoEmpresaRepository grupoEmpresaRepository;
     
     @Autowired
     private FinanceiroPedidosCustomRepository financeiroPedidosCustomRepository;
     
     @Autowired
     private StatusCached statusCached;
     
     private List<StatusDescricao> dominioStatusCargas;
     
     @Autowired
     private PermissoesUsuariosRhRepository permissoesUsuariosRhRepository;
     
     @Autowired
     private DeloitteRest deloitteService;
     
     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;
     
     @Autowired
     private UsuarioPierService usuarioPierService;
     
     @Autowired
     private GrupoEmpresaService grupoEmpresaService;
     
     @Autowired
     private FinanceiroCargaControleFinanceiroCustomRepository financeiroCargaControleFinanceiroCustomRepository;

     public PageArquivoCargasResponseCustom listarPedidosFinanceiro(Integer page, Integer size, ArquivoPedidoRequest request) {

          Optional<GrupoEmpresa> grupoEmpresa = this.grupoEmpresaRepository.findById(request.getIdGrupoEmpresa());
          PreconditionCustom.checkThrow(!grupoEmpresa.isPresent(), ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_ENCONTRADO_PRE_CONDITION);
          
          Optional<Usuario> usuario = this.usuarioRepository.findById(request.getIdUsuarioSessao());
          PreconditionCustom.checkThrow(!usuario.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO_PRE_CONDITION);
          dominioStatusCargas = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, ATIVO.name());

          Optional<PermissoesUsuariosRh> optPermissao = permissoesUsuariosRhRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(request.getIdUsuarioSessao(), request.getIdGrupoEmpresa(), statusPermissaoAtiva.getStatus());
          ForbiddenCdt.checkThrow(!optPermissao.isPresent(), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);
          
          List<Integer> idsEmpresas = permissoesUsuariosRhRepository.findEmpresasbyIdPermissao(optPermissao.get().getId());
          
          if(CollectionUtils.isNotEmpty(request.getIdEmpresasPermitidas()))
               idsEmpresas = idsEmpresas.stream().filter(ide -> request.getIdEmpresasPermitidas().stream().anyMatch(iep -> ide.equals(iep))).collect(Collectors.toList());

          ForbiddenCdt.checkThrow(CollectionUtils.isEmpty(idsEmpresas), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);          
          
          Integer contador = financeiroPedidosCustomRepository.countFinanceiroPedidosPorPermissoes(request.getIdGrupoEmpresa(), idsEmpresas,statusArquivoParaVisualizarRPS());
          NoContentCdt.checkThrow(contador == 0, ExceptionsMessagesCdtEnum.PESQUISA_NAO_ENCONTRADA);
          
          List<ArquivoCargaFinanceiroCustom> pedidosPaginado = financeiroPedidosCustomRepository.buscarPedidosPorPermissoesPaginado(request.getIdGrupoEmpresa(), idsEmpresas,statusArquivoParaVisualizarRPS(), page, size);
          NotFoundCdt.checkThrow(CollectionUtils.isEmpty(pedidosPaginado), ExceptionsMessagesCdtEnum.PESQUISA_NAO_ENCONTRADA);
          
          List<Long> idsArquivoCarga = pedidosPaginado.stream().map(ArquivoCargaFinanceiroCustom::getIdArquivoCarga).collect(Collectors.toList());
          
          List<PedidoFinanceiroCustom> pedidosFinanceiro = financeiroPedidosCustomRepository.buscarFinanceiroPorPedidosEPermissoes(request.getIdGrupoEmpresa(), idsEmpresas, idsArquivoCarga,statusArquivoParaVisualizarRPS());
          
          List<DadosPedidoFinanceiroCustom> listaDadosPedidosFinanceiro = financeiroPedidosCustomRepository.buscarDadosPedidoFinanceiro(idsArquivoCarga, statusCargaBeneficioParaNaoVisualizarRPS());

          List<ArquivoPedidoResponseCustom> listaPedidosCustom = new ArrayList<>();
          
          pedidosPaginado.stream().forEach(pedido -> {
               
               DadosPedidoFinanceiroCustom dadosPedidosFinanceiro = listaDadosPedidosFinanceiro.stream()
                         .filter(ldpf -> ldpf.getId().equals(pedido.getIdArquivoCarga()))
                         .findFirst()
                         .get();
               
               ArquivoPedidoResponseCustom arquivoPedidoResponseCustom = new ArquivoPedidoResponseCustom();
               arquivoPedidoResponseCustom.setIdArquivoCargaStd(pedido.getIdArquivoCarga());
               arquivoPedidoResponseCustom.setDataImportacao(pedido.getDataImportacao());
               arquivoPedidoResponseCustom.setQuantidadeNotas(dadosPedidosFinanceiro.getQtdNotas());
               arquivoPedidoResponseCustom.setValorTotal(dadosPedidosFinanceiro.getValorTotal().setScale(2, RoundingMode.HALF_UP));
               
               List<CargaControleFinanceiroResponse> listaCargaControleFinanceiro = new ArrayList<>();
               
               pedidosFinanceiro.stream()
                         .filter(pf -> pf.getIdArquivoCarga().equals(pedido.getIdArquivoCarga()))
                         .forEach((financeiro) -> {
                              
                              CargaControleFinanceiroResponse cargaControleFinanceiro = new CargaControleFinanceiroResponse();
                              cargaControleFinanceiro.setIdCargaControleFinanceiro(financeiro.getIdCargaControleFinanceiro());
                              cargaControleFinanceiro.setDataEmissao(financeiro.getDataEmissaoRps());
                              cargaControleFinanceiro.setValorNota(financeiro.getValor());
                              cargaControleFinanceiro.setNumeroNF(financeiro.getNumeroNf());
                              
                              UsuarioResumido usuarioResumido = new UsuarioResumido();
                              usuarioResumido.setNome(financeiro.getNomeBeneficiario());
                              
                              cargaControleFinanceiro.setUsuarioResumido(usuarioResumido);
                              listaCargaControleFinanceiro.add(cargaControleFinanceiro);
                         });
               
               arquivoPedidoResponseCustom.setCargaControleFinanceiros(listaCargaControleFinanceiro);
               listaPedidosCustom.add(arquivoPedidoResponseCustom);
               
          });

          PageResponse<ArquivoPedidoResponseCustom> pageResponse = new PageResponse<ArquivoPedidoResponseCustom>();

          pageResponse.setContent(listaPedidosCustom.stream()
                    .sorted((Comparator.comparing(ArquivoPedidoResponseCustom::getIdArquivoCargaStd)).reversed())
                    .collect(Collectors.toList()));
          
          pageResponse = PageUtils.ajustarPageResponse(pageResponse, page, size, contador);

          PageArquivoCargasResponseCustom response = new PageArquivoCargasResponseCustom(pageResponse);

          return response;

     }
     
     private List<Integer> statusArquivoParaVisualizarRPS() {
          
          List<StatusDescricao> dominioStatusArquivo = this.statusCached.busca(TipoStatus.ARQUIVO_CARGAS.getNome());
          
          return Arrays.asList(mapStatusDescricao(dominioStatusArquivo, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus()).getStatus(),mapStatusDescricao(dominioStatusArquivo, StatusArquivoEnum.CANCELADO_PARCIAL.getStatus()).getStatus());
     }

     private List<Integer> statusCargaBeneficioParaNaoVisualizarRPS() {

          List<StatusDescricao> dominioStatusCarga = this.statusCached.busca(TipoStatus.CARGAS_BENEFICIOS.getNome());
          return Arrays.asList(mapStatusDescricao(dominioStatusCarga, StatusCargaBeneficioEnum.CARGA_PROCESSANDO.getStatus()).getStatus(),mapStatusDescricao(dominioStatusCarga, StatusCargaBeneficioEnum.PEDIDO_CANCELADO.getStatus()).getStatus(), mapStatusDescricao(dominioStatusCarga, StatusCargaBeneficioEnum.EXPIRADO.getStatus()).getStatus());
     }

     public ResponseEntity<?> consultarNotaRPS(final Long codigoRPS) {

          Optional<DadoEmpresaRps> dadoEmpresaRps = dadoEmpresaRpsRepository.findById(codigoRPS);

          BadRequestCdt.checkThrow(!dadoEmpresaRps.isPresent(), ExceptionsMessagesCdtEnum.NOTA_RPS_NAO_ENCONTRADO);

          List<ProdutoRps> produtos = produtoRpsRepository.findByNumeroRps(codigoRPS);

          BadRequestCdt.checkThrow((Objects.isNull(produtos) || produtos.isEmpty()), ExceptionsMessagesCdtEnum.NOTA_RPS_NAO_ENCONTRADO);

          return demonstrativoRpsService.gerarArquivo(dadoEmpresaRps.get(), produtos);
     }
     
     public NotaFiscalResponse consultarNotaFiscal(Long codigoNotaFiscal, IdentificacaoRequest request) {
          
          Optional<Long> optIdGrupoEmpresa = grupoEmpresaService.isIdGrupoEmpresaSalvo(request.getIdGrupoEmpresa());
          PreconditionCustom.checkThrow(!optIdGrupoEmpresa.isPresent(), GRUPO_EMPRESA_NAO_ENCONTRADO_PRE_CONDITION);
          
          Optional<Long> optIdUsuario = usuarioPierService.isIdUsuarioSalvo(request.getIdUsuario());
          PreconditionCustom.checkThrow(!optIdUsuario.isPresent(), USUARIO_NAO_ENCONTRADO_PRE_CONDITION);
          
          Long idPermissao = permissoesUsuariosRhService.buscaIdPermissaoAtivaPor(request.getIdUsuario(), request.getIdGrupoEmpresa());
          
          List<Integer> idsEmpresas = permissoesUsuariosRhService.buscaIdsEmpresasPorPermissaoUsuario(idPermissao);
          
          ForbiddenCdt.checkThrow(CollectionUtils.isEmpty(idsEmpresas), ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO);
          
          List<NotaFinanceiroCustom> retorno = financeiroCargaControleFinanceiroCustomRepository.buscaDadosFinanceiroPor(codigoNotaFiscal, request.getIdGrupoEmpresa(), idsEmpresas);
          
          ForbiddenCdt.checkThrow(CollectionUtils.isEmpty(retorno), USUARIO_SEM_ACESSO_NOTA_FISCAL_DELOITTE);
          
          PreconditionCustom.checkThrow(Objeto.isBlank(retorno.stream().findFirst().get().getNumeroNf()), NOTA_FISCAL_PRE_CONDITION);
          
          return deloitteService.buscarNotaFiscal(codigoNotaFiscal);
     }

}
