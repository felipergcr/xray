
package br.com.conductor.rhblueapi.service.strategy.nivelpermissao;

import static br.com.conductor.rhblueapi.enums.StatusNivelPermissaoAcessoEnum.ATIVO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.USUARIOS_NIVEIS_PERMISSOES_RH;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacional;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.enums.StatusNivelPermissaoAcessoEnum;
import br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService;

@Service
public class EmpresaStrategy extends GerenciarNivelPermissaoStrategy {

     @Override
     public List<UsuarioPermissaoNivelAcessoRh> retornarNiveisAcessoInativado(HierarquiaOrganizacional hierarquiaOrganizacional, List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso) {

          List<StatusDescricao> dominioStatusCargas = this.statusCached.busca(USUARIOS_NIVEIS_PERMISSOES_RH.getNome());

          final StatusDescricao statusNivelAcessoInativo = mapStatusDescricao(dominioStatusCargas, StatusNivelPermissaoAcessoEnum.INATIVO.getDescricao());

          return listaNiveisAcesso.stream().filter(nivel -> (Objects.nonNull(nivel.getIdSubgrupoEmpresa()) && hierarquiaOrganizacional.getListaIdsSubgrupoEmpresa().contains(nivel.getIdSubgrupoEmpresa())) || (Objects.nonNull(nivel.getIdGrupoEmpresa()) && hierarquiaOrganizacional.getIdGrupoEmpresa().equals(nivel.getIdGrupoEmpresa()))).map(ativo -> {
               ativo.setStatus(statusNivelAcessoInativo.getStatus());
               return ativo;
          }).collect(Collectors.toList());
     }

     @Override
     public boolean isPossuiAcessoNivelSuperior(HierarquiaOrganizacional hierarquiaOrganizacional, List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso) {

          if (UsuarioNivelPermissaoRhService.isAcessoNivelGrupo(hierarquiaOrganizacional, listaNiveisAcesso)) {
               return true;
          } else {
               return UsuarioNivelPermissaoRhService.isAcessoNivelSubgrupo(hierarquiaOrganizacional, listaNiveisAcesso);
          }

     }

     @Override
     public boolean isPossuiNivelAcessoPretendido(Long idPretendido, List<UsuarioPermissaoNivelAcessoRh> listaNiveisAcesso) {

          return listaNiveisAcesso.stream().anyMatch(nivel -> Objects.nonNull(nivel.getIdEmpresa()) && nivel.getIdEmpresa().equals(idPretendido));
     }

     @Override
     public UsuarioPermissaoNivelAcessoRh criarNivelAcesso(Long idPretendido, Long idPermissao, Long idUsuarioRegistro) {

          final StatusDescricao statusNivelAcessoAtivo = mapStatusDescricao(this.statusCached.busca(USUARIOS_NIVEIS_PERMISSOES_RH.getNome()), ATIVO.getDescricao());

          return UsuarioPermissaoNivelAcessoRh.builder().idEmpresa(idPretendido).idPermissao(idPermissao).idUsuarioRegistro(idUsuarioRegistro).dataRegistro(LocalDateTime.now()).status(statusNivelAcessoAtivo.getStatus()).build();
     }

     
}
