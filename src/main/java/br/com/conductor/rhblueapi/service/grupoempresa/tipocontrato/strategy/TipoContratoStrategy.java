
package br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.strategy;

import java.time.LocalDate;

public interface TipoContratoStrategy {

     void validaDataVigenciaLimite(LocalDate data);

     void validaPrazoPagamento(Integer prazoPagamento);

     Integer aplicaRegraPrazoPagamento(Integer prazoPagamentoRequest);
     
     Integer obtemQuantidadeDiasMinimoUploadPedido();
}
