
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.limiteCredito.OrigemBuscaLimiteCreditoEnum.GERENCIADOR_CARGAS_PEDIDO;
import static br.com.conductor.rhblueapi.enums.limiteCredito.OrigemBuscaLimiteCreditoEnum.GERENCIADOR_LIMITE_DIARIO;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.LIMITE_CREDITO_VALOR_TOTAL_PEDIDOS_PAGO;
import static br.com.conductor.rhblueapi.util.AppConstantes.LIMITE_CREDITO_VALOR_TOTAL_PEDIDOS_SOLICITADOS;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.LimiteCredito.CreditoUtilizadoCustom;
import br.com.conductor.rhblueapi.domain.LimiteCredito.LimiteCredito;
import br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum;
import br.com.conductor.rhblueapi.enums.StatusPagamentoEnum;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum;
import br.com.conductor.rhblueapi.enums.limiteCredito.OrigemBuscaLimiteCreditoEnum;
import br.com.conductor.rhblueapi.repository.LimiteCreditoRepository;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.service.whitelist.ExcecoesLimiteDisponivelService;
import br.com.conductor.rhblueapi.util.AppConstantes;

@Service
public class LimiteCreditoService {

     @Autowired
     private ParametrosService parametrosService;

     @Autowired
     private ExcecoesLimiteDisponivelService excecoesLimiteDisponivelService;

     @Autowired
     private LimiteCreditoRepository limiteCreditoRepository;
     
     @Autowired
     private StatusCached statusCached;
     
     
     private List<Integer> statusPagamentoPedidoParaBuscaLimite(OrigemBuscaLimiteCreditoEnum origemBusca) {
          
          List<StatusDescricao> dominioArquivoCargasStatusPagamento = this.statusCached.busca(TipoStatus.ARQUIVO_CARGAS_STATUSPAGAMENTO.getNome());

          final StatusDescricao statusPagamentoConfirmado = mapStatusDescricao(dominioArquivoCargasStatusPagamento, StatusPagamentoEnum.PAGO.getStatus());
          final StatusDescricao statusLimiteCreditoVencido= mapStatusDescricao(dominioArquivoCargasStatusPagamento, StatusPagamentoEnum.LIMITE_CREDITO_VENCIDO.getStatus());
          final StatusDescricao statusLimiteInsulficiente = mapStatusDescricao(dominioArquivoCargasStatusPagamento, StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE.getStatus());
          
          List<Integer> status = new ArrayList<>();
          
          status.add(statusPagamentoConfirmado.getStatus());
          
          if(GERENCIADOR_LIMITE_DIARIO.equals(origemBusca)) {
               status.addAll(Arrays.asList(statusLimiteCreditoVencido.getStatus(), statusLimiteInsulficiente.getStatus()));
          } 

          return status;
     }
     
     private List<Integer> statusCargaBeneficioParaBuscaLimite() {
          
          List<StatusDescricao> dominioCargasBeneficios = this.statusCached.busca(TipoStatus.CARGAS_BENEFICIOS.getNome());
          final StatusDescricao statusCargasBeneficiosCancelado = mapStatusDescricao(dominioCargasBeneficios, StatusCargaBeneficioEnum.PEDIDO_CANCELADO.getStatus());
          final StatusDescricao statusCargasBeneficiosExpirado = mapStatusDescricao(dominioCargasBeneficios, StatusCargaBeneficioEnum.EXPIRADO.getStatus());
          
          return Arrays.asList(statusCargasBeneficiosCancelado.getStatus(), statusCargasBeneficiosExpirado.getStatus());
     }
     
     public LimiteCredito obterLimiteCredito(final long idGrupoEmpresa, OrigemBuscaLimiteCreditoEnum origemBusca) {

          if (existeValidacaoLimiteCredito(idGrupoEmpresa)) {

               ParametroGrupoEmpresa parametroLimiteCredito = parametrosService.buscaParametrosGrupoEmpresaPor(idGrupoEmpresa, AppConstantes.PARAMETRO_RH, ParametrosEnum.LIMITE_CREDITO);
               BigDecimal valorLimiteDisponivel = new BigDecimal(parametroLimiteCredito.getValor());

               List<CreditoUtilizadoCustom> listaCreditoUtilizadoCustom = limiteCreditoRepository.listarValoresCreditoUtilizado(idGrupoEmpresa, this.statusPagamentoPedidoParaBuscaLimite(origemBusca), this.statusCargaBeneficioParaBuscaLimite());
               BigDecimal valorTotalPedidosSolicitados = new BigDecimal(0), valorTotalPedidosPago = new BigDecimal(0);
               for (CreditoUtilizadoCustom creditoUtilizadoCustom : listaCreditoUtilizadoCustom) {
                    if (creditoUtilizadoCustom.getTipo().equals(LIMITE_CREDITO_VALOR_TOTAL_PEDIDOS_SOLICITADOS))
                         valorTotalPedidosSolicitados = Objects.isNull(creditoUtilizadoCustom.getValor()) ? valorTotalPedidosSolicitados : creditoUtilizadoCustom.getValor();
                    else if (creditoUtilizadoCustom.getTipo().equals(LIMITE_CREDITO_VALOR_TOTAL_PEDIDOS_PAGO))
                         valorTotalPedidosPago = Objects.isNull(creditoUtilizadoCustom.getValor()) ? valorTotalPedidosPago : creditoUtilizadoCustom.getValor();
               }
               BigDecimal valorUtilizado = valorTotalPedidosSolicitados.subtract(valorTotalPedidosPago);

               BigInteger porcentagemUtilizado;
               if(valorLimiteDisponivel.equals(BigDecimal.ZERO)) {
                    porcentagemUtilizado = BigInteger.valueOf(100L);
               } else {
                    porcentagemUtilizado = valorUtilizado.divide(valorLimiteDisponivel, 2, RoundingMode.UP).multiply(new BigDecimal(100)).toBigInteger();
               }

               BigDecimal valorDisponivel = valorLimiteDisponivel.subtract(valorUtilizado);

               return LimiteCredito.builder().validaLimite(true).valorLimiteDisponivel(valorLimiteDisponivel.setScale(2, RoundingMode.HALF_UP)).porcentagemUtilizado(porcentagemUtilizado).valorDisponivel(valorDisponivel.setScale(2, RoundingMode.HALF_UP)).valorUtilizado(valorUtilizado.setScale(2, RoundingMode.HALF_UP)).build();

          } else {
               return LimiteCredito.builder().validaLimite(false).build();
          }
     }

     private boolean existeValidacaoLimiteCredito(long idGrupoEmpresa) {

          if (parametrosService.isTipoPagamentoPrePago(idGrupoEmpresa) || excecoesLimiteDisponivelService.isListaDeExcecoes(idGrupoEmpresa))
               return false;
          else
               return true;
     }

     public boolean isLimiteCreditoInsuficiente(final long idGrupoEmpresa) {

          LimiteCredito limiteCredito = obterLimiteCredito(idGrupoEmpresa, GERENCIADOR_CARGAS_PEDIDO);

          if (Boolean.TRUE.equals(limiteCredito.isValidaLimite()) && 
                    limiteCredito.getValorDisponivel().compareTo(BigDecimal.ZERO) < 0) {
               return true;
          } else {
               return false;
          }
     }
     
     public boolean isDataVigenciaDoLimiteExpirado(final long idGrupoEmpresa) {

          LocalDate dataVigenciaLimite = parametrosService.obterDataVigenciaDoLimite(idGrupoEmpresa);
          if (existeValidacaoLimiteCredito(idGrupoEmpresa) && dataVigenciaLimite.isBefore(LocalDate.now())) {
               return true;
          } else {
               return false;
          }
     }
     
}
