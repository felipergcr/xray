
package br.com.conductor.rhblueapi.service.status.pagamento.enumeration;

public enum StatusPagamentoBaseEnum {
     
     LIMITE_CREDITO,
     PAGO,
     PAGO_PARCIALMENTE,
     AGUARDANDO_PAGAMENTO,
     CANCELADO,
     CANCELADO_PARCIAL,
     EXPIRADO;
}
