
package br.com.conductor.rhblueapi.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import br.com.conductor.rhblueapi.domain.Empresa;
import br.com.conductor.rhblueapi.domain.EmpresaSetor;
import br.com.conductor.rhblueapi.domain.SubgrupoEmpresa;
import br.com.conductor.rhblueapi.domain.empresa.EmpresaMinimo;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.ContaPersist;
import br.com.conductor.rhblueapi.domain.persist.EmpresaPersist;
import br.com.conductor.rhblueapi.domain.persist.PermissoesRhPersist;
import br.com.conductor.rhblueapi.domain.persist.EmpresaContatoPersist;
import br.com.conductor.rhblueapi.domain.pier.EnderecoResponsePier;
import br.com.conductor.rhblueapi.domain.pier.OrigensComercialResponse;
import br.com.conductor.rhblueapi.domain.pier.ProdutoResponsePier;
import br.com.conductor.rhblueapi.domain.pier.TelefoneResponse;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.request.EmpresaRequest;
import br.com.conductor.rhblueapi.domain.request.PessoaPierRest;
import br.com.conductor.rhblueapi.domain.response.EmpresaCustomResponse;
import br.com.conductor.rhblueapi.domain.response.EmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.PageEmpresaCustomResponse;
import br.com.conductor.rhblueapi.domain.response.PessoaResponse;
import br.com.conductor.rhblueapi.domain.update.EmpresaUpdate;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.enums.Plataforma;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.EmpresaRepository;
import br.com.conductor.rhblueapi.repository.EmpresaSetorRepository;
import br.com.conductor.rhblueapi.repository.SubgrupoRepository;
import br.com.conductor.rhblueapi.rest.EnderecoPierRest;
import br.com.conductor.rhblueapi.rest.OrigensComercialRest;
import br.com.conductor.rhblueapi.rest.ProdutosRest;
import br.com.conductor.rhblueapi.rest.TelefonePierRest;
import br.com.conductor.rhblueapi.service.utils.EmpresaServiceUtils;
import br.com.conductor.rhblueapi.util.GenericConvert;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class EmpresaService {

     @Autowired
     private OrigensComercialRest origensRest;

     @Autowired
     private ProdutosRest produtosRest;

     @Autowired
     private SubgrupoRepository subGrupoRepository;

     @Autowired
     private EmpresaRepository empresaRepository;

     @Autowired
     private PessoaPierRest pessoaPierRest;

     @Autowired
     private TelefonePierRest telefonePierRest;
     
     @Autowired
     private EmpresaServiceUtils empresaServiceUtils;

     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;

     @Autowired
     private EnderecoPierRest enderecoPierRest;

     @Autowired
     private ContaService contaService;

     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;

     @Autowired
     private CadastroEmpresaService cadastroEmpresaService;

     @Autowired
     private PessoaService pessoaService;
     
     @Autowired
     private EmpresaSetorRepository empresaSetorRepository;

     @Value("${app.plataforma.rh.cod}")
     private Integer rh;

     private static final String produtoGerencial = "Produto RH - Gerencial";

     private Long idProduto;

     private Long idOrigemComercial;

     public EmpresaResponse cadastraEmpresa(Long idSubGrupo, EmpresaPersist empresaPersist) throws IOException {

          verificaEmpresaCadastrada(empresaPersist);

          final Long idGrupoEmpresaPai = verificaSubGrupoCadastrado(idSubGrupo);

          List<OrigensComercialResponse> origensResponse = origensRest.listaOrigensComerciais();

          List<ProdutoResponsePier> produtosResponse = produtosRest.consultaProduto();

          identificaOrigemComercial(origensResponse, produtosResponse);

          EmpresaResponse empresaResponse = cadastroEmpresaService.salvar(idSubGrupo, empresaPersist);

          try {
               cadastrarUsuario(idSubGrupo, idGrupoEmpresaPai, empresaPersist, empresaResponse);
               contaService.criarContasEmpresa(getContaPersist(idGrupoEmpresaPai, produtosResponse, empresaResponse));
          } catch (Exception e) {
               deleteEmpresa(empresaResponse.getIdEmpresa());
               throw e;
          }

          return empresaResponse;
     }

     private void cadastrarUsuario(Long idSubGrupo, Long idGrupoEmpresaPai, EmpresaPersist empresaPersist, EmpresaResponse empresaResponse) throws IOException {

          List<UsuarioResponse> usuarios = new ArrayList<>();

          if (Objects.isNull(empresaPersist.getNivelPermissaoContato())) {

               for (EmpresaContatoPersist contato : empresaPersist.getContatos()) {
                    usuarios.add(cadastraUsuario(contato, empresaPersist.getCnpj(), idGrupoEmpresaPai, empresaResponse.getIdEmpresa()));
               }

          } else if (StringUtils.equals(empresaPersist.getNivelPermissaoContato().name(), NivelPermissaoEnum.SUBGRUPO.name())) {

               for (EmpresaContatoPersist contato : empresaPersist.getContatos()) {
                    usuarios.add(cadastraUsuarioSubgrupo(contato, empresaPersist.getCnpj(), idGrupoEmpresaPai, idSubGrupo));

               }
          }

          empresaResponse.setUsuarios(usuarios);
     }

     private UsuarioResponse cadastraUsuario(EmpresaContatoPersist contato, String cnpj, Long idGrupoEmpresaPai, Long idNivel) throws IOException {

          List<UsuarioResponse> usuarioList = empresaServiceUtils.verificaCadastroUsuario(contato.getCpf(), rh);

          UsuarioResponse usuarioResponse;

          if (CollectionUtils.isEmpty(usuarioList)) {

               usuarioResponse = empresaServiceUtils.criaUsuario(contato.getNome(), contato.getCpf(), cnpj, contato.getEmail(), Plataforma.RH);

               PermissoesRhPersist permissaoPersist = PermissoesRhPersist.builder()
                         .idGrupoEmpresa(idGrupoEmpresaPai)
                         .idUsuario(usuarioResponse.getId())
                         .idUsuarioRegistro(0L)
                         .build();

               Long idPermissao = permissoesUsuariosRhService.salvarPermissaoViaSalesforce(permissaoPersist);

               usuarioNivelPermissaoRhService.salvarNivelAcessoViaSalesforce(null, null, idNivel, idPermissao, 0L, NivelPermissaoEnum.EMPRESA);

               empresaServiceUtils.criaPermissoesAcessoUsuarioPrincipal(empresaServiceUtils.listaOperacoes(), idPermissao);

               return usuarioResponse;

          } else {

               PermissoesRhPersist permissaoPersist = PermissoesRhPersist.builder()
                         .idGrupoEmpresa(idGrupoEmpresaPai)
                         .idUsuario(usuarioList.get(0).getId())
                         .idUsuarioRegistro(0L)
                         .build();

               Long idPermissao = permissoesUsuariosRhService.salvarPermissaoViaSalesforce(permissaoPersist);

               usuarioNivelPermissaoRhService.salvarNivelAcessoViaSalesforce(null, null, idNivel, idPermissao, 0L, NivelPermissaoEnum.EMPRESA);

               empresaServiceUtils.criaPermissoesAcessoUsuarioPrincipal(empresaServiceUtils.listaOperacoes(), idPermissao);

               return usuarioList.get(0);
          }
     }

     private UsuarioResponse cadastraUsuarioSubgrupo(EmpresaContatoPersist contato, String cnpj, Long idGrupoEmpresaPai, Long idNivel) throws IOException {

          List<UsuarioResponse> usuarioList = empresaServiceUtils.verificaCadastroUsuario(contato.getCpf(), rh);

          if (CollectionUtils.isEmpty(usuarioList)) {

               UsuarioResponse usuarioResponse = empresaServiceUtils.criaUsuario(contato.getNome(), contato.getCpf(), cnpj, contato.getEmail(), Plataforma.RH);

               PermissoesRhPersist permissaoPersist = PermissoesRhPersist.builder()
                         .idGrupoEmpresa(idGrupoEmpresaPai)
                         .idUsuario(usuarioResponse.getId())
                         .idUsuarioRegistro(0L)
                         .build();

               Long idPermissao = permissoesUsuariosRhService.salvarPermissaoViaSalesforce(permissaoPersist);

               usuarioNivelPermissaoRhService.salvarNivelAcessoViaSalesforce(null, idNivel, null, idPermissao, 0L, NivelPermissaoEnum.SUBGRUPO);

               empresaServiceUtils.criaPermissoesAcessoUsuarioPrincipal(empresaServiceUtils.listaOperacoes(), idPermissao);

               usuarioResponse.setIdPermissao(idPermissao);

               return usuarioResponse;

          } else {

               PermissoesRhPersist permissaoPersist = PermissoesRhPersist.builder()
                         .idGrupoEmpresa(idGrupoEmpresaPai)
                         .idUsuario(usuarioList.get(0).getId())
                         .idUsuarioRegistro(0L)
                         .build();

               Long idPermissao = permissoesUsuariosRhService.salvarPermissaoViaSalesforce(permissaoPersist);

               usuarioNivelPermissaoRhService.salvarNivelAcessoViaSalesforce(null, idNivel, null, idPermissao, 0L, NivelPermissaoEnum.SUBGRUPO);

               empresaServiceUtils.criaPermissoesAcessoUsuarioPrincipal(empresaServiceUtils.listaOperacoes(), idPermissao);

               usuarioList.get(0).setIdPermissao(idPermissao);

               return usuarioList.get(0);
          }
     }

     private Long verificaSubGrupoCadastrado(Long idSubGrupo) {

          Optional<SubgrupoEmpresa> consultaSubGrupo = subGrupoRepository.findById(idSubGrupo);

          BadRequestCdt.checkThrow(!consultaSubGrupo.isPresent(), ExceptionsMessagesCdtEnum.SUBGRUPO_EMPRESA_NAO_CADASTRADO);

          return consultaSubGrupo.get().getIdGrupoEmpresaPai();
     }

     private void verificaEmpresaCadastrada(EmpresaPersist empresaPersist) {

          List<PessoaResponse> pessoa = this.pessoaPierRest.consultaPessoaFisica(empresaPersist.getCnpj());
          BadRequestCdt.checkThrow(!pessoa.isEmpty(), ExceptionsMessagesCdtEnum.EMPRESA_JA_CADASTRADA);
     }

     private void identificaOrigemComercial(List<OrigensComercialResponse> origensResponse, List<ProdutoResponsePier> produtosResponse) {

          for (ProdutoResponsePier produtoResponsePier : produtosResponse) {
               if (StringUtils.equals(produtoResponsePier.getNome(), produtoGerencial)) {
                    idProduto = produtoResponsePier.getId();
               }
          }

          for (OrigensComercialResponse origensComercialResponse : origensResponse) {
               if (Objects.equals(origensComercialResponse.getProdutosOrigem().iterator().next().getIdProduto(), idProduto)) {
                    idOrigemComercial = origensComercialResponse.getId();
               }
          }
     }

     private ContaPersist getContaPersist(Long idGrupoEmpresaPai, List<ProdutoResponsePier> produtosResponse, EmpresaResponse empresaResponse) {

          return ContaPersist.builder()
                    .idEmpresa(empresaResponse.getIdEmpresa())
                    .idGrupoEmpresaPai(idGrupoEmpresaPai)
                    .produtosResponse(produtosResponse)
                    .idEnderecoCorrespondencia(empresaResponse.getIdEnderecoCorrespondencia())
                    .idOrigemComercial(idOrigemComercial)
                    .idProduto(idProduto)
                    .idPessoa(empresaResponse.getIdPessoa())
                    .build();
     }

     public List<EmpresaResponse> listar(EmpresaRequest empresaRequest) {

          List<Empresa> empresas = this.empresaRepository.findByIdIn(empresaRequest.getIdsEmpresas());
          NotFoundCdt.checkThrow(empresas.isEmpty(), ExceptionsMessagesCdtEnum.EMPRESA_NAO_ENCONTRADA);

          return empresas.stream().map(emp -> EmpresaResponse.builder()
                    .idEmpresa(emp.getId())
                    .idPessoa(emp.getPessoa().getIdPessoa())
                    .nomeEmpresa(emp.getNomeFantasia())
                    .cnpj(emp.getPessoa().getDocumento())
                    .idGrupoEmpresa(emp.getIdGrupoEmpresa()).build()).collect(Collectors.toList());
     }


     public PageEmpresaCustomResponse listarEmpresasdoSubGrupoEmpresa(Long idSubGrupoEmpresa, Integer page, Integer size ){

          Pageable pageable = PageRequest.of(page, size);

          Optional<SubgrupoEmpresa> subgrupoEmpresa = subGrupoRepository.findById(idSubGrupoEmpresa);

          NotFoundCdt.checkThrow(!subgrupoEmpresa.isPresent(), ExceptionsMessagesCdtEnum.SUBRGRUPO_NAO_EXISTE);

          BadRequestCdt.checkThrow(subgrupoEmpresa.isPresent() && Objects.isNull(subgrupoEmpresa.get().getIdGrupoEmpresaPai()), ExceptionsMessagesCdtEnum.ID_INFORMADO_NAO_E_DE_SUBGRUPO);

          Page<Empresa> listaEmpresa = empresaRepository.findByIdGrupoEmpresa(idSubGrupoEmpresa, pageable);
          NoContentCdt.checkThrow(Objects.isNull(listaEmpresa) || listaEmpresa.getContent().isEmpty(), ExceptionsMessagesCdtEnum.PESQUISA_NAO_ENCONTRADA);

          PageEmpresaCustomResponse pageFuncionarioResponse = new PageEmpresaCustomResponse(GenericConvert.convertModelMapperToPageResponse(listaEmpresa, new TypeToken<List<EmpresaCustomResponse>>() {
          }.getType()));

          return pageFuncionarioResponse;
     }

     /**
      * atualizar
      * @param empresaUpdate
      * @param idSubGrupo
      * @param idEmpresa
      * @return
      */
     @Transactional
     public EmpresaResponse atualizar(EmpresaUpdate empresaUpdate, Long idSubGrupo, Long idEmpresa) {

          //TODO validar campos de contato para atualização -- campos de contato devem ser obrigaório se existir atualização

          //Verifica se existe empresa cadastrada
          Optional<Empresa> empresaCadastrada = empresaRepository.findById(idEmpresa);
          NotFoundCdt.checkThrow(!empresaCadastrada.isPresent(), ExceptionsMessagesCdtEnum.EMPRESA_NAO_ENCONTRADA);

          //Verifica se subgrupo esta cadastrado
          verificaSubGrupoCadastrado(idSubGrupo);

          //Verifica se tem alteracao de endereco para ser atualizado
          if (Objects.nonNull(empresaUpdate.getEndereco())) {

               //Busca endereço cadastrado no pier
               List<EnderecoResponsePier> enderecoPessoaFisica = enderecoPierRest.consultaEndereco(empresaCadastrada.get().getIdPessoa());

               //Monta objeto de parametros para serem atualizados
               MultiValueMap<String, String> parametros = new LinkedMultiValueMap<>();
               if (!enderecoPessoaFisica.isEmpty()) parametros.add("id", enderecoPessoaFisica.get(0).getId().toString());
               parametros.add("idPessoa", empresaCadastrada.get().getIdPessoa().toString());
               parametros.add("idTipoEndereco", !enderecoPessoaFisica.isEmpty() ? enderecoPessoaFisica.get(0).getIdTipoEndereco().toString() : "1");
               parametros.add("cep", empresaUpdate.getEndereco().getCep());
               parametros.add("logradouro", empresaUpdate.getEndereco().getLogradouro());
               parametros.add("numero", empresaUpdate.getEndereco().getNumero().toString());
               if (Objects.nonNull(empresaUpdate.getEndereco().getComplemento())) parametros.add("complemento", empresaUpdate.getEndereco().getComplemento());
               else if (!enderecoPessoaFisica.isEmpty() && Objects.nonNull(enderecoPessoaFisica.get(0).getComplemento())) parametros.add("complemento", enderecoPessoaFisica.get(0).getComplemento());
               if (!enderecoPessoaFisica.isEmpty() && Objects.nonNull(enderecoPessoaFisica.get(0).getPontoReferencia())) parametros.add("pontoReferencia", enderecoPessoaFisica.get(0).getPontoReferencia());
               parametros.add("bairro", empresaUpdate.getEndereco().getBairro());
               parametros.add("cidade", empresaUpdate.getEndereco().getCidade());
               parametros.add("uf", empresaUpdate.getEndereco().getUf());
               if (!enderecoPessoaFisica.isEmpty() && Objects.nonNull(enderecoPessoaFisica.get(0).getPais())) parametros.add("pais", enderecoPessoaFisica.get(0).getPais());

               //Atualiza no BD através do pier
               enderecoPierRest.atualizaOuCriaEndereco(parametros, !enderecoPessoaFisica.isEmpty() ? HttpMethod.PUT : HttpMethod.POST);

          }

          //Verifica se tem alteracao de telefone para ser atualizado
          if (Objects.nonNull(empresaUpdate.getTelefone())) {

               //Busca telefone cadastrado no pier
               List<TelefoneResponse> telefonePessoaFisica = telefonePierRest.consultaTelefone(empresaCadastrada.get().getIdPessoa());

               MultiValueMap<String, String> parametrosTelefone = new LinkedMultiValueMap<>();
               if (!telefonePessoaFisica.isEmpty()) parametrosTelefone.add("id", telefonePessoaFisica.get(0).getId().toString());
               parametrosTelefone.add("idPessoa", empresaCadastrada.get().getIdPessoa().toString());
               parametrosTelefone.add("idTipoTelefone", telefonePessoaFisica.get(0).getIdTipoTelefone().toString());
               parametrosTelefone.add("ddd", "0"+empresaUpdate.getTelefone().getDdd());
               parametrosTelefone.add("telefone", empresaUpdate.getTelefone().getTelefone());

               if (Objeto.notBlank(empresaUpdate.getTelefone().getRamal())) {
                    parametrosTelefone.add("ramal", empresaUpdate.getTelefone().getRamal());
               }

               //Atualiza no BD através do pier
               telefonePierRest.atualizaOuCriaTelefone(parametrosTelefone, !telefonePessoaFisica.isEmpty() ? HttpMethod.PUT : HttpMethod.POST);

          }

          //TODO criar/atualizar usuário e permisões aqui -- regra ainda está sendo definida

          //Atualiza dados de empresa -- verificando quais campos devem ser atualizados
          Empresa empresa = EmpresaServiceUtils.validarUpdate(empresaCadastrada.get(), empresaUpdate);
          empresa.setIdGrupoEmpresa(idSubGrupo);

          //Atualiza no BD
          empresaRepository.save(empresa);

          return EmpresaResponse
                    .builder()
                    .idEmpresa(idEmpresa)
                    .idPessoa(empresaCadastrada.get().getIdPessoa())
                    .nomeEmpresa(empresaCadastrada.get().getNomeExibicao())
                    .idGrupoEmpresa(idSubGrupo)
                    .build();

     }

     @Transactional
     public void deleteEmpresa(Long idEmpresa) {
          Optional<Empresa> empresa = empresaRepository.findById(idEmpresa);

          empresa.ifPresent((e) -> {
               usuarioNivelPermissaoRhService.deletePermissaoNivelAcessoByIdEmpresa(e.getId());
               contaService.deleteContaEmpresa(e.getId());
               empresaRepository.delete(e);
               pessoaService.deletePessoa(e.getPessoa().getIdPessoa());
          });
     }

     public Optional<Long> buscaIdEmpresaPorCnpj(String cnpj) {

          Optional<Empresa> optEmpresa = Optional.ofNullable(empresaRepository.findByPessoaDocumento(cnpj));

          return Optional.ofNullable(optEmpresa.isPresent() ? optEmpresa.get().getId() : null);
     }
     
     private Long geraSetor(Long idEmpresa, String descricao) {
          
          EmpresaSetor empresaSetor = empresaSetorRepository.save(EmpresaSetor.builder()
                    .idEmpresa(idEmpresa)
                    .descricao(descricao).build());
          
          return empresaSetor.getId();
     }
     
     public EmpresaMinimo obtemEmpresaMinimo(String cnpj) {
          
          Optional<Long> optIdPessoaJuridica = pessoaService.buscaPessoaJuridica(cnpj);
          Optional<Empresa> optEmpresa = empresaRepository.findByIdPessoa(optIdPessoaJuridica.get());
          
          Long idEmpresa = optEmpresa.get().getId();
          
          Optional<EmpresaSetor> optEmpresaSetor = empresaSetorRepository.findByIdEmpresaAndDescricao(idEmpresa, StringUtils.EMPTY);
          
          Long idEmpresaSetor = optEmpresaSetor.isPresent() ? optEmpresaSetor.get().getId() : this.geraSetor(idEmpresa, StringUtils.EMPTY); 
          
          return EmpresaMinimo.builder()
                    .idEmpresa(idEmpresa)
                    .idEmpresaSetor(idEmpresaSetor)
                    .build();
     }

}
