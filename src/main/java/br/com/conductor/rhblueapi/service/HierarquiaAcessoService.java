
package br.com.conductor.rhblueapi.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.persist.HierarquiaAcessoUpdatePersist;
import br.com.conductor.rhblueapi.domain.persist.UsuarioNivelPermissaoRhPersist;
import br.com.conductor.rhblueapi.domain.persist.UsuarioPersist;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.request.UsuarioHierarquiaAcessoRequest;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoGrupoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoSubGrupoResponse;
import br.com.conductor.rhblueapi.domain.response.UsuarioHierarquiaAcessoResponse;
import br.com.conductor.rhblueapi.enums.HierarquiaAcessoEnum;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.exception.ExceptionCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso.HierarquiaAcessoEmpresStrategy;
import br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso.HierarquiaAcessoGrupoStrategy;
import br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso.HierarquiaAcessoHibridoStrategy;
import br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso.HierarquiaAcessoSubgrupoStrategy;
import br.com.conductor.rhblueapi.service.template.method.hierarquia.acesso.HierarquiaAcessoIncompleta;
import br.com.conductor.rhblueapi.service.template.method.hierarquia.acesso.HierarquiaAcessoNova;
import br.com.conductor.rhblueapi.util.GenericConvert;

@Service
public class HierarquiaAcessoService {

     @Value("${app.plataforma.rh.cod}")
     private Integer rh;

     @Autowired
     private UsuarioPierService usuarioPierService;

     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;

     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;

     @Autowired
     private HierarquiaAcessoEmpresStrategy hierarquiaAcessoEmpresStrategy;

     @Autowired
     private HierarquiaAcessoGrupoStrategy hierarquiaAcessoGrupoStrategy;

     @Autowired
     private HierarquiaAcessoSubgrupoStrategy hierarquiaAcessoSubgrupoStrategy;

     @Autowired
     private HierarquiaAcessoHibridoStrategy hierarquiaAcessoHibridoStrategy;
     
     @Autowired
     private HierarquiaAcessoNova hierarquiaAcessoNovoUsuario;

     @Autowired
     private HierarquiaAcessoIncompleta hierarquiaAcessoUsuarioIntermediario;

     private static final String TIPONIVEL = "nivelPermissao";

     private static final String ACESSOS = "acessos";
     
     
     public void criaHierarquiaAcesso(UsuarioPersist usuarioPersist) {
          
          usuarioPersist.setCpf(formatCpf(usuarioPersist.getCpf()));
               
          HierarquiaAcessoEnum tipoHierarquia = getCondicaoHierarquia(usuarioPersist.getCpf(), usuarioPersist.getIdGrupoEmpresa());
          
          if (Objects.equals(tipoHierarquia, HierarquiaAcessoEnum.HIERARQUIA_NOVA)) {
               
               hierarquiaAcessoNovoUsuario.criarHierarquiaMethod(usuarioPersist);
          }else {
               
               hierarquiaAcessoUsuarioIntermediario.criarHierarquiaMethod(usuarioPersist);
          }
     }

     private String formatCpf(String cpf) {

          return cpf.replace(".", "").replace("-", "");
     }     

     public void atualizarHierarquiaAcesso(HierarquiaAcessoUpdatePersist update, String cpf) {
          
          UsuarioResponse usuarioPretendido = usuarioPierService.recuperarUsuario(formatCpf(cpf), rh);

          ExceptionCdt.checkThrow(Objects.isNull(usuarioPretendido) || Objects.isNull(usuarioPretendido.getId()), ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO_PRE_CONDITION);
          
          Long idPermissaoUsuarioPretendido = buscaPermissaoUserPretendido(usuarioPretendido.getId(), update.getIdGrupoEmpresa());

          UsuarioNivelPermissaoRhPersist persistNivel = UsuarioNivelPermissaoRhPersist.builder()
                    .idNivelAcesso(update.getIdsNivelAcesso())
                    .idUsuarioRegistro(update.getIdUsuarioLogado())
                    .nivelAcesso(update.getNivelAcesso())
                    .build();
          
          usuarioNivelPermissaoRhService.salvarNivelPermissaoUsuario(persistNivel, idPermissaoUsuarioPretendido);

     }

     public UsuarioHierarquiaAcessoResponse retornaHierarquiaAcesso(UsuarioHierarquiaAcessoRequest request) {

          validaUsuarioLogado(request.getIdUsuarioLogado());

          Long idPermissaoUserLogado = buscaPermissaoUsuarioLogado(request.getIdUsuarioLogado(), request.getIdGrupoEmpresa());

          Map<String, Object> acessoUserLogado = obterNivelAcesso(idPermissaoUserLogado);

          PreconditionCustom.checkThrow(acessoUserLogado.isEmpty(), ExceptionsMessagesCdtEnum.USUARIO_LOGADO_SEM_NIVEL_ACESSO);
          
          UsuarioResponse userPretendido = buscaUsuarioPor(request.getCpf());

          UsuarioHierarquiaAcessoResponse responseUsuarioHierarquia = buildHierarquiaUsuarioResponse(userPretendido);

          Long permissaoUserPretendido = null;

          try {
               permissaoUserPretendido = permissoesUsuariosRhService.buscaIdPermissaoAtivaPor(responseUsuarioHierarquia.getIdUsuario(), request.getIdGrupoEmpresa());

          } catch (Exception e) {
               return responseUsuarioHierarquia;
          }

          Map<String, Object> acessoUserPretendido = obterNivelAcesso(permissaoUserPretendido);

          if (Objects.isNull(acessoUserPretendido.get(ACESSOS))) {
               responseUsuarioHierarquia.setPermissao(HierarquiaAcessoResponse.builder().build());
               return responseUsuarioHierarquia;
          }

          NivelPermissaoEnum nivelPermissaoUserPretendido = (NivelPermissaoEnum) acessoUserPretendido.get(TIPONIVEL);

          switch (nivelPermissaoUserPretendido) {
               case GRUPO:
                    List<UsuarioPermissaoNivelAcessoRh> acessoGrupo = hierarquiaAcessoGrupoStrategy.retornaAcessoPermitido(acessoUserLogado, acessoUserPretendido);
                    HierarquiaAcessoGrupoEmpresaResponse hirarquiaAcessoGrupoPorGrupo = hierarquiaAcessoGrupoStrategy.mapHierarquiaGrupo(acessoGrupo.isEmpty() ? null : acessoGrupo.get(0).getIdGrupoEmpresa());
                    responseUsuarioHierarquia = mapHierarquiaAcessoResponse(hirarquiaAcessoGrupoPorGrupo, responseUsuarioHierarquia, null);
                    break;

               case SUBGRUPO:
                    List<UsuarioPermissaoNivelAcessoRh> acessoSubgrupo = hierarquiaAcessoSubgrupoStrategy.retornaAcessoPermitido(acessoUserLogado, acessoUserPretendido);
                    HierarquiaAcessoGrupoEmpresaResponse hirarquiaAcessoGrupoPorSubgrupoGrupo = hierarquiaAcessoGrupoStrategy.mapHierarquiaGrupo(request.getIdGrupoEmpresa());
                    List<HierarquiaAcessoSubGrupoResponse> hirarquiaAcessoSubgrupoPorSubgrupoGrupo = hierarquiaAcessoSubgrupoStrategy.mapHierarquiaSubGrupo(acessoSubgrupo);
                    responseUsuarioHierarquia = mapHierarquiaAcessoResponse(hirarquiaAcessoGrupoPorSubgrupoGrupo, responseUsuarioHierarquia, hirarquiaAcessoSubgrupoPorSubgrupoGrupo);
                    break;

               case EMPRESA:
                    List<UsuarioPermissaoNivelAcessoRh> empresaAcesso = hierarquiaAcessoEmpresStrategy.retornaAcessoPermitido(acessoUserLogado, acessoUserPretendido);
                    HierarquiaAcessoGrupoEmpresaResponse hirarquiaAcesspGrupoPorEmpresa = hierarquiaAcessoGrupoStrategy.mapHierarquiaGrupo(request.getIdGrupoEmpresa());
                    List<HierarquiaAcessoSubGrupoResponse> hirarquiaAcessoSubgrupoPorEmpresa = hierarquiaAcessoEmpresStrategy.mapHierarquiaSubGrupoEmpresa(empresaAcesso);
                    responseUsuarioHierarquia = mapHierarquiaAcessoResponse(hirarquiaAcesspGrupoPorEmpresa, responseUsuarioHierarquia, hirarquiaAcessoSubgrupoPorEmpresa);
                    break;

               case HIBRIDO:
                    List<UsuarioPermissaoNivelAcessoRh> acessoHibrido = hierarquiaAcessoHibridoStrategy.retornaAcessoPermitido(acessoUserLogado, acessoUserPretendido);
                    HierarquiaAcessoGrupoEmpresaResponse hirarquiaAcessoGrupoPorHibrido = hierarquiaAcessoGrupoStrategy.mapHierarquiaGrupo(request.getIdGrupoEmpresa());
                    List<HierarquiaAcessoSubGrupoResponse> hirarquiaAcessoSubgrupoPorHibrido = hierarquiaAcessoHibridoStrategy.mapHierarquiaSugropoHibrido(acessoHibrido);
                    responseUsuarioHierarquia = mapHierarquiaAcessoResponse(hirarquiaAcessoGrupoPorHibrido, responseUsuarioHierarquia, hirarquiaAcessoSubgrupoPorHibrido);
                    break;

               default:
                    break;
          }

          return responseUsuarioHierarquia;

     }
     
     private HierarquiaAcessoEnum getCondicaoHierarquia(String cpfUsuario, Long idGrupoEmpresa) {

          UsuarioResponse usuario = usuarioPierService.recuperarUsuario(cpfUsuario, rh);

          if (Objects.nonNull(usuario)) {

               PreconditionCustom.checkThrow(permissoesUsuariosRhService.obterPermissaoAtivaPor(usuario.getId(), idGrupoEmpresa).isPresent(), ExceptionsMessagesCdtEnum.HIERARQUIA_ACESSO_NAO_PODE_SER_CRIADA);
               
               return HierarquiaAcessoEnum.HIERARQUIA_INCOMPLETA;
          } 
          
          return HierarquiaAcessoEnum.HIERARQUIA_NOVA;
     }

     private UsuarioHierarquiaAcessoResponse buildHierarquiaUsuarioResponse(UsuarioResponse userPretendido) {

          UsuarioHierarquiaAcessoResponse response = GenericConvert.convertModelMapper(userPretendido, UsuarioHierarquiaAcessoResponse.class);
          response.setCpf(userPretendido.getCpf().trim());
          response.setIdUsuario(userPretendido.getId());
          return response;
     }

     private UsuarioResponse buscaUsuarioPor(String cpf) {

          UsuarioResponse user = usuarioPierService.recuperarUsuario(cpf, rh);

          NotFoundCdt.checkThrow(Objects.isNull(user) || Objects.isNull(user.getCpf()) || Objects.isNull(user.getId()), ExceptionsMessagesCdtEnum.USUARIO_NAO_LOCALIZADO);
          return user;
     }

     private UsuarioHierarquiaAcessoResponse mapHierarquiaAcessoResponse(HierarquiaAcessoGrupoEmpresaResponse grupo, UsuarioHierarquiaAcessoResponse response, List<HierarquiaAcessoSubGrupoResponse> subgrupos) {

          if (!CollectionUtils.isEmpty(subgrupos)) {
               grupo.setSubgrupos(subgrupos);
          }

          response.setPermissao(HierarquiaAcessoResponse.builder().grupo(grupo).build());

          return response;
     }

     private Map<String, Object> obterNivelAcesso(Long idPermissao) {

          Map<String, Object> mapAcessos = new HashMap<>();

          List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUser = usuarioNivelPermissaoRhService.listarNiveisAcessoAtivosPor(idPermissao);

          if (nivelAcessoUser.isEmpty()) {
               return mapAcessos;
          }

          List<Long> nivelGrupo = nivelAcessoUser.stream().filter(nivel -> Objects.nonNull(nivel.getIdGrupoEmpresa())).map(UsuarioPermissaoNivelAcessoRh::getIdGrupoEmpresa).collect(Collectors.toList());

          List<Long> nivelSubgrupo = nivelAcessoUser.stream().filter(nivel -> Objects.nonNull(nivel.getIdSubgrupoEmpresa())).map(UsuarioPermissaoNivelAcessoRh::getIdSubgrupoEmpresa).collect(Collectors.toList());

          List<Long> nivelEmpresa = nivelAcessoUser.stream().filter(nivel -> Objects.nonNull(nivel.getIdEmpresa())).map(UsuarioPermissaoNivelAcessoRh::getIdEmpresa).collect(Collectors.toList());

          if (!nivelGrupo.isEmpty()) {

               mapAcessos.put(TIPONIVEL, NivelPermissaoEnum.GRUPO);
               mapAcessos.put(ACESSOS, nivelAcessoUser);

               return mapAcessos;

          } else if ( !nivelSubgrupo.isEmpty() && nivelEmpresa.isEmpty()) {

               mapAcessos.put(TIPONIVEL, NivelPermissaoEnum.SUBGRUPO);
               mapAcessos.put(ACESSOS, nivelAcessoUser);

               return mapAcessos;

          } else if (!nivelSubgrupo.isEmpty() && !nivelEmpresa.isEmpty()) {
               mapAcessos.put(TIPONIVEL, NivelPermissaoEnum.HIBRIDO);
               mapAcessos.put(ACESSOS, nivelAcessoUser);

               return mapAcessos;
          }

          mapAcessos.put(TIPONIVEL, NivelPermissaoEnum.EMPRESA);
          mapAcessos.put(ACESSOS, nivelAcessoUser);

          return mapAcessos;

     }

     private void validaUsuarioLogado(Long idUsuairoLogado) {

          try {

               usuarioPierService.autenticarUsuario(idUsuairoLogado);

          } catch (Exception e) {
               ExceptionsMessagesCdtEnum.USUARIO_LOGADO_NAO_ENCONTRADO.raise();
          }
     }

     private Long buscaPermissaoUsuarioLogado(Long idUsuarioLogado, Long idGrupoEmpresa) {

          Long idPermissaoUsarioLogado = null;

          try {

               idPermissaoUsarioLogado = permissoesUsuariosRhService.buscaIdPermissaoAtivaPor(idUsuarioLogado, idGrupoEmpresa);

          } catch (Exception e) {
               ExceptionsMessagesCdtEnum.PERMISSAO_NAO_ENCONTRADA.raise();
          }

          return idPermissaoUsarioLogado;
     }

     private Long buscaPermissaoUserPretendido(Long idUserLogado, Long  idGrupoEmpresa) {
          
          Long idPermissaoUsuarioPretendido = null;

          try {

               idPermissaoUsuarioPretendido = permissoesUsuariosRhService.buscaIdPermissaoAtivaPor(idUserLogado, idGrupoEmpresa);

          } catch (Exception e) {
               
              ExceptionsMessagesCdtEnum.PERMISSAO_HIERARQUIA_PRE_CONDICAO.raise();
          }
          return idPermissaoUsuarioPretendido;
     }
}
