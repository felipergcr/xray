
package br.com.conductor.rhblueapi.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.EmpresasCargas;
import br.com.conductor.rhblueapi.repository.EmpresasCargasCustomRepository;
import br.com.conductor.rhblueapi.repository.EmpresasCargasRepository;

@Service
public class EmpresasCargasService {

     @Autowired
     private EmpresasCargasRepository empresasCargasRepository;
     
     @Autowired
     private EmpresasCargasCustomRepository empresasCargasCustomRepository;
     
     @Value("${app.cargas.paginacao.tamanho}")
     private Integer quantidadeItensPaginacao;
     
     public List<EmpresasCargas> buscaEmpresasCargaPorIdsCargaBeneficio(List<Long> idsCargaBeneficio) {
          
          List<EmpresasCargas> empresasCargas = new ArrayList<EmpresasCargas>();
          
          IntStream.range(0, (idsCargaBeneficio.size() + quantidadeItensPaginacao - 1) / quantidadeItensPaginacao)
          .mapToObj(i -> idsCargaBeneficio.subList(i * quantidadeItensPaginacao, Math.min(quantidadeItensPaginacao * (i + 1), idsCargaBeneficio.size())))
          .forEach(lista -> {
               empresasCargas.addAll(empresasCargasRepository.findByIdCargaBeneficioIn(lista));
          });
          
          return empresasCargas;
     }
     
     public void salvarEmLote(final List<Long> idsEmpresasCargas) {
          
          idsEmpresasCargas.stream().forEach(idEmpresaCarga -> {
               
               empresasCargasCustomRepository.updateTotalizaValorByIdEmpresaCarga(idEmpresaCarga);
               
               empresasCargasCustomRepository.updateTotalizacaoQtdNovosCartoesByIdEmpresaCarga(idEmpresaCarga);
          });
     }

     public void geraEmpresasCargas(Long idCargaBeneficio, Long idEmpresa) {
          
          this.empresasCargasRepository.save(EmpresasCargas.builder()
                    .idCargaBeneficio(idCargaBeneficio)
                    .idEmpresa(idEmpresa)
                    .qtdeNovosCartoes(0)
                    .valor(new BigDecimal(0.0))
                    .build());
     }
     
     public Optional<Long> buscaIdEmpresaCargaPor(Long idCargaBeneficio, Long idEmpresa) {
          
          Optional<EmpresasCargas> optEmpresasCargas = empresasCargasRepository.findByIdCargaBeneficioAndIdEmpresa(idCargaBeneficio, idEmpresa);
          
          return Optional.ofNullable(optEmpresasCargas.isPresent() ? optEmpresasCargas.get().getId() : null);
     }
     
     public void persisteEObtemIdEmpresaCarga(Long idCargaBeneficio, Long idEmpresa) {

          Optional<Long> optIdEmpresaCarga = this.buscaIdEmpresaCargaPor(idCargaBeneficio, idEmpresa);

          if (!optIdEmpresaCarga.isPresent())
               this.geraEmpresasCargas(idCargaBeneficio, idEmpresa);
     }
}
