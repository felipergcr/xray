
package br.com.conductor.rhblueapi.service;

import java.util.List;
import java.util.Objects;

import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.OperacaoAcesso;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.request.OperacaoAcessoRequest;
import br.com.conductor.rhblueapi.domain.response.OperacaoAcessoResponse;
import br.com.conductor.rhblueapi.domain.response.PageOperacaoAcessoResponse;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.OperacaoAcessoRepository;
import br.com.conductor.rhblueapi.util.GenericConvert;

@Service
public class OperacaoAcessoService {

     @Autowired
     private OperacaoAcessoRepository operacaoAcessosRepository;

     public PageOperacaoAcessoResponse listar(OperacaoAcessoRequest request) {

          OperacaoAcesso operacao = GenericConvert.convertModelMapper(request, OperacaoAcesso.class);
          
          ExampleMatcher matcher = ExampleMatcher.matching().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

          Example<OperacaoAcesso> example = Example.of(operacao, matcher);

          Pageable pageable = PageRequest.of(request.getPage(), request.getSize());

          Page<OperacaoAcesso> operacoesAcessos = operacaoAcessosRepository.findAll(example, pageable);

          NotFoundCdt.checkThrow(Objects.isNull(operacoesAcessos) || !operacoesAcessos.hasContent(), ExceptionsMessagesCdtEnum.RECURSO_NAO_ENCONTRADO);
          
          PageOperacaoAcessoResponse pageResponse =  new PageOperacaoAcessoResponse(GenericConvert.convertModelMapperToPageResponse(operacoesAcessos, new TypeToken<List<OperacaoAcessoResponse>>() {}.getType()));

          return pageResponse;
     }

}
