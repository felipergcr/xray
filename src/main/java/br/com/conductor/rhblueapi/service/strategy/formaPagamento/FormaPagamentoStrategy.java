
package br.com.conductor.rhblueapi.service.strategy.formaPagamento;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;

public abstract class FormaPagamentoStrategy {

     public abstract void registrarPendenciaFinanceira(final ArquivoCargas arquivoCargas);
}
