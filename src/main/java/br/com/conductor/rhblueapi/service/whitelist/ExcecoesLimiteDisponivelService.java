
package br.com.conductor.rhblueapi.service.whitelist;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.whitelist.ExcecoesLimiteDisponivel;
import br.com.conductor.rhblueapi.repository.whitelist.ExcecoesLimiteDisponivelRepository;

@Service
public class ExcecoesLimiteDisponivelService {

     @Autowired
     private ExcecoesLimiteDisponivelRepository excecoesLimiteDisponivelRepository;

     private final int STATUS_ATIVO = 1;

     public Optional<ExcecoesLimiteDisponivel> obterPorGrupoEmpresaEAtivo(final long idGrupoEmpresa) {

          return excecoesLimiteDisponivelRepository.findTop1ByIdGrupoEmpresaAndStatus(idGrupoEmpresa, STATUS_ATIVO);
     }

     public boolean isListaDeExcecoes(final long idGrupoEmpresa) {

          return this.obterPorGrupoEmpresaEAtivo(idGrupoEmpresa).isPresent();
     }
}
