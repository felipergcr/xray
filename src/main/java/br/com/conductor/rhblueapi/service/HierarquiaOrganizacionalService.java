
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.HIERARQUIA_ORANIZACIONAL_EMPRESAS_NAO_ENCONTRADO;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.HIERARQUIA_ORGANIZACIONAL_INEXISTENTE;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.NAO_HA_HIERARQUIA_ORGANIZACIONAL_GRUPO;
import static br.com.conductor.rhblueapi.util.LambdaCustomUtils.distinctByKey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacional;
import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacionalCustom;
import br.com.conductor.rhblueapi.domain.HierarquiaOrganizacionalGrupoCustom;
import br.com.conductor.rhblueapi.domain.response.HierarquiaOrganizacionalEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaOrganizacionalGrupoResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaOrganizacionalSubGrupoResponse;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.HierarquiaOrganizacionalCustomRepository;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class HierarquiaOrganizacionalService {

     @Autowired
     private HierarquiaOrganizacionalCustomRepository hierarquiaOrganizacionalCustomRepository;
     
     @Autowired
     PermissoesUsuariosRhService permissoesUsuariosRhService;

     public HierarquiaOrganizacional buscaHierarquiaOrganizacional(Long idGrupoEmpresa, Long idSubgrupoEmpresa, Long idEmpresa) {

          List<HierarquiaOrganizacionalCustom> listaHierarquia = this.buscaHieraquiaOrganizacionalPor(idGrupoEmpresa, idSubgrupoEmpresa, idEmpresa);

          List<Long> listaIdGrupoEmpresa = listaHierarquia.stream().map(HierarquiaOrganizacionalCustom::getIdGrupoEmpresa).collect(Collectors.toList());
          List<Long> listaIdsSubgrupoEmpresas = listaHierarquia.stream().map(HierarquiaOrganizacionalCustom::getIdSubgrupoEmpresa).collect(Collectors.toList());
          List<Long> listaIdsEmpresas = listaHierarquia.stream().map(HierarquiaOrganizacionalCustom::getIdEmpresa).collect(Collectors.toList());

          HierarquiaOrganizacional hierarquiaOrganizacional = new HierarquiaOrganizacional();

          if (CollectionUtils.isNotEmpty(listaIdGrupoEmpresa))
               hierarquiaOrganizacional.setIdGrupoEmpresa(listaIdGrupoEmpresa.stream().findFirst().get());
          if (CollectionUtils.isNotEmpty(listaIdsSubgrupoEmpresas))
               hierarquiaOrganizacional.setListaIdsSubgrupoEmpresa(listaIdsSubgrupoEmpresas);
          if (CollectionUtils.isNotEmpty(listaIdsEmpresas))
               hierarquiaOrganizacional.setListaIdsEmpresas(listaIdsEmpresas);

          return hierarquiaOrganizacional;
     }

     private List<HierarquiaOrganizacionalCustom> buscaHieraquiaOrganizacionalPor(Long idGrupoEmpresa, Long idSubgrupoEmpresa, Long idEmpresa) {

          List<HierarquiaOrganizacionalCustom> listaHierarquia = hierarquiaOrganizacionalCustomRepository.buscaHierarquiaOrganizacional(idGrupoEmpresa, idSubgrupoEmpresa, idEmpresa);

          NotFoundCdt.checkThrow(CollectionUtils.isEmpty(listaHierarquia), HIERARQUIA_ORGANIZACIONAL_INEXISTENTE);

          return listaHierarquia;
     }

     public HierarquiaOrganizacionalGrupoResponse listarHierarquiaOrganizacionalGrupoResponse(final Long idGrupoEmpresa, final Long idUsuarioSessao) {

          List<Integer> idsEmpresas = permissoesUsuariosRhService.listarEmpresasPermitidas(idUsuarioSessao, idGrupoEmpresa);
          NoContentCdt.checkThrow(Objeto.isBlank(idsEmpresas), HIERARQUIA_ORANIZACIONAL_EMPRESAS_NAO_ENCONTRADO);

          return this.listarHierarquiaOrganizacionalGrupoResponse(idsEmpresas);
     }

     public HierarquiaOrganizacionalGrupoResponse listarHierarquiaOrganizacionalGrupoResponse(final List<Integer> idsEmpresas) {

          List<HierarquiaOrganizacionalGrupoCustom> listaHierarquicaGrupo = this.listarHierarquiaOrganizacionalGrupoCustom(idsEmpresas);
          NoContentCdt.checkThrow(Objects.isNull(listaHierarquicaGrupo), NAO_HA_HIERARQUIA_ORGANIZACIONAL_GRUPO);

          List<HierarquiaOrganizacionalGrupoCustom> listaHierarquicaUnicoGrupo = this.obterMesmoGrupo(listaHierarquicaGrupo);

          Map<Long, List<HierarquiaOrganizacionalEmpresaResponse>> subGruposMapeado = this.agruparSubGruposMap(listaHierarquicaUnicoGrupo);

          List<HierarquiaOrganizacionalSubGrupoResponse> listaSubGrupoResponse = this.listarHierarquiaOrganizacionalSubGrupoResponse(listaHierarquicaUnicoGrupo, subGruposMapeado);

          return this.converterGrupoResponse(listaHierarquicaUnicoGrupo.get(0), listaSubGrupoResponse);
     }

     public List<HierarquiaOrganizacionalGrupoCustom> listarHierarquiaOrganizacionalGrupoCustom(final List<Integer> idsEmpresas) {

          return hierarquiaOrganizacionalCustomRepository.listarHierarquiaOrganizacionalGrupoCustom(idsEmpresas);
     }

     private List<HierarquiaOrganizacionalGrupoCustom> obterMesmoGrupo(final List<HierarquiaOrganizacionalGrupoCustom> lista) {

          return lista.stream().filter(g -> Objects.equals(g.getIdGrupoEmpresa(), lista.get(0).getIdGrupoEmpresa())).collect(Collectors.toList());
     }

     private Map<Long, List<HierarquiaOrganizacionalEmpresaResponse>> agruparSubGruposMap(final List<HierarquiaOrganizacionalGrupoCustom> hierarquiaOrganizacionalGrupo) {

          return hierarquiaOrganizacionalGrupo.stream().collect(Collectors.groupingBy(HierarquiaOrganizacionalGrupoCustom::getIdSubGrupoEmpresa, Collectors.mapping(e -> this.converterEmpresaResponse(e), Collectors.toList())));
     }

     private HierarquiaOrganizacionalEmpresaResponse converterEmpresaResponse(final HierarquiaOrganizacionalGrupoCustom empresa) {

          return HierarquiaOrganizacionalEmpresaResponse.builder().id(empresa.getIdEmpresa()).cnpj(empresa.getCnpj()).nome(empresa.getNomeEmpresa()).build();
     }

     private List<HierarquiaOrganizacionalSubGrupoResponse> listarHierarquiaOrganizacionalSubGrupoResponse(final List<HierarquiaOrganizacionalGrupoCustom> hierarquiaOrganizacionalGrupo, Map<Long, List<HierarquiaOrganizacionalEmpresaResponse>> subGrupoResponseMap) {

          List<HierarquiaOrganizacionalSubGrupoResponse> subGruposResponse = new ArrayList<>();
          hierarquiaOrganizacionalGrupo.stream().filter(distinctByKey(s -> s.getIdSubGrupoEmpresa())).collect(Collectors.toList()).forEach(s -> {
               HierarquiaOrganizacionalSubGrupoResponse subGrupoResponse = this.converterSubGrupoResponse(s, subGrupoResponseMap);
               subGruposResponse.add(subGrupoResponse);
          });
          return subGruposResponse;
     }

     private HierarquiaOrganizacionalSubGrupoResponse converterSubGrupoResponse(final HierarquiaOrganizacionalGrupoCustom subgrupo, final Map<Long, List<HierarquiaOrganizacionalEmpresaResponse>> subGrupoResponseMap) {

          return HierarquiaOrganizacionalSubGrupoResponse.builder().id(subgrupo.getIdSubGrupoEmpresa()).nome(subgrupo.getNomeSubGrupoEmpresa()).empresas(subGrupoResponseMap.get(subgrupo.getIdSubGrupoEmpresa())).build();
     }

     private HierarquiaOrganizacionalGrupoResponse converterGrupoResponse(final HierarquiaOrganizacionalGrupoCustom grupo, final List<HierarquiaOrganizacionalSubGrupoResponse> subGrupos) {

          return HierarquiaOrganizacionalGrupoResponse.builder().idGrupoEmpresa(grupo.getIdGrupoEmpresa()).nome(grupo.getNomeGrupoEmpresa()).subGrupos(subGrupos).build();
     }

}
