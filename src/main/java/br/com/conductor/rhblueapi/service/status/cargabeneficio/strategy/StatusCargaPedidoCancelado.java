
package br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy;

import static br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum.CANCELADO;
import static br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum.CANCELADO_PARCIAL;

import br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum;

public class StatusCargaPedidoCancelado implements StatusCargaBeneficioStrategy {

     @Override
     public StatusPagamentoBaseEnum retornaStatusPagamento(boolean todosStatusIguais, int qtdCargasPedido, int qtdCargasPedidoPagas) {

          if (todosStatusIguais)
               return CANCELADO;

          return CANCELADO_PARCIAL;
     }

}