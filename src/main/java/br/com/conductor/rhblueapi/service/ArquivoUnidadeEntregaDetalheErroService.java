
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.NAO_HA_DETALHES_ARQUIVO_UNIDADE_ENTREGA_ERRO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaDetalheErro;
import br.com.conductor.rhblueapi.domain.LinhaUnidadeEntregaExcel;
import br.com.conductor.rhblueapi.domain.response.PageArquivoUnidadeEntregaDetalhesErrosReponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.domain.response.ArquivoUnidadeEntregaDetalheErroReponse;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.repository.ArquivoUnidadeEntregaDetalheErroCustomRepository;
import br.com.conductor.rhblueapi.repository.ArquivoUnidadeEntregaDetalheErroRepository;

@Service
public class ArquivoUnidadeEntregaDetalheErroService {

     @Autowired
     private ArquivoUnidadeEntregaDetalheErroCustomRepository arquivoUnidadeEntregaDetalheErroCustomRepository;

     @Autowired
     private ArquivoUnidadeEntregaDetalheErroRepository arquivoUnidadeEntregaDetalheErroRepository;

     @Async
     public void salvarErrosLinhaDetalhada(Long idArquivosUnidadeEntregaDetalhe, LinhaUnidadeEntregaExcel linhaValidada) {

          List<ArquivoUnidadeEntregaDetalheErro> loteErros = new ArrayList<>();
          linhaValidada.getErros().stream().forEach(erro -> {
               ArquivoUnidadeEntregaDetalheErro detalheErro = new ArquivoUnidadeEntregaDetalheErro();
               detalheErro.setIdArquivoUEDetalhe(idArquivosUnidadeEntregaDetalhe);
               detalheErro.setMotivo(erro);
               loteErros.add(detalheErro);
          });
          arquivoUnidadeEntregaDetalheErroRepository.saveAll(loteErros);
     }

     public PageArquivoUnidadeEntregaDetalhesErrosReponse buscarErrosUploadArquivoUnidadeEntrega(Long idArquivoUnidadeEntrega, Pageable pageable) {

          PageResponse<ArquivoUnidadeEntregaDetalheErroReponse> listaErrosPaginado = arquivoUnidadeEntregaDetalheErroCustomRepository.findByIdArquivoUnidadeEntregaWithPageable(idArquivoUnidadeEntrega, pageable);
          NoContentCdt.checkThrow(Objects.isNull(listaErrosPaginado.getContent()) || listaErrosPaginado.getContent().isEmpty(), NAO_HA_DETALHES_ARQUIVO_UNIDADE_ENTREGA_ERRO);

          return new PageArquivoUnidadeEntregaDetalhesErrosReponse(listaErrosPaginado);
     }

}
