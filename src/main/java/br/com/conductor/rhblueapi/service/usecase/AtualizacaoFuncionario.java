
package br.com.conductor.rhblueapi.service.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.pedido.MensagemGerarCarga;
import br.com.conductor.rhblueapi.domain.proc.BeneficioFuncionarioProdutoResponse;
import br.com.conductor.rhblueapi.domain.proc.CadastroFuncionarioDetalhe;
import br.com.conductor.rhblueapi.repository.BeneficioFuncionarioRepository;

@Component
public class AtualizacaoFuncionario {
     
     @Autowired
     private BeneficioFuncionarioRepository beneficioFuncionarioRepository;
     
     public CadastroFuncionarioDetalhe atualizaFuncionarioProduto(final MensagemGerarCarga mensagem) {

          final CadastroFuncionarioDetalhe cadastroFuncionarioDetalhe = new CadastroFuncionarioDetalhe();

          cadastroFuncionarioDetalhe.setIdEmpresa(mensagem.getFuncionarioMinimoResponse().getIdEmpresa());
          cadastroFuncionarioDetalhe.setIdFuncionario(mensagem.getFuncionarioMinimoResponse().getIdFuncionario());
          cadastroFuncionarioDetalhe.setIdPessoa(mensagem.getFuncionarioMinimoResponse().getIdPessoa());

          final BeneficioFuncionarioProdutoResponse respFuncionarioProduto = this.beneficioFuncionarioRepository
                    .atualizaFuncionarioProduto(Long.valueOf(mensagem.getMensagemItemPedido().getDetalhe().getProduto()), mensagem.getFuncionarioMinimoResponse().getIdFuncionario());

          cadastroFuncionarioDetalhe.setIdConta(respFuncionarioProduto.getIdConta());
          cadastroFuncionarioDetalhe.setIdFuncionarioProduto(respFuncionarioProduto.getIdFuncionarioProduto());
          cadastroFuncionarioDetalhe.setFlagCartao(respFuncionarioProduto.getFlagCartao());
          
          return cadastroFuncionarioDetalhe;
     }
}
