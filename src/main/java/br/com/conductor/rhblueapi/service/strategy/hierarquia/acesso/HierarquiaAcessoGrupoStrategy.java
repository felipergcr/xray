
package br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoGrupoEmpresaResponse;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.service.GrupoEmpresaService;

@Service
public class HierarquiaAcessoGrupoStrategy extends HierarquiaAcessoStrategy {

     private static final String ACESSOS = "acessos";

     private static String NIVEL_PERMISSAO_ACESSO = "nivelPermissao";

     @Autowired
     private GrupoEmpresaService grupoEmpresaService;

     @Override
     public List<UsuarioPermissaoNivelAcessoRh> retornaAcessoPermitido(Map<String, Object> acessoUserLogado, Map<String, Object> acessoUserPretendido) {

          List<UsuarioPermissaoNivelAcessoRh> grupoAcessoUserPretendido = (List<UsuarioPermissaoNivelAcessoRh>) acessoUserPretendido.get(ACESSOS);

          Long idGrupoAcessoPretendido = grupoAcessoUserPretendido.stream().map(UsuarioPermissaoNivelAcessoRh::getIdGrupoEmpresa).findFirst().orElse(0L);

          List<UsuarioPermissaoNivelAcessoRh> grupoAcessoUserLogado = (List<UsuarioPermissaoNivelAcessoRh>) acessoUserLogado.get(ACESSOS);

          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.SUBGRUPO)) {

               return filtrarPorAcessoSubgrupo(grupoAcessoUserPretendido, idGrupoAcessoPretendido, grupoAcessoUserLogado);
          }

          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.EMPRESA)) {

               return filtrarAcessoPorEmpresa(grupoAcessoUserPretendido, idGrupoAcessoPretendido, grupoAcessoUserLogado);
          }

          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.HIBRIDO)) {

               List<UsuarioPermissaoNivelAcessoRh> acessoPorSubgrupo = filtrarPorAcessoSubgrupo(grupoAcessoUserPretendido, idGrupoAcessoPretendido, grupoAcessoUserLogado);

               List<UsuarioPermissaoNivelAcessoRh> acessoPorEmpresa = filtrarAcessoPorEmpresa(grupoAcessoUserPretendido, idGrupoAcessoPretendido, grupoAcessoUserLogado);

               return mergeListAcessos(acessoPorSubgrupo, acessoPorEmpresa);

          }

          return filtraAcessoPorGrupo(grupoAcessoUserPretendido, idGrupoAcessoPretendido, grupoAcessoUserLogado);
     }

     public HierarquiaAcessoGrupoEmpresaResponse mapHierarquiaGrupo(Long idGrupo) {

          if (Objects.isNull(idGrupo)) {
               return null;
          }

          GrupoEmpresa grupoEmpresa = grupoEmpresaService.buscaGrupoEmpresaPor(idGrupo).get();

          return HierarquiaAcessoGrupoEmpresaResponse.builder().idGrupoEmpresa(idGrupo).nomeGrupoEmpresa(grupoEmpresa.getNomeGrupo()).build();

     }

     private List<UsuarioPermissaoNivelAcessoRh> filtraAcessoPorGrupo(List<UsuarioPermissaoNivelAcessoRh> grupoAcessoUserPretendido, Long idGrupoAcessoPretendido, List<UsuarioPermissaoNivelAcessoRh> grupoAcessoUserLogado) {

          return (grupoAcessoUserLogado.stream().anyMatch(nivel -> nivel.getIdGrupoEmpresa().equals(idGrupoAcessoPretendido))) ? grupoAcessoUserPretendido : Collections.emptyList();
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtrarAcessoPorEmpresa(List<UsuarioPermissaoNivelAcessoRh> grupoAcessoUserPretendido, Long idGrupoAcessoPretendido, List<UsuarioPermissaoNivelAcessoRh> grupoAcessoUserLogado) {

          Long idSubgrupoEmpresaUserLogado = grupoAcessoUserLogado.stream().filter(id -> Objects.nonNull(id.getEmpresa())).mapToLong(nivel -> nivel.getEmpresa().getIdGrupoEmpresa()).sum();

          Optional<GrupoEmpresa> subgrupoUserLogado = grupoEmpresaService.buscaGrupoEmpresaPor(idSubgrupoEmpresaUserLogado);

          return (Objects.equals(idGrupoAcessoPretendido, subgrupoUserLogado.get().getIdGrupoEmpresaPai())) ? grupoAcessoUserPretendido : Collections.emptyList();
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtrarPorAcessoSubgrupo(List<UsuarioPermissaoNivelAcessoRh> grupoAcessoUserPretendido, Long idGrupoAcessoPretendido, List<UsuarioPermissaoNivelAcessoRh> grupoAcessoUserLogado) {

          Long idGrupoPaiUserLogado = grupoAcessoUserLogado.stream().map(nivel -> nivel.getSubgrupo().getIdGrupoEmpresaPai()).findFirst().orElse(null);

          return (Objects.equals(idGrupoAcessoPretendido, idGrupoPaiUserLogado)) ? grupoAcessoUserPretendido : Collections.emptyList();
     }

     private List<UsuarioPermissaoNivelAcessoRh> mergeListAcessos(List<UsuarioPermissaoNivelAcessoRh> listAcessoPrimaria, List<UsuarioPermissaoNivelAcessoRh> listAcessoSecundaria) {

          List<UsuarioPermissaoNivelAcessoRh> acessosPermitidos = new ArrayList<>();

          listAcessoPrimaria.forEach(nivel -> acessosPermitidos.add(nivel));

          listAcessoSecundaria.forEach(nivel -> acessosPermitidos.add(nivel));

          return acessosPermitidos.stream().distinct().collect(Collectors.toList());
     }
}
