
package br.com.conductor.rhblueapi.service.strategy.formaPagamento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.FormasPagamento;

@Component
public class FormaPagamentoFactory {

     @Autowired
     private TedStrategy tedStrategy;

     @Autowired
     private BoletoStrategy boletoStrategy;

     @Autowired
     private DebitoContaStrategy debitoContaStrategy;

     public FormaPagamentoStrategy criar(FormasPagamento formaPagamento) {

          switch (formaPagamento) {
               case TED_DOC:
                    return tedStrategy;

               case BOLETO:
                    return boletoStrategy;

               default:
                    return debitoContaStrategy;
          }
     }

}
