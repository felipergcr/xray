
package br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.strategy;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.twsoftware.alfred.object.Objeto;

@Component
public class PosPago implements TipoContratoStrategy {
     
     @Value("${app.regras.cadastro.grupo-empresa.parametros.prazo-pagamento}")
     private Integer qtdMaxDiasPrazoPagamento;
     
     @Value("${app.regras.pedido.quantidade-dias-minimo-upload.pos}")
     private Integer qtdDiasMinimoUploadPedido;

     @Override
     public void validaDataVigenciaLimite(LocalDate data) {

          BadRequestCdt.checkThrow(Objeto.isBlank(data), ExceptionsMessagesCdtEnum.GRUPOEMPRESA_REQUEST_DATA_VIGENCIA_OBRIGATORIA);

          BadRequestCdt.checkThrow(data.isBefore(LocalDate.now()), ExceptionsMessagesCdtEnum.GRUPOEMPRESA_REQUEST_DATA_VIGENCIA_INFERIOR_DATA_ATUAL);
     }

     @Override
     public void validaPrazoPagamento(Integer prazoPagamento) {

          BadRequestCdt.checkThrow(Objeto.isBlank(prazoPagamento), ExceptionsMessagesCdtEnum.GRUPOEMPRESA_REQUEST_PRAZO_PAGAMENTO_OBRIGATORIO);

          BadRequestCdt.checkThrow(prazoPagamento < 0, ExceptionsMessagesCdtEnum.GRUPOEMPRESA_REQUEST_PRAZO_PAGAMENTO_MENOR_QUE_ZERO);
          
          BadRequestCdt.checkThrow(prazoPagamento > qtdMaxDiasPrazoPagamento, ExceptionsMessagesCdtEnum.GRUPOEMPRESA_REQUEST_PRAZO_PAGAMENTO_MAIOR_QUE_VALOR_MAXIMO, 
                    String.format(ExceptionsMessagesCdtEnum.GRUPOEMPRESA_REQUEST_PRAZO_PAGAMENTO_MAIOR_QUE_VALOR_MAXIMO.getMessage(), qtdMaxDiasPrazoPagamento));
          
     }
     
     @Override
     public Integer aplicaRegraPrazoPagamento(Integer prazoPagamento) {

          return prazoPagamento;
     }

     @Override
     public Integer obtemQuantidadeDiasMinimoUploadPedido() {
          return qtdDiasMinimoUploadPedido;
     }

}
