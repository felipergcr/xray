
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.ARQUIVO_CARGAS_STATUS_IMPORTADO_NAO_ENCONTRADO;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.ARQUIVO_CARGAS_STATUS_RECEBIDO_NAO_ENCONTRADO;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.FORMA_PAGAMENTO_DIFERENTE_TED;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.GRUPO_EMPRESA_NAO_ENCONTRADO_PRE_CONDITION;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.NIVEL_ACESSO_NAO_ENCONTRADO;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.PERMISSAO_NAO_ENCONTRADA;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.PESQUISA_NAO_ENCONTRADA;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.USUARIO_NAO_ENCONTRADO_PRE_CONDITION;
import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.IMPORTADO;
import static br.com.conductor.rhblueapi.enums.StatusArquivoEnum.RECEBIDO;
import static br.com.conductor.rhblueapi.enums.StatusPermissaoEnum.ATIVO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_CARGAS;
import static br.com.conductor.rhblueapi.enums.TipoStatus.PERMISSOES_USUARIOS_RH;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.FLAG_FATURAMENTO_CENTRALIZADO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ParametrosEnum.FORMA_PAGAMENTO;
import static br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum.CENTRALIZADO;
import static br.com.conductor.rhblueapi.exception.ExceptionCdt.checkThrow;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;
import static br.com.conductor.rhblueapi.util.AppConstantes.PARAMETRO_RH;
import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.repository.UsuarioRepository;
import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.PermissoesUsuariosRh;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.UnidadeEntrega;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.response.NotificacoesArquivoPedidoResponse;
import br.com.conductor.rhblueapi.domain.response.NotificacoesArquivoUnidadeEntregaResponse;
import br.com.conductor.rhblueapi.domain.response.PendenciasPagamentoTedResponse;
import br.com.conductor.rhblueapi.enums.StatusUnidadesEntrega;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPagamento;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import br.com.conductor.rhblueapi.repository.NotificacaoPendenciaTedCustomRepository;
import br.com.conductor.rhblueapi.repository.PermissoesUsuariosRhRepository;
import br.com.conductor.rhblueapi.repository.UsuarioNivelPermissaoRhRepository;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class NotificacaoService {

     @Autowired
     private UsuarioRepository usuarioRepository;

     @Autowired
     private GrupoEmpresaRepository grupoEmpresaRepository;

     @Autowired
     private ParametrosService parametrosService;

     @Autowired
     private PermissoesUsuariosRhRepository permissoesUsuariosRhRepository;

     @Autowired
     private UsuarioNivelPermissaoRhRepository usuarioNivelPermissaoRhRepository;

     @Autowired
     private StatusCached statusCached;

     @Autowired
     private NotificacaoPendenciaTedCustomRepository notificacaoPendenciaTedCustomRepository;

     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;

     @Autowired
     private PedidoService pedidoService;

     private List<StatusDescricao> dominioStatusCargas;

     @Autowired
     private ArquivoUnidadeEntregaService arquivoUnidadeEntregaService;

     @Autowired
     private UnidadeEntregaService unidadeEntregaService;

     @Value("${app.plataforma.rh.dados-bancarios-ted.nome}")
     private String nome;

     @Value("${app.plataforma.rh.dados-bancarios-ted.cnpj}")
     private String cnpj;

     @Value("${app.plataforma.rh.dados-bancarios-ted.banco}")
     private String banco;

     @Value("${app.plataforma.rh.dados-bancarios-ted.agencia}")
     private String agencia;

     @Value("${app.plataforma.rh.dados-bancarios-ted.conta}")
     private String conta;
     
     public PendenciasPagamentoTedResponse buscaPendenciasPagamentoTed(IdentificacaoRequest identificacaoRequest) {
          
          Optional<GrupoEmpresa> grupoEmpresa = this.grupoEmpresaRepository.findById(identificacaoRequest.getIdGrupoEmpresa());
          checkThrow(!grupoEmpresa.isPresent(), GRUPO_EMPRESA_NAO_ENCONTRADO_PRE_CONDITION);

          Optional<Usuario> usuario = this.usuarioRepository.findById(identificacaoRequest.getIdUsuario());
          checkThrow(!usuario.isPresent(), USUARIO_NAO_ENCONTRADO_PRE_CONDITION);

          dominioStatusCargas = this.statusCached.busca(PERMISSOES_USUARIOS_RH.getNome());
          final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, ATIVO.name());

          Optional<PermissoesUsuariosRh> optPermissao = permissoesUsuariosRhRepository.findTopByIdUsuarioAndGrupoEmpresaIdAndStatusOrderByIdDesc(identificacaoRequest.getIdUsuario(), identificacaoRequest.getIdGrupoEmpresa(), statusPermissaoAtiva.getStatus());

          checkThrow(!optPermissao.isPresent(), PERMISSAO_NAO_ENCONTRADA);

          String formaPagamento = parametrosService.buscaParametrosGrupoEmpresaPor(identificacaoRequest.getIdGrupoEmpresa(), PARAMETRO_RH, FORMA_PAGAMENTO).getValor();

          checkThrow(!equalsIgnoreCase(TipoPagamento.TED.getValor(), formaPagamento), FORMA_PAGAMENTO_DIFERENTE_TED);

          List<Integer> ids = this.buscaPendenciasPorFaturamento(identificacaoRequest, optPermissao.get().getId());

          Integer numeroPedido = ids.stream().sorted().findFirst().get();

          return PendenciasPagamentoTedResponse.builder()
                    .nome(nome)
                    .cnpj(cnpj)
                    .banco(banco)
                    .agencia(agencia)
                    .conta(conta)
                    .numeroPedido(numeroPedido)
                    .build();
     } 
     
     private List<Integer> buscaPendenciasPorFaturamento(IdentificacaoRequest identificacaoRequest, Long idPermissao) {
          
          String tipoFaturamento = parametrosService.buscaParametrosGrupoEmpresaPor(identificacaoRequest.getIdGrupoEmpresa(), PARAMETRO_RH, FLAG_FATURAMENTO_CENTRALIZADO).getValor();

          List<Integer> idsPedidos = new ArrayList<Integer>();

          if (equalsIgnoreCase(CENTRALIZADO.getValor().toString(), tipoFaturamento)) {

               dominioStatusCargas = this.statusCached.busca(TipoStatus.USUARIOS_NIVEIS_PERMISSOES_RH.getNome());
               final StatusDescricao statusPermissaoAtiva = mapStatusDescricao(dominioStatusCargas, ATIVO.name());

               Optional<UsuarioPermissaoNivelAcessoRh> optNivelPermissao = usuarioNivelPermissaoRhRepository.findByIdPermissaoAndIdGrupoEmpresaAndStatus(idPermissao, identificacaoRequest.getIdGrupoEmpresa(), statusPermissaoAtiva.getStatus());

               checkThrow(!optNivelPermissao.isPresent() || Objects.isNull(optNivelPermissao.get().getIdGrupoEmpresa()), NIVEL_ACESSO_NAO_ENCONTRADO);

               idsPedidos = notificacaoPendenciaTedCustomRepository.buscaPendenciasTedCentralizado(identificacaoRequest.getIdGrupoEmpresa());

          } else {

               List<Integer> idsEmpresas = permissoesUsuariosRhRepository.findEmpresasbyIdPermissao(idPermissao);
               checkThrow(CollectionUtils.isEmpty(idsEmpresas), USUARIO_SESSAO_SEM_PERMISSAO);

               idsPedidos = notificacaoPendenciaTedCustomRepository.buscaPendenciasTedDescentralizado(idsEmpresas);

          }

          checkThrow(CollectionUtils.isEmpty(idsPedidos), PESQUISA_NAO_ENCONTRADA);

          return idsPedidos;

     }

     public NotificacoesArquivoUnidadeEntregaResponse buscaArquivoUnidadeEntregaEmProcessamento(IdentificacaoRequest identificacaoRequest) {

          Boolean existeUnidadeEntregaCadastradaBoolean = Boolean.FALSE;

          Boolean existeArquivoDeUnidadeEntregaEmProcessamentoBoolean = arquivoUnidadeEntregaService.isVerificarArquivoUnidadeEntregaEmProcessamento(identificacaoRequest.getIdGrupoEmpresa()) ? Boolean.TRUE : Boolean.FALSE;

          Optional<List<UnidadeEntrega>> existeUnidadeCadastrada = unidadeEntregaService.buscaUnidadesPorGrupoEmpresaEStatus(identificacaoRequest.getIdGrupoEmpresa(), StatusUnidadesEntrega.ATIVO);
          if (existeUnidadeCadastrada.isPresent())
               existeUnidadeEntregaCadastradaBoolean = Boolean.TRUE;

          return NotificacoesArquivoUnidadeEntregaResponse.builder().existeArquivoDeUnidadeEntregaEmProcessamento(existeArquivoDeUnidadeEntregaEmProcessamentoBoolean).existeUnidadeEntregaCadastrada(existeUnidadeEntregaCadastradaBoolean).build();
     }

     public NotificacoesArquivoPedidoResponse buscaArquivoProcessoEmProcessamento(IdentificacaoRequest identificacaoRequest) {

          usuarioNivelPermissaoRhService.autenticarNivelAcessoRhGrupo(identificacaoRequest.getIdUsuario(), identificacaoRequest.getIdGrupoEmpresa());

          List<StatusDescricao> dominioStatusCargas = statusCached.busca(ARQUIVO_CARGAS.getNome());

          final StatusDescricao statusRecebido = mapStatusDescricao(dominioStatusCargas, RECEBIDO.getStatus());
          checkThrow(Objeto.isBlank(statusRecebido), ARQUIVO_CARGAS_STATUS_RECEBIDO_NAO_ENCONTRADO);

          final StatusDescricao statusImportado = mapStatusDescricao(dominioStatusCargas, IMPORTADO.getStatus());
          checkThrow(Objeto.isBlank(statusImportado), ARQUIVO_CARGAS_STATUS_IMPORTADO_NAO_ENCONTRADO);

          List<Integer> statuses = new ArrayList<Integer>();
          Optional.ofNullable(statusRecebido).ifPresent(s -> statuses.add(s.getStatus()));
          Optional.ofNullable(statusImportado).ifPresent(s -> statuses.add(s.getStatus()));

          return NotificacoesArquivoPedidoResponse.builder().existeArquivoDePedidoEmProcessamento(pedidoService.existeArquivoPedidoPorGrupoComStatus(identificacaoRequest.getIdGrupoEmpresa(), statuses)).build();
     }

}
