
package br.com.conductor.rhblueapi.service.status.cargabeneficio.strategy;

import static br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum.LIMITE_CREDITO;

import br.com.conductor.rhblueapi.service.status.pagamento.enumeration.StatusPagamentoBaseEnum;

public class StatusCargaPendenciaLimite implements StatusCargaBeneficioStrategy {

     @Override
     public StatusPagamentoBaseEnum retornaStatusPagamento(boolean todosStatusIguais, int qtdCargasPedido, int qtdCargasPedidoPagas) {

          return LIMITE_CREDITO;
     }

}
