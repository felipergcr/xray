
package br.com.conductor.rhblueapi.service.grupoempresa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.persist.SubgrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.update.GrupoEmpresaUpdate;
import br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.factory.TipoContratoFactory;
import br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.strategy.TipoContratoStrategy;

@Service
public class CadastroGrupoEmpresaValidacaoService {
     
     @Autowired
     private TipoContratoFactory tipoContratoFactory;

     public void validaRequest(GrupoEmpresaPersist request) {
          
          TipoContratoStrategy strategy = tipoContratoFactory.newStrategy(request.getParametros().getTipoContrato());
          strategy.validaDataVigenciaLimite(request.getParametros().getDataVigenciaLimite());
          strategy.validaPrazoPagamento(request.getParametros().getPrazoPagamento());
          
     }

     public void validaRequest(GrupoEmpresaUpdate request) {

          TipoContratoStrategy strategy = tipoContratoFactory.newStrategy(request.getTipoContrato());
          strategy.validaDataVigenciaLimite(request.getDataVigenciaLimite());
          strategy.validaPrazoPagamento(request.getPrazoPagamento());
     }
     
     public void validaRequest(SubgrupoEmpresaPersist request) {

          TipoContratoStrategy strategy = tipoContratoFactory.newStrategy(request.getParametros().getTipoContrato());
          strategy.validaDataVigenciaLimite(request.getParametros().getDataVigenciaLimite());
          strategy.validaPrazoPagamento(request.getParametros().getPrazoPagamento());
     }

}
