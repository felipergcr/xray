
package br.com.conductor.rhblueapi.service.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.email.GenericoEmail;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;

@Service
public class EmailPendenciaTedService extends EmailUtilService<ArquivoCargas, StatusArquivoEnum> {

     @Value("${app.pier.notificacao.parametros.header.img}")
     private String header_img;

     @Value("${app.pier.notificacao.parametros.footer.img}")
     private String footer_img;

     @Value("${app.pier.notificacao.parametros.pendencias-ted.id}")
     private Long idTemplateNotificacaoPendenciaTed;

     @Value("${app.pier.notificacao.parametros.url.detalhes}")
     private String pedidoDetalhadoLink;

     @Value("${app.plataforma.rh.dados-bancarios-ted.nome}")
     private String nome;

     @Value("${app.plataforma.rh.dados-bancarios-ted.cnpj}")
     private String cnpj;

     @Value("${app.plataforma.rh.dados-bancarios-ted.banco}")
     private String banco;

     @Value("${app.plataforma.rh.dados-bancarios-ted.agencia}")
     private String agencia;

     @Value("${app.plataforma.rh.dados-bancarios-ted.conta}")
     private String conta;

     @Override
     public void enviar(ArquivoCargas arquivo, StatusArquivoEnum status) {

          GenericoEmail<StatusArquivoEnum> genericoEmail = new GenericoEmail<StatusArquivoEnum>();

          genericoEmail.setIdUsuario(arquivo.getIdUsuario());
          genericoEmail.setIdObjeto(arquivo.getId());
          genericoEmail.setDataHora(arquivo.getDataStatus());
          genericoEmail.setStatus(status);

          super.enviar(genericoEmail);

     }

     @Override
     Long getContentTemplate(StatusArquivoEnum status) {
          return idTemplateNotificacaoPendenciaTed;
     }

     @Override
     String getContentUrl(Long idPath, StatusArquivoEnum status) {
          return String.format(pedidoDetalhadoLink, idPath);
     }

     @Override
     Map<String, Object> popularParametros(GenericoEmail<StatusArquivoEnum> genericoEmail, String nomeDestinatario, String url) {

          Map<String, Object> parametros = new HashMap<>();
          parametros.put("user_name", nomeDestinatario);
          parametros.put("orders_link", url);
          parametros.put("order_number", genericoEmail.getIdObjeto());
          parametros.put("date", genericoEmail.getDate());
          parametros.put("time", genericoEmail.getHour());
          parametros.put("header_img", header_img);
          parametros.put("footer_img", footer_img);
          parametros.put("dados_bancarios_ted_nome", nome);
          parametros.put("dados_bancarios_ted_cnpj", cnpj);
          parametros.put("dados_bancarios_ted_banco", banco);
          parametros.put("dados_bancarios_ted_agencia", agencia);
          parametros.put("dados_bancarios_ted_conta_corrente", conta);

          return parametros;
     }

}
