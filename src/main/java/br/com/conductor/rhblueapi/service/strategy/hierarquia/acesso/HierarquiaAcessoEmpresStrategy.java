
package br.com.conductor.rhblueapi.service.strategy.hierarquia.acesso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.UsuarioPermissaoNivelAcessoRh;
import br.com.conductor.rhblueapi.domain.request.EmpresaRequest;
import br.com.conductor.rhblueapi.domain.response.EmpresaCustomResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoEmpresaResponse;
import br.com.conductor.rhblueapi.domain.response.HierarquiaAcessoSubGrupoResponse;
import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.service.EmpresaService;
import br.com.conductor.rhblueapi.service.GrupoEmpresaService;

@Service
public class HierarquiaAcessoEmpresStrategy extends HierarquiaAcessoStrategy {

     private static final String ACESSOS = "acessos";

     private static String NIVEL_PERMISSAO_ACESSO = "nivelPermissao";

     @Autowired
     private EmpresaService empresaService;

     @Autowired
     private GrupoEmpresaService grupoEmpresaService;

     @Override
     public List<UsuarioPermissaoNivelAcessoRh> retornaAcessoPermitido(Map<String, Object> acessoUserLogado, Map<String, Object> acessoUserPretendido) {

          List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado = getAcessos(acessoUserLogado);

          List<Long> idEmpresaUserLogado = nivelAcessoUserLogado.stream().map(UsuarioPermissaoNivelAcessoRh::getIdEmpresa).collect(Collectors.toList());

          List<UsuarioPermissaoNivelAcessoRh> nivelEmpresaUserPretendido =  getAcessos(acessoUserPretendido);

          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.GRUPO)) 
               return nivelEmpresaUserPretendido;

          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.SUBGRUPO)) 
               return filtrarPorAcessoSubgrupo(nivelAcessoUserLogado, nivelEmpresaUserPretendido);
          
          if (acessoUserLogado.get(NIVEL_PERMISSAO_ACESSO).equals(NivelPermissaoEnum.HIBRIDO)) {
              
               List<UsuarioPermissaoNivelAcessoRh> acessoPorSubgrupos = filtrarPorAcessoSubgrupo(nivelAcessoUserLogado, nivelEmpresaUserPretendido);
               
               List<UsuarioPermissaoNivelAcessoRh> acessosPorEmpresas =  filtrarPorAcessoEmpresa(idEmpresaUserLogado, nivelEmpresaUserPretendido);
               
               return mergeListAcessos(acessoPorSubgrupos,acessosPorEmpresas);
          }
               
          return filtrarPorAcessoEmpresa(idEmpresaUserLogado, nivelEmpresaUserPretendido);
     }
     
     public List<HierarquiaAcessoSubGrupoResponse> mapHierarquiaSubGrupoEmpresa(List<UsuarioPermissaoNivelAcessoRh> acesso) {

          List<HierarquiaAcessoSubGrupoResponse> listNivelSubGrupos = new ArrayList<>();

          if (acesso.isEmpty()) {
               return null;
          }

          acesso.forEach(action -> {

               Optional<GrupoEmpresa> subgrupoEmpresa = grupoEmpresaService.buscaGrupoEmpresaPor(action.getEmpresa().getIdGrupoEmpresa());

               if (subgrupoEmpresa.isPresent()) {
                    
                    HierarquiaAcessoSubGrupoResponse subrupo = HierarquiaAcessoSubGrupoResponse.builder().idSubgrupo(subgrupoEmpresa.get().getId()).nomeDescricao(subgrupoEmpresa.get().getNomeGrupo()).build();

                    List<UsuarioPermissaoNivelAcessoRh> empresas = acesso.stream().filter(nivel -> nivel.getEmpresa().getIdGrupoEmpresa().equals(subrupo.getIdSubgrupo())).collect(Collectors.toList());

                    subrupo.setEmpresas(buildHierarquiaEmpresa(empresas));

                    listNivelSubGrupos.add(subrupo);
               }

          });

          return listNivelSubGrupos;

     }

     private List<UsuarioPermissaoNivelAcessoRh> filtrarPorAcessoEmpresa(List<Long> idEmpresaUserLogado, List<UsuarioPermissaoNivelAcessoRh> nivelEmpresaUserPretendido) {

          return nivelEmpresaUserPretendido.stream().filter(empresa -> idEmpresaUserLogado.contains(empresa.getIdEmpresa())).collect(Collectors.toList());
     }

     private List<UsuarioPermissaoNivelAcessoRh> filtrarPorAcessoSubgrupo(List<UsuarioPermissaoNivelAcessoRh> nivelAcessoUserLogado, List<UsuarioPermissaoNivelAcessoRh> nivelEmpresaUserPretendido) {

          List<EmpresaCustomResponse> empresaSubgrupoUserLogado = new ArrayList<>();

          List<Long> subGruposUserLogado = nivelAcessoUserLogado.stream().map(UsuarioPermissaoNivelAcessoRh::getIdSubgrupoEmpresa).collect(Collectors.toList());

          subGruposUserLogado.stream().filter(subgrupo-> Objects.nonNull(subgrupo)).forEach(id -> {

               empresaSubgrupoUserLogado.addAll(empresaService.listarEmpresasdoSubGrupoEmpresa(id, 0, 10).content);
          });

          List<Long> idEmpresasUserLogado = empresaSubgrupoUserLogado.stream().map(EmpresaCustomResponse::getId).collect(Collectors.toList());

          return filtrarPorAcessoEmpresa(idEmpresasUserLogado, nivelEmpresaUserPretendido);
     }

     private List<HierarquiaAcessoEmpresaResponse> buildHierarquiaEmpresa(List<UsuarioPermissaoNivelAcessoRh> empresas) {

          EmpresaRequest empresasRequest = new EmpresaRequest();

          empresasRequest.setIdsEmpresas(empresas.stream().map(empresa -> empresa.getIdEmpresa()).collect(Collectors.toList()));

          List<HierarquiaAcessoEmpresaResponse> listEmpresas = new ArrayList<>();

          empresaService.listar(empresasRequest).forEach(action -> {

               listEmpresas.add(HierarquiaAcessoEmpresaResponse.builder().cnpj(action.getCnpj()).idEmpresa(action.getIdEmpresa()).nomeEmpresa(action.getNomeEmpresa()).build());
          });

          return listEmpresas;

     }

     private List<UsuarioPermissaoNivelAcessoRh> getAcessos(Map<String, Object> acesso) {

          return (List<UsuarioPermissaoNivelAcessoRh>) acesso.get(ACESSOS);
     }
     
     private List<UsuarioPermissaoNivelAcessoRh> mergeListAcessos(List<UsuarioPermissaoNivelAcessoRh> listAcessoPrimaria, List<UsuarioPermissaoNivelAcessoRh> listAcessoSecundaria) {

          List<UsuarioPermissaoNivelAcessoRh> acessosPermitidos = new ArrayList<>();

          listAcessoPrimaria.forEach(nivel -> acessosPermitidos.add(nivel));

          listAcessoSecundaria.forEach(nivel -> acessosPermitidos.add(nivel));

          return acessosPermitidos.stream().distinct().collect(Collectors.toList());
     }

}
