
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.StatusDetalheEnum.INVALIDADO;
import static br.com.conductor.rhblueapi.enums.StatusDetalheEnum.VALIDADO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.ARQUIVO_UNIDADE_ENTREGA_DETALHE;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntrega;
import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaDetalhe;
import br.com.conductor.rhblueapi.domain.LinhaUnidadeEntregaExcel;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.enums.HeaderUnidadeEntregaEnum;
import br.com.conductor.rhblueapi.enums.StatusArquivoUnidadeEntregaDetalhe;
import br.com.conductor.rhblueapi.enums.TemplatesExcel;
import br.com.conductor.rhblueapi.enums.TipoStatus;
import br.com.conductor.rhblueapi.exception.ConteudoExcelException;
import br.com.conductor.rhblueapi.repository.ArquivoUnidadeEntregaDetalheRepository;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;
import br.com.conductor.rhblueapi.util.RhHeaderDefinition;
import br.com.conductor.rhblueapi.util.ValidacaoTemplate;

@Service
public class UnidadeEntregaDetalheService implements Serializable {

     private static final long serialVersionUID = 1L;

     @Autowired
     private StatusCached statusCached;

     @Autowired
     private ArquivoUnidadeEntregaDetalheRepository arquivoUnidadeEntregaDetalheRepository;

     @Autowired
     private ArquivoUnidadeEntregaDetalheErroService arquivoUnidadeEntregaDetalheErroService;

     private List<StatusDescricao> dominioStatusUnidadeEntregaDetalhe;

     @Transactional
     public void deletarArquivoCargasDetalhadoPor(Long idArquivoUnidadeEntrega) {

          this.arquivoUnidadeEntregaDetalheRepository.deleteByIdArquivoUE(idArquivoUnidadeEntrega);

     }

     public boolean isConteudoValido(final Workbook workbook, final TemplatesExcel template, final ArquivoUnidadeEntrega arquivoUnidadeEntrega) {
          
          final Sheet sheet = workbook.getSheetAt(template.getIndiceAba());
          
          Integer linhaCabecalho = 0;

          String tokenInicioCabecalho = HeaderUnidadeEntregaEnum.CODIGO_LOCAL_ENTREGA.getHeader();
          for (Row row : sheet) {

               if (row.getCell(0) == null) {
                    linhaCabecalho++;
                    continue;
               }

               if (tokenInicioCabecalho.equals(row.getCell(0).getStringCellValue())) {
                    break;
               }
               linhaCabecalho++;
          }
          
          List<RhHeaderDefinition> header = Arrays.asList(HeaderUnidadeEntregaEnum.values());
          
          LinhaUnidadeEntregaExcel linhasValidada = null;
          int controleLinhas = 0;
          boolean isConteudoValido = true;
          
          List<String> codUnidEntregas = new ArrayList<>();
          
          for (Row row : sheet) {
               if (controleLinhas > linhaCabecalho) {
                    if (isCelulasObrigatoriasVazio(row, header))
                         break;
                    
                    Map<Integer, String> mapeamento = mapeiaLinha(row, template.getQuantidadeCelulas());
                    
                    linhasValidada = isLinhaValida(mapeamento, row.getRowNum()+1, template, codUnidEntregas);
                    
                    salvarRespostaValidacao(arquivoUnidadeEntrega.getId(), linhasValidada);
                    
                    isConteudoValido = isConteudoValido ? linhasValidada.isValido() : isConteudoValido;
               }
               controleLinhas++;
          }

          return isConteudoValido;
     }
     
     private boolean isCelulasObrigatoriasVazio(final Row row, final List<RhHeaderDefinition> listHeader) {

          boolean linhaVazia = false;
          for (RhHeaderDefinition h : listHeader) {
               if (h.isObrigatorio()) {
                    if (Objects.isNull(row.getCell(h.getPosicaoCelula())) || StringUtils.isEmpty(row.getCell(h.getPosicaoCelula()).toString()))
                         linhaVazia = true;
                    else {
                         linhaVazia = false;
                         break;
                    }
               }
          }
          return linhaVazia;
     }
     
     private Map<Integer, String> mapeiaLinha(Row row, Integer quantidadeCelulas) {

          int celula = 1;
          final Map<Integer, String> mapeamento = new HashMap<>();
          for (final Cell cell : row) {
               if (celula > quantidadeCelulas)
                    break;

               mapeamento.put(celula, cell.toString());
               celula++;
          }
          return mapeamento;
     }
     
     private void salvarRespostaValidacao(Long idArquivoUnidadeEntrega,final LinhaUnidadeEntregaExcel linhasValidada) {
          
          ArquivoUnidadeEntregaDetalhe arquivosUnidadeEntregaDetalhe = alterarStatusPosValidacao(linhasValidada);
          arquivosUnidadeEntregaDetalhe = salvarLinhaDetalhada(idArquivoUnidadeEntrega, arquivosUnidadeEntregaDetalhe);
          if (!linhasValidada.isValido()) {
               arquivoUnidadeEntregaDetalheErroService.salvarErrosLinhaDetalhada(arquivosUnidadeEntregaDetalhe.getIdArquivoDetalhe(), linhasValidada);
          }
     }

     private ArquivoUnidadeEntregaDetalhe alterarStatusPosValidacao(final LinhaUnidadeEntregaExcel linhasValidada) {
          
          obterDominioStatusArquivoUnidadeEntregaDetalhe();
          StatusDescricao status = null;
          
          ArquivoUnidadeEntregaDetalhe arquivoUnidadeEntregaDetalhe = linhasValidada.getDetalhe();
          if (linhasValidada.isValido())
               status = mapStatusDescricao(dominioStatusUnidadeEntregaDetalhe, VALIDADO.name());
          else
               status = mapStatusDescricao(dominioStatusUnidadeEntregaDetalhe, INVALIDADO.name());
          
          arquivoUnidadeEntregaDetalhe.setStatus(status.getStatus());
          
          return arquivoUnidadeEntregaDetalhe;
     }

     private void obterDominioStatusArquivoUnidadeEntregaDetalhe() {
          if (Objects.isNull(dominioStatusUnidadeEntregaDetalhe))
               dominioStatusUnidadeEntregaDetalhe = this.statusCached.busca(TipoStatus.ARQUIVO_UNIDADE_ENTREGA_DETALHE.getNome());
     }

     private ArquivoUnidadeEntregaDetalhe salvarLinhaDetalhada(Long idArquivoUnidadeEntrega, ArquivoUnidadeEntregaDetalhe arquivoUnidadeEntrega) {

          arquivoUnidadeEntrega.setIdArquivoUE(idArquivoUnidadeEntrega);
          return arquivoUnidadeEntregaDetalheRepository.save(arquivoUnidadeEntrega);
     }

     private String limpaString(String str) {

          if (!Objects.isNull(str)) {
               str = str.replace("\u00A0", "").replace("\u2007", "").replace("\u202F", "").replace("\u0009", "").trim();
          }

          return str;
     }

     private LinhaUnidadeEntregaExcel isLinhaValida(final Map<Integer, String> conteudo, final Integer numeroLinhaExcel, final TemplatesExcel template, List<String> codUnidEntregas) {

          final LinhaUnidadeEntregaExcel linhasValidada = LinhaUnidadeEntregaExcel.builder().isValido(true).numeroLinhaExcel(numeroLinhaExcel).erros(new ArrayList<>()).build();
          ArquivoUnidadeEntregaDetalhe detalhe = new ArquivoUnidadeEntregaDetalhe();
          detalhe.setNumeroLinhaArquivo(numeroLinhaExcel);
          linhasValidada.setDetalhe(detalhe);

          if (conteudo.size() != template.getQuantidadeCelulas()) {
               linhasValidada.getErros().add("Quantidade de células preenchidas diferente do esperado.");
               linhasValidada.setValido(false);
               return linhasValidada;
          }

          try {
               detalhe.setCodigoUnidadeEntrega(ValidacaoTemplate.validacaoCodigoUnidadeEntrega(limpaString(conteudo.get(1)).toUpperCase(), codUnidEntregas));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }

          try {
               detalhe.setLogradouro(ValidacaoTemplate.validaLogradouro(limpaString(conteudo.get(2)).toUpperCase()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setEndereco(ValidacaoTemplate.validaEndereco(limpaString(conteudo.get(3)).toUpperCase()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setEnderecoNumero(ValidacaoTemplate.validaNumero(limpaString(conteudo.get(4)).toUpperCase()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setEnderecoComplemento(ValidacaoTemplate.validaComplemento(limpaString(conteudo.get(5)).toUpperCase()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setBairro(ValidacaoTemplate.validaBairro(limpaString(conteudo.get(6)).toUpperCase()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setCidade(ValidacaoTemplate.validaCidade(limpaString(conteudo.get(7)).toUpperCase()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setUf(ValidacaoTemplate.validaUF(limpaString(conteudo.get(8)).toUpperCase()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setCep(ValidacaoTemplate.validaCEP(limpaString(conteudo.get(9))));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setNomeResponsavel1(ValidacaoTemplate.validaNomeCompleto(conteudo.get(10)).toUpperCase());
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setTipoDocumentoResponsavel1(ValidacaoTemplate.validacaoTipoDocumento(limpaString(conteudo.get(11)).toUpperCase()));
          }catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setDocumentoResponsavel1(ValidacaoTemplate.validacaoCpfRg(limpaString(conteudo.get(12)),detalhe.getTipoDocumentoResponsavel1()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }

          try {
               detalhe.setTelefoneResponsavel1(ValidacaoTemplate.validacaoTelefone(limpaString(conteudo.get(13))));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setNomeResponsavel2(ValidacaoTemplate.validaNomeCompleto(conteudo.get(14)).toUpperCase());
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setTipoDocumentoResponsavel2(ValidacaoTemplate.validacaoTipoDocumento(limpaString(conteudo.get(15)).toUpperCase()));
          }catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          try {
               detalhe.setDocumentoResponsavel2(ValidacaoTemplate.validacaoCpfRg(limpaString(conteudo.get(16)),detalhe.getTipoDocumentoResponsavel2()));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }

          try {
               detalhe.setTelefoneResponsavel2(ValidacaoTemplate.validacaoTelefone(limpaString(conteudo.get(17))));
          } catch (ConteudoExcelException e) {
               linhasValidada.getErros().add(e.getMessage());
               linhasValidada.setValido(false);
          }
          
          

          return linhasValidada;
     }
     
     public Optional<List<ArquivoUnidadeEntregaDetalhe>> buscaListaDetalhesPorIdArquivo(long idArquivoUnidadeEntrega) {
          
          return arquivoUnidadeEntregaDetalheRepository.findByIdArquivoUE(idArquivoUnidadeEntrega);
     }
     
     public Long buscaQuantidadeRegistrosPor(Long idArquivoUnidadeEntrega, StatusArquivoUnidadeEntregaDetalhe statusDetalhe) {
          
          final StatusDescricao status = mapStatusDescricao(this.statusCached.busca(ARQUIVO_UNIDADE_ENTREGA_DETALHE.getNome()), statusDetalhe.name());
          
          return arquivoUnidadeEntregaDetalheRepository.countByIdArquivoUEAndStatus(idArquivoUnidadeEntrega, status.getStatus()).get();
     }

     public ArquivoUnidadeEntregaDetalhe alterarStatus(ArquivoUnidadeEntregaDetalhe arquivoUnidadeEntregaDetalhe, StatusArquivoUnidadeEntregaDetalhe status) {
          
          final StatusDescricao statusDescricao = mapStatusDescricao(this.statusCached.busca(ARQUIVO_UNIDADE_ENTREGA_DETALHE.getNome()), status.name());
          
          arquivoUnidadeEntregaDetalhe.setStatus(statusDescricao.getStatus());
          
          return arquivoUnidadeEntregaDetalheRepository.save(arquivoUnidadeEntregaDetalhe);
     }
 
}
