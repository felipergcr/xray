
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.enums.StatusUnidadesEntrega.ATIVO;
import static br.com.conductor.rhblueapi.enums.StatusUnidadesEntrega.INATIVO;
import static br.com.conductor.rhblueapi.enums.StatusUnidadesEntrega.PROCESSADO;
import static br.com.conductor.rhblueapi.enums.TipoStatus.UNIDADES_ENTREGA;
import static br.com.conductor.rhblueapi.service.utils.StatusUtils.mapStatusDescricao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoUnidadeEntregaDetalhe;
import br.com.conductor.rhblueapi.domain.StatusDescricao;
import br.com.conductor.rhblueapi.domain.UnidadeEntrega;
import br.com.conductor.rhblueapi.domain.request.UnidadeEmpresaRequest;
import br.com.conductor.rhblueapi.enums.StatusUnidadesEntrega;
import br.com.conductor.rhblueapi.repository.UnidadeEntregaRepository;
import br.com.conductor.rhblueapi.service.usecase.StatusCached;

@Service
public class UnidadeEntregaService {

     @Autowired
     private UnidadeEntregaRepository unidadeEntregaRepository;

     @Autowired
     private UsuarioPierService usuarioPierService;

     @Autowired
     private GrupoEmpresaService grupoEmpresaService;
     
     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;
     
     @Autowired
     private StatusCached statusCached;
     
     public void validarUnidadeEmpresaRequest(final UnidadeEmpresaRequest request) {

          usuarioPierService.autenticarUsuario(request.getIdUsuarioSessao());

          grupoEmpresaService.autenticarGrupoEmpresa(request.getIdGrupoEmpresa());

          permissoesUsuariosRhService.autenticarPermissaoGrupoAtiva(request.getIdUsuarioSessao(), request.getIdGrupoEmpresa());
     }

     public Page<UnidadeEntrega> listarUnidadeEntregaPaginado(final Pageable pageable, final UnidadeEntrega unidadeEntrega) {

          if (Objects.isNull(pageable)) {
               return new PageImpl<>(Collections.emptyList(), Pageable.unpaged(), 0);
          } else if (Objects.isNull(unidadeEntrega)) {
               return new PageImpl<>(Collections.emptyList(), pageable, 0);
          } else {
               ExampleMatcher matcher = ExampleMatcher.matching().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
               Example<UnidadeEntrega> example = Example.of(unidadeEntrega, matcher);
               Page<UnidadeEntrega> retorno = unidadeEntregaRepository.findAll(example, pageable);
               return retorno;
          }
     }
     
     private UnidadeEntrega converte(final ArquivoUnidadeEntregaDetalhe arquivoUnidadeEntregaDetalhe) {
          
          UnidadeEntrega unidadeEntrega = new UnidadeEntrega();
          
          BeanUtils.copyProperties(arquivoUnidadeEntregaDetalhe, unidadeEntrega);
          
          unidadeEntrega.setDataRegistro(LocalDateTime.now());

          return unidadeEntrega;
     }
     
     private UnidadeEntrega incluirGrupo(UnidadeEntrega unidadeEntrega,Long idGrupoEmpresa) {
          
          unidadeEntrega.setIdGrupoEmpresa(idGrupoEmpresa);
          
          return unidadeEntrega;
     }
     
     private UnidadeEntrega alterarStatus(UnidadeEntrega unidadeEntrega, StatusUnidadesEntrega status) {
          
          List<StatusDescricao> dominioStatusUnidadeEntrega = this.statusCached.busca(UNIDADES_ENTREGA.getNome());
          final StatusDescricao statusDescricao = mapStatusDescricao(dominioStatusUnidadeEntrega, status.name());
          
          unidadeEntrega.setStatus(statusDescricao.getStatus());
          unidadeEntrega.setDataStatus(LocalDateTime.now());
          
          return unidadeEntrega;
     }
     
     public void processoIncluirRegistro(final ArquivoUnidadeEntregaDetalhe arquivoUnidadeEntregaDetalhe, Long idGrupoEmpresa) {
          
          UnidadeEntrega unidadeEntrega = this.converte(arquivoUnidadeEntregaDetalhe);
          unidadeEntrega = this.incluirGrupo(unidadeEntrega, idGrupoEmpresa);

          Optional<UnidadeEntrega> optUnidadeEntregaAtiva = this.buscaUnidadeAtivaPorCodigo(unidadeEntrega.getCodigoUnidadeEntrega(), idGrupoEmpresa);
          
          if(!optUnidadeEntregaAtiva.isPresent() || 
                    !optUnidadeEntregaAtiva.get().equals(unidadeEntrega)) {
               
               unidadeEntrega = this.alterarStatus(unidadeEntrega, StatusUnidadesEntrega.PROCESSADO);
               
               unidadeEntregaRepository.save(unidadeEntrega);
          } 

     }
     
     public Optional<List<UnidadeEntrega>> buscaUnidadesPorGrupoEmpresaEStatus(Long idGrupoEmpresa, StatusUnidadesEntrega status) {
          
          List<StatusDescricao> dominioStatusUnidadeEntrega = this.statusCached.busca(UNIDADES_ENTREGA.getNome());
          final StatusDescricao statusDescricao = mapStatusDescricao(dominioStatusUnidadeEntrega, status.getDescricao());
                   
          return unidadeEntregaRepository.findByIdGrupoEmpresaAndStatus(idGrupoEmpresa, statusDescricao.getStatus());
          
     }
     
     private Optional<UnidadeEntrega> buscaUnidadeAtivaPorCodigo(String codigoUnidade, Long idGrupoEmpresa) {

          List<StatusDescricao> dominioStatusUnidadeEntrega = this.statusCached.busca(UNIDADES_ENTREGA.getNome());
          final StatusDescricao statusDescricao = mapStatusDescricao(dominioStatusUnidadeEntrega, ATIVO.getDescricao());

          return unidadeEntregaRepository.findTopByIdGrupoEmpresaAndCodigoUnidadeEntregaAndStatus(idGrupoEmpresa, codigoUnidade, statusDescricao.getStatus());

     }

     private List<UnidadeEntrega> alterarUnidadeEntregaDeCodigoExistente(UnidadeEntrega unidadeEntregaProcessada, UnidadeEntrega unidadeEntregaAtiva) {

          return Arrays.asList(alterarStatus(unidadeEntregaAtiva, INATIVO), alterarStatus(unidadeEntregaProcessada, ATIVO));
     }

     public void processoPersistenciaUnidades(Long idGrupoEmpresa) {

          Optional<List<UnidadeEntrega>> optUnidadesEntregaProcessadas = this.buscaUnidadesPorGrupoEmpresaEStatus(idGrupoEmpresa, PROCESSADO);

          List<UnidadeEntrega> unidadesEntregaAtualizado = new ArrayList<>();
          
          if (optUnidadesEntregaProcessadas.isPresent()) {

               optUnidadesEntregaProcessadas.get().stream().forEach(unidadeEntregaProcessada -> {

                    Optional<UnidadeEntrega> optUnidadeEntregaAtiva = buscaUnidadeAtivaPorCodigo(unidadeEntregaProcessada.getCodigoUnidadeEntrega(), idGrupoEmpresa);

                    if (optUnidadeEntregaAtiva.isPresent()) {
                         unidadesEntregaAtualizado.addAll(alterarUnidadeEntregaDeCodigoExistente(unidadeEntregaProcessada, optUnidadeEntregaAtiva.get()));
                    } else {
                         unidadesEntregaAtualizado.add(alterarStatus(unidadeEntregaProcessada, ATIVO));
                    }

               });

               unidadeEntregaRepository.saveAll(unidadesEntregaAtualizado);
          }

     }
     
     public  Optional<List<UnidadeEntrega>> buscarUnidadeEntregaAtivaPorGrupoEmpresa(final Long idGrupoEmpresa) {

          return buscaUnidadesPorGrupoEmpresaEStatus(idGrupoEmpresa, ATIVO);
     }

}
