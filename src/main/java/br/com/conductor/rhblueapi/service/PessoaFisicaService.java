
package br.com.conductor.rhblueapi.service;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.PESSOA_NAO_ENCONTRADA;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import br.com.conductor.rhblueapi.domain.PessoaEmpresa;
import br.com.conductor.rhblueapi.domain.PessoaFisicaCadastro;
import br.com.conductor.rhblueapi.domain.request.funcionario.FuncionarioAtualizacaoRequest;
import br.com.conductor.rhblueapi.domain.request.pessoafisica.PessoaFisicaRequest;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.repository.PessoaRepository;
import br.com.conductor.rhblueapi.util.AppConstantes;

@Service
public class PessoaFisicaService {

     @Autowired
     private PessoaRepository pessoaRepository;
     
     public Optional<PessoaEmpresa> buscaPessoaPorDocumento(String documento) {
          
          return pessoaRepository.findByDocumento(documento);
     }

     public PessoaEmpresa persisteEObtemPessoaFisica(Optional<PessoaEmpresa> optPessoaFisica, PessoaFisicaRequest pessoaFisicaRequest) {

          return optPessoaFisica.isPresent() ? this.atualizar(pessoaFisicaRequest, optPessoaFisica.get()) : this.cadastrar(pessoaFisicaRequest);
     }

     private PessoaEmpresa montaPessoa(PessoaFisicaRequest pessoaFisicaRequest) {

          PessoaEmpresa pessoa = PessoaEmpresa.builder().build();

          BeanUtils.copyProperties(pessoaFisicaRequest, pessoa);

          return pessoa;
     }

     private PessoaFisicaCadastro montaPessoaFisicaCadastro(PessoaEmpresa pessoaEmpresa) {

          PessoaFisicaCadastro cadastro = new PessoaFisicaCadastro();
          cadastro.setPessoaEmpresa(pessoaEmpresa);
          cadastro.setIdEmissor(AppConstantes.EMISSOR_PADRAO);

          return cadastro;
     }

     private PessoaEmpresa montaPessoaFisicaECadastro(PessoaFisicaRequest pessoaFisicaRequest) {

          PessoaEmpresa pessoaEmpresa = this.montaPessoa(pessoaFisicaRequest);

          PessoaFisicaCadastro cadastro = this.montaPessoaFisicaCadastro(pessoaEmpresa);

          pessoaEmpresa.setPessoaFisica(cadastro);

          return pessoaEmpresa;
     }

     private PessoaEmpresa cadastrar(PessoaFisicaRequest pessoaFisicaRequest) {

          PessoaEmpresa pessoa = this.montaPessoaFisicaECadastro(pessoaFisicaRequest);

          return pessoaRepository.save(pessoa);

     }

     private PessoaEmpresa atualizar(PessoaFisicaRequest pessoaFisicaRequest, PessoaEmpresa pessoaPersistida) {
          
          PessoaEmpresa pessoaFisica = PessoaEmpresa.builder().build();
          
          BeanUtils.copyProperties(pessoaFisicaRequest, pessoaFisica);
          
          if(this.isEqualsBusinessRules(pessoaFisica, pessoaPersistida))
               return pessoaPersistida;
          
          BeanUtils.copyProperties(pessoaFisicaRequest, pessoaPersistida, "idPessoa", "numeroIdentidade");

          return pessoaRepository.save(pessoaPersistida);
     }

     public PessoaFisicaRequest converte(ArquivosCargasDetalhes detalhe) {

          return PessoaFisicaRequest.builder()
                    .idEmissor(AppConstantes.EMISSOR_PADRAO)
                    .nome(detalhe.getNome())
                    .documento(detalhe.getCpf())
                    .dataNascimento(LocalDate.parse(detalhe.getDataNascimento(), DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                    .numeroIdentidade(detalhe.getCpf())
                    .build();
     }
     
     private boolean isEqualsBusinessRules(PessoaEmpresa source, PessoaEmpresa target) {

          if (StringUtils.equals(source.getNome(), target.getNome()) && source.getDataNascimento().isEqual(target.getDataNascimento()))
               return true;

          return false;
     }
     
     private PessoaEmpresa editarAtributoNomeEDataNascimento(PessoaEmpresa pessoaAtualizada, FuncionarioAtualizacaoRequest funcionarioAtualizacao) {

          pessoaAtualizada.setNome(StringUtils.equals(pessoaAtualizada.getNome(), funcionarioAtualizacao.getNome()) ? pessoaAtualizada.getNome() :  funcionarioAtualizacao.getNome());
          pessoaAtualizada.setDataNascimento(pessoaAtualizada.getDataNascimento().isEqual(funcionarioAtualizacao.getDataNascimento()) ? pessoaAtualizada.getDataNascimento() :  funcionarioAtualizacao.getDataNascimento());
          
          return pessoaAtualizada;
     }
     
     public void atualizar(Long idPessoa, FuncionarioAtualizacaoRequest funcionarioAtualizacao) {
          
          Optional<PessoaEmpresa> optPessoaEmpresa = pessoaRepository.findById(idPessoa);
          
          NotFoundCdt.checkThrow(!optPessoaEmpresa.isPresent(), PESSOA_NAO_ENCONTRADA);
          
          PessoaEmpresa pessoaAtualizada = PessoaEmpresa.builder().build();
          
          BeanUtils.copyProperties(optPessoaEmpresa.get(), pessoaAtualizada);
          
          pessoaAtualizada = this.editarAtributoNomeEDataNascimento(pessoaAtualizada, funcionarioAtualizacao);
          
          if(!pessoaAtualizada.equals(optPessoaEmpresa.get()))
               pessoaRepository.save(pessoaAtualizada);
     }

}
