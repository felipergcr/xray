
package br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.strategy.PosPago;
import br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.strategy.PrePago;
import br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.strategy.TipoContratoStrategy;

@Component
public class TipoContratoFactory {
     
     @Autowired
     private PosPago posPago;
     
     @Autowired
     private PrePago prePago;

     public TipoContratoStrategy newStrategy(TipoContratoEnum tipoContrato) {

          switch (tipoContrato) {
               case POS:
                    return posPago;
               default:
                    return prePago;
          }

     }

}
