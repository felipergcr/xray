package br.com.conductor.rhblueapi.service.template.method.hierarquia.acesso;

import java.io.IOException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.persist.PermissoesRhPersist;
import br.com.conductor.rhblueapi.domain.persist.UsuarioNivelPermissaoRhPersist;
import br.com.conductor.rhblueapi.domain.persist.UsuarioPersist;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.request.UsuarioRequestBlue;
import br.com.conductor.rhblueapi.domain.response.PermissoesRhResponse;
import br.com.conductor.rhblueapi.enums.Plataforma;
import br.com.conductor.rhblueapi.service.PermissoesUsuariosRhService;
import br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService;
import br.com.conductor.rhblueapi.service.UsuarioPierService;
import br.com.conductor.rhblueapi.util.GenericConvert;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HierarquiaAcessoNova extends HierarquiaAcessoAbstract {
     
     @Autowired
     private UsuarioPierService usuarioPierService;
     
     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;
     
     @Autowired
     private UsuarioNivelPermissaoRhService usuarioNivelPermissaoRhService;
     
     @Override
     public void criarHierarquiaMethod(UsuarioPersist usuarioPersist) {
          
          UsuarioRequestBlue usuarioRequest = GenericConvert.convertModelMapper(usuarioPersist, UsuarioRequestBlue.class);
          usuarioRequest.setPlataforma(Plataforma.RH);

          UsuarioResponse usuarioResponse = criaUsuario(usuarioRequest);

          PermissoesRhPersist permissaoPersist = buildPermissaoPersist(usuarioPersist, usuarioResponse.getId());

          PermissoesRhResponse permissaoResponse = permissoesUsuariosRhService.salvar(permissaoPersist, true);

          UsuarioNivelPermissaoRhPersist usuarioNivelPermissaoRhPersist = buildNivelPermissaoPersist(usuarioPersist);

          usuarioNivelPermissaoRhService.salvarNivelPermissaoUsuario(usuarioNivelPermissaoRhPersist, permissaoResponse.getId());

     }

     private UsuarioNivelPermissaoRhPersist buildNivelPermissaoPersist(UsuarioPersist usuarioPersist) {

          return UsuarioNivelPermissaoRhPersist.builder()
                    .idNivelAcesso(usuarioPersist.getIdsNivelAcesso())
                    .idUsuarioRegistro(usuarioPersist.getIdUsuarioLogado())
                    .nivelAcesso(usuarioPersist.getNivelAcesso())
                    .build();
     }

     private PermissoesRhPersist buildPermissaoPersist(UsuarioPersist usuarioPersist, Long idUsuario) {

          return PermissoesRhPersist.builder()
                    .idGrupoEmpresa(usuarioPersist.getIdGrupoEmpresa())
                    .idUsuario(idUsuario)
                    .idUsuarioRegistro(usuarioPersist.getIdUsuarioLogado())
                    .build();
     }

     private UsuarioResponse criaUsuario(UsuarioRequestBlue usuarioRequest) {

          UsuarioResponse usuarioResponse = null;

          try {
               usuarioResponse = this.usuarioPierService.criaUsuario(usuarioRequest);
               
               if(Objects.isNull(usuarioResponse)) {
                    throw new InternalError();
               }

          } catch (IOException e) {
               log.warn("Falha ao criar usuário no Pier" + e.getMessage());
          }
          return usuarioResponse;
     }
     
}
