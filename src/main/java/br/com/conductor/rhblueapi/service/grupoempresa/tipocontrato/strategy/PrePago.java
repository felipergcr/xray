
package br.com.conductor.rhblueapi.service.grupoempresa.tipocontrato.strategy;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PrePago implements TipoContratoStrategy {
     
     @Value("${app.regras.pedido.quantidade-dias-minimo-upload.pre}")
     private Integer qtdDiasMinimoUploadPedido;

     @Override
     public Integer aplicaRegraPrazoPagamento(Integer prazoPagamentoRequest) {
          return 0;
     }

     @Override
     public void validaDataVigenciaLimite(LocalDate data) {
          
     }

     @Override
     public void validaPrazoPagamento(Integer prazoPagamento) {
          
     }

     @Override
     public Integer obtemQuantidadeDiasMinimoUploadPedido() {
          return this.qtdDiasMinimoUploadPedido;
     }

}
