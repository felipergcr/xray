
package br.com.conductor.rhblueapi.service.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.email.GenericoEmail;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;

@Service
public class EmailFinalizaPedidoService extends EmailUtilService<ArquivoCargas, StatusArquivoEnum>{
     
     @Value("${app.pier.notificacao.parametros.header.img}")
     private String header_img;

     @Value("${app.pier.notificacao.parametros.footer.img}")
     private String footer_img;
     
     @Value("${app.pier.notificacao.parametros.completo.id}")
     private Long idTemplateNotificacaoCompleto;
     
     @Value("${app.pier.notificacao.parametros.erros.id}")
     private Long idTemplateNotificacaoErros;
     
     @Value("${app.pier.notificacao.parametros.incompleto.id}")
     private Long idTemplateNotificacaoIncompleto;
     
     @Value("${app.pier.notificacao.parametros.url.detalhes}")
     private String pedidoDetalhadoLink;

     @Value("${app.pier.notificacao.parametros.url.erros}")
     private String pedidoDetalhadoErrosLink;

     @Override
     public void enviar(ArquivoCargas arquivo, StatusArquivoEnum status) {

          GenericoEmail<StatusArquivoEnum> genericoEmail = new GenericoEmail<StatusArquivoEnum>();
          
          genericoEmail.setIdUsuario(arquivo.getIdUsuario());
          genericoEmail.setIdObjeto(arquivo.getId());
          genericoEmail.setDataHora(arquivo.getDataStatus());
          genericoEmail.setStatus(status);
          
          super.enviar(genericoEmail);
          
     }

     @Override
     Long getContentTemplate(StatusArquivoEnum status) {

          Long idTemplateNotificao = null;

          switch (status) {
               case INVALIDADO:
                    idTemplateNotificao = idTemplateNotificacaoErros;
                    break;
               case PROCESSAMENTO_CONCLUIDO:
                    idTemplateNotificao = idTemplateNotificacaoCompleto;
                    break;
               default:
                    idTemplateNotificao = idTemplateNotificacaoIncompleto;
                    break;
          }
          return idTemplateNotificao;
     }
     
     
     @Override
     String getContentUrl(Long idPath, StatusArquivoEnum status) {

          String url = null;
          switch(status) {
               case PROCESSAMENTO_CONCLUIDO:
                    url = pedidoDetalhadoLink;
               break;
               default:
                    url = pedidoDetalhadoErrosLink;
               break;
          }
          
          return String.format(url, idPath);
     }
     
     @Override
     Map<String, Object> popularParametros(GenericoEmail<StatusArquivoEnum> genericoEmail, String nomeDestinatario, String url) {

          Map<String, Object> parametros = new HashMap<>();
          parametros.put("header_img", header_img);
          parametros.put("user_name", nomeDestinatario);
          parametros.put("order_number", genericoEmail.getIdObjeto());
          parametros.put("date", genericoEmail.getDate());
          parametros.put("time", genericoEmail.getHour());
          parametros.put("new_password_link", url);
          parametros.put("footer_img", footer_img);

          return parametros;
     }

}
