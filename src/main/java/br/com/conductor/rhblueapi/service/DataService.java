package br.com.conductor.rhblueapi.service;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.blue.FeriadoResponse;
import br.com.conductor.rhblueapi.repository.blue.FeriadoBlueRestRepository;

@Service
public class DataService {
     
     @Autowired
     private FeriadoBlueRestRepository feriadoBlueRestRepository;
     
     public List<LocalDate> diasUteis(final LocalDate dataInicio, final LocalDate dataFinal) {

          final long qtdDiasIntervalo = ChronoUnit.DAYS.between(dataInicio, dataFinal);

          if (qtdDiasIntervalo <= 0) {
               return new ArrayList<>();
          }

          final List<FeriadoResponse> feriados = this.feriadoBlueRestRepository.listarFeriados();

          return Stream.iterate(dataInicio.plusDays(1), date -> date.plusDays(1))
                    .limit(qtdDiasIntervalo)
                    .filter(data -> data.getDayOfWeek().getValue() < SATURDAY.getValue())
                    .filter(data -> (feriados.stream()
                              .filter(feriado -> feriado.getData().equals(data))
                              .count()) < 1)
                    .collect(Collectors.toList());
     }
     
     public LocalDate obterDiaUtilMenorQue(final LocalDate data, final int qtdDiasUteis) {
          
          final List<FeriadoResponse> feriados = this.feriadoBlueRestRepository.listarFeriados();
          
          if(qtdDiasUteis <= 0) {
               boolean isDiaUtil = false;
               while(Boolean.FALSE.equals(isDiaUtil)) {
                    if(this.isDiaUtil(data, feriados)) {
                         isDiaUtil = true;
                    } else {
                         data.minusDays(1);
                    }
               }
          } else {
               int qtdDiasUteisAux = 0;
               while(qtdDiasUteisAux < qtdDiasUteis) {
                    data.minusDays(1);
                    if(this.isDiaUtil(data, feriados)) {
                         qtdDiasUteisAux++;
                    }
               }
          }
          
          return data;
     }
     
     private boolean isDiaUtil(final LocalDate data, List<FeriadoResponse> feriados) {
          
          return Boolean.FALSE.equals(isFinalDeSemana(data)) && Boolean.FALSE.equals(isFeriado(data, feriados));
     }
     
     private static boolean isFinalDeSemana(final LocalDate data) {
          
          DayOfWeek dayOfWeek = data.getDayOfWeek();
          
          return dayOfWeek == SATURDAY || dayOfWeek == SUNDAY;
     }
     
     private static boolean isFeriado(final LocalDate data, List<FeriadoResponse> feriados) {
          
          return feriados.stream().anyMatch(f -> f.getData().equals(data));
     }
     
}
