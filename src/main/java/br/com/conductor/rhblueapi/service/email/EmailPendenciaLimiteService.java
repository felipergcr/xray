
package br.com.conductor.rhblueapi.service.email;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.email.GenericoEmail;
import br.com.conductor.rhblueapi.enums.StatusPagamentoEnum;

@Service
public class EmailPendenciaLimiteService extends EmailUtilService<ArquivoCargas, StatusPagamentoEnum> {
     
     @Value("${app.pier.notificacao.parametros.header.img}")
     private String header_img;

     @Value("${app.pier.notificacao.parametros.footer.img}")
     private String footer_img;
     
     @Value("${app.pier.notificacao.parametros.limite-credito.id}")
     private Long idTemplateLimiteCredito;
     
     @Value("${app.pier.notificacao.parametros.url.detalhes}")
     private String pedidoDetalhadoLink;

     @Override
     public void enviar(ArquivoCargas arquivo, StatusPagamentoEnum status) {
          
          GenericoEmail<StatusPagamentoEnum> genericoEmail = new GenericoEmail<StatusPagamentoEnum>();
          
          genericoEmail.setIdUsuario(arquivo.getIdUsuario());
          genericoEmail.setIdObjeto(arquivo.getId());
          genericoEmail.setDataHora(arquivo.getDataStatus());
          genericoEmail.setStatus(status);
          
          super.enviar(genericoEmail);
     }

     @Override
     Long getContentTemplate(StatusPagamentoEnum status) {

          return idTemplateLimiteCredito;
     }
     
     @Override
     String getContentUrl(Long idPath, StatusPagamentoEnum status) {

          return String.format(pedidoDetalhadoLink, idPath);
     }

     @Override
     Map<String, Object> popularParametros(GenericoEmail<StatusPagamentoEnum> genericoEmail, String nomeDestinatario, String url) {
          
          Map<String, Object> parametros = new HashMap<>();
          parametros.put("header", header_img);
          parametros.put("username", nomeDestinatario);
          parametros.put("date", genericoEmail.getDate());
          parametros.put("time", genericoEmail.getHour());
          parametros.put("order_number", genericoEmail.getIdObjeto());
          parametros.put("orders_link", url);
          parametros.put("footer", footer_img);

          return parametros;
     }

}
