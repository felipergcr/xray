
package br.com.conductor.rhblueapi.service.email;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;

import br.com.conductor.rhblueapi.domain.NotificacaoEmail;
import br.com.conductor.rhblueapi.domain.email.GenericoEmail;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.repository.pier.NotificacaoEmailPierRest;
import br.com.conductor.rhblueapi.repository.pier.UsuarioPierRestRepository;

public abstract class EmailUtilService <T, S> {
     
     @Autowired
     private UsuarioPierRestRepository usuarioPierRestRepository;
     
     @Autowired
     private NotificacaoEmailPierRest notificacaoEmailPierRest;
     
     @Async
     public abstract void enviar(T objetoT, S objetoS);
     
     void enviar(GenericoEmail<S> genericoEmail) {
          
          UsuarioResponse usuario = usuarioPierRestRepository.consultarUsuario(genericoEmail.getIdUsuario());
          
          Long idTemplate = this.getContentTemplate(genericoEmail.getStatus());
          
          String url = this.getContentUrl(genericoEmail.getIdObjeto(), genericoEmail.getStatus());
          
          Map<String, Object> parametros = this.popularParametros(genericoEmail, usuario.getNome(), url);
          
          NotificacaoEmail notificacaoEmail = this.montarConteudo(usuario.getEmail(), idTemplate, parametros);
          
          notificacaoEmailPierRest.enviaEmail(notificacaoEmail);
     }
     
     abstract Long getContentTemplate(S objetoS);
     
     abstract String getContentUrl(Long idPath, S objetoS);
     
     abstract Map<String, Object> popularParametros(GenericoEmail<S> genericoEmail, String nomeDestinatario, String url);
     
     NotificacaoEmail montarConteudo(String emailDestinatario, Long idTemplate, Map<String, Object> parametros) {
          
          return NotificacaoEmail.builder().destinatarios(Arrays.asList(emailDestinatario)).idTemplateNotificacao(idTemplate).parametrosConteudo(parametros).build();
     }
     
}
