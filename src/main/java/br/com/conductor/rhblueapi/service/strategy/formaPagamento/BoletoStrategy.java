
package br.com.conductor.rhblueapi.service.strategy.formaPagamento;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.RegistroBoletoCustom;
import br.com.conductor.rhblueapi.domain.request.registroBoleto.RegistroBoletoPagadorRequest;
import br.com.conductor.rhblueapi.domain.request.registroBoleto.RegistroBoletoRequest;
import br.com.conductor.rhblueapi.domain.request.registroBoleto.RegistroBoletoTituloRequest;
import br.com.conductor.rhblueapi.enums.TipoPagamentoPedidoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.repository.RegistroBoletoCustomRepositoryImpl;
import br.com.conductor.rhblueapi.service.DataService;
import br.com.twsoftware.alfred.object.Objeto;

@Service
public class BoletoStrategy extends FormaPagamentoStrategy {

     @Autowired
     private RegistroBoletoCustomRepositoryImpl registroBoletoCustomRepositoryImpl;
     
     @Autowired
     private DataService dataService;

     private static final String TIPO_DOCUMENTO_DEFAULT = "01";
     
     private static final String ESPECIE_DOCUMENTO_DEFAULT = "99";
     
     private static final int QTD_DIAS_VENCIMENTO_BOLETO_PRE_PAGO_CENTRALIZADO = 3;
     
     @Override
     public void registrarPendenciaFinanceira(final ArquivoCargas arquivoCargas) {

          List<RegistroBoletoCustom> listaRegistroBoletoCustom = registroBoletoCustomRepositoryImpl.listarPorPedido(arquivoCargas.getId());

          for(RegistroBoletoCustom registroBoletoCustom : listaRegistroBoletoCustom) {
               RegistroBoletoRequest registroBoletoRequest = converteRegistroBoletoRequest(registroBoletoCustom, arquivoCargas);
               
               //TODO ENVIA PARA A FILA
          }

     }

     private RegistroBoletoRequest converteRegistroBoletoRequest(final RegistroBoletoCustom registroBoletoCustom, final ArquivoCargas arquivoCargas) {

          RegistroBoletoPagadorRequest pagadorRequest = RegistroBoletoPagadorRequest.builder()
                    .bairro(registroBoletoCustom.getPagadorBairro())
                    .cep(registroBoletoCustom.getPagadorCep())
                    .cidade(registroBoletoCustom.getPagadorCidade())
                    .endereco(obterEndereco(registroBoletoCustom))
                    .nome(registroBoletoCustom.getPagadorNome())
                    .numeroDocumento(registroBoletoCustom.getPagadorCnpj())
                    .tipoDocumento(TIPO_DOCUMENTO_DEFAULT)
                    .uf(registroBoletoCustom.getPagadorUf())
                    .build();
          
          TipoPagamentoPedidoEnum tipoPagamento = TipoPagamentoPedidoEnum.getByNome(arquivoCargas.getTipoPagamentoPedido());
          TipoFaturamentoEnum tipoFaturamento = TipoFaturamentoEnum.equalsTipo(arquivoCargas.getFlagFaturamentoCentralizado() ? "1" : "0");
          LocalDate dataVencimento = calculoDataVencimento(registroBoletoCustom.getDataAgendamento(), tipoPagamento, tipoFaturamento);
          
          RegistroBoletoTituloRequest tituloRequest = RegistroBoletoTituloRequest.builder()
                    .especieDocumento(ESPECIE_DOCUMENTO_DEFAULT)
                    .dataEmissao(LocalDate.now())
                    .dataVencimento(dataVencimento)
                    .nossoNumero(registroBoletoCustom.getTituloNossoNumero())
                    .valorNominal(registroBoletoCustom.getValorBoleto())
                    .build();

          RegistroBoletoRequest registroBoletoRequest = RegistroBoletoRequest.builder()
                    .codigoExterno(registroBoletoCustom.getIdBoleto())
                    .pagador(pagadorRequest)
                    .titulo(tituloRequest)
                    .build();

          return registroBoletoRequest;
     }

     private String obterEndereco(final RegistroBoletoCustom registroBoletoCustom) {

          String endereco = new StringBuilder(registroBoletoCustom.getPagadorNomeLogradouro().trim())
                    .append(" ")
                    .append(registroBoletoCustom.getPagadorNumeroEndereco().trim())
                    .toString();

          String complemento = Objeto.isBlank(registroBoletoCustom.getPagadorComplementoEndereco().trim()) ? "" : ", " + registroBoletoCustom.getPagadorComplementoEndereco().trim();

          return new StringBuilder(endereco)
                    .append(complemento)
                    .toString();
     }
     
     private LocalDate calculoDataVencimento(final LocalDate dataCredito, final TipoPagamentoPedidoEnum tipoPagamento, final TipoFaturamentoEnum tipoFaturamento) {
          
          LocalDate dataVencimento;
          
          if(tipoPagamento.equals(TipoPagamentoPedidoEnum.PRE) && tipoFaturamento.equals(TipoFaturamentoEnum.CENTRALIZADO)) {
               dataVencimento = dataService.obterDiaUtilMenorQue(dataCredito, QTD_DIAS_VENCIMENTO_BOLETO_PRE_PAGO_CENTRALIZADO);
          } else {
               dataVencimento = LocalDate.now();
          }
          
          return dataVencimento;
     }

}
