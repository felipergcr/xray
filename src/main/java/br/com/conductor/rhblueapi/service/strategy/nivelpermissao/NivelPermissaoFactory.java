
package br.com.conductor.rhblueapi.service.strategy.nivelpermissao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;

@Component
public class NivelPermissaoFactory {

     @Autowired
     private GrupoEmpresaStrategy grupoEmpresaStrategy;

     @Autowired
     private SubgrupoEmpresaStrategy subgrupoEmpresaStrategy;

     @Autowired
     private EmpresaStrategy empresaStrategy;

     public GerenciarNivelPermissaoStrategy criar(NivelPermissaoEnum nivelPermissao) {

          switch (nivelPermissao) {

               case GRUPO:
                    return grupoEmpresaStrategy;

               case SUBGRUPO:
                    return subgrupoEmpresaStrategy;

               default:
                    return empresaStrategy;

          }
     }

}
