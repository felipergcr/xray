
package br.com.conductor.rhblueapi.service.acessos;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.USUARIO_SESSAO_SEM_PERMISSAO;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.service.GrupoEmpresaService;
import br.com.conductor.rhblueapi.service.PermissoesUsuariosRhService;
import br.com.conductor.rhblueapi.service.UsuarioPierService;
import br.com.conductor.rhblueapi.service.UsuarioService;

@Service
public class GerenciadorAcessoService {

     @Autowired
     private UsuarioPierService usuarioPierService;

     @Autowired
     private GrupoEmpresaService grupoEmpresaService;

     @Autowired
     private PermissoesUsuariosRhService permissoesUsuariosRhService;
     
     @Autowired
     private UsuarioService usuarioService;
     
     public void validaIdentificacaoUsuario(IdentificacaoRequest identificacaoRequest) {
          
          usuarioPierService.autenticarUsuario(identificacaoRequest.getIdUsuario());

          grupoEmpresaService.autenticarGrupoEmpresa(identificacaoRequest.getIdGrupoEmpresa());
          
     }
     
     public Long validaIdentificaoEObtemIdPermissaoUsuarioLogado(IdentificacaoRequest identificacaoRequest) {
          
          this.validaIdentificacaoUsuario(identificacaoRequest);

          return permissoesUsuariosRhService.autenticarPermissaoGrupoAtiva(identificacaoRequest.getIdUsuario(), identificacaoRequest.getIdGrupoEmpresa()).get().getId();
     }

     public List<Integer> validaIdentificacaoEObtemIdsEmpresas(IdentificacaoRequest identificacaoRequest) {

          Long idPermissao = this.validaIdentificaoEObtemIdPermissaoUsuarioLogado(identificacaoRequest);

          List<Integer> idsEmpresas = permissoesUsuariosRhService.buscaIdsEmpresasPorPermissaoUsuario(idPermissao);

          ForbiddenCdt.checkThrow(isEmpty(idsEmpresas), USUARIO_SESSAO_SEM_PERMISSAO);

          return idsEmpresas;
     }

     public Usuario validaPorCpfEGrupoEmpresa(final String cpf, final Long idGrupoEmpresa) {

          Usuario usuario = usuarioService.autenticarUsuarioPorCpf(cpf);

          grupoEmpresaService.autenticarGrupoEmpresa(idGrupoEmpresa);

          try {
               permissoesUsuariosRhService.autenticarPermissaoGrupoAtiva(usuario.getId(), idGrupoEmpresa);
          } catch (ForbiddenCdt e) {
               ExceptionsMessagesCdtEnum.CPF_SEM_PERMISSAO_AO_GRUPO.raise();
          }

          return usuario;
     }

}
