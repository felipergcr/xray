
package br.com.conductor.rhblueapi.service.funcionario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.rhblueapi.domain.empresa.EmpresaMinimo;
import br.com.conductor.rhblueapi.domain.funcionario.FuncionarioMinimoResponse;
import br.com.conductor.rhblueapi.domain.funcionario.FuncionarioProcessamentoStep;
import br.com.conductor.rhblueapi.domain.pedido.MensagemGerarCarga;
import br.com.conductor.rhblueapi.domain.pedido.MensagemItemPedido;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.CargasPublish;
import br.com.conductor.rhblueapi.service.EmpresaService;

@Service
public class GerenciadorFuncionario {
     
     @Autowired
     private EmpresaService empresaService;
     
     @Autowired
     private FuncionarioService funcionarioService;
     
     @Autowired
     private CargasPublish cargasPublish; 
     
     public void gerenciaFuncionario(final MensagemItemPedido mensagemItemPedido) {
          
          EmpresaMinimo empresaMinimoResponse = empresaService.obtemEmpresaMinimo(mensagemItemPedido.getDetalhe().getCnpj());
          
          FuncionarioProcessamentoStep funcionarioProcessamentoStep = funcionarioService.popularSteps(mensagemItemPedido.getDetalhe(), empresaMinimoResponse);
          
          FuncionarioMinimoResponse funcionarioMinimoResponse = funcionarioService.processoGerarPessoaFisica(funcionarioProcessamentoStep, mensagemItemPedido.getDetalhe(), empresaMinimoResponse);
          
          cargasPublish.enviarDetalheParaGerarCarga(MensagemGerarCarga.builder()
                    .mensagemItemPedido(mensagemItemPedido)
                    .funcionarioMinimoResponse(funcionarioMinimoResponse)
                    .build());
     }
     
}
