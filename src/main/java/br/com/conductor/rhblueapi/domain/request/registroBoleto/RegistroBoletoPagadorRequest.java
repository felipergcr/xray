
package br.com.conductor.rhblueapi.domain.request.registroBoleto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegistroBoletoPagadorRequest implements Serializable {

     private static final long serialVersionUID = 5047492120330464838L;

     private String bairro;

     private String cep;

     private String cidade;

     private String endereco;

     private String nome;

     private String numeroDocumento;

     private String tipoDocumento;

     private String uf;

}
