
package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class UsuarioResumidoResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do usuário", example = "1", position = 1)
     private Long id;

     @ApiModelProperty(value = "cpf", notes = "CPF do usuário", example = "12345678910", position = 2)
     private String cpf;

     @ApiModelProperty(value = "email", notes = "Email do usuário", example = "email@conductor.com.br", position = 3)
     private String email;

     @ApiModelProperty(value = "nome", notes = "Nome do usuário", example = "Maria da Paz", position = 4)
     private String nome;

     @ApiModelProperty(notes = "Data de Nascimento do usuário", example = "1", position = 5)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     private LocalDate dataNascimento;

}
