package br.com.conductor.rhblueapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PARAMETROS")
public class Parametro {
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_PARAMETRO")
     private Long idParametro;
     
     @Column(name = "TIPOPARAMETRO")
     private String tipoParametro;
     
     @Column(name = "CODIGO")
     private String codigo;
     
     @Column(name ="DESCRICAO")
     private String descricao;
     
     @Column(name = "TIPOVALOR")
     private String tipoValor;
     
     @Column(name = "VALORDEFAULT")
     private String valorDefault;
     
}
