package br.com.conductor.rhblueapi.domain.exception;

import static java.util.Arrays.asList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

import org.apache.commons.lang3.StringUtils;

import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.ConflictCdt;
import br.com.conductor.rhblueapi.exception.ExceptionCdt;
import br.com.conductor.rhblueapi.exception.ForbiddenCdt;
import br.com.conductor.rhblueapi.exception.NoContentCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.exception.PreconditionCustom;
import br.com.conductor.rhblueapi.exception.UnauthorizedCdt;
import br.com.conductor.rhblueapi.exception.UnprocessableEntityCdt;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public enum ExceptionsMessagesCdtEnum {

     GLOBAL_ERRO_SERVIDOR(INTERNAL_SERVER_ERROR.value(), "Erro interno de servidor.", ExceptionCdt.class),
     GLOBAL_RECURSO_NAO_ENCONTRADO(NOT_FOUND.value(), "Recurso não encontrado.", NotFoundCdt.class),
     GLOBAL_RECURSO_EXISTENTE(CONFLICT.value(), "Recurso já existe.", ConflictCdt.class),
     GLOBAL_PRECONDICAO_NAO_ATENDIDA(PRECONDITION_FAILED.value(), "Pré-condição não atendida.", ExceptionCdt.class),
     GLOBAL_INVALIDO(UNAUTHORIZED.value(), "Recurso não autorizado.",UnauthorizedCdt.class),  
     GLOBAL_ERRO(BAD_REQUEST.value(), "Recurso invalido.", BadRequestCdt.class),
     PLANILHA_VAZIA(BAD_REQUEST.value(),"Planilha vazia." ,BadRequestCdt.class),
     PLANILHA_EXTENSAO(BAD_REQUEST.value(),"Extensão do arquivo inválido." ,BadRequestCdt.class),
     PLANILHA_INVALIDA(BAD_REQUEST.value(),"Layout inválido." ,BadRequestCdt.class),
     USUARIO_INEXISTENTE(BAD_REQUEST.value(),"Usuário não existe." ,BadRequestCdt.class), 
     USUARIO_NAO_ENCONTRADO(NO_CONTENT.value(),"Usuário não encontrado.", NoContentCdt.class),
     USUARIO_NAO_LOCALIZADO(NOT_FOUND.value(),"Usuário não encontrado", NotFoundCdt.class),
     NAO_HA_PEDIDOS(NO_CONTENT.value(), "Não existem pedidos.", NoContentCdt.class),
     USUARIO_SESSAO_SEM_PERMISSAO(FORBIDDEN.value(), "Usuário da sessão não possui vínculo para realizar essa operação", ForbiddenCdt.class),
     PEDIDO_NAO_ENCONTRADO(BAD_REQUEST.value(),"Pedido não encontrado." ,BadRequestCdt.class),
     GRUPO_EMPRESA_NAO_ENCONTRADO(BAD_REQUEST.value(),"Grupo empresa não encontrado." ,BadRequestCdt.class),
     GRUPO_EMPRESA_NAO_LOCALIZADO(NOT_FOUND.value(),"Grupo empresa não localizado." ,NotFoundCdt.class),
     NOTA_RPS_NAO_ENCONTRADO(BAD_REQUEST.value(),"Nota RPS não encontrado." ,BadRequestCdt.class),
     ERRO_AO_GERAR_ARQUIVO(INTERNAL_SERVER_ERROR.value(),"Erro ao gerar arquivo." ,ExceptionCdt.class),
     CARGA_CONTROLE_FINANCEIRO_NAO_ENCONTRADO(NOT_FOUND.value(), "Carga controle financeiro não encontrado.", NotFoundCdt.class),
     CARGA_NAO_ENCONTRADA(NOT_FOUND.value(),"Carga não encontrada." , NotFoundCdt.class),
     NAO_HA_NOTAS(NO_CONTENT.value(),"Não existe notas." , NoContentCdt.class),
     PEDIDO_NAO_VINCULADO_AO_USUARIO(NO_CONTENT.value(),"Pedido não vinculado ao usuário." ,BadRequestCdt.class),
     PEDIDO_INEXISTENTE(NO_CONTENT.value(),"Pedido não existe." ,NoContentCdt.class),
     ARQUIVO_NAO_ENCONTRADO(NOT_FOUND.value(),"Pedido não encontrado.", NotFoundCdt.class),
     STATUS_ARQUIVO_NAO_PERMITE_CANCELAMENTO(PRECONDITION_FAILED.value(),"Status do pedido não permite cancelamento.", PreconditionCustom.class),
     DATA_PROCESSAMENTO_ARQUIVO_NAO_PERMITE_CANCELAMENTO(PRECONDITION_FAILED.value(),"Data de processamento do pedido não permite cancelamento.", PreconditionCustom.class),
     FALHA_AO_CANCELAR_PEDIDO(BAD_REQUEST.value(),"Ocorreu uma falha ao processar o cancelamento do pedido.", BadRequestCdt.class),
     GRUPO_EMPRESA_INEXISTENTE(NO_CONTENT.value(),"Grupo empresa não encontrado." ,NoContentCdt.class),
     USUARIO_SEM_PERMISSAO_PEDIDO(FORBIDDEN.value(),"Usuário não possui permissão para visualizar detalhes deste pedido.", ForbiddenCdt.class),
     NAO_HA_DETALHES_PEDIDO(NO_CONTENT.value(), "Não existem detalhes para o pedido informado.", NoContentCdt.class),
     NAO_HA_DETALHES_CNPJ(NO_CONTENT.value(), "Não existem detalhes para o CNPJ informado.", NoContentCdt.class),
     CARGA_BENEFICIO_INEXISTENTE(NO_CONTENT.value(), "Carga beneficio não encontrado.", NoContentCdt.class),
     EMPRESA_INEXISTENTE(NO_CONTENT.value(), "Empresa não encontrada", NoContentCdt.class),
     PEDIDO_STATUS_DIFERENTE_INVALIDO_PROCESSAMENTO_CONCLUIDO(PRECONDITION_FAILED.value(),"O status do pedido deve ser igual Inválidado ou Processamento Concluído", PreconditionCustom.class),
     GRUPO_EMPRESA_NAO_ENCONTRADO_PRE_CONDITION(PRECONDITION_FAILED.value(),"Grupo empresa não encontrado." ,PreconditionCustom.class),
     USUARIO_NAO_ENCONTRADO_PRE_CONDITION(PRECONDITION_FAILED.value(),"Usuário não encontrado." ,PreconditionCustom.class),
     DADOS_ATUALIZACAO_NAO_INFORMADOS(PRECONDITION_FAILED.value(), "Dados para a atualização do registros não informados.", PreconditionCustom.class),
     ERRO_SUBGRUPO_NAO_E_GRUPO(PRECONDITION_FAILED.value(), "Subgrupo não é do grupo de empresas informado.", PreconditionCustom.class),
     ERRO_EMPRESA_NAO_E_DE_SUBGRUPO(PRECONDITION_FAILED.value(), "Empresa não é do subgrupo de empresas informado.", PreconditionCustom.class),
     SUBGRUPO_EMPRESA_NAO_CADASTRADO(BAD_REQUEST.value(), "SubGrupo não cadastrado", BadRequestCdt.class),
     SUBGRUPOS_NAO_LOCALIZADOS(NO_CONTENT.value(), "Grupo-empresa não possui subgrupos", NoContentCdt.class),
     SUBRGRUPO_NAO_EXISTE(NOT_FOUND.value(), "Registros não encontrados", NotFoundCdt.class),
     EMPRESA_NAO_ENCONTRADA(NOT_FOUND.value(), "Empresa não encontrada", NotFoundCdt.class),
     OPERACAO_INVALIDA(NOT_FOUND.value(),"Operação invalida" ,NotFoundCdt.class),
     PERMISSAO_NAO_ENCONTRADA(NOT_FOUND.value(), "Permissão não encontrada", NotFoundCdt.class),
     PESQUISA_NAO_ENCONTRADA(NO_CONTENT.value(), "Registros não encontrados", NoContentCdt.class),
     USUARIO_OBRIGATORIO(BAD_REQUEST.value(), "Usuário é obrigatório", BadRequestCdt.class),
     GRUPO_EMPRESA_OBRIGATORIO(BAD_REQUEST.value(),"Grupo Empresa é obrigatório",BadRequestCdt.class),
     USUARIO_REGISTRO_NAO_ENCONTRADO(NOT_FOUND.value(), "Usuário Registro não existe", NotFoundCdt.class),
     USUARIO_REGISTRO_OBRIGATORIO(BAD_REQUEST.value(), "Usuário Registro é obrigatório", BadRequestCdt.class),
     STATUS_INVALIDO(BAD_REQUEST.value(), "Status inválido", BadRequestCdt.class),
     LISTA_DE_NIVEL_POR_GRUPO_ACEITA_APENAS_UM_PARAMETRO(BAD_REQUEST.value(),"Lista de nível acesso grupo aceita apenas um parametro" ,BadRequestCdt.class),
     PERMISSAO_USUARIO_GRUPO_EMPRESA_EXISTENTE(CONFLICT.value(),"Permissão já cadastrado para esse usuário com o grupo empresa",ConflictCdt.class),
     NIVEL_ACESSO_NAO_EXISTE(NOT_FOUND.value(), "Indentificador de nível acesso informado não existe", NotFoundCdt.class),
     NIVEL_PERMISSAO_ACESSO_NAO_AUTORIZADO(FORBIDDEN.value(),"Nível permissão acesso não autorizado" ,ForbiddenCdt.class),
     EMPRESA_JA_CADASTRADA(BAD_REQUEST.value(), "Empresa já cadastrada", BadRequestCdt.class),
     FORMA_DE_FATURAMENTO_INVALIDA( BAD_REQUEST.value() ,"Forma de furamento e pedido inválida" , BadRequestCdt.class),
     PARAMETROS_GRUPO_EMPRESA_NAO_CADASTRADO(BAD_REQUEST.value(), "Parametros grupo empresa não cadastrado", BadRequestCdt.class),
     PARAMETRO_GRUPO_NAO_ENCONTRADO(PRECONDITION_FAILED.value(), "Parametro empresa não localizado", PreconditionCustom.class),
     ERRO_AO_CADASTRAR_EMPRESA(BAD_REQUEST.value(), "Erro ao cadastrar empresa", BadRequestCdt.class),
     SUBGRUPO_NAO_PODE_SER_ATUALIZADO(BAD_REQUEST.value(), "Subgrupo não é um grupo para ser atualizado", BadRequestCdt.class),
     USUARIO_REGISTRO_SEM_PERMISSAO_VINCULO(BAD_REQUEST.value(), "Usuário Registro não possui vínculo para realizar essa operação", BadRequestCdt.class),
     EMPRESA_NAO_FAZ_PARTE_GRUPO(BAD_REQUEST.value(), "Empresa não faz parte do grupo", BadRequestCdt.class),
     ID_INFORMADO_NAO_E_DE_SUBGRUPO(BAD_REQUEST.value(), "Id informado não é Subgrupo.", BadRequestCdt.class),
     PLATAFORMA_INVALIDA(BAD_REQUEST.value(),"Plataforma invalida.", BadRequestCdt.class),
     RECURSO_NAO_ENCONTRADO(NO_CONTENT.value(), "Recurso não encotrado", NoContentCdt.class),
     NAO_HA_FUNCIONARIOS_CARGA_BENEFICIO(NO_CONTENT.value(),"Não existe funcionários para a Carga Benefício informado.", NoContentCdt.class),
     FORA_DO_PERIODO_DE_CANCELAMENTO(BAD_REQUEST.value(), "Fora do período de cancelamento.", BadRequestCdt.class),
     NAO_CANCELADO_FATURAMENTRO_DESCENTRALIZADO(PRECONDITION_FAILED.value(), "Pedido não pode ser cancelado pois o faturamento é descentralizado", PreconditionCustom.class),
     CARGA_FORA_DO_PERIODO_DE_CANCELAMENTO(PRECONDITION_FAILED.value(), "Fora do período de cancelamento.", PreconditionCustom.class),
     FORMA_PAGAMENTO_DIFERENTE_TED(PRECONDITION_FAILED.value(), "Forma de pagamento é diferente de TED", PreconditionCustom.class),
     NIVEL_ACESSO_NAO_ENCONTRADO(NOT_FOUND.value(), "Nível de acesso não encontrado.", NotFoundCdt.class),
     PARAMETRO_TIPO_FATURAMENTO_NAO_ENCONTRADO(PRECONDITION_FAILED.value(), "Paramêtro tipo faturamento não encontrado", PreconditionCustom.class),
     TIPO_FATURAMENTO_NAO_PERMITE_CANCELAMENTO(PRECONDITION_FAILED.value(), "Não é possível cancelar uma carga deste pedido , pois o mesmo é centralizado", PreconditionCustom.class),
     STATUS_CARGA_NAO_PERMITE_CANCELAMENTO(PRECONDITION_FAILED.value(),"Status da carga benefício não permite cancelamento.", PreconditionCustom.class),
     NAO_POSSUI_PERMISSAO_PARA_CANCELAR_CARGA(FORBIDDEN.value(),"Usuário não possui permissão para cancelar o pedido desta empresa", ForbiddenCdt.class),
     FALHA_AO_EXECUTAR_PROC_DE_CANCELAMENTO_DE_PEDIDO(INTERNAL_SERVER_ERROR.value(), "Falha ao executar procedure de cancelamento de pedidos.", ExceptionCdt.class),
     ERRO_AO_BUSCAR_MENOR_DATA_AGENDAMENTO_CARGA(NOT_FOUND.value(), "Erro ao buscar menor data agendamento do pedido.", NotFoundCdt.class),
     CARTAO_NAO_ENCONTRADO(NOT_FOUND.value(), "Cartão não encontrado.", NotFoundCdt.class),
     CARTAO_CANCELAMENTO_JA_REALIZADO(BAD_REQUEST.value(), "Cartão já se encontra cancelado.", BadRequestCdt.class),
     CARTAO_STATUS_CANCELAMENTO_INVALIDO(BAD_REQUEST.value(), "Status de cancelamento inválido.", BadRequestCdt.class),
     CARTAO_STATUS_IGUAL(BAD_REQUEST.value(), "Informe um status diferente do atual.", BadRequestCdt.class),
     CARTAO_STATUS_NAO_PERMITE_CANCELAMENTO(BAD_REQUEST.value(), "Status desejado não permite cancelamento.", BadRequestCdt.class),
     STATUS_CARTAO_ERRO_INVALIDO(BAD_REQUEST.value(), "Status do cartão inválido", BadRequestCdt.class),
     FUNCIONARIO_NAO_ENCONTRADO(NOT_FOUND.value(), "Funcionário não encontrado", NotFoundCdt.class),
     GRUPOEMPRESA_NAO_ENCONTRADO(NOT_FOUND.value(), "Grupo empresa não encontrado", NotFoundCdt.class),
     ERRO_GERACAO_CARTAO(BAD_REQUEST.value(), "Erro na geração do cartão.", BadRequestCdt.class),
     ERRO_AO_CRIAR_USUARIO(INTERNAL_SERVER_ERROR.value(),"Erro ao criar usuario." ,ExceptionCdt.class),
     HIERARQUIA_ORGANIZACIONAL_INEXISTENTE(NOT_FOUND.value(), "Hierarquia organizacional não encontrada.", NotFoundCdt.class),
     CPF_NAO_INFORMADO(BAD_REQUEST.value(), "CPF não informado.", BadRequestCdt.class),
     DATA_NASCIMENTO_NAO_INFORMADA(BAD_REQUEST.value(), "Data de nascimento não informada.", BadRequestCdt.class),
     DATA_NASCIMENTO_INVALIDA(BAD_REQUEST.value(), "Data de nascimento inválida.", BadRequestCdt.class),
     ORIGEM_COMERCIAL_NAO_ENCONTRADA(BAD_REQUEST.value(), "Origem comercial não encontrada.", BadRequestCdt.class),
     ENDERECO_CORRESPONDENCIA_INEXISTENTE(BAD_REQUEST.value(), "Endereço de correspondência inexistente.", BadRequestCdt.class),
     ENDERECO_NAO_DEVE_CONTER_MAIS_DE_TRES(BAD_REQUEST.value(), "Pessoa não deve ter mais de três endereços.", BadRequestCdt.class),
     ENDERECO_DEVE_CONTER_APENAS_UM_PARA_CORRESPONDENCIA(BAD_REQUEST.value(), "Pessoa deve ter somente um endereço de correspondência.", BadRequestCdt.class),
     ENDERECO_DEVE_SER_DE_TIPOS_DIFERENTES(BAD_REQUEST.value(), "Endereços devem ser de tipos diferentes.", BadRequestCdt.class),
     ERRO_AO_SALVAR_PESSOA_FISICA(PRECONDITION_FAILED.value(), "Erro ao salvar pessoa física.", PreconditionCustom.class),
     CONTA_DIA_VENCIMENTO_INVALIDO(BAD_REQUEST.value(), "Dia de vencimento inválido.", BadRequestCdt.class),
     ID_PESSOA_NAO_INFORMADO(BAD_REQUEST.value(), "ID da pessoa não informado.", BadRequestCdt.class),
     ID_EMPRESA_NAO_INFORMADO(BAD_REQUEST.value(), "ID da pessoa não informado.", BadRequestCdt.class),
     ID_PRODUTO_NAO_INFORMADO(BAD_REQUEST.value(), "ID do produto não informado.", BadRequestCdt.class),
     PARAMETROS_CONTA_NAO_INFORMADOS(BAD_REQUEST.value(), "Parâmetros da conta não informados.", BadRequestCdt.class),
     ERRO_AO_SALVAR_EMPRESA(INTERNAL_SERVER_ERROR.value(), "Erro ao salvar empresa.", ExceptionCdt.class),
     HIERARQUIA_ORANIZACIONAL_EMPRESAS_NAO_ENCONTRADO(NO_CONTENT.value(), "Empresas não encontrado para o usuário e grupo empresa informado", NoContentCdt.class),
     ARQUIVO_UNIDADE_ENTREGA_NAO_ENCONTRADO(PRECONDITION_FAILED.value(),"Não foi encontrado nenhum arquivo com o identificador informado", PreconditionCustom.class),
     NAO_HA_HIERARQUIA_ORGANIZACIONAL_GRUPO(NO_CONTENT.value(), "Não existe hierarquia organizacional para o usuário e grupo empresa informado", NoContentCdt.class),
     HIERARQUIA_ACESSO_NAO_PODE_SER_CRIADA(PRECONDITION_FAILED.value(), "Hierarquia não pode ser criada, usuário já possui níveis de acesso, usuário deve ser atualizado", PreconditionCustom.class),
     ERRO_GERAR_ACCESS_TOKEN_DELOITTE(UNPROCESSABLE_ENTITY.value(), "Ocorreu um erro ao tentar gerar o access token para acesso a Nota Fiscal.", UnprocessableEntityCdt.class),
     ERRO_BUSCAR_NOTA_FISCAL_DELOITTE(UNPROCESSABLE_ENTITY.value(), "Ocorreu um erro ao tentar gerar a Nota Fiscal.", UnprocessableEntityCdt.class),
     USUARIO_SEM_ACESSO_NOTA_FISCAL_DELOITTE(FORBIDDEN.value(), "Usuário não possui acesso para visualizar a nota fiscal.", ForbiddenCdt.class),
     NOTA_FISCAL_PRE_CONDITION(PRECONDITION_FAILED.value(), "O código pesquisado não é de uma Nota Fiscal, é de uma RPS", PreconditionCustom.class),
     NAO_POSSUI_NIVEL_ACESSO_GRUPO(FORBIDDEN.value(), "Usuário informado não possui permissão de acesso a nível grupo", ForbiddenCdt.class),
     PERMISSAO_HIERARQUIA_PRE_CONDICAO(PRECONDITION_FAILED.value(), "Permissão de usuário para criar hierarquia não encotrado" , PreconditionCustom.class),
     USUARIO_LOGADO_SEM_NIVEL_ACESSO(PRECONDITION_FAILED.value(), "Usuário logado não possui nível de acesso.", PreconditionCustom.class),
     USUARIO_LOGADO_NAO_ENCONTRADO(PRECONDITION_FAILED.value(), "Usuário logado não foi encontrado.", PreconditionCustom.class),
     UPLOAD_ARQUIVO_UNIDADE_ENTREGA_PROCESSANDO(CONFLICT.value(), "Já existe um arquivo em processamento.", ConflictCdt.class),
     ARQUIVO_UNIDADE_ENTREGA_STATUS_DIFERENTE_INVALIDADO_PROCESSAMENTO_CONCLUIDO(PRECONDITION_FAILED.value(),"O status do cadastro da unidade de entrega deve ser igual Inválidado ou Processamento Concluído", PreconditionCustom.class),
     NAO_HA_DETALHES_ARQUIVO_UNIDADE_ENTREGA_ERRO(NO_CONTENT.value(), "Não existem erros para o arquivo de cadastro da unidade informado.", NoContentCdt.class),
     PAGINACAO_NEGATIVA(PRECONDITION_FAILED.value(), "A paginação não pode ser negativa.", PreconditionCustom.class),
     PARAMETROS_GRUPO_EMPRESA_NAO_ENCONTRADO(NOT_FOUND.value(), "Parametros do grupo empresa não encontrados.", NotFoundCdt.class),
     NAO_EXISTE_UNIDADE_DE_ENTREGA(PRECONDITION_FAILED.value(), "Não existe unidades de entrega cadastrados.", PreconditionCustom.class),
     PARAMETRO_TIPO_PAGAMENTO_NAO_ENCONTRADO(PRECONDITION_FAILED.value(), "Parametro do tipo de pagamento não encontrado", PreconditionCustom.class),
     PESSOA_NAO_ENCONTRADA(NOT_FOUND.value(), "Pessoa não encontrada.", NotFoundCdt.class),
     ENDERECO_NAO_ENCONTRADO(NOT_FOUND.value(), "Endereço não encontrado.", NotFoundCdt.class),
     REQUEST_COM_DADOS_INVALIDO(BAD_REQUEST.value(), "O request possui dados inválidos.", BadRequestCdt.class),
     USUARIO_SEM_PERMISSAO_ATUALIZAR_FUNCIONARIO(FORBIDDEN.value(), "Usuário não possui acesso para atualizar os dados do funcionário.", ForbiddenCdt.class),
     DADOS_ENDERECO_OBRIGATORIOS(BAD_REQUEST.value(), "Endereço obrigatório.", BadRequestCdt.class),
     ARQUIVO_CARGAS_STATUS_RECEBIDO_NAO_ENCONTRADO(NOT_FOUND.value(), "Não existe status recebido para arquivo de carga cadastrado na base .", NotFoundCdt.class),
     ARQUIVO_CARGAS_STATUS_IMPORTADO_NAO_ENCONTRADO(NOT_FOUND.value(), "Não existe status importado para arquivo de carga cadastrado na base .", NotFoundCdt.class),
     GRUPOEMPRESA_REQUEST_DATA_VIGENCIA_OBRIGATORIA(BAD_REQUEST.value(), "A data de vigência do limite é obrigatória.", BadRequestCdt.class),
     GRUPOEMPRESA_REQUEST_DATA_VIGENCIA_INFERIOR_DATA_ATUAL(BAD_REQUEST.value(), "A data de vigência do limite não pode ser inferior a data atual.", BadRequestCdt.class),
     PARAMETRO_DATA_VIGENCIA_NAO_ENCONTRADO(PRECONDITION_FAILED.value(), "Parametro data de vigência do limite de crédito não encontrado", PreconditionCustom.class),
     GRUPOEMPRESA_REQUEST_PRAZO_PAGAMENTO_OBRIGATORIO(BAD_REQUEST.value(), "O prazo de pagamento é obrigatório.", BadRequestCdt.class),
     GRUPOEMPRESA_REQUEST_PRAZO_PAGAMENTO_MENOR_QUE_ZERO(BAD_REQUEST.value(), "O prazo de pagamento não pode ser menor que zero.", BadRequestCdt.class),
     GRUPOEMPRESA_REQUEST_PRAZO_PAGAMENTO_MAIOR_QUE_VALOR_MAXIMO(BAD_REQUEST.value(), "O prazo de pagamento não pode ser maior que %s dias.", BadRequestCdt.class),
     PERIODO_DATAINICIO_MAIOR_DATAATUAL(BAD_REQUEST.value(), "A data início não pode ser maior que a data atual.", BadRequestCdt.class),
     PERIODO_DATAFIM_MAIOR_DATAATUAL(BAD_REQUEST.value(), "A data fim não pode ser maior que a data atual.", BadRequestCdt.class),
     PERIODO_DATAFIM_MENOR_DATAINICIO(BAD_REQUEST.value(), "A data fim do período não pode ser menor que a data início.", BadRequestCdt.class),
     PERIODO_INTERVALO_MAIOR_QUE_O_PERMITIDO(BAD_REQUEST.value(), "O intervalo do período solicitado não pode ser maior do que %s dias.", BadRequestCdt.class),
     CPF_SEM_PERMISSAO_AO_GRUPO(FORBIDDEN.value(), "O CPF informado não possui vinculo com o Grupo empresa.", ForbiddenCdt.class),
     ARQUIVO_SOMENTE_UMA_ABA(BAD_REQUEST.value(), "O arquivo não deve conter mais de uma aba", BadRequestCdt.class),
     DATA_NASCIMENTO_MAIOR_QUE_ATUAL(BAD_REQUEST.value(), "A data de nascimento não pode ser maior que a data atual", BadRequestCdt.class),
     USUARIO_E_GRUPO_SEM_ACESSO_AO_RELATORIO(FORBIDDEN.value(), "O usuário e o grupo informados não possui acesso ao(s) relatório(s) informado(s)", ForbiddenCdt.class),
     EXCLUSAO_RELATORIOS_STATUS_PROCESSANDO(BAD_REQUEST.value(), "Não é possível excluir relatório(s) com status Processando", BadRequestCdt.class),
     RELATORIO_PEDIDO_EMPTY(NOT_FOUND.value(), "Não existe dados para o relatório solicitado", NotFoundCdt.class),
     RELATORIO_NOT_FOUND(NOT_FOUND.value(), "Não foi possível encontrar o relatório com os parâmetros informados", NotFoundCdt.class),
     RELATORIO_DIFERENTE_PROCESSADO(BAD_REQUEST.value(), "O relatório precisa estar com o status processado para realizar a operação", BadRequestCdt.class)
     ;
    

     private Integer code;

     @Setter
     private String message;

     private Class<? extends ExceptionCdt> klass;

     ExceptionsMessagesCdtEnum(int code, String message, Class<? extends ExceptionCdt> klass) {

          this.message = message;
          this.klass = klass;
          this.code = code;
     }

     public static ExceptionsMessagesCdtEnum getEnum(final String key) {

          return asList(ExceptionsMessagesCdtEnum.values()).stream().map(e -> e).filter(e -> StringUtils.equals(e.getMessage(), key)).findAny().orElse(null);
     }

     public void raise() throws ExceptionCdt {

          log.debug("Raising error: {}", this);
          if (BAD_REQUEST.value() == this.code) {
               throw new BadRequestCdt(this.message);
          } else if (CONFLICT.value() == this.code) {
               throw new ConflictCdt(this.message);
          } else if (PRECONDITION_FAILED.value() == this.code) {
               throw new PreconditionCustom(this.message);
          } else if (NOT_FOUND.value() == this.code) {
               throw new NotFoundCdt(this.message);
          } else if (NO_CONTENT.value() == this.code) {
               throw new NoContentCdt(this.message);
          } else if (PRECONDITION_FAILED.value() == this.code) {
               throw new PreconditionCustom(this.message);
          } else if (UNAUTHORIZED.value() == this.code) {
               throw new UnauthorizedCdt(this.message);
          } else if (FORBIDDEN.value() == this.code) {
               throw new ForbiddenCdt(this.message);
          } else if (UNPROCESSABLE_ENTITY.value() == this.code) {
               throw new UnprocessableEntityCdt(this.message);
          } else {
               throw new ExceptionCdt(INTERNAL_SERVER_ERROR, this.message);
          }
     }

     public void raise(String errorMessage) {

          this.setMessage(errorMessage);
          this.raise();
     }

}
