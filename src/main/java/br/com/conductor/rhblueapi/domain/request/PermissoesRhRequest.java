
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import br.com.conductor.rhblueapi.enums.StatusPermissaoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ApiModel(value = "PermissoesRhRequest", description = "Permissões de usuários com grupo empresa")
public class PermissoesRhRequest implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = -7945881106958351489L;

     /**
      * Id da permissão
      */
     @ApiModelProperty(value = "Id da permissão (Ex: 11)", position = 1)
     private Long id;

     /**
      * Id do usuário
      */
     @ApiModelProperty(value = "Id do usuário (Ex: 123)", position = 2)
     private Long idUsuario;

     /**
      * Id do grupo empresa
      */
     @ApiModelProperty(value = "Id do grupo empresa (Ex: 1)", position = 3)
     private Long idGrupoEmpresa;

     /**
      * Id do usuário que realizou o cadastro
      */
     @ApiModelProperty(value = "Id do usuário que realizou o cadastro (Ex: 153)", position = 4)
     private Long idUsuarioRegistro;
     
     /**
      * Status do usuário
      */
     @ApiModelProperty(value = "Status do usuário (Ex: ATIVO ou INATIVO ou BLOQUEADO)", position = 5)
     private StatusPermissaoEnum statusDescricao;

}
