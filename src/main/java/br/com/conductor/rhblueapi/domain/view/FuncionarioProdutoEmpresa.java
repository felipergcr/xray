
package br.com.conductor.rhblueapi.domain.view;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@Table(name = "VW_FUNCIONARIOPRODUTOEMPRESA")
public class FuncionarioProdutoEmpresa implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @Id
     @Column(name = "EMPRESACARGA_ID")
     private Long empresaCargaId;

     @Column(name = "NOME")
     private String nome;

     @Column(name = "CPF")
     private String cpf;

     @Column(name = "VALOR")
     private Double valor;

     @Column(name = "DATA_CARGA")
     private Date dataCarga;

     @Column(name = "PRODUTO_ID")
     private Long produtoId;

     @Column(name = "EMPRESA_ID")
     private Long empresaId;

     @Column(name = "FUNCIONARIO_ID")
     private Long funcionarioId;

     @Column(name = "FUNCIONARIOPRODUTO_ID")
     private Long funcionarioProdutoId;

     @Column(name = "STATUS")
     private String status;
}
