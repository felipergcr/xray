
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "TELEFONES")
public class TelefonePessoaFisica implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_TELEFONE")
     private Long id;

     @Column(name = "ID_TIPOTELEFONE")
     private Integer idTipoTelefone;

     @Column(name = "ID_ESTABELECIMENTO")
     private Long idEstabelecimento = 0l;

     @Column(name = "DDD", length = 4)
     private String ddd;

     @Column(name = "TELEFONE", length = 10)
     private String telefone;

     @Column(name = "RAMAL", length = 10)
     private String ramal;

     @Column(name = "STATUSATIVO")
     private Integer status = 1;

     @ManyToOne(optional = false)
     @JoinColumn(name = "ID_PESSOAFISICA", nullable = false)
     @OnDelete(action = OnDeleteAction.CASCADE)
     private PessoaFisicaCadastro pessoaFisica;
}