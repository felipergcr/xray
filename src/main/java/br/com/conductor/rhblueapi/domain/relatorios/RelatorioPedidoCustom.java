
package br.com.conductor.rhblueapi.domain.relatorios;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RelatorioPedidoCustom implements Serializable {

     private static final long serialVersionUID = 6445282528365138881L;

     @Id
     @Column(name = "ID")
     private Long id;

     @Column(name = "ID_PEDIDO")
     private Long idPedido;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;

     @Column(name = "CNPJ")
     private String cnpj;

     @Column(name = "RAZAO_SOCIAL")
     private String razaoSocial;

     @Column(name = "ID_GRUPO")
     private Long idGrupo;

     @Column(name = "NOME_GRUPO")
     private String nomeGrupo;

     @Column(name = "ID_SUBGRUPO")
     private Long idSubGrupo;

     @Column(name = "NOME_SUBGRUPO")
     private String nomeSubGrupo;

     @Column(name = "STATUS_CARGASBENEFICIOS")
     private Integer statusCargasBeneficios;

     @Column(name = "DT_CREDITO_AGENDADA")
     private LocalDate dtCreditoAgendada;

     @Column(name = "DT_CREDITO_EFETIVA")
     private LocalDate dtCreditoEfetiva;

     @Column(name = "DT_VENCIMENTO")
     private LocalDate dtVencimento;

     @Column(name = "VALOR_TOTAL_PAGAMENTO")
     private BigDecimal valorTotalPagamento;

     @Column(name = "DT_PAGAMENTO")
     private LocalDate dtPagamento;

     @Column(name = "DESCRICAO_TIPO_BOLETO")
     private String descricaoTipoBoleto;

     @Column(name = "DT_PEDIDO")
     private LocalDateTime dtPedido;

     @Column(name = "PRAZO_PAGAMENTO")
     private String prazoPagamento;

     @Column(name = "MODALIDADE_PAGAMENTO")
     private String modalidadePagamento;

     @Column(name = "QTD_CARTOES_NOVOS")
     private Long qtdCartoesNovos;

     @Column(name = "VALOR_PEDIDO")
     private BigDecimal valorPedido;

     @Column(name = "STATUS_PAGAMENTO_ARQUIVOCARGASTD")
     private Integer statusPagamentoArquivoCargaStd;

     @Column(name = "QTD_FUNC_SOLICITADO")
     private Long qtdFuncSolicitado;

     @Column(name = "QTD_FUNC_CREDITADO")
     private Long qtdFuncCreditado;

     @Column(name = "ID_PRODUTO")
     private Long idProduto;

}
