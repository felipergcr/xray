
package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.Email;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.caelum.stella.bean.validation.CPF;
import br.com.conductor.rhblueapi.controleAcesso.domain.PerfisAcessosUsuarios;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do usuário", example = "1", position=1)
     private Long id;
     
     @ApiModelProperty(notes = "Identificador de permissão usuário cadastrado", example = "1",  position=2)
     private Long idPermissao;
     
     @ApiModelProperty(notes = "Identificador de nivel de permissão acesso do usuário cadastrado", example = "1",  position=3)
     private Long idNivelPermissaoAcesso;

     @CPF
     @ApiModelProperty(value = "cpf", notes = "CPF do usuário", example = "12345678910",  position=4)
     private String cpf;

     @Email
     @ApiModelProperty(value = "email", notes = "Email do usuário", example = "email@conductor.com.br",  position=5)
     private String email;

     @ApiModelProperty(value = "nome", notes = "Nome do usuário", example = "Maria da Paz",  position=6)
     private String nome;

     @ApiModelProperty(value = "login", notes = "Login do usuário", example = "12345678910",  position=7)
     private String login;

     @ApiModelProperty(notes = "Status do usuário", required = false, example = "1",  position=8)
     private Integer status;
     
     @ApiModelProperty(notes = "Flag de bloqueio de acesso", required = false, example = "true",  position=9)
     private Boolean bloquearAcesso;

     @ApiModelProperty(notes = "Quantidade de tentativas com senha incorreta", required = false, example = "1",  position=10)
     private Integer tentativasIncorretas;

     @ApiModelProperty(value = "dataModificacao", notes = "Data da última alteração do usuário", example = "2018-09-25'T'10:10:00.000'Z'",  position=11)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat( shape=Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @DateTimeFormat(iso = ISO.DATE_TIME)
     private LocalDateTime dataModificacao;

     @ApiModelProperty(notes = "Identificador da Plataform", example = "1",  position=12)
     private Integer idPlataforma;
     
     @ApiModelProperty(hidden = true, notes = "Lista de Identificador de Perfis", example = "[1,1]",  position=13)
     private List<PerfisAcessosUsuarios> idsPerfis;

}
