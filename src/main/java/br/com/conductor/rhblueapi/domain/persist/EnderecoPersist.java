
package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(value = Include.NON_NULL)
public class EnderecoPersist implements Serializable {

     private static final long serialVersionUID = 3577755808229640437L;

     /**
      * Logradouro do endereço do estabelecimento
      */
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 50, message = "quantidade de caracteres deve ser menor que 50")
     @ApiModelProperty(value = "Lougradoro da empresa", example = "Rua Angatuba", required = true, position = 1)
     private String logradouro;

     /**
      * Número do endereço do estabelecimento
      */
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Numero do lougradoro", example = "41", required = true, position = 2)
     private Integer numero;

     /**
      * Complemento do endereço
      */

     @Size(max = 30, message = "quantidade de caracteres deve ser menor que 30")
     @ApiModelProperty(value = "Complemento do lougradoro", example = "casa 3", required = true, position = 3)
     private String complemento;

     /**
      * CEP do endereço do estabelecimento
      */
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Pattern(regexp = "^([0-9]{8})$", message = "inválido")
     @ApiModelProperty(value = "CEP", example = "02258200", required = true, position = 4)
     private String cep;

     /**
      * Bairro do endereço do estabelecimento
      */
     @NotNull
     @NotEmpty
     @Size(max = 40, message = "quantidade de caracteres deve ser menor que 40")
     @ApiModelProperty(value = "Bairro", example = "Vila Constança", required = true, position = 5)
     private String bairro;

     /**
      * Cidade do endereço do estabelecimento
      */
     @NotNull
     @NotEmpty
     @Size(max = 50, message = "quantidade de caracteres deve ser menor que 50")
     @ApiModelProperty(value = "Cidade", example = "São Paulo", required = true, position = 6)
     private String cidade;

     /**
      * UF do endereço do estabelecimento
      */
     @NotNull
     @NotEmpty
     @Size(max = 2, message = "quantidade de caracteres deve ser menor que 2")
     @ApiModelProperty(value = "Estado", example = "SP", required = true, position = 7)
     private String uf;
     
     @NotEmpty
     @Pattern(regexp = "^([0-9]{7,8})$", message = "O código IBGE está fora do padrão esperado")
     @ApiModelProperty(value = "Código IBGE", example = "3550308", required = true, position = 8)
     private String codigoIbge;
}
