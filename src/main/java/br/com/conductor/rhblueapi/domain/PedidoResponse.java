package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response Customizado para mostrar do pedido")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PedidoResponse implements Serializable {
     
     private static final long serialVersionUID = -3615148445881156655L;
     
     @ApiModelProperty(value = "Número do pedido", position = 1)
     private Long numeroPedido;

}
