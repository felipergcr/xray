
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="GRUPOSEMPRESAS")
public class SubgrupoEmpresa extends GrupoEmpresaAbstract implements Serializable {

     
     private static final long serialVersionUID = 1L;

     @Setter
     @Getter
     @Column(name = "NOME")
     private String nomeSubGrupo;

     @OneToOne(fetch = FetchType.EAGER)
     @JoinColumn(name = "ID_EMPRESAPRINCIPAL", referencedColumnName = "ID_EMPRESA",  insertable=false, updatable=false)
     @NotFound(action = NotFoundAction.IGNORE)
     private Empresa empresa;

}
