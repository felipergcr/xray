
package br.com.conductor.rhblueapi.domain.deloitte;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NotaFiscalResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     private byte[] notaFiscal;

     private String nomeArquivo;

}
