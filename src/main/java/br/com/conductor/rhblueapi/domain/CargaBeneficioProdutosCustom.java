
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CargaBeneficioProdutosCustom implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = -6831807903563658468L;

     @Id
     @Column(name = "ID")
     private Integer id;
     
     @Column(name = "ID_CARGABENEFICIO")
     private Long idCargaBeneficio;

     @Column(name = "VALOR")
     private BigDecimal valorTotal;

     @Column(name = "ID_PRODUTO")
     private Long idProduto;

     @Column(name = "NOME_PRODUTO")
     private String nomeProduto;

}
