package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TELEFONESESTABELECIMENTOS")
@Getter @Setter
public class TelefoneEstabelecimento implements Serializable {

     private static final long serialVersionUID = -1150928620268138359L;
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_TELEFONE")
     private Long id;
     
     @Column(name = "DDD")
     private String ddd;
     
     @Column(name = "TELEFONE")
     private String telefone;
     
     @Column(name = "RAMAL")
     private String ramal;
}
