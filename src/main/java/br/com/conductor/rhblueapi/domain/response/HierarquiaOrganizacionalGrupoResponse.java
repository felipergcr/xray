
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Campos do objeto hierarquico Grupo Empresa")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class HierarquiaOrganizacionalGrupoResponse implements Serializable {

     private static final long serialVersionUID = -2748840910055531933L;

     @ApiModelProperty(value = "Id do Grupo Empresa", position = 1)
     private Long idGrupoEmpresa;

     @ApiModelProperty(value = "Nome do Grupo Empresa", position = 2)
     private String nome;

     @ApiModelProperty(value = "SubGrupos do Grupo Empresa", position = 3)
     private List<HierarquiaOrganizacionalSubGrupoResponse> subGrupos;
}
