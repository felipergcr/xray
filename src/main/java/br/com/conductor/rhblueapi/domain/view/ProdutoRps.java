
package br.com.conductor.rhblueapi.domain.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "vw_produtos_rps")
public class ProdutoRps implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1768947385857114454L;

     @Id
     @Column(name = "NUMERO_PEDIDO")
     private Long numeroPedido;

     @Column(name = "ID_PRODUTO")
     private Long produtoId;

     @Column(name = "NOME")
     private String nome;

     @Column(name = "QTD")
     private Integer quantidade;

     @Column(name = "VALOR")
     private Double valor;

     @Column(name = "NUMERO_RPS")
     private Long numeroRps;
}
