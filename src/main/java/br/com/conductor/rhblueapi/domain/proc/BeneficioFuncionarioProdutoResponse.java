package br.com.conductor.rhblueapi.domain.proc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class BeneficioFuncionarioProdutoResponse implements Serializable {
     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     @Id
     @Column(name = "ID_FUNCIONARIOPRODUTO")
     private Long idFuncionarioProduto;
     
     @Column(name = "ID_CONTA")
     private Long idConta;
     
     @Column(name = "FLAGCARTAO")
     private Integer flagCartao;
     
     @Column(name = "CODIGORETORNO")
     private String codigoRetorno;
     
     @Column(name = "MSGRETORNO")
     private String msgRetorno;

}
