package br.com.conductor.rhblueapi.domain.response;

import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.SubgrupoEmpresa;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CadastroGrupoEmpresaResponse {

     private GrupoEmpresa grupoEmpresa;

     private SubgrupoEmpresa subGrupo;

     private UsuarioResponse usuarioCadastrado;

     private EmpresaResponse empresa;

}
