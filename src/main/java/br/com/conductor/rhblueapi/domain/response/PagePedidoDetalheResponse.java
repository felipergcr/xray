
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel
@Getter
@Setter
@Builder
@NoArgsConstructor
public class PagePedidoDetalheResponse extends PageResponse<PedidoDetalhesResponse> implements Serializable {

     private static final long serialVersionUID = 1787274198353328643L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PagePedidoDetalheResponse(PageResponse p) {

          super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last, p.nextPage, p.previousPage, p.content);
     }

}
