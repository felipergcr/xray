
package br.com.conductor.rhblueapi.domain.relatorios;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "RELATORIOPORTAL")
public class RelatorioPortal implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_RELATORIO")
     private Long id;

     @ManyToOne(fetch = FetchType.LAZY)
     @JoinColumns({ @JoinColumn(name = "ID_TIPORELATORIOPORTAL", referencedColumnName = "ID_TIPORELATORIOPORTAL") })
     @NotFound(action = NotFoundAction.IGNORE)
     private TipoRelatorioPortal tipoRelatorioPortal;

     @Column(name = "DATASOLICITACAO")
     private LocalDateTime dataSolicitacao;

     @Column(name = "DATAINICIO")
     private LocalDate dataInicio;

     @Column(name = "DATAFIM")
     private LocalDate dataFim;
     
     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Column(name = "ID_USUARIO")
     private Long idUsuario;

     @Column(name = "NOMESOLICITANTE")
     private String nomeSolicitante;

     @Column(name = "CAMINHO")
     private String caminhoArmazenamento;

     @Column(name = "STATUSRELATORIO")
     private Integer status;
     
}
