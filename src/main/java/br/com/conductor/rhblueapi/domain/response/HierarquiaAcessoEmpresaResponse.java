package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class HierarquiaAcessoEmpresaResponse implements Serializable{

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value="Identificador da Empresa", example = "1", position= 1 )
     private Long idEmpresa;
     
     @ApiModelProperty(value="Nome Empresa", example="Santander",position=2)
     private String nomeEmpresa;
     
     @ApiModelProperty(value="CNPJ da Empresa", example="14246329000141",position=3)
     private String cnpj;
}
