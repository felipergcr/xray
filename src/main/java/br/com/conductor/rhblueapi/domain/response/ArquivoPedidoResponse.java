
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioResumido;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response Customizado para mostrar do pedido")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArquivoPedidoResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do Pedido", position = 1)
     private Long id;

     @ApiModelProperty(value = "Nome do Arquivo", position = 2)
     private String nome;

     private Long status;

     @ApiModelProperty(value = "Status Pedido", position = 3)
     private String statusEnum;

     @ApiModelProperty(value = "Motivo", position = 4)
     private String motivo;

     @ApiModelProperty(value = "Valor Pedido", position = 5)
     private BigDecimal valor;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @ApiModelProperty(value = "Data de Upload do arquivo", position = 6)
     private LocalDateTime dataImportacao;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @ApiModelProperty(value = "Data de processamento do arquivo", position = 7)
     private LocalDateTime dataProcessamento;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @ApiModelProperty(value = "Data de atualização dos status", position = 8)
     private LocalDateTime dataStatus;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @ApiModelProperty(value="Data de atualização de Agendamento", position=9)
     private LocalDateTime dataAgendamento;
     
     @ApiModelProperty(value="Identificador do grupo empresa", position=10)
     private Long idGrupoEmpresa;

     @ApiModelProperty(value = " Informações do usuário", position = 11)
     private UsuarioResumido usuario;

     @JsonIgnore
     private Long idUsuario;
     
     private Integer statusPagamento;
     
     @ApiModelProperty(value = "Status Pagamento", position = 12)
     private String statusPagamentoEnum;
     
     @JsonIgnore
     private Integer flagFaturamentoCentralizado;
     
     @JsonIgnore
     private LocalDate menorDataAgendamento;
     
     @ApiModelProperty(value = "Ações possíveis por pedido", position = 13)
     private AcoesPedidoResponse acoes;

}
