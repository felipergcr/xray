
package br.com.conductor.rhblueapi.domain.deloitte;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AutenticacaoDeloitte {
     
     private boolean authenticated;

     private String created;

     private String expiration;

     private String accessToken;

     private String message;
}
