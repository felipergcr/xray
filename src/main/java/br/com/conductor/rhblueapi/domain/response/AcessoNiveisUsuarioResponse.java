package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Response da alteracao de senha do usuario")
public class AcessoNiveisUsuarioResponse implements Serializable {

     private static final long serialVersionUID = -1150928620268138359L;
     
     @ApiModelProperty(value = "Identificador do perfil acesso", position = 1, example = "1")
     private Long idNivelUsuario;

     @ApiModelProperty(value = "Descrição do perfil acesso", position = 2, example = "Master")
     private String descricaoNivelPefil;
     
     @ApiModelProperty(value = "Nível do acesso", position = 3, example = "1")
     private Integer nivelPerfil;
     
     @ApiModelProperty(value = "Id da Plataforma", position = 4, example = "16")
     private Integer idPlataforma;

}