
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude={"id", "dataStatus", "dataRegistro", "status"})
@Table(name = "UNIDADESENTREGA")
@ApiModel(description = "Response da unidade de entrega")
public class UnidadeEntrega implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_UNIDADEENTREGA")
     @ApiModelProperty(value = "Identificador da unidade de entrega", position = 1, example = "1")
     private Long id;

     @Column(name = "ID_GRUPOEMPRESA")
     @ApiModelProperty(value = "Identificador do grupo empresa", position = 2, example = "123")
     private Long idGrupoEmpresa;

     @Column(name = "CODIGOUNIDADEENTREGA")
     @ApiModelProperty(value = "Código da unidade de entrega", position = 3, example = "EMPRESA1")
     private String codigoUnidadeEntrega;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATASTATUS")
     @ApiModelProperty(value = "Data da alteração do status", position = 4, example = "2019-05-29 18:51:00")
     private LocalDateTime dataStatus;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAREGISTRO")
     @ApiModelProperty(value = "data da alteração do registro", position = 5, example = "2019-05-29 18:51:00")
     private LocalDateTime dataRegistro;

     @Column(name = "STATUS")
     @ApiModelProperty(value = "Status da unidade de entrega", position = 6, example = "1")
     private Integer status;

     @Column(name = "LOGRADOURO")
     @ApiModelProperty(value = "Logradouro", position = 7, example = "Rua/Av")
     private String logradouro;

     @Column(name = "ENDERECO")
     @ApiModelProperty(value = "Endereço", position = 8, example = "VERGUEIRO")
     private String endereco;

     @Column(name = "ENDERECONUMERO")
     @ApiModelProperty(value = "Número do endereço", position = 9, example = "123")
     private String enderecoNumero;

     @Column(name = "ENDERECOCOMPLEMENTO")
     @ApiModelProperty(value = "Complemento do endereço", position = 10, example = "15 andar")
     private String enderecoComplemento;

     @Column(name = "BAIRRO")
     @ApiModelProperty(value = "Bairro", position = 11, example = "SAUDE")
     private String bairro;

     @Column(name = "CIDADE")
     @ApiModelProperty(value = "Cidade", position = 12, example = "São Paulo")
     private String cidade;

     @Column(name = "UF")
     @ApiModelProperty(value = "Uf", position = 13, example = "SP")
     private String uf;

     @Column(name = "CEP")
     @ApiModelProperty(value = "Cep", position = 14, example = "04143010")
     private String cep;

     @Column(name = "NOMERESPONSAVEL1")
     @ApiModelProperty(value = "Nome do Rsponsável 1", position = 15, example = "Alfredo Nunes")
     private String nomeResponsavel1;

     @Column(name = "TIPODOCUMENTORESPONSAVEL1")
     @ApiModelProperty(value = "Tipo do documento do responsável 1", position = 16, example = "RG/CPF")
     private String tipoDocumentoResponsavel1;

     @Column(name = "DOCUMENTORESPONSAVEL1")
     @ApiModelProperty(value = "Documento do responsável 1", position = 17, example = "10252484857")
     private String documentoResponsavel1;

     @Column(name = "TELEFONERESPONSAVEL1")
     @ApiModelProperty(value = "Telefone do responsável 1", position = 18, example = "11984562356")
     private String telefoneResponsavel1;

     @Column(name = "NOMERESPONSAVEL2")
     @ApiModelProperty(value = "Nome do Rsponsável 2", position = 19, example = "Fernando Nunes")
     private String nomeResponsavel2;

     @Column(name = "TIPODOCUMENTORESPONSAVEL2")
     @ApiModelProperty(value = "Tipo do documento do responsável 2", position = 20, example = "RG/CPF")
     private String tipoDocumentoResponsavel2;

     @Column(name = "DOCUMENTORESPONSAVEL2")
     @ApiModelProperty(value = "Documento do responsável 2", position = 12, example = "28023654x")
     private String documentoResponsavel2;

     @Column(name = "TELEFONERESPONSAVEL2")
     @ApiModelProperty(value = "Telefone do responsável 2", position = 22, example = "11964256389")
     private String telefoneResponsavel2;

}