
package br.com.conductor.rhblueapi.domain.relatorios;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "TIPORELATORIOPORTAL")
public class TipoRelatorioPortal implements Serializable {
     
     private static final long serialVersionUID = -4560705743587837140L;
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_TIPORELATORIOPORTAL")
     private Long id;
     
     @Column(name = "ID_PLATAFORMA")
     private Integer idPlataforma;
     
     @Column(name = "DESCRICAOTIPORELATORIO")
     private String descricaoTipoRelatorio;

}
