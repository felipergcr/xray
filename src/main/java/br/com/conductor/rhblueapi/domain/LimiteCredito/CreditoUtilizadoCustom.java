
package br.com.conductor.rhblueapi.domain.LimiteCredito;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CreditoUtilizadoCustom implements Serializable {

     private static final long serialVersionUID = 3406450776787301277L;

     @Id
     @Column(name="ID")
     private Long id;
     
     @Column(name="TIPO")
     private String tipo;

     @Column(name="VALOR")
     private BigDecimal valor;

}
