package br.com.conductor.rhblueapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "GRUPOSOPERACOESACESSOS")
@Getter
@Setter
@ToString
public class GruposOperacoesAcessos {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_GRUPOSOPERACOESACESSOS")
     private Long id;

     @Column(name = "DESCRICAO")
     private String descricao;

     @Column(name = "ORDEM")
     private Long ordem;
     
}
