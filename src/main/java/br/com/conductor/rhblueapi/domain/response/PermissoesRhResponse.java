package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.enums.StatusPermissaoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PermissoesRhResponse implements Serializable {
     /**
      * 
      */
     private static final long serialVersionUID = 746410472607208416L;

     @ApiModelProperty(value = "Id da permissão", position = 1)
     private Long id;
     
     @ApiModelProperty(value = "Id do usuário", position = 2)
     private Long idUsuario;
     
     @ApiModelProperty(value = "Nome do grupo empresa", position = 4)
     private String nomeGrupoEmpresa;
     
     @ApiModelProperty(value = "Status da permissão", position = 5)
     private StatusPermissaoEnum statusDescricao;
     
     @ApiModelProperty(value = "Id do usuário que realizou o cadastro", position = 6)
     private Long idUsuarioRegistro;
     
     @ApiModelProperty(value = "Dados do grupo empresa", position = 7)
     private GrupoEmpresa grupoEmpresa;
}

