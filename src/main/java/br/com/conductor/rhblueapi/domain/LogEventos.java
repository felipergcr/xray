
package br.com.conductor.rhblueapi.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * LogEventos
 */
@Builder
@Entity
@Data
@AllArgsConstructor
@Table(name = "LOGEVENTOS")
public class LogEventos implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_LOGEVENTO")
     private Long id;

     @Column(name = "ID_TABELA")
     private Integer idTabela;

     @Column(name = "NOMECAMPO")
     private String nomeCampo;

     @Column(name = "VALORANTERIOR")
     private String valorAnterior;

     @Column(name = "VALORATUAL")
     private String valorAtual;

     @Column(name = "DATAMODIFICACAO")
     private LocalDateTime dataModificacao;

     @Column(name = "MODIFICADOPOR")
     private String modificadoPor;

     @Column(name = "PROCESSO")
     private String processo;

     @Column(name = "CHAVE")
     private String chave;

     @Column(name = "ID_SESSAO")
     private Integer idSessao;

     @Column(name = "ID_ATENDIMENTO")
     private Integer idAtendimento;

     public LogEventos(Integer idTabela, String nomeCampo, String chave, String valorAnterior, String valorAtual, String modificadoPor, String processo) {
          this.idTabela = idTabela;
          this.nomeCampo = nomeCampo;
          this.chave = chave;
          this.valorAnterior = valorAnterior;
          this.valorAtual = valorAtual;
          this.modificadoPor = modificadoPor;
          this.processo = processo;
          this.dataModificacao = LocalDateTime.now();
     }


     public LogEventos(Integer idTabela, String nomeCampo, String chave, String valorAnterior, String valorAtual, String modificadoPor, String processo, LocalDateTime dataModificacao) {
          this(idTabela, nomeCampo, chave, valorAnterior, valorAtual, modificadoPor, processo);
          this.dataModificacao = dataModificacao;
     }
}
