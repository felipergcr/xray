package br.com.conductor.rhblueapi.domain.blue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Produto
 */
@ApiModel
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Produto implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "Id Produto", position = 1, example = "1")
    private Long id;

    /**
     * nome
     */
    @ApiModelProperty(value = "Nome", position = 2, example = "Vale Refeição")
    private String nome;

    /**
     * descricao
     */
    @ApiModelProperty(value = "Descrição", position = 3, example = "Vale Refeição")
    private String descricao;

}
