
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ACESSONIVEISUSUARIO")
public class AcessoNiveisUsuario implements Serializable {

     /**
     * 
     */
     private static final long serialVersionUID = 1L;
     
     @Id
     @GeneratedValue(strategy=GenerationType.IDENTITY)
     @Column(name = "ID_NIVELUSUARIO")
     private Long idNivelUsuario;
     
     @Column(name = "NIVEL")
     private Integer  nivelPerfil;
     
     @Column(name = "DESCRICAONIVEL")
     private String  descricaoNivelPefil;
     
     @Column(name = "ID_PLATAFORMA")
     private Integer idPlataforma;
     
}
