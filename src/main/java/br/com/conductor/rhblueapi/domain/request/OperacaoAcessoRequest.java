
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "Operações de sistema")
@Getter
@Setter
public class OperacaoAcessoRequest implements Serializable {

     private static final long serialVersionUID = 1563934387082467416L;

     @ApiModelProperty(value = "Id da operação", position = 1)
     private Long id;

     @ApiModelProperty(value = "Nome da Operação, Ex: Editar Perfil de Acesso", position = 2)
     @Size(max = 100, message = "Limite de caracteres excedido")
     private String nomeOperacao;

     @ApiModelProperty(value = "identificador da plataforma, Ex: 16", position = 3)
     private Long idPlataforma;

     @ApiModelProperty(value = "Agrupamento lógico de operações, Ex: 1", position = 4)
     private Long idGrupoOperacoesAcessos;

     @ApiModelProperty(value = "Ordem de exibição das operações, Ex: 1", position = 5)
     private Integer ordem;

     @ApiModelProperty(value = "Índice de página, Ex: 0", position = 6)
     private Integer page;

     @ApiModelProperty(value = "Tamanho da página, Ex: 50", position = 7)
     private Integer size;
}
