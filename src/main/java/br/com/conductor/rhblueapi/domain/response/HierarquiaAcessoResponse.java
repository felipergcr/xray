package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class HierarquiaAcessoResponse implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1916029197570181696L;
     
     @ApiModelProperty(value="Nome do grupo de empresa pai.", example="Grupo Empresa Santander", position = 1)
     private HierarquiaAcessoGrupoEmpresaResponse grupo;
     
  
}
