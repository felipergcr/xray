
package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "UsuarioNivelPermissaoRhPersist", description = "UsuarioNivelPermissaoRhPersist")
public class UsuarioNivelPermissaoRhPersist implements Serializable {

     private static final long serialVersionUID = 1L;
     
     @ApiModelProperty(value = "Nível de acesso da permissão", required = true, position = 1)
     @NotNull(message = "não pode ser null")
     private NivelPermissaoEnum nivelAcesso;

     @ApiModelProperty(value = "Identificador do nível de acesso, ex(1)", required = true, position = 2)
     @NotNull(message = "não pode ser null")
     private List<Long> idNivelAcesso;

     @ApiModelProperty(value = "Identificador do usuário responsável pela ação, ex(1231)", required = true, position = 3)
     @NotNull(message = "não pode ser null")
     private Long idUsuarioRegistro;

}
