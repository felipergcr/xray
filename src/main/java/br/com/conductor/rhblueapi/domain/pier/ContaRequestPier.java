
package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContaRequestPier implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     private Long idPessoa;

     private Long idOrigemComercial;

     private Long idProduto;
     
     private Integer diaVencimento;
     
     private BigDecimal valorRenda;
     
     private String canalEntrada;
     
     private Integer valorPontuacao;
     
     private Long idEnderecoCorrespondencia;
     
     private BigDecimal limiteGlobal;
     
     private BigDecimal limiteMaximo;
     
     private BigDecimal limiteParcelas;
     
     private BigDecimal limiteConsignado;
     
     private Integer flagFaturaPorEmail;
     
     
     
     

}
