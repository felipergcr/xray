
package br.com.conductor.rhblueapi.domain.deloitte;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NotaFiscalDeloitte {

     private String mensagem;

     private byte[] pdfFile;

     private String nomeArquivo;

}
