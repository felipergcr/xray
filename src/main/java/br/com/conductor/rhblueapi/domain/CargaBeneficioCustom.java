
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CargaBeneficioCustom implements Serializable {

     private static final long serialVersionUID = 5402907252031395161L;

     @Id
     @Column(name = "ID")
     @JsonIgnore
     private BigInteger id;

     @Column(name = "ID_ARQUIVOCARGASTD")
     private Long idPedido;

     @Column(name = "ID_CARGABENEFICIO")
     private Long idCargaBeneficio;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;

     @Column(name = "CPF")
     private String cnpj;

     @Column(name = "VALOR")
     private BigDecimal valorTotal;

     @Column(name = "STATUSARQUIVOCARGA")
     private Integer statusArquivoCarga;

     @Column(name = "STATUS")
     private Integer statusCargaBeneficio;

     @Transient
     private String status;

     @Column(name = "FLAGFATURAMENTOCENTRALIZADO")
     private Integer flagFaturamentoCentralizado;

     @Column(name = "DATAAGENDAMENTO")
     private LocalDate dataAgendamento;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Column(name = "QTDFUNC")
     private Integer qtdFuncionarios;

}
