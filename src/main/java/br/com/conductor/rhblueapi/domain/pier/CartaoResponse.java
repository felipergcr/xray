package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.conductor.rhblueapi.domain.ContaSaldo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Getter @Setter
@ApiModel(description = "Campos do objeto cartão")
public class CartaoResponse extends ResourceSupport implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "Identificador do cartão", example = "25")
    @JsonProperty("id")
    private Long idCartao;

    @ApiModelProperty(value = "Identificador da pessoa", example = "20")
    private Long idPessoa;

    @ApiModelProperty(value = "Identificador da conta", example = "10")
    private Long idConta;

    @ApiModelProperty(value = "Identificador do status", example = "2")
    private Long idStatus;

    @ApiModelProperty(value = "Nome do status", example = "Bloqueado")
    private String nomeStatus;

    @ApiModelProperty(value = "Identificador do produto", example = "10")
    private Long idProduto;

    @ApiModelProperty(value = "Data de alteração do status - yyyy-MM-dd'T'HH:mm:ss.SSSZ", example = "2018-01-01'T'01:02:03.123Z")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataStatus;

    @ApiModelProperty(value = "Data da emissão do cartão - yyyy-MM-dd'T'HH:mm:ss.SSSZ", example = "2018-01-01'T'01:02:03.123Z")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataEmissao;

    @ApiModelProperty(value = "Data de validade - yyyy-MM-dd'T'HH:mm:ss.SSSZ", example = "2018-01-01'T'01:02:03.123Z")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataValidade;

    @ApiModelProperty(value = "Data criação do cartão - yyyy-MM-dd'T'HH:mm:ss.SSSZ", example = "2018-01-01'T'01:02:03.123Z")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataGeracao;

    @ApiModelProperty(value = "Nome impresso no cartão", example = "DOM PEDRO PRIMEIRO")
    private String nomeImpresso;

    @ApiModelProperty(value = "Tipo do portador", example = "T")
    private String tipoPortador;

    @ApiModelProperty(value = "numeroCartao", notes = "numero do cartao", required = false, example = "12345678")
    private String numeroCartao;

    @ApiModelProperty(value = "Nome da Empresa", example = "Ben")
    private String nomeEmpresa;

    @ApiModelProperty(value = "Cnpj da empresa", example = "36616048000176")
    private String cnpj;

    @ApiModelProperty(value = "SaldoCartao",  notes = "Saldo do Cartão", example = "25.00")
    private BigDecimal saldo;

    @ApiModelProperty(value = "ContaSaldo",  notes = "Informações sobre o saldo da conta do cartão")
    private ContaSaldo saldoConta;

    @ApiModelProperty(value = "Código de rastreio",  notes = "Código de rastreio do cartão.")
    private String codigoRastreio;

}
