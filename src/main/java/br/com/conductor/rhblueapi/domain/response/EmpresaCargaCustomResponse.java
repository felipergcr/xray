
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel
@Getter
@Setter
@NoArgsConstructor
public class EmpresaCargaCustomResponse extends PageResponse<EmpresaCargaCustomResponse> implements Serializable {

     private static final long serialVersionUID = -3264105712719861258L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public EmpresaCargaCustomResponse(PageResponse p) {

          super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last, p.nextPage, p.previousPage, p.content);
     }
}