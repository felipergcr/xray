
package br.com.conductor.rhblueapi.domain.request.relatorios;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Relatorio filter")  
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RelatorioFilter implements Serializable {
     
     private static final long serialVersionUID = 1L;
     
     @ApiModelProperty(value = "Data início do período", position = 1, required = true, example = "2019-10-01")
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @NotNull(message = "A data de início não pode ser nula")  
     private LocalDate dataInicio;
     
     @ApiModelProperty(value = "Data fim do período", position = 2, required = true, example = "2019-10-01")
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @NotNull(message = "A data final não pode ser nula")
     private LocalDate dataFim;
     
     @ApiModelProperty(value = "Identificação de usuário", position = 3, required = true, example = "1")
     @NotNull(message = "O identificador do usuário não pode ser nulo")
     private Long idUsuario;

     @ApiModelProperty(value = "Identificação do grupo empresa", position = 4, required = true, example = "1")
     @NotNull(message = "O identificador do grupo empresa não pode ser nulo")
     private Long idGrupoEmpresa;
     
     @ApiModelProperty(value = "Número da página", position = 5, required = true, example = "0")
     @NotNull
     @Min(value = 0, message = "O valor do número da página não pode ser menor que 0")
     private int numberPage;
     
     @ApiModelProperty(value = "Tamanho da página", position = 6, required = true, example = "1")
     @NotNull
     @Min(value = 1, message = "O valor do tamanho da página não pode ser menor que 1")
     private int sizePage;

}
