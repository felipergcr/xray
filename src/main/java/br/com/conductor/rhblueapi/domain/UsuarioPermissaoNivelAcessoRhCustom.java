package br.com.conductor.rhblueapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioPermissaoNivelAcessoRhCustom {
     
     @Id
     @Column(name = "ID_NIVEISPERMISSOES")
     private Long idNivelPermissao;

     @Column(name = "ID_PERMISSAO")
     private Long idPermissao;
     
     @Column(name = "STATUS")
     private Integer status;
     
     @Column(name = "ID_USUARIO_REGISTRO")
     private Long idUsuarioRegistro;
     
     @Column(name = "STATUS_PERMISSAO")
     private Integer statusPermissao;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;
     
     @Column(name = "NOME_GRUPOEMPRESA")
     private String nomeGrupoEmpresa;

     @Column(name = "ID_SUBGRUPOEMPRESA")
     private Long idSubgrupoEmpresa;
     
     @Column(name = "NOME_SUBGRUPOEMPRESA")
     private String nomeSubGrupoEmpresa;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;
     
     @Column(name = "NOME_EMPRESA")
     private String nomeEmpresa;
     
     @Column(name = "NOME_USUARIO")
     private String nomeUsuario;
     
     @Column(name = "CPF_USUARIO")
     private String cpfUsuario;
     
}
