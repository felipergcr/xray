package br.com.conductor.rhblueapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class StatusDescricao {

     @Id
     @Column(name = "ID")
     private Integer id;

     @Column(name = "CODIGOSTATUS")
     private Integer codigoStatus;

     @Column(name = "STATUS")
     private Integer status;

     @Column(name = "TIPOSTATUS")
     private String tipoStatus;

     @Column(name = "DESCRICAO")
     private String descricao;

}
