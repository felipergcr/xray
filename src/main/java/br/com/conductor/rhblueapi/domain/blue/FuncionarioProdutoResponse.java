package br.com.conductor.rhblueapi.domain.blue;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description = "Campos de filtro FuncionarioProduto")
@Getter @Setter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FuncionarioProdutoResponse implements Serializable{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "Id Funcionário", position = 1)
    private Long idFuncionariosProdutos;

    @ApiModelProperty(value = "Identificador da empresa", position = 2)
    private Long idFuncionario;

    @ApiModelProperty(value = "Identificador da pessoa", position = 3)
    private Long idConta;

    @ApiModelProperty(value = "Data do cadastro do funcionário", position = 5)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataCadastro;

    @ApiModelProperty(value = "Status do funcionário", position = 6)
    private Long status;

    @ApiModelProperty(value = "Data da atualização do status", position = 7)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime statusData;

    @ApiModelProperty(value = "Valor Default", position = 8)
    private BigDecimal valorDefault;

}
