
package br.com.conductor.rhblueapi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * HistoricoCancelamentoCartao
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "HISTORICOCANCELAMENTOCARTOES")
public class HistoricoCancelamentoCartao implements Serializable{

     private static final long serialVersionUID = 1L;

     @EmbeddedId
     private HistoricoCancelamentoCartaoPK id;

     @Column(name = "ID_CARTAO")
     private Long idCartao;

     @Column(name = "CARTAO", length = 16)
     private String cartao;

     @Column(name = "DESCRICAO")
     private String descricao;

     @Column(name = "RESPONSAVEL", length = 30)
     private String responsavel;
}
