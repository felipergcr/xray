package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import br.com.conductor.rhblueapi.enums.OrderByEnum;
import br.com.conductor.rhblueapi.enums.StatusNivelPermissaoAcessoEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioNivelPermissaoRhRequest implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do nível de acesso, ex(10)" , position = 1)
     private Long idNivelPermissao;

     @ApiModelProperty(value = "Identificador do grupo empresa, ex(5)" , position = 2)
     private Long IdGrupoEmpresa;

     @ApiModelProperty(value = "Identificador do subgrupo, ex(19)" , position = 3)
     private Long IdSubgrupoEmpresa;

     @ApiModelProperty(value = "Identificador da empresa, ex(190)" , position = 4)
     private Long idEmpresa;

     @ApiModelProperty(value = "Identificador do usuário que concedeu acesso, ex(1231)" , position = 5)
     private Long idUsuarioRegistro;
     
     @ApiModelProperty(value = "Identificador de permissão do usuário, ex(1231)" , position = 6)
     private Long idPermissao;
     
     @ApiModelProperty(value = "Status do nível de permissão" , position = 7)
     private StatusNivelPermissaoAcessoEnum status;
     
     @ApiModelProperty(value = "Parâmetro para ordernar lista em ordem alfabética (ex:ASC)" , position = 8)
     private OrderByEnum orderBy = OrderByEnum.ASC;

}