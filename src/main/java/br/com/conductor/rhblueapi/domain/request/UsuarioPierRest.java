package br.com.conductor.rhblueapi.domain.request;

import static br.com.conductor.rhblueapi.util.AppConstantes.PIER_USUARIO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.exception.NotFoundCdt;
import br.com.conductor.rhblueapi.exception.UnauthorizedCdt;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;
import lombok.extern.slf4j.Slf4j;

@Service
@RefreshScope
@Lazy
@Slf4j
public class UsuarioPierRest {

     @Value("${app.pier.host}${app.pier.basepath}")
     private String serverCriarUsuario;


     private String pathAlterarSenha = "/alterar-senha";
     
     private String pathCadastrarSenha = "/cadastrar-senha";

     @Autowired
     private HeadersDefaultPier headersPier;

     private RestTemplate restTemplate;

     private ObjectMapper objectMapper;

     private static final String SENHANOVA = "senhaNova";

     private static final String SENHA_NOVA = "senha_nova";

     private static final String SENHA_ATUAL = "senha_atual";

     private String pathValidarSenha = "/validar-senha";

     private static final String SENHA = "senha";

     private static final String PLATAFORMA = "plataforma";

     @Autowired
     public UsuarioPierRest(RestTemplate restTemplate, ObjectMapper objectMapper) {

          this.restTemplate = restTemplate;
          this.objectMapper = objectMapper;
     }

     /**
      * Realiza chamada para Pier criar o usuário.
      * @param usuario - dados do usuário a ser criado.
      * @return usuário criado.
      */
     public UsuarioResponse criaUsuario(UsuarioRequestPier usuario){

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverCriarUsuario.concat(PIER_USUARIO));
          HttpHeaders headers = headersPier.creatHeaders();
          HttpEntity<?> entity = new HttpEntity<>(usuario, headers);

          try {
               ResponseEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, entity, String.class);
               return objectMapper.readValue(response.getBody(), UsuarioResponse.class);

          } catch (HttpClientErrorException e) {
               log.warn("[rh-api][UsuarioPierRest][criaUsuario] Error ao criar usuario." + e.getResponseBodyAsString());
               throw new BadRequestCdt(e.getResponseBodyAsString());
          } catch (Exception e) {
               log.error("[rh-api][UsuarioPierRest][criaUsuario] Error ao converter retorno do Pier na criação de usuário.", e);
               ExceptionsMessagesCdtEnum.ERRO_AO_CRIAR_USUARIO.raise();               
          } 
          
          return null;
          
     }

     public UsuarioResponse recuperarUsuario(final String cpf, final Integer idPlataforma)
             throws JsonParseException, JsonMappingException, IOException {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverCriarUsuario.concat(PIER_USUARIO));
          builder.queryParam("cpf", cpf);
          builder.queryParam("idPlataforma", idPlataforma);

          HttpHeaders headers = headersPier.creatHeaders();
          HttpEntity<?> entity = new HttpEntity<>(headers);

          try {
               ResponseEntity<PageResponse<UsuarioResponse>> response;
               final ParameterizedTypeReference<PageResponse<UsuarioResponse>> responseType =
                       new ParameterizedTypeReference<PageResponse<UsuarioResponse>>() { };
               response = restTemplate.exchange(builder.queryParam("page", String.valueOf(0)).toUriString(), HttpMethod.GET, entity, responseType);

               return response.getBody().getContent().get(0);

          } catch (HttpClientErrorException e) {
               log.warn("[Blue-api][UsuarioPierRest][recuperarUsuario] Error ao recuperar usuario.");
               if (HttpStatus.NOT_FOUND.equals(e.getStatusCode())) {
                    throw new NotFoundCdt(e.getResponseBodyAsString());
               }
               throw new BadRequestCdt(e.getResponseBodyAsString());
          }

     }

     public HttpStatus alterarSenha(AlterarSenhaRequestPier dadosAlterarSenha) throws JsonParseException, JsonMappingException, IOException {

          return alterarSenhaIdUsuario(dadosAlterarSenha);
     
     }

     private HttpStatus alterarSenhaLogin(final AlterarSenhaRequestPier dadosAlterarSenha) {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverCriarUsuario.concat(PIER_USUARIO))
                  .path(StringUtils.join("/", String.valueOf(dadosAlterarSenha.getLogin()), pathAlterarSenha));
          HttpHeaders headers = headersPier.creatHeaders();
          headers.set(SENHANOVA, dadosAlterarSenha.getSenhaNova());
          HttpEntity<?> entity = new HttpEntity<>(headers);

          try {
               ResponseEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, entity, String.class);
               return response.getStatusCode();

          } catch (HttpClientErrorException e) {
               log.warn("[Blue-api][UsuarioPierRest][alterarSenhaLogin] Error ao alterar a senha do usuario.");
               throw new BadRequestCdt(e.getResponseBodyAsString());
          }
     }

     private HttpStatus alterarSenhaIdUsuario(final AlterarSenhaRequestPier dadosAlterarSenha) {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverCriarUsuario.concat(PIER_USUARIO))
                  .path(StringUtils.join("/", String.valueOf(dadosAlterarSenha.getId()), pathCadastrarSenha));
          HttpHeaders headers = headersPier.creatHeaders();
          headers.set(SENHA_NOVA, dadosAlterarSenha.getSenhaNova());
          HttpEntity<?> entity = new HttpEntity<>(headers);

          try {
               ResponseEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.PUT, entity, String.class);
               return response.getStatusCode();

          } catch (HttpClientErrorException e) {
               log.warn("[Blue-api][UsuarioPierRest][cadastrarSenhaIdUsuario] Error ao cadastrar/alterar a senha do usuario.");
               throw new BadRequestCdt(e.getResponseBodyAsString());
          }
     }

     public List<UsuarioResponse> verificaEmail(String email) throws JsonParseException, JsonMappingException, IOException {
          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverCriarUsuario.concat(PIER_USUARIO)).queryParam("email", email);
          HttpHeaders headers = headersPier.creatHeaders();
          HttpEntity<?> entity = new HttpEntity<>(headers);

          try {
               final List<UsuarioResponse> origins = new ArrayList<UsuarioResponse>();
               ParameterizedTypeReference<PageResponse<UsuarioResponse>> responseType = new ParameterizedTypeReference<PageResponse<UsuarioResponse>>() {
               };
               ResponseEntity<PageResponse<UsuarioResponse>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, responseType);
               origins.addAll(response.getBody().getContent());
               return origins;

          } catch (HttpClientErrorException e) {
               return Collections.emptyList();
          }
     }
     
     public List<UsuarioResponse> consultaUsuario(String login, Integer idPlataforma) {
          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverCriarUsuario.concat(PIER_USUARIO)).queryParam("cpf", login).queryParam("idPlataforma", idPlataforma);
          HttpHeaders headers = headersPier.creatHeaders();
          HttpEntity<?> entity = new HttpEntity<>(headers);

          try {
               final List<UsuarioResponse> origins = new ArrayList<UsuarioResponse>();
               ParameterizedTypeReference<PageResponse<UsuarioResponse>> responseType = new ParameterizedTypeReference<PageResponse<UsuarioResponse>>() {
               };
               ResponseEntity<PageResponse<UsuarioResponse>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, responseType);
               origins.addAll(response.getBody().getContent());
               return origins;

          } catch (HttpClientErrorException e) {
               return Collections.emptyList();
          }
     }
     

     public void validarSenhaLogin(String login, @Valid UsuarioValidarSenhaLoginRequest usuario, String idPlataforma) {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverCriarUsuario.concat(PIER_USUARIO)).path(StringUtils.join("/", String.valueOf(login), pathValidarSenha));

          HttpHeaders headers = headersPier.creatHeaders();
          headers.set(SENHA, usuario.getSenha());
          headers.set(PLATAFORMA, idPlataforma);
          HttpEntity<?> entity = new HttpEntity<>(headers);

          try {
               restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, entity, UsuarioResponse.class);

          } catch (HttpClientErrorException e) {
               if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                    throw new NotFoundCdt(e.getResponseBodyAsString());
               } else if (e.getStatusCode().equals(HttpStatus.FORBIDDEN)) {
                    throw new UnauthorizedCdt(e.getResponseBodyAsString());
               } else {
                    throw new BadRequestCdt(e.getResponseBodyAsString());
               }
          }
     }

}