package br.com.conductor.rhblueapi.domain.view;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "VW_DADOS_EMPRESA_RPS")
public class DadoEmpresaRps implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = -7376407156954007270L;

     @Id
     @Column(name = "numero_rps")
     private Long numeroRps;

     @Column(name = "data_emissao")
     private Date dataEmissao;

     @Column(name = "razao_social")
     private String razaoSocial;

     @Column(name = "numero_receita")
     private String numeroReceita;

     @Column(name = "telefone")
     private String telefone;

     @Column(name = "endereco")
     private String endereco;

     @Column(name = "bairro")
     private String bairro;

     @Column(name = "cidade")
     private String cidade;

     @Column(name = "uf")
     private String uf;

     @Column(name = "cep")
     private String cep;

     @Column(name = "numero_nf")
     private String numeroNF;

     @Column(name = "codigo_autenticidade")
     private String codigoAutenticidade;

     @Column(name = "iss")
     private String iss;

     @Column(name = "pis")
     private String pis;

     @Column(name = "cofins")
     private String cofins;

     @Column(name = "ir")
     private String valorIR;

     @Column(name = "base_calculo")
     private String baseCalculo;

     @Transient
     private Double valorTotalPedido;

     @Transient
     private String valorExtenso;
}
