package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EnderecoResponsePier
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnderecoResponsePier implements Serializable {

     /**
      * serialVersionUID
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "id", name = "Identificador do endereço", position = 1)
     private Long id;

     @ApiModelProperty(value = "idPessoa", name = "Identificador da pessoa", position = 2)
     private Long idPessoa;

     @ApiModelProperty(value = "idTipoEndereco", name = "Identificador tipo do endereço", position = 3)
     private Long idTipoEndereco;

     @ApiModelProperty(value = "cep", name = "CEP do endereço", position = 4)
     private String cep;

     @ApiModelProperty(value = "logradouro", name = "Logradouro do endereço", position = 5)
     private String logradouro;

     @ApiModelProperty(value = "numero", name = "Número do endereço", position = 6)
     private String numero;

     @ApiModelProperty(value = "complemento", name = "Complemento do endereço", position = 7)
     private String complemento;

     @ApiModelProperty(value = "pontoReferencia", name = "Ponto de referência do endereço", position = 8)
     private String pontoReferencia;

     @ApiModelProperty(value = "bairro", name = "Bairro do endereço", position = 9)
     private String bairro;

     @ApiModelProperty(value = "cidade", name = "Cidade do endereço", position = 10)
     private String cidade;

     @ApiModelProperty(value = "uf", name = "UF do endereço", position = 11)
     private String uf;

     @ApiModelProperty(value = "pais", name = "País do endereço", position = 12)
     private String pais;

     @ApiModelProperty(value = "dataInclusao", name = "Data da inclusão do registro", position = 13)
     private LocalDateTime dataInclusao;

     @ApiModelProperty(value = "dataUltimaAtualizacao", name = "Data da última atualização", position = 14)
     private LocalDateTime dataUltimaAtualizacao;

}
