package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response da listagem de erros de um arquivo de pedido")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ArquivoPedidoDetalhesErrosReponse implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     @Id
     @JsonIgnore
     private BigInteger id;
     
     @ApiModelProperty(value="Linha do excel", position = 1)
     private Integer linha;
     
     @ApiModelProperty(value="Descrição do erro", position = 2)
     private String erro;
     
}
