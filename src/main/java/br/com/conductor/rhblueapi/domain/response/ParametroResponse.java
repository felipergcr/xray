package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response da Entidade Parametro")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ParametroResponse implements Serializable{
     
     private static final long serialVersionUID = -6848206109922342975L;
     
     @ApiModelProperty(value = "Id do Parâmetro", position = 1, example="1")
     private Long idParametro;
     
     @ApiModelProperty(value = "Tipo Empresa", position = 2, example="EC")
     private String tipoParametro;
     
     @ApiModelProperty(value = "Codigo de referêcia do Parâmetro", position = 3, example="PAT_Ref_NumMaxRefeicoesDia")
     private String codigo;
     
     @ApiModelProperty(value = "Descrição do Parâmetro", position = 4, example="PAT Refeição - Numero Máximo de Refeições por dia")
     private String descricao;
     
     @ApiModelProperty(value = "Tipo Valor do Parâmetro", position = 5, example="N")
     private String tipoValor;
     
     @ApiModelProperty(value = "Valor default", position = 6, example="100")
     private String valorDefault;

}
