
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "Identificação request")
public class IdentificacaoRequest implements Serializable {

     private static final long serialVersionUID = 1L;

     @NotNull
     @ApiModelProperty(value = "Identificação de usuário", position = 1, required = true, example = "1")
     private Long idUsuario;

     @NotNull
     @ApiModelProperty(value = "Identificação do grupo empresa", position = 2, required = true, example = "1")
     private Long idGrupoEmpresa;

}
