
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PageArquivoUnidadeEntregaDetalhesErrosReponse extends PageResponse<ArquivoUnidadeEntregaDetalheErroReponse> implements Serializable {

     private static final long serialVersionUID = 6980660587675328911L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PageArquivoUnidadeEntregaDetalhesErrosReponse(PageResponse p) {

          super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last, p.nextPage, p.previousPage, p.content);
     }
}
