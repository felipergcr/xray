
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Response Customizado para mostrar se existe arquivo em status Recebido e se existe unidade entrega cadastrado para o grupo empresa")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NotificacoesArquivoUnidadeEntregaResponse implements Serializable {

     /**
      * 
      */  
     private static final long serialVersionUID = 2416869801541245301L;

     @ApiModelProperty(value = "Existe unidades cadastradas para o grupo empresa?", position = 1)
     private Boolean existeUnidadeEntregaCadastrada;

     @ApiModelProperty(value = "Existe arquivo de unidade entrega em status de processamento (0 - Recebido)?", position = 2)
     private Boolean existeArquivoDeUnidadeEntregaEmProcessamento;

}
