package br.com.conductor.rhblueapi.domain.blue;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Getter @Setter
@ApiModel(description = "Campos do objeto Funcionario")
public class FuncionarioRequest implements Serializable{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "Identificador do funcionário", position = 1)
    private Long id;

    @ApiModelProperty(value = "Identificador da empresa", position = 2)
    private Long idEmpresa;

    @ApiModelProperty(value = "Identificador da pessoa", position = 3)
    private Long idPessoa;

    @ApiModelProperty(value = "Identificador do setor", position = 4)
    private Long idSetor;

    @ApiModelProperty(value = "Data do cadastro do funcionário", position = 5)
    private LocalDateTime dataCadastro;

    @ApiModelProperty(value = "Status do funcionário", position = 6)
    private Long status;

    @ApiModelProperty(value = "Data da atualização do status", position = 7)
    private LocalDateTime dataStatus;

}
