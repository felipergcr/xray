
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class HierarquiaOrganizacionalGrupoCustom implements Serializable {

     private static final long serialVersionUID = -5412704173830042700L;

     @Id
     @Column(name = "ID")
     @JsonIgnore
     private BigInteger id;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Column(name = "NOME_GRUPOEMPRESA")
     private String nomeGrupoEmpresa;

     @Column(name = "ID_SUBGRUPOEMPRESA")
     private Long idSubGrupoEmpresa;

     @Column(name = "NOME_SUBGRUPOEMPRESA")
     private String nomeSubGrupoEmpresa;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;

     @Column(name = "NOME_EMPRESA")
     private String nomeEmpresa;

     @Column(name = "CNPJ")
     private String cnpj;

}
