package br.com.conductor.rhblueapi.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ArquivoCargaFinanceiroCustom {
     
     @Id
     @Column(name="ID_ARQUIVOCARGASTD")
     private Long idArquivoCarga;
     
     @Column(name="DATAIMPORTACAO")
     private LocalDateTime dataImportacao;

}
