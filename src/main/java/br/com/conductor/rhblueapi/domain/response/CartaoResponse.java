package br.com.conductor.rhblueapi.domain.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Classe que representa a resposta do recurso Cartão
 */
@Data
@ApiModel(description = "Cartão Response")
public class CartaoResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     /**
      * Código de Identificação do Cartão.
      */
     @ApiModelProperty(value = "Código de Identificação do Cartão", name = "id", position = 1)
     private Long id;

     /**
      * Apresenta o tipo do Portador do cartão, sendo: (1: Titular, 0: Adicional).
      */
     @ApiModelProperty(value = "Apresenta o tipo do Portador do cartão, sendo: (1: Titular, 0: Adicional).", position = 2)
     private Integer flagTitular;

     /**
      * Código de Identificação da Pessoa a qual o cartão pertence.
      */
     @ApiModelProperty(value = "Código de Identificação da Pessoa a qual o cartão pertence.", position = 3)
     private Long idPessoa;

     /**
      * Número sequencial do cartão.
      */
     @ApiModelProperty(value = "Número sequencial do cartão.", position = 4)
     private Integer sequencialCartao;

     /**
      * Código de Identificação da Conta a qual o cartão pertence (id).
      */
     @ApiModelProperty(value = "Código de Identificação da Conta a qual o cartão pertence (id).", position = 5)
     private Long idConta;

     /**
      * Código de Identificação do Status do Cartão (id).
      */
     @ApiModelProperty(name = "Código de Identificação do Status do Cartão (id).", value = "idStatus", position = 6)
     private Long idStatus;

     /**
      * Apresenta a data em que o idStatus atual do cartão fora aplicado, quando houver.
      */
     @ApiModelProperty(value = "Apresenta a data em que o idStatus atual do cartão fora aplicado, quando houver.", example = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", position = 7)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     private LocalDateTime dataStatus;

     /**
      * Código de Identificação do Estágio de Impressão do Cartão (id).
      */
     @ApiModelProperty(name = "Código de Identificação do Estágio de Impressão do Cartão (id).", value = "idEstagio", position = 8)
     private Long idEstagio;

     /**
      * Apresenta a data em que o idEstagio atual do cartão fora aplicado, quando houver.
      */
     @ApiModelProperty(value = "Apresenta a data em que o idEstagio atual do cartão fora aplicado, quando houver.", example = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", position = 9)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     private LocalDateTime dataEstagio;

     /**
      * Número do bin do cartão.
      */
     @ApiModelProperty(value = "Número do bin do cartão.", position = 10)
     private Long numeroBin;

     /**
      * Apresenta o número do cartão em formato mascarado, sendo exibido apenas os 4 primeiros e 4 últimos números do cartão. Os demais são substituídos por X.
      */
     @ApiModelProperty(value = "Apresenta o número do cartão em formato mascarado, sendo exibido apenas os 4 primeiros e 4 últimos números do cartão. Os demais são substituídos por X.", position = 11)
     private String numeroCartao;

     /**
      * Número do cartão hash.
      */
     @ApiModelProperty(value = "Número do cartão hash.", position = 12)
     private Long numeroCartaoHash;

     /**
      * Número do cartão criptografado.
      */
     @ApiModelProperty(value = "Número do cartão criptografado.", position = 13)
     private String numeroCartaoCriptografado;

     /**
      * Apresenta a data de emissão(geração) do cartão.
      */
     @ApiModelProperty(value = "Apresenta a data de emissão(geração) do cartão.", example = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", position = 14)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     private LocalDateTime dataEmissao;

     /**
      * Apresenta a data de validade do cartão em formato MMAAAA, quando houver.
      */
     @ApiModelProperty(value = "Apresenta a data de validade do cartão em formato MMAAAA, quando houver.", example = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", position = 15)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     private LocalDateTime dataValidade;

     /**
      * Apresenta o status que informa se o cartão é virtual.
      */
     @ApiModelProperty(value = "Apresenta o status que informa se o cartão é virtual.", position = 16)
     private Integer cartaoVirtual;

     /**
      * Quando ativa, indica que o cartão fora impresso na Origem Comercial.
      */
     @ApiModelProperty(value = "Quando ativa, indica que o cartão fora impresso na Origem Comercial.", position = 17)
     private Integer impressaoAvulsa;

     /**
      * Apresenta a data em que o cartão fora impresso, caso impressão em loja, ou a data em que ele fora incluído no arquivo para impressão via gráfica.
      */
     @ApiModelProperty(value = "Apresenta a data em que o cartão fora impresso, caso impressão em loja, ou a data em que ele fora incluído no arquivo para impressão via gráfica.", example = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", position = 18)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     private LocalDateTime dataImpressao;

     /**
      * Apresenta o nome do arquivo onde o cartão fora incluído para impressão por uma gráfica, quando houver.
      */
     @ApiModelProperty(value = "Apresenta o nome do arquivo onde o cartão fora incluído para impressão por uma gráfica, quando houver.", position = 19)
     private String nomeArquivoImpressao;

     /**
      * Código de Identificação do Produto a qual o cartão pertence (id).
      */
     @ApiModelProperty(value = "Código de Identificação do Produto a qual o cartão pertence (id).", position = 20)
     private Long idProduto;

     /**
      * Apresenta o nome impresso no cartão.
      */
     @ApiModelProperty(value = "Apresenta o nome impresso no cartão.", position = 21)
     private String nomeImpresso;

     /**
      * Apresenta um código específico para ser utilizado como variável no processo de desbloqueio do cartão para emissores que querem usar esta funcionalidade.
      */
     @ApiModelProperty(value = "Apresenta um código específico para ser utilizado como variável no processo de desbloqueio do cartão para emissores que querem usar esta funcionalidade.", position = 22)
     private String codigoDesbloqueio;

     /**
      * Apresenta o tipo do Portador do cartão, sendo: ('T': Titular, 'A': Adicional).
      */
     @ApiModelProperty(value = "Apresenta o tipo do Portador do cartão, sendo: ('T': Titular, 'A': Adicional).", hidden = true)
     private String tipoPortador;

     /**
      * Código de Identificação do Status do Cartão (id).
      */
     @ApiModelProperty(name = "Código de Identificação do Status do Cartão (id).", value = "idStatusCartao", hidden = true)
     private Long idStatusCartao;

     /**
      * Apresenta a data em que o idStatusCartao atual do cartão fora aplicado, quando houver.
      */
     @ApiModelProperty(value = "Apresenta a data em que o idStatusCartao atual do cartão fora aplicado, quando houver.", example = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", hidden = true)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     private LocalDateTime dataStatusCartao;

     /**
      * Código de Identificação do Estágio de Impressão do Cartão (id).
      */
     @ApiModelProperty(name = "Código de Identificação do Estágio de Impressão do Cartão (id).", value = "idEstagioCartao", hidden = true)
     private Long idEstagioCartao;

     /**
      * Apresenta a data em que o idEstagioCartao atual do cartão fora aplicado, quando houver.
      */
     @ApiModelProperty(value = "Apresenta a data em que o idEstagioCartao atual do cartão fora aplicado, quando houver.", example = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", hidden = true)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     private LocalDateTime dataEstagioCartao;

     /**
      * Apresenta a data em que o cartão foi gerado.
      */
     @ApiModelProperty(value = "Apresenta a data em que o cartão foi gerado.", example = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", hidden = true)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     private LocalDateTime dataGeracao;

     /**
      * Apresenta o status que informa se o cartão é virtual.
      */
     @ApiModelProperty(value = "Apresenta o status que informa se o cartão é virtual.", hidden = true)
     private Integer flagVirtual;

     /**
      * Quando ativa, indica que o cartão fora impresso na Origem Comercial.
      */
     @ApiModelProperty(value = "Quando ativa, indica que o cartão fora impresso na Origem Comercial.", hidden = true)
     private Integer flagImpressaoOrigemComercial;

     /**
      * Apresenta o nome do arquivo onde o cartão fora incluído para impressão por uma gráfica, quando houver.
      */
     @ApiModelProperty(value = "Apresenta o nome do arquivo onde o cartão fora incluído para impressão por uma gráfica, quando houver.", hidden = true)
     private String arquivoImpressao;
}