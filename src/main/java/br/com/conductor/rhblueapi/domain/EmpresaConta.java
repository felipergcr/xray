package br.com.conductor.rhblueapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "EMPRESASCONTAS")
public class EmpresaConta {
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_EMPRESACONTA")
     private Long idEmpresaConta;
     
     @Column(name = "ID_CONTA")
     private Long idConta;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;
}
