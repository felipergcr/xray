package br.com.conductor.rhblueapi.domain.response;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response customisado da Empresa")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class EmpresaCustomResponse {
     
     @ApiModelProperty(value = "Identificador da empresa",example= "1", position = 1)
     private Long id;

     @ApiModelProperty(value = "Objeto Pessoa com dados de ", position = 2)
     private PessoaEmpresaResponse pessoa;
     
     @ApiModelProperty(value="Descrição do ramo de atividade da empresa", example="Ramo alimenticio", position=3)
     private String descricao;
     
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE_TIME)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @ApiModelProperty(value="Data de cadastro da empresa", example="2018-05-01", position=4)
     private LocalDateTime dataCadastro;

     @ApiModelProperty(value = "Nome da empresa", example= "Empresa teste" , position = 5)
     private String nomeExibicao;
     
     @ApiModelProperty(value="Status da Empresa", example="1", position=6)
     private Integer status;
     
     @ApiModelProperty(value="Flag para identificar se a empresa é a matriz", example ="0", position = 7)
     private Integer flagMatriz;
     
     @ApiModelProperty(value = "Identificador do Grupo Empresa", example= "123", position = 8)
     private Long idGrupoEmpresa;

     @ApiModelProperty(value="Identifica qual é a empresa matriz do grupo", example="42", position=9)
     private Long idEmpresaPai;
     
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @ApiModelProperty(value= "Data de contrato", example="2018-06-28", position=10)
     private LocalDate dataContrato;
     
     @ApiModelProperty(value="Nome do contato da empresa", example="Célia", position=11)
     private String nomeContato;
     
     @ApiModelProperty(value="CPF do contato", example="75666985833", position=12)
     private String cpfContato;
     
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @ApiModelProperty(value="Data Nascimento do contato", example="1976-10-10", position=13)
     private LocalDate dataNascimentoContato;
     
     @ApiModelProperty(value="Email do contato", example="teste@teste.com.br", position=14)
     private String emailContato;
     
     @ApiModelProperty(value="DDD do telefone de contato", example="11", position=15)
     private String dddTelContato;
     
     @ApiModelProperty(value="Numero do telefone de contato", example="48759658", position=16)
     private String numTelContato;
     
     @ApiModelProperty(value="Dados do banco", example="341", position=17)
     private Long banco;
     
     @ApiModelProperty(value="Dados Agencia", example="0896", position=18)
     private String agencia;
     
     @ApiModelProperty(value="Numero da conta corrente", example="58694", position=19)
     private Long contaCorrente;
     
     @Column(name = "DVCONTACORRENTE")
     @ApiModelProperty(value = "Identificador da Conta", example= "1" , position = 20)
     private String dvContaCorrente;

}
