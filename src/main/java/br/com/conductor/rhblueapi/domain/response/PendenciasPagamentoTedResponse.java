
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Response com as informações para pagamento de pendências TED")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PendenciasPagamentoTedResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Nome da empresa", position = 1, example = "Ben Benefícios e Serviços S/A")
     private String nome;

     @ApiModelProperty(value = "CNPJ da empresa", position = 2, example = "30.798.783/0001-61\n")
     private String cnpj;

     @ApiModelProperty(value = "Banco da empresa", position = 3, example = "Banco Santander")
     private String banco;

     @ApiModelProperty(value = "Agência da empresa", position = 4, example = "2271")
     private String agencia;

     @ApiModelProperty(value = "Conta da empresa", position = 5, example = "130108654")
     private String conta;

     @ApiModelProperty(value = "Número de pedido com pendência", position = 6, example = "1010")
     private Integer numeroPedido;

}