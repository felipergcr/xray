
package br.com.conductor.rhblueapi.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * PortadorPK
 */
@Data
@Embeddable
public class PortadorPK implements Serializable {

     private static final long serialVersionUID = 1L;

     @Column(name = "ID_CONTA")
     private Long idConta;

     @Column(name = "PORTADOR")
     private Integer portador;
}
