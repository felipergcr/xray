
package br.com.conductor.rhblueapi.domain.deloitte;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class IdentificacaoDeloitte {

     @JsonProperty("userID")
     private String userId;

     @JsonProperty("accessKey")
     private String accessKey;

}
