package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PageOperacaoAcessoResponse extends PageResponse<OperacaoAcessoResponse> implements Serializable {

     private static final long serialVersionUID = 6164923782796389853L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PageOperacaoAcessoResponse(PageResponse p) {

          super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last, p.nextPage, p.previousPage, p.content);
     }
}
