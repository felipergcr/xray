
package br.com.conductor.rhblueapi.domain.update;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.conductor.rhblueapi.enums.ModeloDePedido;
import br.com.conductor.rhblueapi.enums.SegmentoEmpresaEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ModeloDeCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoEntregaCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPagamento;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPedido;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoProduto;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TransferenciaProdutos;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(value=Include.NON_NULL)
@ApiModel(value="Objeto de atualização do grupo empresa")
public class GrupoEmpresaUpdate implements Serializable {

     private static final long serialVersionUID = -3158183851400533947L;
     
     @ApiModelProperty(value = "Nome do Grupo", example = "Group S.A", position = 1)
     @Size(min = 5, max = 100)
     @NotEmpty(message = "não pode ser vazio")
     private String nomeGrupo;
     
     @ApiModelProperty(value = "Identificador da empresa principal ", example = "14", position = 2)
     @NotNull(message = "não pode ser null")
     private Long idEmpresaPrincipal;
    
     @ApiModelProperty(value = "Flag que indica o tipo de faturamento", example = "CENTRALIZADO", position = 3)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private TipoFaturamentoEnum tipoFaturamento;
    
     @ApiModelProperty(value = "Tipo do contrato", example = "POS", position = 4)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private TipoContratoEnum tipoContrato;
     
     @ApiModelProperty(value = "Prazo de pagamento", example = "90", position = 5)
     @NotNull(message = "não pode ser null")
     @Max(value = 90, message = "O prazo de pagamento não pode ser maior que 90")
     @Min(value = 0, message = "O prazo de pagamento não pode ser menor que 0")
     private Integer prazoPagamento;
     
     @ApiModelProperty(value = "Limite de credito disponivel para grupo empresa", example = "20.10", position = 6)
     @NotNull(message = "não pode ser null")
     private BigDecimal limiteCreditoDisponivel;
     
     @ApiModelProperty(value = "Segmento da Empresa",  example = "E1_DIGITAL", position = 7)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private SegmentoEmpresaEnum segmentoEmpresa;

     @ApiModelProperty(value = "Email para envio da nota fiscal eletrônica",  example = "notafiscal@nota.com.br", position = 8)
     @Email(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "e-mail inválido")
     @NotEmpty(message = "não pode ser vazio")
     private String emailNotaFiscal;

     @ApiModelProperty(value = "Tipo de pedido",  example = "CENTRALIZADO", position = 9)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private TipoPedido tipoPedido;

     @ApiModelProperty(value = "Modelo do cartão", example = "COM_NOME", position =10)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private ModeloDeCartao modeloCartao;

     @ApiModelProperty(value = "Modelo do pedido", example = "WEB", position = 11)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private ModeloDePedido modeloPedido;

     @ApiModelProperty(value = "Tipo de entrega", example = "RH", position = 12)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private TipoEntregaCartao tipoEntrega;

     @ApiModelProperty(value = "Transferencia entre VA e VR", example = "HABILITADA", position = 13)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private TransferenciaProdutos transferencia;
     
     @ApiModelProperty(value = "Flag que indica se transferência entre produto é permitida", example = "BOLETO", position = 14)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private TipoPagamento tipoPagamento;

     @ApiModelProperty(value = "Tipo produto",  example = "[\"VA\",\"VR\"]", position = 15)
     @Enumerated(EnumType.STRING)
     @NotEmpty(message = "não pode ser null")
     private List<TipoProduto> tipoProduto;
     
     @ApiModelProperty(value = "Data da vigência do limite", required = true, example = "2010-01-01", position = 16)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     private LocalDate dataVigenciaLimite;

}
