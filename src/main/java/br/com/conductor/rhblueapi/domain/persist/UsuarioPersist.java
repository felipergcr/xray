package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Dados do usuário a ser cadastrado")
public class UsuarioPersist implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 50, message = "não pode ter mais de 50 caracteres")
     @ApiModelProperty(required = true, value = "Nome do usuário", example = "Donatelo Best", position = 1)
     private String nome;
     
     /**
      * CPF do usuário
      */
     @CPF
     @NotNull
     @ApiModelProperty(required = true, value = "CPF do Usuário", example = " '12345678901' ", position = 2)
     private String cpf;

     /**
      * Email do usuário
      */
     @Email
     @ApiModelProperty(required = true, value = "Email do usuário", example = "teste@teste.com.br", position = 3)
     private String email;

     /**
      * Senha do usuario
      */
     @ApiModelProperty(value = "Senha do usuario, auto-gerado neste recurso", hidden= true, position = 4)
     private String senha;

     /**
      * Ids de empresas
      */
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @ApiModelProperty(required = true, value = "Lista de Identifícador(id de grupo ou ids de subgrupos) dos níveis que o usuário terá acesso", example = "[1,2,3]", position = 5)
     private List<Long> idsNivelAcesso;

     /**
      * Id do usuário logado
      */
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(required = true, value = "Identifícador do usuário logado", example = "12345", position = 6)
     private Long idUsuarioLogado;

     /**
      * Id do grupo empresa.
      */
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(required = true, value = "Identifícador do grupo empresa a qual usuário fará parte", example = "8563", position = 7)
     private Long idGrupoEmpresa; 

     /**
      * Enum que define qual nível de acesso será gerado.
      */
     @ApiModelProperty(value = "Nível de acesso da permissão", required = true, position = 8)
     @NotNull(message = "não pode ser null")
     private NivelPermissaoEnum nivelAcesso;
     
}
