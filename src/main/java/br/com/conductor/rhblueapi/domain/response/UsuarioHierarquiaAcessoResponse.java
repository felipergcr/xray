package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ApiModel(value=" Informações sobre as permissões que um usuário possuí dentro de um grupo")
public class UsuarioHierarquiaAcessoResponse implements Serializable  {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     @ApiModelProperty(value = "Identificador do usuário", example= "123", position=1)
     private Long idUsuario;
     
     @ApiModelProperty(value = "nome", notes = "Nome do usuário", example = "Maria da Paz",  position=2)
     private String nome;
     
     @ApiModelProperty(value = "email", notes = "Email do usuário", example = "email@conductor.com.br",  position=3)
     private String email;
     
     @ApiModelProperty(value = "cpf", notes = "CPF do usuário", example = "12345678910",  position=4)
     private String cpf;
     
     @ApiModelProperty(notes = "Identificador de permissão usuário cadastrado",  position=5)
     private HierarquiaAcessoResponse permissao;

}
