
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@ApiModel(description = "Dados de uma conta")
public class ContaResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador de conta.", position = 1)
     private Long id;

     @ApiModelProperty(value = "Identificador do produto.", position = 2)
     private Integer idProduto;

     @ApiModelProperty(value = "Identificador da pessoa.", position = 3)
     private Long idPessoa;

}
