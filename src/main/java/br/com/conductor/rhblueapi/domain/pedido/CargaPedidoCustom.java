
package br.com.conductor.rhblueapi.domain.pedido;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CargaPedidoCustom implements Serializable {
     
     private static final long serialVersionUID = 2983653442612494330L;

     @Id
     @Column(name="NUMERO_PEDIDO")
     private Long numeroPedido;
     
     @Column(name="STATUS_PAGAMENTO_PEDIDO")
     private Integer statusPagamentoPedido;
     
     @Column(name="ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;
     
     @Column(name="VALOR_CARGA")
     private BigDecimal valorCarga;

}
