
package br.com.conductor.rhblueapi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;

import static br.com.conductor.rhblueapi.enums.TipoPortador.ADICIONAL;
import static br.com.conductor.rhblueapi.enums.TipoPortador.TITULAR;

/**
 * Portador
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PORTADORES")
public class Portador implements Serializable {

     private static final long serialVersionUID = 1L;

     @EmbeddedId
     private PortadorPK id;

     @Column(name = "ID_PESSOAFISICA")
     private Long idPessoa;

     @Column(name = "CODIGOPARENTESCO")
     private Long idParentesco;

     @Column(name = "NOMEPLASTICO")
     private String nomeImpresso;

     @Column(name = "FLAGATIVO")
     private Integer flagAtivo;

     @Column(name = "DATAINCLUSAO")
     private LocalDateTime dataCadastroPortador;

     @Column(name = "DATACANCELAMENTO")
     private LocalDateTime dataCancelamentoPortador;

     @Column(name = "ID_EMISSOR")
     private Long idEmissor;

     @Column(name = "PARENTESCO")
     private String parentesco;

     @Transient
     private String tipoPortador;

     @PostLoad
     private void carregarTipoPortador() {

          tipoPortador = TITULAR.getCodigo().equals(id.getPortador()) ? TITULAR.getTipo() : ADICIONAL.getTipo();
     }
}
