
package br.com.conductor.rhblueapi.domain;

import static br.com.conductor.rhblueapi.util.AppConstantes.ENDERECO_FLAG_BLOQUEADO_PADRAO;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import br.com.twsoftware.alfred.object.Objeto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "ENDERECOS")
public class Endereco implements Serializable {
     
     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name="ID_ENDERECO")
     private Long id;

     @Column(name = "ID_ESTABELECIMENTO")
     private Long idEstabelecimento;

     @Column(name = "ID_TIPOENDERECO")
     private Integer idTipoEndereco;

     @Column(name="NOMELOGRADOURO")
     private String logradouro;
     
     @Column(name="NUMEROENDERECO")
     private Integer numero;
     
     @Column(name="COMPLEMENTOENDERECO")
     private String complemento;

     @Column(name="BAIRRO")
     private String bairro;

     @Column(name="CEP")
     private String cep;

     @Column(name="CIDADE")
     private String cidade;

     @Column(name="UF")
     private String uf;

     @Column(name="PAIS")
     private String pais;
     
     @Column(name="REFERENCIALOCALIZACAO")
     private String pontoReferencia;
     
     @Column(name="DATAINCLUSAO")
     private LocalDateTime dataInclusao;
     
     @Column(name = "FLAGBLOQUEADO", columnDefinition = "BIT")
     private Boolean flagBloqueado;
     
     @Column(name="DATAULTIMAATUALIZACAO")
     private LocalDateTime dataAtualizacao;
     
     @Column(name="ID_PESSOAFISICA")
     private Long idPessoaFisica;

     @PrePersist
     public void prePersist() {

          this.idEstabelecimento = Objeto.notBlank(this.idEstabelecimento) ? this.idEstabelecimento : 0l;
          this.flagBloqueado = Objeto.notBlank(this.flagBloqueado) ? this.flagBloqueado : ENDERECO_FLAG_BLOQUEADO_PADRAO;
     }

     @PostLoad
     public void trataCampos() {
          if(StringUtils.isNotEmpty(this.logradouro)) {
               this.logradouro = this.logradouro.trim();
          }
          
          if(StringUtils.isNotEmpty(this.complemento)) {
               this.complemento = this.complemento.trim();
          }
          
          if(StringUtils.isNotEmpty(this.cep)) {
               this.cep = this.cep.trim();
          }
          
          if(StringUtils.isNotEmpty(this.bairro)) {
               this.bairro = this.bairro.trim();
          }
          
          if(StringUtils.isNotEmpty(this.cidade)) {
               this.cidade = this.cidade.trim();
          }
     }
}
