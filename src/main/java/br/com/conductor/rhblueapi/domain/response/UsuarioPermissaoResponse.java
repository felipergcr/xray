
package br.com.conductor.rhblueapi.domain.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Response com informações sobre o usuário")
public class UsuarioPermissaoResponse implements java.io.Serializable {

     private static final long serialVersionUID = -901400395305054405L;

     @ApiModelProperty(value = "Identificador do usuário", example = "20", position = 2)
     private Long idUsuario;

     @ApiModelProperty(notes = "Identificador de permissão do usuário cadastrado", example = "1", position = 3)
     private Long idPermissao;

     @ApiModelProperty(notes = "Identificador do nível de Permissão Acesso do usuário cadastrado", example = "1", position = 4)
     private Long idNivelPermissaoAcesso;

}