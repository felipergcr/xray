
package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ProdutosOrigem implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "id", name = "Identificador do produto", position = 1)
     private long idProduto;
}
