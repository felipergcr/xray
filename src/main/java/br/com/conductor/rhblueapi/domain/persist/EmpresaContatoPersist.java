
package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

@ApiModel(description = "Contato da Empresa")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class EmpresaContatoPersist implements Serializable {

     private static final long serialVersionUID = 4547028430073509237L;

     @NotEmpty(message = "não pode ser vazio")
     @ApiModelProperty(value = "Nome do contato da empresa", example = "Daniel Ribeiro", notes = "Nome do contato da empresa", required = true, position = 1)
     @Size(max = 50, message = "Limite de caracteres excedido")
     private String nome;

     @Pattern(regexp = "([0-9]{11})", message = "CPF inválido")
     @NotEmpty(message = "não pode ser vazio")
     @ApiModelProperty(value = "CPF do contato da empresa", example = "\"38077348130\"", notes = "CPF do contato da empresa", required = true, position = 2)
     private String cpf;

     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @ApiModelProperty(value = "Data nascimento do contato", example = "2010-01-01", required = true, position = 3)
     private LocalDate dataNascimento;

     @NotEmpty(message = "não pode ser vazio")
     @Email(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "e-mail inválido")
     @Size(max = 100, message = "Email maior que 100 caracteres")
     @ApiModelProperty(value = "Email do contato master", example = "email@email.com.br", required = true, position = 4)
     private String email;

     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 2, message = "Limite de caracteres excedido")
     @Pattern(regexp = "((([1,4,6,8,9][1-9])|(2[1,2,4,7,8])|(3[1-8])|(4[1-9])|(5[1-5])|(7[1,3,4,5,7,9])))", message = "DDD inválido")
     @ApiModelProperty(value = "DDD do contato da empresa", example = "11", notes = "DDD do contato da empresa", required = true, position = 5)
     private String dddTel;

     @NotEmpty(message = "não pode ser vazio")
     @Pattern(regexp = "[9]{0,1}[1-9]{1}[0-9]{7}", message = "Telefone deve conter de 8 a 10 caracteres.")
     @Size(min = 8, max = 10, message = "Limite deve ser de 8 a 10 caracteres excedido")
     @ApiModelProperty(value = "Telefone do contato da empresa", example = "\"953629457\"", notes = "Telefone do contato da empresa", required = true, position = 6)
     private String numTel;

}
