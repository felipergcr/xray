package br.com.conductor.rhblueapi.domain;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificacaoEmail {
     
     private Long idTemplateNotificacao ;
     private List<String> destinatarios;
     private List<AnexoNotificacaoEmail> anexos;
     private Map<String, Object> parametrosConteudo;

}
