package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PageSubgrupoEmpresaResponse extends PageResponse<SubgrupoEmpresaResponse> implements Serializable {

     /**
      */
     private static final long serialVersionUID = -2613205339320166870L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PageSubgrupoEmpresaResponse(PageResponse p) {
          super(p.getNumber(), 
                    p.size, 
                    p.totalPages, 
                    p.numberOfElements, 
                    p.totalElements, 
                    p.hasContent, 
                    p.first, 
                    p.last, 
                    p.nextPage, 
                    p.previousPage, 
                    p.content);
     }
}
