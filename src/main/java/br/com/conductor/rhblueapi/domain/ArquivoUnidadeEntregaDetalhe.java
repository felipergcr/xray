
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ARQUIVOSUNIDADEENTREGADETALHE")
public class ArquivoUnidadeEntregaDetalhe implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_ARQUIVOUEDETALHE")
     private Long idArquivoDetalhe;

     @Column(name = "ID_ARQUIVOUNIDADEENTREGA")
     private Long idArquivoUE;

     @Column(name = "STATUS")
     private Integer status;

     @Column(name = "CODIGOUNIDADEENTREGA")
     private String codigoUnidadeEntrega;

     @Column(name = "LOGRADOURO")
     private String logradouro;

     @Column(name = "ENDERECO")
     private String endereco;

     @Column(name = "ENDERECONUMERO")
     private String enderecoNumero;

     @Column(name = "ENDERECOCOMPLEMENTO")
     private String enderecoComplemento;

     @Column(name = "BAIRRO")
     private String bairro;

     @Column(name = "CIDADE")
     private String cidade;

     @Column(name = "UF")
     private String uf;

     @Column(name = "CEP")
     private String cep;

     @Column(name = "NOMERESPONSAVEL1")
     private String nomeResponsavel1;

     @Column(name = "TIPODOCUMENTORESPONSAVEL1")
     private String tipoDocumentoResponsavel1;

     @Column(name = "DOCUMENTORESPONSAVEL1")
     private String documentoResponsavel1;

     @Column(name = "TELEFONERESPONSAVEL1")
     private String telefoneResponsavel1;

     @Column(name = "NOMERESPONSAVEL2")
     private String nomeResponsavel2;

     @Column(name = "TIPODOCUMENTORESPONSAVEL2")
     private String tipoDocumentoResponsavel2;

     @Column(name = "DOCUMENTORESPONSAVEL2")
     private String documentoResponsavel2;

     @Column(name = "TELEFONERESPONSAVEL2")
     private String telefoneResponsavel2;
     
     @Column(name = "NUMEROLINHAARQUIVO")
     private Integer numeroLinhaArquivo;

}
