
package br.com.conductor.rhblueapi.domain.status;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CargaBeneficioMinimoCustom implements Serializable {
     
     private static final long serialVersionUID = 1L;
     
     @Id
     @Column(name = "ID")
     private BigInteger id;

     @Column(name = "ID_ARQUIVOCARGASTD")
     private Long idPedido;

     @Column(name = "ID_CARGABENEFICIO")
     private Long idCargaBeneficio;

     @Column(name = "STATUS")
     private Integer statusCargaBeneficio;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

}
