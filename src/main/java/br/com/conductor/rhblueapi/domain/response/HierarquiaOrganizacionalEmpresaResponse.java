
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Campos do objeto hierarquico da Empresa")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class HierarquiaOrganizacionalEmpresaResponse implements Serializable {

     private static final long serialVersionUID = -2748840910055531933L;

     @ApiModelProperty(value = "Id da Empresa", position = 1)
     private Long id;

     @ApiModelProperty(value = "Nome da Empresa", position = 2)
     private String nome;

     @ApiModelProperty(value = "CNPJ da Empresa", position = 3)
     private String cnpj;
}
