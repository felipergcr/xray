
package br.com.conductor.rhblueapi.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@MappedSuperclass
public class PessoaAbstract {

     @Getter
     @Setter
     @Id
     @Column(name = "ID_PESSOAFISICA")
     private Long idPessoa;
}
