
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value = "Resposta de criação de subgrupo")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubgrupoResponse implements Serializable {

     private static final long serialVersionUID = -3158183851400533947L;

     @ApiModelProperty(value = "Identificador do Grupo", example = "10", position = 1)
     private Long idGrupoEmpresaPai;

     @ApiModelProperty(value = "Identificador do usuário", example = "20", position = 2)
     private Long idUsuario;

     @ApiModelProperty(value = "Identificador da empresa", example = "10", position = 3)
     private Long idEmpresa;

     @ApiModelProperty(value = "Identificador de pessoa criada para empresa", example = "30", position = 4)
     private Long idPessoa;

     @ApiModelProperty(value = "Identificador subgrupo", example = "90", position = 5)
     private Long id;

}
