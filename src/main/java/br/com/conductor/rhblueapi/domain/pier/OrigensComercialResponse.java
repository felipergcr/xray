
package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class OrigensComercialResponse implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "id", name = "Identificador content", position = 1)
     private long id;

     @ApiModelProperty(value = "id", name = "Identificador do produto", position = 2)
     private List<ProdutosOrigem> produtosOrigem; 

     @ApiModelProperty(value = "nome", name = "Nome do porduto", position = 3)
     private String nome;

}
