
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.conductor.rhblueapi.enums.ModeloDePedido;
import br.com.conductor.rhblueapi.enums.SegmentoEmpresaEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ModeloDeCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoEntregaCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPagamento;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPedido;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoProduto;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TransferenciaProdutos;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(value = Include.NON_NULL)
public class GrupoEmpresaResponse implements Serializable {

     private static final long serialVersionUID = -3158183851400533947L;

     @ApiModelProperty(value = "Identificador do Grupo", example = "10", position = 1)
     private Long id;

     @ApiModelProperty(value = "Usuários", position = 2)
     private List<UsuarioPermissaoResponse> usuarios;

     @ApiModelProperty(value = "Identificador da empresa", example = "10", position = 5)
     private Long idEmpresa;

     @ApiModelProperty(value = "Identificador de pessoa criada para empresa", example = "30", position = 6)
     private Long idPessoa;

     @ApiModelProperty(value = "Identificador subgrupo", example = "90", position = 7)
     private Long idSubGrupo;

     @ApiModelProperty(value = "Nome do Grupo", example = "Group S.A", position = 8)
     private String nomeGrupo;

     @ApiModelProperty(value = "Identificador da empresa principal", example = "14", position = 9)
     private Long idEmpresaPrincipal;

     @ApiModelProperty(value = "Flag que indica o tipo de faturamento", required = true, example = "CENTRALIZADO", position = 10)
     private TipoFaturamentoEnum tipoFaturamento;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Tipo do contrato", required = true, example = "POS", position = 11)
     private TipoContratoEnum tipoContrato;

     @ApiModelProperty(value = "Prazo de pagamento", required = true, example = "60", position = 12)
     private Integer prazoPagamento;

     @ApiModelProperty(value = "Limite de crédito disponível para grupo empresa", required = true, example = "20.10", position = 13)
     private BigDecimal limiteCreditoDisponivel;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Segmento da Empresa", required = true, example = "E1_DIGITAL", position = 14)
     private SegmentoEmpresaEnum segmentoEmpresa;

     @Email(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "e-mail inválido")
     @ApiModelProperty(value = "Email para envio da nota fiscal eletrônica", required = true, example = "notafiscal@nota.com.br", position = 15)
     private String emailNotaFiscal;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Tipo de pedido", required = true, example = "CENTRALIZADO", position = 16)
     private TipoPedido tipoPedido;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Modelo do cartão", required = true, example = "COM_NOME", position = 17)
     private ModeloDeCartao modeloCartao;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Modelo do pedido", required = true, example = "WEB", position = 18)
     private ModeloDePedido modeloPedido;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Tipo de entrega", required = true, example = "RH", position = 19)
     private TipoEntregaCartao tipoEntrega;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Transferência entre VA e VR", required = true, example = "HABILITADA", position = 20)
     private TransferenciaProdutos transferencia;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Tipo do pagamento", required = true, example = "BOLETO", position = 21)
     private TipoPagamento tipoPagamento;

     @Enumerated(EnumType.STRING)
     @ApiModelProperty(value = "Tipo do produto", required = true, example = "[\"VA\",\"VR\"]", position = 22)
     private List<TipoProduto> tipoProduto;
     
     @ApiModelProperty(value = "Data da vigência do limite", example = "2010-01-01", position = 23)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     private LocalDate dataVigenciaLimite;

}
