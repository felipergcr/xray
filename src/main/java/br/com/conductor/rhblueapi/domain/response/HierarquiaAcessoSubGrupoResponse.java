package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ApiModel(value = "Hierarquia de acesso de subgrupos")
public class HierarquiaAcessoSubGrupoResponse implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do subgrupo a qual usuário pertence", example = "8563", position = 1)
     private Long idSubgrupo;
     
     @ApiModelProperty(value="Nome do Subgrupo",example="Santander",position=2)
     private String nomeDescricao;
     
     @ApiModelProperty(value="Lista de empresas do subgrupo",example="Santander",position=3)
     private List<HierarquiaAcessoEmpresaResponse> empresas; 
     
}
