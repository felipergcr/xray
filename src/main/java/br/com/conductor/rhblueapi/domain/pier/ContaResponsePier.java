package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContaResponsePier implements Serializable{/**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     private Long id;
     
     private Integer idPessoa;

}
