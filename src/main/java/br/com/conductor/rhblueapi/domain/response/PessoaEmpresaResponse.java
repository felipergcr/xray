package br.com.conductor.rhblueapi.domain.response;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response customizado da Pessoa Empresa")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PessoaEmpresaResponse {
     
     @ApiModelProperty(value = "Identificador da pessoa", position = 1)
     private Long idPessoa;
     
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @ApiModelProperty(value = "Data nascimento do funcionário", example="1976-10-10", position = 2)
     private LocalDate dataNascimento;
     
     @ApiModelProperty(value = "Nome do funcionário", position = 3)
     private String nome;
          
     @ApiModelProperty(value = "CPF do funcionário", position = 4)
     @JsonProperty("cnpj")
     private String documento;
     
     @ApiModelProperty(value = "Sexo do funcionário", position = 5)
     private String sexo;
     
     @ApiModelProperty(value = "Identificador do emissor", position = 6)
     private Long idEmissor;
     
     @ApiModelProperty(value = "Identidade funcionário", position = 7)
     private String numeroIdentidade;
     
}
