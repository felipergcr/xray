package br.com.conductor.rhblueapi.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DadosPedidoFinanceiroCustom {
     
     @Id
     @Column(name="ID_ARQUIVOCARGASTD")
     private Long id;
     
     @Column(name="VALOR_TOTAL")
     private BigDecimal valorTotal;
     
     @Column(name="QTD_NOTAS")
     private Integer qtdNotas;
     
}