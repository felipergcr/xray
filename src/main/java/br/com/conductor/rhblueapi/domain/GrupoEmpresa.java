
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "GRUPOSEMPRESAS")
@AllArgsConstructor
@NoArgsConstructor
public class GrupoEmpresa extends GrupoEmpresaAbstract implements Serializable {

     private static final long serialVersionUID = 1L;

     @Getter
     @Setter
     @Column(name = "NOME")
     private String nomeGrupo;

}
