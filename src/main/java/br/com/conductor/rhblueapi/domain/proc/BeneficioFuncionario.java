
package br.com.conductor.rhblueapi.domain.proc;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BeneficioFuncionario implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     private String cpf;

     private String cnpj;

     private String nome;

     private String dataNascimento;

     private String sexo;

     private String numeroIdentidade;

     private String orgaoIdentidade;

     private String estadoIdentidade;

     private LocalDate dataEmissaoIdentidade;

     private String nomeMae;

     private String setor;

     private String matricula;

     private String endereco;

     private Integer numeroEndereco;

     private String complemento;

     private String bairro;

     private String cidade;

     private String cep;

     private String uf;

}
