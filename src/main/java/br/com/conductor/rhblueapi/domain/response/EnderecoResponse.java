package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import br.com.conductor.rhblueapi.enums.TipoEnderecoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "Representa o endereço retornado")
public class EnderecoResponse implements Serializable {

     @ApiModelProperty(value = "Código de Identificação do Endereço (id)", position = 1)
     @NotNull
     private Long id;

     @ApiModelProperty(value = "Código de Identificação da Tipo Endereço (id)", position = 2)
     @NotNull
     private Integer idTipoEndereco;

     @ApiModelProperty(value = "Apresenta o Código de Endereçamento Postal (CEP)", position = 3)
     @Pattern(regexp = "^([0-9]{8})$", message = "")
     private String cep;

     @ApiModelProperty(value = "Apresenta o nome do logradouro", position = 4)
     @NotNull
     private String logradouro;

     @ApiModelProperty(value = "Apresenta o número do endereço", position = 5)
     private Integer numero;

     @ApiModelProperty(value = "Apresenta descriçoes complementares referente ao endereço", position = 6)
     private String complemento;

     @ApiModelProperty(value = "Apresenta a descrição de ponto de referência do endereço", position = 7)
     private String pontoReferencia;

     @ApiModelProperty(value = "Apresenta nome do bairro.", position = 8)
     @NotNull
     private String bairro;

     @ApiModelProperty(value = "Apresenta nome da cidade", position = 9)
     @NotNull
     private String cidade;

     @ApiModelProperty(value = "Apresenta cigla da Unidade Federativa", position = 10)
     @NotNull
     private String uf;

     @ApiModelProperty(value = "Apresenta nome do pais", position = 11)
     private String pais;

     public boolean isCorrespondencia() {
          return idTipoEndereco.equals(TipoEnderecoEnum.CORRESPONDENCIA.getId());
     }
}