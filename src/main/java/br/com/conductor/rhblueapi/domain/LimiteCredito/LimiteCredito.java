
package br.com.conductor.rhblueapi.domain.LimiteCredito;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class LimiteCredito implements Serializable {

     private static final long serialVersionUID = 3406450776787301277L;

     private boolean validaLimite;

     private BigDecimal valorLimiteDisponivel;

     private BigInteger porcentagemUtilizado;

     private BigDecimal valorDisponivel;

     private BigDecimal valorUtilizado;

}
