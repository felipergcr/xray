
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "CARGASBENEFICIOS")
public class CargaBeneficio implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_CARGABENEFICIO")
     private Long idCargaBeneficio;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Column(name = "ID_BOLETO")
     private Long idBoleto;

     @Column(name = "ID_USUARIO")
     private Long idUsuario;

     @Column(name = "ID_ARQUIVOCARGASTD")
     private Long idArquivoCargas;

     @Column(name = "STATUS")
     private Integer status;

     @Column(name = "NOMEARQUIVO")
     private String nomeArquivo;

     @Column(name = "ORIGEM")
     private String origem;

     @Column(name = "DATAPROCESSAMENTO")
     private LocalDateTime dataProcessamento;

     @Column(name = "DATAIMPORTACAO")
     private LocalDateTime dataImportacao;

     @Column(name = "DATAAGENDAMENTO")
     private LocalDateTime dataAgendamento;

     @Column(name = "DATAPROCESSAMENTOCARGA")
     private LocalDateTime dataProcessamentoCarga;
     
     @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
     @JoinColumns({ @JoinColumn(name = "Id_CargaControleFinanceiro", referencedColumnName = "Id_CargaControleFinanceiro") })
     @NotFound(action = NotFoundAction.IGNORE)      
     private CargaControleFinanceiro cargaControleFinanceiro;
     
     @Column(name = "DATACANCELAMENTO")
     private LocalDateTime dataCancelamento;

}
