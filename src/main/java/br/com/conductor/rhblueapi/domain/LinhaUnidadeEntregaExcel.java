package br.com.conductor.rhblueapi.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LinhaUnidadeEntregaExcel {
     
     private ArquivoUnidadeEntregaDetalhe detalhe;
     private boolean isValido;
     private Integer numeroLinhaExcel;
     private List<String> erros;

}
