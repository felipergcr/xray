
package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnderecoAprovadoResponse implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     private Integer id;
}
