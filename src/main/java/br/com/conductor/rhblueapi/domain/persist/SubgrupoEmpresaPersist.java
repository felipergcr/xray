
package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.conductor.rhblueapi.domain.ParametrosPersist;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Dados de subgrupo")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class SubgrupoEmpresaPersist implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 100)
     @ApiModelProperty(value = "Nome do subgrupo", required = true, example = "Pão de Açucar", position = 1)
     private String nomeSubGrupo;
     
     @Valid
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Parâmetros contratuais", required = true, position = 2)
     private ParametrosPersist parametros;

     @Valid
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Condições comerciais", required = true, position = 3)
     private CondicoesComerciaisPersist condicoes;

     /**
      * Empresa Matriz
      */
     @ApiModelProperty(value = "Dados da empresa matriz do subgrupo", required = true, position = 4)
     @Valid
     @NotNull(message = "não pode ser null")
     private EmpresaPersist empresaMatriz;

}
