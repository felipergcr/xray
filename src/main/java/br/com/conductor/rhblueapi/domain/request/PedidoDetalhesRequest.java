
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "Parâmetro  de requisição de Carga ")
public class PedidoDetalhesRequest implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(required = true, value = "Identificador do grupo empresa", position = 1)
     private Long idGrupoEmpresa;

     @ApiModelProperty(required = true, value = " Identificador do usuário da sessão", position = 2)
     private Long idUsuarioSessao;

     @ApiModelProperty(hidden = true, value = "StatusPedido", position = 3)
     private StatusCargaBeneficioEnum statusCarga;

     @JsonIgnore
     @ApiModelProperty(hidden = true)
     private Long idPedido;
     
     @JsonIgnore
     @ApiModelProperty(value="Status da tabela EmpresaCargaDetalheProduto")
     private Integer statusEmpresaCargaDetalheProduto;

     @ApiModelProperty(value = "Identificador da empresa a ser mostrado", position = 4)
     private List<Integer> idEmpresasPermitidas;
     
     @ApiModelProperty(value = "Identificador da carga benefício", position = 5)
     private Long idCargaBeneficio;

}
