
package br.com.conductor.rhblueapi.domain.funcionario;

import java.io.Serializable;
import java.util.Optional;

import br.com.conductor.rhblueapi.domain.Endereco;
import br.com.conductor.rhblueapi.domain.Funcionario;
import br.com.conductor.rhblueapi.domain.PessoaEmpresa;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FuncionarioProcessamentoStep implements Serializable {
     
     private static final long serialVersionUID = 1L;

     private Optional<PessoaEmpresa> optPessoaFisica;
     
     private Optional<Funcionario> optFuncionario;
     
     private Optional<Endereco> optEndereco;
     
}
