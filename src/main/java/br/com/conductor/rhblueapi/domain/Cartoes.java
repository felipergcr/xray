
package br.com.conductor.rhblueapi.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Cartoes
 */
@Builder
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CARTOES")
public class Cartoes implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_CARTAO")
     private Long id;

     @Column(name = "STATUS")
     private Long idStatusCartao;

     @Column(name = "ESTAGIO")
     private Long idEstagioCartao;

     @Column(name = "ID_PESSOAFISICA")
     private Long idPessoa;

     @ManyToOne
     @JoinColumns({ @JoinColumn(name = "ID_CONTA", referencedColumnName = "ID_CONTA"), @JoinColumn(name = "PORTADOR", referencedColumnName = "PORTADOR") })
     @NotFound(action = NotFoundAction.IGNORE)
     private Portador portador;

     @Column(name = "CARTAO")
     private String numeroCartao;

     @Column(name = "DATAEMISSAO")
     private LocalDateTime dataGeracao;

     @Column(name = "STATUSDATA")
     private LocalDateTime dataStatusCartao;

     @Column(name = "DATAESTAGIO")
     private LocalDateTime dataEstagioCartao;

     @Column(name = "DATAVALIDADE")
     private LocalDateTime dataValidade;

     @Column(name = "DATAEMBOSSING")
     private LocalDateTime dataImpressao;

     @Column(name = "ARQUIVOLOTE")
     private String arquivoImpressao;

     @Column(name = "FLAGEMBOSSADOLOJA")
     private Integer flagImpressaoOrigemComercial;

     @Column(name = "FLAGPROVISORIO")
     private Integer flagProvisorio;

     @Column(name = "CODIGODESBLOQUEIO")
     private String codigoDesbloqueio;

     @Column(name = "BIN")
     private Long bin;

     @Column(name = "CONTA")
     private Long conta;

     @Column(name = "CARTAOHASH")
     private Long cartaoHash;

     @Column(name = "SEQUENCIALCARTAO")
     private Integer sequencialCartao;

     @Column(name = "ID_EMISSOR")
     private Long idEmissor;

     @Column(name = "ID_FILIAL")
     private Long idFilial;

     @Column(name = "DIGITOVERIFICADOR1")
     private Integer digitoVerificador1;

     @Column(name = "DIGITOVERIFICADOR2")
     private Integer digitoVerificador2;

     @Column(name = "CODIGOCANCELAMENTO")
     private String codigoCancelamento;

     @Column(name = "CRIPTOGRAFIAHSM")
     private String criptografiaHSM;

     @Column(name = "SEQUENCIALANTERIOR")
     private Integer sequencialAnterior;

     @Column(name = "ID_BINSCHAVE")
     private Long idBinsChave;

     @Column(name = "NUMEROCARTAOORIGINAL")
     private String numeroCartaoOriginal;

     @Transient
     private String numeroCartaoReal;

     @Transient
     private final Boolean usaCartaoMascarado = true;

     @Column(name = "QTDSENHASINCORRETAS")
     private Integer qtdSenhasIncorretas;
}
