package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@ApiModel
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PessoaResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do funcionário", position = 1)
     private Long idPessoa;

     @ApiModelProperty(value = "Data nascimento do funcionário", position = 2)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     private LocalDate dataNascimento;

     @ApiModelProperty(value = "Documento do funcionário", position = 3)
     private String documento;

     @ApiModelProperty(value = "Sexo do funcionário", position = 4)
     private String sexo;

     @ApiModelProperty(value = "Sexo do funcionário", position = 5)
     private String numeroIdentidade;

     @ApiModelProperty(value = "Orgão expedidor do RG", required = false, position = 7)
     @Size(max = 6)
     private String orgaoExpedidorIdentidade;

     @ApiModelProperty(value = "Unidade Federativa (UF) de onde foi expedido o RG", required = false, position = 8)
     @Size(max = 2)
     private String unidadeFederativaIdentidade;

     @ApiModelProperty(value = "Data emissão RG", required = false, position = 9)
     @DateTimeFormat(iso = ISO.DATE)
     private LocalDate dataEmissaoIdentidade;

     @ApiModelProperty(value = "Nome do funcionário", position = 6)
     private String nome;

     @ApiModelProperty(value = "Identificador do emissor", position = 7)
     private Long idEmissor;

     @ApiModelProperty(value = "Identificador da conta", position = 8)
     private Long idConta;

     @ApiModelProperty(value = "Identificador da lista de contas do funcionário", position = 9)
     private List<ContaResponse> contas;

     @ApiModelProperty(value = "Telefones", position = 10)
     private List<TelefonePessoaResponse> telefones;

     @ApiModelProperty(value = "Endereços", position = 11)
     private List<EnderecoResponse> enderecos;
}