
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import br.com.conductor.rhblueapi.util.AppConstantes;
import br.com.twsoftware.alfred.object.Objeto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Builder
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="PESSOAS") 
@EqualsAndHashCode(exclude= "pessoaFisica")
public class PessoaEmpresa implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_PESSOAFISICA")
     private Long idPessoa;

     @Column(name="DATANASCIMENTO" ,columnDefinition="TIMESTAMP")
     private LocalDate dataNascimento;

     @Column(name="NOME")
     private String nome;

     @Column(name="CPF")
     private String documento;

     @Column(name="SEXO")
     private String sexo;

     @Column(name="ID_EMISSOR")
     private Long idEmissor;

     @Column(name="NUMEROIDENTIDADE")
     private String numeroIdentidade;

     @Column(name = "ORGAOIDENTIDADE")
     private String orgaoExpedidorIdentidade;

     @Column(name = "ESTADOIDENTIDADE")
     private String estadoIdentidade;

     @Column(name = "DATAEMISSAOIDENTIDADE")
     private LocalDate dataEmissaoIdentidade;
     
     @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "pessoaEmpresa")
     private PessoaFisicaCadastro pessoaFisica;
     
     @PrePersist
     public void prePersist() {
          
          this.idEmissor = Objeto.notBlank(this.idEmissor) ? this.idEmissor : AppConstantes.EMISSOR_PADRAO; 
     }
     
     @PostLoad
     public void trataCampos() {
          
          this.numeroIdentidade = Objeto.notBlank(this.numeroIdentidade) ? this.numeroIdentidade.trim() : null;
     }

}
