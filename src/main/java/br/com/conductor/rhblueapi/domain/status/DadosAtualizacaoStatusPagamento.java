
package br.com.conductor.rhblueapi.domain.status;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DadosAtualizacaoStatusPagamento {
     
     private Long idGrupoEmpresa;
     
     private Long numeroPedido;
     
     private List<Integer> statusCargas;
     
     private Integer qtdCargasPedido;
     
     private Integer qtdCargasPedidoPagas;
     
}
