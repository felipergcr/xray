
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ARQUIVOSCARGASBINARIO")
public class ArquivoCargasBinario implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = -2245495944482280987L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_ARQUIVOCARGABINARIO")
     private Long id;

     @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
     @JoinColumns({ @JoinColumn(name = "ID_ARQUIVOCARGASTD", referencedColumnName = "ID_ARQUIVOCARGASTD") })
     @NotFound(action = NotFoundAction.IGNORE)
     private ArquivoCargas arquivoCargas;

     @Lob
     @NotNull
     @Column(name = "ARQUIVO")
     private byte[] arquivo;

}
