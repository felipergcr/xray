
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Response de erros de um arquivo de unidade de entrega")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ArquivoUnidadeEntregaDetalheErroReponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Linha do excel", position = 1)
     private Integer linha;

     @ApiModelProperty(value = "Descrição do erro", position = 2)
     private String erro;

}
