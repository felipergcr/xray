
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "EMPRESASCARGASDETALHESPRODUTOS")
public class EmpresasCargasDetalhesProdutos implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_EMPRESACARGADETALHEPRODUTO")
     private Long id;

     @Column(name = "ID_EMPRESACARGADETALHE")
     private Long idEmpresaCargaDetalhe;

     @Column(name = "ID_FUNCIONARIOSPRODUTOS")
     private Long idFuncionariosProdutos;

     @Column(name = "STATUS")
     private Integer status;

     @Column(name = "FLAGNOVOCARTAO")
     private Integer flagNovoCartao;

     @Column(name = "VALOR")
     private BigDecimal valor;

     @Column(name = "STATUSDATA")
     private LocalDateTime statusData;

}
