
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "ARQUIVOSCARGASDETALHES")
public class ArquivosCargasDetalhes implements Serializable {

     private static final long serialVersionUID = -5555231495014214409L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_DETALHEARQUIVO")
     private Long id;

     @Column(name = "ID_ARQUIVOCARGASTD")
     private Long idArquivoCargas;

     @Column(name = "CNPJ")
     private String cnpj;

     @Column(name = "CODUNIDADE")
     private String codUnidade;

     @Column(name = "MATRICULA")
     private String matricula;

     @Column(name = "NOME")
     private String nome;

     @Column(name="CPF")
     private String cpf;

     @Column(name = "DATANASCIMENTO")
     private String dataNascimento;

     @Column(name="PRODUTO")
     private String produto; 
     
     @Column(name = "VALOR")
     private String valorBeneficio;
     
     @Column(name = "DATACREDITO")
     private String dataCredito;
     
     @Column(name="LOGRADOURO")
     private String logradouro;

     @Column(name="ENDERECO")
     private String endereco;

     @Column(name="COMPLEMENTO")
     private String complemento;
     
     @Column(name = "NUMERO")
     private String numero;

     @Column(name="BAIRRO")
     private String bairro;

     @Column(name="CIDADE")
     private String cidade;
     
     @Column(name="UF")
     private String uf;

     @Column(name="CEP")
     private String cep;
     
     @Column(name = "statusDetalhe")
     private Integer statusDetalhe;
     
     @Column(name="MOTIVO")
     private String motivoErro;
     
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @Column(name="DATAPROCESSAMENTO")
     private LocalDateTime dataProcessamento;

     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @Column(name="DATAAGENDAMENTOCARGA")
     private LocalDate dataAgendamentoCarga;

     @Column(name = "NUMEROLINHAARQUIVO")
     private Integer numeroLinhaArquivo;

}
