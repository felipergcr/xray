
package br.com.conductor.rhblueapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NotaFinanceiroCustom {

     @Id
     @Column(name = "ID")
     private Long id;

     @Column(name = "ID_CARGACONTROLEFINANCEIRO")
     private Long idCargaControleFinanceiro;

     @Column(name = "NUMERONF")
     private String numeroNf;

}
