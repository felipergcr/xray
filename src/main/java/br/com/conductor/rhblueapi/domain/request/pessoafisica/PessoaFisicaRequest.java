
package br.com.conductor.rhblueapi.domain.request.pessoafisica;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class PessoaFisicaRequest implements Serializable {
     
     private static final long serialVersionUID = 1L;
     
     private Long idPessoa;
     
     private Long idEmissor;
     
     private String nome;

     private String documento;

     private LocalDate dataNascimento;

     private String numeroIdentidade;
     
}
