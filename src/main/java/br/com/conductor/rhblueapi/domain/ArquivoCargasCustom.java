package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioResumido;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ArquivoCargasCustom implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @Id
     @Column(name = "ID")
     @JsonIgnore
     private BigInteger idSelecao;
     
     @Column(name="ID_ARQUIVOCARGASTD")
     private Long id;
     
     @Column(name="NOMEARQUIVO")
     private String nome;
     
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATASTATUS")
     private LocalDateTime dataStatus;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAPROCESSAMENTO")
     private LocalDateTime dataProcessamento;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAIMPORTACAO")
     private LocalDateTime dataImportacao;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAAGENDAMENTO")
     private LocalDateTime dataAgendamento;
     
     @Column(name="VALOR")
     private BigDecimal valor;
     
     @Column(name="MOTIVOERRO")
     private String motivo;
     
     @Column(name="STATUS")
     private Long status;
     
     @Column(name="ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;
     
     @Column(name="ID_USUARIOREGISTRO")
     private Long idUsuario;
     
     @Column(name="STATUSPAGAMENTO")
     private Long statusPagamento;
     
     @Column(name="FLAGFATURAMENTOCENTRALIZADO")
     private Integer flagFaturamentoCentralizado;
     
     @Column(name="MENORDATAAGENDAMENTO")
     private LocalDate menorDataAgendamento;
     
     @Transient
     private UsuarioResumido usuarioResumido;
     
     @Transient
     private StatusArquivoEnum statusEnum;

}
