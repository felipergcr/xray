
package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "Classe para persistir dados de operação e permissão de usuário")
public class PermissoesOperacoesRhPersist implements Serializable {

  private static final long serialVersionUID = 1L;

  @NotNull(message = "não pode ser null")
  @ApiModelProperty(value = "Identificador de usuário que concedeu a permissão", example = "5631", position = 1,
      required = true)
  private Long idUsuarioRegistro;

  @ApiModelProperty(value = "Identificador de operação", position = 2, required = true, reference = "List")
  List<Long> IdOperacao;

}
