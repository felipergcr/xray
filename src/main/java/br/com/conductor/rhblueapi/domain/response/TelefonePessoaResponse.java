package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Classe que representa o objeto a ser persistido")
public class TelefonePessoaResponse implements Serializable {

     @ApiModelProperty(value = "Código de Identificação do Endereço (id)", position = 1)
     private Long id;

     @ApiModelProperty(value = "Código de Identificação do Tipo do Telefone (id).", position = 2)
     @NotNull
     private Long idTipoTelefone;

     @ApiModelProperty(value = "Código DDD do telefone (id).", position = 5)
     @Pattern(regexp = "(((0[1,4,6,8,9][1-9])|(02[1,2,4,7,8])|(03[1-8])|(04[1-9])|(05[1-5])|(07[1,3,4,5,7,9])))", message = "Insira um DDD válido.")
     @NotNull
     private String ddd;

     @ApiModelProperty(value = "Número do telefone.", position = 6)
     @Pattern(regexp = "[9]{0,1}[1-9]{1}[0-9]{3}[0-9]{4}", message = "Insira um telefone válido.")
     @NotNull
     private String telefone;

     @ApiModelProperty(value = "Número do ramal.", position = 7)
     @Pattern(regexp = "[0-9]{1,4}", message = "Insira um ramal válido.")
     private String ramal;
}