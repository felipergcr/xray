
package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.conductor.rhblueapi.enums.TipoEnderecoEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class EnderecoAprovadoPersist implements Serializable {

     private static final long serialVersionUID = 1L;

     @NotNull()
     @ApiModelProperty(value = "Id do endereço", example = "123", required = true, position = 1)
     private Long id;

     @NotNull()
     @ApiModelProperty(value = "Id da pessoa associada", example = "123", required = true, position = 2)
     private Long idPessoa;

     @NotNull()
     @ApiModelProperty(value = "Tipo de endereço", example = "1", required = true, position = 3)
     private Integer idTipoEndereco;

     @NotNull()
     @ApiModelProperty(value = "CEP", example = "11222333", required = true, position = 4)
     private String cep;

     @NotNull()
     @ApiModelProperty(value = "Logradouro", example = "Av. Paulista", required = true, position = 5)
     private String logradouro;

     @NotNull()
     @ApiModelProperty(value = "Tipo de endereço", example = "123", required = true, position = 6)
     private Integer numero;

     @ApiModelProperty(value = "Complemento de endereço", example = "sala 1122", position = 7)
     private String complemento;

     @ApiModelProperty(value = "Ponto de referência", position = 8)
     @Size(max = 50)
     private String pontoReferencia;

     @NotNull()
     @ApiModelProperty(value = "Bairro", example = "Centro", required = true, position = 9)
     private String bairro;

     @NotNull()
     @ApiModelProperty(value = "Cidade", example = "São Paulo", required = true, position = 10)
     private String cidade;

     @NotNull()
     @ApiModelProperty(value = "UF", example = "SP", required = true, position = 11)
     private String uf;

     @NotNull()
     @Size(max = 30)
     @ApiModelProperty(value = "País", example = "Brasil", required = true, position = 12)
     private String pais;

     public boolean isCorrespondencia() {
          return idTipoEndereco.equals(TipoEnderecoEnum.CORRESPONDENCIA.getId());
     }
}
