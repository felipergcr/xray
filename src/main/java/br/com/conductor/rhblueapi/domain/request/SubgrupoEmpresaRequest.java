package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Parametros de consulta de Subgrupo Empresa")
public class SubgrupoEmpresaRequest implements Serializable {

     private static final long serialVersionUID = -6100697776609320155L;

     @ApiModelProperty(value = "Nome do Subgrupo da Empresa", position = 1)
     private String nome;
     
     @ApiModelProperty(value = "Identificação de Usuário", position = 2)
     private Integer idUsuario;
     
}
