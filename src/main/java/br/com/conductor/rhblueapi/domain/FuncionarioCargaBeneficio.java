
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class FuncionarioCargaBeneficio implements Serializable {

     /**
     * 
     */
     private static final long serialVersionUID = -8977189800355963909L;

     @Id
     @Column(name = "ID")
     private Integer id;
     
     @Column(name = "ID_FUNCIONARIO")
     private BigInteger idFuncionario;
     
     @Column(name = "NOME_FUNCIONARIO")
     private String nome;

     @Column(name = "CPF")
     private String cpf;

     @Column(name = "PRODUTO_DESCRICAO")
     private String nomeProduto;

     @Column(name = "VALOR_CARGA")
     private BigDecimal valorCarga;

     @Column(name = "ID_PRODUTO")
     private Long idProduto;

     @Column(name = "EMISSAO_CARTAO")
     private String indicadorEmissaoCartao;

     @Column(name = "LOGRADOURO")
     private String logradouro;

     @Column(name = "COMPLEMENTO")
     private String complemento;

     @Column(name = "BAIRRO")
     private String bairro;

     @Column(name = "CIDADE")
     private String cidade;

     @Column(name = "UF")
     private String uf;

     @Column(name = "CEP")
     private String cep;



     
}
