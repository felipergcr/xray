package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Document(collection = "UserToken")
public class UserToken implements Serializable {

	private static final long serialVersionUID = 7706164217528204179L;

	@Id
	private String token;
	
	private String login;

	private Long userId;

	private Boolean statusToken;
		
	private String senhaPadrao;

}
