
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Response Customizado para mostrar do numero do arquivo de unidade de entrega")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UnidadeEntregaResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Número do arquivo de unidade de entrega", position = 1)
     private Long idArquivoUnidadeEntrega;

}
