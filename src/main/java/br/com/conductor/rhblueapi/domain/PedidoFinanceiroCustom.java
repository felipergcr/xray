package br.com.conductor.rhblueapi.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PedidoFinanceiroCustom {

     @Id
     @Column(name="ID")
     private Long id;
     
     @Column(name="ID_ARQUIVOCARGASTD")
     private Long idArquivoCarga;

     @Column(name="ID_CARGACONTROLEFINANCEIRO")
     private Long idCargaControleFinanceiro;
     
     @Column(name="DATAEMISSAORPS")
     private LocalDateTime dataEmissaoRps;
     
     @Column(name="VALOR")
     private BigDecimal valor;
     
     @Column(name="NUMERONF")
     private String numeroNf;
     
     @Column(name="NOME_BENEFICIARIO")
     private String nomeBeneficiario;
     
}
