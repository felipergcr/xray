
package br.com.conductor.rhblueapi.domain.relatorios.response;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Relatório response")
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RelatorioResponse implements Serializable {
     
     private static final long serialVersionUID = 1L;
     
     @ApiModelProperty(value = "Id", position = 1)
     @Id
     @Column(name="ID_RELATORIO")
     private Long id;
     
     @ApiModelProperty(value = "Data solicitação", position = 2)
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name="DATASOLICITACAO")
     private LocalDateTime dataSolicitacao;

     @ApiModelProperty(value = "Data início do período", position = 3)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @Column(name="DATAINICIO")
     private LocalDate dataInicio;

     @ApiModelProperty(value = "Data final do período", position = 4)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @Column(name="DATAFIM")
     private LocalDate dataFim;
     
     @ApiModelProperty(value = "Nome relatório", position = 5)
     @Column(name="NOMERELATORIO")
     private String nomeRelatorio;

     @ApiModelProperty(value = "Status do relatório", position = 6)
     @Transient
     private String status;
     
     @JsonIgnore
     @Column(name="STATUS")
     private Integer statusRelatorio;
     
}
