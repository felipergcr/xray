package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.br.CPF;

import br.com.conductor.rhblueapi.enums.Plataforma;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description="Dados para alterar senha do usuário")
public class UsuarioSenhaUpdateRequest implements Serializable {

     /**
      */
     private static final long serialVersionUID = 1L;

     @CPF
     @ApiModelProperty(value = "CPF do usuário", example = "12345678901", position = 1)
     private String cpf;
     
     @Email
     @Pattern(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "inválido")
     @ApiModelProperty(value = "E-mail do usuário", example = "joao.henrique@dominio.com", position = 3)
     private String email;
     
     @ApiModelProperty(required = true, value = "ESTABELECIOMENTO, RH, PORTADOR", example = "RH", position = 4)
     @NotNull(message = "não pode ser nulo")
     private Plataforma plataforma;
}
