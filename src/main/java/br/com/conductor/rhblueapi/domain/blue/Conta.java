package br.com.conductor.rhblueapi.domain.blue;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Conta
 */
@ApiModel
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Conta implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "Id Conta", position = 1, example = "1")
    private Long id;

    /**
     * idProduto
     */
    @ApiModelProperty(value = "Id do Produto", position = 2, example = "1")
    private Long idProduto;

    /**
     * produto
     */
    @ApiModelProperty(value = "Produto", position = 3)
    private Produto produto;

}
