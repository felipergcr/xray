package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CNPJ;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Dados de Empresa")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class EmpresaPersist implements Serializable {

     private static final long serialVersionUID = 4038780846193609863L;

     /**
      * Descrição
      */
     @NotNull(message = "não pode ser nulo")
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 200, message = "Limite de caracteres excedido")
     @ApiModelProperty(value = "Descrição", example = "Empresa do ramo alimentício", position = 1)
     private String descricao;

     /**
      * Flag Matriz
      */
     @ApiModelProperty(value = "Flag que identifica se a empresa é matriz", hidden = true, example = "0", position = 2)
     private Integer flagMatriz;

     /**
      * CNPJ da empresa
      */
     @NotNull(message = "não pode ser nulo")
     @NotEmpty(message = "não pode ser vazio")
     @CNPJ(message = "CNPJ inválido")
     @ApiModelProperty(dataType = "String", value = "CNPJ da empresa", example = "\"90755142000151\"", required = true, position = 3)
     private String cnpj;

     /**
      * Razão Social
      */
     @NotNull(message = "não pode ser nulo")
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 100)
     @ApiModelProperty(value = "Razão Social da Empresa", example = "Nome Empresa LTDA", required = true, position = 4)
     private String razaoSocial;

     /**
      * Data do Contrato
      */
     @NotNull(message = "não pode ser nulo")
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @ApiModelProperty(value = "Data cadastro contrato", example = "2010-01-01", required = true, position = 5)
     private LocalDate dataContrato;

     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 20, message = "O campo ultrapassou 20 caracteres")
     @ApiModelProperty(value = "Nome de exibição da empresa no cartão", notes = "Nome de exibição da empresa no cartão", example = "Ben Benefícios", required = true, position = 6)
     private String nomeExibicao;

     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 50, message = "Limite de caracteres excedido")
     @ApiModelProperty(value = "Nome fantasia", notes = "Nome fantasia", example = "Ben Visa Vale", required = true, position = 7)
     private String nomeFantasia;

     @ApiModelProperty(value = "Banco da empresa", notes = "Banco da empresa", example = "341", required = true, position = 8)
     private Long banco;

     @Size(max = 4, message = "Limite de caracteres excedido")
     @ApiModelProperty(value = "Agência bancária da empresa", notes = "Dados do endereço da empresa", example = "0983", required = true, position = 9)
     private String agencia;

     @Size(max = 15, message = "Limite de 15 caracteres excedido")
     @ApiModelProperty(value = "Conta corrente empresa", example = "343456", notes = "Dados da conta corrente da empresa", required = true, position = 10)
     private String contaCorrente;

     @Size(max = 2, message = "Limite de caracteres excedido")
     @ApiModelProperty(value = "Digito verificador de conta corrente", example = "09", notes = "Digito verificador conta corrente", required = true, position = 11)
     private String dvContaCorrente;

     @Valid
     @NotNull(message = "não pode ser nulo")
     @ApiModelProperty(value = "Telefone do usuário", notes = "Dados do telefone da empresa", required = true, position = 12)
     private TelefonePersist telefone;

     @ApiModelProperty(value = "Nível de acesso do usuário", notes = "Nível de acesso do usuário", required = true, position = 13, hidden = true)
     private NivelPermissaoEnum nivelPermissaoContato;

     @Valid
     @NotNull(message = "não pode ser nulo")
     @ApiModelProperty(value = "Endereço da empresa", notes = "Dados do endereço da empresa", required = true, position = 14)
     private EnderecoPersist endereco;

     @Valid
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @ApiModelProperty(required = true, value = "Lista de contatos da empresa", position = 15)
     private List<EmpresaContatoPersist> contatos;

}
