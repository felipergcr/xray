package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * ContaGastosMedioDiarioResponse
 *
 */
@Getter
@Setter
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ContaGastosMedioDiarioResponse implements Serializable {

    /** Atributo serialVersionUID */
    private static final long serialVersionUID = 1L;

    /** Atributo menorValor */
    @ApiModelProperty(value = "Menor valor", example = "1.00", position = 1)
    private BigDecimal menorValor;

    /** Atributo mediaGastos */
    @ApiModelProperty(value = "Média de gastos", example = "2.00", position = 2)
    private BigDecimal mediaGastos;

    /** Atributo maiorValor */
    @ApiModelProperty(value = "Maior valor", example = "3.00", position = 3)
    private BigDecimal maiorValor;

}
