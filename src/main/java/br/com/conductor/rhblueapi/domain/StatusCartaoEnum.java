package br.com.conductor.rhblueapi.domain;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusCartaoEnum {

    NORMAL("Normal"),

    BLOQUEADO("Bloqueado"),

    CANCELADO_PERDA("Cancelado Perda"),

    CANCELADO_ROUBO("Cancelado Roubo"),

    CANCELADO_EXTRAVIADO("Cancelado Extraviado"),

    BLOQUEADO_SENHA_INCORRETA("Bloqueado Senha Incorreta"),

    BLOQUEADO_TEMPORARIO("Bloqueado Prevenção");

    @ApiModelProperty(value = "nome", notes = "Nome do status", position = 1)
    public String nome;

    public boolean equalsNome(String nomeStatus) {
        if (equalsIgnoreCase(this.nome, nomeStatus)) {
            return true;
        } else {
            return false;
        }
    }
}
