package br.com.conductor.rhblueapi.domain.persist;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PessoaPersist implements Serializable {

     private Long idPessoa;

     private String nome;

     private String tipo;

     private String documento;

     private LocalDate dataNascimento;

     private String sexo;

     private String numeroIdentidade;

     private String orgaoExpedidorIdentidade;

     private String unidadeFederativaIdentidade;

     private LocalDate dataEmissaoIdentidade;

     private Integer numeroAgencia;

     private String numeroContaCorrente;

     private String email;

     private String nomeImpresso;

     private String nomeEmpresa;

     private BigDecimal valorRenda;

     private String canalEntrada;

     private Integer valorPontuacao;

     private Integer idEstadoCivil;

     private Integer diaVencimento;

     private List<TelefonePessoaAprovadaPersist> telefones;

     private List<EnderecoAprovadoPersist> enderecos;
}
