package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "Parâmetro  de requisição de pedido")
public class ArquivoPedidoRequest implements Serializable{

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     @ApiModelProperty(required=true,value="Identificador do grupo empresa", position=1)
     @NotNull
     private Long idGrupoEmpresa;
     
     @ApiModelProperty(required=true,value=" Identificador do usuário da sessão", position=2)
     @NotNull
     private Long idUsuarioSessao;

     @ApiModelProperty(value="Identificador do Arquivo de carga", position=3)
     private Long idArquivoCarga;
     
     @ApiModelProperty(value = "Identificador da empresa a ser mostrado", position = 4)
     private List<Integer> idEmpresasPermitidas;
     
}
