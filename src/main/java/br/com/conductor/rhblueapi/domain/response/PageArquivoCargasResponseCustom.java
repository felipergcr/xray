package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel
@Getter 
@Setter
@NoArgsConstructor
public class PageArquivoCargasResponseCustom extends PageResponse<ArquivoPedidoResponseCustom> implements Serializable{

     /**
      * 
      */
     private static final long serialVersionUID = 693726136964353898L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PageArquivoCargasResponseCustom(PageResponse p) {

          super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last, p.nextPage, p.previousPage, p.content);
     }
}