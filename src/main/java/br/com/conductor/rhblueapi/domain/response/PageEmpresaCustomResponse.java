package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel
@Getter 
@Setter
public class PageEmpresaCustomResponse extends PageResponse<EmpresaCustomResponse> implements Serializable{

     private static final long serialVersionUID = 6433186402363526040L;

     
     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PageEmpresaCustomResponse(PageResponse p) {

          super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last, p.nextPage, p.previousPage, p.content);
     }

}
