
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class RegistroBoletoCustom implements Serializable {

     private static final long serialVersionUID = -7073349262905113696L;

     @Id
     @Column(name = "ID")
     private BigInteger id;

     @Column(name = "ID_BOLETO")
     private Long idBoleto;

     @Column(name = "PAGADOR_BAIRRO")
     private String pagadorBairro;

     @Column(name = "PAGADOR_CEP")
     private String pagadorCep;

     @Column(name = "PAGADOR_CIDADE")
     private String pagadorCidade;

     @Column(name = "PAGADOR_NOMELOGRADOURO")
     private String pagadorNomeLogradouro;

     @Column(name = "PAGADOR_NUMEROENDERECO")
     private String pagadorNumeroEndereco;

     @Column(name = "PAGADOR_COMPLEMENTOENDERECO")
     private String pagadorComplementoEndereco;

     @Column(name = "PAGADOR_NOME")
     private String pagadorNome;

     @Column(name = "PAGADOR_CNPJ")
     private String pagadorCnpj;

     @Column(name = "PAGADOR_UF")
     private String pagadorUf;

     @Column(name = "TITULO_NOSSONUMERO")
     private Long tituloNossoNumero;

     @Column(name = "VALORBOLETO")
     private BigDecimal valorBoleto;
     
     @Column(name = "DATAEMISSAO")
     private LocalDate dataEmissao;
     
     @Column(name = "DATAAGENDAMENTO")
     private LocalDate dataAgendamento;

}
