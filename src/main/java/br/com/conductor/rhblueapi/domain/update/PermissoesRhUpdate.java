
package br.com.conductor.rhblueapi.domain.update;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import br.com.conductor.rhblueapi.enums.StatusPermissaoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ApiModel(value = "PermissoesRhUpdate", description = "PermissoesRhUpdate")
public class PermissoesRhUpdate implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = -7854819344716340547L;

     /**
      * Id do usuário
      */
     @ApiModelProperty(value = "Id do usuário", example = "144445567", position = 2)
     private Long idUsuario;

     /**
      * Id do grupo empresa
      */
     @ApiModelProperty(value = "Id do grupo empresa", example = "5", position = 3)
     private Long idGrupoEmpresa;

     /**
      * Id do usuário que realizou o cadastro
      */
     @NotNull
     @ApiModelProperty(value = "Id do usuário que realizou o cadastro", example = "12345", position = 4)
     private Long idUsuarioRegistro;

     @NotNull
     @ApiModelProperty(value = "Status da permissão de acesso", example = "ATIVO/INATIVO/BLOQUEADO", position = 5)
     private StatusPermissaoEnum statusDescricao;
     
}