
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Informa os dados do limite de crédito para os grupos que possuem essa informação")
public class LimiteCreditoResponse implements Serializable {

     private static final long serialVersionUID = 3214847945723068490L;

     @ApiModelProperty(value = "Informa se o grupo possui validação de limite de crédito", position = 1, example = "true/false")
     private boolean validaLimite;

     @ApiModelProperty(value = "Valor limite disponível para realizar pedidos", position = 2, example = "100000.54")
     private BigDecimal valorLimiteDisponivel;

     @ApiModelProperty(value = "Informa o percentual utilizado do valor do limite disponível", position = 3, example = "45")
     private BigInteger porcentagemUtilizado;

     @ApiModelProperty(value = "Valor disponível para utilização no pedido", position = 4, example = "4526.84")
     private BigDecimal valorDisponivel;

     @ApiModelProperty(value = "Valor utilizado em pedidos pendentes de pagamento", position = 5, example = "5600.87")
     private BigDecimal valorUtilizado;

}