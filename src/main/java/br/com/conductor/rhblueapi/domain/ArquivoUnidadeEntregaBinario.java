
package br.com.conductor.rhblueapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ARQUIVOSUNIDADEENTREGABINARIO")
public class ArquivoUnidadeEntregaBinario {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_ARQUIVOUEBINARIO")
     private Long id;

     @Column(name = "ID_ARQUIVOUNIDADEENTREGA")
     private Long idArquivoUnidadeEntrega;

     @Lob
     @NotNull
     @Column(name = "ARQUIVO")
     private byte[] arquivo;
}
