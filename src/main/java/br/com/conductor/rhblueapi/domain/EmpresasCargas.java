
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "EMPRESASCARGAS")
public class EmpresasCargas implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_EMPRESACARGA")
     private Long id;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;

     @Column(name = "ID_CARGABENEFICIO")
     private Long idCargaBeneficio;

     @Column(name = "VALOR")
     private BigDecimal valor;

     @Column(name = "QTDENOVOSCARTOES")
     private Integer qtdeNovosCartoes;

}
