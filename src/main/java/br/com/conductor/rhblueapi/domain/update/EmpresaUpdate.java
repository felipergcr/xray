package br.com.conductor.rhblueapi.domain.update;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.conductor.rhblueapi.domain.persist.EnderecoPersist;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EmpresaUpdate
 */
@ApiModel(description = "Dados para atualização de Empresa")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class EmpresaUpdate implements Serializable {

     /**
      * serialVersionUID
      */
     private static final long serialVersionUID = 1L;

     /**
      * Descrição
      */
     @Size(max=50, message="Limite de caracteres excedido")
     @ApiModelProperty(value = "Descrição", example = "Empresa do ramo alimentício", position = 1)
     private String descricao;

     /**
      * Flag Matriz
      */
     @ApiModelProperty(value = "Flag que identifica se a empresa é matriz", hidden = true, example = "0", position = 2)
     private Integer flagMatriz;

     /**
      * Razão Social
      */
     @Size(max = 80)
     @ApiModelProperty(value = "Razão Social da Empresa", example = "Nome Empresa LTDA", required = true, position = 3)
     private String razaoSocial;

     /**
      * Data do Contrato
      */
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso= ISO.DATE)
     @ApiModelProperty(value = "Data cadastro contrato", example = "2010-01-01", required = true, position = 4)
     private LocalDate dataContrato;

     /**
      * Email do contato
      */
     @Email(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "e-mail inválido")
     @Size(max=100 , message="Email maior que 100 caracteres")
     @ApiModelProperty(value = "Email do contato master", example = "email@email.com.br", required = true, position = 5)
     private String emailContato;
     
     /**
      * Data de nascimento do contato
      */
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso= ISO.DATE)
     @ApiModelProperty(value = "Data nasicmento contato", example = "2010-01-01", required = true, position = 6)
     private LocalDate dataNascimentoContato;     

     @Size( max=20, message="O campo ultrapassou 20 caracteres")
     @ApiModelProperty(value = "Nome de exibição da empresa no cartão", notes = "Nome de exibição da empresa no cartão", example = "Ben Benefícios", required = true, position = 7)
     private String nomeExibicao;
     
     @Size(max=50, message="Limite de caracteres excedido")
     @ApiModelProperty(value = "Nome fantasia", notes = "Nome fantasia", example = "Ben Visa Vale", required = true, position = 8)
     private String nomeFantasia;

     @ApiModelProperty(value = "Banco da empresa", notes = "Banco da empresa", example = "341", required = true, position = 9)
     private Long banco;

     @Size(max=4, message="Limite de caracteres excedido")
     @ApiModelProperty(value = "Agência bancária da empresa", notes = "Dados do endereço da empresa", example = "0983", required = true, position = 10)
     private String agencia;

     @Size(max=15, message="Limite de 15 caracteres excedido")
     @ApiModelProperty(value = "Conta corrente empresa", example = "343456", notes = "Dados da conta corrente da empresa", required = true, position = 11)
     private String contaCorrente;

     @Size(max=2, message="Limite de caracteres excedido")
     @ApiModelProperty(value = "Digito verificador de conta corrente", example = "09", notes = "Digito verificador conta corrente", required = true, position = 12)
     private String dvContaCorrente;

     @ApiModelProperty(value = "Nome do contato master da empresa", example = "Daniel Ribeiro", notes = "Nome do contato master da empresa", required = true, position = 13)
     @Size(max=50, message="Limite de caracteres excedido")
     private String nomeContato;

     @CPF(message="CPF invalido")
     @ApiModelProperty(value = "CPF do contato master da empresa", example = "\"38077348130\"", notes = "CPF do contato master da empresa", required = true, position = 14)
     private String cpfContato;

     @Size(max=2, message="Limite de caracteres excedido")
     @Pattern(regexp = "((([1,4,6,8,9][1-9])|(2[1,2,4,7,8])|(3[1-8])|(4[1-9])|(5[1-5])|(7[1,3,4,5,7,9])))", message = "DDD inválido")
     @ApiModelProperty(value = "DDD do contato master da empresa", example = "11", notes = "DDD do contato master da empresa", required = true, position = 15)
     private String dddTelContato;

     @Pattern(regexp = "[9]{0,1}[1-9]{1}[0-9]{8}", message = "Telefone inválido")
     @Size(min=8, max=10, message="Limite deve ser de 8 a 10 caracteres excedido")
     @ApiModelProperty(value = "Telefone do contato master da empresa", example = "\"987290178\"", notes = "Telefone do contato master da empresa", required = true, position = 16)
     private String numTelContato;

     @Valid
     @ApiModelProperty(value = "Telefone do usuário", notes = "Dados do telefone da empresa", required = true, position = 17)
     private TelefoneUpdate telefone;

     @Valid
     @ApiModelProperty(value = "Endereço da empresa", notes = "Dados do endereço da empresa", required = true, position = 18)
     private EnderecoPersist endereco;

     @ApiModelProperty(value = "Status", notes = "Status da empresa", required = true, position = 19, example = "1")
     private Integer status;

}
