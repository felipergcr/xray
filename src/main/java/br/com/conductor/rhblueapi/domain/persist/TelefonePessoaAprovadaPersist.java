
package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TelefonePessoaAprovadaPersist implements Serializable {

     private Integer id;
     
     private Integer idTipoTelefone;

     private String ddd;

     private String telefone;

     private String ramal;
}
