
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response Customizado para mostrar pedido financeiro")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper=false)
public class ArquivoPedidoResponseCustom implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do Pedido", position = 1)
     @JsonProperty("id")
     private Long idArquivoCargaStd;
     
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @ApiModelProperty(value = "Data do pedido", position = 2)
     private LocalDateTime dataImportacao;     
     
     @ApiModelProperty(value = "valor Total Pedido", position = 3)
     private BigDecimal valorTotal;     

     @ApiModelProperty(value = "Quantidade de notas", position = 4)
     private Integer quantidadeNotas;     
     
     @ApiModelProperty(value = "Status do Pedido", position = 5)     
     private Long status;

     @ApiModelProperty(value = "Identificador do grupo empresa", position = 6)
     private Long idGrupoEmpresa;

     @ApiModelProperty(value = "objeto controle financeiro", position = 7)     
     private List<CargaControleFinanceiroResponse> cargaControleFinanceiros;     

}
