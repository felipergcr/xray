
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ARQUIVOSUNIDADEENTREGADETALHEERRO")
public class ArquivoUnidadeEntregaDetalheErro implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_ARQUIVOUEDETALHEERRO")
     private Long id;

     @Column(name = "ID_ARQUIVOUEDETALHE")
     private Long idArquivoUEDetalhe;

     @Column(name = "MOTIVO")
     private String motivo;

}