package br.com.conductor.rhblueapi.domain.blue;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel
@Getter 
@Setter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeriadoResponse implements Serializable {
     
     private static final long serialVersionUID = 8835797293944904489L;

     @ApiModelProperty(value = "Id Feriado", position = 1, example = "1")
     private Long id;

     @ApiModelProperty(value = "Data", position = 2, example = "2018-01-01")
     private LocalDate data;

     @ApiModelProperty(value = "Tipo", position = 3, example = "1")
     private Integer tipo;

     @ApiModelProperty(value = "Descrição", position = 4, example = "Ano novo")
     private String descricao;
     
     @ApiModelProperty(value = "Feriado móvel", position = 5, example = "1")
     private Long idFeriadoMovel;
}
