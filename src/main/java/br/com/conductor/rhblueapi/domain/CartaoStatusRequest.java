package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.conductor.rhblueapi.domain.validator.PreRequisitesCartaoStatusRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@PreRequisitesCartaoStatusRequest
@Getter
@Setter
@ApiModel(value = "CartaoStatusRequest", description = "Request para alteração de status do cartão")
public class CartaoStatusRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "Status do Cartão", notes = "Id do Status do cartão", required = true, example = "CANCELADO_PERDA", position = 1)
    @NotNull
    private StatusCartaoEnum statusCartao;

    @ApiModelProperty(value = "Data inicial do Agendamento. Ex.: 2018-01-01", notes = "Data início para agendamento do status do cartão", required = false, position = 2)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dataAgendamentoInicio;

    @ApiModelProperty(value = "Data final do Agendamento, Ex.: 2018-05-01", notes = "Data fim para agendamento do status do cartão", required = false, position = 3)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dataAgendamentoFim;

    @ApiModelProperty(value = "motivo. Ex.: cancelamento", notes = "Motivo da troca de status", required = false, position = 4)
    @JsonProperty(value = "motivo", defaultValue = "Desbloqueio APP")
    private String motivo;

    @ApiModelProperty(value = "id do usuário, Ex.: 1", notes = "Identificador usuário logado", required = false, position = 5)
    private Long idUsuario;

}
