package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel
@Getter 
@Setter
@NoArgsConstructor
public class PageUsuarioNivelPermissaoRHResponse extends PageResponse<UsuarioNivelPermissaoRhResponse>
    implements Serializable {

  private static final long serialVersionUID = 6164923782796389853L;

  @SuppressWarnings({"rawtypes", "unchecked"})
  public PageUsuarioNivelPermissaoRHResponse(PageResponse p) {

    super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last,
        p.nextPage, p.previousPage, p.content);
  }
}
