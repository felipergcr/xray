
package br.com.conductor.rhblueapi.domain.request.relatorios;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Relatório deleção request")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter  
public class RelatorioDeletarRequest implements Serializable {
     
     private static final long serialVersionUID = 3983504513083365469L;

     @Valid
     @NotNull
     private IdentificacaoRequest identificacao;
     
     @NotEmpty
     @ApiModelProperty(value = "lista dos identificadores a serem excluídos", position = 2, required = true, example = "[12,13,14,22]")
     private List<Long> idsRelatorio;

}
