
package br.com.conductor.rhblueapi.domain.request.registroBoleto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegistroBoletoRequest implements Serializable {

     private static final long serialVersionUID = 8344927551810934735L;

     private Long codigoExterno;

     private RegistroBoletoPagadorRequest pagador;

     private RegistroBoletoTituloRequest titulo;
}
