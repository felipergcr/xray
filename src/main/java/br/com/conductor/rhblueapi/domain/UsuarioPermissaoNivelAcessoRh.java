package br.com.conductor.rhblueapi.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "USUARIOSNIVEISPERMISSOESRH")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioPermissaoNivelAcessoRh {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_NIVEISPERMISSOES")
     private Long idNivelPermissao;

     @Column(name = "ID_PERMISSAO")
     private Long idPermissao;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Column(name = "ID_SUBGRUPOEMPRESA")
     private Long idSubgrupoEmpresa;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;

     @Column(name = "STATUS")
     private Integer status;

     @Column(name = "DATAREGISTRO")
     private LocalDateTime dataRegistro;

     @Column(name = "ID_USUARIO_REGISTRO")
     private Long idUsuarioRegistro;

     @OneToOne(fetch = FetchType.EAGER)
     @JoinColumn(name = "ID_SUBGRUPOEMPRESA", referencedColumnName = "ID_GRUPOEMPRESA", insertable = false, updatable = false)
     @NotFound(action = NotFoundAction.IGNORE)
     private SubgrupoEmpresa subgrupo;

     @OneToOne(fetch = FetchType.EAGER)
     @JoinColumn(name = "ID_GRUPOEMPRESA", referencedColumnName = "ID_GRUPOEMPRESA", insertable = false, updatable = false)
     @NotFound(action = NotFoundAction.IGNORE)
     private GrupoEmpresa grupoEmpresa;

     @OneToOne(fetch = FetchType.EAGER)
     @JoinColumn(name = "ID_EMPRESA", referencedColumnName = "ID_EMPRESA", insertable = false, updatable = false)
     @NotFound(action = NotFoundAction.IGNORE)
     private Empresa empresa;

     @OneToOne(fetch = FetchType.EAGER)
     @JoinColumn(name = "ID_PERMISSAO", referencedColumnName = "ID_PERMISSAO", insertable = false, updatable = false)
     @NotFound(action = NotFoundAction.IGNORE)
     private PermissoesUsuariosRh permissoes;

}
