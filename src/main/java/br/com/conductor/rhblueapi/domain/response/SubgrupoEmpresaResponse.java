
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value = "Resposta de criação de subgrupo")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubgrupoEmpresaResponse implements Serializable {

	   private static final long serialVersionUID = -3158183851400533947L;

	     @ApiModelProperty(value = "Identificador do Grupo", example = "10", position = 1)
	     private Long idGrupoEmpresaPai;

	     @ApiModelProperty(value = "Usuários", position = 2)
		private List<UsuarioPermissaoResponse> usuarios;

	     @ApiModelProperty(value = "Identificador da empresa", example = "10", position = 3)
	     private Long idEmpresaPrincipal;

	     @ApiModelProperty(value = "Identificador de pessoa criada para empresa", example = "30", position = 4)
	     private Long idPessoa;

	     @ApiModelProperty(value = "Identificador subgrupo", example = "90", position = 5)
	     private Long id;

	     @ApiModelProperty(value = "Nome do subgrupo", example = "90", position = 6)
	     private String nomeSubGrupo;

	     @ApiModelProperty(value = "Data cadastro do subgrupo", example = "2018-10-31T12:13:00", position = 7)
	     @JsonSerialize(using = LocalDateTimeSerializer.class)
	     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
	     @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	     @DateTimeFormat(iso = ISO.DATE_TIME)
	     private LocalDateTime dataCadastro;

	     @ApiModelProperty(value = "Codigo externo subgrupo", example = "Codigo", position = 8)
	     private String codigoExterno;


}
