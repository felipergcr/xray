
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import br.com.twsoftware.alfred.object.Objeto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "FUNCIONARIOS")
public class Funcionario implements Serializable {
     
     private static final long serialVersionUID = 1L;
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_FUNCIONARIO")
     private Long id;
     
     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;
     
     @Column(name = "ID_PESSOAFISICA")
     private Long idPessoaFisica;
     
     @Column(name = "ID_EmpresaSetor")
     private Long idEmpresaSetor;
     
     @Column(name="DATACADASTRO")
     private LocalDateTime dataCadastro;
     
     @Column(name="STATUS")
     private Integer status;
     
     @Column(name="STATUSDATA")
     private LocalDateTime dataStatus;
     
     @Column(name = "ID_ENDERECOENTREGA")
     private Long idEnderecoEntrega;
     
     @Column(name = "MATRICULA")
     private String matricula;
     
     @Column(name = "CODIGOUNIDADEENTREGA")
     private String codigoUnidadeEntrega;
     
     @PrePersist
     public void prePersist() {
          
          this.status = Objeto.notBlank(this.status) ? this.status : 1; 
          
     }

}
