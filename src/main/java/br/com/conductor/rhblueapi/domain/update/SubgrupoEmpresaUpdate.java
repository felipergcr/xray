package br.com.conductor.rhblueapi.domain.update;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SubgrupoEmpresaUpdate")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubgrupoEmpresaUpdate implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = -6795100773417890569L;

     @ApiModelProperty(value = "Nome para atualizar Subgrupo", example = "Mini-mercado Jorge S/A", position = 1)
     private String nomeSubGrupo;
     
     @ApiModelProperty(value = "Identificador da Empresa Principal a ser atualizada", example = "7131", position = 2)
     private Long idEmpresaPrincipal;
     
}
