
package br.com.conductor.rhblueapi.domain.update;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(value=Include.NON_NULL)
public class TelefoneUpdate implements Serializable{

     private static final long serialVersionUID = 9212095351628887796L;

     /**
      * DDD do telefone do estabelecimento
      */
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Size(max=2, message="Limite de caracteres excedido")
     @Pattern(regexp = "((([1,4,6,8,9][1-9])|(2[1,2,4,7,8])|(3[1-8])|(4[1-9])|(5[1-5])|(7[1,3,4,5,7,9])))", message = "DDD inválido")
     @ApiModelProperty(value = "DDD", example = "11", required = true, position = 1)
     private String ddd;
     
     /**
      * Número do telefone do estabelecimento
      */
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Pattern(regexp = "[9]{0,1}[1-9]{1}[0-9]{3}[0-9]{4}", message = " Telefone inválido")
     @ApiModelProperty(value = "Telefone", example = "922349807", required = true, position = 2)
     private String telefone;
     
     /**
      * Número do ramal
      */
     @Pattern(regexp = "[0-9]{0,4}", message = "inválido")
     @ApiModelProperty(value = "Ramal", example = "4356", required = false, position = 3)
     private String ramal;
}    
