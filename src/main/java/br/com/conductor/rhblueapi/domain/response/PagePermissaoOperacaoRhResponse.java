package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PagePermissaoOperacaoRhResponse extends PageResponse<PermissaoOperacaoRhResponse> implements Serializable {

     /**
      */
     private static final long serialVersionUID = -2613205339320166870L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PagePermissaoOperacaoRhResponse(PageResponse p) {
          super(p.getNumber(), 
                    p.size, 
                    p.totalPages, 
                    p.numberOfElements, 
                    p.totalElements, 
                    p.hasContent, 
                    p.first, 
                    p.last, 
                    p.nextPage, 
                    p.previousPage, 
                    p.content);
     }
}
