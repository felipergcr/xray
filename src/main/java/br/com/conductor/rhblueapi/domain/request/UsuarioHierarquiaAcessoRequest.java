package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioHierarquiaAcessoRequest implements Serializable{

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     @CPF
     @ApiModelProperty(required = true, value = "CPF do usário pretendido (ex. 77448523201)", position = 1)
     private String cpf;
     
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(required = true, value = "Identificador do grupo empresa a qual usuário pertence (ex. 8563)", position = 2)
     private Long idGrupoEmpresa;

     @NotNull(message = "não pode ser null")
     @ApiModelProperty(required = true, value = "Identificador do usuário logado (ex. 12345)", position = 3)
     private Long idUsuarioLogado;
     
}
