package br.com.conductor.rhblueapi.domain.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.conductor.rhblueapi.domain.CartaoStatusRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PreRequisitesCartaoStatusRequestValidator implements ConstraintValidator<PreRequisitesCartaoStatusRequest, CartaoStatusRequest> {

    @Override
    public void initialize(final PreRequisitesCartaoStatusRequest arg0) {

        log.debug("Validator: {}", arg0);
    }

    @Override
    public boolean isValid(final CartaoStatusRequest validatedClass, final ConstraintValidatorContext arg1) {

        boolean isValid = true;

        if (validatedClass.getDataAgendamentoFim() == null && validatedClass.getDataAgendamentoInicio() != null) {
            isValid = false;
        }

        return isValid;
    }

}
