
package br.com.conductor.rhblueapi.domain.response.funcionario;

import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.conductor.rhblueapi.enums.Estados;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Payload de response de um endereço de um funcionário")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EnderecoFuncionarioResponse implements Serializable {

     private static final long serialVersionUID = 7393141004981821594L;

     @ApiModelProperty(value = "Logradouro", required = true, example = "Avenida das Nações Unidas", position = 1)
     private String logradouro;

     @ApiModelProperty(value = "Número", required = true, example = "12901", position = 2)
     private Integer numero;

     @ApiModelProperty(value = "Complemento", example = "Torre Norte 23 andar", position = 3)
     private String complemento;

     @ApiModelProperty(value = "Bairro", required = true, example = "Vila Olímpia", position = 4)
     private String bairro;

     @ApiModelProperty(value = "Cidade", required = true, example = "São Paulo", position = 5)
     private String cidade;

     @ApiModelProperty(value = "Estados", required = true, example = "SP", position = 6)
     @Enumerated(EnumType.STRING)
     private Estados uf;

     @ApiModelProperty(value = "CEP", required = true, example = "04578000", position = 7)
     private String cep;
}
