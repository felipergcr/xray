
package br.com.conductor.rhblueapi.domain.response.funcionario;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Payload de response para atualização de um funcionário")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FuncionarioAtualizacaoResponse implements Serializable {
     
     private static final long serialVersionUID = -2828144345074873909L;
     
     @ApiModelProperty(value = "Nome", required = true, example = "Bendito Ben", position = 1)
     private String nome;
     
     @ApiModelProperty(value = "Matrícula", required = true, example = "123456789012", position = 2)
     private String matricula;
     
     @ApiModelProperty(value = "Data de nascimento", required = true, example = "2010-01-01", position = 3)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @NotNull(message = "não pode ser null")
     private LocalDate dataNascimento;
     
     @ApiModelProperty(value = "Payload de response do Endereço", required = true, position = 4)
     private EnderecoFuncionarioResponse endereco;

}
