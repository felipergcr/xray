package br.com.conductor.rhblueapi.domain.blue;

import br.com.conductor.rhblueapi.domain.PessoaEmpresa;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@ApiModel
@Getter
@Setter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FuncionarioResponse implements Serializable {

    private static final long serialVersionUID = -6252873387171651569L;

    @ApiModelProperty(value = "Id Funcionário", position = 1, example = "1")
    private Long id;

    @ApiModelProperty(value = "Identificador da empresa", position = 2, example = "2")
    private Long idEmpresa;

    @ApiModelProperty(value = "Entidade pessoa do funcionario", position = 3)
    private PessoaEmpresa pessoa;

    @ApiModelProperty(value = "Identificador do setor", position = 4, example = "3")
    private Long idSetor;

    @ApiModelProperty(value = "Identificador da Conta", position = 5, example = "4")
    private Long idConta;

    @ApiModelProperty(value = "Data do cadastro do funcionário", position = 6, example = "2018-06-28 12:00:00.000")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataCadastro;

    @ApiModelProperty(value = "Status do funcionário", position = 7, example = "1")
    private Long status;

    @ApiModelProperty(value = "Data da atualização do status", position = 8, example = "2018-06-28 12:00:00.000")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataStatus;

    @ApiModelProperty(value = "Identificador da lista de produtos do funcionário", position = 9)
    private List<Long> idsFuncionariosProdutos;

    @ApiModelProperty(value = "Funcionários Produtos", position = 10)
    private List<FuncionariosProdutos> funcionariosProdutos;

}