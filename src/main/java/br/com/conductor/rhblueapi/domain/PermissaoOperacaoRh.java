
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "PERMISSOESOPERACOESRH")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PermissaoOperacaoRh implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_PERMISSAOOPERACAO")
     private Long idPermissaoOperacao;

     @Column(name = "ID_PERMISSAO")
     private Long idPermissao;

     @Column(name = "ID_OPERACAO")
     private Long idOperacao;

     @Column(name = "ID_USUARIO_REGISTRO")
     private Long idUsuarioRegistro;

     @Column(name = "DATAREGISTRO")
     private LocalDateTime dataRegistro;

     @Column(name = "STATUS")
     private Long status;

}
