package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "OPERACOESACESSOS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class OperacaoAcesso implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_OPERACAO")
     private Long id;

     @Column(name = "NOMEOPERACAO")
     private String nomeOperacao;

     @Column(name = "ID_PLATAFORMA")
     private Long idPlataforma;

     @Column(name = "ID_GRUPOSOPERACOESACESSOS")
     private Long idGrupoOperacoesAcessos;

     @Column(name = "ORDEM")
     private Integer ordem;

     @OneToOne(cascade = CascadeType.ALL)
     @JoinColumn(name = "ID_GRUPOSOPERACOESACESSOS", referencedColumnName = "ID_GRUPOSOPERACOESACESSOS", insertable = false, updatable = false)
     private GruposOperacoesAcessos grupoOperacao;

}
