
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Request para alterar dados de usuário no Pier")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class UsuarioAcessoRequest implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(notes = "Login do usuário para acesso", required = false)
     private String login;

     @ApiModelProperty(notes = "Email do usuário", required = false )
     @Email(message = "Email com formato inválido")
     private String email;

     @ApiModelProperty(notes = "Status do usuário", required = false)
     private Integer status;

     @ApiModelProperty(notes = "Flag de bloqueio de acesso", required = false)
     private Boolean bloquearAcesso;

     @ApiModelProperty(notes = "Quantidade de tentativas com senha incorreta", required = false)
     private Integer tentativasIncorretas;

}
