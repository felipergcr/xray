
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class HierarquiaOrganizacionalCustom implements Serializable {

     private static final long serialVersionUID = -262116336314008592L;

     @Id
     @Column(name = "ID")
     @JsonIgnore
     private BigInteger id;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Column(name = "ID_SUBGRUPOEMPRESA")
     private Long idSubgrupoEmpresa;

     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;

}
