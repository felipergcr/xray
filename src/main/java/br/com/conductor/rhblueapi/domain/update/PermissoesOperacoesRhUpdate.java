
package br.com.conductor.rhblueapi.domain.update;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import br.com.conductor.rhblueapi.enums.StatusPermissaoOperacaoRhEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PermissoesOperacoesRhUpdate implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "Identificador de operação", example = "56", position = 1)
  private Long idOperacao;

  @ApiModelProperty(value = "Identificador de permissão", example = "56", position = 2)
  private Long idPermissao;

  @ApiModelProperty(value = "Status de operação", example = "ATIVO", position = 3)
  private StatusPermissaoOperacaoRhEnum status;

  @NotNull(message = "não pode ser null")
  @ApiModelProperty(value = "Identificador de usuário que concedeu a permissão", example = "87", position = 4)
  private Long idUsuarioRegistro;
}
