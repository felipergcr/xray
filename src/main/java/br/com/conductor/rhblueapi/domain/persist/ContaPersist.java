package br.com.conductor.rhblueapi.domain.persist;

import br.com.conductor.rhblueapi.domain.pier.ProdutoResponsePier;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ContaPersist {

     Long idEmpresa;
     Long idPessoa;
     Long idGrupoEmpresaPai;
     Long idProduto;
     Long idEnderecoCorrespondencia;
     Long idOrigemComercial;
     List<ProdutoResponsePier> produtosResponse;

}
