
package br.com.conductor.rhblueapi.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import br.com.twsoftware.alfred.object.Objeto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "PESSOASFISICASCADASTRO")
public class PessoaFisicaCadastro extends PessoaAbstract {

     @Column(name = "ID_EMISSOR")
     private Long idEmissor;

     @Column(name = "BANCO")
     private Integer banco;

     @Column(name = "AGENCIA")
     private Integer agencia;

     @Column(name = "CONTACORRENTE")
     private String contaCorrente;

     @Column(name = "EMPRESA")
     private String nomeEmpregador;

     @Column(name = "EMAIL")
     private String email;

     @Column(name = "NOMEFANTASIA")
     private String nomeFantasia;

     @Column(name = "DATACONSTITUICAOEMP")
     private LocalDateTime dataConstituicaoEmp;

     @Column(name = "NOMEMAE")
     private String nomeMae;

     @Column(name = "NOMEPAI")
     private String nomePai;

     @Column(name = "ESTADOCIVIL")
     private Long idEstadoCivil;

     @Column(name = "PROFISSAO")
     private String idProfissao;

     @Column(name = "NATUREZAOCUPACAO")
     private Long idNaturezaOcupacao;

     @Column(name = "NACIONALIDADE")
     private Long idNacionalidade;

     @Column(name = "SEGUROVIDA")
     private Integer seguroVida;

     @Column(name = "SEGUROAUTO")
     private Integer seguroAuto;

     @Column(name = "SEGUROIMOVEL")
     private Integer seguroImovel;

     @Column(name = "AMERICANEXPRESS")
     private Integer americanExpress;

     @Column(name = "DINERS")
     private Integer diners;

     @Column(name = "VISA")
     private Integer visa;

     @Column(name = "MASTERCARD")
     private Integer mastercard;

     @Column(name = "CARTAO5")
     private Integer cartao5;

     @Column(name = "CARTAO6")
     private Integer cartao6;

     @Column(name = "CARTAO7")
     private Integer cartao7;

     @Column(name = "CARTAO8")
     private Integer cartao8;

     @Column(name = "CARTAO9")
     private Integer cartao9;

     @Column(name = "OUTROS")
     private Integer outros;

     @Column(name = "CHEQUEESPECIAL")
     private Integer chequeEspecial;

     @Column(name = "NUMERODEPENDENTES")
     private Integer numeroDependentes;

     @Column(name = "NATURALIDADECIDADE")
     private String naturalidadeCidade;

     @Column(name = "NATURALIDADEESTADO")
     private String naturalidadeEstado;

     @Column(name = "ORIGEMOUTRASRENDAS")
     private String origensOutrasRendas;

     @Column(name = "CGC")
     private String cgc;

     @Column(name = "EMPRESAANTERIOR")
     private String empresa;

     @Column(name = "IMOVEL1")
     private String imovel1;

     @Column(name = "CIDADE1")
     private String cidade1;

     @Column(name = "IMOVEL2")
     private String imovel2;

     @Column(name = "CIDADE2")
     private String cidade2;

     @Column(name = "MARCAVEICULO1")
     private String marcaVeiculo1;

     @Column(name = "MODELOVEICULO1")
     private String modeloVeiculo1;

     @Column(name = "MARCAVEICULO2")
     private String marcaVeiculo2;

     @Column(name = "MODELOVEICULO2")
     private String modeloVeiculo2;

     @Column(name = "PATRIMONIOTOTAL")
     private BigDecimal patrimonioTotal;

     @Column(name = "VALORDIVIDA")
     private BigDecimal valorDivida;

     @Column(name = "GRAUINSTRUCAO")
     private Integer grauInstrucao;

     @Column(name = "NOMEAGENCIA")
     private String nomeAgencia;

     @Column(name = "TELEFONEBANCO")
     private String telefoneBanco;

     @Column(name = "NOMECONJUGE")
     private String nomeConjuge;

     @Column(name = "CPFCONJUGE")
     private String cpfConjuge;

     @Column(name = "NUMEROIDENTIDADECONJUGE")
     private String numeroIdentidadeConjuge;

     @Column(name = "EMPRESACONJUGE")
     private String empresaConjuge;

     @Column(name = "NOMEREFERENCIA1")
     private String nomeReferencia1;

     @Column(name = "ENDERECOREFERENCIA1")
     private String enderecoReferencia1;

     @Column(name = "NOMEREFERENCIA2")
     private String nomeReferencia2;

     @Column(name = "ENDERECOREFERENCIA2")
     private String enderecoReferencia2;

     @Column(name = "MATRICULA1")
     private String matricula1;

     @Column(name = "MATRICULA2")
     private String matricula2;

     @Column(name = "UFEMISSAOIDENTIDADE")
     private String ufEmissaoIdentidade;

     @Column(name = "UFCARTEIRATRABALHO")
     private String ufCarteiraTrabalho;

     @Column(name = "REFERENCIAPESSOAL3")
     private String referenciaPessoal3;

     @Column(name = "OBSERVACAOLOJISTA")
     private String observacaoLojista;

     @Column(name = "PARENTESCOPESSOAL1")
     private Integer parentescoPessoal1;

     @Column(name = "REPRESENTANTELEGAL")
     private String representanteLegal;

     @Column(name = "INSCRICAOESTADUAL")
     private String inscricaoEstadual;

     @Column(name = "EMAILSECUNDARIO")
     private String emailSecundario;

     @Column(name = "INSCRICAOMUNICIPAL")
     private String inscricaoMunicipal;

     @Column(name = "NOMERESPONSAVELCAD")
     private String nomeResponsavelCad;

     @Column(name = "CPFRESPONSAVELCAD")
     private String cpfResponsavelCad;

     @Column(name = "DDDRESPONSAVELCAD")
     private String dddResponsavelCad;

     @Column(name = "TELEFONERESPONSAVELCAD")
     private String telefoneResponsavelCad;

     @Column(name = "DDDCELRESPONSAVELCAD")
     private String dddCelResponsavelCad;

     @Column(name = "CELULARRESPONSAVELCAD")
     private String celularResponsavelCad;

     @Column(name = "EMAILRESPONSAVELCAD")
     private String emailResponsavelCad;

     @Column(name = "NOMECONTATOEMPRESA")
     private String nomeContatoEmpresa;

     @Column(name = "DDDTELEFONECONTATOEMPRESA")
     private String dddTelefoneContatoEmpresa;

     @Column(name = "TELEFONECONTATOEMPRESA")
     private String telefoneContatoEmpresa;

     @Column(name = "DDDCELULARCONTATOEMPRESA")
     private String dddCelularContatoEmpresa;

     @Column(name = "CELULARCONTATOEMPRESA")
     private String celularContatoEmpresa;

     @Column(name = "EMAILCONTATOEMPRESA")
     private String emailContatoEmpresa;

     @Column(name = "CCORRENTEREF1")
     private String contaCorrenteRef1;

     @Column(name = "GERENTEREF1")
     private String gerenteRef1;

     @Column(name = "DDDREF1")
     private String dddRef1;

     @Column(name = "TELEFONEREF1")
     private String telefoneRef1;

     @Column(name = "RAZAOSOCIALREFCOMERCIAL1")
     private String razaoSocialRefComercial1;

     @Column(name = "NOMECONTRATOREFCOMERCIAL1")
     private String nomeContratoRefComercial1;

     @Column(name = "DDDREFCOMERCIAL1")
     private String dddRefComercial1;

     @Column(name = "TELEFONEREFCOMERCIAL1")
     private String telefoneRefComercial1;

     @Column(name = "EMAILREFCOMERCIAL1")
     private String emailRefComercial1;

     @Column(name = "CCORRENTEREF2")
     private String contaCorrenteRef2;

     @Column(name = "GERENTEREF2")
     private String gerenteRef2;

     @Column(name = "DDDREF2")
     private String dddRef2;

     @Column(name = "TELEFONEREF2")
     private String telefoneRef2;

     @Column(name = "RAZAOSOCIALREFCOMERCIAL2")
     private String razaoSocialRefComercial2;

     @Column(name = "NOMECONTRATOREFCOMERCIAL2")
     private String nomeContratoRefComercial2;

     @Column(name = "DDDREFCOMERCIAL2")
     private String dddRefComercial2;

     @Column(name = "TELEFONEREFCOMERCIAL2")
     private String telefoneRefComercial2;

     @Column(name = "EMAILREFCOMERCIAL2")
     private String emailRefComercial2;

     @OneToMany(mappedBy = "pessoaFisica", orphanRemoval = true, cascade = CascadeType.ALL)
     private List<TelefonePessoaFisica> telefones;

     @OneToOne
     @JoinColumn(name = "ID_PESSOAFISICA")
     @MapsId
     private PessoaEmpresa pessoaEmpresa;

     @PrePersist
     public void iniciarValores() {

          this.idEmissor = Objeto.notBlank(this.idEmissor) ? this.idEmissor : 1l;
          this.banco = Objeto.notBlank(this.banco) ? this.banco : 0;
          this.agencia = Objeto.notBlank(this.agencia) ? this.agencia : 0;
          this.contaCorrente = Objeto.notBlank(this.contaCorrente) ? this.contaCorrente : "";
          this.nomeEmpregador = Objeto.notBlank(this.nomeEmpregador) ? this.nomeEmpregador : "";
          this.email = Objeto.notBlank(this.email) ? this.email : "";
          this.nomeFantasia = Objeto.notBlank(this.nomeFantasia) ? this.nomeFantasia : "";
          this.nomeMae = Objeto.notBlank(this.nomeMae) ? this.nomeMae : "";
          this.nomePai = Objeto.notBlank(this.nomePai) ? this.nomePai : "";
          this.idProfissao = Objeto.notBlank(this.idProfissao) ? this.idProfissao : "";
          this.idNacionalidade = Objeto.notBlank(this.idNacionalidade) ? this.idNacionalidade : 1L;
          this.seguroVida = Objeto.notBlank(this.seguroVida) ? this.seguroVida : 0;
          this.seguroAuto = Objeto.notBlank(this.seguroAuto) ? this.seguroAuto : 0;
          this.seguroImovel = Objeto.notBlank(this.seguroImovel) ? this.seguroImovel : 0;
          this.americanExpress = Objeto.notBlank(this.americanExpress) ? this.americanExpress : 0;
          this.diners = Objeto.notBlank(this.diners) ? this.diners : 0;
          this.visa = Objeto.notBlank(this.visa) ? this.visa : 0;
          this.mastercard = Objeto.notBlank(this.mastercard) ? this.mastercard : 0;
          this.cartao5 = Objeto.notBlank(this.cartao5) ? this.cartao5 : 0;
          this.cartao6 = Objeto.notBlank(this.cartao6) ? this.cartao6 : 0;
          this.cartao7 = Objeto.notBlank(this.cartao7) ? this.cartao7 : 0;
          this.cartao8 = Objeto.notBlank(this.cartao8) ? this.cartao8 : 0;
          this.cartao9 = Objeto.notBlank(this.cartao9) ? this.cartao9 : 0;
          this.outros = Objeto.notBlank(this.outros) ? this.outros : 0;
          this.chequeEspecial = Objeto.notBlank(this.chequeEspecial) ? this.chequeEspecial : 0;
          this.numeroDependentes = Objeto.notBlank(this.numeroDependentes) ? this.numeroDependentes : 0;
          this.naturalidadeCidade = Objeto.notBlank(this.naturalidadeCidade) ? this.naturalidadeCidade : "";
          this.naturalidadeEstado = Objeto.notBlank(this.naturalidadeEstado) ? this.naturalidadeEstado : "";
          this.origensOutrasRendas = Objeto.notBlank(this.origensOutrasRendas) ? this.origensOutrasRendas : "";
          this.cgc = Objeto.notBlank(this.cgc) ? this.cgc : "";
          this.empresa = Objeto.notBlank(this.empresa) ? this.empresa : "";
          this.imovel1 = Objeto.notBlank(this.imovel1) ? this.imovel1 : "";
          this.cidade1 = Objeto.notBlank(this.cidade1) ? this.cidade1 : "";
          this.imovel2 = Objeto.notBlank(this.imovel2) ? this.imovel2 : "";
          this.cidade2 = Objeto.notBlank(this.cidade2) ? this.cidade2 : "";
          this.marcaVeiculo1 = Objeto.notBlank(this.marcaVeiculo1) ? this.marcaVeiculo1 : "";
          this.modeloVeiculo1 = Objeto.notBlank(this.modeloVeiculo1) ? this.modeloVeiculo1 : "";
          this.marcaVeiculo2 = Objeto.notBlank(this.marcaVeiculo2) ? this.marcaVeiculo2 : "";
          this.modeloVeiculo2 = Objeto.notBlank(this.modeloVeiculo2) ? this.modeloVeiculo2 : "";
          this.patrimonioTotal = Objeto.notBlank(this.patrimonioTotal) ? this.patrimonioTotal : new BigDecimal(0);
          this.valorDivida = Objeto.notBlank(this.valorDivida) ? this.valorDivida : new BigDecimal(0);
          this.grauInstrucao = Objeto.notBlank(this.grauInstrucao) ? this.grauInstrucao : 0;
          this.nomeAgencia = Objeto.notBlank(this.nomeAgencia) ? this.nomeAgencia : "0";
          this.telefoneBanco = Objeto.notBlank(this.telefoneBanco) ? this.telefoneBanco : "";
          this.nomeConjuge = Objeto.notBlank(this.nomeConjuge) ? this.nomeConjuge : "";
          this.cpfConjuge = Objeto.notBlank(this.cpfConjuge) ? this.cpfConjuge : "";
          this.numeroIdentidadeConjuge = Objeto.notBlank(this.numeroIdentidadeConjuge) ? this.numeroIdentidadeConjuge : "";
          this.empresaConjuge = Objeto.notBlank(this.empresaConjuge) ? this.empresaConjuge : "";
          this.nomeReferencia1 = Objeto.notBlank(this.nomeReferencia1) ? this.nomeReferencia1 : "";
          this.enderecoReferencia1 = Objeto.notBlank(this.enderecoReferencia1) ? this.enderecoReferencia1 : "";
          this.nomeReferencia2 = Objeto.notBlank(this.nomeReferencia2) ? this.nomeReferencia2 : "";
          this.enderecoReferencia2 = Objeto.notBlank(this.enderecoReferencia2) ? this.enderecoReferencia2 : "";
          this.matricula1 = Objeto.notBlank(this.matricula1) ? this.matricula1 : "";
          this.matricula2 = Objeto.notBlank(this.matricula2) ? this.matricula2 : "";
          this.ufEmissaoIdentidade = Objeto.notBlank(this.ufEmissaoIdentidade) ? this.ufEmissaoIdentidade : "";
          this.ufCarteiraTrabalho = Objeto.notBlank(this.ufCarteiraTrabalho) ? this.ufCarteiraTrabalho : "";
          this.referenciaPessoal3 = Objeto.notBlank(this.referenciaPessoal3) ? this.referenciaPessoal3 : "";
          this.observacaoLojista = Objeto.notBlank(this.observacaoLojista) ? this.observacaoLojista : "";
          this.parentescoPessoal1 = Objeto.notBlank(this.parentescoPessoal1) ? this.parentescoPessoal1 : 0;
          this.representanteLegal = Objeto.notBlank(this.representanteLegal) ? this.representanteLegal : "";
          this.inscricaoEstadual = Objeto.notBlank(this.inscricaoEstadual) ? this.inscricaoEstadual : "0";
          this.emailSecundario = Objeto.notBlank(this.emailSecundario) ? this.emailSecundario : "";
          this.inscricaoMunicipal = Objeto.notBlank(this.inscricaoMunicipal) ? this.inscricaoMunicipal : "";
          this.nomeResponsavelCad = Objeto.notBlank(this.nomeResponsavelCad) ? this.nomeResponsavelCad : "";
          this.cpfResponsavelCad = Objeto.notBlank(this.cpfResponsavelCad) ? this.cpfResponsavelCad : "";
          this.dddResponsavelCad = Objeto.notBlank(this.dddResponsavelCad) ? this.dddResponsavelCad : "";
          this.telefoneResponsavelCad = Objeto.notBlank(this.telefoneResponsavelCad) ? this.telefoneResponsavelCad : "";
          this.dddCelResponsavelCad = Objeto.notBlank(this.dddCelResponsavelCad) ? this.dddCelResponsavelCad : "";
          this.celularResponsavelCad = Objeto.notBlank(this.celularResponsavelCad) ? this.celularResponsavelCad : "";
          this.emailResponsavelCad = Objeto.notBlank(this.emailResponsavelCad) ? this.emailResponsavelCad : "";
          this.nomeContatoEmpresa = Objeto.notBlank(this.nomeContatoEmpresa) ? this.nomeContatoEmpresa : "";
          this.dddTelefoneContatoEmpresa = Objeto.notBlank(this.dddTelefoneContatoEmpresa) ? this.dddTelefoneContatoEmpresa : "";
          this.telefoneContatoEmpresa = Objeto.notBlank(this.telefoneContatoEmpresa) ? this.telefoneContatoEmpresa : "";
          this.dddCelularContatoEmpresa = Objeto.notBlank(this.dddCelularContatoEmpresa) ? this.dddCelularContatoEmpresa : "";
          this.celularContatoEmpresa = Objeto.notBlank(this.celularContatoEmpresa) ? this.celularContatoEmpresa : "";
          this.emailContatoEmpresa = Objeto.notBlank(this.emailContatoEmpresa) ? this.emailContatoEmpresa : "";
          this.contaCorrenteRef1 = Objeto.notBlank(this.contaCorrenteRef1) ? this.contaCorrenteRef1 : "0";
          this.gerenteRef1 = Objeto.notBlank(this.gerenteRef1) ? this.gerenteRef1 : "";
          this.dddRef1 = Objeto.notBlank(this.dddRef1) ? this.dddRef1 : "";
          this.telefoneRef1 = Objeto.notBlank(this.telefoneRef1) ? this.telefoneRef1 : "";
          this.razaoSocialRefComercial1 = Objeto.notBlank(this.razaoSocialRefComercial1) ? this.razaoSocialRefComercial1 : "";
          this.nomeContratoRefComercial1 = Objeto.notBlank(this.nomeContratoRefComercial1) ? this.nomeContratoRefComercial1 : "";
          this.dddRefComercial1 = Objeto.notBlank(this.dddRefComercial1) ? this.dddRefComercial1 : "";
          this.telefoneRefComercial1 = Objeto.notBlank(this.telefoneRefComercial1) ? this.telefoneRefComercial1 : "";
          this.emailRefComercial1 = Objeto.notBlank(this.emailRefComercial1) ? this.emailRefComercial1 : "";
          this.contaCorrenteRef2 = Objeto.notBlank(this.contaCorrenteRef2) ? this.contaCorrenteRef2 : "0";
          this.gerenteRef2 = Objeto.notBlank(this.gerenteRef2) ? this.gerenteRef2 : "";
          this.dddRef2 = Objeto.notBlank(this.dddRef2) ? this.dddRef2 : "";
          this.telefoneRef2 = Objeto.notBlank(this.telefoneRef2) ? this.telefoneRef2 : "";
          this.razaoSocialRefComercial2 = Objeto.notBlank(this.razaoSocialRefComercial2) ? this.razaoSocialRefComercial2 : "";
          this.nomeContratoRefComercial2 = Objeto.notBlank(this.nomeContratoRefComercial2) ? this.nomeContratoRefComercial2 : "";
          this.dddRefComercial2 = Objeto.notBlank(this.dddRefComercial2) ? this.dddRefComercial2 : "";
          this.telefoneRefComercial2 = Objeto.notBlank(this.telefoneRefComercial2) ? this.telefoneRefComercial2 : "";
          this.emailRefComercial2 = Objeto.notBlank(this.emailRefComercial2) ? this.emailRefComercial2 : "";
     }

     @PostLoad
     private void onLoad() {

          this.contaCorrente = Objeto.notBlank(this.contaCorrente) ? this.contaCorrente.trim() : null;
          this.nomeEmpregador = Objeto.notBlank(this.nomeEmpregador) ? this.nomeEmpregador.trim() : null;
          this.email = Objeto.notBlank(this.email) ? this.email.trim() : null;
          
     }
}
