
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel
@Getter
@Setter
@Builder
@NoArgsConstructor
public class PagePedidoFuncionariosDetalhesResponse extends PageResponse<PedidoDetalheFuncionarioResponse> implements Serializable {

     private static final long serialVersionUID = 6723050092485487814L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PagePedidoFuncionariosDetalhesResponse(PageResponse p) {

          super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last, p.nextPage, p.previousPage, p.content);
     }

}
