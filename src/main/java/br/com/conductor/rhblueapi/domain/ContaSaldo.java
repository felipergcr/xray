package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Campos de filtro do cartão")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class ContaSaldo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "Identificador da conta", example = "1")
    private Long id;

    @ApiModelProperty(value = "Saldo limite para compra", example = "10.00")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal saldoDisponivelGlobal;

    @ApiModelProperty(value = "Consumo médio do saldo restante", example = "10.00")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal consumoMedioSugerido;

    @ApiModelProperty(value = "dataProximaCarga", name = "Data proxima carga", example = "2018-08-25")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dataProximaCarga;

    @ApiModelProperty(value = "Valor última carga", example = "10.00")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal valorUltimaCarga;

    @ApiModelProperty(value = "Valor da próxima carga", example = "10.00")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal valorProximaCarga;

    @ApiModelProperty(value = "dataUltimaCarga", name = "Data última carga", example = "2018-08-25")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dataUltimaCarga;

    @ApiModelProperty(value = "Quantidade de dias úteis previsto para próxima recarga", example = "14")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Integer diasUteisProximaRecarga;

    @ApiModelProperty(value = "Gasto médio diário")
    private ContaGastosMedioDiarioResponse gastoMedioDiario;

}
