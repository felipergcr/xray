
package br.com.conductor.rhblueapi.domain.email;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GenericoEmail<S> {
     
     private Long idUsuario;
     
     private Long idObjeto;
     
     private S status;
     
     @Getter(value = AccessLevel.PRIVATE)
     private LocalDateTime dataHora;
     
     public String getDate() {
          
          return this.dataHora.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
     }
     
     public String getHour() {
          
          return this.dataHora.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
     }

}
