package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.conductor.rhblueapi.domain.GruposOperacoesAcessos;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(value=Include.NON_NULL)
@ApiModel(value="Resposta da listagem de operações")
public class OperacaoAcessoResponse implements Serializable {

     private static final long serialVersionUID = -3158183851400533947L;

     @ApiModelProperty(value = "Id da operação", example = "1")
     private Long id;
     
     @ApiModelProperty(value = "Nome da Operação", example = "Editar Perfis de Acesso")
     private String nomeOperacao;
     
     @ApiModelProperty(value = "identificador da plataforma", example = "16")
     private Long idPlataforma;
     
     @ApiModelProperty(value = "Agrupamento lógico de operações", example = "PERFIS DE ACESSO")
     private String agrupamento;
     
     @ApiModelProperty(value = "Ordem de exibição das operações", example = "1")
     private Integer ordem;
     
     private GruposOperacoesAcessos grupoOperacao;

}
