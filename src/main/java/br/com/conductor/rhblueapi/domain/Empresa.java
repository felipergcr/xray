package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "EMPRESAS")
public class Empresa implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_EMPRESA")
     private Long id;

     @Column(name = "ID_PESSOA", insertable = false, updatable = false)
     private Long idPessoa;

     @Column(name = "DESCRICAO")
     private String descricao;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE_TIME)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATACADASTRO")
     private LocalDateTime dataCadastro;

     @Column(name = "NOMEEXIBICAO")
     private String nomeExibicao;

     @Column(name = "STATUS")
     private Integer status;

     @Column(name = "FLAGMATRIZ")
     private Integer flagMatriz;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;
     
     @Column(name = "ID_EMPRESAPAI")
     private Long idEmpresaPai;

     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @Column(name = "DATACONTRATO")
     private LocalDate dataContrato;

     @Column(name = "NOMECONTATO")
     private String nomeContato;

     @Column(name = "CPFCONTATO")
     private String cpfContato;

     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @Column(name = "DTNASCIMENTOCONTATO")
     private LocalDate dataNascimentoContato;

     @Column(name = "EMAILCONTATO")
     private String emailContato;

     @Column(name = "DDDTELCONTATO")
     private String dddTelContato;

     @Column(name = "NUMTELCONTATO")
     private String numTelContato;

     @Column(name = "BANCO")
     private Long banco;

     @Column(name = "AGENCIA")
     private String agencia;

     @Column(name = "CONTACORRENTE")
     private Long contaCorrente;

     @Column(name = "DVCONTACORRENTE")
     private String dvContaCorrente;

     @OneToOne(fetch = FetchType.EAGER)
     @JoinColumns({ @JoinColumn(name = "ID_PESSOA", referencedColumnName = "ID_PESSOAFISICA") })
     @OnDelete(action = OnDeleteAction.CASCADE)
     private PessoaEmpresa pessoa;
     
     @Column(name = "NomeFantasia")
     private String nomeFantasia;
}
