package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import io.swagger.annotations.ApiModelProperty;

public class HeirarquiaAcessoRequest implements Serializable{

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     @NotNull
     @ApiModelProperty(value = "Identificação do grupo empresa", required = true, example = "1", position = 1)
     private Long idGrupoEmpresa;
     
     @CPF
     @ApiModelProperty(value = "CPF do usuário", required = true, example = " '12345678901' ", position = 2)
     private String cpf;
     
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(required = true, value = "Identifícador do usuário logado", example = "12345", position = 3)
     private Long idUsuarioLogado;
}
