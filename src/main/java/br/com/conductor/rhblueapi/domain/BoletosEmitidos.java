
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "Consultar Carga Benefício de funcionário")
@Entity
public class BoletosEmitidos implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @Id
     @Column(name = "ID_BOLETO")
     private Long idBoleto;

     @Column(name = "ID_TIPOBOLETO")
     private Long idTipoBoleto;

     @Column(name = "ID_EVENTOPAGAMENTO")
     private Long idEventoPagamento;

     @Column(name = "STATUSPARCELA")
     private Integer statusParcela;
     
     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAVENCIMENTO")
     @ApiModelProperty(value = "Data do vencimento", position = 4, example = "2019-05-29 18:51:00")
     private LocalDateTime dataVencimento;

     @Column(name = "NOSSONUMERO")
     private Long nossoNumero;
}
