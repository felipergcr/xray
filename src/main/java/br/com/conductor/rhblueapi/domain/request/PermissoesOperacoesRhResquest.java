package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import br.com.conductor.rhblueapi.enums.StatusPermissaoOperacaoRhEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class PermissoesOperacoesRhResquest implements Serializable  {
     
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador da associação de permissão operação", position = 1)
     private Long idPermissaoOperacao;

     @ApiModelProperty(value = "Identificador de operação", position = 2)
     private Long idOperacao;

     @ApiModelProperty(value = "Identificador de usuário que concedeu a permissão", position = 3)
     private Long idUsuarioRegistro;

     @ApiModelProperty(value = "Status de operação", position = 4)
     private StatusPermissaoOperacaoRhEnum status;
     
     
}
