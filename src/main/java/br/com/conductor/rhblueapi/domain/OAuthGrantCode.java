
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class OAuthGrantCode implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     private String code;

}
