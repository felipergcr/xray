
package br.com.conductor.rhblueapi.domain.request.relatorios;

import java.io.Serializable;

import javax.validation.Valid;

import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import br.com.conductor.rhblueapi.domain.request.relatorios.PeriodoRequest;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Relatório request")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter  
public class RelatorioRequest implements Serializable {
     
     private static final long serialVersionUID = 3983504513083365469L;

     @Valid
     private PeriodoRequest periodoRequest;
     
     @Valid
     private IdentificacaoRequest identificacao;

}
