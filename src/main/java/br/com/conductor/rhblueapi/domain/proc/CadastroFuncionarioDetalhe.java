
package br.com.conductor.rhblueapi.domain.proc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CadastroFuncionarioDetalhe implements Serializable {

     private static final long serialVersionUID = 1L;

     private Long idEmpresa;

     private Long idFuncionario;

     private Long idPessoa;

     private Long idConta;

     private Long idFuncionarioProduto;

     private Integer flagCartao;

}
