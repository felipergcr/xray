package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "PermissaoOperacaoRhResponse", description = "PermissaoOperacaoRhResponse")
public class PermissaoOperacaoRhResponse implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "Identificador da associação de permissão operação gerada", example = "12", position = 1)
  private Long idPermissaoOperacao;

  @ApiModelProperty(value = "Identificador de operação", example = "56", position = 2)
  private Long idOperacao;

  @ApiModelProperty(value = "Identificador de permissão", example = "56", position = 2)
  private Long idPermissao;

  @ApiModelProperty(value = "Status de operação", example = "true", position = 3)
  private Boolean permissaoAtiva;

  @NotNull(message = "não pode ser null")
  @ApiModelProperty(value = "Identificador de usuário que concedeu a permissão", example = "87", position = 4)
  private Long idUsuarioRegistro;

}
