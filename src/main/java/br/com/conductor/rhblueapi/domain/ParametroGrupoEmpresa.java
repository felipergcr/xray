
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.lang.Nullable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PARAMETROSGRUPOSEMPRESAS")
public class ParametroGrupoEmpresa implements Serializable {

     private static final long serialVersionUID = 8838668311161809371L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_PARAMETROGRUPOEMPRESA")
     private Long id;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Nullable
     @OneToOne(cascade = CascadeType.ALL)
     @JoinColumn(name = "ID_PARAMETRO", referencedColumnName = "ID_PARAMETRO")
     private Parametro parametro;

     @Column(name = "VALOR")
     private String valor;

}
