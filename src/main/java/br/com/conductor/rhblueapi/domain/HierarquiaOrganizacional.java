
package br.com.conductor.rhblueapi.domain;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HierarquiaOrganizacional {

     private Long idGrupoEmpresa;

     private List<Long> listaIdsSubgrupoEmpresa;

     private List<Long> listaIdsEmpresas;

}
