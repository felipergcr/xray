package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description="Dados para alterar senha no pier")
public class UsuarioSenhaTokenUpdateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "nova senha a ser alterada no pier", example = "!aw@u#R433", required = true)
	private String novaSenha;

}
