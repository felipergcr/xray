
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "Parâmetro de requisição das Unidades de Entrega")
public class UnidadeEmpresaRequest implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @NotNull
     @ApiModelProperty(required = true, value = " Identificador do usuário da sessão", position = 1)
     private Long idUsuarioSessao;

     @NotNull
     @ApiModelProperty(required = true, value = "Identificador do grupo empresa", position = 2)
     private Long idGrupoEmpresa;

     @Size(max = 30, message = "Limite de caracteres excedido")
     @ApiModelProperty(value = "Código da Unidade de Entrega", position = 3)
     private String codUnidadeEntrega;

}
