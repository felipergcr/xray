package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class AlterarSenhaRequestPier implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String senhaAtual;
	
	private String senhaNova;
	
	private String login;
}
