
package br.com.conductor.rhblueapi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * StatusCartoes
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "STATUSCARTAO")
public class StatusCartoes implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
     @Column(name = "STATUS")
     private Long id;

     @Column(name = "ID_STATUSCARTAO")
     private Long idStatusCartao;

     @Column(name = "DESCRICAO", length = 100)
     private String nome;

     @Column(name = "FLAGALTERASTATUS")
     private Integer flagCancelaCartao;

     @Column(name = "FLAGCANCELARDESBLOQUEIO")
     private Integer flagCancelaNoDesbloqueio;

     @Column(name = "STATUSDESTINODESBLOQUEIO")
     private Long idStatusDestinoDesbloqueio;

     @Column(name = "STATUSDESTINOCONTA")
     private Long idStatusDestinoConta;

     @Column(name = "FLAGCOBRATARIFA")
     private Integer flagCobraTarifa;

     @Column(name = "FLAGPERMITEDESBLOQUEIO")
     private Integer flagPermiteDesbloqueio;

     @Column(name = "FLAGCANCELAMENTO")
     private Integer flagCancelamento;

     @Column(name = "FLAGCANCELACONTA")
     private Integer flagCancelaConta;

     @Column(name = "FLAGREEMITECARTAO")
     private Integer flagPermiteNovaViaCartao;

     @Column(name = "FLAGCADASTROSENHA")
     private Integer flagCadastroSenha;

     @Column(name = "FLAGCADASTRANOVASENHA")
     private Integer flagCadastraNovaSenha;

     @Column(name = "RESPAUTORIZADOR")
     private String respAutorizador;

     @Column(name = "FLAGBLOQUEIOSENHAINCORRETA")
     private Integer flagBloqueioSenhaIncorreta;

     @Column(name = "FLAGPERMITEBLOQUEIO")
     private Integer flagPermiteBloqueio;

     @Column(name = "FLAGREVERSIVEL")
     private Integer flagReativar;

     @Column(name = "FLAGSTATUSFRAUDE")
     private Integer flagStatusFraude;
}
