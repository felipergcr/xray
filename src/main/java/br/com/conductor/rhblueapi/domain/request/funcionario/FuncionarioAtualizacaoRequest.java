
package br.com.conductor.rhblueapi.domain.request.funcionario;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.conductor.rhblueapi.domain.request.IdentificacaoRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Payload para atualização de um funcionário")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FuncionarioAtualizacaoRequest implements Serializable {
     
     private static final long serialVersionUID = 1L;
     
     @ApiModelProperty(value = "Nome", required = true, example = "Bendito Ben", position = 1)
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 100)
     private String nome;
     
     @ApiModelProperty(value = "Matrícula", required = true, example = "123456789012", position = 2)
     private String matricula;
     
     @ApiModelProperty(value = "Data de nascimento", required = true, example = "2010-01-01", position = 3)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @NotNull(message = "não pode ser null")
     private LocalDate dataNascimento;
     
     @ApiModelProperty(value = "Payload do Endereço", required = true, position = 4)
     @Valid
     private EnderecoFuncionarioRequest endereco;
     
     @ApiModelProperty(value = "Payload de Identificacao", required = true, position = 5)
     @Valid
     @NotNull(message = "não pode ser null")
     private IdentificacaoRequest identificacao;
     
}
