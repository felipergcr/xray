
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Response Customizado de notificação para pedido")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NotificacoesArquivoPedidoResponse implements Serializable {

     /**
      *
      */
     private static final long serialVersionUID = 8723869801541245301L;

     @ApiModelProperty(value = "Existe arquivo de unidade entrega em status de processamento (0 - RECEBIDO) ou (1 - IMPORTADO)?", position = 1)
     private Boolean existeArquivoDePedidoEmProcessamento;

}
