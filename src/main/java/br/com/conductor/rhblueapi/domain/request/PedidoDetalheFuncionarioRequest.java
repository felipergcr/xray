
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "Parâmetros do detalhamento da carga por funcionário")
public class PedidoDetalheFuncionarioRequest implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(required = true, value = " Identificador do usuário da sessão", position = 1)
     @NotNull
     private Long idUsuarioSessao;

}
