package br.com.conductor.rhblueapi.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="USUARIOSNIVEISPERMISSOESRH")
public class UsuariosNiveisPermissoesRh {
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_NIVEISPERMISSOES")
     private Long id;
     
     @Column(name = "ID_PERMISSAO")
     private Long idPermissao;
     
     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;
     
     @Column(name = "ID_SUBGRUPOEMPRESA")
     private Long idSubGrupoEmpresa;
     
     @Column(name = "ID_EMPRESA")
     private Long idEmpresa;
     
     @Column(name = "STATUS")
     private Integer status;
     
     @Column(name = "DATAREGISTRO")
     private LocalDateTime dataRegistro;
     
     @Column(name = "ID_USUARIO_REGISTRO")
     private Long idUsuarioRegistro;
}
