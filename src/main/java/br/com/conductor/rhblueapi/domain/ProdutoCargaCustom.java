package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProdutoCargaCustom implements Serializable{
     /**
      * 
      */
     private static final long serialVersionUID = -7336212842637710028L;
     
     private Long idProduto;
     
     private String nomeProduto;
     
     private BigDecimal valorTotal;

}
