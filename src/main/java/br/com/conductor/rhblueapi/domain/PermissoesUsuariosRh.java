
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioResumido;
import br.com.conductor.rhblueapi.enums.StatusPermissaoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "PERMISSOESUSUARIOSRH")
public class PermissoesUsuariosRh implements Serializable {

     private static final long serialVersionUID = 4970321536847501871L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_PERMISSAO")
     private Long id;

     @Column(name = "ID_USUARIO", nullable = false)
     private Long idUsuario;

     @Column(name = "STATUS")
     private Integer status;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAREGISTRO")
     private LocalDateTime dataRegistro;

     @Column(name = "ID_USUARIO_REGISTRO")
     private Long idUsuarioRegistro;

     @OneToOne(fetch = FetchType.EAGER)
     @JoinColumn(name = "ID_GRUPOEMPRESA", referencedColumnName = "ID_GRUPOEMPRESA")
     private GrupoEmpresa grupoEmpresa;

     @Transient
     private UsuarioResumido usuario;

     @Transient
     private StatusPermissaoEnum statusDescricao;
}
