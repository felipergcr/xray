package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import br.com.conductor.rhblueapi.domain.UnidadeEntrega;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PageUnidadeEntregaResponse extends PageResponse<UnidadeEntrega> implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 5477626027833596493L;

     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PageUnidadeEntregaResponse(PageResponse p) {
          super(p.getNumber(), 
                    p.size, 
                    p.totalPages, 
                    p.numberOfElements, 
                    p.totalElements, 
                    p.hasContent, 
                    p.first, 
                    p.last, 
                    p.nextPage, 
                    p.previousPage, 
                    p.content);
     }
}
