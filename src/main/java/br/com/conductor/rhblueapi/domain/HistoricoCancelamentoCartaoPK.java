
package br.com.conductor.rhblueapi.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * HistoricoCancelamentoCartaoPK
 */
@Data
@Embeddable
public class HistoricoCancelamentoCartaoPK implements Serializable{

     private static final long serialVersionUID = 1L;

     @Column(name = "ID_CONTA")
     private Long idConta;

     @Column(name = "DATACANCELAMENTO")
     private LocalDateTime dataCancelamento;
}
