
package br.com.conductor.rhblueapi.domain.request.funcionario;

import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.conductor.rhblueapi.enums.Estados;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ApiModel(description = "Payload para atualização de um endereço")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EnderecoFuncionarioRequest implements Serializable {

     private static final long serialVersionUID = 1L;
     
     @ApiModelProperty(value = "Logradouro", required = true, example = "Avenida das Nações Unidas", position = 1)
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 100)
     private String logradouro;
     
     @ApiModelProperty(value = "Número", required = true, example = "12901", position = 2)
     @NotNull(message = "não pode ser null")
     private Integer numero;
     
     @ApiModelProperty(value = "Complemento", example = "Torre Norte 23 andar", position = 3)
     @Size(max = 30)
     private String complemento;
     
     @ApiModelProperty(value = "Bairro", required = true, example = "Vila Olímpia", position = 4)
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 80)
     private String bairro;
     
     @ApiModelProperty(value = "Cidade", required = true, example = "São Paulo", position = 5)
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 60)
     private String cidade;

     @ApiModelProperty(value = "Estados", required = true, example = "SP", position = 6)
     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     private Estados uf;
     
     @ApiModelProperty(value = "CEP", required = true, example = "04578000", position = 7)
     @NotEmpty(message = "não pode ser null")
     @Pattern(regexp = "[0-9]{8}", message = "O cep está fora do padrão.")
     private String cep;
     
}