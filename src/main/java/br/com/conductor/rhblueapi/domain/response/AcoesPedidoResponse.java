
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response customizado para mostrar as possíveis ações do pedido")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AcoesPedidoResponse implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Possibilidade de cancelamento do pedido", position = 1)
     private boolean cancelar;

}
