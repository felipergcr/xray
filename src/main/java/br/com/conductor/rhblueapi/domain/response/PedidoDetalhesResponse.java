
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.conductor.rhblueapi.domain.ProdutoCargaCustom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response com detalhamento do pedido")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PedidoDetalhesResponse implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do pedido", position = 1)
     private Long idPedido;

     @ApiModelProperty(value = "Identificador do grupo empresa", position = 2)
     private Long idGrupoEmpresa;

     @ApiModelProperty(value = "Identificador da carga benefício", position = 3)
     private Long idCargaBeneficio;

     @ApiModelProperty(value = "Identificador da empresa", position = 4)
     private Long idEmpresa;

     @ApiModelProperty(value = "Número de registro da empresa (CNPJ) ", position = 5)
     private String cnpj;

     @JsonIgnore
     private Long statusCargaBeneficio;

     @ApiModelProperty(value = "Status atual da carga para a empresa", position = 6)
     private String statusEnum;

     @ApiModelProperty(value = "Valor total da carga", position = 7)
     private BigDecimal valorTotal;

     @ApiModelProperty(value = "Quantidade de funcioários beneficiados na carga", position = 8)
     private Long qtdFuncionarios;
     
     @JsonIgnore
     private Integer flagFaturamentoCentralizado;
     
     @JsonIgnore
     private Integer statusArquivoCarga;

     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
     @ApiModelProperty(value = "Data de crédito da carga", position = 9)
     private LocalDate dataAgendamento;

     @ApiModelProperty(value = "Produtos da carga benefício", position = 10)
     private List<ProdutoCargaCustom> produtos;
     
     @ApiModelProperty(value="Ações possiveis para esta carga", position = 11)
     private AcoesCargaDetalheResponse acoes;
     
}
