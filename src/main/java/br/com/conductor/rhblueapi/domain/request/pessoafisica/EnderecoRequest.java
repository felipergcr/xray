
package br.com.conductor.rhblueapi.domain.request.pessoafisica;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class EnderecoRequest implements Serializable {
     
     private static final long serialVersionUID = 1L;

     private String logradouro;
     
     private Integer numero;
     
     private String complemento;

     private String bairro;

     private String cep;

     private String cidade;

     private String uf;

}
