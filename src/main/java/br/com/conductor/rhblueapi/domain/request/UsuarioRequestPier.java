package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UsuarioRequestPier implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -408388268936098068L;

	private String nome;

	private String cpf;

	private String cnpj;
	/**
	 * Email do usuário
	 */
	private String email;
	/**
	 * Plataforma do usuário
	 */
	private int plataforma;

	private String login;

	private String senha;
	
	private List<Long> idsPerfis;

}
