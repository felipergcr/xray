package br.com.conductor.rhblueapi.domain.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = PreRequisitesCartaoStatusRequestValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface PreRequisitesCartaoStatusRequest {

    String message() default "Para agendamento a data final é obrigatória";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
