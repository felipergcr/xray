
package br.com.conductor.rhblueapi.domain.financeiro;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CargaBeneficioFinanceiroCustom  implements Serializable {
     
     private static final long serialVersionUID = 1L;

     @Id
     @Column(name = "ID")
     private BigInteger id;

     @Column(name="TIPO")
     private String tipo;

     @Column(name="VALOR")
     private Integer valor;

}
