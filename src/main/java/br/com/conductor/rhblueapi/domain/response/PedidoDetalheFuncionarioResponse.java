
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@ApiModel(description = "Response com detalhamento da carga por funcionário")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PedidoDetalheFuncionarioResponse implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 929634196629014074L;

     @ApiModelProperty(value = "Identificador do funcionário", position = 1)
     private BigInteger idFuncionario;
     
     @ApiModelProperty(value = "Nome do funcionário", position = 2)
     private String nome;

     @ApiModelProperty(value = "Cpf do funcionário", position = 3)
     private String cpf;

     @ApiModelProperty(value = "Apresenta o nome do logradouro", position = 4)
     private String logradouro;

     @ApiModelProperty(value = "Apresenta descrições complementares referente ao endereço", position = 5)
     private String complemento;

     @ApiModelProperty(value = "Apresenta nome do bairro.", position = 6)
     private String bairro;

     @ApiModelProperty(value = "Apresenta nome da cidade", position = 7)
     private String cidade;

     @ApiModelProperty(value = "Apresenta sigla da Unidade Federativa", position = 8)
     private String uf;

     @ApiModelProperty(value = "Apresenta o Código de Endereço Postal (CEP)", position = 9)
     private String cep;

     @ApiModelProperty(value = "Lista de produtos com carga", position = 10)
     private List<PedidoDetalheFuncionarioProdutoResponse> produtos;
}
