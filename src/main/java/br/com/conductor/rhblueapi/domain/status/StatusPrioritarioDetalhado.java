
package br.com.conductor.rhblueapi.domain.status;

import br.com.conductor.rhblueapi.enums.StatusCargaBeneficioEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StatusPrioritarioDetalhado {
     
     private StatusCargaBeneficioEnum status;
     
     private boolean isTodosStatusIguais;

}
