
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.conductor.rhblueapi.enums.Plataforma;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Validar senha login do usuário")
public class UsuarioValidarSenhaLoginRequest implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = -4391608119935553935L;

     @ApiModelProperty(value = "senha", example = "12qw90OP", required = true, position = 2)
     @NotNull
     private String senha;
     
     @ApiModelProperty(required = true, value = "ESTABELECIOMENTO, RH, PORTADOR", example = "RH", position = 3)
     @NotNull(message = "não pode ser null")
     private Plataforma plataforma;     

}
