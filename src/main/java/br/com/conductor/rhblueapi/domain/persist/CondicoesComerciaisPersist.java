package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Condições comerciais")
public class CondicoesComerciaisPersist implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Valor mínimo do VA
	 */
	@NotNull(message = "não pode ser null")
	@ApiModelProperty(value = "Valor mínimo do VA", example = "20.10", position = 1)
	private BigDecimal valorMinimoVA;

	/**
	 * Valor mínimo do VR
	 */
	@NotNull(message = "não pode ser null")
	@ApiModelProperty(value = "Valor mínimo do VR", example = "20.10", position = 2)
	private BigDecimal valorMinimoVR;
	/**
	 * Taxa de Administração
	 */
	@NotNull(message = "não pode ser null")
	@ApiModelProperty(value = "Taxa de administração", example = "20.10", position = 3)
	private BigDecimal taxaAdministracao;

	/**
	 * Taxa de Entrega
	 */
	@NotNull(message = "não pode ser null")
	@ApiModelProperty(value = "Taxa de entrega", example = "20.10", position = 4)
	private BigDecimal taxaEntrega;

	/**
	 * Taxa de Disponibilização de Crédito
	 */
	@NotNull(message = "não pode ser null")
	@ApiModelProperty(value = "Taxa de disponibilização de crédito", example = "20.10", position = 5)
	private BigDecimal taxaDisponibilizacaoCredito;

	/**
	 * Taxa de Emissão de Cartão
	 */
	@NotNull(message = "não pode ser null")
	@ApiModelProperty(value = "Taxa de emissão de cartão", example = "20.10", position = 6)
	private BigDecimal taxaEmissaoCartao;

	/**
	 * Taxa de Reemissão de Cartão
	 */
	@NotNull(message = "não pode ser null")
	@ApiModelProperty(value = "Taxa de reemissão de cartão", example = "20.10", position = 7)
	private BigDecimal taxaReemissaoCartao;
}
