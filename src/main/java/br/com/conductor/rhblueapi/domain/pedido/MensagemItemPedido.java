
package br.com.conductor.rhblueapi.domain.pedido;

import java.io.Serializable;

import br.com.conductor.rhblueapi.domain.ArquivoCargas;
import br.com.conductor.rhblueapi.domain.ArquivosCargasDetalhes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MensagemItemPedido implements Serializable {
     
     private static final long serialVersionUID = 1L;

     private ArquivoCargas arquivo;
     
     private ArquivosCargasDetalhes detalhe;
     
}
