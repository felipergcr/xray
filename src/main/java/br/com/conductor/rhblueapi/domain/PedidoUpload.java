package br.com.conductor.rhblueapi.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PedidoUpload {

     private Long idArquivoCargas;
     
     private String tipoEntregaCartao;
}
