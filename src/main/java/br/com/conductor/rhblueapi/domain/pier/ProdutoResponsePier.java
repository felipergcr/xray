package br.com.conductor.rhblueapi.domain.pier;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProdutoResponsePier {

     @ApiModelProperty(value = "id", name = "Identificador do produto", position = 1)
     private Long id;
     
     @ApiModelProperty(value = "nome", name = "Nome do produto", position = 2)
     private String nome;
}
