
package br.com.conductor.rhblueapi.domain.whitelist;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "EXCECOESLIMITEDISPONIVEL")
public class ExcecoesLimiteDisponivel implements Serializable {

     private static final long serialVersionUID = -7486856570008713251L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_EXCECOESLIMITEDISPONIVEL")
     private Long id;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Column(name = "DESCRICAO")
     private String descricao;

     @Column(name = "STATUS")
     private Integer status;

     @Column(name = "DATAALTERACAO")
     private LocalDateTime dataAlteracao;

     @Column(name = "ID_USUARIOREGISTRO")
     private Long idUsuarioRegistro;

}
