
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response com os produtos do funcionário")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PedidoDetalheFuncionarioProdutoResponse implements Serializable {
     
     /**
      * 
      */
     private static final long serialVersionUID = -7313084055022486472L;
     
     @ApiModelProperty(value = "Identificador do produto", position = 1)
     private Long idProduto;

     @ApiModelProperty(value = "Nome do produto", position = 2)
     private String nomeProduto;

     @ApiModelProperty(value = "Valor da carga", position = 3)
     private BigDecimal valorCarga;

     @ApiModelProperty(value = "Indicador de novo cartão emitido", position = 4)
     private String indicadorEmissaoCartao;

}
