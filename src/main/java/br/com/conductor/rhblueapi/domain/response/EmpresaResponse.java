
package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.conductor.rhblueapi.domain.pier.UsuarioResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response da Empresa")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class EmpresaResponse implements Serializable {

     private static final long serialVersionUID = -3158183851400533947L;
   
     @ApiModelProperty(value = "Id empresa", example = "23", position = 1)
     private Long idEmpresa;

     @ApiModelProperty(value = "Identificador da pessoa", example = "1809", position = 2)
     private Long idPessoa;

     @ApiModelProperty(value = "Identificador da Conta", example = "1", position = 3)
     private Long idConta;

     @ApiModelProperty(value = "Nome da empresa", example = "Empresa teste", position = 4)
     private String nomeEmpresa;

     @ApiModelProperty(value = "Cnpj Empresa", example = "68856247640", position = 5)
     private String cnpj;

     @ApiModelProperty(value = "Identificador do Grupo Empresa", example = "1", position = 6)
     private Long idGrupoEmpresa;

     @ApiModelProperty(value = "Dados dos usuários gerados", position = 7)
     private List<UsuarioResponse> usuarios;

     @ApiModelProperty(value = "Id do endereço de correspondência", position = 8)
     private Long idEnderecoCorrespondencia;
}
