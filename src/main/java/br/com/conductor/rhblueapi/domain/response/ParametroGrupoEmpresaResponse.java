package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "Campos do objeto Parâmetros de um grupo empresa")
public class ParametroGrupoEmpresaResponse implements Serializable{

     private static final long serialVersionUID = 8908713980939589198L;
     
     @ApiModelProperty(value = "Identificador do Parametro do Grupo Empresa", notes = "Identificador do Parametro do Grupo Empresa", example = "1", position = 1)
     private Long idParametroGrupoEmpresa;
     
     @ApiModelProperty(value = "Identificador do Grupo Empresa", notes = "Identificador do Grupo Empresa", example = "2", position = 2)
     private Long idGrupoEmpresa;
     
     @ApiModelProperty(value = "Identificador do Parametro", notes = "Identificador do Parametro", example = "3", position = 3)
     private Long idParametro;

     @ApiModelProperty(value = "Valor do Parametro", notes = "Valor do Parametro", example = "0", position = 4)
     private String valor;
     
     @ApiModelProperty(value = "Descrição do Parâmetro", notes = "Descrição do Parâmetro", example = "Define se o RH embossa cartões com o nome impresso ou não", position = 5)
     private String descricao;
}
