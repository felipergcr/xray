package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import br.com.conductor.rhblueapi.domain.ArquivoPedidoDetalhesErrosReponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PageArquivoPedidoDetalhesErrosReponse extends PageResponse<ArquivoPedidoDetalhesErrosReponse> implements Serializable{

     /**
      * 
      */
     private static final long serialVersionUID = 507679561477232878L;
     
     @SuppressWarnings({ "rawtypes", "unchecked" })
     public PageArquivoPedidoDetalhesErrosReponse(PageResponse p) {
          super(p.getNumber(), p.size, p.totalPages, p.numberOfElements, p.totalElements, p.hasContent, p.first, p.last, p.nextPage, p.previousPage, p.content);
     }
}
