package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HierarquiaAcessoGrupoEmpresaResponse implements Serializable{
     
     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value="Nome do grupo de empresa pai.", example="Grupo Empresa Santander", position = 1)
     private String nomeGrupoEmpresa;
     
     @ApiModelProperty(required = true, value = "Identificador do grupo empresa a qual usuário pertence", example = "8563", position = 2)
     private Long idGrupoEmpresa;
     
     @ApiModelProperty(value="Estrutura contendo Dados do Subgrupo.", position=3)
     private List<HierarquiaAcessoSubGrupoResponse> subgrupos;
     
     

}
