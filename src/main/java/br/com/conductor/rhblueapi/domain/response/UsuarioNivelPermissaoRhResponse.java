package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import br.com.conductor.rhblueapi.enums.StatusNivelPermissaoAcessoEnum;
import br.com.conductor.rhblueapi.enums.StatusPermissaoEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioNivelPermissaoRhResponse implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "Identificado do nível de acesso gerado", example = "123")
  private Long idNivelPermissao;

  @ApiModelProperty(value = "Identificado da permissâo de acesso", example = "123")
  private Long idPermissao;

  @ApiModelProperty(value = "Status do nivel de acesso", example = "ATIVO")
  private StatusNivelPermissaoAcessoEnum status;

  @ApiModelProperty(value = "Nível de acesso", example = "GRUPO")
  private NivelPermissaoEnum nivelAcesso;

  @ApiModelProperty(value = "Identificador do usuário resposavel pela ação", example = "1231")
  private Long idUsuarioRegistro;
  
  @ApiModelProperty(value = "Nome do nivel de acesso", example = "Pão de açucar")
  private String nomeNivel;
  
  @ApiModelProperty(value = "Status da permissão usuário", example = "ATIVO")
  private StatusPermissaoEnum statusPermissao;
  
  @ApiModelProperty(value = "Nome de usuário com nível de acesso", example = "Dexter")
  private String nomeUsuarioNivel;
  
  @ApiModelProperty(value = "Cpf de usuário com nível de acesso", example = "1234567898541")
  private String cpfUsuarioNivel;

}
