package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description= "Requisição da empresa")
@Getter @Setter
public class EmpresaRequest implements Serializable {

	private static final long serialVersionUID = 1563934387082467416L;

	@ApiModelProperty(value = "Id da empresa", example = "[1,5,82]", required = true)
	private List<Long> idsEmpresas;
}
