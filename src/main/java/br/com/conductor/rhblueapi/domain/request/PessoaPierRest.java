package br.com.conductor.rhblueapi.domain.request;

import static br.com.conductor.rhblueapi.util.AppConstantes.PIER_PESSOA_FISICAS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.domain.response.PessoaResponse;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;

@Service
@RefreshScope
@Lazy
public class PessoaPierRest {

	@Value("${app.pier.host}${app.pier.basepath}")
	private String server;

	@Autowired
	private HeadersDefaultPier headersPier;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	public PessoaPierRest(RestTemplate restTemplate) {

		this.restTemplate = restTemplate;
	}

	public List<PessoaResponse> consultaPessoaFisica(String cpf) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_PESSOA_FISICAS).queryParam("cpf", cpf);
		HttpHeaders headers = headersPier.creatHeaders();
		HttpEntity<?> entity = new HttpEntity<>(headers);

		try {
			final List<PessoaResponse> origins = new ArrayList<PessoaResponse>();
			ParameterizedTypeReference<PageResponse<PessoaResponse>> responseType = new ParameterizedTypeReference<PageResponse<PessoaResponse>>() {
			};
			ResponseEntity<PageResponse<PessoaResponse>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, responseType);
			origins.addAll(response.getBody().getContent());
			return origins;

		} catch (HttpClientErrorException e) {
			if(Objects.equals(e.getStatusCode(), HttpStatus.NOT_FOUND)) {
				 return Collections.emptyList();
			}
				throw new BadRequestCdt(e.getResponseBodyAsString());
		}
	}

	/**
	 * consultaPessoaFisicaPorId
	 * método responsável por realizar a consulta de pessoa física no pier
	 *
	 * @param id
	 * @return
	 */
	public PessoaResponse consultaPessoaFisicaPorId(Long id) {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_PESSOA_FISICAS+"/"+id);
		HttpHeaders headers = headersPier.creatHeaders();
		HttpEntity<?> entity = new HttpEntity<>(headers);

		try {

			ResponseEntity<PessoaResponse> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, PessoaResponse.class);
			return response.getBody();

		} catch (HttpClientErrorException e) {
			if(Objects.equals(e.getStatusCode(), HttpStatus.NOT_FOUND)) {
				return null;
			}
			throw new BadRequestCdt(e.getResponseBodyAsString());
		}
	}

}
