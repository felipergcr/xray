package br.com.conductor.rhblueapi.domain.pier;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TelefoneResponse implements Serializable {

     private static final long serialVersionUID = 1L;
     
     @ApiModelProperty(value = "id", name = "Identificador do telefone", position = 1)
     private Long id;

     @ApiModelProperty(value = "idTipoTelefone", name = "Código de Identificação do Tipo do Telefone.", position = 2)
     private Integer idTipoTelefone;

     @ApiModelProperty(value = "ddd", name = "Código DDD do telefone.", position = 3)
     private String ddd;

     @ApiModelProperty(value = "telefone", name = "Número do telefone.", position = 4)
     private String telefone;

     @ApiModelProperty(value = "status", name = "Status do Telefone, onde: '0': Inativo e '1': Ativo",  position = 5)
     private Integer status;

     @ApiModelProperty(value = "ramal", name = "Ramal do Telefone",  position = 6)
     private String ramal;
}
