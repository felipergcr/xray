package br.com.conductor.rhblueapi.domain.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Response com as ações que o usuário pode executar na tela de pedido detalhado.")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AcoesCargaDetalheResponse implements Serializable{

     private static final long serialVersionUID = -3048474642153998956L;
    
     @ApiModelProperty(value = "Ação que permite o cancelamento parcial de um pedido caso o registro atenda as regras", position = 1)
     private boolean cancelar;
     
}