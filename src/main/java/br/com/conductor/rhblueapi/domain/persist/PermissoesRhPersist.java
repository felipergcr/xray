package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ApiModel(value = "PermissoesRhPersist", description = "PermissoesRhPersist")
public class PermissoesRhPersist implements Serializable {
     
     /**
      * 
      */
     private static final long serialVersionUID = -7854819344716340547L;
     
     /**
      * Id do usuário
      */
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Id do usuário", example = "152", position = 1)
     private Long idUsuario;

     /**
      * Id do grupo empresa
      */
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Id do grupo empresa", example = "1234", position = 2)
     private Long idGrupoEmpresa;

     /**
      * Id do usuário que realizou o cadastro
      */
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Id do usuário que realizou o cadastro", example = "153", position = 3)
     private Long idUsuarioRegistro;

}
