
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioResumido;
import br.com.conductor.rhblueapi.enums.StatusArquivoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ARQUIVOCARGASTD")
public class ArquivoCargas implements Serializable {

     /**
     * 
     */
     private static final long serialVersionUID = -365088118189475319L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_ARQUIVOCARGASTD")
     private Long id;

     @Column(name = "ID_PRODUTO")
     private Long idProduto;

     @Column(name = "VALORESPERADO")
     private BigDecimal valor;

     @NotNull
     @Column(name = "STATUS")
     private Integer status;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATASTATUS")
     private LocalDateTime dataStatus;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAPROCESSAMENTO")
     private LocalDateTime dataProcessamento;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAIMPORTACAO")
     private LocalDateTime dataImportacao;

     @JsonSerialize(using = LocalDateTimeSerializer.class)
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)
     @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
     @Column(name = "DATAAGENDAMENTO")
     private LocalDateTime dataAgendamento;

     @NotNull
     @Column(name = "NOMEARQUIVO")
     private String nome;

     @Column(name = "JOB_ID")
     private Long idJob;

     @Column(name = "ID_USUARIOREGISTRO")
     private Long idUsuario;

     @Column(name = "ID_GRUPOEMPRESA")
     private Long idGrupoEmpresa;

     @Column(name = "ORIGEM")
     private String origem;
     
     @Column(name = "MOTIVOERRO")
     private String motivoErro;
     
     @Column(name = "FLAGPEDIDOCENTRALIZADO", columnDefinition = "BIT")
     private Boolean flagPedidoCentralizado;

     @Column(name = "FLAGFATURAMENTOCENTRALIZADO", columnDefinition = "BIT")
     private Boolean flagFaturamentoCentralizado;

     @Column(name = "DATACANCELAMENTOPEDIDO")
     private LocalDateTime dataCancelamentoPedido;

     @Column(name = "TIPOPAGAMENTOPEDIDO")
     private String tipoPagamentoPedido;

     @Column(name = "STATUSPAGAMENTO")
     private Integer statusPagamento;

     @Column(name = "UUID")
     private String uuid;
     
     @Transient
     private UsuarioResumido usuarioResumido;
     
     @Transient
     private StatusArquivoEnum statusEnum;

}
