
package br.com.conductor.rhblueapi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.conductor.rhblueapi.enums.ModeloDePedido;
import br.com.conductor.rhblueapi.enums.SegmentoEmpresaEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ModeloDeCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoEntregaCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPagamento;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPedido;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoProduto;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TransferenciaProdutos;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Dados de grupo empresa")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParametrosPersist implements Serializable {

     private static final long serialVersionUID = 1L;
     
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Email(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "e-mail inválido")
     @ApiModelProperty(value = "Email para envio da nota fiscal eletrônica", required = true, example = "notafiscal@nota.com.br", position = 1)
     private String emailNotaFiscal;

     @NotNull
     @ApiModelProperty(value = "Flag que indica o tipo de faturamento", required = true, example = "CENTRALIZADO", position = 2)
     private TipoFaturamentoEnum tipoFaturamento;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Tipo do contrato", required = true, example = "POS", position = 3)
     private TipoContratoEnum tipoContrato;

     @ApiModelProperty(value = "Prazo de pagamento", example = "90", position = 4)
     private Integer prazoPagamento;

     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Limite de credito disponivel para grupo empresa", required = true, example = "20.10", position = 5)
     private BigDecimal limiteCreditoDisponivel;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Segmento da Empresa", required = true, example = "E1_DIGITAL", position = 6)
     private SegmentoEmpresaEnum segmentoEmpresa;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Tipo de pedido", required = true, example = "CENTRALIZADO", position = 7)
     private TipoPedido tipoPedido;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Modelo do cartão", required = true, example = "COM_NOME", position = 8)
     private ModeloDeCartao modeloCartao;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Modelo do pedido", required = true, example = "WEB", position = 9)
     private ModeloDePedido modeloPedido;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Tipo de entrega", required = true, example = "RH", position = 10)
     private TipoEntregaCartao tipoEntrega;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Transferencia entre VA e VR", required = true, example = "HABILITADA", position = 11)
     private TransferenciaProdutos transferencia;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @ApiModelProperty(value = "Tipo de pagamento", required = true, example = "BOLETO", position = 12)
     private TipoPagamento tipoPagamento;

     @Enumerated(EnumType.STRING)
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @ApiModelProperty(value = "Tipo do produto", required = true, example = "[\"VA\",\"VR\"]", position = 13)
     private List<TipoProduto> tipoProduto;
     
     @ApiModelProperty(value = "Data da vigência do limite", example = "2010-01-01", position = 14)
     @JsonSerialize(using = LocalDateSerializer.class)
     @JsonDeserialize(using = LocalDateDeserializer.class)
     @DateTimeFormat(iso = ISO.DATE)
     private LocalDate dataVigenciaLimite;

}
