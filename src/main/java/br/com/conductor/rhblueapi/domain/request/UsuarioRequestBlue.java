package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.conductor.rhblueapi.enums.Plataforma;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "Dados do usuário a ser cadastrado")
public class UsuarioRequestBlue implements Serializable {
     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Size(max = 50, message = "não pode ter mais de 50 caracteres")
     @ApiModelProperty(required = true, value = "Nome do usuário", example = "Donatelo Best")
     private String nome;

     /**
      * CPF do usuário
      */
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @Pattern(regexp = "([0-9]{11})", message = "inválido")
     @CPF
     @ApiModelProperty(required = true, position = 1, value = "CPF do Usuário", example = " '12345678901' ")
     private String cpf;

     /**
      * CNPJ do estabelecimento
      */
     @Pattern(regexp = "([0-9]{14})", message = "inválido")
     @CNPJ
     @ApiModelProperty(position = 2, value = "CNPJ Estabelecimento", example = " '12345678901234'")
     private String cnpj;

     /**
      * Email do usuário
      */
     @NotEmpty(message = "não pode ser vazio")
     @Pattern(regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", message = "inválido")
     @Email
     @ApiModelProperty(required = true, value = "Email do usuário", example = "teste@teste.com.br", position = 3)
     private String email;

     /**
      * Plataforma do usuário
      */
     @ApiModelProperty(required = true, value = "ESTABELECIOMENTO, RH, PORTADOR", example = "RH", position = 4)
     @NotNull(message = "não pode ser null")
     private Plataforma plataforma;
     
     /**
      * Nome da empresa
      */
     @ApiModelProperty(value = "Nome do Estabelecimento", example = "Restaurante Dois Irmãos LTDA", position = 5)
     private String nomeEmpresa;

     /**
      * Senha do usuario
      */
     @ApiModelProperty(value = "Senha do usuario", example = "Q12#wqw", position = 6)
     private String senha;
}
