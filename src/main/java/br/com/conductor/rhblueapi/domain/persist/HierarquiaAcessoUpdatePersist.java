
package br.com.conductor.rhblueapi.domain.persist;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.conductor.rhblueapi.enums.NivelPermissaoEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HierarquiaAcessoUpdatePersist implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Nível de acesso da permissão", required = true, position = 1)
     @NotNull(message = "não pode ser null")
     private NivelPermissaoEnum nivelAcesso;

     @ApiModelProperty(value = "Identificador do grupo empresa do usuário pretendido a dar acesso", example="10", required = true, position = 2)
     @NotNull(message = "não pode ser null")
     private Long idGrupoEmpresa;
     
     @NotNull(message = "não pode ser null")
     @NotEmpty(message = "não pode ser vazio")
     @ApiModelProperty(required = true, value = "Lista de Identifícador(id de grupo ou ids de subgrupos) dos níveis que o usuário terá acesso", example = "[1,2]", position = 3)
     private List<Long> idsNivelAcesso;

     @NotNull(message = "não pode ser null")
     @ApiModelProperty(required = true, value = "Identifícador do usuário logado", example = "12345", position = 4)
     private Long idUsuarioLogado;


}
