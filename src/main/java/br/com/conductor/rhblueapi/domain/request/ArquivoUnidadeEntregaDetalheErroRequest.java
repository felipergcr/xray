
package br.com.conductor.rhblueapi.domain.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "Request com campos necessários para realizar a listagem dos erros no arquivo")
public class ArquivoUnidadeEntregaDetalheErroRequest implements Serializable {

     private static final long serialVersionUID = 1L;

     @ApiModelProperty(value = "Identificador do usuário", position = 1, required = true, example = "1")
     @NotNull
     private Long idUsuarioLogado;

     @ApiModelProperty(value = "Identificador do grupo empresa", position = 2, required = true, example = "1")
     @NotNull
     private Long idGrupoEmpresa;

}
