
package br.com.conductor.rhblueapi.task.status.pagamento;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.financeiro.CargaBeneficioFinanceiroCustom;
import br.com.conductor.rhblueapi.domain.status.CargaBeneficioMinimoCustom;
import br.com.conductor.rhblueapi.domain.status.DadosAtualizacaoStatusPagamento;
import br.com.conductor.rhblueapi.repository.financeiro.statuscarga.CargaBeneficioFinanceiroImpl;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.status.StatusPagamentoPublish;
import br.com.conductor.rhblueapi.service.CargaBeneficioService;
import br.com.conductor.rhblueapi.util.AppConstantes;

@Component
public class StatusPagamentoTask {
     
     @Autowired
     private CargaBeneficioService cargaBeneficioService;
     
     @Autowired
     private StatusPagamentoPublish statusPagamentoPublish;
     
     @Autowired
     private CargaBeneficioFinanceiroImpl cargaBeneficioFinanceiroImpl;
     
     @Scheduled(cron = "${app.task.cron.status-pagamento.status-carga.carga-confirmado.periodicidade}")
     public void atualizarStatusPagamentoComStatusCargaPago(){
          
          Map<Long, List<CargaBeneficioMinimoCustom>> mapDadosParaAtualizacao = cargaBeneficioService.gerenciaRegistrosComStatusCargaPago();
          
          if(!mapDadosParaAtualizacao.isEmpty()) {
               mapDadosParaAtualizacao.entrySet().stream().forEach(dadoParaAtualizar -> {
                    
                    List<CargaBeneficioFinanceiroCustom> dadosFinanceiro = cargaBeneficioFinanceiroImpl.buscaDadosFinaceiroCargasPorNumeroPedido(dadoParaAtualizar.getKey());
                    
                    int qtdCargasPedido = dadosFinanceiro.stream().filter(item -> item.getTipo().equals(AppConstantes.QTD_CARGASPEDIDO)).findFirst().get().getValor();
                    int qtdCargasPedidoPagas = dadosFinanceiro.stream().filter(item -> item.getTipo().equals(AppConstantes.QTD_CARGASPEDIDOPAGAS)).findFirst().get().getValor();
                    
                    statusPagamentoPublish.enviarCargasPedidosParaAtualizacaoStatus(
                              
                              DadosAtualizacaoStatusPagamento.builder()
                              .numeroPedido(dadoParaAtualizar.getKey())
                              .idGrupoEmpresa(new Long(dadoParaAtualizar.getValue().stream().findFirst().get().getIdGrupoEmpresa()))
                              .statusCargas(dadoParaAtualizar.getValue().stream().map(CargaBeneficioMinimoCustom::getStatusCargaBeneficio).collect(Collectors.toList()))
                              .qtdCargasPedido(qtdCargasPedido)
                              .qtdCargasPedidoPagas(qtdCargasPedidoPagas)
                              .build());
                    
               });
          }
          
     }
     
     @Scheduled(cron = "${app.task.cron.status-pagamento.status-carga.carga-cancelada.periodicidade}")
     public void atualizarStatusPagamentoComStatusCancelado(){
          
          Map<Long, List<CargaBeneficioMinimoCustom>> mapDadosParaAtualizacao = cargaBeneficioService.gerenciaRegistrosComStatusCargaCancelado();
          
          if(!mapDadosParaAtualizacao.isEmpty()) {
               mapDadosParaAtualizacao.entrySet().stream().forEach(dadoParaAtualizar -> {
                    
                    List<CargaBeneficioFinanceiroCustom> dadosFinanceiro = cargaBeneficioFinanceiroImpl.buscaDadosFinaceiroCargasPorNumeroPedido(dadoParaAtualizar.getKey());
                    
                    int qtdCargasPedido = dadosFinanceiro.stream().filter(item -> item.getTipo().equals(AppConstantes.QTD_CARGASPEDIDO)).findFirst().get().getValor();
                    int qtdCargasPedidoPagas = dadosFinanceiro.stream().filter(item -> item.getTipo().equals(AppConstantes.QTD_CARGASPEDIDOPAGAS)).findFirst().get().getValor();
                    
                    statusPagamentoPublish.enviarCargasPedidosParaAtualizacaoStatus(
                              
                              DadosAtualizacaoStatusPagamento.builder()
                              .numeroPedido(dadoParaAtualizar.getKey())
                              .idGrupoEmpresa(new Long(dadoParaAtualizar.getValue().stream().findFirst().get().getIdGrupoEmpresa()))
                              .statusCargas(dadoParaAtualizar.getValue().stream().map(CargaBeneficioMinimoCustom::getStatusCargaBeneficio).collect(Collectors.toList()))
                              .qtdCargasPedido(qtdCargasPedido)
                              .qtdCargasPedidoPagas(qtdCargasPedidoPagas)
                              .build());
                    
               });
          }
          
     }
     
     @Scheduled(cron = "${app.task.cron.status-pagamento.status-carga.carga-expirada.periodicidade}")
     public void atualizarStatusPagamentoComStatusExpirado(){
          
          Map<Long, List<CargaBeneficioMinimoCustom>> mapDadosParaAtualizacao = cargaBeneficioService.gerenciaRegistrosComStatusCargaExpirado();
          
          if(!mapDadosParaAtualizacao.isEmpty()) {
               mapDadosParaAtualizacao.entrySet().stream().forEach(dadoParaAtualizar -> {
                    
                    List<CargaBeneficioFinanceiroCustom> dadosFinanceiro = cargaBeneficioFinanceiroImpl.buscaDadosFinaceiroCargasPorNumeroPedido(dadoParaAtualizar.getKey());
                    
                    int qtdCargasPedido = dadosFinanceiro.stream().filter(item -> item.getTipo().equals(AppConstantes.QTD_CARGASPEDIDO)).findFirst().get().getValor();
                    int qtdCargasPedidoPagas = dadosFinanceiro.stream().filter(item -> item.getTipo().equals(AppConstantes.QTD_CARGASPEDIDOPAGAS)).findFirst().get().getValor();
                    
                    statusPagamentoPublish.enviarCargasPedidosParaAtualizacaoStatus(
                              
                              DadosAtualizacaoStatusPagamento.builder()
                              .numeroPedido(dadoParaAtualizar.getKey())
                              .idGrupoEmpresa(new Long(dadoParaAtualizar.getValue().stream().findFirst().get().getIdGrupoEmpresa()))
                              .statusCargas(dadoParaAtualizar.getValue().stream().map(CargaBeneficioMinimoCustom::getStatusCargaBeneficio).collect(Collectors.toList()))
                              .qtdCargasPedido(qtdCargasPedido)
                              .qtdCargasPedidoPagas(qtdCargasPedidoPagas)
                              .build());
                    
               });
          }
          
     }
     
}
