
package br.com.conductor.rhblueapi.task;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.pedido.CargaPedidoCustom;
import br.com.conductor.rhblueapi.repository.rabbitmq.publish.LimiteCreditoPublisher;
import br.com.conductor.rhblueapi.service.PedidoService;

@Component
public class LimiteCreditoTask {
     
     @Autowired
     private PedidoService pedidoService;
     
     @Autowired
     private LimiteCreditoPublisher publisher;
     
     @Scheduled(cron = "${app.task.cron.limite-credito.validacao-diaria}")
     public void tarefaValidacaoDiaria() {
          
          Map<Long, List<CargaPedidoCustom>> mapCargasPedidos = pedidoService.buscarMapPedidosComProblemasDeLimiteDeCredito();
          
          if(!mapCargasPedidos.isEmpty()) {
               
               mapCargasPedidos.entrySet().stream().forEach(itemMapCargasPedidos -> {
                    
                    publisher.enviarCargasPedidosPorGrupoEmpresaParaValidacaoLimiteDiario(itemMapCargasPedidos.getValue());
                    
               });
          }
     }
}
