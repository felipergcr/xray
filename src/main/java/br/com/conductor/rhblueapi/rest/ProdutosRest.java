
package br.com.conductor.rhblueapi.rest;

import static br.com.conductor.rhblueapi.util.AppConstantes.PIER_PRODUTOS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.pier.ProdutoResponsePier;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;

@Service
public class ProdutosRest {

     @Value("${app.pier.host}${app.pier.basepath}")
     private String server;

     @Autowired
     HeadersDefaultPier headersPier;

     @Autowired
     private RestTemplate restTemplate;

     @Autowired
     public ProdutosRest(RestTemplate restTemplate) {

          this.restTemplate = restTemplate;
     }

     public List<ProdutoResponsePier> consultaProduto() throws JsonParseException, JsonMappingException, IOException {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_PRODUTOS);
          HttpHeaders headers = headersPier.creatHeaders();
          HttpEntity<?> entity = new HttpEntity<>(headers);

          try {
               final List<ProdutoResponsePier> origins = new ArrayList<ProdutoResponsePier>();
               ParameterizedTypeReference<PageResponse<ProdutoResponsePier>> responseType = new ParameterizedTypeReference<PageResponse<ProdutoResponsePier>>() {
               };
               ResponseEntity<PageResponse<ProdutoResponsePier>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, responseType);
               origins.addAll(response.getBody().getContent());
               return origins;

          } catch (HttpClientErrorException e) {
               throw new BadRequestCdt(e.getResponseBodyAsString());
          }
     }
}
