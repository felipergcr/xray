package br.com.conductor.rhblueapi.rest;


import static br.com.conductor.rhblueapi.util.AppConstantes.PIER_ENDERECOS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.rhblueapi.domain.pier.EnderecoResponsePier;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;

@Service
@RefreshScope
@Lazy
public class EnderecoPierRest {

	@Value("${app.pier.host}${app.pier.basepath}")
	private String server;

	@Autowired
	private HeadersDefaultPier headersPier;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	public EnderecoPierRest(RestTemplate restTemplate) {

		this.restTemplate = restTemplate;
	}

	/**
      * consultaEndereco
	 * método responsável por consultar o endereço de uma pessoa física no pier
	 *
	 * @param idPessoa
      * @return
      */
	public List<EnderecoResponsePier> consultaEndereco(Long idPessoa) {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_ENDERECOS).queryParam("idPessoa", idPessoa);
		HttpHeaders headers = headersPier.creatHeaders();
		HttpEntity<?> entity = new HttpEntity<>(headers);

		try {

			final List<EnderecoResponsePier> origins = new ArrayList<EnderecoResponsePier>();
			ParameterizedTypeReference<PageResponse<EnderecoResponsePier>> responseType = new ParameterizedTypeReference<PageResponse<EnderecoResponsePier>>() {};
			ResponseEntity<PageResponse<EnderecoResponsePier>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, responseType);
			origins.addAll(response.getBody().getContent());
			return origins;

		} catch (HttpClientErrorException e) {
			if(Objects.equals(e.getStatusCode(), HttpStatus.NOT_FOUND)) {
				return Collections.emptyList();
			}
			throw new BadRequestCdt(e.getResponseBodyAsString());
		}
	}

     /**
	 * atualizaOuCriaEndereco
	 * método responsável por atualizar ou criar endereço de pessoa física no pier
	 *
	 * @param parametros
	 * @param method
	 * @return
	 */
	public EnderecoResponsePier atualizaOuCriaEndereco(MultiValueMap parametros, HttpMethod method) {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_ENDERECOS).queryParams(parametros);
		HttpHeaders headers = headersPier.creatHeaders();
		HttpEntity<?> entity = new HttpEntity<>(headers);

		try {

			final ResponseEntity<EnderecoResponsePier> response = this.restTemplate.exchange(builder.build().encode().toUri(), method, entity, EnderecoResponsePier.class);
			return response.getBody();

		} catch (HttpClientErrorException e) {
			throw new BadRequestCdt(e.getResponseBodyAsString());
		}

	}

}
