package br.com.conductor.rhblueapi.rest;

import br.com.conductor.rhblueapi.domain.pier.AgendamentoStatusCartao;
import br.com.conductor.rhblueapi.domain.pier.AgendamentoStatusCartaoResponse;
import br.com.conductor.rhblueapi.domain.pier.CartaoResponse;
import br.com.conductor.rhblueapi.exception.HttpClientErrorPierException;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

@Repository
public class CartaoPierRestRepository {

    private static final String CARTAO_URI = "cartoes";

    @Value("${app.pier.host}${app.pier.basepath}")
    private String pierServer;

    @Autowired
    private HeadersDefaultPier headersPier;

    @Autowired
    private RestTemplate restTemplate;

    public AgendamentoStatusCartaoResponse agendaTrocaStatus(final Long id, final AgendamentoStatusCartao agendamentoStatusCartao) {

        final UriComponentsBuilder builder = fromUriString(pierServer).pathSegment(CARTAO_URI, String.valueOf(id), "status", "agendamento");

        final ResponseEntity<AgendamentoStatusCartaoResponse> resp = this.restTemplate.exchange(builder.toUriString(), POST,
                new HttpEntity<AgendamentoStatusCartao>(agendamentoStatusCartao, headersPier.creatHeaders()), AgendamentoStatusCartaoResponse.class);

        return resp.getBody();
    }

    public void bloquear(final Long id, final Long idStatus, final String motivo) {

        final UriComponentsBuilder builder = fromUriString(pierServer).pathSegment(CARTAO_URI, String.valueOf(id), "bloquear");
        builder.queryParam("id_status", idStatus);
        builder.queryParam("observacao", motivo);

        try {
            this.restTemplate.exchange(builder.toUriString(), POST, new HttpEntity<String>(headersPier.creatHeaders()), Void.class);
        } catch (HttpClientErrorException ex) {
            throw new HttpClientErrorPierException(ex.getResponseBodyAsString());
        }
    }

    public void desbloquear(final Long id) {

        final UriComponentsBuilder builder = fromUriString(pierServer).pathSegment(CARTAO_URI, String.valueOf(id), "desbloquear");

        this.restTemplate.exchange(builder.toUriString(), POST, new HttpEntity<String>(headersPier.creatHeaders()), Void.class);
    }

    public CartaoResponse gerarNovaVia(final Long idCartao) {

        final UriComponentsBuilder builder = fromUriString(pierServer).pathSegment(CARTAO_URI, String.valueOf(idCartao), "gerar-nova-via");

        ResponseEntity<CartaoResponse> cartao = this.restTemplate.exchange(builder.toUriString(), POST, new HttpEntity<>(headersPier.creatHeaders()), CartaoResponse.class);

        return cartao.getBody();
    }
}
