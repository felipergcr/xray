package br.com.conductor.rhblueapi.rest.blue;

import br.com.conductor.rhblueapi.domain.blue.FuncionarioRequest;
import br.com.conductor.rhblueapi.domain.blue.FuncionarioResponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.repository.utils.BlueUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.springframework.http.HttpMethod.GET;

@Repository
public class FuncionarioBlueRestRepository {

    @Autowired
    private BlueUtils blueUtils;

    @Value("${app.blue.host}")
    private String blueApiUri;

    @Autowired
    private RestTemplate restTemplate;

    public List<FuncionarioResponse> consultarFuncionario(final FuncionarioRequest funcionario) {

        final HttpHeaders headers = this.blueUtils.initializeHeaders();

        final StringBuilder builder = this.blueUtils.initializePathBuilder().append("/funcionarios");
        builder.append("?idPessoa=").append(funcionario.getIdPessoa());

        final HttpEntity<String> httpEntity = new HttpEntity<String>(headers);

        final ResponseEntity<PageResponse<FuncionarioResponse>> pageableResponse = this.restTemplate.exchange(builder.toString(), GET, httpEntity, new ParameterizedTypeReference<PageResponse<FuncionarioResponse>>() {
        });

        final PageResponse<FuncionarioResponse> response = pageableResponse.getBody();

        return response.getContent();
    }

}
