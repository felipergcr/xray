
package br.com.conductor.rhblueapi.rest;

import static br.com.conductor.rhblueapi.util.AppConstantes.PIER_ORIGENS_COMERCIAIS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.twsoftware.alfred.object.Objeto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.rhblueapi.domain.pier.OrigensComercialResponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;

@Service
public class OrigensComercialRest {

     @Value("${app.pier.host}${app.pier.basepath}")
     private String server;

     @Autowired
     HeadersDefaultPier headersPier;

     @Autowired
     private RestTemplate restTemplate;

     @Autowired
     public OrigensComercialRest(RestTemplate restTemplate) {

          this.restTemplate = restTemplate;
     }

     public boolean hasOrigemComercial(Long idOrigemComercial) {
          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_ORIGENS_COMERCIAIS).pathSegment(idOrigemComercial.toString());
          HttpHeaders headers = headersPier.creatHeaders();

          ResponseEntity<OrigensComercialResponse> response = this.restTemplate.exchange(builder.toUriString(), HttpMethod.GET, new HttpEntity<String>(headers), OrigensComercialResponse.class);

          return Objeto.notBlank(response.getBody());
     }

     public List<OrigensComercialResponse> listaOrigensComerciais() throws JsonParseException, JsonMappingException, IOException {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_ORIGENS_COMERCIAIS);
          HttpHeaders headers = headersPier.creatHeaders();
          HttpEntity<?> entity = new HttpEntity<>(headers);

          try {
               final List<OrigensComercialResponse> origins = new ArrayList<OrigensComercialResponse>();
               ParameterizedTypeReference<PageResponse<OrigensComercialResponse>> responseType = new ParameterizedTypeReference<PageResponse<OrigensComercialResponse>>() {
               };
               ResponseEntity<PageResponse<OrigensComercialResponse>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, responseType);
               origins.addAll(response.getBody().getContent());
               return origins;

          } catch (HttpClientErrorException e) {
               // TODO: handle exception
               throw new BadRequestCdt(e.getResponseBodyAsString());
          }
     }
}
