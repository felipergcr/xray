package br.com.conductor.rhblueapi.rest;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

import java.util.ArrayList;
import java.util.List;

import br.com.conductor.rhblueapi.domain.response.EmpresaResponse;
import br.com.conductor.rhblueapi.repository.utils.BlueUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Repository
public class EmpresaBlueRestRepository {

    @Value("${app.blue.host}")
    private String blueApiUri;

    @Autowired
    private BlueUtils blueUtils;

    @Autowired
    private RestTemplate restTemplate;

    public List<EmpresaResponse> listarEmpresas(final List<Long> idsEmpresas) {

        final HttpHeaders headers = this.blueUtils.initializeHeaders();
        final HttpEntity<String> httpEntity = new HttpEntity<>(headers);

        final UriComponentsBuilder builder = fromUriString(blueApiUri).pathSegment("empresas");
        idsEmpresas.stream().forEach(id -> builder.queryParam("idsEmpresas", id));

        final ResponseEntity<List<EmpresaResponse>> pageableResponse;
        try {
            pageableResponse = this.restTemplate.exchange(builder.toUriString(), GET, httpEntity, new ParameterizedTypeReference<List<EmpresaResponse>>() {
            });
            return pageableResponse.getBody();
        } catch (HttpClientErrorException e) {
            //log.warn("Empresa não encontrado {} ", idsEmpresas);
        }
        return new ArrayList<EmpresaResponse>();
    }
}
