package br.com.conductor.rhblueapi.rest;

import static br.com.conductor.rhblueapi.util.AppConstantes.PIER_TELEFONES;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.rhblueapi.domain.pier.TelefoneResponse;
import br.com.conductor.rhblueapi.domain.response.PageResponse;
import br.com.conductor.rhblueapi.exception.BadRequestCdt;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;

@Service
@RefreshScope
@Lazy
public class TelefonePierRest {

	@Value("${app.pier.host}${app.pier.basepath}")
	private String server;

	@Autowired
	private HeadersDefaultPier headersPier;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	public TelefonePierRest(RestTemplate restTemplate) {

		this.restTemplate = restTemplate;
	}

	/**
      * consultaTelefone
	 * método responsável por consultar o telefone de uma pessoa física no pier
	 *
	 * @param idPessoa
      * @return
      */
	public List<TelefoneResponse> consultaTelefone(Long idPessoa) {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_TELEFONES).queryParam("idPessoa", idPessoa);
		HttpHeaders headers = headersPier.creatHeaders();
		HttpEntity<?> entity = new HttpEntity<>(headers);

		try {

			final List<TelefoneResponse> origins = new ArrayList<TelefoneResponse>();
			ParameterizedTypeReference<PageResponse<TelefoneResponse>> responseType = new ParameterizedTypeReference<PageResponse<TelefoneResponse>>() {};
			ResponseEntity<PageResponse<TelefoneResponse>> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, entity, responseType);
			origins.addAll(response.getBody().getContent());
			return origins;

		} catch (HttpClientErrorException e) {
			if(Objects.equals(e.getStatusCode(), HttpStatus.NOT_FOUND)) {
				return Collections.emptyList();
			}
			throw new BadRequestCdt(e.getResponseBodyAsString());
		}
	}

     /**
	 * atualizaOuCriaTelefone
	 * método responsável por atualizar ou criar telefone de pessoa física no pier
	 *
	 * @param parametros
	 * @param method
	 * @return
	 */
	public TelefoneResponse atualizaOuCriaTelefone(MultiValueMap parametros, HttpMethod method) {

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_TELEFONES).queryParams(parametros);
		HttpHeaders headers = headersPier.creatHeaders();
		HttpEntity<?> entity = new HttpEntity<>(headers);

		try {

			final ResponseEntity<TelefoneResponse> response = this.restTemplate.exchange(builder.build().encode().toUri(), method, entity, TelefoneResponse.class);
			return response.getBody();

		} catch (HttpClientErrorException e) {
			throw new BadRequestCdt(e.getResponseBodyAsString());
		}

	}

}
