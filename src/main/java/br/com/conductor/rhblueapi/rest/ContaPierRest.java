
package br.com.conductor.rhblueapi.rest;

import static br.com.conductor.rhblueapi.util.AppConstantes.PIER_CONTA;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.rhblueapi.domain.pier.ContaRequestPier;
import br.com.conductor.rhblueapi.domain.pier.ContaResponsePier;
import br.com.conductor.rhblueapi.util.HeadersDefaultPier;

@Service
@RefreshScope
@Lazy
public class ContaPierRest {

     @Value("${app.pier.host}${app.pier.basepath}")
     private String server;

     @Autowired
     private HeadersDefaultPier headersPier;

     @Autowired
     private RestTemplate restTemplate;

     @Autowired
     public ContaPierRest(RestTemplate restTemplate) {

          this.restTemplate = restTemplate;
     }

     public ResponseEntity<ContaResponsePier> criaConta(ContaRequestPier persist) {

          UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_CONTA);
          HttpHeaders headers = headersPier.creatHeaders();
          HttpEntity<?> entity = new HttpEntity<>(persist, headers);

          final ResponseEntity<ContaResponsePier> response = this.restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity, new ParameterizedTypeReference<ContaResponsePier>() {
          });
          return response;

     }
     
     public ContaResponsePier consultarConta(final Long idConta) {
          final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(server).path(PIER_CONTA).queryParam("idConta", idConta);
          final HttpHeaders headers = headersPier.creatHeaders();

          final ResponseEntity<ContaResponsePier> resp = this.restTemplate.exchange(builder.toUriString(), HttpMethod.GET,  new HttpEntity<String>(headers), ContaResponsePier.class);

          return resp.getBody();
     }
}
