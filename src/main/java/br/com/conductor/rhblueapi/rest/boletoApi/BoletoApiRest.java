package br.com.conductor.rhblueapi.rest.boletoApi;

import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.conductor.rhblueapi.domain.request.registroBoleto.RegistroBoletoRequest;

@Component
public class BoletoApiRest {
     
     @Value("${app.boleto.host}")
     private String boletoApiUri;
     
     @Autowired
     private RestTemplate restTemplate;
     
     private static final String PATH_BOLETOS = "/api/boletos";
     
     public void registrarBoleto(final RegistroBoletoRequest registroBoletoRequest) {
          
          final HttpHeaders headers = new HttpHeaders();
          headers.setAccept(singletonList(APPLICATION_JSON_UTF8));
          headers.setContentType(APPLICATION_JSON_UTF8);

          final StringBuilder uri = new StringBuilder(boletoApiUri).append(PATH_BOLETOS).append("/registrar");
          
          final HttpEntity<?> httpEntity = new HttpEntity<Object>(registroBoletoRequest, headers);
          
          try {
               
               final ResponseEntity<String> response = this.restTemplate.exchange(uri.toString(), HttpMethod.POST, httpEntity, String.class);
               
               response.getStatusCodeValue();
               response.getBody();
               
               //TODO: INCLUIR NO MONGO O RESPONSE
          } catch (HttpClientErrorException e) {
               // TODO: INSERIR NO MONGO
               e.getStatusCode();
               e.getMessage();
          } catch (Exception e) {
               //TODO: INSERIR NO MONGO
               e.getMessage();
          }
          
     }
}
