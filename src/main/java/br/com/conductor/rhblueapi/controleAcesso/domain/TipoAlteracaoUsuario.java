
package br.com.conductor.rhblueapi.controleAcesso.domain;

import lombok.Getter;

@Getter
public enum TipoAlteracaoUsuario {
     INSERCAO("I"), ALTERACAO("A"), REMOCAO("R");

     private String id;

     TipoAlteracaoUsuario(final String id) {

          this.id = id;
     }

}