
package br.com.conductor.rhblueapi.controleAcesso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.controleAcesso.domain.PerfisAcessosUsuarios;

public interface PerfisAcessosUsuariosRepository extends JpaRepository<PerfisAcessosUsuarios, Long> {

}
