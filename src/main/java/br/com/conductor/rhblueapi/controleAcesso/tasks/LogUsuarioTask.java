package br.com.conductor.rhblueapi.controleAcesso.tasks;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.controleAcesso.domain.LogUsuarioPK;
import br.com.conductor.rhblueapi.controleAcesso.domain.TipoAlteracaoUsuario;
import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;
import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioLog;
import br.com.conductor.rhblueapi.controleAcesso.repository.LogUsuarioRepositorio;

@Component
public class LogUsuarioTask {
     
     private static final long ID_EMISSOR_DEFAULT = 31l;

     @Autowired
     private LogUsuarioRepositorio logUsuarioRepositorio;
     
     @Async
     public void logUsuario(final Usuario usuarioWS, final TipoAlteracaoUsuario tipoAlteracao) {
          final UsuarioLog log = new UsuarioLog();
          
          BeanUtils.copyProperties(usuarioWS, log);
          log.setTipoAlteracao(tipoAlteracao.getId());
          log.setIdEmissor(ID_EMISSOR_DEFAULT);
          
          final LogUsuarioPK pk = new LogUsuarioPK();
          pk.setIdUsuario(usuarioWS.getId());
          pk.setDataModificacao(usuarioWS.getDataUltimaAtualizacao());
          log.setId(pk);
          
          this.logUsuarioRepositorio.save(log);
     }

}
