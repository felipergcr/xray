
package br.com.conductor.rhblueapi.controleAcesso.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "PERFISACESSOSUSUARIOS")
@Entity
@Data
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@ToString(of = { "id" })
public class PerfisAcessosUsuarios implements Serializable {

     /**
     * 
     */
     private static final long serialVersionUID = -5309717475779107822L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_PERFILACESSOUSUARIO")
     private Long id;

     @NotNull
     @Column(name = "DESCRICAO")
     private String descricao;

}
