
package br.com.conductor.rhblueapi.controleAcesso.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "USUARIOSLOG")
@Entity
@Data
@DynamicInsert
@DynamicUpdate
@EqualsAndHashCode(of = { "id" }, callSuper = false)
@ToString(of = { "id" })
public class UsuarioLog implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;

     @EmbeddedId
     private LogUsuarioPK id;

     @Column(name = "NOME")
     private String nome;

     @Column(name = "LOGIN")
     private String login;

     @NotNull
     @Column(name = "ID_EMISSOR")
     private Long idEmissor;

     @Column(name = "CPF")
     private String cpf;

     @Column(name = "BLOQUEARACESSO")
     private Boolean bloquearAcesso;

     @Column(name = "EMAIL")
     private String email;

     @Column(name = "LOGINUSUARIO")
     private String loginResponsavel;

     @Size(min = 1, max = 1)
     @Column(name = "TIPOALTERACAO")
     private String tipoAlteracao;

}
