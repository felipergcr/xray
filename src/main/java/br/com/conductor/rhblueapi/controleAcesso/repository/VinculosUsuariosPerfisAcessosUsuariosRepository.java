
package br.com.conductor.rhblueapi.controleAcesso.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.controleAcesso.domain.VinculosUsuariosPerfisAcessosUsuarios;

public interface VinculosUsuariosPerfisAcessosUsuariosRepository extends JpaRepository<VinculosUsuariosPerfisAcessosUsuarios, Long> {

     VinculosUsuariosPerfisAcessosUsuarios findOneByIdUsuario(Long idUsuario);

     List<VinculosUsuariosPerfisAcessosUsuarios> findByIdUsuario(Long id);

}
