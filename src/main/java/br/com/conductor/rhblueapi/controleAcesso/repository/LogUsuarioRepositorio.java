package br.com.conductor.rhblueapi.controleAcesso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.controleAcesso.domain.UsuarioLog;

public interface LogUsuarioRepositorio extends JpaRepository<UsuarioLog, Long>{
}
