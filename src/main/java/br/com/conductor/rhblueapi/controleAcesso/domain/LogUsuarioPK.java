package br.com.conductor.rhblueapi.controleAcesso.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Embeddable
public class LogUsuarioPK implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 1L;
     
     @Column(name = "ID_USUARIO")
     private Long idUsuario;
     
     @NotNull
     @Column(name = "DATAALTERACAO")
     private LocalDateTime dataModificacao;

}
