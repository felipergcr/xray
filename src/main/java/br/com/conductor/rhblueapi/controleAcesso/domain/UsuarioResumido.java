
package br.com.conductor.rhblueapi.controleAcesso.domain;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioResumido implements Serializable {

     private static final long serialVersionUID = -9130200726694089977L;

     private Long id;

     private String nome;

     private String login;

     private String cpf;

     private String email;

     private Integer status;

     private Boolean ativo;

     private Long idPlataforma;

}
