package br.com.conductor.rhblueapi.controleAcesso.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "VINCULOSUSUARIOSPERFISACESSOSUSUARIOS")
public class VinculosUsuariosPerfisAcessosUsuarios implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 2336085012475634979L;

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "ID_VINCULOUSUARIOPERFILACESSOUSUARIO")
     private Long id;

     @NotNull
     @Column(name = "ID_USUARIO")
     private Long idUsuario;

     @NotNull
     @Column(name = "ID_PERFILACESSOUSUARIO")
     private Long idPerfilAcessoUsuario;     
     
}
