
package br.com.conductor.rhblueapi.controleAcesso.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.rhblueapi.controleAcesso.domain.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

     List<Usuario> findAllByIdIn(List<Long> id);

     Usuario findByEmailAndIdPlataforma(String login, Long idPlataforma);

     Optional<Usuario> findByCpfAndIdPlataforma(String cpf, long idPlataforma);

     Optional<Usuario> findByIdAndIdPlataforma(long idUsuario, long idPlataforma);

}
