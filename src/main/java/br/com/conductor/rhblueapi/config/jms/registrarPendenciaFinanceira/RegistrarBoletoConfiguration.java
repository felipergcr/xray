
package br.com.conductor.rhblueapi.config.jms.registrarPendenciaFinanceira;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Configuration
public class RegistrarBoletoConfiguration {

     @Bean
     TopicExchange exchangeRegistrarBoleto() {

          return new TopicExchange(RabbitMQConstantes.RegistrarBoleto.EXCHANGE_REGISTRAR_BOLETO);
     }

     @Bean
     FanoutExchange fanoutRegistrarBoleto() {

          return new FanoutExchange(RabbitMQConstantes.RegistrarBoleto.EXCHANGE_REGISTRAR_BOLETO_DLX);
     }

     @Bean
     Queue queueRegistrarBoleto() {

          return QueueBuilder.durable(RabbitMQConstantes.RegistrarBoleto.QUEUE_REGISTRAR_BOLETO).withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.RegistrarBoleto.EXCHANGE_REGISTRAR_BOLETO_DLX).withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.RegistrarBoleto.QUEUE_REGISTRAR_BOLETO_DLQ).build();
     }

     @Bean
     Queue queueRegistraBoletoDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.RegistrarBoleto.QUEUE_REGISTRAR_BOLETO_DLQ).build();
     }

     @Bean
     Binding bindingRegistrarBoleto(Queue queueRegistrarBoleto, TopicExchange exchangeRegistrarBoleto) {

          return BindingBuilder.bind(queueRegistrarBoleto).to(exchangeRegistrarBoleto).with(RabbitMQConstantes.RegistrarBoleto.ROUTING_KEY_REGISTRAR_BOLETO);
     }

     @Bean
     Binding bindingRegistrarBoletoDlq(Queue queueRegistraBoletoDlq, FanoutExchange fanoutRegistrarBoleto) {

          return BindingBuilder.bind(queueRegistraBoletoDlq).to(fanoutRegistrarBoleto);
     }
}
