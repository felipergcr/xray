
package br.com.conductor.rhblueapi.config.jms.limiteCredito;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Configuration
public class LimiteCreditoConfiguration {
     
     
     @Bean
     TopicExchange exchangeLimiteCreditoValidacaoDiaria() {

          return new TopicExchange(RabbitMQConstantes.LimiteCredito.EXCHANGE_VALIDACAO_DIARIA);
     }

     @Bean
     FanoutExchange fanoutLimiteCreditoValidacaoDiaria() {

          return new FanoutExchange(RabbitMQConstantes.LimiteCredito.EXCHANGE_VALIDACAO_DIARIA_DLX);
     }

     @Bean
     Queue queueLimiteCreditoValidacaoDiaria() {

          return QueueBuilder.durable(RabbitMQConstantes.LimiteCredito.QUEUE_VALIDACAO_DIARIA)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.LimiteCredito.EXCHANGE_VALIDACAO_DIARIA_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.LimiteCredito.QUEUE_VALIDACAO_DIARIA_DLQ)
                    .build();
     }

     @Bean
     Queue queueLimiteCreditoValidacaoDiariaDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.LimiteCredito.QUEUE_VALIDACAO_DIARIA_DLQ).build();
     }

     @Bean
     Binding bindingLimiteCreditoValidacaoDiaria(Queue queueLimiteCreditoValidacaoDiaria, TopicExchange exchangeLimiteCreditoValidacaoDiaria) {

          return BindingBuilder.bind(queueLimiteCreditoValidacaoDiaria).to(exchangeLimiteCreditoValidacaoDiaria).with(RabbitMQConstantes.LimiteCredito.ROUTING_KEY_VALIDACAO_DIARIA);
     }

     @Bean
     Binding bindingLimiteCreditoValidacaoDiariaDlq(Queue queueLimiteCreditoValidacaoDiariaDlq, FanoutExchange fanoutLimiteCreditoValidacaoDiaria) {

          return BindingBuilder.bind(queueLimiteCreditoValidacaoDiariaDlq).to(fanoutLimiteCreditoValidacaoDiaria);
     }

}
