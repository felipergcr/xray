package br.com.conductor.rhblueapi.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages="br.com.conductor.rhblueapi.repository.mongo")
public class MongoConfiguration extends AbstractMongoConfiguration {

    @Value("${mongodb.uri}")
    private String mongoUri;

    @Value("${mongodb.replicaSetName}")
    private String replicaSetName;

    @Value("${mongodb.maxConnectionIdleTime:60000}")
    private int maxConnectionIdleTime;

    @Value("${mongodb.maxConnectionTimeout:10000}")
    private int maxConnectionTimeout;

    @Value("${mongodb.minConnectionsPerHost:50}")
    private int minConnectionsPerHost;

    @Value("${mongodb.maxConnectionsPerHost:100}")
    private int maxConnectionsPerHost;

    @Value("${mongodb.sslEnabled:true}")
    private boolean sslEnabled;

    @Value("${mongodb.dataBaseName}")
    private String dataBaseName;

    @Override
    public MongoClient mongoClient() {


        MongoClientOptions.Builder options = MongoClientOptions.builder()
                .maxConnectionIdleTime(maxConnectionIdleTime)
                .connectTimeout(maxConnectionTimeout)
                .minConnectionsPerHost(minConnectionsPerHost)
                .connectionsPerHost(maxConnectionsPerHost)
                .alwaysUseMBeans(true)
                .sslEnabled(sslEnabled)
                .requiredReplicaSetName(replicaSetName);

        MongoClientURI mongoClientURIUri = new MongoClientURI(mongoUri, options);

        return new MongoClient(mongoClientURIUri);
    }

    @Override
    protected String getDatabaseName() {
        return this.dataBaseName;
    }
}
