
package br.com.conductor.rhblueapi.config.jms.pedido;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Configuration
public class CargasConfiguration {

     @Bean
     TopicExchange exchangeImportacao() {

          return new TopicExchange(RabbitMQConstantes.Cargas.EXCHANGE_IMPORTACAO);
     }

     @Bean
     FanoutExchange fanoutImportacao() {

          return new FanoutExchange(RabbitMQConstantes.Cargas.EXCHANGE_IMPORTACAO_DLX);
     }

     @Bean
     Queue queueImportacao() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_IMPORTACAO)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.Cargas.EXCHANGE_IMPORTACAO_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.Cargas.QUEUE_IMPORTACAO_DLQ)
                    .build();
     }

     @Bean
     Queue queueImportacaoDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_IMPORTACAO_DLQ).build();
     }

     @Bean
     Binding bindingImportacao(Queue queueImportacao, TopicExchange exchangeImportacao) {

          return BindingBuilder.bind(queueImportacao).to(exchangeImportacao).with(RabbitMQConstantes.Cargas.ROUTING_KEY_IMPORTACAO);
     }

     @Bean
     Binding bindingImportacaoDlq(Queue queueImportacaoDlq, FanoutExchange fanoutImportacao) {

          return BindingBuilder.bind(queueImportacaoDlq).to(fanoutImportacao);
     }

     @Bean
     TopicExchange exchangeGerenciaDetalhes() {

          return new TopicExchange(RabbitMQConstantes.Cargas.EXCHANGE_GERENCIA_DETALHES);
     }

     @Bean
     FanoutExchange fanoutGerenciaDetalhes() {

          return new FanoutExchange(RabbitMQConstantes.Cargas.EXCHANGE_GERENCIA_DETALHES_DLX);
     }

     @Bean
     Queue queueGerenciaDetalhes() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_GERENCIA_DETALHES)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.Cargas.EXCHANGE_GERENCIA_DETALHES_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.Cargas.QUEUE_GERENCIA_DETALHES_DLQ)
                    .build();
     }

     @Bean
     Queue queueGerenciaDetalhesDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_GERENCIA_DETALHES_DLQ).build();
     }

     @Bean
     Binding bindingGerenciaDetalhes(Queue queueGerenciaDetalhes, TopicExchange exchangeGerenciaDetalhes) {

          return BindingBuilder.bind(queueGerenciaDetalhes).to(exchangeGerenciaDetalhes).with(RabbitMQConstantes.Cargas.ROUTING_KEY_GERENCIA_DETALHES);
     }

     @Bean
     Binding bindingGerenciaDetalhesDlq(Queue queueGerenciaDetalhesDlq, FanoutExchange fanoutGerenciaDetalhes) {

          return BindingBuilder.bind(queueGerenciaDetalhesDlq).to(fanoutGerenciaDetalhes);
     }
     
     @Bean
     TopicExchange exchangeGerenciaFuncionario() {

          return new TopicExchange(RabbitMQConstantes.Cargas.EXCHANGE_GERENCIA_FUNCIONARIO);
     }

     @Bean
     FanoutExchange fanoutGerenciaFuncionario() {

          return new FanoutExchange(RabbitMQConstantes.Cargas.EXCHANGE_GERENCIA_FUNCIONARIO_DLX);
     }

     @Bean
     Queue queueGerenciaFuncionario() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_GERENCIA_FUNCIONARIO)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.Cargas.EXCHANGE_GERENCIA_FUNCIONARIO_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.Cargas.QUEUE_GERENCIA_FUNCIONARIO_DLQ)
                    .build();
     }

     @Bean
     Queue queueGerenciaFuncionarioDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_GERENCIA_FUNCIONARIO_DLQ).build();
     }

     @Bean
     Binding bindingGerenciaFuncionario(Queue queueGerenciaFuncionario, TopicExchange exchangeGerenciaFuncionario) {

          return BindingBuilder.bind(queueGerenciaFuncionario).to(exchangeGerenciaFuncionario).with(RabbitMQConstantes.Cargas.ROUTING_KEY_GERENCIA_FUNCIONARIO);
     }

     @Bean
     Binding bindingGerenciaFuncionarioDlq(Queue queueGerenciaFuncionarioDlq, FanoutExchange fanoutGerenciaFuncionario) {

          return BindingBuilder.bind(queueGerenciaFuncionarioDlq).to(fanoutGerenciaFuncionario);
     }

     @Bean
     TopicExchange exchangeGerarCargas() {

          return new TopicExchange(RabbitMQConstantes.Cargas.EXCHANGE_GERA_CARGAS);
     }

     @Bean
     FanoutExchange fanoutGerarCargas() {

          return new FanoutExchange(RabbitMQConstantes.Cargas.EXCHANGE_GERA_CARGAS_DLX);
     }

     @Bean
     Queue queueGerarCargas() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_GERA_CARGAS)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.Cargas.EXCHANGE_GERA_CARGAS_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.Cargas.QUEUE_GERA_CARGAS_DLQ)
                    .build();
     }

     @Bean
     Queue queueGerarCargasDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_GERA_CARGAS_DLQ).build();
     }

     @Bean
     Binding bindingGerarCargas(Queue queueGerarCargas, TopicExchange exchangeGerarCargas) {

          return BindingBuilder.bind(queueGerarCargas).to(exchangeGerarCargas).with(RabbitMQConstantes.Cargas.ROUTING_KEY_GERA_CARGAS);
     }

     @Bean
     Binding bindingGerarCargasDlq(Queue queueGerarCargasDlq, FanoutExchange fanoutGerarCargas) {

          return BindingBuilder.bind(queueGerarCargasDlq).to(fanoutGerarCargas);
     }
     
     
     @Bean
     TopicExchange exchangeFinalizaPedido() {

          return new TopicExchange(RabbitMQConstantes.Cargas.EXCHANGE_FINALIZA_PEDIDO);
     }

     @Bean
     FanoutExchange fanoutFinalizaPedido() {

          return new FanoutExchange(RabbitMQConstantes.Cargas.EXCHANGE_FINALIZA_PEDIDO_DLX);
     }

     @Bean
     Queue queueFinalizaPedido() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_FINALIZA_PEDIDO)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.Cargas.EXCHANGE_FINALIZA_PEDIDO_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.Cargas.QUEUE_FINALIZA_PEDIDO_DLQ)
                    .build();
     }

     @Bean
     Queue queueFinalizaPedidoDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.Cargas.QUEUE_FINALIZA_PEDIDO_DLQ).build();
     }

     @Bean
     Binding bindingFinalizaPedido(Queue queueFinalizaPedido, TopicExchange exchangeFinalizaPedido) {

          return BindingBuilder.bind(queueFinalizaPedido).to(exchangeFinalizaPedido).with(RabbitMQConstantes.Cargas.ROUTING_KEY_FINALIZA_PEDIDO);
     }

     @Bean
     Binding bindingFinalizaPedidoDlq(Queue queueFinalizaPedidoDlq, FanoutExchange fanoutFinalizaPedido) {

          return BindingBuilder.bind(queueFinalizaPedidoDlq).to(fanoutFinalizaPedido);
     }

}
