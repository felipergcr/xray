
package br.com.conductor.rhblueapi.config.jms.status;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Configuration
public class StatusPagamentoConfiguration {
     
     @Bean
     TopicExchange exchangeStatusPagamento() {

          return new TopicExchange(RabbitMQConstantes.StatusPagamento.EXCHANGE_STATUS_PAGAMENTO);
     }

     @Bean
     FanoutExchange fanoutStatusPagamento() {

          return new FanoutExchange(RabbitMQConstantes.StatusPagamento.EXCHANGE_STATUS_PAGAMENTO_DLX);
     }

     @Bean
     Queue queueStatusPagamento() {

          return QueueBuilder.durable(RabbitMQConstantes.StatusPagamento.QUEUE_STATUS_PAGAMENTO)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.StatusPagamento.EXCHANGE_STATUS_PAGAMENTO_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.StatusPagamento.QUEUE_STATUS_PAGAMENTO_DLQ)
                    .build();
     }

     @Bean
     Queue queueStatusPagamentoDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.StatusPagamento.QUEUE_STATUS_PAGAMENTO_DLQ).build();
     }

     @Bean
     Binding bindingStatusPagamento(Queue queueStatusPagamento, TopicExchange exchangeStatusPagamento) {

          return BindingBuilder.bind(queueStatusPagamento).to(exchangeStatusPagamento).with(RabbitMQConstantes.StatusPagamento.ROUTING_KEY_STATUS_PAGAMENTO);
     }

     @Bean
     Binding bindingStatusPagamentoDlq(Queue queueStatusPagamentoDlq, FanoutExchange fanoutStatusPagamento) {

          return BindingBuilder.bind(queueStatusPagamentoDlq).to(fanoutStatusPagamento);
     }


}
