
package br.com.conductor.rhblueapi.config.jms.unidadeentrega;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Configuration
public class UnidadeEntregaConfiguration {
     
     @Bean
     TopicExchange exchangeImportacaoUE() {

          return new TopicExchange(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_IMPORTACAO);
     }

     @Bean
     FanoutExchange fanoutImportacaoUE() {

          return new FanoutExchange(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_IMPORTACAO_DLX);
     }

     @Bean
     Queue queueImportacaoUE() {

          return QueueBuilder.durable(RabbitMQConstantes.UnidadesEntrega.QUEUE_IMPORTACAO)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.UnidadesEntrega.EXCHANGE_IMPORTACAO_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.UnidadesEntrega.QUEUE_IMPORTACAO_DLQ)
                    .build();
     }

     @Bean
     Queue queueImportacaoUEDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.UnidadesEntrega.QUEUE_IMPORTACAO_DLQ).build();
     }

     @Bean
     Binding bindingImportacaoUE(Queue queueImportacaoUE, TopicExchange exchangeImportacaoUE) {

          return BindingBuilder.bind(queueImportacaoUE).to(exchangeImportacaoUE).with(RabbitMQConstantes.UnidadesEntrega.ROUTING_KEY_IMPORTACAO);
     }

     @Bean
     Binding bindingImportacaoUEDlq(Queue queueImportacaoUEDlq, FanoutExchange fanoutImportacaoUE) {

          return BindingBuilder.bind(queueImportacaoUEDlq).to(fanoutImportacaoUE);
     }
     
     @Bean
     TopicExchange exchangeGerenciaDetalhesUE() {

          return new TopicExchange(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_GERENCIA_DETALHES);
     }

     @Bean
     FanoutExchange fanoutGerenciaDetalhesUE() {

          return new FanoutExchange(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_GERENCIA_DETALHES_DLX);
     }

     @Bean
     Queue queueGerenciaDetalhesUE() {

          return QueueBuilder.durable(RabbitMQConstantes.UnidadesEntrega.QUEUE_GERENCIA_DETALHES)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.UnidadesEntrega.EXCHANGE_GERENCIA_DETALHES_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.UnidadesEntrega.QUEUE_GERENCIA_DETALHES_DLQ)
                    .build();
     }

     @Bean
     Queue queueGerenciaDetalhesUEDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.UnidadesEntrega.QUEUE_GERENCIA_DETALHES_DLQ).build();
     }

     @Bean
     Binding bindingGerenciaDetalhesUE(Queue queueGerenciaDetalhesUE, TopicExchange exchangeGerenciaDetalhesUE) {

          return BindingBuilder.bind(queueGerenciaDetalhesUE).to(exchangeGerenciaDetalhesUE).with(RabbitMQConstantes.UnidadesEntrega.ROUTING_KEY_GERENCIA_DETALHES);
     }

     @Bean
     Binding bindingGerenciaDetalhesUEDlq(Queue queueGerenciaDetalhesUEDlq, FanoutExchange fanoutGerenciaDetalhesUE) {

          return BindingBuilder.bind(queueGerenciaDetalhesUEDlq).to(fanoutGerenciaDetalhesUE);
     }

     @Bean
     TopicExchange exchangeGerarUnidades() {

          return new TopicExchange(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_GERA_UNIDADES);
     }

     @Bean
     FanoutExchange fanoutGerarUnidades() {

          return new FanoutExchange(RabbitMQConstantes.UnidadesEntrega.EXCHANGE_GERA_UNIDADES_DLX);
     }

     @Bean
     Queue queueGerarUnidades() {

          return QueueBuilder.durable(RabbitMQConstantes.UnidadesEntrega.QUEUE_GERA_UNIDADES)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.UnidadesEntrega.EXCHANGE_GERA_UNIDADES_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.UnidadesEntrega.QUEUE_GERA_UNIDADES_DLQ)
                    .build();
     }

     @Bean
     Queue queueGerarUnidadesDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.UnidadesEntrega.QUEUE_GERA_UNIDADES_DLQ).build();
     }

     @Bean
     Binding bindingGerarUnidades(Queue queueGerarUnidades, TopicExchange exchangeGerarUnidades) {

          return BindingBuilder.bind(queueGerarUnidades).to(exchangeGerarUnidades).with(RabbitMQConstantes.UnidadesEntrega.ROUTING_KEY_GERA_UNIDADES);
     }

     @Bean
     Binding bindingGerarUnidadesDlq(Queue queueGerarUnidadesDlq, FanoutExchange fanoutGerarUnidades) {

          return BindingBuilder.bind(queueGerarUnidadesDlq).to(fanoutGerarUnidades);
     }

}
