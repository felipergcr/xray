
package br.com.conductor.rhblueapi.config.jms.relatorios;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.conductor.rhblueapi.util.RabbitMQConstantes;

@Configuration
public class RelatorioDetalhesPedidos {
     
     @Bean
     TopicExchange exchangeDetalhesPedidos() {

          return new TopicExchange(RabbitMQConstantes.Relatorios.EXCHANGE_RELATORIOS_DETALHES_PEDIDOS);
     }

     @Bean
     FanoutExchange fanoutDetalhesPedidos() {

          return new FanoutExchange(RabbitMQConstantes.Relatorios.EXCHANGE_RELATORIOS_DETALHES_PEDIDOS_DLX);
     }

     @Bean
     Queue queueDetalhesPedidos() {

          return QueueBuilder.durable(RabbitMQConstantes.Relatorios.QUEUE_RELATORIOS_DETALHES_PEDIDOS)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_EXCHANGE, RabbitMQConstantes.Relatorios.EXCHANGE_RELATORIOS_DETALHES_PEDIDOS_DLX)
                    .withArgument(RabbitMQConstantes.XDEAD_LETTER_ROUTING_KEY, RabbitMQConstantes.Relatorios.QUEUE_RELATORIOS_DETALHES_PEDIDOS_DLQ)
                    .build();
     }

     @Bean
     Queue queueDetalhesPedidosDlq() {

          return QueueBuilder.durable(RabbitMQConstantes.Relatorios.QUEUE_RELATORIOS_DETALHES_PEDIDOS_DLQ).build();
     }

     @Bean
     Binding bindingDetalhesPedidos(Queue queueDetalhesPedidos, TopicExchange exchangeDetalhesPedidos) {

          return BindingBuilder.bind(queueDetalhesPedidos).to(exchangeDetalhesPedidos).with(RabbitMQConstantes.Relatorios.ROUTING_KEY_RELATORIOS_DETALHES_PEDIDOS);
     }

     @Bean
     Binding bindingDetalhesPedidosDlq(Queue queueDetalhesPedidosDlq, FanoutExchange fanoutDetalhesPedidos) {

          return BindingBuilder.bind(queueDetalhesPedidosDlq).to(fanoutDetalhesPedidos);
     }

}
