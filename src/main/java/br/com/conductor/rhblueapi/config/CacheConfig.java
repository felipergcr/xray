
package br.com.conductor.rhblueapi.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class CacheConfig {

     public static final String LISTA_ACESSO_NIVEIS_USUARIO = "ListaAcessoNiveisUsuario";
     
     public static final String LISTA_STATUS = "ListaStatus";

     public static final String LISTA_STATUS_DESCRICOES = "ListaStatusDescricoes";
     
}
