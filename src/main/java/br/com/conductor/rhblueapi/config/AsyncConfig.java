
package br.com.conductor.rhblueapi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@EnableAsync
@Configuration
public class AsyncConfig {
     
     private final String NAME_PREFIX = "TaskValidacao";

     @Value("${app.thread.poolsize:10}")
     private int threadPoolSize;

     @Bean
     public ApplicationEventMulticaster applicationEventMulticaster(TaskExecutor taskExecutor) {

          SimpleApplicationEventMulticaster eventMulticaster = new SimpleApplicationEventMulticaster();
          eventMulticaster.setTaskExecutor(taskExecutor);
          return eventMulticaster;
     }
     
     @Bean
     public TaskExecutor threadPoolTaskExecutor() {
         ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
         executor.setCorePoolSize(threadPoolSize);
         executor.setMaxPoolSize(threadPoolSize);
         executor.setThreadNamePrefix(NAME_PREFIX);
         executor.initialize();
         return executor;
     }

}
