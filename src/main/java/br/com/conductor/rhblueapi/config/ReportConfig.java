package br.com.conductor.rhblueapi.config;

import br.com.conductor.rhblueapi.util.CurrencyText;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReportConfig {

    @Bean
    public CurrencyText currencyText() {
        return new CurrencyText();
    }
}
