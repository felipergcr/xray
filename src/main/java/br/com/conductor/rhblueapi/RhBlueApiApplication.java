
package br.com.conductor.rhblueapi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@ComponentScan({"br.com.conductor.rhblueapi"})
public class RhBlueApiApplication {

     @Value("${spring.jackson.time-zone:America/Sao_Paulo}")
     private String timeZone;

     @PostConstruct
     public void init(){
          TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
     }

     public static void main(String[] args) {

          SpringApplication.run(RhBlueApiApplication.class, args);
     }
}
