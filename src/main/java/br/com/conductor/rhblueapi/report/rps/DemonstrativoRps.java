
package br.com.conductor.rhblueapi.report.rps;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DemonstrativoRps implements Serializable {

     /**
      * 
      */
     private static final long serialVersionUID = 5667287183402121550L;

     private String numRps;

     private String dtEmissao;

     private String razaoSocial;

     private String numeroReceita;

     private String inscricaoEstadual;

     private String inscricaoMunicipal;

     private String telefone;

     private String endereco;

     private String bairro;

     private String cidade;

     private String uf;

     private String cep;

     private String valorExtenso;

     private String dadosComplementares;

     private String codigoRps;

     private Integer quantidade;

     private String descricao;

     private Double valorTotal;

     private Double baseCalculo;

     private Double iss;

     private Double valorIR;

     private Double valorTotalPedido;

     private String destinatario;

     private String numeroNF;

     private String codigoAutencidade;

     private Long numeroPedido;

    private Integer quantidadePedido;

    private String descricaoPedido;

    private Double valorPedido;

}
