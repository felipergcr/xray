
package br.com.conductor.rhblueapi.report.rps;

import static java.util.stream.Collectors.summingDouble;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.conductor.rhblueapi.domain.view.DadoEmpresaRps;
import br.com.conductor.rhblueapi.domain.view.ProdutoRps;
import br.com.conductor.rhblueapi.report.ReportService;
import br.com.conductor.rhblueapi.util.CurrencyText;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class DemonstrativoRpsService extends ReportService {

     @Autowired
     private ObjectMapper objectMapper;

     @Autowired
     private CurrencyText currencyText;

     public ResponseEntity<?> gerarArquivo(final DadoEmpresaRps dadosEmpresa, final List<ProdutoRps> produtos) {

          List<DemonstrativoRps> demonstrativos = new ArrayList<>(produtos.size());

          produtos.forEach(produto -> demonstrativos.add(DemonstrativoRps.builder().numeroPedido(produto.getNumeroPedido()).descricaoPedido(produto.getNome()).quantidadePedido(produto.getQuantidade()).valorPedido(produto.getValor()).build()));

          Double valorTotal = produtos.stream().collect(summingDouble(produto -> produto.getValor()));

          dadosEmpresa.setValorTotalPedido(valorTotal);
          dadosEmpresa.setValorExtenso(currencyText.write(new BigDecimal(valorTotal)).toUpperCase());

          Map<String, Object> paramentro = objectMapper.convertValue(dadosEmpresa, new TypeReference<Map<String, Object>>(){});
          paramentro.put("dataEmissao", dadosEmpresa.getDataEmissao());

          byte[] demonstrativo = gerarArquivo(new JRBeanCollectionDataSource(demonstrativos), paramentro);

          return downloadPDF(demonstrativo);
     }

     @Override
     protected String getCaminhoRelJasper() {

          return "/report/ben_rps_v1.jasper";
     }
}