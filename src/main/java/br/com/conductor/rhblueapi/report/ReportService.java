
package br.com.conductor.rhblueapi.report;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;
import br.com.conductor.rhblueapi.exception.ExceptionCdt;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.swing.*;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

import static br.com.conductor.rhblueapi.util.AppConstantes.APPLICATION;
import static br.com.conductor.rhblueapi.util.AppConstantes.ATTACHMENT;
import static br.com.conductor.rhblueapi.util.AppConstantes.FileExtension.PDF;

@Slf4j
public abstract class ReportService {

     private String CAMINHO_LOGO_BEN = "/report/Logo_Ben.png";

     private String LOGO_BEN = "LOGO_BEN";

     public byte[] gerarArquivo(JRBeanCollectionDataSource ds, Map<String, Object> parametros) {

          try {
               ImageIcon gto = new ImageIcon(this.getClass().getResource(CAMINHO_LOGO_BEN));

               parametros.put(LOGO_BEN, gto.getImage());

               InputStream jasperStream = this.getClass().getResourceAsStream(getCaminhoRelJasper());
               JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
               JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, ds);

               return JasperExportManager.exportReportToPdf(jasperPrint);
          } catch (Exception e) {
               log.error("Falha ao gerar report", e);
               throw new ExceptionCdt(HttpStatus.INTERNAL_SERVER_ERROR, ExceptionsMessagesCdtEnum.ERRO_AO_GERAR_ARQUIVO.getMessage());
          }
     }

     protected ResponseEntity<?> downloadPDF(byte[] pdf) {

          String nomeArquivo = UUID.randomUUID().toString().concat(".").concat(PDF);
          HttpHeaders headers = new HttpHeaders();
          headers.setContentType(new MediaType(APPLICATION, PDF));
          headers.setContentDispositionFormData(ATTACHMENT, nomeArquivo);
          headers.setContentLength(pdf.length);
          return new ResponseEntity<>(pdf, headers, HttpStatus.OK);
     }

     protected abstract String getCaminhoRelJasper();

}