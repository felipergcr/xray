
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
public enum ModeloDeCartao {

     COM_NOME("COM_NOME"), SEM_NOME("SEM_NOME");

     
     @ApiModelProperty(value = "modelo", notes = "Modelo", position = 1)
     private String modelo;

     ModeloDeCartao(String modelo) {

          this.modelo = modelo;
     }

     @JsonCreator
     public static ModeloDeCartao initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "ModeloDeCartao");
          }
          for (ModeloDeCartao v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "ModeloDeCartao");
     }
}
