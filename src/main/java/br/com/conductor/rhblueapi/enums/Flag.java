package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Flag {

     NAO(0),
     SIM(1),
     ATIVO(1),
     BLOQUEADO(2);

     private Integer flag;

}

