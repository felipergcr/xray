
package br.com.conductor.rhblueapi.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NivelPermissaoEnum {

  GRUPO("GRUPO"),
  SUBGRUPO("SUBGRUPO"),
  EMPRESA("EMPRESA"),
  HIBRIDO("SUBGRUPO_EMPRESA");   
     
  @Getter
  private String tipo;

  @JsonCreator
  public static NivelPermissaoEnum initCreator(String value) throws EnumValidationException {

       if (value == null) {
            throw new EnumValidationException("null", "NivelPermissaoEnum");
       }
       for (NivelPermissaoEnum v : values()) {
            if (value.equals(v.name())) {
                 return v;
            }
       }
       throw new EnumValidationException(value, "NivelPermissaoEnum");
  }

}
