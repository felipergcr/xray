
package br.com.conductor.rhblueapi.enums;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SegmentoEmpresaEnum {

     E1_DIGITAL("E1_DIGITAL"),
     
     E1("E1"),

     E2("E2"),
     
     E2_POLO("E2_POLO"),
     
     E3("E3"),
     
     CORPORATE("CORPORATE"),
     
     GCB("GCB");
     
     @ApiModelProperty(value = "descricao", notes = "Descrição", position = 1)
     private String descricao;
     
     @JsonCreator
     public static SegmentoEmpresaEnum initCreator (String value) throws EnumValidationException {
         if(Objects.isNull(value)) {
             throw new EnumValidationException("null", "SegmentoEmpresaEnum");
         }
         for(SegmentoEmpresaEnum v : values()) {
             if(value.equals(v.name())) {
                 return v;
             }
         }
         throw new EnumValidationException(value, "SegmentoEmpresaEnum");
     }
     
}
