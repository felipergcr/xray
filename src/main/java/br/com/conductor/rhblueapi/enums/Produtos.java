package br.com.conductor.rhblueapi.enums;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Produtos {
     
     VA("Vale Alimentação", "001"),
     VR("Vale Refeição", "002");
     
     @Getter
     private String descricao;
     @Getter
     private String codigo;
     
     public static Produtos findByName(String texto) {
          return Arrays.asList(Produtos.values())
                    .stream()
                    .filter(p -> StringUtils.equals(p.name(), texto.toUpperCase()))
                    .findAny()
                    .orElse(null);
     }
     
     public static Produtos findByCodigo(String codigo) {
          return Arrays.asList(Produtos.values())
                    .stream()
                    .filter(p -> StringUtils.equals(p.getCodigo(), codigo.toUpperCase()))
                    .findAny()
                    .orElse(null);
     }

}
