
package br.com.conductor.rhblueapi.enums;

import java.util.Objects;

import org.junit.Ignore;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusCargaBeneficioEnum {

     PEDIDO_PROCESSADO(1,"Pedido Processado"),
     PENDENCIA_LIMITE(2,"Pendencia de Limite"),
     PEDIDO_CONFIRMADO(3,"Pedido Confirmado"),
     AGUARDANDO_PAGAMENTO(4,"Aguardando Pagamento"),
     CARGA_EFETUADA(5,"Carga Efetuada - Aguardando Confirmação"),
     PEDIDO_CANCELADO(6,"Pedido Cancelado"),
     EXPIRADO(7,"Expirado"),
     CARGA_PROCESSANDO(8,"Carga em Processamento");

     @ApiModelProperty(value = "Prioridade", notes = "Prioridade entre status")
     public Integer prioridade;

     @ApiModelProperty(value = "Status", notes = "Status")
     public String status;

     @JsonCreator
     public static StatusCargaBeneficioEnum initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "StatusArquivoEnum");
          }
          for (StatusCargaBeneficioEnum v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "StatusArquivoEnum");
     }

     public static StatusCargaBeneficioEnum getByDescricao(String descricao) {

          for (StatusCargaBeneficioEnum sae : StatusCargaBeneficioEnum.values()) {
               if (descricao.equals(sae.status)) {
                    return sae;
               }
          }
          return null;
     }

}
