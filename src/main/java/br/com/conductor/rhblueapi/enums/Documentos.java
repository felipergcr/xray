package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Documentos {

     CPF("CPF", 11),
     CNPJ("CNPJ", 14),
     RG("RG", 13);

     @Getter
     private final String descricao;
     @Getter
     private final Integer tamanho;
}
