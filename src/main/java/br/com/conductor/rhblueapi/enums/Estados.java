package br.com.conductor.rhblueapi.enums;

import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Estados {

     AC("Acre"),
     AL("Alagoas"), 
     AP("Amapá"), 
     AM("Amazonas"), 
     BA("Bahia"), 
     CE("Ceará"), 
     DF("Distrito Federal"), 
     ES("Espírito Santo"), 
     GO("Goiás"), 
     MA("Maranhão"), 
     MT("Mato Grosso"), 
     MS("Mato Grosso do Sul"), 
     MG("Minas Gerais"), 
     PA("Pará"), 
     PB("Paraíba"), 
     PR("Paraná"), 
     PE("Pernambuco"), 
     PI("Piauí"), 
     RJ("Rio de Janeiro"), 
     RN("Rio Grandedo Norte"), 
     RS("Rio Grande doS ul"), 
     RO("Rondônia"), 
     RR("Roraima"), 
     SC("Santa Catarina"), 
     SP("São Paulo"), 
     SE("Sergipe"), 
     TO("Tocantins");
     
     @Getter
     private final String descricao;
    
     public static Estados findByName(String texto) {
          return Arrays.asList(Estados.values())
                    .stream()
                    .filter(e -> StringUtils.equals(e.name(), texto.toUpperCase()))
                    .findAny()
                    .orElse(null);
     }
     
     @JsonCreator
     public static Estados initCreator(String value) throws EnumValidationException {
         if(Objects.isNull(value)) {
             throw new EnumValidationException("null", "Estados");
         }
         for(Estados v : values()) {
             if(value.equals(v.name())) {
                 return v;
             }
         }
         throw new EnumValidationException(value, "Estados");
     }
}
