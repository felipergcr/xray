
package br.com.conductor.rhblueapi.enums;

import br.com.conductor.rhblueapi.util.RhHeaderDefinition;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum HeaderPedidoPortaPortaEnum implements RhHeaderDefinition {

     CNPJ("CNPJ", true, 0),
     CODIGO_DA_UNIDADE("CODIGO DA UNIDADE", false, 1),
     MATRICULA("MATRICULA", false, 2),
     NOME_COMPLETO("NOME COMPLETO", true, 3),
     CPF("CPF", true, 4),
     DATA_DE_NASCIMENTO("DATA DE NASCIMENTO", true, 5),
     PRODUTO("PRODUTO", true, 6),
     VALOR_DO_CREDITO("VALOR DO CREDITO", true, 7),
     DATA_DO_CREDITO("DATA DO CREDITO", true, 8),
     LOGRADOURO("LOGRADOURO (TIPO/AV/RUA)", true, 9),
     ENDERECO("ENDERECO", true, 10),
     NUMERO("NUMERO", true, 11),
     COMPLEMENTO("COMPLEMENTO", false, 12),
     BAIRRO("BAIRRO", true, 13),
     CIDADE("CIDADE", true, 14),
     UF("UF", true, 15),
     CEP("CEP", true, 16);

     private String header;
     private boolean obrigatorio;
     private int posicaoCelula;

     @Override
     public String getHeader() {
          return this.header;
     }

     @Override
     public boolean isObrigatorio() {
          return this.obrigatorio;
     }

     @Override
     public int getPosicaoCelula() {
          return this.posicaoCelula;
     }
}