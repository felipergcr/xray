
package br.com.conductor.rhblueapi.enums.relatorios;

import br.com.conductor.rhblueapi.util.relatorio.RelatorioHeaderDefinition;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum RelatorioHeaderPedido implements RelatorioHeaderDefinition {

     ID_PEDIDO("ID Pedido", 0),
     CNPJ("CNPJ", 1),
     RAZAO_SOCIAL("Razão Social", 2),
     GRUPO("Grupo", 3),
     SUBGRUPO("Subgrupo", 4),
     PRODUTO("Produto", 5),
     DT_PEDIDO("Dt Pedido", 6),
     STATUS_PEDIDO("Status do Pedido", 7),
     QTD_FUNC_SOLICITADO("Qtd de Funcionários Solicitado", 8),
     QTD_FUNC_CREDITADO("Qtd de Funcionários Creditado", 9),
     QTD_CARTOES_NOVOS("Qtd de Cartões Novos", 10),
     VALOR_PEDIDO("Valor do Pedido", 11),
     DT_CREDITO_AGENDADA("Dt do Crédito Agendada", 12),
     DT_CREDITO_EFETIVADA("Dt do Crédito Efetivada", 13),
     FORMA_PAGAMENTO("Forma de Pagamento", 14),
     MODALIDADE_PAGAMENTO("Modalidade de Pagamento", 15),
     PRAZO_PAGAMENTO("Prazo de Pagamento", 16),
     DT_VENCIMENTO("Dt do Vencimento", 17),
     VALOR_TOTAL_PAGAMENTO("Valor total do Pagamento", 18),
     STATUS_PAGAMENTO("Status do Pagamento", 19),
     DT_PAGAMENTO("Dt do Pagamento", 20);

     private String header;

     private int posicaoCelula;

     @Override
     public String getHeader() {

          return header;
     }

     @Override
     public int getPosicaoCelula() {

          return posicaoCelula;
     }

}
