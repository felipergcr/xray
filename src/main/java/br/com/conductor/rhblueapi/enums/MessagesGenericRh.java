package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum  MessagesGenericRh {


    CNPJ_NOT_NULL("CNPJ nao pode ser nulo."),
    EMAIL_NOT_NULL("E-mail nao pode ser nulo."),
    ERRO_ENVIO_EMAIL("Erro ao enviar e-mail."),
    TELEFONE_NOT_NULL("Telefone nao pode ser nulo."),
    CANAL_NOT_NULL("O Canal nao pode ser nulo."),
    TELEFONE_TAMANHO_INVALIDO("Telefone deve conter 9 digitos."),
    TELEFONE_DDD_NOT_NULL("DDD do telefone nao pode ser nulo."),
    TELEFONE_DDD_TAMANHO_INVALIDO("DDD do telefone deve conter 2 dígitos."),
    INVALID_CNPJ("CNPJ invalido."),
    INVALID_EMAIL("E-mail invalido."),
    INVALID_TELEFONE("Telefone invalido."),
    CNPJ_NAO_LIBERADO("CNPJ invalido para operacao."),
    CNAE_NAO_VINCULADO("CNAE nao vinculado."),
    CNAE_INVALIDO("CNAE invalido."),
    REQUEST_INVALIDA("Necessario enviar o corpo da request."),
    BANCO_NAO_EXISTE("O codigo do banco informado nao existe."),
    GRUPO_ECONOMICO_INVALIDO("Nao foi possivel cadastrar o grupo economico."),
    TOKEN_NOT_NULL("Token nao pode ser nulo."),
    PIER_SERVICE_REGRAS_DE_NEGOCIO("Problema referente as regras de negocio"),
    PIER_SERVICE_REGRAS_DE_NEGOCIO_ARGS("Problema referente as regras de negocio: %s"),
    CNPJ_INEXISTENTE("CNPJ nao pode ser null."),
    CPF_INEXISTENTE("CNPJ nao pode ser null."),
    CPF_INVALIDO("CPF invalido."),
    PLATAFORM_INVALIDO("Plataforma invalida."),
    PLATAFORMA_INEXISTENTE("Plataforma nao pode ser null."),
    USUARIO_INEXISTENTE("Usuario nao existe."),
    REQUISICAO_MAL_FORMATADO("Json mal formatado ou atributos invalidos para operacao"),
    CPF_NOT_NULL("CPF nao pode ser nulo."),
    INVALID_CPF("CPF invalido"),
    PROBLEMA_COMUNICACAO_NEUROTECH("Problema na validacao dos dados."),
    PROBLEMA_SOCIO_NAO_PRESENTE_NEUROTECH("Socios nao presente no retorno neurotech."),
    PROBLEMA_SOCIO_NAO_PRESENTE_CADEIA("O CPF informado não consta na cadeia societária."),
    TOKEN_INVALIDO("Token informado não localizado."),
    PARAM_OPERACAO_INVALIDO("Parametro de Operação inválido."),
    CARTAO_SENHA_INVALIDA("Senha incorreta"),
    CARTAO_NAO_PERMITIDO("Cartao não permite validação!")
    ;

    @Getter
    private String message;
}
