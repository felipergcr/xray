
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoPagamento {

     BOLETO("BOLETO", "B"), 
     DEBITO("DEBITO", "D"), 
     TED("TED", "T");

     private String tipo;

     private String valor;

     @JsonCreator
     public static TipoPagamento initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "TipoPagamento");
          }
          for (TipoPagamento v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "TipoPagamento");
     }
     
     public static TipoPagamento buscaPorValor(String valor) {
          
          return Arrays.asList(TipoPagamento.values()).stream().filter(tp -> tp.getValor().equals(valor)).findAny().orElse(BOLETO);            
     }
}
