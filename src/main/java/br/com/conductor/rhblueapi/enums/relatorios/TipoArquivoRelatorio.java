
package br.com.conductor.rhblueapi.enums.relatorios;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TipoArquivoRelatorio {
     
     XLS("xls");

     @Getter
     private String tipo; 

}
