package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoStatus {

     ADQUIRENTES("Adquirentes"),
     ARQUIVO_CONCILIACAO_VAN("Arquivo Conciliacao VAN"),
     ARQUIVO_CONCILIACAO_VAN_DETALHES("Arquivo Conciliacao VAN - Detalhes"),
     ARQUIVO_CARGAS("ARQUIVOCARGAS"),
     ARQUIVO_CARGAS_DETALHES("ARQUIVOCARGASDETALHES"),
     CARGAS_BENEFICIOS("CargasBeneficios"),
     EMPRESAS("Empresas"),
     EMPRESAS_CARGAS_DETALHES("EmpresasCargasDetalhes"),
     EMPRESAS_CARGAS_DETALHES_PRODUTOS("EmpresasCargasDetalhesProdutos"),
     EMPRESASTARIFAS("EmpresasTarifas"),
     ESTABELECIMENTOS_PRODUTOS("EstabelecimentosProdutos"),
     FUNCIONARIOS("Funcionarios"),
     FUNCIONARIOS_PRODUTOS("FuncionariosProdutos"),
     PERMISSOES_OPERACOES_RH("PERMISSOESOPERACOESRH"),
     PERMISSOES_USUARIOS_RH("PERMISSOESUSUARIOSRH"),
     USUARIOS_NIVEIS_PERMISSOES_RH("USUARIOSNIVEISPERMISSOESRH"),
     BOLETOS_EMITIDOS("BoletosEmitidos"),
     ARQUIVO_CARGAS_STATUSPAGAMENTO("ARQUIVOCARGAS_StatusPagamento"),
     ARQUIVO_UNIDADE_ENTREGA("ARQUIVOUNIDADEENTREGA"),
     ARQUIVO_UNIDADE_ENTREGA_DETALHE("ARQUIVOUNIDADEENTREGADETALHE"),
     UNIDADES_ENTREGA("UNIDADESENTREGA"),
     RELATORIOS("RELATORIOS")
     ;

     private String nome;
}
