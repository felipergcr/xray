
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoFaturamentoEnum {
     
     CENTRALIZADO(1), DESCENTRALIZADO(0);

     private Integer valor;

     @JsonCreator
     public static TipoFaturamentoEnum initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "TipoFaturamentoEnum");
          }
          for (TipoFaturamentoEnum v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "TipoFaturamentoEnum");
     }

     public static TipoFaturamentoEnum equalsTipo(String tipo) {

          if (TipoFaturamentoEnum.CENTRALIZADO.getValor() == Integer.parseInt(tipo)) {
               return TipoFaturamentoEnum.CENTRALIZADO;
          } else {
               return TipoFaturamentoEnum.DESCENTRALIZADO;
          }
     }
}
