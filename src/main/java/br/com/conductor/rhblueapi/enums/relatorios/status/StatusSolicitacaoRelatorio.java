package br.com.conductor.rhblueapi.enums.relatorios.status;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum StatusSolicitacaoRelatorio {
     
     PROCESSANDO("Processando"),
     NAO_HA_PEDIDOS("Não há pedidos"),
     PROCESSADO("Processado");
     
     @Getter
     private String descricao;
     
     public static StatusSolicitacaoRelatorio findByDescricao(String descricao) {

          return Arrays.asList(StatusSolicitacaoRelatorio.values()).stream().filter(d -> equalsIgnoreCase(d.getDescricao(), descricao)).findAny().orElse(null);
     }

}
