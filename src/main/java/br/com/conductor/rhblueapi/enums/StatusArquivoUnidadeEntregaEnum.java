package br.com.conductor.rhblueapi.enums;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusArquivoUnidadeEntregaEnum {

     RECEBIDO("RECEBIDO"),
     IMPORTADO("IMPORTADO"), 
     INVALIDADO("Invalidado"), 
     PROCESSAMENTO_CONCLUIDO("Processamento Concluido");
     
     @ApiModelProperty(value = "Status", notes = "Status")
     public String status;
     
     @JsonCreator
     public static StatusArquivoUnidadeEntregaEnum initCreator(String value) throws EnumValidationException {
         if(Objects.isNull(value)) {
             throw new EnumValidationException("null", "StatusArquivoEnum");
         }
         for(StatusArquivoUnidadeEntregaEnum v : values()) {
             if(value.equals(v.name())) {
                 return v;
             }
         }
         throw new EnumValidationException(value, "StatusArquivoEnum");
     }

     public static StatusArquivoUnidadeEntregaEnum getByDescricao(String descricao) {
          
          return Arrays.asList(StatusArquivoUnidadeEntregaEnum.values()).stream().filter(d -> equalsIgnoreCase(d.getStatus(), descricao)).findAny().orElse(null);

     }
}
