package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TemplatesExcel {

     ARQUIVO_CARGA_PORTA_A_PORTA("Arquivo para pedido de carga Porta a Porta.", 17, 6, 0),
     ARQUIVO_CARGA_LOTE("Arquivo para pedido de carga Lote.", 9, 6, 0),
     ARQUIVO_UNIDADES_ENTREGA("Arquivo para unidades de entrega.", 17, 6, 0);

     @Getter
     private final String descricao;
     @Getter
     private final Integer quantidadeCelulas;
     @Getter
     private final Integer linhaInicioConteudo;
     @Getter
     private final Integer indiceAba;
}