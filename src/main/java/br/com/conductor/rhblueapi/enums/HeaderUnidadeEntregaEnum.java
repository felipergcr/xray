
package br.com.conductor.rhblueapi.enums;

import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import br.com.conductor.rhblueapi.util.RhHeaderDefinition;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum HeaderUnidadeEntregaEnum implements RhHeaderDefinition {

     CODIGO_LOCAL_ENTREGA("CODIGO LOCAL DE ENTREGA", true, 0), 
     LOGRADOURO_TIPO_AV_RUA("LOGRADOURO (TIPO/AV/RUA)", true, 1), 
     ENDERECO("ENDERECO", true, 2), 
     NUMERO("NUMERO", true, 3), 
     COMPLEMENTO("COMPLEMENTO", false, 4), 
     BAIRRO("BAIRRO", true, 5), 
     CIDADE("CIDADE", true, 6), 
     UF("UF", true, 7), 
     CEP("CEP", true, 8), 
     RESPONSAVEL1("RESPONSAVEL 1", true, 9), 
     TIPO_DOCUMENTO_RESPONSAVEL1("TIPO DO DOCUMENTO OFICIAL RG OU CPF?", true, 10), 
     CPF_RG_RESPONSAVEL1("NUMERO DO DOCUMENTO OFICIAL RG OU CPF - RESPONSAVEL 1", true, 11), 
     TELEFONE_CONTATO_RESPONSAVEL1("TELEFONE DE CONTATO - RESPONSAVEL 1", true, 12), 
     RESPONSAVEL2("RESPONSAVEL 2", true, 13), 
     TIPO_DOCUMENTO_RESPONSAVEL2("TIPO DO DOCUMENTO OFICIAL RG OU CPF?", true, 14), 
     CPF_RG_RESPONSAVEL2("NUMERO DO DOCUMENTO OFICIAL RG OU CPF - RESPONSAVEL 2", true, 15), 
     TELEFONE_CONTATO_RESPONSAVEL2("TELEFONE DE CONTATO - RESPONSAVEL 2", true, 16);

     private String header;

     private boolean obrigatorio;

     private int posicaoCelula;

     public static boolean existeHeaderUnidadeEntregaEnum(String header, int posicaoCelula) {

          HeaderUnidadeEntregaEnum headerUnidadeEntregaEnum =  Arrays.asList(HeaderUnidadeEntregaEnum.values())
                    .stream()
                    .filter(h -> StringUtils.equals(h.getHeader(), header.toUpperCase()) && h.getPosicaoCelula() == posicaoCelula)
                    .findAny()
                    .orElse(null);

          return Objects.nonNull(headerUnidadeEntregaEnum) ? true : false;
     }
}
