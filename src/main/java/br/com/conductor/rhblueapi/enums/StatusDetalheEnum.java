package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatusDetalheEnum {
     
     IMPORTADO(0), VALIDADO(1), INVALIDADO(2);

     private final Integer index;

}
