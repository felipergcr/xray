package br.com.conductor.rhblueapi.enums;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusArquivoEnum {

     RECEBIDO("RECEBIDO"),
     IMPORTADO("IMPORTADO"), 
     INVALIDADO("Invalidado"), 
     PROCESSAMENTO_CONCLUIDO("Processamento Concluido"),
     VALIDADO_PARCIAL("VALIDADO PARCIAL"),
     CANCELADO("Cancelado"),
     CANCELADO_PARCIAL("Cancelado parcial");   
     
     @ApiModelProperty(value = "Status", notes = "Status")
     public String status;
     
     @JsonCreator
     public static StatusArquivoEnum initCreator(String value) throws EnumValidationException {
         if(Objects.isNull(value)) {
             throw new EnumValidationException("null", "StatusArquivoEnum");
         }
         for(StatusArquivoEnum v : values()) {
             if(value.equals(v.name())) {
                 return v;
             }
         }
         throw new EnumValidationException(value, "StatusArquivoEnum");
     }

     public static StatusArquivoEnum getByDescricao(String descricao) {
          
          return Arrays.asList(StatusArquivoEnum.values()).stream().filter(d -> equalsIgnoreCase(d.getStatus(), descricao)).findAny().orElse(null);

     }
}
