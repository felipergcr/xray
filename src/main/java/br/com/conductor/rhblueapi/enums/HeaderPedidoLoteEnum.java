
package br.com.conductor.rhblueapi.enums;

import br.com.conductor.rhblueapi.util.RhHeaderDefinition;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum HeaderPedidoLoteEnum implements RhHeaderDefinition {

     CNPJ("CNPJ", true, 0),
     CODIGO_DA_UNIDADE("CODIGO DO LOCAL DE ENTREGA", true, 1),
     MATRICULA("MATRICULA", false, 2),
     NOME_COMPLETO("NOME COMPLETO", true, 3),
     CPF("CPF", true, 4),
     DATA_DE_NASCIMENTO("DATA DE NASCIMENTO", true, 5),
     PRODUTO("PRODUTO", true, 6),
     VALOR_DO_CREDITO("VALOR DO CREDITO", true, 7),
     DATA_DO_CREDITO("DATA DO CREDITO", true, 8);

     private String header;
     private boolean obrigatorio;
     private int posicaoCelula;

     @Override
     public String getHeader() {
          return this.header;
     }

     @Override
     public boolean isObrigatorio() {
          return this.obrigatorio;
     }

     @Override
     public int getPosicaoCelula() {
          return this.posicaoCelula;
     }
}
