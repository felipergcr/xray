package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoTelefoneEnum {

     Residencial(1, "Residencial"),
     Comercial(2, "Comercial");

     @Getter
     private final Integer id;

     @Getter
     private final String descricao;
}
