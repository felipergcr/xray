package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoPortador {

     TITULAR("T", 1),

     ADICIONAL("A", 0);

     private String tipo;

     private Integer codigo;
}