
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ParametrosEnum {

     TIPO_CONTRATO("RH_TipoContrato","Especifica se o tipo e contrato do RH é Pós ou Pré - (valores: POS ou PRE)", false),
     EMAIL_NF("RH_EmailNotaFiscal ","E-Mail para envio da NF ", false),
     SEGMENTO_EMPRESA("RH_SegmentoEmpresa ","Seguimento da empresa", false),
     TAXA_ADMINISTRACAO("RH_TaxaAdministracao","Valor da Taxa de Administração", false),
     TAXA_ENTREGA("RH_TaxaEntrega","Valor da Taxa de Entrega", false),
     TAXA_DISPONIBILIZACAO_CREDITO("RH_TaxaDisponibilizacaoCredito","Valor da Taxa de Disponibilização de Crédito", false),
     TAXA_EMISSAO_CARTAO("RH_TaxaEmissaoCartao","Valor da Taxa de Emissão de Cartão", false),
     TAXA_REEMISSAO_CARTAO("RH_TaxaReemissaoCartao","Valor da Taxa de Reemissão de Cartão", false),
     VALOR_MINIMO_VA("RH_ValorMinimoVA","Valor mínimo para carga no produto VA", false),
     VALOR_MINIMO_VR("RH_ValorMinimoVR","Valor mínimo para carga no produto VR", false),
     PRAZO_PAGAMENTO("RH_PrazoPagamento","Quantidade de Dias para pagamento do boleto ", false),
     PRAZO_FIDELIDADE("RH_PrazoFidelidade ","Quantidade de Dias para fidelidade", false),
     TRANSFERENCIA_PRODUTOS("RH_TransferenciaProdutos","Indica se permite a transferencia de saldo entre produtos", false),
     FORMA_PAGAMENTO("RH_FormaPagamento","Especifica se o pagamento das cargas é via Boleto (B, false), Débito (D) ou TED (T)", true),
     QTDE_FUNCIONARIOS("RH_QtdeFuncionarios","Quantidade de funcionários da empresa", false),
     MODELO_PEDIDO("RH_ModeloPedido","Solicitação de pedidos feito via Web (WEB) ou Arquivo (ARQ)", false),
     FLAG_EMBOSSA_NOME_PORTADOR("RH_FlagEmbossaNomePortador","Define se o RH embossa cartões com o nome impresso ou não", false),
     LIMITE_CREDITO("RH_LimiteCredito","Limite de crédito caso contrato PósPago", false),
     FLAG_PEDIDO_CENTRALIZADO("RH_FlagPedidoCentralizado","Define se o RH fará pedidos centralizados (1) ou descentralizados (0)", false),
     USA_PRODUTO_VA("RH_UsaProdutoVA","Define se o RH possui produto VA contratado ", false),
     USA_PRODUTO_VR("RH_UsaProdutoVR","Define se o RH possui produto VR contratado ", false),
     TIPO_ENTREGA_CARTAO("RH_TipoEntregaCartao","Como o cartão será entregue?, RH ou endereço portador (PO) ", true),
     FLAG_FATURAMENTO_CENTRALIZADO("RH_FlagFaturamentoCentralizado","Define se o RH fará faturamento de pedidos centralizados (1) ou descentralizados (0)", false),
     QTD_DIAS_CONFIRMA_CARGA("RH_QtdDiasConfirmaCarga ","Qtde de dias antes do agendamento para confirmar a carga agendada", false),
     QTD_DIAS_CANCELA_CARGA("RH_QtdDiasCancelaCarga","Qtde de dias antes do agendamento para cancelar a carga agendada", false),
     VIGENCIA_LIMITE("RH_VigenciaLimite","Data de vigência do limite de crédito", false);

     private final String valor;

     private final String descricao;

     private final boolean exibirParaFront;
     
}