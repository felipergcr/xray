
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoProduto {
     
     VA("VA", 1, "Ben Alimentação"), VR("VR", 2, "Ben Refeição");

     private String tipo;
     
     private Integer identificador;
     
     private String descricaoBen;

     @JsonCreator
     public static TipoProduto initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "TipoProduto");
          }
          for (TipoProduto v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "TipoProduto");
     }

     public static TipoProduto equalsTipo(String produto) {

          TipoProduto tipoProduto = null;

          if (TipoProduto.VA.name().equals(produto)) {
               tipoProduto = TipoProduto.VA;
          } else if (TipoProduto.VR.name().equals(produto)) {
               tipoProduto = TipoProduto.VR;
          }
          return tipoProduto;
     }

     public static TipoProduto obterPorIdentificador(final Integer id) {
          return Arrays.asList(TipoProduto.values()).stream().filter(tp -> tp.getIdentificador() == id).findFirst().orElse(null);
     }
     
}
