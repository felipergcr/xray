package br.com.conductor.rhblueapi.enums;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusPagamentoEnum {

     AGUARDANDO_PAGAMENTO("Aguardando Pagamento"), 
     PAGO("Pago"), 
     PAGO_PARCIALMENTE("Pago Parcialmente"), 
     LIMITE_CREDITO_VENCIDO("Limite de Credito Vencido"),
     LIMITE_CREDITO_INSUFICIENTE("Limite de Credito Insuficiente"), 
     CANCELADO("Cancelado"), 
     CANCELADO_PARCIAL("Cancelado Parcial"), 
     EXPIRADO("Expirado");

     @ApiModelProperty(value = "Status", notes = "Status")
     private String status;

     @JsonCreator
     public static StatusPagamentoEnum initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "StatusPagamentoEnum");
          }
          for (StatusPagamentoEnum v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "StatusPagamentoEnum");
     }

     public static StatusPagamentoEnum getByDescricao(String descricao) {

          for (StatusPagamentoEnum sae : StatusPagamentoEnum.values()) {
               if (descricao.equals(sae.status)) {
                    return sae;
               }
          }
          return null;
     }

}
