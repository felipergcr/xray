package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoEnderecoEnum {

     RESIDENCIAL(1, "Residencial"),
     COMERCIAL(2, "Comercial"),
     CORRESPONDENCIA(3, "Correspondência"),
     FUNCIONARIO(4, "Funcionário");

     private final Integer id;

     private final String descricao;
}