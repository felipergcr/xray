package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TabelasSistema {

     CARTOES(23, "Cartoes"),

     CONTAS(33, "Contas"),

     ESTADOSCONTAS(50, "EstadosContas"),

     PORTADORES(122, "Portadores"),

     LIMITESDISPONIBILIDADES(80, "LimitesDisponibilidades"),

     TELEFONES(146, "Telefones"),

     PESSOAS(118, "Pessoas"),

     CREDORES(41, "Credores"),

     ENDERECOS(46, "Enderecos"),

     PESSOAS_FISICAS_CADASTRO(120,  "PessoasFisicasCadastro"),

     PESSOAS_JURIDICAS(121, "PessoasJuridicas"),

     ESTABELECIMENTOS(50, "Estabelecimentos"),

     ORIGEM_COMERCIAL(91, "OrigensComerciais");

     private Integer id;

     private String nome;
}


