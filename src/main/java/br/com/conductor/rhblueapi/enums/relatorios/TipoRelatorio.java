
package br.com.conductor.rhblueapi.enums.relatorios;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TipoRelatorio {
     
     DETALHES_PEDIDOS("Detalhes Pedidos");

     @Getter
     private String descricao; 

}
