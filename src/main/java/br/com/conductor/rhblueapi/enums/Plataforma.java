
package br.com.conductor.rhblueapi.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.Getter;

public enum Plataforma {

      RH("RH");

     @Getter
     private String tipo;

     private Plataforma(String tipo) {

          this.tipo = tipo;
     }

     @JsonCreator
     public static Plataforma initCreator(String value) throws EnumValidationException {

          if (value == null) {
               throw new EnumValidationException("null", "Plataforma");
          }
          for (Plataforma v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "Plataforma");
     }
}
