package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatusEmpresaEnum {

     PENDENTE(0), ATIVA(1), INATIVA(2);

     @Getter
     private final Integer id;
}
