
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoEntregaCartao {

     RH("RH"), PORTA_A_PORTA("PO");
     
     private String valor;

     @JsonCreator
     public static TipoEntregaCartao initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "TipoEntregaCartao");
          }
          for (TipoEntregaCartao v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "TipoEntrega");
     }
     
     public static TipoEntregaCartao findItemByValue(String valor) {
          
          return Arrays.asList(TipoEntregaCartao.values()).stream().filter(tipoEntrega ->  tipoEntrega.getValor().equals(valor)).findFirst().orElse(null);
     }
     
}
