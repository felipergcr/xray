
package br.com.conductor.rhblueapi.enums;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OperacaoSenhaEnum {
     ESQUECI_SENHA ("esqueci-senha");
     
     @ApiModelProperty(value = "descricao", notes = "Descrição", position = 1)
     private String descricao;
     
     @JsonCreator
     public static OperacaoSenhaEnum initCreator (String value) throws EnumValidationException {
         if(Objects.isNull(value)) {
             throw new EnumValidationException("null", "OperacaoSenhaEnum");
         }
         for(OperacaoSenhaEnum v : values()) {
             if(value.equals(v.name())) {
                 return v;
             }
         }
         throw new EnumValidationException(value, "OperacaoSenhaEnum");
     }
}
