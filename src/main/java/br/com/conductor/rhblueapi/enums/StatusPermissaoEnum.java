package br.com.conductor.rhblueapi.enums;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.Arrays;

import org.junit.Ignore;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusPermissaoEnum {

     ATIVO("ATIVO"), INATIVO("INATIVO"), BLOQUEADO("BLOQUEADO");
     
     @Getter
     private String descricao;

     @JsonCreator
     public static StatusPermissaoEnum initCreator(String value) throws EnumValidationException {

          if (value == null) {
               throw new EnumValidationException("null", "StatusPermissaoEnum");
          }
          for (StatusPermissaoEnum v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "StatusPermissaoEnum");
     }
     
     public static StatusPermissaoEnum findByDescricao(String descricao) {
          
          return Arrays.asList(StatusPermissaoEnum.values()).stream().filter(d -> equalsIgnoreCase(d.getDescricao(), descricao)).findAny().orElse(null);
     } 

}
