package br.com.conductor.rhblueapi.enums;

import org.junit.Ignore;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusEmpresaCargaDetalheProdutoEnum {
     
     PENDENTE("Pendente"), 
     CONFIRMADO("Confirmado"), 
     CANCELADO("Cancelado"), 
     PROCESSADO("Processado"), 
     ERRO_NO_PROCESSAMENTO("Erro no Processamento"), 
     CARGA_EM_PROCESSAMENTO("Carga em Processamento");

     @Getter
     private String status;

     @JsonCreator
     public static StatusEmpresaCargaDetalheProdutoEnum initCreator(String value) throws EnumValidationException {

          if (value == null) {
               throw new EnumValidationException("null", "StatusEmpresaCargaDetalheProdutoEnum");
          }
          for (StatusEmpresaCargaDetalheProdutoEnum v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "StatusEmpresaCargaDetalheProdutoEnum");
     }
}
