
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FormasPagamento {

     TED_DOC("TED/DOC RH", "Ted", "T"),
     BOLETO("Boleto RH", "Boleto", "B"),
     DEBITO_EM_CONTA("Débito em Conta RH", "Débito em Conta", "D");

     private final String chave;

     private final String descricao;
     
     private final String valorParametro;

     public static FormasPagamento obterPorChave(final String chave) {
          return Arrays.asList(FormasPagamento.values()).stream().filter(fp -> fp.getChave().equals(chave)).findFirst().orElse(null);
     }

     public static FormasPagamento obterPorValorParametro(String valorParametro) {

          return Arrays.asList(FormasPagamento.values()).stream().filter(fp -> fp.getValorParametro().equals(valorParametro)).findAny().orElse(null);
     }

}
