package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum OperacoesEnum {
     
     GERAR_NOVO_PEDIDO("Gerar Novo Pedido");
     
     @Getter
     private final String descricao;

}
