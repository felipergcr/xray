
package br.com.conductor.rhblueapi.enums;

import org.junit.Ignore;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusPermissaoOperacaoRhEnum {

     ATIVO("ATIVO"), INATIVO("INATIVO");

     @Getter
     private String tipo;

     @JsonCreator
     public static StatusPermissaoOperacaoRhEnum initCreator(String value) throws EnumValidationException {

          if (value == null) {
               throw new EnumValidationException("null", "StatusNivelPermissaoAcessoEnum");
          }
          for (StatusPermissaoOperacaoRhEnum v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "StatusNivelPermissaoAcessoEnum");
     }

}
