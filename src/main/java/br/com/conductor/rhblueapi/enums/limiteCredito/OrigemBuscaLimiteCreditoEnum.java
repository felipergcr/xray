
package br.com.conductor.rhblueapi.enums.limiteCredito;

public enum OrigemBuscaLimiteCreditoEnum {
     
     GERENCIADOR_LIMITE_DIARIO,
     ENDPOINT_BUSCA_LIMITE_DIARIO,
     GERENCIADOR_CARGAS_PEDIDO;

}
