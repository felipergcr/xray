
package br.com.conductor.rhblueapi.enums;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusUnidadesEntrega {

     ATIVO("ATIVO"), INATIVO("INATIVO"), PROCESSADO("PROCESSADO");

     @Getter
     private String descricao;

     @JsonCreator
     public static StatusUnidadesEntrega initCreator(String value) throws EnumValidationException {

          if (value == null) {
               throw new EnumValidationException("null", "StatusUnidadesEntrega");
          }
          for (StatusUnidadesEntrega v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "StatusUnidadesEntrega");
     }

     public static StatusUnidadesEntrega findByDescricao(String descricao) {

          return Arrays.asList(StatusUnidadesEntrega.values()).stream().filter(d -> equalsIgnoreCase(d.getDescricao(), descricao)).findAny().orElse(null);
     }

}
