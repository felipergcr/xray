
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransferenciaProdutos {
     
     HABILITADA("HABILITADA"), DESABILITADA("DESABILITADA");

     private String transferencia;

     @JsonCreator
     public static TransferenciaProdutos initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "TransferenciaProdutos");
          }
          for (TransferenciaProdutos v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "TransferenciaProdutos");
     }
}
