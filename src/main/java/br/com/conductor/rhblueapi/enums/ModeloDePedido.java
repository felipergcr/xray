package br.com.conductor.rhblueapi.enums;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

public enum ModeloDePedido {
	WEB("WEB"), TRANSFERENCIA_ARQUIVO("TRANSFERENCIA_ARQUIVO");

	@Getter
	@ApiModelProperty(value = "modelo", notes = "Modelo", position = 1)
	private String modelo;

	ModeloDePedido(String modelo) {
		this.modelo = modelo;
	}
	
     @JsonCreator
     public static ModeloDePedido initCreator (String value) throws EnumValidationException {
         if(Objects.isNull(value)) {
             throw new EnumValidationException("null", "ModeloDePedido");
         }
         for(ModeloDePedido v : values()) {
             if(value.equals(v.name())) {
                 return v;
             }
         }
         throw new EnumValidationException(value, "ModeloDePedido");
     }
}
