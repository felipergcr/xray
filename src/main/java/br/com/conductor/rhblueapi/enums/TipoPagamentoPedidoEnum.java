package br.com.conductor.rhblueapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoPagamentoPedidoEnum {

     PRE("PRE", "Pré-Pago"),
     POS("POS", "Pós-Pago")
     ;

     private String nome;
     
     private String descricao;
     
     public static TipoPagamentoPedidoEnum getByNome(String nome) {

          for (TipoPagamentoPedidoEnum tp : TipoPagamentoPedidoEnum.values()) {
               if (nome.equals(tp.nome)) {
                    return tp;
               }
          }
          return null;
     }
}
