package br.com.conductor.rhblueapi.enums;

public enum StatusPagamentoFinanceiroEnum {
     
     INATIVO,
     ATIVO,
     BLOQUEADO,
     CANCELADO;

}
