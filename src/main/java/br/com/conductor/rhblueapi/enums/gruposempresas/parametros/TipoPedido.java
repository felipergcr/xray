
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoPedido {

     CENTRALIZADO(1), DESCENTRALIZADO(0);

     private Integer tipo;

     @JsonCreator
     public static TipoPedido initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "TipoPedido");
          }
          for (TipoPedido v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "TipoPedido");
     }

     public static TipoPedido equalsTipo(String tipo) {

          TipoPedido tipoPedido = null;

          if (TipoPedido.CENTRALIZADO.getTipo() == Integer.parseInt(tipo)) {
               tipoPedido = TipoPedido.CENTRALIZADO;
          } else if (TipoPedido.DESCENTRALIZADO.getTipo() == Integer.parseInt(tipo)) {
               tipoPedido = TipoPedido.DESCENTRALIZADO;
          }
          return tipoPedido;
     }
}
