
package br.com.conductor.rhblueapi.enums;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusArquivoUnidadeEntregaDetalhe {

     IMPORTADO("IMPORTADO"), INVALIDADO("INVALIDADO"), VALIDADO("VALIDADO");

     @ApiModelProperty(value = "Status", notes = "Status")
     public String status;

     @JsonCreator
     public static StatusArquivoUnidadeEntregaDetalhe initCreator(String value) throws EnumValidationException {

          if (Objects.isNull(value)) {
               throw new EnumValidationException("null", "StatusArquivoUnidadeEntregaDetalhe");
          }
          for (StatusArquivoUnidadeEntregaDetalhe v : values()) {
               if (value.equals(v.name())) {
                    return v;
               }
          }
          throw new EnumValidationException(value, "StatusArquivoUnidadeEntregaDetalhe");
     }

     public static StatusArquivoUnidadeEntregaDetalhe getByDescricao(String descricao) {

          return Arrays.asList(StatusArquivoUnidadeEntregaDetalhe.values()).stream().filter(d -> equalsIgnoreCase(d.getStatus(), descricao)).findAny().orElse(null);

     }

}
