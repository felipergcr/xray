
package br.com.conductor.rhblueapi.enums.gruposempresas.parametros;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoContratoEnum {

     PRE("Pré-Pagamento"), POS("Pós-Pagamento");

     private final String descricao;

     public static TipoContratoEnum getByName(final String name) {

          return Arrays.asList(TipoContratoEnum.values()).stream().filter(d -> equalsIgnoreCase(d.name(), name)).findAny().orElse(TipoContratoEnum.PRE);
     }
}
