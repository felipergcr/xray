package br.com.conductor.rhblueapi.exception;

import static br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum.GLOBAL_ERRO_SERVIDOR;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import br.com.conductor.rhblueapi.domain.exception.ExceptionsMessagesCdtEnum;

public class ExceptionsMessagesCdtEnumTest {

	@Test
	public void getEnum() {
		assertThat(ExceptionsMessagesCdtEnum.getEnum(GLOBAL_ERRO_SERVIDOR.getMessage()))
				.isEqualTo(GLOBAL_ERRO_SERVIDOR);
	}

}
