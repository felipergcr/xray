package br.com.conductor.rhblueapi.exception;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Method;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.client.HttpClientErrorException;

import br.com.conductor.rhblueapi.domain.exception.ErroInfo;
import br.com.conductor.rhblueapi.exception.AsyncLogException;
import br.com.conductor.rhblueapi.exception.ExceptionCdt;
import br.com.conductor.rhblueapi.exception.GlobalExceptionHandler;

@RunWith(SpringRunner.class)
public class GlobalExceptionHandlerTest {
	
	private static final String ANY_MESSAGE = "any message";
	
	private static final String FALHA_NO_REQUEST_MSG_PATTERN = "Falha no request: Objeto[%s] Campo[%s] Valor[%s]";

	@Mock
	private MockHttpServletRequest request;
	
	@Mock
	private AsyncLogException asyncLogException;

	@InjectMocks
	private GlobalExceptionHandler globalExceptionHandler;

	@Test
	public void handleExceptionServerError() {
		Exception mockedException = new Exception();
		ResponseEntity<ErroInfo> actualErrorInfo = globalExceptionHandler.handleExceptionServerError(request, mockedException);
		assertThat(actualErrorInfo).isNotNull();
		assertThat(actualErrorInfo.getStatusCode().is5xxServerError()).isTrue();
		assertThat(actualErrorInfo.getBody()).isNotNull();
		assertThat(actualErrorInfo.getBody().getException()).isEqualTo(mockedException.getClass().getSimpleName());
	}
	
	@Test
	public void handleExceptionCdt() {
		ExceptionCdt mockedException = new ExceptionCdt(HttpStatus.BAD_REQUEST, ANY_MESSAGE);
		ResponseEntity<ErroInfo> actualErrorInfo = globalExceptionHandler.handleExceptionCdt(request, mockedException);
		assertThat(actualErrorInfo).isNotNull();
		assertThat(actualErrorInfo.getBody()).isNotNull();
		assertThat(actualErrorInfo.getBody().getException()).isEqualTo(mockedException.getClass().getSimpleName());
	}

	@Test
	public void handleHttpClientErrorException() {
		HttpClientErrorException mockedException = new HttpClientErrorException(HttpStatus.BAD_REQUEST, ANY_MESSAGE);
		ResponseEntity<ErroInfo> actualErrorInfo = globalExceptionHandler.handleHttpClientErrorException(request, mockedException);
		assertThat(actualErrorInfo).isNotNull();
		assertThat(actualErrorInfo.getBody()).isNotNull();
		assertThat(actualErrorInfo.getBody().getException()).isEqualTo(mockedException.getClass().getSimpleName());
	}
	
	@Test
	public void handleMethodArgumentNotValidException() {
		Method method = asList(GlobalExceptionHandler.class.getMethods()).stream().findAny().orElse(null);;
		MethodParameter parameter=new MethodParameter(method, 0);
		String objectName = "objectName";
		String defaultMessage = "obrigatorio";
		String field = "field1";

		BindingResult bindingResult = new BeanPropertyBindingResult("aaa", objectName);
		bindingResult.addError(new FieldError(objectName, field, defaultMessage));

		MethodArgumentNotValidException mockedException = new MethodArgumentNotValidException(parameter, bindingResult);
		ErroInfo actualErrorInfo = globalExceptionHandler.handleMethodArgumentNotValidException(request, mockedException);
		assertThat(actualErrorInfo).isNotNull();
		assertThat(actualErrorInfo.getException()).isEqualTo(mockedException.getClass().getSimpleName());
		assertThat(actualErrorInfo.getMessages().size()).isGreaterThan(0);
		assertThat(actualErrorInfo.getMessages().get(0)).isEqualTo(format(FALHA_NO_REQUEST_MSG_PATTERN, objectName, field, null));
	}
	
}
