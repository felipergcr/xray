
package br.com.conductor.rhblueapi.domain.persist;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class EmpresaPersistTest {

     final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

     private Validator validator = validatorFactory.getValidator();

     private EmpresaPersist empresa;

     private EnderecoPersist endereco;

     private TelefonePersist telefone;

     private EmpresaContatoPersist empresaContatoPersist;
     
     private EmpresaContatoPersist firstContatoPersist;

     @Before
     public void setUp() {

          endereco = new EnderecoPersist();
          endereco.setBairro(RandomStringUtils.randomAlphabetic(40));
          endereco.setCep(RandomStringUtils.randomNumeric(8));
          endereco.setCidade(RandomStringUtils.randomAlphabetic(50));
          endereco.setComplemento(RandomStringUtils.randomAlphabetic(30));
          endereco.setNumero(RandomUtils.nextInt());
          endereco.setUf(RandomStringUtils.randomAlphabetic(2));
          endereco.setLogradouro(RandomStringUtils.randomAlphabetic(2));

          telefone = new TelefonePersist();
          telefone.setDdd("83");
          telefone.setTelefone("996413058");

          empresaContatoPersist = EmpresaContatoPersist
                    .builder()
                    .cpf("44718472369")
                    .email("email@conductor.com.br")
                    .nome(RandomStringUtils.randomAlphabetic(30))
                    .dddTel("11")
                    .numTel("922122495")
                    .dataNascimento(LocalDate.now())
                    .build();

          List<EmpresaContatoPersist> contatoPersistList = Arrays.asList(empresaContatoPersist);

          empresa = new EmpresaPersist();
          empresa.setCnpj("18135168000170");
          empresa.setEndereco(endereco);
          empresa.setRazaoSocial(RandomStringUtils.randomAlphabetic(30));
          empresa.setAgencia(RandomStringUtils.randomAlphabetic(4));
          empresa.setBanco(Long.valueOf(RandomStringUtils.randomNumeric(4)));
          empresa.setDvContaCorrente(RandomStringUtils.randomAlphabetic(2));
          empresa.setNomeExibicao(RandomStringUtils.randomAlphabetic(20));
          empresa.setContaCorrente(RandomStringUtils.randomAlphabetic(15));
          empresa.setDescricao(RandomStringUtils.randomAlphabetic(200));
          empresa.setDataContrato(LocalDate.now());
          empresa.setTelefone(telefone);
          empresa.setContatos(contatoPersistList);
          empresa.setNomeFantasia(RandomStringUtils.randomAlphabetic(50));

          firstContatoPersist = empresa.getContatos().get(0);

     }

     @After
     public void closeUp() {

          telefone = null;
          endereco = null;
          empresa = null;
     }
     
     @Test
     public void testFailNomeExibicaoCaracteresMaiorQue100() {
          empresa.setNomeExibicao(RandomStringUtils.randomAlphabetic(101));
          
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);
          TestCase.assertTrue(violations.size() >= 0);
     }

     @Test
     public void testRazaoSocialCaractaresMaiorQue100Caracteres() {

          empresa.setRazaoSocial(RandomStringUtils.randomAlphabetic(101));
          
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);
          TestCase.assertTrue(violations.size() > 0);
     }
     
     @Test
     public void testRazaoSocialOk() {

          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);
          assertThat(violations).isEmpty();
     }
     
     @Test
     public void testDescricaoCaractaresMaiorQue100Caracteres() {

          empresa.setDescricao(RandomStringUtils.randomAlphabetic(201));
          
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);
          TestCase.assertTrue(violations.size() > 0);
     }
     
     @Test
     public void testOK() {

          
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);
          assertThat(violations).isEmpty();
     }

     @Test
     public void testFailNullAllParams() {

          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(new EmpresaPersist());
          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testCPFNull() {

          firstContatoPersist.setCpf(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailCPFEmpty() {

          firstContatoPersist.setCpf("");
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailCPFInvalido() {

          firstContatoPersist.setCpf(RandomStringUtils.randomNumeric(60));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNumTelContatoNull() {

          firstContatoPersist.setNumTel(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNumTelContatoEmpty() {

          firstContatoPersist.setNumTel("");
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNumTelContatoLimiteDeCaracteres() {

          firstContatoPersist.setNumTel(RandomStringUtils.randomNumeric(60));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNomeExibicaoNull() {

          empresa.setNomeExibicao(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNomeExibicaoEmpty() {

          empresa.setNomeExibicao("");
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNomeExibicaoLimiteDeCaracteres() {

          empresa.setNomeExibicao(RandomStringUtils.randomNumeric(60));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() >= 0);
     }

     @Test
     public void testFailNomeContatoNull() {

          firstContatoPersist.setNome(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNomeContatoEmpty() {

          firstContatoPersist.setNome("");
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNomeContatoLimiteDeCaracteres() {

          firstContatoPersist.setNome(RandomStringUtils.randomNumeric(60));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailDDDNull() {

          firstContatoPersist.setDddTel(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailDDDEmpty() {

          firstContatoPersist.setDddTel("");
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailDDDLimiteDeCaracteres() {

          firstContatoPersist.setDddTel(RandomStringUtils.randomNumeric(20));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailContaCorrenteLimiteDeCaracteres() {

          empresa.setContaCorrente(RandomStringUtils.randomNumeric(20));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailAgenciaLimiteDeCaracteresExecedido() {

          empresa.setAgencia(RandomStringUtils.randomNumeric(10));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNullTelefoneParams() {

          empresa.setTelefone(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailNullEnderecoParams() {

          empresa.setEndereco(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailEnderecoBairroNull() {

          endereco.setBairro(null);
          empresa.setEndereco(endereco);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailEnderecoCEP() {

          endereco.setCep(null);
          empresa.setEndereco(endereco);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailEnderecoCidade() {

          endereco.setCidade(null);
          empresa.setEndereco(endereco);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailEnderecoComplementoMaiorQue50caracteres() {

          endereco.setComplemento(RandomStringUtils.randomAlphabetic(52));
          empresa.setEndereco(endereco);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailEnderecoLogradoro() {

          endereco.setLogradouro(null);
          empresa.setEndereco(endereco);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailEnderecoNumero() {

          endereco.setNumero(null);
          empresa.setEndereco(endereco);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailEnderecoUF() {

          endereco.setUf(null);
          empresa.setEndereco(endereco);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }

     @Test
     public void testFailEmailCom101Caracteres() {

          firstContatoPersist.setEmail(RandomStringUtils.randomAlphabetic(101) + "@" + ".com.br");
          empresa.setEndereco(endereco);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }
     
     @Test
     public void testFailNomeExibicaoComMaisde20Caracteres() {

          empresa.setNomeExibicao(RandomStringUtils.randomAlphabetic(21));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }
     
     @Test
     public void testFailNomeExibicaoNulo() {

          empresa.setNomeExibicao(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }
     
     @Test
     public void testFailNomeFantasiaComMaisde50Caracteres() {

          empresa.setNomeExibicao(RandomStringUtils.randomAlphabetic(51));
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }
     
     @Test
     public void testFailNomeFantasiaNulo() {

          empresa.setNomeFantasia(null);
          Set<ConstraintViolation<EmpresaPersist>> violations = validator.validate(empresa);

          TestCase.assertTrue(violations.size() > 0);
     }
}
