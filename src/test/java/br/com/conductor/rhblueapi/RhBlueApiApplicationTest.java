package br.com.conductor.rhblueapi;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoClassFilter;
import com.openpojo.reflection.impl.PojoClassFactory;

import br.com.twsoftware.alfred.object.Objeto;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
public class RhBlueApiApplicationTest extends TestCase {
        
     public static final String PACKAGE = "br.com.conductor.rhblueapi";
     
     public static final List<String> PACKAGE_EXCLUDES = Lists.newArrayList("br.com.conductor.rhblueapi.cipher.domain",
               "br.com.conductor.rhblueapi.config",
               "br.com.conductor.rhblueapi.controleAcesso",
               "br.com.conductor.rhblueapi.controleAcesso.domain",
               "br.com.conductor.rhblueapi.controleAcesso.repository",
               "br.com.conductor.rhblueapi.controleAcesso.tasks",
               "br.com.conductor.rhblueapi.domain",
               "br.com.conductor.rhblueapi.domain.blue",
               "br.com.conductor.rhblueapi.domain.exception",
               "br.com.conductor.rhblueapi.domain.persist",
               "br.com.conductor.rhblueapi.domain.pier",
               "br.com.conductor.rhblueapi.domain.proc",
               "br.com.conductor.rhblueapi.domain.request",
               "br.com.conductor.rhblueapi.domain.response",
               "br.com.conductor.rhblueapi.domain.update",
               "br.com.conductor.rhblueapi.domain.view",
               "br.com.conductor.rhblueapi.domain.whitelist",
               "br.com.conductor.rhblueapi.enums",
               "br.com.conductor.rhblueapi.exception",
               "br.com.conductor.rhblueapi.report",
               "br.com.conductor.rhblueapi.report.rps",
               "br.com.conductor.rhblueapi.repository",
               "br.com.conductor.rhblueapi.repository.blue",
               "br.com.conductor.rhblueapi.repository.pier",
               "br.com.conductor.rhblueapi.repository.rabbitmq",
               "br.com.conductor.rhblueapi.repository.utils",
               "br.com.conductor.rhblueapi.repository.view",
               "br.com.conductor.rhblueapi.repository.whitelist",
               "br.com.conductor.rhblueapi.resource",
               "br.com.conductor.rhblueapi.rest",
               "br.com.conductor.rhblueapi.task",
               "br.com.conductor.rhblueapi.util",
               "br.com.conductor.rhblueapi.service.usecase",
               "br.com.conductor.rhblueapi.service.status.pagamento.enumeration",
               "br.com.conductor.rhblueapi.RhBlueApiApplication");
               
     public static final String SUFIXO = "Test";
     
     private boolean packageValid(String name) {

          for (String p : PACKAGE_EXCLUDES) {
               if (name.contains(p)) {
                    return false;
               }
          }
          return true;
     }

     @Test
     public void testPatternsOfTests() {

          System.out.println("Iniciando a Validação de Testes/Java");
          List<PojoClass> klassesJava = PojoClassFactory.getPojoClassesRecursively(PACKAGE, new PojoClassFilter() {

               @Override
               public boolean include(PojoClass pojoClass) {

                    return packageValid(pojoClass.getPackage().getName()) && !pojoClass.getSourcePath().contains("/test-classes/") && !pojoClass.isEnum() && !pojoClass.isNestedClass() && !pojoClass.isInterface() && !pojoClass.isAbstract() && Objeto.isBlank(pojoClass.getAnnotation(Ignore.class));
               }
          });

          System.out.println("---------------------------------------------------------------------");
          System.out.println("Total de Classes Java envolvidas na validação: " + klassesJava.size());

          List<PojoClass> klassesTest = PojoClassFactory.getPojoClassesRecursively(PACKAGE, new PojoClassFilter() {

               @Override
               public boolean include(PojoClass pojoClass) {

                    return packageValid(pojoClass.getPackage().getName()) && pojoClass.getSourcePath().contains("/test-classes/") && !pojoClass.isEnum() && !pojoClass.isNestedClass() && !pojoClass.isInterface();
               }
          });
          System.out.println("Total de Classes de Teste encontradas no projeto: " + klassesTest.size());

          List<PojoClass> semTest = Lists.newArrayList();
          for (PojoClass pj : klassesJava) {

               List<PojoClass> result = klassesTest.stream().filter(k -> k.getName().equals(pj.getName() + SUFIXO)).collect(Collectors.toList());
               if (Objeto.isBlank(result)) {
                    semTest.add(pj);
               }
          }

          if (Objeto.notBlank(semTest)) {
               
               Object[] params = new Object[]{"Existe", "classe"};
               if(semTest.size()>1) {
                    params = new Object[]{"Existem", "classes"};
               }
               String mf = MessageFormat.format(" \n\n\nAtenção: {0} " + semTest.size() + " {1} sem seu respectivo teste unitário. \nÉ obrigatório implementar testes unitários para todas as classes de serviço (service). \n\n", params);
               
               System.out.println(" ----------------------------------------------------- ");
               System.out.println(mf);

               for (PojoClass pj : semTest) {
                    System.out.println(pj.getName());
                    System.out.println(" \n\n\n" + " package " + pj.getPackage().getName() + ";\n" + " import org.springframework.boot.test.IntegrationTest; \n import org.springframework.test.context.web.WebAppConfiguration; \n import org.junit.runner.RunWith; \n" + " import org.springframework.boot.test.SpringApplicationConfiguration;     \n" + " import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;  \n" + " import br.com.conductor.pier.Aplicacao;  \n" + " import junit.framework.TestCase; \n" + "\n" + " @RunWith(SpringRunner.class)    \n" + " @SpringBootTest(classes = Aplicacao.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)     \n @IntegrationTest  @WebAppConfiguration \n " + " public class " + pj.getClazz().getSimpleName() + "Test {  \n" + "    \n" + " }  \n");
               }
               System.out.println("---------------------------------------------------------------------");
          }else {
               System.out.println("---------------------------------------------------------------------");
               System.out.println("Verificação OK: Todas as classes de serviço possuem testes unitários.");
               System.out.println("---------------------------------------------------------------------");
          }

          assertTrue(semTest.isEmpty());

     }

}