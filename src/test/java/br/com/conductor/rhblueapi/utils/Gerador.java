package br.com.conductor.rhblueapi.utils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;

import br.com.conductor.rhblueapi.domain.persist.CondicoesComerciaisPersist;
import br.com.conductor.rhblueapi.domain.persist.EmpresaContatoPersist;
import br.com.conductor.rhblueapi.domain.persist.EmpresaPersist;
import br.com.conductor.rhblueapi.domain.persist.EnderecoPersist;
import br.com.conductor.rhblueapi.domain.persist.TelefonePersist;
import br.com.conductor.rhblueapi.domain.update.TelefoneUpdate;
import br.com.conductor.rhblueapi.integration.config.Property;
import br.com.twsoftware.alfred.cnpj.CNPJ;
import br.com.twsoftware.alfred.cpf.CPF;

@Component
public class Gerador {

	private Faker faker = new Faker();
	
	public EmpresaPersist gerarEmpresaGenericoUtils() {

		EmpresaPersist empresaPesist = new EmpresaPersist();
		
		EmpresaContatoPersist empresaContatoPersist = new EmpresaContatoPersist();
		
		List<EmpresaContatoPersist> empresaContatoPersistList = new ArrayList<EmpresaContatoPersist>();
		
		empresaContatoPersist = EmpresaContatoPersist
				.builder()
				.email("felipe.roque@conductor.com.br")
				.dataNascimento(LocalDate.now())
				.nome(faker.name().firstName())
				.cpf(CPF.gerar())
				.dddTel(String.valueOf(EntityGenericUtil.gerarDdd()))
				.numTel(String.valueOf("9" + EntityGenericUtil.numerotelefoneValido()))
				.build();

		empresaContatoPersistList = Arrays.asList(empresaContatoPersist);

		empresaPesist = EmpresaPersist
				.builder()
				.descricao(faker.company().name())
				.cnpj(CNPJ.gerar())
				.razaoSocial(faker.name().lastName())
				.dataContrato(LocalDate.now())
				.nomeExibicao(faker.name().firstName() + " Exibicao")
				.nomeFantasia(faker.name().firstName() + " Fantasia")
				.banco(new Long(3))
				.agencia("0001")
				.contaCorrente("1234")
				.dvContaCorrente("1")
				.telefone(gerarTelefoneGenericoUtils())
				.endereco(gerarEnderecoGenericoUtils())
				.flagMatriz(1)
				.contatos(empresaContatoPersistList)
				.build();
			
		return empresaPesist;
	}
	
	public TelefonePersist gerarTelefoneGenericoUtils() {

		return TelefonePersist.builder().ddd(String.valueOf(EntityGenericUtil.gerarDdd()))
				.ramal(RandomStringUtils.randomNumeric(2)).telefone(EntityGenericUtil.numerotelefoneValido()).build();
	}
	
	public TelefoneUpdate gerarTelefoneUpdateGenericoUtils() {
		
		return TelefoneUpdate.builder().ddd(String.valueOf(EntityGenericUtil.gerarDdd()))
				.ramal(RandomStringUtils.randomNumeric(2)).telefone(EntityGenericUtil.numerotelefoneValido()).build();
	}
	
	public EnderecoPersist gerarEnderecoGenericoUtils() {

		return EnderecoPersist.builder().bairro(faker.address().streetName())
				.cep(String.valueOf(EntityGenericUtil.getLong(8))).cidade(faker.address().cityName())
				.complemento(RandomStringUtils.randomAlphabetic(6)).logradouro(RandomStringUtils.randomAlphabetic(3))
				.numero(Integer.valueOf(RandomStringUtils.randomNumeric(4))).uf("SP").codigoIbge(EntityGenericUtil.getLong(8).toString()).build();
	}
	
	public CondicoesComerciaisPersist gerarCondicoesGenericoUtils() {

		return CondicoesComerciaisPersist.builder().taxaAdministracao(BigDecimal.valueOf(20.01))
				.taxaEntrega(BigDecimal.valueOf(20.01)).taxaDisponibilizacaoCredito(BigDecimal.valueOf(20.01))
				.taxaEmissaoCartao(BigDecimal.valueOf(20.01)).taxaReemissaoCartao(BigDecimal.valueOf(20.01))
				.valorMinimoVA(BigDecimal.valueOf(20.01)).valorMinimoVR(BigDecimal.valueOf(20.01)).build();
	}
}
