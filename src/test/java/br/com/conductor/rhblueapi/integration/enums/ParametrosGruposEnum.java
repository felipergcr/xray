package br.com.conductor.rhblueapi.integration.enums;

import java.util.Objects;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.Getter;

public enum ParametrosGruposEnum {

	RH_PRAZOPAGAMENTO("RH_PrazoPagamento");

	@Getter
	private final String nome;
	
	ParametrosGruposEnum(String nome) {
		this.nome = nome;
	}
	
	public static String get(String nome) throws EnumValidationException {
		if (Objects.isNull(nome)) {
			throw new EnumValidationException("null", "ParametrosGruposEnum");
		}
		
		for (ParametrosGruposEnum t : ParametrosGruposEnum.values()) {

			if (nome.equals(t.name())) {
				return t.getNome();
			}

		}
		throw new EnumValidationException(nome, "ParametrosGruposEnum");
	}
}
