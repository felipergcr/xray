package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import br.com.conductor.rhblueapi.domain.ParametrosPersist;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;

public interface GrupoEmpresaStrategy {
	
   public GrupoEmpresaPersist gerar(final ParametrosPersist parametrosPersist);
   
}
