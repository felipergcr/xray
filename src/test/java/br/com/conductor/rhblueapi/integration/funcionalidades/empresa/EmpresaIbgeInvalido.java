package br.com.conductor.rhblueapi.integration.funcionalidades.empresa;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;

import br.com.conductor.rhblueapi.domain.persist.EmpresaContatoPersist;
import br.com.conductor.rhblueapi.domain.persist.EmpresaPersist;
import br.com.conductor.rhblueapi.integration.funcionalidades.endereco.EnderecoFactory;
import br.com.conductor.rhblueapi.integration.funcionalidades.endereco.EnderecoStrategy;
import br.com.conductor.rhblueapi.utils.EntityGenericUtil;
import br.com.conductor.rhblueapi.utils.Gerador;
import br.com.twsoftware.alfred.cnpj.CNPJ;
import br.com.twsoftware.alfred.cpf.CPF;

@Component
public class EmpresaIbgeInvalido implements EmpresaStrategy {

	@Autowired
	private EnderecoFactory enderecoFactory;
	
	@Override
	public EmpresaPersist gerar() {
		
		Faker faker = new Faker();
		Gerador gerador = new Gerador();
		EmpresaPersist empresaPesist = new EmpresaPersist();
		EmpresaContatoPersist empresaContatoPersist = new EmpresaContatoPersist();
		List<EmpresaContatoPersist> empresaContatoPersistList = new ArrayList<EmpresaContatoPersist>();
		EnderecoStrategy endereco = enderecoFactory.cadastro("IBGE_INVALIDO");

		empresaContatoPersist = EmpresaContatoPersist.builder().email("felipe.roque@conductor.com.br")
				.dataNascimento(LocalDate.now()).nome(faker.name().firstName()).cpf(CPF.gerar())
				.dddTel(String.valueOf(EntityGenericUtil.gerarDdd()))
				.numTel(String.valueOf("9" + EntityGenericUtil.numerotelefoneValido())).build();

		empresaContatoPersistList = Arrays.asList(empresaContatoPersist);

		empresaPesist = EmpresaPersist.builder().descricao(faker.company().name()).cnpj(CNPJ.gerar())
				.razaoSocial(faker.name().lastName()).dataContrato(LocalDate.now())
				.nomeExibicao(new StringBuilder(faker.name().firstName()).append(" Exibicao").toString())
				.nomeFantasia(new StringBuilder(faker.name().firstName()).append(" Fantasia").toString())
				.banco(new Long(3)).agencia("0001").contaCorrente("1234").dvContaCorrente("1")
				.telefone(gerador.gerarTelefoneGenericoUtils()).endereco(endereco.gerar())
				.flagMatriz(1).contatos(empresaContatoPersistList).build();

		return empresaPesist;
	}
}
