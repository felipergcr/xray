package br.com.conductor.rhblueapi.integration.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Xray {

	private static String token;
//	private static String client_id;
//	private static String client_secret;

	public Xray() {
		try {
			Xray.token = autenticacao();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private String autenticacao() throws JSONException {
		
		JSONObject body = new JSONObject();
		
		body.put("client_id", "CFB1CAB6AE5E42AD878FC904FD75D42B");
		body.put("client_secret", "94807ca6c119e638831344dba2ebf7a89ccf90b5e46792f0d3c20e84c0f05aef");
			
		final Request request = new Request();

		final ResponseEntity<?> t = request.postAutenticacaoXray("http://xray.cloud.xpand-it.com/api/v1/authenticate",
				new JSONObject(body.toString()), "AUTENTICAÇÃO DO XRAY");
		
		return t.getBody().toString().replace("\"", "");
		
	}
	
    public void exportarTestesCucumber(final String jiraKeys, final String caminho) throws JSONException, IOException {
    	
    	final Request request = new Request();
    	
    	final Map<String, String> urlVariables = new HashMap<String, String>();
    	
    	urlVariables.put("keys", jiraKeys);
    	
		final ResponseEntity<byte[]> response = request.getExportsFeatureXray("http://xray.cloud.xpand-it.com/api/v1/export/cucumber?keys={keys}", token, urlVariables);
        
        Utils.unzip(response.getBody(), caminho);
    }
   
	public void importarResultadosTestesCucumber(String caminho) throws IOException {

		final Request request = new Request();

		final ResponseEntity<?> t = request.postImportResultsXray("http://xray.cloud.xpand-it.com/api/v1/import/execution/cucumber", token, caminho, 
				"IMPORTANDO RESULTADOS PARA O XRAY");
		
		if (t.getStatusCode().equals(HttpStatus.OK)) {
			
		}
	}
}
