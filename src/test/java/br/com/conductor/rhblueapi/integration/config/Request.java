package br.com.conductor.rhblueapi.integration.config;

import static java.util.Collections.singletonList;
import static org.springframework.http.CacheControl.noCache;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class Request {

	public ResponseEntity<?> postAutenticacaoXray(final String url, final JSONObject body, final String msgLog) {
			
		final HttpHeaders headers = new HttpHeaders();
		
		final TestRestTemplate template = new TestRestTemplate();

		headers.setAccept(singletonList(APPLICATION_JSON_UTF8));
		headers.setContentType(APPLICATION_JSON_UTF8);
		headers.setCacheControl(noCache());
		
		final HttpEntity<String> entity = new HttpEntity<>(body.toString(), headers);

		return template.exchange(url, HttpMethod.POST, entity, String.class);

	}
	
	public ResponseEntity<?> postImportResultsXray(final String url, final String tokenBearer, final String caminho, final String msgLog) throws IOException {
		
		final HttpHeaders headers = new HttpHeaders();
		
		final TestRestTemplate template = new TestRestTemplate();
		
		headers.setContentType(APPLICATION_JSON_UTF8);
		headers.add("Authorization", "Bearer "+ tokenBearer);
		
		String json = String.join(" ", Files.readAllLines(Paths.get(caminho), StandardCharsets.UTF_8));
		
		final HttpEntity<String> entity = new HttpEntity<String>(json, headers);
			
		return template.exchange(url, HttpMethod.POST, entity, String.class);
			
	}
	
	public ResponseEntity<byte[]> getExportsFeatureXray(final String url, final String tokenBearer, final Map<String, String> urlVariables) {
		
		final HttpHeaders headers = new HttpHeaders();
		
		TestRestTemplate template = new TestRestTemplate();
		
		headers.add("Authorization", "Bearer " + tokenBearer);
		
		final HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		
		return template.exchange(url, GET, requestEntity, byte[].class, urlVariables);
		
	}
}
