package br.com.conductor.rhblueapi.integration.funcionalidades.endereco;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;

import br.com.conductor.rhblueapi.domain.persist.EnderecoPersist;
import br.com.conductor.rhblueapi.utils.EntityGenericUtil;

@Component
public class EnderecoIbgeInvalido implements EnderecoStrategy {

	@Override
	public EnderecoPersist gerar() {
		
		Faker faker = new Faker();
		
		return EnderecoPersist.builder().bairro(faker.address().streetName())
				.cep(String.valueOf(EntityGenericUtil.getLong(8))).cidade(faker.address().cityName())
				.complemento(RandomStringUtils.randomAlphabetic(6)).logradouro(RandomStringUtils.randomAlphabetic(3))
				.numero(Integer.valueOf(RandomStringUtils.randomNumeric(4))).uf("SP").codigoIbge("ahdcjV").build();
	}

}
