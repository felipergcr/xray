package br.com.conductor.rhblueapi.integration.tests;

import java.io.IOException;

import org.junit.runner.RunWith;

import br.com.conductor.rhblueapi.integration.config.AfterAll;
import br.com.conductor.rhblueapi.integration.config.Property;
import br.com.conductor.rhblueapi.integration.config.Utils;
import br.com.conductor.rhblueapi.integration.config.Xray;
import cucumber.api.CucumberOptions;

@RunWith(SuiteTest.class)
@CucumberOptions(
		features = { "src/test/resources/featureJira" }, 
		glue = { "br.com.conductor.rhblueapi.integration.funcionalidades" }, 
		plugin = {"pretty", "json:target/cucumber.json" },
		strict = true
		)
public class CucumberTest extends BaseTest {

	@AfterAll
	public static void afterAll() throws IOException {
		Xray xray = new Xray();
		xray.importarResultadosTestesCucumber(Property.CUCUMBER_JSON_REPORT_FILEPATH);
		Utils.limparDiretorio("src/test/resources/featureJira");
	}
}
