package br.com.conductor.rhblueapi.integration.enums;

import java.util.Objects;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.Getter;

public enum UtilsEnum {
	
	MENSAGEM("messages"), DADOS_VALIDOS("DADOS_VALIDOS"), IBGE_INVALIDO("IBGE_INVALIDO");

	@Getter
	private final String desc;
	
	UtilsEnum(String desc) {
		this.desc = desc;
	}
	
	public static String get(String desc) throws EnumValidationException {
		if (Objects.isNull(desc)) {
			throw new EnumValidationException("null", "UtilsEnum");
		}
		
		for (UtilsEnum t : UtilsEnum.values()) {

			if (desc.equals(t.name())) {
				return t.getDesc();
			}

		}
		throw new EnumValidationException(desc, "UtilsEnum");
	}

}
