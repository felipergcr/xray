package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.hamcrest.CoreMatchers;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import br.com.conductor.rhblueapi.domain.ParametroGrupoEmpresa;
import br.com.conductor.rhblueapi.domain.ParametrosPersist;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.domain.response.GrupoEmpresaResponse;
import br.com.conductor.rhblueapi.exception.EnumValidationException;
import br.com.conductor.rhblueapi.integration.config.CucumberRoot;
import br.com.conductor.rhblueapi.integration.config.Utils;
import br.com.conductor.rhblueapi.integration.enums.HttpStatusEnum;
import br.com.conductor.rhblueapi.integration.enums.ParametrosGruposEnum;
import br.com.conductor.rhblueapi.integration.enums.UtilsEnum;
import br.com.conductor.rhblueapi.repository.ParametrosGruposEmpresasCustomRepository;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GrupoEmpresaStep extends CucumberRoot {
	
	@Autowired
	private GrupoEmpresaActions grupoEmpresaActions;
	
	@Autowired
	private GrupoEmpresa grupoEmpresaService;
	
	@Autowired
	private GrupoEmpesaFactory grupoEmpesaFactory;
	
	@Autowired
	private ParametrosGruposEmpresasCustomRepository parametrosGruposEmpresasCustomRepository;
	
	@Given("Cadastrar com dados \"([^\"]*)\" grupo com parametros:$")
	public void cadastrar_grupo_com_parametros(final String cenario, final DataTable dt) throws EnumValidationException {
	
		grupoEmpresaService.setParametrosPersist(this.preencherParametros(dt));
		grupoEmpresaService.setGrupoEmpresaPersist(this.preencherGrupoEmpresaPersist(UtilsEnum.get(cenario), grupoEmpresaService.getParametrosPersist()));
	}
	
	@When("Enviar \"([^\"]*)\" para a api \"([^\"]*)\"$")
	public void enviar_requisicao_para_api(final String tipo, final String url) throws EnumValidationException {
		
		grupoEmpresaService.setResponseGrupoEmpresa(this.cadastro_grupo(GrupoEmpresaPathsEnum.getPath(url), tipo));
	}
	
	@And("Validar valor (\\d+) cadastro do parametro \"([^\"]*)\" no \"([^\"]*)\"$")
	public void validar_cadastro_parametro(final Integer valorEsperado, final String codigo, final String tipo) throws EnumValidationException {
		this.validarCadastroParametro(valorEsperado.toString(), ParametrosGruposEnum.get(codigo), tipo);
	}
	
	@Then("^Recebo o codigo de retorno \"([^\"]*)\"$")
	public void recebo_codigo_retorno(String codigo) throws EnumValidationException {
		
		this.validarCodigoRetorno(HttpStatusEnum.get(codigo));
	}
	
	@And("Validar cadastro")
	public void validar_cadastro_grupo() {
		this.validarCadastroGrupo();
	}
	
	@And("^Validar se (.*) contem (.*)$")
	public void recebo_codigo_retorno(final String tipo, final String mensagem) throws EnumValidationException {
		
		this.validarMensagemErro(UtilsEnum.get(tipo), mensagem, grupoEmpresaService.getResponseGrupoEmpresa());
	}
	
	private void validarCadastroGrupo() {
		GrupoEmpresaResponse grupoEmpresaResponse = Utils.retornaClass(grupoEmpresaService.getResponseGrupoEmpresa().getBody().toString(), GrupoEmpresaResponse.class);
		grupoEmpresaActions.validarCadastroGrupo(grupoEmpresaResponse);
	}
	
	private void validarMensagemErro(final String tipo, final String mensagem, final ResponseEntity<?> responseEntity) {
        try {
             JSONObject json = new JSONObject(responseEntity.getBody().toString());
             assertThat(mensagem, CoreMatchers.containsString(json.getString(tipo)));
        } catch (Exception e) {
             e.printStackTrace();
        }
	}
	
	private ParametrosPersist preencherParametros(DataTable dt) {
		List<Map<String, String>> list = dt.asMaps(String.class, String.class);

		ParametrosPersist parametrosPersist = new ParametrosPersist();
		
		for (Map<String, String> item : list ) {
			
			int prazo = Integer.parseInt(item.get("PRAZOPAGAMENTO"));
			int dias = Integer.parseInt(item.get("DATAVIGENCIA"));
			
			parametrosPersist = grupoEmpresaService.preencherParametros(item.get("TIPOFATURAMENTO"), 
					item.get("TIPOCONTRATO"), prazo, item.get("LIMITECREDITO"), 
					item.get("TIPOPEDIDO"), item.get("TIPOENTREGA"), item.get("TIPOPAGAMENTO"), 
					item.get("TIPOPRODUTO"), dias);
		}
		
		return parametrosPersist;
	}
	
	private GrupoEmpresaPersist preencherGrupoEmpresaPersist(final String cenario, final ParametrosPersist parametrosPersist) {
		
		GrupoEmpresaStrategy grupo = grupoEmpesaFactory.cadastro(cenario);
		
		return grupo.gerar(parametrosPersist);
	}
	
	private ResponseEntity<?> cadastro_grupo(final String url, final String tipo) {
		
		return grupoEmpresaService.cadastro(grupoEmpresaService.getGrupoEmpresaPersist(), url, tipo);
	}

	private void validarCodigoRetorno(String codigo) {
		
		assertEquals(codigo, grupoEmpresaService.getResponseGrupoEmpresa().getStatusCode().toString());
	}
	
	private void validarCadastroParametro(final String valorEsperado, final String codigo, final String tipo) {
		
		GrupoEmpresaResponse grupoEmpresaResponse = Utils.retornaClass(grupoEmpresaService.getResponseGrupoEmpresa().getBody().toString(), GrupoEmpresaResponse.class);
		
		List<ParametroGrupoEmpresa> p = parametrosGruposEmpresasCustomRepository.findByIdGrupoEmpresaAndTipoParametroAndCodigo(grupoEmpresaResponse.getId(), tipo, codigo);
		
		assertTrue(p.stream().filter(item -> item.getValor().equals(valorEsperado)).findFirst().isPresent());
		
		grupoEmpresaActions.validarCadastroGrupo(grupoEmpresaResponse);
		
	}
}
