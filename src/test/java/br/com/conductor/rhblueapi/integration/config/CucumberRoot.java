package br.com.conductor.rhblueapi.integration.config;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import br.com.conductor.rhblueapi.RhBlueApiApplication;
import br.com.conductor.rhblueapi.integration.ProfilesResolver;

@SpringBootTest(classes = RhBlueApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles={"Local", "test", "qa", "stg"}, resolver=ProfilesResolver.class)
@ContextConfiguration
public class CucumberRoot {
	
}
