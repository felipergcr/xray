package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.GrupoEmpresa;
import br.com.conductor.rhblueapi.domain.response.GrupoEmpresaResponse;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;

@Component
public class GrupoEmpresaActions {

	@Autowired
	private GrupoEmpresaRepository grupoEmpresaRepository;
	
	public void validarCadastroGrupo(GrupoEmpresaResponse grupoEmpresaResponse) {
		
		Optional<GrupoEmpresa> grupoEmpresa = grupoEmpresaRepository.findById(grupoEmpresaResponse.getId());
		
		assertEquals(grupoEmpresa.get().getId(), grupoEmpresaResponse.getId());
		assertEquals(grupoEmpresa.get().getIdEmpresaPrincipal(), grupoEmpresaResponse.getIdEmpresa());
	}
}
