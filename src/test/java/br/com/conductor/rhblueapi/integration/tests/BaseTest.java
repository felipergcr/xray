package br.com.conductor.rhblueapi.integration.tests;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import org.junit.AfterClass;
import org.junit.AssumptionViolatedException;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.http.HttpStatus;

import com.github.tomakehurst.wiremock.WireMockServer;

import br.com.conductor.rhblueapi.integration.config.Utils;

@RunWith(JUnit4.class)
public class BaseTest {

	private static WireMockServer wireMockServer = new WireMockServer(8181);
	
	@BeforeClass
	public static void getDriver() {
		mockServers();
	}
	
	@AfterClass
	public static void finalizar() {
		Utils.limparDiretorio("/tmp");
		wireMockServer.stop();
	}
	
	 @Rule
     public final TestRule testRule = new TestWatcher() {

          @Override
          protected void succeeded(Description description) {
        	  System.out.println(description);

          }

          @Override
          protected void failed(Throwable e, Description description) {
        	  System.out.println(description);
          }

          @Override
          protected void skipped(AssumptionViolatedException e, Description description) {
        	  System.out.println(description);

          }

          @Override
          protected void starting(Description description) {
        	  System.out.println(description);

          }

          @Override
          protected void finished(Description description) {
        	  System.out.println(description);

          }
     };
	
	private static void mockServers() {

		if (!wireMockServer.isRunning()) {
			wireMockServer.start();
			configureFor("localhost", 8181);
		}

		stubFor(post(urlEqualTo("/v2/api/notificacoes-email")).willReturn(aResponse().withStatus(HttpStatus.OK.value())
				.withHeader("Content-Type", "application/json").withBody("{\"status\":\"MOCK OK\"}")));

		stubFor(post(urlEqualTo("/v2/api/notificacoes/sms")).willReturn(aResponse().withStatus(HttpStatus.OK.value())
				.withHeader("Content-Type", "application/json").withBody("{\"status\":\"MOCK SMS TOKEN OK\"}")));

		stubFor(post(urlEqualTo("/v2/api/notificacoes-sms/gerar-codigo-seguranca"))
				.willReturn(aResponse().withStatus(HttpStatus.OK.value()).withHeader("Content-Type", "application/json")
						.withBody("{\"status\":\"MOCK GERAR TOKEN OK\"}")));

		stubFor(post(urlEqualTo("/v2/api/notificacoes-sms/reenviar-codigo-seguranca"))
				.willReturn(aResponse().withStatus(HttpStatus.OK.value()).withHeader("Content-Type", "application/json")
						.withBody("{\"status\":\"MOCK REENVIAR TOKEN OK\"}")));

		stubFor(post(urlEqualTo("/v2/api/notificacoes-sms/validar-codigo-seguranca"))
				.willReturn(aResponse().withStatus(HttpStatus.OK.value()).withHeader("Content-Type", "application/json")
						.withBody("{\"status\":\"MOCK VALIDAR TOKEN OK\"}")));
	}
}
