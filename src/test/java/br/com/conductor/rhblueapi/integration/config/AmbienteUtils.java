package br.com.conductor.rhblueapi.integration.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class AmbienteUtils {

	@Autowired
    Environment environment;
	
	public String getPort() {
    	return environment.getProperty("local.server.port");
    }
}
