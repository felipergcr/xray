package br.com.conductor.rhblueapi.integration.config;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.lang3.RandomStringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Utils {

    private static final int BUFFER_SIZE = 4096;

	public boolean existeArquivo(String caminhoArquivo) {
		return new File(caminhoArquivo).exists();
	}
	
	private static void remover(File f) {
		if (f.isDirectory()) {
			File[] files = f.listFiles();
			for (int i = 0; i < files.length; ++i) {
				files[i].deleteOnExit();
			}
		}
	}

	public static void limparDiretorio(String path) {
		File destDir = new File(path);
        if (destDir.exists()) {
        	remover(new File(path));
        } else 
        	destDir.mkdir();
        
	}
	
	public static void gravarFeatureDoXray(String feature, String caminho) throws IOException {

		try {
			FileWriter out = new FileWriter(caminho);
			out.write(feature);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void unzip(byte[] data, String dirName) throws IOException {
        ZipInputStream zipIn = new ZipInputStream(new ByteArrayInputStream(data));
        ZipEntry entry = zipIn.getNextEntry();
        
        while (entry != null) {
            String filePath = dirName + "/" + entry.getName();
            if (!entry.isDirectory()) {
                extrairArquivo(zipIn, filePath);
            } else {
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }
	
	private static void extrairArquivo(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
	
	public static String converterObjetoParaString(Object objeto) {
		ObjectMapper mapper = new ObjectMapper();
		String body = null;
		try {
			body = mapper.writeValueAsString(objeto);
		} catch (JsonProcessingException e) {
			System.err.println(new StringBuilder("ERRO AO CONVERTER OBJETO: ").append(e.getMessage()).toString());
		} 
		return body;
	}
	
	public static String convertObjectToString(Object objeto) {
		ObjectMapper mapper = new ObjectMapper();
		String body = null;
		try {
			body = mapper.writeValueAsString(objeto);
		} catch (JsonProcessingException e) {
			System.err.println("Message Error: <br/>" + e.getMessage());
		} 
		return body;
	}
	
    public static String converterTextoSemCaracteres(String text) { 
 		 
		return text.replaceAll("[ãâàáä]", "a")   
                .replaceAll("[êèéë]", "e")   
                .replaceAll("[îìíï]", "i")   
                .replaceAll("[õôòóö]", "o")   
                .replaceAll("[ûúùü]", "u")   
                .replaceAll("[ÃÂÀÁÄ]", "A")   
                .replaceAll("[ÊÈÉË]", "E")   
                .replaceAll("[ÎÌÍÏ]", "I")   
                .replaceAll("[ÕÔÒÓÖ]", "O")   
                .replaceAll("[ÛÙÚÜ]", "U")   
                .replace('ç', 'c')   
                .replace('Ç', 'C')   
                .replace('ñ', 'n')   
                .replace('Ñ', 'N');
	}
    
    public static String getAlphabetic(Integer size) {

        return RandomStringUtils.randomAlphabetic(size);

   }
    
	public static <T> T retornaClass(final String texto, final Class<T> clazz) {
	
		final ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(texto, clazz);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
