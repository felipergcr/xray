package br.com.conductor.rhblueapi.integration.tests;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;

import br.com.conductor.rhblueapi.integration.config.AfterAll;
import br.com.conductor.rhblueapi.integration.config.BeforeAll;
import br.com.conductor.rhblueapi.integration.config.Property;
import br.com.conductor.rhblueapi.integration.config.Utils;
import br.com.conductor.rhblueapi.integration.config.Xray;
import cucumber.api.junit.Cucumber;

public class SuiteTest extends Runner {

	@SuppressWarnings("rawtypes")
	private Class clazz;
	private Cucumber cucumber;
	
	public SuiteTest(@SuppressWarnings("rawtypes") Class clazzValue) throws Exception {
		Xray xray = new Xray();
		Property.loadProperties();
		Utils.limparDiretorio("src/test/resources/featureJira");
		xray.exportarTestesCucumber(Property.JIRA_ISSUE_KEYS, Property.IMPORTED_SCENARIO_DIRECTORY);
		clazz = clazzValue;
		cucumber = new Cucumber(clazzValue);
	}

	@Override
	public Description getDescription() {
		return cucumber.getDescription();
	}

	private void runPredefinedMethods(@SuppressWarnings("rawtypes") Class annotation) throws Exception {
		if (!annotation.isAnnotation()) {
			return;
		}
		Method[] methodList = this.clazz.getMethods();
		for (Method method : methodList) {
			Annotation[] annotations = method.getAnnotations();
			for (Annotation item : annotations) {
				if (item.annotationType().equals(annotation)) {
					method.invoke(null);
					break;
				}
			}
		}
	}

	@Override
	public void run(RunNotifier notifier) {
		try {
			runPredefinedMethods(BeforeAll.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cucumber.run(notifier);
		try {
			runPredefinedMethods(AfterAll.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

