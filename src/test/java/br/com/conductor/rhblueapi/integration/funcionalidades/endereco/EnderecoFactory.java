package br.com.conductor.rhblueapi.integration.funcionalidades.endereco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EnderecoFactory {

	@Autowired
    private EnderecoIbgeInvalido enderecoIbgeInvalido;
	
	@Autowired
	private EnderecoValido enderecoValido;
	
	public EnderecoStrategy cadastro(String cenario) {

		switch (cenario) {

		case "IBGE_INVALIDO":
			return enderecoIbgeInvalido;

		default:
			return enderecoValido;
		}

	}
}
