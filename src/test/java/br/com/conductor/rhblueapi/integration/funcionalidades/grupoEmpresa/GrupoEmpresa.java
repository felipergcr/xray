package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.ParametrosPersist;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.repository.GrupoEmpresaRepository;
import lombok.Getter;
import lombok.Setter;

@Component
public class GrupoEmpresa {

	@Autowired
	private GrupoEmpresaImpl grupoEmpresaImpl;
	
	@Getter
	@Setter
	private ParametrosPersist parametrosPersist;
	
	@Getter
	@Setter
	private GrupoEmpresaPersist grupoEmpresaPersist;
	
	@Getter
	@Setter
	private ResponseEntity<?> responseGrupoEmpresa;
	
	ParametrosPersist preencherParametros(String tipoFaturamento, String tipoContrato, int prazoPagamento, String limiteCredito, String tipoPedido, String tipoEntrega, String tipoPagamento, String tipoProduto, int diasDataVigencia) {
		
		return grupoEmpresaImpl.preencheParametros(tipoFaturamento, tipoContrato, prazoPagamento, limiteCredito, tipoPedido, tipoEntrega, tipoPagamento, tipoProduto, diasDataVigencia);
	}

	ResponseEntity<?> cadastro(GrupoEmpresaPersist grupoEmpresaPersist, String url, String tipo) {
		
		return grupoEmpresaImpl.cadastro(grupoEmpresaPersist, url, tipo);
	}

}
