package br.com.conductor.rhblueapi.integration.requisicao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RequestFactory {

	@Autowired
    private PostApi postApi;
	
	public RequestStrategy requisicao(String cenario) {

		switch (cenario) {

		case "POST":
			return postApi;

		default:
			return postApi;
		}

	}
}
