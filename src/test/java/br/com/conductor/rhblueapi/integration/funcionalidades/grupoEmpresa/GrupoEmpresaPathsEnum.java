package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import java.util.Objects;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.Getter;

public enum GrupoEmpresaPathsEnum {

	PATH_GRUPO_EMPRESA("/grupos-empresas");

	@Getter
	private final String path;
	
	GrupoEmpresaPathsEnum(String path) {
		this.path = path;
	}
	
	public static String getPath(String path) throws EnumValidationException {
		if (Objects.isNull(path)) {
			throw new EnumValidationException("null", "GrupoEmpresaPathsEnum");
		}
		
		for (GrupoEmpresaPathsEnum t : GrupoEmpresaPathsEnum.values()) {

			if (path.equals(t.name())) {
				return t.getPath();
			}

		}
		throw new EnumValidationException(path, "GrupoEmpresaPathsEnum");
	}
}
