package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import org.springframework.http.ResponseEntity;

import br.com.conductor.rhblueapi.domain.ParametrosPersist;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;

public interface GrupoEmpresaCustom {

	ParametrosPersist preencheParametros(String tipoFaturamento, String tipoContrato, int prazoPagamento,
			String limiteCredito, String tipoPedido, String tipoEntrega, String tipoPagamento, String tipoProduto,
			int diasDataVigencia);
	
	ResponseEntity<?> cadastro(GrupoEmpresaPersist grupoEmpresaPersist, String url, String tipo);
}
