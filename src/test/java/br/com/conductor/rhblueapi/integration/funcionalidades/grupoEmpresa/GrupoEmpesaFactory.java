package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GrupoEmpesaFactory {
	
	@Autowired
    private GrupoEmpresaIbgeInvalido grupoEmpresaIbgeInvalido;
	
	@Autowired
	private GrupoEmpresaValido grupoEmpresaValido;
	
	 public GrupoEmpresaStrategy cadastro(String cenario) {

         switch (cenario) {

              case "IBGE_INVALIDO":
                   return grupoEmpresaIbgeInvalido;
             
              default:
            	  return grupoEmpresaValido;
         }
		
    }
    
}
