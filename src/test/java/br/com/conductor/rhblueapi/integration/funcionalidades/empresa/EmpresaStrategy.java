package br.com.conductor.rhblueapi.integration.funcionalidades.empresa;

import br.com.conductor.rhblueapi.domain.persist.EmpresaPersist;

public interface EmpresaStrategy {

	public EmpresaPersist gerar();
}
