package br.com.conductor.rhblueapi.integration.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Property {

	public static String BROWSER;

	public static String API_FRONT;
	
	public static String TOKEN_BLUE;

	public static String API_BACK;

	public static String KLOV_URL;

	public static String KLOV_PROJECT_NAME;

	public static String KLOV_REPORT_NAME;

	public static String EXREPORTS_DOC_TITLE;

	public static String EXREPORTS_REPORT_NAME;

	public static String MONGO_URI;

	public static String EMISSOR;

	private static final String PROP_FILE_CONFIG = "src/test/resources/config.properties";
	
	public static String JIRA_CLIENT_ID;
	
	public static String JIRA_CLIENT_SECRET;
	
	public static String JIRA_ISSUE_KEYS;
	
	public static final String IMPORTED_SCENARIO_DIRECTORY = "src/test/resources/featureJira";
	
	public static final String CUCUMBER_JSON_REPORT_FILEPATH = "target/cucumber.json";
	
	public static String EMAIL;
	
	 private static Properties getProp() throws IOException {

         Properties props = new Properties();
         
         FileInputStream file = new FileInputStream(PROP_FILE_CONFIG);
         
         props.load(file);
         
         return props;
    }
	 
	 public static void loadProperties() {

         try {
        	 
              Properties properties = getProp();
              
              API_FRONT = properties.getProperty("api.front");
              
              API_BACK = properties.getProperty("api.back");
              
              BROWSER = properties.getProperty("browser");
              
              KLOV_URL = properties.getProperty("klov.url");
              
              KLOV_REPORT_NAME = properties.getProperty("klov.report.name");
              
              KLOV_PROJECT_NAME = properties.getProperty("klov.project.name");
              
              EXREPORTS_DOC_TITLE = properties.getProperty("exreport.document.title");
              
              EXREPORTS_REPORT_NAME = properties.getProperty("exreport.report.name");
              
              MONGO_URI = properties.getProperty("banco.mongo.uri");
              
              EMISSOR = properties.getProperty("nome.emissor");
              
              TOKEN_BLUE = properties.getProperty("token.blue");
              
              JIRA_CLIENT_ID = properties.getProperty("client.id");
              
              JIRA_CLIENT_SECRET = properties.getProperty("client.secret");
              
              JIRA_ISSUE_KEYS = properties.getProperty("jiraIssue.Keys");
              
              EMAIL = properties.getProperty("email");
              
         } catch (IOException e) {
        	 
              e.printStackTrace();
         }
    }
}
