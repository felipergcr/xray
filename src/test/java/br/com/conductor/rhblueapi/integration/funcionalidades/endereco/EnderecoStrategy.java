package br.com.conductor.rhblueapi.integration.funcionalidades.endereco;

import br.com.conductor.rhblueapi.domain.persist.EnderecoPersist;

public interface EnderecoStrategy {

	public EnderecoPersist gerar();
}
