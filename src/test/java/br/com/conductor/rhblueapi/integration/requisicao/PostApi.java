package br.com.conductor.rhblueapi.integration.requisicao;

import static java.util.Collections.singletonList;
import static org.springframework.http.CacheControl.noCache;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class PostApi implements RequestStrategy {

	@Autowired
	private Environment environment;
	
	@Override
	public ResponseEntity<?> enviar(final String url, final MultiValueMap<String, String> params, final String body) {
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(new StringBuilder("http://localhost:").append(environment.getProperty("local.server.port")).append(url).toString());

		if (!Objects.isNull(params)) 
			builder.queryParams(params);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(singletonList(APPLICATION_JSON_UTF8));
		headers.setContentType(APPLICATION_JSON_UTF8);
		headers.setCacheControl(noCache());
		
		HttpEntity<String> entity = new HttpEntity<>(body.toString(), headers);
		
		if (!Objects.isNull(body)) {
			 entity = new HttpEntity<>(body, headers);
		} else {
			 entity = new HttpEntity<>(headers);
		}
		
		TestRestTemplate template = new TestRestTemplate();

		return template.exchange(builder.toUriString(),
				HttpMethod.POST, entity, String.class);
	}
}
