package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;

import br.com.conductor.rhblueapi.domain.ParametrosPersist;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.integration.funcionalidades.empresa.EmpresaFactory;
import br.com.conductor.rhblueapi.integration.funcionalidades.empresa.EmpresaStrategy;
import br.com.conductor.rhblueapi.utils.Gerador;

@Component
public class GrupoEmpresaIbgeInvalido implements GrupoEmpresaStrategy {
	
	@Autowired
	private EmpresaFactory empresaFactory;
	
	@Override
	public GrupoEmpresaPersist gerar(ParametrosPersist parametrosPersist) {
		
		Faker faker = new Faker();
		Gerador gerador = new Gerador();
		EmpresaStrategy empresa = empresaFactory.cadastro("IBGE_INVALIDO");
		
		return GrupoEmpresaPersist.builder().nomeGrupo(new StringBuilder(faker.name().firstName()).append(" GRUPO").toString())
				.nomeSubgrupo(new StringBuilder(faker.name().firstName()).append(" SUBGRUPO").toString())
				.parametros(parametrosPersist)
				.empresaMatriz(empresa.gerar())
				.condicoes(gerador.gerarCondicoesGenericoUtils()).build();
	}

}
