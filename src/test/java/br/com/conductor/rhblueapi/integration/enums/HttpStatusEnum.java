package br.com.conductor.rhblueapi.integration.enums;

import java.util.Objects;

import br.com.conductor.rhblueapi.exception.EnumValidationException;
import lombok.Getter;

public enum HttpStatusEnum {

	CREATED("201"), OK("200"), NO_CONTENT("204"), BAD_REQUEST("400"), FORBIDDEN("403"), NOT_FOUND("404"), INTERNAL_SEVER_ERROR("500");

	@Getter
	private final String status;
	
	HttpStatusEnum(String status) {
		this.status = status;
	}
	
	public static String get(String status) throws EnumValidationException {
		if (Objects.isNull(status)) {
			throw new EnumValidationException("null", "HttpStatusEnum");
		}
		
		for (HttpStatusEnum s : HttpStatusEnum.values()) {

			if (status.equals(s.name())) return s.getStatus();

		}
		throw new EnumValidationException(status, "HttpStatusEnum");
	}
}
