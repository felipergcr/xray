package br.com.conductor.rhblueapi.integration.funcionalidades.grupoEmpresa;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.conductor.rhblueapi.domain.ParametrosPersist;
import br.com.conductor.rhblueapi.domain.persist.GrupoEmpresaPersist;
import br.com.conductor.rhblueapi.enums.ModeloDePedido;
import br.com.conductor.rhblueapi.enums.SegmentoEmpresaEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.ModeloDeCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoContratoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoEntregaCartao;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoFaturamentoEnum;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPagamento;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoPedido;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TipoProduto;
import br.com.conductor.rhblueapi.enums.gruposempresas.parametros.TransferenciaProdutos;
import br.com.conductor.rhblueapi.integration.config.Utils;
import br.com.conductor.rhblueapi.integration.requisicao.RequestFactory;
import br.com.conductor.rhblueapi.integration.requisicao.RequestStrategy;
import br.com.conductor.rhblueapi.resource.GrupoEmpresaResource;

@Component
public class GrupoEmpresaImpl implements GrupoEmpresaCustom {

	@Autowired
	private GrupoEmpresaResource grupoEmpresaResource;
	
	@Autowired
	private RequestFactory requestFactory;

	@Override
	public ParametrosPersist preencheParametros(String tipoFaturamento, String tipoContrato, int prazoPagamento,
			String limiteCredito, String tipoPedido, String tipoEntrega, String tipoPagamento, String tipoProduto,
			int diasDataVigencia) {
		
		ParametrosPersist parametrosPersist = new ParametrosPersist();
		BigDecimal limiteCreditoDisponivel = new BigDecimal(limiteCredito);
		List<TipoProduto> produtos = new ArrayList<>();

		parametrosPersist = ParametrosPersist.builder().dataVigenciaLimite(LocalDate.now().plusDays(diasDataVigencia))
				.emailNotaFiscal("felipe.roque@conductor.com.br")
				.limiteCreditoDisponivel(limiteCreditoDisponivel)
				.modeloCartao(ModeloDeCartao.COM_NOME)
				.modeloPedido(ModeloDePedido.WEB)
				.prazoPagamento(prazoPagamento)
				.segmentoEmpresa(SegmentoEmpresaEnum.E1_DIGITAL)
				.transferencia(TransferenciaProdutos.DESABILITADA)
				.tipoContrato(tipoContrato.equals(TipoContratoEnum.POS.name()) ? TipoContratoEnum.POS : TipoContratoEnum.PRE)
				.tipoEntrega(tipoEntrega.equals(TipoEntregaCartao.PORTA_A_PORTA.getValor()) ? TipoEntregaCartao.PORTA_A_PORTA : TipoEntregaCartao.RH)
				.tipoFaturamento(tipoFaturamento.equals(TipoFaturamentoEnum.CENTRALIZADO.name()) ? TipoFaturamentoEnum.CENTRALIZADO : TipoFaturamentoEnum.DESCENTRALIZADO)
				.tipoPagamento(tipoPagamento.equals(TipoPagamento.BOLETO.name()) ? TipoPagamento.BOLETO : TipoPagamento.TED)
				.tipoPedido(tipoPedido.equals(TipoPedido.CENTRALIZADO.name()) ? TipoPedido.CENTRALIZADO : TipoPedido.DESCENTRALIZADO)
				.build();
		
		if (tipoProduto.equals("VA")) produtos.add(TipoProduto.VA);
		else if (tipoProduto.equals("VR")) produtos.add(TipoProduto.VR);
		else {
			produtos.add(TipoProduto.VA);
			produtos.add(TipoProduto.VR);
		}
		
 	    parametrosPersist.setTipoProduto(produtos);		
		return parametrosPersist;
	}

	@Override
	public ResponseEntity<?> cadastro(GrupoEmpresaPersist grupoEmpresaPersist, String url, String tipo) {
		
		RequestStrategy request = requestFactory.requisicao(tipo);
		
		String body = Utils.converterObjetoParaString(grupoEmpresaPersist);
		
		return request.enviar(url, null, body);
	}
}
