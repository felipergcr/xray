package br.com.conductor.rhblueapi.integration.funcionalidades.empresa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmpresaFactory {
	
	@Autowired
    private EmpresaIbgeInvalido empresaIbgeInvalido;
	
	@Autowired
	private EmpresaValida empresaValida;
	
	public EmpresaStrategy cadastro(String cenario) {

		switch (cenario) {

		case "IBGE_INVALIDO":
			return empresaIbgeInvalido;

		default:
			return empresaValida;
		}

	}

}
