package br.com.conductor.rhblueapi.integration.requisicao;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public interface RequestStrategy {

	public ResponseEntity<?> enviar(String url, MultiValueMap<String, String> params, String body);
}
