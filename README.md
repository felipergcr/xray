# rh-blue-api

Projeto com o objetivo de criar interfaces para aplicações do RH.


## Requerimentos

Para executar a aplicação é necessário:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

# Arquitetura

## Tecnologias

* Java
* Spring 
* Spring Boot
* Maven
* Restfull (hateoas)
* Swagger
* JUnit
* Splunk

## Integrações

Este microserviço tem fronteiras com os seguintes serviços:

* Heimdall
* Pier Blue
* Splunk

## Health Check

http://{host}/actuator/health

## Estrutura do projeto

Definicao da estrutura do projeto 


# Desenvolvimento

## Executando a aplicação localmente

Há muitas formas para executar uma aplicação Spring Boot em localhost. Uma das formas é executar o método `main` no  na classe `br.com.conductor.portador.RhBlueApiApplication` à partir da IDE.

Alternativamente pode usar o [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) para executar via terminal desta forma:

```shell
mvn spring-boot:run -Pspring.profiles.active=dev
```

## Branches

Para o desenvolvimento, deve ser criado uma feature copiando a branch do projeto principal.

No projeto principal as branchs devem sempre estar sincronizadas com a branch master.

* **feature/*:** Branch que deve ser criada para o desenvolvimento das features, nasce à partir da *master* e deve ser removida após entrega na release.
* **release-n.n.n:** Branch que serão entregues as features via Merge Requests. Branch que será empacotada para promoção nos ambientes posteriores.
* **hotfix-n.n.n:** Branch que serão entregues os hotfix via Merge Requests. Branch que será empacotada para promoção nos ambientes posteriores.
* **master:**  Branch principal do projeto, esta branch reflete o código que está em produção. Deve ser sempre sincronizada com as branchs release e hotfix após entrada em produção.

## Pipeline e Deploy

O pipeline é inicializado a cada alteração nas branchs: feature/*, release e hotfix.

* **feature/*:** Executa build do código na branch e o teste integrado.
* **release-n.n.n:** Executa build e test do código na branch. É feito deploy nos ambientes QA, STAGING e PROD sob aprovação.
* **hotfix-n.n.n:** Executa build e test do código na branch. É feito deploy nos ambientes QA, STAGING e PROD sob aprovação.

Para verificar o que é executado em cada uma das branchs citadas verificar o arquivo .gitlab-ci.yml

## Merge Request / Code Review

No fluxo de desenvolvimento utilizamos a prática de Merge Request e Code Review para garantir a qualidade nas entregas e o controle de features que serão promovidas para os ambientes.


> Para mais informações visite: [Heimdall](https://github.com/getheimdall/heimdall) para ler sobre o gateway de entrada e [Pier](http://gitlab.conductor.tecnologia/pier-blue/pier-api) para a fronteira do consumo de serviços internos.


