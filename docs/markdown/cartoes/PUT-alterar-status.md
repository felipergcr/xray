# Resumo: Alterar status de cartão
Descrição: Este recurso permite alterar o status de um cartão.

## Arquitetura

**Nome da Tag:** Cartao

**Path:** PUT /cartoes/{id}/status

**Dependencia externa**
  * Banco de dados
  * PIER Blue


### Requisição

|Parâmetro | Descrição | Tipo parâmetro | Tipo de dados | Exemplo | Validador | 
|-|-|-|-|-|-|
| id | Identificador do cartão | Path | Long | 1 | Required |
| statusCartao | status atual do cartão | Body | statusCartaoEnum | 2 | Required |
| dataAgendamentoInicio | Data início para agendamento do status do cartão | Body | String | "2018-01-25" | - |
| dataAgendamentoFim | Data fim para agendamento do status do cartão | Body | String | "2018-01-25" | - |
| idUsuario | Identificador usuário logado | Body | Long | 1 | - |

O domínio dos status enviados no request podem ser consultados no path de domínios:
**Path:** GET /dominios/cartao/status

 - Para o bloqueio, deve ser enviado o status *BLOQUEADO_TEMPORARIO* no request.
 - Para o desbloqueio, deve ser enviado o status *NORMAL* no request.


##  Resposta

**Código status da resposta: 200**

## Detalhes de implementação

**Fluxo de execução**

1. Verifica se o novo status do cartão existe no banco de dados.
    1. Se novo status não existir retorna status 400.
2. Executa a ação de acordo com o novo status escolhido.
    1. Se a açao escolhida for Bloqueio
       POST /v2/api/cartoes/{id}/bloquear
    2. Se desbloqueio, altera status do cartão.
       POST /v2/api/cartoes/{id}/desbloquear
    3. Se o flagCancelamento do status requisitado for positivo, realiza o cancelamento do cartão
    4. Gerar uma nova via do cartão caso o cartão seja nominal e o status do cartao for CANCELADO_PERDA ou CANCELADO_ROUBO
       POST /v2/api/cartoes/{id}/gerar-nova-via
3. Verifica se tem agendamento de desbloqueio pelos campos do request 


## Casos de teste

* Realiza o cancelamento do cartão: 200
* Realiza o bloqueio do cartão: 200
* Realiza o bloqueio do cartão com agendamento: 200
* Realiza a mudança de status para um status inexistente: 404
* Realiza o cancelamento de um cartão que não existe: 404
* Realiza o cancelamento de um cartão já cancelado: 400
