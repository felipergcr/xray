# Resumo: Atualizar Funcionário
Descrição: Este recurso permite atualizar dados básicos de um funcionário.

**Path:** PUT /funcionarios/{id}

**Definition**
br.com.conductor.rhblueapi.resource.FuncionarioDefinition

**Resource**
br.com.conductor.rhblueapi.FuncionarioResource

**Dependencia externa**
  * Banco de dados

**Fluxo de execução**

1. Verifica se dados do request são válidos.
2. Verifica se id de usuário informado no request é válido.
3. Verifica se id de grupo empresa informado no request é válido.
4. Verifica se usuário possui permissão de acesso através do id de usuário e id do grupo empresa.
5. Verifica se id de funcionário informado no request é válido.
6. Verifica se usuário logado tem permissão para atualizar o funcionário solicitado.
7. Busca o tipo de entrega do cartão de acordo com os parametros do grupo empresa.
8. Realiza teste para verificar de acordo com o tipo de entrega se irá atualizar os dados do endereço.
- Se permirtir atualizar endereço
	- Verifica se request possui dados de endereço

9. Atualiza o atributo matrícula na entidade Funcionarios.
10. Atualiza o nome e data de nascimento na entidade Pessoas.
11. Verifica a condição de atualização do endereço
- Se atualizar endereço for verdadeiro
 	- Atualiza logradouro, numero, complemento, bairro, cidade, uf e cep. 

- Se atualizar endereço for falso
	- Não realiza atualização de endereço


## Casos de teste

1. O request possui dados inválidos: 400
2. Usuário não encontrado: 404  
3. Grupo empresa não localizado: 404
4. Usuário da sessão não possui vínculo para realizar essa operação: 403
5. Usuário da sessão não possui vínculo para realizar essa operação: 403
6. Usuário não possui acesso para atualizar os dados do funcionário: 403
7. Endereço obrigatório. : 400
8. Pessoa não encontrada: 404
9. Endereço não encontrado: 404
