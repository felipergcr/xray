]# Resumo: Upload de unidades de entrega
Descrição: Este recurso permite fazer upload de unidades de entrega em formato XLSX e XLS.

**Path:** POST /pedidos/upload

**Definition**
br.com.conductor.rhblueapi.resource.UnidadeEntregaDefinition

**Resource**
br.com.conductor.rhblueapi.UnidadeEntregaResource

**Dependencia externa**
  * Banco

**Fluxo de execução**

1. Importar planilha;
2. Valida Planilha;
3. Valida Usuário - GET /usuario/{id};
4. Validar Grupo Empresa;
5. Inserir a planilha na tabela ARQUIVO_UNIDADE_ENTREGA e ARQUIVO_BINARIO_UNIDADE_ENTREGA e ARQUIVO_UNIDADE_ENTREGA_DETALHE.

**Campos obrigatórios**
	planilha no formato XLSX e XLS

## Casos de teste

1. validar se a planilha está na extensão XLSX e XLS : 400
2. validar se usuário não existe: 404
3. validar se grupo empresa não existe: 404
4. validar se o usuário possuí permissão ao grupo : 403
5. Validar se a planilha está vazia: 400
6. Validar se o layout está no formato padrão: 400
7. Validar se a primeira linha de dados os campos obrigatórios estão preenchidos: 400 
