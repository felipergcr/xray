# Resumo: Upload de unidades de entrega
Descrição: Este recurso permite listar as unidades de entrega do grupo informado que estão com o status ativo.

**Path:** GET /unidades-entrega

**Definition**
br.com.conductor.rhblueapi.resource.UnidadeEntregaDefinition

**Resource**
br.com.conductor.rhblueapi.UnidadeEntregaResource

**Dependencia externa**
  * Banco
  	tabelas : 1. USUARIOS
  	          2. GRUPOSEMPRESAS
  	          3. PERMISSOESUSUARIOSRH
  	          4. UNIDADESENTREGA

**Fluxo de execução**

1. Recebe na request o id do usuário sessão(obrigatório), id do grupo empresa(obrigatório) e o id da unidade de entrega(não obrigatório)
2. Valida se o usuário sessão existe 
3. Valida se o grupo empresa existe
4. Validar se o usuário sessão possui permissão para visualizar as unidades e entrega do grupo empresa informado
5. Obtem as unidades de entrega ativo(s) de acordo com o grupo empresa e id da unidade de entrega informados na request

**Campos obrigatórios**
	id do usuário sessão
	id do grupo empresa

## Casos de teste

1. validar se usuário existe: 404
2. validar se grupo empresa existe: 404
3. validar se usuário possui permissão ao grupo empresa: 403
4. Validar listagem sem dados: 204
5. Validar listagem com dados: 200
