# Resumo:Lista de erros no upload do arquivo de unidades de entrega
Descrição: Este recurso permite listar o erros ocorridos ao fazer upload de arquivo com unidades de entrega.

**Path:** GET /unidades-entrega/arquivos/{id}/erros;

**Definition**
br.com.conductor.rhblueapi.resource.UnidadeEntregaDefinition

**Resource**
br.com.conductor.rhblueapi.UnidadeEntregaResource

**Dependencia externa**
  * Banco
  	tabelas : 1. USUARIOS
  	          2. GRUPOSEMPRESAS
  	          3. PERMISSOESUSUARIOSRH
  	          4. ArquivosUnidadeEntrega
  	          5. ARQUIVOSUNIDADEENTREGADETALHEERRO
  	          6. ARQUIVOSUNIDADEENTREGADETALHES

**Fluxo de execução**

1. Valida dados enviados no request.
2. Valida se o usuário sessão existe.
3. Valida se o grupo empresa existe. 
4. Validar se o usuário sessão possui permissão para visualizar as informações do grupo em questão.
5. Validar se id de arquivo de unidade de entrega existe.
6. Obtem lista com possíveis erros

**Campos obrigatórios**
	id do usuário sessão.
	id do grupo empresa.
	id do arquivo de unidade de entrega.

## Casos de teste

1. validar se usuário existe: 404
2. validar se grupo empresa existe: 404
3. validar se arquivo existe: 404
4. validar se usuário possui permissão ao grupo empresa: 403
5. Validar listagem sem dados: 204
6. Validar listagem com dados: 200
7. Validar existencia do arquivo: 412
8. Verificar se o status do arquivo esta como "invalidado" ou "concluido": 412
9. Validar paginação negativa: 412