# Resumo: Download do relatório
Descrição: Este recurso permite realizar o download do relatório informado. No momento só é possível realizar o download do relatório de detalhamento de pedidos.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.RelatorioResource

**Path:** GET /relatorios/{id}
**Nome do método: ** gerar()

**Depencia externa**
  **Banco** **Tabela**
  
  1.RelatorioPortal
  2.TipoRelatorioPortal
  3.GruposEmpresas
  4.Usuarios
  5.ARQUIVOCARGASTD
  6.ParametrosGruposEmpresas
  7.Parametros
  8.CargasBeneficios
  9.EmpresasCargas
  10.ArquivosCargasDetalhes
  11.Pessoas
  12.Empresas
  13.EmpresasCargasDetalhes
  14.EmpresasCargasDetalhesProdutos
  15.FuncionariosProdutos
  16.Contas
  17.CargaControleFinanceiro
  18.BoletosEmitidos
  19.TipoBoleto
  20.EventosExternosPagamentos
  
  **Service**
  GerenciadorAcessoService.validaIdentificaoEObtemIdPermissaoUsuarioLogado()
  RelatorioService.autenticarRelatorioProcessadoPorIdEUsuarioEGrupoEmpresa()
  RelatorioPedidoCriacao.gerarRelatorioPorGrupoEPeriodo()
  
### **Detalhes de Implatação**
**Fluxo de execução**

1. Verifica se Usuário existe;
2. Verifica se Grupo Empresa existe;
3. Verifica se usuário possui permissão atrelado ao grupo empresa;
4. Verifica se o relatório informado existe;
5. Verifica se o relatório solicitado esta com o status processado;
6. Verifica se existe dados para o relatório solicitado;
7. Reaiza a geração do relatorio para xls;
8. Disponibiliza o relatorio xls para o cliente realizar o download.

**Campos obrigatórios**
	idGrupoEmpresa, idUsuarioSessao, idRelatorio

## **Exceções**

| Código | Descrição | Mensagem |
|-|-|-|-|
| 404 | USUARIO_INEXISTENTE | Usuário não existe. |
| 404 | GRUPO_EMPRESA_NAO_ENCONTRADO | Grupo empresa não encontrado. |
| 403 | USUARIO_SESSAO_SEM_PERMISSAO | Usuário da sessão não possui vínculo para realizar essa operação |
| 404 | RELATORIO_NOT_FOUND | Não foi possível encontrar o relatório com os parâmetros informados |
| 400 | RELATORIO_DIFERENTE_PROCESSADO | O relatório precisa estar com o status processado para realizar a operação |
| 404 | RELATORIO_PEDIDO_EMPTY | Não existe dados para o relatório solicitado |
| 200 |  | Solicitação recebida com sucesso. |