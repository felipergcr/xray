# Resumo: Solicitar relatório
Descrição: Este recurso permite que o usuário realize a deleção de um ou mais relatórios que foram gerados por ele mesmo.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.RelatorioResource

**Path:** POST /relatorios/deletar
**Nome do método: ** deletar()

**Depencia externa**
  **Banco** **Tabela**
  
  1.RelatorioPortal
  2.Usuarios
  3.GruposEmpresas
  
  **Service**
  RelatorioService.deletar
  
### **Detalhes de Implatação**
**Fluxo de execução**

1. Verifica se Usuário existe;
2. Verifica se Grupo Empresa existe;
2. Verifica se o usuario/grupo empresa possui permissao para deletar o(s) relatorio(s);
3. Realiza a deleção dos relatorios informados.

**Campos obrigatórios**
	idUsuario
	
## **Exceções**

| Código | Descrição | Mensagem |
|-|-|-|-|
| 404 | USUARIO_INEXISTENTE | Usuário não existe. |
| 404 | GRUPO_EMPRESA_NAO_LOCALIZADO | Grupo empresa não localizado. |
| 403 | USUARIO_E_GRUPO_SEM_ACESSO_AO_RELATORIO | O usuário e o grupo informados não possui acesso ao(s) relatório(s) informado(s) |
| 201 |  |  |