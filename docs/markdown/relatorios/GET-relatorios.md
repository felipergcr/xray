# Resumo: Listar solicitações de relatórios
Descrição: Este recurso permite que o usuário visualize todas as suas solicitações de relatório abertas em um determinado período.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.RelatorioResource

**Path:** GET /relatorios
**Nome do método: ** solicitar()

**Depencia externa**
  **Banco** **Tabela**
  
  1.RelatorioPortal
  2.TipoRelatorioPortal
  3.GruposEmpresas
  4.Usuarios
  
  **Service**
  RelatorioService.listar
  
### **Detalhes de Implatação**
**Fluxo de execução**

1. Verifica se Usuário existe;
2. Verifica se Grupo Empresa existe;
3. Verifica se usuário possui permissão atrelado ao grupo empresa
4. Verifica se a data início não é maior que a data atual;
5. Verifica se a data fim é maior que a data atual;
6. Verifica se a data fim do período é menor que a data início;
7. Verifica se o intervalo do período solicitado não é maior do que permitido;

**Campos obrigatórios**
	idGrupoEmpresa, idUsuarioSessao, dataInicio e dataFim.
	
## **Exceções**

| Código | Descrição | Mensagem |
|-|-|-|-|
| 404 | USUARIO_INEXISTENTE | Usuário não existe. |
| 404 | GRUPO_EMPRESA_NAO_ENCONTRADO | Grupo empresa não encontrado. |
| 403 | USUARIO_SESSAO_SEM_PERMISSAO | Usuário da sessão não possui vínculo para realizar essa operação |
| 400 | PERIODO_DATAINICIO_MAIOR_DATAATUAL | A data início não pode ser maior que a data atual. |
| 400 | PERIODO_DATAFIM_MAIOR_DATAATUAL | A data fim não pode ser maior que a data atual. |
| 400 | PERIODO_DATAFIM_MENOR_DATAINICIO | A data fim do período não pode ser menor que a data início. |
| 400 | PERIODO_INTERVALO_MAIOR_QUE_O_PERMITIDO | O intervalo do período solicitado é maior do que permitido. |