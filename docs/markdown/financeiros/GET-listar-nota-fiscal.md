# Resumo: Consulta de notas
Descrição: Este recurso permite realizar o download de uma nota fiscal

**Path:** GET /financeiros/{codigoNotaFiscal}/nota-fiscal

**Definition**
br.com.conductor.rhblueapi.resource.FinanceiroDefinition

**Resource**
br.com.conductor.rhblueapi.FinanceiroResource

**Dependencia externa**
  * Banco
  * Deloitte Api

**Fluxo de execução**

1. Verifica se id de grupo empresa informado no request é válido.
2. Verifica se id de usuário informado no request é válido.
3. Busca permissão a partir de id do usuário e id do grupo empresa.
4. Busca empresas a partir do id da permissão.
5. Verifica se a partir das empresas o usuário possui acesso para realizar o download da nota fiscal.
6. Verifica se o código buscado é de uma nota fiscal.
7. Realiza autenticação na api de Deloitte
8. Realiza a busca na api da Deloitte recebendo um byte [] como retorno.
9. Realiza o download de um arquivo PDF.

**Campos obrigatórios**
	Número da nota fiscal (identificador da tabela CargaControleFinanceiro).

## Casos de teste

1. Grupo empresa não encontrado. : 412
2. Usuário não encontrado. : 412
3. Permissão não encontrada : 404
4. Usuário da sessão não possui vínculo para realizar essa operação : 403 
5. Usuário não possui acesso para visualizar a nota fiscal. : 403
6. O código pesquisado não é de uma Nota Fiscal, é de uma RPS. : 412
7. Ocorreu um erro ao tentar gerar o access token para acesso a Nota Fiscal. : 422
8. Ocorreu um erro ao tentar gerar a Nota Fiscal. : 422