# Resumo: Listagem de pedidos financeiros
Descrição: Este recurso permite listar os pedidos financeiros.

**Path:** GET /financeiros/pedidos

**Definition**
br.com.conductor.rhblueapi.resource.FinanceiroDefinition

**Resource**
br.com.conductor.rhblueapi.FinanceiroResource

**Dependencia externa**
  * Banco

**Fluxo de execução**

1. Verifica se grupo existe.
2. Verifica se usuário existe.
3. Consulta permissão de usuario e Grupo.
4. Consulta empresas a partir da permissão.
5. Consulta as cargas.

**Campos obrigatórios**
idUsuarioSessao 
idGrupoEmpresa 

## Casos de teste

1. Valida se o grupo existe: 412
2. Valida se o usuario existe: 412
3. Valida se o usuario e grupo tem permissão: 403
4. Valida se o usuario e tem empresas vinculadas a ele: 403
5. Validar se existe carga: 404

