# Resumo: Consulta de notas
Descrição: Este recurso permite consultar uma nota RPS informando o identificador da nota RPS.

**Path:** GET /relatorios/demonstrativo-faturas/{codigo}

**Definition**
br.com.conductor.rhblueapi.resource.FinanceiroDefinition

**Resource**
br.com.conductor.rhblueapi.FinanceiroResource

**Dependencia externa**
  * Banco
  * JasperReport

**Fluxo de execução**

1. Informar o identificador da nota.
2. Validar se existe dados nas views vw_produtos_rps e vw_dados_empresa_rps.
3. Compila os dados no Jasper.
3. Gerar PDF e retorna na request como bytes[].

**Campos obrigatórios**
	Número da RPS (identificador da tabela CargaControleFinanceiro).

## Casos de teste

1. Validar se existe nota: 400.
2. Gerar PDF em byte[]: 200

