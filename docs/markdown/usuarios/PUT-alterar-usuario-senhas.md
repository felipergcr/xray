# Resumo: Realizar operações em SENHA de Usuario do PIER
Descrição: Este recurso permite alterar senha conforme uma operação parametrizada na senha do Usuário do PIER.

## Detalhes de implementação

**Path:** PUT /usuarios/senhas

**Definition**
br.com.conductor.blue.resource.UsuarioDefinition

**Request**
br.com.conductor.blue.domain.request.UsuarioSenhaUpdateRequest

**Response**
br.com.conductor.blue.domain.response.AlterarSenhaResponse

**Dependencia externa**
 * SQL (ControleAcesso)
 * Mongo (User Token)

**Fluxo de execução**

1 - Validacao dos dados da requisição

2 - Verificar a operação

2.1 - Operação: *esqueci-senha*

2.1.1 - Validação dos dados de login conforme plataforma

2.1.2 - Recuperação de usuário

2.1.3 - Criação de token para usuário

2.1.4 - Envio de e-mail com template cadastrado

## Casos de teste

* Usuário não encotrado: 404

* Dados do request inválidos: 400

* Envio de email inválido: 400

* Envio de email sucesso: 201
