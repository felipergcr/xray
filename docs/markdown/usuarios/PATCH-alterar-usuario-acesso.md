# Resumo: Alterar dados de acesso do usuário no PIER
Descrição: Este recurso permite alterar os dados de acesso do usuário na plataforma PIER.

## Detalhes de implementação

**Path:** 
*PATCH* /usuarios/{id}/acessos

**Definition**
br.com.conductor.blue.resource.UsuarioDefinition

**Request**
br.com.conductor.blue.domain.request.UsuarioAcessoRequest

**Response**
br.com.conductor.blue.domain.response.UsuarioResponse

**Dependencia externa**
 * SQL (ControleAcesso)

**Fluxo de execução**

1 - Verifica se usuário existe
2 - Altera dados do usuário

## Casos de teste

* Alteração com sucesso: 200

* Fluxo de alteração com usuário não encotrado: 404 (Recurso não encontrado)

* Fluxo de alteração com dados do request inválidos: 400 (o único campo que valida é email -  mensage:Email com formato inválido)
