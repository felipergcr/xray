# **Listar operações de acesso.**
Descrição: Este recurso permite listar operações de acesso.

## **Arquitetura**

**Path:** GET /usuarios/operacoes

**Nome do Método:** listarOperacoesAcesso

### **Exemplo Request:**
```
curl -XGET "http://10.70.30.41:8080/usuarios/operacoes" -H "Accept: application/json;charset=UTF-8" -H "access_token: 31blue"

```
**Definition**
br.com.conductor.blue.resource.OperacaoAcessoDefinition

**Request**
br.com.conductor.portador.domain.OperacaoAcessoRequest

**Response**
br.com.conductor.blue.domain.response.OperacaoAcessoResponse

**Dependencia externa**
   DataBase Blue	

**Fluxo de execução**

1. Receber dados de entrada da consulta.
2. Validar dados de entrada.
3. Realizar consulta no banco de dados do Blue.
4. Montar resposta de operacoes de acesso.

## Casos de teste

* Listar uma ou mais operações de acesso com sucesso: 200
* Listar operações de acesso não encontrada: 204 (NO_CONTENT)
* Listar operações de acesso com filtro inválido: 400 (BAD_REQUEST)