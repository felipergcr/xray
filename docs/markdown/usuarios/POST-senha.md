# Resumo: Validar senha login usuário
Descrição: Este recurso permite validar a senha do login do usuário.

## Detalhes de implementação

**Path:** POST /usuarios/{login}/senha

**Definition**
br.com.conductor.blue.resource.UsuarioDefinition

**Request**
br.com.conductor.blue.domain.request.UsuarioValidarSenhaLoginRequest

**Response**
br.com.conductor.blue.domain.response.UsuarioResponse

**Dependencia externa**
 * Pier 
 * SQL (ControleAcesso)

**Fluxo de execução**


## Casos de teste




