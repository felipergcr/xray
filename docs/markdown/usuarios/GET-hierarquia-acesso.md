# Resumo: Lista hierarquia de acesso de um usuario RH.
Descrição: Este recurso permite listar a hierarquia de acesso de usuário do RH .

## Arquitetura

**Nome da Tag:** usuarios

**Path:** GET /usuarios/hierarquia-acessos

## Requisição
**br.com.conductor.rhblueapi.domain.request.UsuarioHierarquiaAcessoRequest
	
## Resposta

**br.com.conductor.rhblueapi.domain.response.UsuarioHierarquiaAcessoResponse

## Tabelas
USUARIOS
PERMISSOES
NIVELPERMISSAOACESSO

## Detalhes de implementação

**Fluxo de execusão**

1-  Valida dados do request.

2 - buscar usuário logado
	- se não existir
	2.1 - retornar status code 412 + msg
	
3 - buscar permissão do usuário logado
	- se não existir
	3.1 - retornar status code 412 + msg	

4-  Verifica se usuário já existe(chamada PIER).
	- se não existir...
	  4.1 - retornar status code 404(NOT_FOUD).
	
5-  Verifica se usuário possui permissão.
	- se não existir permissão 
	5.1 - retornar status code 200  + os dados usuário encotrado.

6- Verificar nível de acesso do usuário atual
	- se existir nível...
		6.1 -  retornar status code 200 + dados do usuário encotrado e estrutura de acesso nula;
	-se existir nível
		6.2 	-  filtrar nível de acesso de acordo com a visibilidade de nível do usuario logado;	
			
7-  Popular response e retornar status code 200.
	

## Casos de teste
* usuário logado não encotrado: 412;

* permissão usuário logado não encontrado:412;

* usuário logado não possui nível acesso:412;

* dados invalidados do request: 400;

* indenticador usuário logado inválido:412

* cpf de usuario não encontrado:404

* usuário encontrado:200