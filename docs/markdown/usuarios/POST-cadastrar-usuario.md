# Resumo: Cadastrar usuários no PIER
Descrição: Este recurso permite cadastrar usuários na plataforma PIER de diversas plataformas .

## Arquitetura

**Nome da Tag:** usuarios

**Path:** POST /usuarios

### Requisição

|Parâmentro | Descrisão | Tipo parâmentro | Tipo de dados | Exemplo | Validador | 
|-|-|-|-|-|-|
| CNPJ 		| CNPJ da empresa |  body | String 	  | 26598043000104   | Required |
| e-mail | E-mail do user master | body | String | user_master@email.com.br | Required |
| cpf | CPF do user master | body | String | 36370733765 | Required |
| nome | Nome do user  | body | String | "João Augusto" | Required |
| plataforma | Plataforma do Usuario | body | ENUM | ESTABELECIMENTO, RH, PORTADOR | Required |
	
## **Resposta** 

**Codigo status da resposta: 201**
	
### CadastroRhResponse
|Parâmentro | Descrisão | Tipo de dados | Exemplo | 
|-|-|-|-|
| id | Identificador do user master | Long |  1 |

### **Exemplo Response:**

```json
{
	"id": 12,       
}
```

## Detalhes de implementação

**Fluxo de execusão**

1-  Validar dados do request
	Post - Blue - /api/usuarios

2-  Valida dados do usuario 

3-  Definir o login e outros dados baseado no tipo de plataforma recebida

3.1-  Se for RH: login será o CPF

3.2-  Se for Estabelecimentos: login será o CPF

3.3-  Se for PORTADOR: login será o CPF

4 - Salva dados do usuário
	Post - pier/v2/api/usuarios



## Casos de teste

* Usuario já cadastrado: 400

* Dados de request nulos: 400 + json com mensagem de campos invalidos

* Plataforma invalida: 400


