# Resumo: Criar heirarquia de acesso para usuários do RH.
Descrição: Este recurso permite criar hierarquia de acesso(usuário caso não exista, permissão e nível de acesso) para usuários do RH .

## Arquitetura

**Nome da Tag:** usuarios

**Path:** POST /usuarios/hierarquia-acessos

## Requisição
br.com.conductor.rhblueapi.domain.persist.UsuarioPersist
	
## Resposta
br.com.conductor.rhblueapi.domain.response.UsuarioHierarquiaAcessoResponse


## Tabelas
USUARIOS
USUARIOSNIVEISPERMISSOESRH
PERMISSOESUSUARIOSRH

## Dependecias Service

br.com.conductor.rhblueapi.service.PermissoesUsuariosRhService.buscaPermissaPor();

br.com.conductor.rhblueapi.service.UsuarioPierService.criaUsuario();

br.com.conductor.rhblueapi.service.PermissoesUsuariosRhService.salvar();

br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService.salvarNivelPermissaoUsuario();

## Detalhes de implementação

**Fluxo de execusão**

1-  Valida dados do request.

2-  Verifica se usuário já existe(chamada PIER).
	- se não existir...
		2.1 - criar usuário.
		2.2 - criar permissão.
		2.3 - criar nível de acesso.
	
3-  	Verifica se usuário já possui permissão para o grupo.
	- se não existir
		3.1 -  criar permissão.
		3.2 -  criar nível de acesso.
	
4-  se já possui permissão lança execeção;
	

## Casos de teste

* Hierarquia de acessos criada: 200;
* Dados invalidos do request: 400;
* Usuário logado sem permissão para o grupo:403
* Usuário logado sem nível de acesso:403	
* Identificador usuário logado inválido:404
* Grupo Empresa não encontrado:404




