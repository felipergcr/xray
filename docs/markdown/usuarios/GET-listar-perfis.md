# **Listar perfis de acesso do RH.**
Descrição: Este recurso permite listar os perfis de acesso

## **Arquitetura**

**Path:** GET /perfis-acesso

**Nome do Método:** listarAcessosNiveisUsuario

**Definition**
br.com.conductor.blue.resource.AcessoNiveisUsuarioDefinition

**Response**
br.com.conductor.blue.domain.response.AcessoNiveisUsuarioResponse

**Dependencia externa**
   DataBase Blue

**Fluxo de execução**

1. Listar perfis de acesso da plataforma (database blue)

## Casos de teste

* Listar perfis de acesso com sucesso: 200
* Listar perfis de acesso dados não encontrados: 204 (NO_CONTENT)
* Parametro Plataforma inválido: 400 (BAD REQUEST)
