# Resumo: Atualizar hierarquia acesso de usuários do RH. 

Descrição: Atualiza o nível na hierarquia acesso do usuário. 

## Arquitetura 

**Nome da Tag:** usuarios 

**Path:** PUT /usuarios/{cpf}/hierarquia-acesso 

**Definition** 

br.com.conductor.blue.resource.UsuarioDefinition 

## Requisição 

br.com.conductor.blue.domain.request.HierarquiaAcessoUpdatePersit 

## Resposta 

org.springframework.http.HttpStatus 

## Dependências Service 

br.com.conductor.rhblueapi.service.UsuarioNivelPermissaoRhService.salvarNivelPermissaoUsuario();

## Tabelas 

*USUARIOSNIVEISPERMISSOESRH 
*USUARIOS
*PERMISSOESUSUARIOSRH

## Service
UsuarioPierService.recuperarUsuario();
PermissoesUsuariosRhService.buscaIdPermissaoAtivaPor();
UsuarioNivelPermissaoRhService.salvarNivelPermissaoUsuario();

## Detalhes de implementação 

**Fluxo de execução** 

1-Validar dados do request. 

2-Encotrar registro do usuário a ser atualizado. 

3-Encotrar permissão do usuário a ser atualizado. 

4 - Criar permissão para o nivel desejado.

5 - Responder com status http 200.  

## Casos de teste 

* Hierarquia de acessos criada: 200 

* Dados inválidos do request: 400 

* Usuário pretendido sem permissão:412 

* CPF não possui usuário criado:412 

* Usuário logado sem permissão para o grupo:403 

* Usuário logado sem nível de acesso:403  

* Identificador usuário logado inválido:404 

* Nível de acesso invalído:412 