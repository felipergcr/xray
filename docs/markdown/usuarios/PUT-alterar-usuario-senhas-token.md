# Resumo: Alterar SENHA pelo TOKEN de Usuario do PIER
Descrição: Alteração de Senha pelo TOKEN enviado para o usuário cadastrado.

## Detalhes de implementação

**Path:** PUT /usuarios/senhas/{token}

**Definition**
br.com.conductor.blue.resource.UsuarioDefinition

**Request**
br.com.conductor.blue.domain.request.UsuarioSenhaUpdateRequest

**Response**
org.springframework.http.HttpStatus

**Dependencia externa**
 * SQL (ControleAcesso)

 * Mongo (User Token)

**Fluxo de execução**

1 - Recuperar e Validar TOKEN

2 - Alterar senha


## Casos de teste
* Token inválido: 400

* Senha alterada com sucesso: 200
