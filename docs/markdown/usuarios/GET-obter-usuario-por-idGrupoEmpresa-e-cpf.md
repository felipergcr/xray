# Resumo: Obtem os dados do usuário
Descrição: Este recurso informa alguns dados do usuário passando o identificador do Grupo Empresa e o cpf.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.UsuarioResource

**Path:** GET /usuarios?cpf=&idGrupoEmpresa=
**Nome do método: ** Informa dados do usuário

**Depencia externa**
  **Banco** **Tabela**
  
  1.ControleAcesso_Blue.Usuarios
  
  **Service**
  UsuarioService.obterUsuario
  
### Response: br.com.conductor.rhblueapi.domain.response.UsuarioResumidoResponse

### **Detalhes de Implatação**
**Fluxo de execução**

1. Verifica se cpf é válido; (se passar um cpf inválido deve rejeitar)
2. Verifica se idGrupoEmpresa é valido; (se passar algo diferente de numerico deve rejeitar)
3. Verifica se idGrupoEmpresa existe;
4. Verifica se cpf existe;
5. Verifica se o cpf tem acesso ao grupo empresa informado;
6. Verifica se retorna as informações do usuário;

**Campos obrigatórios**
	idGrupoEmpresa e cpf
	
| Código | Descrição | Mensagem |
|-|-|-|-|
| 400 | idGrupoEmpresa inválido | Bad Request |
| 400 | cpf inválido | CPF inválido |
| 400 | USUARIO_NAO_LOCALIZADO | Usuário não encontrado |
| 400 | GRUPO_EMPRESA_NAO_LOCALIZADO | Grupo empresa não localizado. |
| 403 | CPF_SEM_PERMISSAO_AO_GRUPO | O CPF informado não possui vinculo com o Grupo empresa. |
