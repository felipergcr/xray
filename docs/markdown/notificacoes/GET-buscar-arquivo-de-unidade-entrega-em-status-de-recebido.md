# Resumo: Notificação de arquivo unidade entrega em processamento
Descrição: Este recurso permite informar se existe algum arquivo em processamento (status 0 - Recebido).

**Path:** GET /notificacoes/unidades-entrega/processando

**Definition**
br.com.conductor.rhblueapi.resource.NotificacaoDefinition

**Resource**
br.com.conductor.rhblueapi.NotificacaoResource

**Dependencia externa**
  * Banco
    tabelas : 1. USUARIOS
  	          2. GRUPOSEMPRESAS
  	          3. PERMISSOESUSUARIOSRH
  	          4. UNIDADESENTREGA
  	          5. ARQUIVOUNIDADEENTREGA

**Fluxo de execução**

1. Verifica se usuário existe.
2. Verifica se grupo existe.
3. Verifica se usuário logado possui permissão a algum nível de hirarquia no grupo
4. Consulta arquivo unidade entrega estiver em status 0 (Recebido).
5. Consulta unidade de entrega de acordo com o grupo empresa e status ativo se existe.

**Campos obrigatórios**
idUsuario 
idGrupoEmpresa 

## Casos de teste

1. Valida se o usuario existe: 404
2. Valida se o grupo existe: 404
3. Valida se usuário logado possui permissão 403
4. Consultar notificação de arquivo unidade entrega: 200