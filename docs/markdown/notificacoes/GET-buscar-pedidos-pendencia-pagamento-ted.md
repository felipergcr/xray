# Resumo: Notificação de pendência de pagamento na modalidade TED 
Descrição: Este recurso permite informar os ids de pedidos com pendências de pagamento na modalidade TED, de acordo com as permissões de um usuário.

**Path:** GET /notificacoes/pendencias-ted

**Definition**
br.com.conductor.rhblueapi.resource.NotificacaoDefinition

**Resource**
br.com.conductor.rhblueapi.NotificacaoResource

**Dependencia externa**
  * Banco

**Fluxo de execução**

1. Verifica se grupo existe.
2. Verifica se usuário existe.
3. Consulta permissão do usuário.
4. Consulta forma de pagamento a partir do grupo empresa.
5. Se TED: Continua o fluxo a partir do item 7.
6. Se Outros: Finaliza o processo. 
7. Consulta tipo de faturamento a partir do grupo empresa.
8. Se Centralizado:
8.1. Consulta nível de permissão do usuário.
8.2. Consulta pedidos com pendência na modalidade TED a partir do grupo empresa.
9. Se Descentralizado:
9.1. Consulta empresas a partir da permissão do usuário.
9.2. Consulta pedidos com pendência na modalidade TED a partir da(as) empresa(as).

**Campos obrigatórios**
idUsuarioSessao 
idGrupoEmpresa 

## Casos de teste

1. Valida se o grupo existe: 412
2. Valida se o usuario existe: 412
3. Valida se o usuario e grupo tem permissão: 404
4. Valida se a forma de pagamento do grupo existe: 412
5. Valida se a forma de pagamento não é TED: 412
5. Valida se o tipo de faturamento para o grupo existe: 412
6. Se Centralizado
6.1. Valida se o usuário possui nível de permissão: 404
7. Se Descentralizado:
7.1. Valida se usuário possui vínculo a empresas: 403
8. Verifica se existem pedidos com pendência de pagamento Ted, caso não exista: 204
9. Caso exista pedidos com pendência: 200