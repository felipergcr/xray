# Resumo: Notificação de arquivo pedido em processamento
Descrição: Este recurso permite informar se existe algum arquivo de pedido em processamento (status 0 - Recebido, status 1 - Importado).

**Path:** GET /notificacoes/pedidos

**Definition**
br.com.conductor.rhblueapi.resource.NotificacaoDefinition

**Resource**
br.com.conductor.rhblueapi.NotificacaoResource

**Dependencia externa**
  * Banco
    tabelas : 1. USUARIOS
  	          2. GRUPOSEMPRESAS
  	          3. PERMISSOESUSUARIOSRH
  	          4. ARQUIVOCARGASTD

**Fluxo de execução**

1. Verifica se usuário existe.
2. Verifica se grupo existe.
3. Consulta permissão do usuário.
4. Consulta arquivo pedido estiver em status 0 (Recebido) ou 1 (Importado).

**Campos obrigatórios**
idUsuarioSessao 
idGrupoEmpresa 

## Casos de teste

1. Valida se o usuario existe: 412
2. Valida se o grupo existe: 404
3. Valida se o usuario e grupo tem permissão: 403
