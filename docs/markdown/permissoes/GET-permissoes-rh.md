# Resumo: Lista permissões dos usuários/grupo empresa do rh.
Este recurso tem como objetivo listar permissões de usuários com o grupo empresa do rh.

## Detalhes de implementação

**Path:** GET /rh/permissoes/

**Definition** br.com.conductor.blue.resource.PermissoesUsuariosRhDefinition

**Request**
br.com.conductor.blue.domain.request.PermissoesRhRequest

**Response**
br.com.conductor.blue.domain.response.PermissoesRhResponse

**Dependência externa**
 
 * Banco de dados
   tabelas : PermissoesUsuariosRH

 **Fluxo de execução**

1. Valida dados enviados.

2. Lista as permissões de acordo com os dados enviados.

## Casos de teste

* Erro de validação de dados da request: 400

* Retorno vazio: 204

* Retorna as permissões existentes: 200
