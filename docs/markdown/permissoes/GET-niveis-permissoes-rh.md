Resumo: Lista nível de permissão de usuário.
Este recurso tem como objetivo listar nível de permissão de usuários (grupo/subbrupo/empresa).

## Detalhes de implementação

**Path:** GET /rh/permissoes/acessos

**Definition** br.com.conductor.blue.resource.UsuarioNivelPermissaoRhResource

**Request**
br.com.conductor.blue.domain.persist.UsuarioNivelPermissaoRhRequest

**Response**
br.com.conductor.blue.domain.response.PageUsuarioNivelPermissaoRHResponse

**Dependência externa**
 
 * Banco de dados
   tabelas : UsuarioNivelPermissaoRh.

 **Fluxo de execução**

1. Validar dados enviados.

2. Aplicar filtros e realizar busca	

3. Paginar acessos encotrados na busca no response.

## Casos de teste

* Erro de validação de dados do persist: 400

* Sem resultado na busca:204 

* Pagina com resultados: 200
