# Resumo: Associar permissão e operação de usuário.
Descrição: Este recurso permite associar permissão e operação de usuário.

## Arquitetura

**Nome da Tag:** RH

**Path:** PUT /rh/permissoes/{id}/operacoes

Resource: PermissoesOperacoesRhResource

Definition : PermissoesOperacoesRhDefinition

Request: PermissoesOperacoesRhPersist

Response: PermissaoOperacaoRhResponse

**Código status da resposta: 201**

## Detalhes de implementação

**Fluxo de execução**

1. Validação dos campos de entrada;

2. Validar se o tipo de operação existe.

2. Validar se o tipo de operação é de RH.

3. Validar se o tipo de permissão existe.

4. Validar se o id do usuário que fez a alteração existe  ou se for cadastro de usuário principal ignorar validações de usuario registro. 

5. Validar se o usuário que esta fazendo a alteração tem permissao  ou se for cadastro de usuário principal ignorar validações de usuario registro. 

6. Cria associação de permissão e operção.

7. reponse com o id do vinculo criado.

## Casos de teste

* Cria associação com sucesso: 200

* Tipo de operação não existe: 404

* Tipo de permissão não existe: 404

* Nivel grupo subgrupo empresa, fora de grupo pai: 403

* Id usuario responsavel pela associação de permissão não existe: 403

* Valores inválidos: 400
