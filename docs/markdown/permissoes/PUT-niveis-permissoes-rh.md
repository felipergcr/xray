Resumo: Define nível de permissão de usuário.
Este recurso tem como objetivo definir nível de permissão de usuários (grupo/subbrupo/empresa).

## Detalhes de implementação

**Path:** PUT /rh/permissoes/{id}/acessos

**Definition** br.com.conductor.blue.resource.UsuarioNivelPermissaoRhResource

**Request**
br.com.conductor.blue.domain.persist.UsuarioNivelPermissaoRhPersist

**Response**
br.com.conductor.blue.domain.response.UsuarioNivelPermissaoRhResponse

**Dependência externa**
 
 * Banco de dados
   tabelas : PermissoesUsuariosRH, GruposEmpresas, Usuario, Empresa, Status, StatusDescricao, UsuarioNivelPermissaoRh.

 **Fluxo de execução**

1. Valida dados enviados.

2. Valida se o id do usuário registro existe  ou se for cadastro de usuário principal ignorar validações de usuario registro.

3. Valida se o id  da permissão existe. 

5. Valida se usuarioRegistro está no mesmo grupo de permissao do usuário  ou se for cadastro de usuário principal ignorar validações de usuario registro.

6. Valida se tipo de nivel (grupo/subgrupo/empresa) existe e está dentro do Grupo da permissao.

7. Vincula permissão ao nivel.

8. Response com campos inseridos

## Casos de teste

* Erro de validação de dados do persist: 400

* Usuário inexistente: 404

* ID permissão inexistente: 404

* Vinculo nível de permissão não autorizado: 403

* Nivel Permissão de acesso(grupo empresa/subgrupo/empresa) não existente: 404

* Permissão criada com sucesso: 200