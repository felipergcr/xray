# Resumo: Altera permissão de usuários/grupo empresa do rh.
Este recurso tem como objetivo alterar dados da permissão de usuários com o grupo empresa.

## Detalhes de implementação

**Path:** PATCH /rh/permissoes/{id}

**Definition** br.com.conductor.blue.resource.PermissoesUsuariosRhResource

**Request**
br.com.conductor.blue.domain.request.PermissoesRhUpdate

**Response**
br.com.conductor.blue.domain.response.PermissoesRhResponse

**Dependência externa**
 
 * Banco de dados
   tabelas : Permissoes, GruposEmpresas, Usuario

 **Fluxo de execução**

1. Valida dados enviados.

2. Valida se existe a permissão.

3. Realiza a alteração dos dados da permissão.

## Casos de teste

* Erro de validação de dados da request: 400

* Permissao e operação a ser editada não existe: 404

* Permissão inexistente: 404

* Operação inexistente: 404

* Usuário inexistente: 404

* Permissao e operação já vinculada para usuário: 409

* Permissão alterada com sucesso: 200
