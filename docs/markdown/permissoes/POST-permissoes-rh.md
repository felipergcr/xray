# Resumo: Define permissão de usuários/grupo empresa do rh.
Este recurso tem como objetivo definir permissão de usuários com o grupo empresa do rh

## Detalhes de implementação

**Path:** POST /rh/permissoes/

**Definition** br.com.conductor.blue.resource.PermissoesUsuariosRhDefinition

**Request**
br.com.conductor.blue.domain.request.PermissoesRhPersist

**Response**
br.com.conductor.blue.domain.response.PermissoesRhResponse

**Dependência externa**
 
 * Banco de dados
   tabelas : PermissoesUsuariosRH, GruposEmpresas, Usuario

 **Fluxo de execução**

1. Valida dados enviados.

2. Valida se o código do usuário existe  ou se for cadastro de usuário principal ignorar validações de usuario registro.

3. Valida se o código do grupo empresa existe. 

4. Valida se já existe permissão para o mesmo usuário e grupo empresa ou se for cadastro de usuário principal ignorar validações de usuario registro.

5. Insere permissão de acesso.

## Casos de teste

* Erro de validação de dados do persist: 400

* Usuário inexistente: 404

* Grupo Empresa inexistente: 404

* Permissão criada com sucesso: 201

* Permissão de acesso existente: 400
