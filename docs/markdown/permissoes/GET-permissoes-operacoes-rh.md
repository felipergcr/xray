# Resumo: Listar permissão e operação de usuário.
Descrição: Este recurso permite listar permissão e operação de usuário.

## Arquitetura

**Nome da Tag:** RH

**Path:** GET /rh/permissoes/{id}/operacoes

Resource: PermissoesOperacoesRhResource

Definition : PermissoesOperacoesRhDefinition

Request: PermissoesOperacoesRhRequest

Response: PermissaoOperacaoRhResponse

**Nome do Método:** listaPermissoesOperacoesRH

**Código status da resposta: 200**

## Detalhes de implementação

**Fluxo de execução**

1. pesquisar permissoes de usuarios de acordo com os filtros aplicados

2. Odernar listar em ordem decrescente pelo id do recurso.

3. Paginar lista e devolver response.

## Casos de teste

* Lista com resultados de pesquisa: 200

* Sem resultado na pesquisa: 204
