# **Listar parâmetros de grupo empresa.**
Descrição: Este recurso permite listar os parametros do grupo empresa pelo id do grupo empresa.

## **Arquitetura**

**Path:** GET /grupos-empresas/{idGrupoEmpresa}/parametros

**Nome do Método:** listaParametrosGrupoEmpresa

### **Exemplo Request:**
```
curl -X GET "http://localhost:8080/grupos-empresas/8/parametros?id-usuario=1397354" -H "accept: application/json;charset=UTF-8"

```
**Definition**
br.com.conductor.blue.resource.GrupoEmpresaDefinition

**Resource**
br.com.conductor.blue.resource.GrupoEmpresaResource

**Campos obrigatórios**
- idGrupoEmpresa 
- idUsuario 

**Dependência externa**
   Banco de Dados

**Fluxo de execução**

1. Verifica se usuário existe.
2. Verifica se grupo existe.
3. Verifica se usuário possui permissão cadastrada.
4. Verifica se a permissão cadastrada possui níveis atrelados.
5. Verifica se existem parametros cadastrados para o grupo.


## Casos de teste

1. Valida se o usuario existe: 404 - Usuário não encontrado
2. Valida se o grupo existe: 404 - Grupo empresa não localizado
3. Valida se o usuario e grupo tem permissão: 403 - Usuário da sessão não possui vínculo para realizar essa operação
4. Valida se a permissão possui níveis atrelados: 404 - Nível de acesso não encontrado
5. Valida se existem parametros cadastrados para o Grupo Empresa: 404 - Parametros do grupo empresa não encontrados.
