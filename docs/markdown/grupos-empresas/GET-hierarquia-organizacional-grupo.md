# Resumo: Lista hierarquia organizacional do grupo
Descrição: Este recurso permite listar a hierarquia organizacional do grupo informado

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.PedidoResource

**Path:** GET /grupos-empresas/{id}/hierarquia-organizacional
**Nome do método :** Consultar a hierarquia organizacional do Grupo Empresa

**Dependencia externa**
  **Banco** **Tabelas**
  
  1.PERMISSOESUSUARIOSRH
  2.USUARIOSNIVEISPERMISSOESRH 
  3.GRUPOSEMPRESAS
  4.EMPRESAS
  5.PESSOAS
  
  **Service**
  HierarquiaOrganizacionalService.listarHierarquiaOrganizacionalGrupoResponse

### Response: br.com.conductor.rhblueapi.domain.response.HierarquiaOrganizacionalGrupoResponse

### **Detalhes de Implantação**
**Fluxo de execução**

1. Verifica se Usuário Sessão existe;
2. Verifica se Grupo Empresa existe;
3. Verifica se existe permissão para o usuário sessão e grupo empresa informados;
4. Retorna a lista das empresas disponíveis para essa permissão. Caso não encontre retorna 204;
5. Obtem a lista de Grupo/SubGrupo/Empresa com as empresas retornadas no item 4. Caso não encontre retorna 204;
6. Organiza a árvore hierarquica onde terá somente um grupo com pelo menos um subgrupo e pelo menos uma empresa.

**Campos obrigatórios**
	idGrupoEmpresa e idUsuarioSessao

## **Casos de teste**
* Consultar Hierarquia: 200
* Consultar Hierarquia Inexistente(Só ocorrerá se exitir uma permissão a um grupo empresa estruturado de uma forma incorreta): 204
* Não existe empresas para a permissão obtida pelo usuário sessão e grupo empresa informados: 204
* Grupo Empresa inexistente: 400
* Usuario Sessão inexistente: 400
* Consulta permissão inexistente com o usuário sessão e grupo empresa informado: 403

## **Excessões**

| Código | Descrição | Mensagem |
|-|-|-|-|
| 404 | USUARIO_INEXISTENTE | Usuário não existe. |
| 404 | GRUPO_EMPRESA_NAO_ENCONTRADO | Grupo empresa não encontrado. |
| 403 | USUARIO_REGISTRO_SEM_PERMISSAO | Usuário da sessão não possui vínculo para realizar essa operação |
| 204 | HIERARQUIA_ORANIZACIONAL_EMPRESAS_NAO_ENCONTRADO | Empresas não encontrado para o usuário e grupo empresa informado |
| 204 | NAO_HA_HIERARQUIA_ORGANIZACIONAL_GRUPO | Não existe hierarquia organizacional para o usuário e grupo empresa informado |