# Resumo: Dados do limite de crédito
Descrição: Este recurso informa dados do limite de crédito disponível para o grupo empresa pós-pago e que não fazem parte da lista de exceções (tabela ExcecoesLimiteDisponivel).

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.LimiteCreditoResource

**Path:** GET /limite-credito
**Nome do método: ** Informa dados do limite de crédito

**Depencia externa**
  **Banco** **Tabela**
  
  1.Parametros
  2.ParametrosGruposEmpresas
  3.Status
  4.StatusDescricoes
  5.ExcecoesLimiteDisponivel
  6.CargasBeneficios
  7.ARQUIVOCARGASTD
  8.EmpresasCargas
  9.BoletosEmitidos
  
  **Service**
  LimiteCreditosService.obterLimiteCredito
  
### Response: br.com.conductor.rhblueapi.domain.response.LimiteCreditoResponse

### **Detalhes de Implatação**
**Fluxo de execução**

1. Verifica se Usuário Sessão existe;
2. Verifica se Grupo Empresa existe;
3. Verifica se existe permissão para o usuário sessão e grupo empresa informados;
4. Verifica se é necessário informar dados do limite para o grupo empresa informado:
4.1 Caso o grupo empresa seja pré-pago retorna um response com o campo validaLimite = false;
4.2 Caso o grupo empresa esteja na white list (esteja na tabela ExcecoesLimiteDisponivel) retorna um response com o campo validaLimite = false;
4.3 Todos os outros casos deve seguir o fluxo seguinte;
5. Busca o valor do limite disponível do grupo empresa;
6. Busca o valor total utlizado nos pedidos realizados com sucesso que não foram pagos.
7. Através do valor limite disponível e o valor total utilizado (obtidos nos passos anteriores) será calculado a porcentagem utilizada e o valor disponível(diferença dos valores obtidos nos passos 5 e 6)
8. Retorna um response com todas as informações obtidas e calculadas.

**Campos obrigatórios**
	idGrupoEmpresa e idUsuarioSessao
	
## **Exceções**

| Código | Descrição | Mensagem |
|-|-|-|-|
| 404 | USUARIO_INEXISTENTE | Usuário não existe. |
| 404 | GRUPO_EMPRESA_NAO_ENCONTRADO | Grupo empresa não encontrado. |
| 403 | USUARIO_REGISTRO_SEM_PERMISSAO | Usuário da sessão não possui vínculo para realizar essa operação |