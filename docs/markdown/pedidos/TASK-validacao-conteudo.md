# Resumo: Task de validação do conteúdo
Descrição: Agendamento de tarefa para validação de conteúdo de excel.

Esta task de agendamento é executada para que seja validado o conteúdo de todas as células preenchidas do excel.

**Task** br.com.conductor.rhblueapi.task.CargaArquivoTasks.jobValidacaoConteudo

**Parametros**
app.tasks.conteudo.intervalo
app.tasks.conteudo.delayinicial

**Dependencia externa**
  * Banco de dados
  * Pier

**Fluxo de execução**

1. Verificar se há arquivos que foram interrompidos na metade de uma validação.
2. Verificar se há arquivos a serem validados.
3. Executar validações das linhas e se todas as celulas estiverem ok, será persistido uma linha na tabela  ArquivosCargasDetalhes e seu status será VALIDADO, caso ocorra erro na validação da linha, além do registro na tabela ArquivosCargasDetalhes, será gerado um registro na tabela ArquivosCargasDetalhesErros, caso ocorra "N" erros em um registro, serão gerados "N" registros com os respectivos erros. 
3.1 Cada linha não deve ultrapassar o valor permitindo de carga máxima de R$ 4.999.
3.2 São campos obrigatórios CNPJ, nome completo, CPF, Data de nacimento, produto (VA ou VR), valor do crédito, data do crédito, logradouro, endereço, número, bairro, cidade, UF e CEP.
3.3 Validar o campo CNPJ, CPF, CEP, Produto, Código da Unidade, Código Matrícula (apenas dígitos numéricos).
3.4 Validar o produto, é necessário ter preenchido 001 ou 002.
3.5 Validar o campo data do crédito. necessário verificar se está preenchido no formato correto (dd/MM/yyyy)
3.6 Validar o campo data de nascimento - necessário verificar se está preenchido no formato correto (dd/MM/yyyy)
4. Por fim, atualizar o status do pedido. Caso seja identificado que umas das linhas possui erro, o arquivo todo será recusado e o registro da tabela ARQUIVOCARGASTD sofrerá atualização de status para INVALIDADO, caso as linhas não possuam erros o status será IMPORTADO.

## Casos de teste

1. Se todos os registros estiverem válidos, o pedido será válido.
2. Se um dos registros possuir falha, o pedido será inválido. 