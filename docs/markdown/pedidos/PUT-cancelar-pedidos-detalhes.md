# Resumo: Cancelamento de detalhes do pedido.

Descrição: Este recurso permite cancelar pedidos parcialmente.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.PedidoResource

### Response: br.com.conductor.rhblueapi.domain.CargaBeneficio

**Path:** PUT  /pedidos/{idPedido}/cargas-detalhes/{idCarga}/cancelar?idUsuario={idUsuario}
**Nome do método :** cancelarPedidoDetalhes

**Dependencia externa**
  
  ** TABELAS
  1.USUARIOS
  2.ARQUIVOCARGASTD
  3.PERMISSOESUSUARIOSRH
  4.USUARIOSNIVEISPERMISSOESRH
  5.ARQUIVOSCARGASDETALHES
  6.CARGASBENEFICIOS
  
  **PROC
  SPR_Beneficio_CancelarCarga

### **Detalhes de Implantação**
**Fluxo de execução**

1. Verificar se usuário existe.

2. Verifica se pedido existe no banco de dados.

3. Verifica forma de faturamento do pedido é do tipo descentralizado
	
	1. se não for descentralizado respondemos com status 412;

4. verificar se usuário possui permissão

	1.validar se nível de permissão permite cancelar, o nível de permissão deve ser igual ou maior ao nível da empresa do pedido, caso seja 
	diferente apresentar status de acesso negado 403.

5. Verifica se status do ArquivoStd(pedido) permite cancelamento.

    1.Somente status 3(processado) ou status 6(cancelado parcialmente) permite cancelamento do pedido.
      Caso status diferente  retorna 412
      
6. Verifica se status da carga(pedidos detalhes) permite cancelamento.

    1.Somente status 0(Carga Efetuada - Aguardando Confirmação), status 1(Aguardando Pagamento) ou 2(Pedido Confirmado) permite cancelamento do pedido.
      Caso status diferente  retorna 412      

7. Verificar se pedido tem no mínimo 2 dias de antecedência do agendamento da carga para fazer o cancelamento.
	
8. Aciona proc para cancelar pedido detalhes.

9. Retorna status 200 em caso de sucesso e pedido atualizado com status de cancelado parcialmente.

**Campos obrigatórios**
	id, idUsuario.

## **Casos de teste**
Cancelar Pedido: 200
Pedido não encontrado: 404
Status do pedido não permite cancelamento: 412
Status da carga  não permite cancelamento: 412
Pedido com faturamento centralizado: 412
Usuário não existe: 404
Usuário sem nível de permissão grupo: 403
Usuário sem permissaão: 403
Pedido fora do período de cancelamento: 412