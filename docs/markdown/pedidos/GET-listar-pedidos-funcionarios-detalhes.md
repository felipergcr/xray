# Resumo: Lista de funcionarios da carga de um pedido
Descrição: Este recurso permite detalhar os funcionários de uma empresa (carga benefício) que faz parte de um pedido.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.resource.PedidoResource

**Path:** GET /cargas/{id}/funcionarios
**Nome do método :** listar funcionários que fazem parte da carga benefício

**Dependência externa**
  * Banco

### Request: br.com.conductor.rhblueapi.domain.request.PedidoDetalheFuncionarioRequest

### Response: br.com.conductor.rhblueapi.domain.response.PedidoDetalheFuncionarioResponse

### **Detalhes de Implantação**
**Fluxo de execução**


1. Verifica se usuário sessão existe.
2. Obtem/vefifica se a empresa carga existe através do carga benefício.
3. Obtem/verifica se a empresa existe através do id empresa da Empresa Carga.
4. Verifica se existe o grupo(subgrupo) na entidade empresa.
5. Com o subgrupo obtem/verifica se existe o grupo pai.
6. Verifica se o usuario sessão tem permissão para o grupo pai.
7. Verifica se existe funcionários na carga benefício informado.
8. Lista os funcionarios da carga beneficio de acordo com a paginação

**Campos obrigatórios**
	idUsuarioSessao e o idCargaBeneficio

## **Casos de teste**
* Consultar Detalhes da Carga Benefício: 200
* Usuário inexistente: 204
* Carga Beneficio inexistente: 204
* Empresa inexistente: 204
* Subgrupo inexistente: 204
* Grupo inexistente: 204
* Usuário sem permissão ao grupo: 403
* Permissão sem acesso a nenhuma empresa: 403
* Usuario que não possui permissão de acesso a empresa: 403
* Carga Benefício sem funcionários: 204
* Lista funcionários sem registro: 204

## **Exceções**

| Código | Descrição | Mensagem |
|-|-|-|-|
| 403 | USUARIO_REGISTRO_SEM_PERMISSAO | Usuário Registro não possui vínculo para realizar essa operação. |

| 204 | USUARIO_NAO_ENCONTRADO | Usuário não encontrado |
| 204 | CARGA_BENEFICIO_INEXISTENTE | Carga Beneficio não encontrado |
| 204 | EMPRESA_INEXISTENTE | Empresa não encontrada |
| 204 | GRUPO_EMPRESA_INEXISTENTE | Grupo empresa não encontrado. |