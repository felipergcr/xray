# Resumo: Cancelamento de pedido
Descrição: Este recurso permite cancelar um pedido.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.PedidoResource

### Response: br.com.conductor.rhblueapi.domain.ArquivoCargaStd

**Path:** PUT /pedidos/{id}/cancelar
**Nome do método :** cancelarPedido

**Dependencia externa**
  ** Banco** **Tabelas**
  
  1.USUARIOS 
  2.ARQUIVOCARGASTD
  3.PERMISSOESUSUARIOSRH
  4.USUARIOSNIVEISPERMISSOESRH
  5.ARQUIVOSCARGASDETALHES
  6.CARGASBENEFICIOS
  
  **PROC**
  SPR_Beneficio_CancelarCarga
  
### **Detalhes de Implantação**
**Fluxo de execução**

1. Verificar se usuário existe.

2. Verifica se pedido existe no banco de dados.

3. Verifica se a forma de faturamento do pedido é centralizado.
	
	1. se não for centralizado respondemos com status 412;

4. verificar se usuário possui permissão

	1.validar se nível de permissão é de grupo caso seja 
	diferente apresentar status de acesso negado.

5. Verifica se status do pedido permite cancelamento.

    1.Somente status 3 permite cancelamento do pedido
      Caso status diferente de 3 retorna 412

6. Verificar se pedido está no prazo de 2 dias antecedência  antes do agendamento da carga para fazer o cancelamento.
	
7. Aciona proc para cancelar pedido.

8. Retorna status 200 em caso de sucesso.

**Campos obrigatórios**
	id, idUsuarioSessao

## **Casos de teste**
* Cancelar Pedido: 200
* Pedido não encontrado: 404
* Status do pedido não permite cancelamento: 412
* Pedido com tipo faturamento descentralizado: 412
* Usuário não existe: 412
* Usuário sem nível de permissão grupo: 403
* Usuário sem permissão: 403
* Pedido fora do período de cancelamento: 412
