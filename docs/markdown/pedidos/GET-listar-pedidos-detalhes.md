# Resumo: Lista de pedidos
Descrição: Este recurso permite detalhar um determinado pedido.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.PedidoDetalhadoResource

**Path:** GET /pedidos/{id}/cargas-detalhes
**Nome do método :** listar cargas por empresas contidas no pedido selecionado do grupo.

**Dependencia externa**
  **Banco** **Tabelas**
  
  1.USUARIOS
  2.GRUPOSEMPRESAS 
  3.ARQUIVOCARGASTD
  4.PERMISSOESUSUARIOSRH
  5.USUARIOSNIVEISPERMISSOESRH
  6.cargasbeneficios
  7.empresascargas
  8.status
  9.statusdescricoes
  10.empresas
  11.pessoas
  12.empresascargasdetalhes
  13.empresascargasdetalhesprodutos
  14.funcionariosprodutos
  15.contas
  16.produtos

### Request: br.com.conductor.rhblueapi.domain.request.PedidoDetalhesRequest

### Response: br.com.conductor.rhblueapi.domain.response.PedidoDetalhesResponse

### **Detalhes de Implantação**
**Fluxo de execução**


1. Verifica se o usuário da sessão e grupo empresa selecionado tem permissão para ver o pedidos.
2. Verifica quais empresas "cnpj's" o usuario tem acesso para visualizar no detalhamento do pedido.
3. Lista todas as empresas que constam dentro do pedido selecionado.
4. Validações para a ação de cancelamento:
	4.1. Verifica se a carga é de um grupo com faturamento descentralizado.
	4.2. Verifica se o status do pedido é igual a 3 (Processamento concluído)
	4.3. Verifica o status da Carga Benefício deve estar com status 0 (Carga Efetuada - Aguardando Confirmação) ou 1 (Aguardando Pagamento) ou 2 (Pedido Confirmado).
	4.4. Verifica se a data de processamento é maior ou igual a 2 dias do crédito. 
	4.5. Se todas as condições anteriores forem verdadeiras a ação de cancelar será true, caso contrário será false. 

**Campos obrigatórios**
	idGrupoEmpresa, idUsuarioSessao e o idPedido 

## **Casos de teste**
* Consultar Pedidos: 200
* Consulta usuario sessão e grupo empresa: se não for encontrado: 403
* Consulta se as empresas dentro do pedido pode ser visualizado pelo usuário sessão: se o usuário não tiver acesso as empresas que constam no detalhe do pedido: 403

## **Exceções**

| Código | Descrição | Mensagem |
|-|-|-|-|
| 403 | USUARIO_REGISTRO_SEM_PERMISSAO | Usuário Registro não possui vínculo para realizar essa operação. |
| 403 | USUARIO_SEM_PERMISSAO_PEDIDO | Usuário não possue permissão para visualizar detalhes deste pedido. |
| 204 | GRUPO_EMPRESA_INEXISTENTE | Grupo empresa não encontrado. |
| 204 | PEDIDO_INEXISTENTE | Pedido não encontrado. |
