# Resumo: Upload de pedidos
Descrição: Este recurso permite fazer upload de pedido em formato XLSX e XLS.

**Path:** POST /pedidos/upload

**Definition**
br.com.conductor.rhblueapi.resource.PedidoDefinition

**Resource**
br.com.conductor.rhblueapi.PedidoResource

**Dependencia externa**
  * Banco
  * Pier

**Fluxo de execução**

1. Importar planilha
2. Verifica se a planilha é do tipo XLSX ou XLS
3. Valida Usuário
4. Obtem o tipo de entrega do cartão (Porta a Porta ou Lote) de acordo com o grupo empresa
5. Valida o cabeçalho da planilha
6. Armazena a planilha na base (ArquivosCargasBinario) e envia para a fila
7. Responde ao usuário com o id do pedido
8. Faz a leitura da fila, obtem os dados do arquivo e valida linha a linha:
8.0 Se alguma das linhas estiver com algum erro o arquivo não é processado e será enviado um email ao usuário informando o(s) erro(s)
8.1 Se as linhas foram validadas com sucesso o arquivo será processado e a carga será efetuada conforme solicidado no arquivo.

**Campos obrigatórios**
	planilha no formato XLSX e XLS
	id do usuário
	id do grupo empresa

## Casos de teste

1. validar se a planilha está na extensão XLSX e XLS : 400
2. validar se usuário existe: 400
3. validar se grupo empresa existe: 400
4. Validar se a planilha está vazia: 400
5. Validar se o layout está no formato padrão: 400
