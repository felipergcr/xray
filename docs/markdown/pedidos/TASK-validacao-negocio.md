# Resumo: Task de validação de negócio da carga
Descrição: Agendamento para validação de regras de negócio do pedido.

Esta task de agendamento é executada de acordo com o intervalo

**Task** br.com.conductor.rhblueapi.task.CargaArquivoTasks.validaNegocio

**Parametros**
app.tasks.negocio.intervalo
app.tasks.negocio.delayinicial

**Dependencia externa**
  * Banco

**Fluxo de execução**

1. Verificar se há arquivos que foram interrompidos na metade de uma validação (ArquivoPedido e ArquivoPedidoDetalhe)
2. Verificar se há arquivos a serem validados (ArquivoPedido)
3. Executar validações (alterando status das linhas (ArquivoPedidoDetalhe) para SUCESSO ou adicionando mensagem de motivo para FALHA)
3.1 Validar se o usuário que tá fazendo o pedido tem acesso à esse grupo.
4. Alterar status do (CargaBeneficio)


**Fluxo de execução DETALHADO**

*Fluxo selecionar arquivo Validação negócio*

1. Busca CargaBeneficio status 5 (Em processamento), origem FRONT-RH e data maior que 1 min (CONSTANTE)
2. Se tiver CargaBenefício
    1. Busca ArquivoCargasSTD pelo ID
    2. Atualiza DataProcessamento do CargaBeneficio
3. Se não encontrar
    1. Busca ArquivoCargasSTD com status *IMPORTADO* e origem FRONT-RH
4. Retorna valor encontrado ou nulo

*Gerador de eventos de validação de negócio*

1. Busca por detalhes do arquivo paginado em 100 (CONSTANTE)
2. Publica evento passando detalhes, número da página, total de elementos e informações do arquivo selecionado

*Validãção dos detalhes do arquivo*

1. Gera chave para identificar o evento
    1. KeySet da task = Join: idArquivo, página, dataprocessamento em ms
    2. ConcurrentHashMap = Chave para validar o término do processamento de um arquivo
2. Entra no loop dos detalhes do arquivo
    1. Validação: valida se o valor do produto do funcionário persistido somado ao valor enviado no detalhe é maior que R$ 4999,00 no mês de agendamento
3. Atualiza informações de funcionário
    1. Executa procedure: SPR_Beneficio_IncluirFuncionario
    2. Executa procedure: SPR_Beneficio_IncluirFuncionarioProduto
    3. Salva empresa carga benefício
        1. Busca empresa por id do arquivo carga, id da empresa e id do grupo empresa
        2. Se não encontrar salva o atual em processamento
        3. Salva EmpresasCargas atualizando valores de qtdNovosCartoes e o valor
        4. Adiciona EmpresasCargasDetalhes e EmpresasCargasDetalhesProdutos com status de "Em Processamento"
    4. Atualiza status e motivo (em caso de erro) de ArquivoCargasDetalhes
4. Valida se é a última página de validação do arquivo
5. Caso seja a última página atualiza os status das cargas: atualiza CargaBeneficio e EmpresasCargasDetalhes para o status 0 (Pendente)
6. Chama procedure de gerar RPS, SPR_Beneficio_CargaControleFinanceiro


## Casos de teste

1. Validar pedido totalmente válido: alteração de status para VALIDADO
2. Validar pedido parcialmente válido: alteração de status das linhas validadas para PARCIALMENTE_VALIDADO
3. Validar pedido inválido: alteração de status para INVALIDADO