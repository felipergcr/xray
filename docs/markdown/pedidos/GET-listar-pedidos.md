# Resumo: Lista de pedidos
Descrição: Este recurso permite listar os pedidos do grupo.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.PedidoResource

**Path:** GET /pedidos
**Nome do método :** listar pedidos do grupo.

**Dependencia externa**
  **Banco** **Tabelas**
  
  1.USUARIOS
  2.GRUPOSEMPRESAS 
  3.ARQUIVOCARGASTD
  4.PERMISSOESUSUARIOSRH
  5.USUARIOSNIVEISPERMISSOESRH
  6.cargasbeneficios
  7.empresascargas
  8.status
  9.statusdescricoes
  
  **Service**
  usuarioNivelPermissaoRhService.obterUsuarioNivelPermissaoAcessoAtivo

### Request: br.com.conductor.rhblueapi.domain.request.ArquivoPedidoRequest

### Response: br.com.conductor.rhblueapi.domain.response.PageArquivoCargasResponse

### **Detalhes de Implantação**
**Fluxo de execução**

1.Verifica se o usuario da sessão e grupo empresa selecionado tem permissão para ver os pedidos.
2. Listar pedidos.
3. Cada pedido possui o campo ações
4. Dentro de ações possui o campo "cancelar", ou seja, informa se é possível realizar o cancelamento desse pedido
	4.1. o cancelamento do pedido só é possível se as seguintes condições forem verdadeiras:
		4.1.1. O usuario sessão deve possuir nível de permissão de acesso ao grupo
		4.1.2. O status do pedido deve esta como "Processamento Concluído"
		4.1.3. O tipo do faturamento deve ser centralizado
		4.1.4. A menor data de crédito (data de agendamento) do pedido deve ser pelo menos 2 dias maior que a data corrente (regra das 24h)

**Campos obrigatórios**
	idGrupoEmpresa e idUsuarioSessao

## **Casos de teste**
* Consultar Pedidos: 200
* Consulta usuario sessão e grupo empresa: se não for encontrado: 403
* Consultar Pedidos: registros não encontrado: 204
* Consultar pedidos sem informar o grupo empresa: 400
* Consultar pedidos sem informar o usuário sessão: 400

## **Excessões**

| Código | Descrição | Mensagem |
|-|-|-|-|
| 204 | NAO_HA_PEDIDOS | Não existem pedidos.|
| 403 | USUARIO_REGISTRO_SEM_PERMISSAO | Usuário Registro não possui vínculo para realizar essa operação. |