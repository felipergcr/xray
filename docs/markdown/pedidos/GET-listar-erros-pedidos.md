# Resumo: Lista de erros de pedido
Descrição: Este recurso permite listar os detalhes de erros de validação de conteúdo de um pedido.

## **Arquitetura**

### Resource: br.com.conductor.rhblueapi.PedidoResource

**Path:** GET /pedidos/{idArquivoCarga}/erros
**Nome do método :** listarDetalhesErros.

**Dependencia externa**
  * Banco

### Response: br.com.conductor.rhblueapi.domain.response.ArquivoPedidoDetalhesErrosReponse

### **Detalhes de Implantação**
**Fluxo de execução**

1. Busca o pedido através do Id informado no path.
2. Caso o retorno seja null, será retornado o código 404.
3. Caso o retorno possua conteúdo, valida se o status do registro diferente de inválido. 412
4. Caso o retorno possua conteúdo, será feita uma consulta para buscar os id's na tabela ArquivosCargasDetalhes que possuam o valor do campo ID_ArquivoCargaSTD igual ao 
recebido no path e com status Invalidado.
5. Caso o retorno seja null, será retornado o código 204.
6. Caso o retorno possua conteúdo, será feita uma consulta para buscar os registros de erros na tabela ArquivosCargasDetalhesErros que possuam o valor do campo 
Id_DetalheArquivo dos valores dos id's recuperados no passo 3.
7. Caso o retorno seja null, será retornado o código 204.
8. Retorna os dados dos erros com o status 200.

## **Casos de teste**
* Consultar erros: 200
* Consultar erros com idArquivoCarga inexistente: 404
* Consultar erros de pedido com status diferente de inválido: 412
* Consultar erros com idArquivoCarga sem registros detalhados: 204
* Consultar erros com idArquivoCarga, com detalhes invalidos e sem registros de erros: 204

## **Excessões**
* 404 - GLOBAL_RECURSO_NAO_ENCONTRADO - Recurso não encontrado.
* 412 - PEDIDO_STATUS_DIFERENTE_INVALIDO - O status do pedido é diferente de inválido.
* 204 - NAO_HA_DETALHES_PEDIDO - Não existem detalhes para o pedido informado.
* 204 - NAO_HA_ERROS_PEDIDO - Não existem erros para o pedido informado.