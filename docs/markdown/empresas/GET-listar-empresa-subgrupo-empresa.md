# **Listar empresas.**
Descrição: Este recurso permite listar empresas a partir do id do subgrupo empresa.

## **Arquitetura**

**Path:** GET /subgrupos-empresas/{id}/empresas

**Nome do Método:** listarEmpresasdoSubGrupoEmpresa

### Definition: br.com.conductor.blue.resource.EmpresaDefinition

### **Exemplo Request**
```
curl -XGET "http://10.70.30.41:8080/subgrupos-empresas/{id}/empresas" -H "Accept: application/json;charset=UTF-8" -H "access_token: 31blue"

```

## **Resposta**

### Response: br.com.conductor.blue.domain.Empresas

### **Exemplo Response:**

```
{
  "number": 0,
  "size": 50,
  "totalPages": 1,
  "numberOfElements": 1,
  "totalElements": 1,
  "hasContent": true,
  "first": true,
  "last": true,
  "nextPage": 0,
  "previousPage": 0,
  "content": [
    {
      "id": 6543,
      "pessoa": {
        "dataNascimento": null,
        "nome": "Marquardt",
        "cnpj": "82982104606176",
        "sexo": " ",
        "idEmissor": 1,
        "numeroIdentidade": "",
        "idPessoa": 15989
      },
      "descricao": "Donnelly and Sons",
      "dataCadastro": "2018-11-08T10:29:00.000Z",
      "nomeExibicao": "Shawna",
      "status": 0,
      "flagMatriz": 1,
      "idGrupoEmpresa": 7108,
      "dataContrato": "2018-11-08",
      "nomeContato": "Devin",
      "cpfContato": "54647647803",
      "dataNascimentoContato": "2018-11-08",
      "emailContato": "bruno.santos.ext.teste-1564899818@conductor.com.br",
      "dddTelContato": "77",
      "numTelContato": "989581010",
      "banco": 3,
      "agencia": "0001",
      "contaCorrente": 1234,
      "dvContaCorrente": "1"
    }
  ]
}
```

** Código status da resposta: 200


### **Detalhes de Implantação**

**Fluxo de execução**

1. Validar paramêtros enviados.

2. Realizar consulta conforme filtros.

3. Listar empresas.



## **Casos de teste**

* Listar uma ou mais empresas com sucesso: 200
* Listar empresa do subgrupo empresa não encontrada: 204 

## **Excessões**

* Subgrupo não existe : 404
* Id informado não é de subgrupo : 400
* Listar empresa com filtro inválido: 400 