# **Listar subgrupos-empresas.**
Descrição: Este recurso permite listar os subgrupos pelo id do grupo empresa.

## **Arquitetura**

**Path:** GET /grupos-empresas/{id}/subgrupos-empresas

**Nome do Método:** listaSubgrupos

### **Exemplo Request:**
```
curl -X GET "http://localhost:8080/grupos-empresas/6663/subgrupos-empresas" -H "accept: application/json;charset=UTF-8"

```


### **Exemplo Response:**
```
{
  "number": 0,
  "size": 50,
  "totalPages": 1,
  "numberOfElements": 4,
  "totalElements": 4,
  "hasContent": true,
  "first": true,
  "last": true,
  "nextPage": 0,
  "previousPage": 0,
  "content": [
    {
      "idGrupoEmpresaPai": 6663,
      "idEmpresaPrincipal": 6285,
      "id": 6664,
      "nomeSubGrupo": "Liliane"
    }
  ]
}

```
**Definition**
br.com.conductor.blue.resource.GrupoEmpresaDefinition

**Resource**
br.com.conductor.blue.resource.GrupoEmpresaResource

**Response**
br.com.conductor.blue.domain.response.PageSubgrupoEmpresaResponse

**Dependência externa**
   DataBase Blue

**Fluxo de execução**

1. Consulta grupo empresa na base de dados do Blue passando o idGrupoEmpresa.
2. Consulta os Subgrupos na base de dados  do Blue passando idGrupoEmpresaPai.
3. Consulta os Subgrupos de empresa com permissão para o idUsuario.
4. Monta a resposta com paginação.

## Casos de teste

* Listar subgrupos de um grupo com sucesso: 200 (OK)
* Listar subgrupos de um grupo sem retorno: 204 (No Content)
* Listar grupo empresa com idGrupoEmpresa não existente: 404 (NOT_FOUND)

