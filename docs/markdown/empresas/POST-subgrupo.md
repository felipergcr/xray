# Resumo: Cadastrar um subgrupo de empresas.
Descrição: Este recurso permite cadastrar um novo subgrupo de empresa.

## Arquitetura

**Nome da Tag:** RH

**Path:** POST - /grupo-empresas/{id}/subgrupo-empresas


## Detalhes de implementação

**Fluxo de execução**

1. Validação dos campos e valida se GrupoEmpresaPai existe.

3. Verifica se empresa existe.

4. Cria o subgrupo.

5. Cadastra parâmetros para subgrupo.

6. Cria a empresa principal.

7. Cria um usuário principal caso não exista.

8. Cria permissão para usuário.

9. Cria nível permissão para usuário com o nível do subgrupo criado.

10. Cria todas operações para usuário.

11. Atualizar o id da empresa principal no subgrupo.

12. E gera response com id de subgrupo, usuário, permissão, nível acesso e id da empresa.


## Casos de teste

* Criar o subgrupo: 201

* Grupo empresa pai não existe: 400

* empresa já existe: 400

* empresa já possui vínculo com outro subgrupo:400.

* Usuário inexistente: 404

* ID permissão inexistente: 404

* Vinculo nível de permissão não autorizado: 403

* Nivel Permissão de acesso(grupo empresa/subgrupo/empresa) não existente: 404

* Permissão criada com sucesso: 200