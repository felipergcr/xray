# Resumo: Cadastrar um grupo de empresas.
Descrição: Este recurso permite cadastrar um novo grupo de empresa.

## Arquitetura

**Nome da Tag:** RH

**Path:** POST /grupos-empresas


**Dependência externa**

OperacaoAcessoService, PermissoesOperacoesRhService, PermissoesUsuariosRhService, UsuarioNivelPermissaoRhService, UsuarioPierRest, ParametroRepository, ParametroGrupoEmpresaRepository, UsuarioPierService, EmpresaService, SubgrupoRepository,GrupoEmpresaRepository

## Detalhes de implementação

**Fluxo de execução**

1. Validação dos campos persistidos.

2. Validação se não existe uma empresa cadastrada como a empresa principal.

3. Criar o grupo de empresas.

4. Criar subgrupo de empresas.

5. Criar a empresa principal associada ao sub-grupo.

6. Atualizar o id da empresa principal no grupo.

7. Salvar todos os parâmetros de grupo definidos.

7. Salvar todos os parâmetros de grupo em parâmetros de subgrupo.

8. Verifica se usuário existe, caso não criar usuario.

9. Conceder permissão de usuário para o grupo criado.

10.Conceder permissão todas operações existente para usuário

11.Conceder nível de permissão acesso de grupo para usuário.

12.Gerar responsta com dados dos recursos criados.

## Casos de teste

* Criar o grupo: 200

* Criar um grupo com empresa já cadastrada: 400

* Criar um grupo forma de pedido descentralizado e Faturamento centralizado: 400

* Prazo de pagamento fora de dias (0, 7, 15, 20, 30, 45, 60) : 400
