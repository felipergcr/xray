# Resumo: Atualizar empresa.
Descrição: Este recurso permite atualizar uma empresa.

## Arquitetura

**Nome da Tag:** RH

**Path:** PATCH /subgrupos-empresas/{id}/empresas/{idEmpresa}

Resource: br.com.conductor.rhblueapi.resource.EmpresaDefinition

Request: br.com.conductor.rhblueapi.domain.persist.EmpresaPersist

Response: br.com.conductor.rhblueapi.domain.response.EmpresaResponse

**Código status da resposta: 200**

## Detalhes de implementação

**Dependências**
* Banco de dados
* Pier

**Fluxo de execução**

1. Validação dos campos de entrada;
2. Validação se empresa já está cadastrada.
3. Validação se subgrupo já está cadastrado.
4. Verifica se tem endereço para ser atualizado.
    1. Busca endereço cadastrado da pessoa física.
    2. Atualiza endereço pessoa física.
5. Verifica se tem telefone para ser atualizado.
    1. Busca telefone da pessoa física.
    2. Atualiza telefone pessoa física.
6. Verifica quais campos da empresa devem ser atualizados.
7. Atualiza empresa.

## Casos de teste

* Atualizar empresa com sucesso: 200
* Atualizar empresa não cadastrada: 404
* Subgrupo informado não encotrado: 400
* Campos inválidos: 400
