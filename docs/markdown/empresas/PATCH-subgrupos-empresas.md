# Resumo: Alterar Subgrupo de Empresa 
Este recurso tem como objetivo alterar dados de Sugbrupo de Empresa.

## Detalhes de implementação

**Path:** PATCH /grupos-empresas/{id}/subgrupos-empresas/{idSubgrupo}

**Definition** br.com.conductor.blue.resource.GrupoEmpresaDefinition

**Request**
br.com.conductor.blue.domain.persist.SubgrupoEmpresaUpdate

**Response**
br.com.conductor.blue.domain.response.SubgrupoEmpresaResponse

**Dependência externa**
 
 * Banco de dados
   tabelas : GruposEmpresas

 **Fluxo de execução**

1. Valida dados enviados.

2. Valida se existe o grupo pai.

3. Valida se existe o grupo.

4. Caso Empresa informada, valida se empresa faz parte do sub grupo de empresa.

5. Realiza a alteração dos dados do subgrupo.

## Casos de teste

* Erro de validação de dados da request: 400

* Grupo Pai inexistente: 404

* Subgrupo inexistente: 404

* Caso Empresa informada, Empresa Inexistente: 404

* Subgrupo não é subgrupo do grupo pai: 412

* Caso Empresa informada, Empresa não faz parte do sub grupo: 412

* Subgrupo empresa alterado com sucesso: 200
