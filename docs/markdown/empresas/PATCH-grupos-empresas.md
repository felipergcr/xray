# Resumo: Atualizar grupo empresa.
Este recurso tem como objetivo de atualizar os grupos de empresa.

## Detalhes de implementação

**Path:** PATCH /grupos-empresas/{id}

**Definition** br.com.conductor.blue.resource.GrupoEmpresaDefinition

**Request**
br.com.conductor.blue.domain.request.GrupoEmpresaUpdate

**Response**
br.com.conductor.blue.domain.response.GrupoEmpresaResponse

**Dependência externa**
 
 * Base de dados Blue
   Tabelas : gruposempresas
   Tabelas : parametrosgruposempresas

 **Fluxo de execução**

1. Valida os dados enviados.

2. Consulta o grupo empresa.

3. Verifica se é um subgrupo.

4. Verifica se a empresa faz parte do grupo.

5. Consulta o grupo empresa com o id do grupo empresa e o id da empresa pricipal.

6. Consulta a empresa com o id da empresa principal.

7. Verifica se a empresa faz parte do grupo. 

8. Monta o objeto grupo empresa.

9. Salva o objeto atualizado do grupo empresa.

7. Atualiza os parametros do grupo empresa.

8. Monta o objeto de resposta.

## Casos de teste
* Atualizar grupo empresa com idGrupoEmpresa existente: 200

* Atualizar grupo empresa com idGrupoEmpresa não existente: 404

* Atualizar grupo empresa com IdEmpresaPrincipal existente: 200

* Atualizar grupo empresa com IdEmpresaPrincipal não existente: 404

* Atualizar o parâmetro grupo empresa com idParametro existente: 200

* Atualizar o parâmetro grupo empresa com idParametro não existente: 404