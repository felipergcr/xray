# Resumo: Cadastrar empresa.
Descrição: Este recurso permite cadastrar um nova empresa.

## Arquitetura

**Nome da Tag:** RH

**Path:** POST /subgrupos/{id}/empresas

Resource: EmpresaDefinition

Request: EmpresaPersist

Response: EmpresaResponse

**Código status da resposta: 201**

## Detalhes de implementação

**Fluxo de execução**

1. Validação dos campos de entrada;

2. Validação se o subgrupo existe;

3. Valida se empresa já existe

4. Cria empresa.

5. Criação do cliente e contas da empresa:
GET /api/origens-comerciais  (Guardar em cache)
 - Consultar as origens comerciais de cada produto;

6- Consultar os produtos disponíveis;

7 Cria conta com produto = Gerencial, cpf = CNPJ da empresa, canal de entrada = "BLUE-API";
 - Se o tipo de pagamento for pré-pago: limiteGLobal = 0, limiteMaximo = 0, limiteParcelas = 0;
 	- Caso seja pós-pago: limiteGLobal = valor retornado pela base Santander, limiteMaximo = valor retornado pela base Santander, limiteParcelas = 0;

8. Cria contas para os produtos requisitados pela empresa (VA, VR);

9. Cria um usuário principal caso não exista.

10. Cria permissão para usuário.

11. Cria nível permissão para usuário com o nível do subgrupo criado.

12. Cria todas operações para usuário.

13. E gera response com id da empresa, usuário, permissão, nível acesso.





## Casos de teste

* Cria a empresa com sucesso: 201
* Cria uma empresa já cadastrada: 400
* Erro no processo, paramentros de grupo empresa não cadastrados: 400.
* Subgrupo informado não encotrado: 400
