# **Listar empresas.**
Descrição: Este recurso permite listar empresas.

## **Arquitetura**

**Path:** GET /empresas

**Nome do Método:** listarEmpresa

### **Exemplo Request:**
```
curl -XGET "http://10.70.30.41:8080/empresass?idEmpresa=1,5,3" -H "Accept: application/json;charset=UTF-8" -H "access_token: 31blue"

```
**Definition**
br.com.conductor.blue.resource.EmpresaDefinition

**Request**
br.com.conductor.portador.domain.EmpresaRequest

**Response**
br.com.conductor.blue.domain.response.EmpresaResponse

**Dependencia externa**
   DataBase Blue	

**Fluxo de execução**

1. Listar empresa passando um filtro.
2. Realizar consulta no banco de dados do Blue.
3. Montar resposta de empresa.

## Casos de teste

* Listar uma ou mais empresas com sucesso: 200
* Listar empresa não encontrada: 404 (NOT_FOUND)
* Listar empresa com filtro inválido: 400 (BAD_REQUEST)