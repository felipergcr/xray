FROM nginx:latest

MAINTAINER Italo Silva

COPY ./dockerfile/config/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 443

ENTRYPOINT ["nginx"]

CMD ["-g", "daemon off;"]