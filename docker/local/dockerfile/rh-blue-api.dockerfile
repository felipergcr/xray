FROM openjdk:8-alpine

MAINTAINER ITALO SILVA

WORKDIR /app/rh-blue-api

COPY ./dockerfile/config/rh-blue-api.jar /app/rh-blue-api/.

CMD ["java", "-jar", "-Dspring.profiles.active=local", "rh-blue-api.jar"]

EXPOSE 8080