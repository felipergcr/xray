#!/bin/bash

echo "######################################## Navegando a pasta raiz do projeto "
cd ../../../

echo "######################################## Verificando existencia do jar na pasta target "
if [ -e ./docker/local/dockerfile/config/rh-blue-api.jar ]
	then
		echo "######################################## Arquivo existente, excluindo o jar "
			rm ./docker/local/dockerfile/config/rh-blue-api.jar
	else
		echo "######################################## Arquivo inexistente " 	
fi

echo "######################################## Gerar .jar da api "
mvn package -DskipTests

echo "######################################## Copiando .jar gerado "
cp ./target/rh-blue-api.jar ./docker/local/dockerfile/config

echo "######################################## Navegando ao diretório docker/local "
cd ./docker/local

echo "######################################## Removendo containers antigos "
docker stop $(docker ps -a -q --filter "name=rh-blue-api") && docker rm $(docker ps -a -q --filter "name=rh-blue-api")

echo "######################################## Removendo imagem antiga "
docker rmi $(docker images -q local/rh-blue-api)

echo "######################################## Realizando o build das aplicações "
docker-compose build

echo "######################################## Inicializando o ambiente "
docker-compose up

echo "######################################## Ambiente inicializado, acesse http://localhost/swagger-ui.html#/ "